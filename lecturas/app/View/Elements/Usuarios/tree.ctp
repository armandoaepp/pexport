<div id="arbol" class="page-aside app filters" >
      <div>
        <div class="content">
          <button class="navbar-toggle" data-target=".app-nav" data-toggle="collapse" type="button">
            <span class="fa fa-chevron-down"></span>
          </button>          
          <h2 class="page-title">Usuarios</h2>          
        </div>        
        <div class="app-nav collapse">
			<div class="form-group">
			
			</div>
          <div class="content">
            <ul class="nav nav-list treeview">
              <li class="open"><label class="tree-toggler nav-header"><i class="fa fa-folder-open-o"></i>Gestion Usuario</label>
                    <ul class="nav nav-list tree">
                      <li><a href="<?php echo ENV_WEBROOT_FULL_URL ?>usuarios/nuevo">Nuevo Usuario</a></li>
                      <li><a href="<?php echo ENV_WEBROOT_FULL_URL ?>personas/listado">Personas</a></li>
                    </ul>
                </li>
                </ul> 
          </div>
          
        </div>
      </div>
</div>