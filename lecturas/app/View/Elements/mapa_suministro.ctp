<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <title>GMaps.js &mdash; Static map with markers</title>
  <!-- 
  <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
   -->
  <?php 
  if(isset($latitud) && isset($longitud)){
  ?>
  <script src="http://maps.google.com/maps/api/js?sensor=true"></script>
  <script src="<?php echo ENV_WEBROOT_FULL_URL ?>js/gmaps.js"></script>
  <!-- <link rel="stylesheet" href="http://twitter.github.com/bootstrap/1.3.0/bootstrap.min.css" /> -->
  <script>
    $(document).ready(function(){
      var url = GMaps.staticMapURL({
        size: [324, 186],
        lat: <?php echo $latitud?>,
        lng: <?php echo $longitud?>,
        zoom: 16,
        markers: [
          {lat: <?php echo $latitud?>, lng: <?php echo $longitud?>, color: 'blue'}
        ]
      });
      $('<img/>').attr('src', url).appendTo('#map');
    });
  </script>
  <?php }?>
</head>
<body>
<a href="<?php echo ENV_WEBROOT_FULL_URL ?>reportes/mapa_literal/<?php echo $latitud?>/<?php echo $longitud?>" target="_blank">
    <div id="map"></div>
</a>
</body>
</html>