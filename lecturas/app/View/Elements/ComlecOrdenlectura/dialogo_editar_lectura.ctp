<div id="dialog-editar-lectura" title="Editar Lectura" style="display: none;">
	<span class="dialog_loading-editar-lectura" style="display: none;"><img
		src="<?php echo ENV_WEBROOT_FULL_URL ?>/js/jquery.select2/select2-spinner.gif">
		Cargando...</span>
	<div id='dialog-contenido-editar-lectura'>
		<input type="hidden" id="txt_suministro" name="txt_suministro">
		<input type="hidden" id="txt_index" name="txt_index">

		<div id="form-editar-lectura-contrasena">
		<div class="form-group" style="" >
			<label class="control-label">Contrase&ntilde;a:</label>              
        	<input type="password" id="txt_clave" name="txt_clave" class="form-control">       			
        </div>
        <button class='btn btn-info' id='continuar-editar-lectura'>Continuar</button>
        <button class='btn' id='cancelar-editar-lectura' onclick="javascript: $('#dialog-editar-lectura').dialog('close');">Cancelar</button>
        </div>
        
        <div id="form-editar-lectura-add-comentario" style="display: none;">
        <div class="form-group" style="" >
			<label class="control-label">Comentario:</label>              
        	<input type="text" id="txt_comentario" name="txt_comentario" placeholder="Ingrese un comentario del motivo del cambio" class="form-control">
        </div>
        
        <div class="form-group" style="" >
			<label class="control-label">Lectura:</label>              
        	<input type="text" id="txt_lectura" name="txt_lectura" class="form-control">
        </div>
        
        <div class="form-group" style="" >
			<label class="control-label">Observaci&oacute;n:</label>              
        	<input type="text" id="txt_obs" name="txt_obs" class="form-control">
        </div>
         
         <button class='btn btn-primary' id='guardar-editar-lectura'>Guardar</button>
         <button class='btn' id='cancelar-editar-lectura' onclick="javascript: $('#dialog-editar-lectura').dialog('close');">Cancelar</button>
         </div>
	</div>
</div>
<script>
$('#continuar-editar-lectura').click(function(){
	if($.trim($('#txt_clave').val())!=''){
		$('.dialog_loading-editar-lectura').show();
		$.ajax({
			url : ENV_WEBROOT_FULL_URL+'Usuarios/verificar_password',
			dataType: 'json',
			type: 'post',
			data: {'clave':$('#txt_clave').val()},
			success:function(data, textStatus, jqXHR){
	
				if(data.success){
					$('#txt_comentario').val('');
					$('#txt_lectura').val('');
					$('#txt_obs').val('');
					$('#form-editar-lectura-contrasena').hide();
					$('#form-editar-lectura-add-comentario').show();
				}else{
					alert(data.message);
					$('#dialog-editar-lectura').dialog('close');		
				}
				$('.dialog_loading-editar-lectura').hide();
				$('#txt_clave').val('');
			}
		});
	}else{
		alert('Ingrese una contraseña');
	}
});

$('#guardar-editar-lectura').click(function(){

	$('.dialog_loading-editar-lectura').show();
	var item = $('#txt_index').val();
	var lectant = $('#'+item+'lectura').val();
	var obsant = $('#'+item+'codigoobservacion').val();
	tipolectura = $('#'+item+'codigo').data('tipo_lectura');
	tipolecturareal = $('#'+item+'codigo').data('tipo_lectura_real');
	resultadoevaluacionant = $('#'+item+'resultadoevaluacion img').attr('title');

	if($.trim($('#txt_comentario').val())!=''){
		if($.trim($('#txt_lectura').val())!='' || $.trim($('#txt_obs').val())!=''){

			$('.dialog_loading-editar-lectura').hide();
			$("#form-editar-lectura-add-comentario").mask("Guardando...");
			
			$.ajax({
				url : ENV_WEBROOT_FULL_URL+'EvaluarInconsistencias/saveregistarlectura',
				dataType: 'json',
				type: "POST",
				data : { tipolectura:tipolectura,tipolecturareal:tipolecturareal,lectura: $('#txt_lectura').val(), codigo: $('#txt_suministro').val(), codigoobservacion: $('#txt_obs').val() },
				success:function(data, textStatus, jqXHR){
		
			       	var montoconsumo=data.montoconsumo;
			       	var resultadoevaluacion=data.resultadoevaluacion;
			       	var validacionlectura=data.validacionlectura;
			       	var validacioncodigo=data.validacioncodigo;
			       	var etiquetacodigo=data.etiquetacodigo;
			
			       	var mensaje=data.mensaje;
			       	var save=data.save;
			
			       	if (validacionlectura==false && validacioncodigo==false){
			       		
			       		$.ajax({
			       			url : ENV_WEBROOT_FULL_URL+'ComlecOrdenlecturas/save_commet_after_update_lectura_distriluz',
			       			dataType: 'json',
			       			type: "POST",
			       			data : { 
				       			suministro:$('#txt_suministro').val(),
				       			comentario: $('#txt_comentario').val(),
				       			lectant: lectant,
				       			lectact: $('#txt_lectura').val(),
				       			obsant: obsant,
				       			obsact: $('#txt_obs').val(),
				       			resultadoevaluacionant: resultadoevaluacionant,
				       			resultadoevaluacion: resultadoevaluacion },
			       			success:function(data, textStatus, jqXHR){
			       				if(data.success){
			       					$('#dialog-editar-lectura').dialog('close');
			       					$('#buscar-inconsistencia-lectura').click();
			       				}else{
				       				alert(data.msg);
			       				}
			       				$("#form-editar-lectura-add-comentario").unmask();
			       			}
			       		});
					}else{
						alert(mensaje);
						$("#form-editar-lectura-add-comentario").unmask();
					}
				}
			});
		}else{
			alert('Ingrese una lectura o codigo de observación.');
			$('.dialog_loading-editar-lectura').hide();
		}
	}else{
		alert('Ingrese un comentario del motivo del cambio.');
		$('.dialog_loading-editar-lectura').hide();
	}
});
</script>