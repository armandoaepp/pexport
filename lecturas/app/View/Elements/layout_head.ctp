<!-- CSS -->
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="DD ANALYTICS">
<meta name="author" content="DD ANALYTICS">
<link rel="shortcut icon" href="images/favicon.png">

<title>Clean Zone</title>
<!--link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,400italic,700,800' rel='stylesheet' type='text/css'-->
<!--link href='http://fonts.googleapis.com/css?family=Raleway:100' rel='stylesheet' type='text/css'-->
<!--link href='http://fonts.googleapis.com/css?family=Open+Sans+Condensed:300,700' rel='stylesheet' type='text/css'-->

<!-- Bootstrap core CSS -->
<link href="<?php echo ENV_WEBROOT_FULL_URL ?>js/bootstrap/dist/css/bootstrap.css" rel="stylesheet" />
<link rel="stylesheet" href="<?php echo ENV_WEBROOT_FULL_URL ?>fonts/font-awesome-4/css/font-awesome.min.css">

<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
<![endif]-->
	<link rel="stylesheet" type="text/css" href="<?php echo ENV_WEBROOT_FULL_URL ?>js/jquery.gritter/css/jquery.gritter.css" />

<link rel="stylesheet" type="text/css" href="<?php echo ENV_WEBROOT_FULL_URL ?>js/jquery.nanoscroller/nanoscroller.css" />
<link rel="stylesheet" type="text/css" href="<?php echo ENV_WEBROOT_FULL_URL ?>js/jquery.easypiechart/jquery.easy-pie-chart.css" />
<link rel="stylesheet" type="text/css" href="<?php echo ENV_WEBROOT_FULL_URL ?>js/bootstrap.switch/bootstrap-switch.css" />
<link rel="stylesheet" type="text/css" href="<?php echo ENV_WEBROOT_FULL_URL ?>js/bootstrap.datetimepicker/css/bootstrap-datetimepicker.min.css" />
<link rel="stylesheet" type="text/css" href="<?php echo ENV_WEBROOT_FULL_URL ?>js/jquery.select2/select2.css" />
<link rel="stylesheet" type="text/css" href="<?php echo ENV_WEBROOT_FULL_URL ?>js/bootstrap.slider/css/slider.css" />
<link rel="stylesheet" type="text/css" href="<?php echo ENV_WEBROOT_FULL_URL ?>js/intro/introjs.css" />
<!-- Custom styles for this template -->
<link href="<?php echo ENV_WEBROOT_FULL_URL ?>css/style.css" rel="stylesheet" />

<?php echo $this->element('environment_variables'); ?>
<script type="text/javascript" src="<?php echo ENV_WEBROOT_FULL_URL ?>js/jquery.js"></script>