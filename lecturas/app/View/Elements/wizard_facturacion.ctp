<ul class="steps">
	<li data-ref="<?= ENV_WEBROOT_FULL_URL?>Facturacion/wizard" class="step0 <?php if($active=='start') {echo 'active';$step=0;}?>">Inicio<span class="chevron"></span></li>
	<li data-ref="<?= ENV_WEBROOT_FULL_URL?>Facturacion/wizard_pliego" class="step1 <?php if($active=='importar_pliego') {echo 'active';$step=1;}?>">Paso 1: Importar Pliego<span class="chevron"></span></li>
	<li data-ref="<?= ENV_WEBROOT_FULL_URL?>Facturacion/wizard_crm" class="step2 <?php if($active=='importar_crm') {echo 'active';$step=2;}?>">Paso 2: Importar CRM<span class="chevron"></span></li>
	<li data-ref="<?= ENV_WEBROOT_FULL_URL?>Facturacion/wizard_cronograma" class="step3 <?php if($active=='importar_cronograma') {echo 'active';$step=3;}?>">Paso 3: Importar Cronograma<span class="chevron"></span></li>
	<li data-ref="<?= ENV_WEBROOT_FULL_URL?>Facturacion/wizard_uit" class="step4 <?php if($active=='uit') {echo 'active';$step=4;}?>">Paso 4: UIT y Alicuota<span class="chevron"></span></li>
	<li data-ref="<?= ENV_WEBROOT_FULL_URL?>Facturacion/wizard_alumbrado_publico" class="step5 <?php if($active=='AP') {echo 'active';$step=5;}?>">Paso 5: Confirmar AP<span class="chevron"></span></li>
	<li data-ref="<?= ENV_WEBROOT_FULL_URL?>Facturacion/wizard_finish" class="step6 <?php if($active=='finish') {echo 'active';$step=6;}?>">Sincronizar Todo<span class="chevron"></span></li>
</ul>
<script>
for(i=0;i<<?php echo $step;?>;i++){
	$('.step'+i).addClass('complete');
}
$('.steps li.complete').click(function(e){
	e.stopPropagation();
	window.open($(this).data('ref'),'_top');
});
</script>