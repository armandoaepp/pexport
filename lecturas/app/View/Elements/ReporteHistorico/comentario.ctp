<?php foreach ($obj_comentarios as $obj_comentario){?>
<li><i class="fa fa-comment"></i> <span class="date"><?php echo date('d M',strtotime($obj_comentario['0']["fecha"]));?>
</span>
	<div class="content">
		<p>
			<strong><?php echo $obj_comentario['0']["nombre"]?> </strong> <br>
			<?php echo $obj_comentario['0']["comentario"]?>
			<br>
		</p>
		<?php if(isset($obj_comentario['0']["imagen"]) && $obj_comentario['0']["imagen"]!=''){ ?>
			<a class="image-zoom" href="<?php echo ENV_WEBROOT_FULL_URL ?>images/upload/<?php echo $obj_comentario['0']["imagen"]?>"><img src="<?php echo ENV_WEBROOT_FULL_URL ?>images/upload/<?php echo $obj_comentario['0']["imagen"]?>" class="img-thumbnail" style="height:64px;" /></a>
		<?php }?>
		<small><?php echo date('Y-m-d H:i',strtotime($obj_comentario['0']["fecha"]));?>
		</small>
	</div></li>
<?php }?>