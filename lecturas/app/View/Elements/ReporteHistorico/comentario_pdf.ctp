<?php $meses = array(1 =>'Enero', 2 => 'Febrero', 3 => 'Marzo', 4 => 'Abril',
		5 =>'Mayo', 6 =>'Junio', 7 => 'Julio', 8 => 'Agosto', 9 =>'Septiembre', 10 => 'Octubre', 11 => 'Noviembre', 12 => 'Diciembre');?>

<?php foreach ($obj_comentarios as $obj_comentario){?>
<div class="content">
	<p>
		<strong><?php echo $obj_comentario['0']["nombre"]?></strong><span
			class="date">  -  <?php echo date("d",strtotime($obj_comentario['0']["fecha"]));?>
			de <?php echo $meses[date("n",strtotime($obj_comentario['0']["fecha"]))];?>
			del <?php echo date("Y",strtotime($obj_comentario['0']["fecha"]));?>
		</span> <br>
		<?php echo $obj_comentario['0']["comentario"]?>
		<br>
	</p>
	<?php if(isset($obj_comentario['0']["imagen"]) && $obj_comentario['0']["imagen"]!=''){ ?>
	<a class="image-zoom"
		href="<?php echo ENV_WEBROOT_FULL_URL ?>images/upload/<?php echo $obj_comentario['0']["imagen"]?>"><img
		src="<?php echo ENV_WEBROOT_FULL_URL ?>images/upload/<?php echo $obj_comentario['0']["imagen"]?>"
		class="img-thumbnail" /> </a>
	<?php }?>
</div>
<?php }?>