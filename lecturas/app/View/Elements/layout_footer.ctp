<script type="text/javascript" src="<?php echo ENV_WEBROOT_FULL_URL ?>js/jquery.js"></script>
<script type="text/javascript" src="<?php echo ENV_WEBROOT_FULL_URL ?>js/jquery.gritter/js/jquery.gritter.min.js"></script>
<script type="text/javascript" src="<?php echo ENV_WEBROOT_FULL_URL ?>js/jquery.nanoscroller/jquery.nanoscroller.js"></script>
<script type="text/javascript" src="<?php echo ENV_WEBROOT_FULL_URL ?>js/behaviour/general.js"></script>
<script src="<?php echo ENV_WEBROOT_FULL_URL ?>js/jquery.ui/jquery-ui.js" type="text/javascript"></script>
<script type="text/javascript" src="<?php echo ENV_WEBROOT_FULL_URL ?>js/jquery.sparkline/jquery.sparkline.min.js"></script>
<script type="text/javascript" src="<?php echo ENV_WEBROOT_FULL_URL ?>js/jquery.easypiechart/jquery.easy-pie-chart.js"></script>
<script type="text/javascript" src="<?php echo ENV_WEBROOT_FULL_URL ?>js/jquery.nestable/jquery.nestable.js"></script>
<script type="text/javascript" src="<?php echo ENV_WEBROOT_FULL_URL ?>js/bootstrap.switch/bootstrap-switch.min.js"></script>
<script type="text/javascript" src="<?php echo ENV_WEBROOT_FULL_URL ?>js/bootstrap.datetimepicker/js/bootstrap-datetimepicker.min.js"></script>
<script src="<?php echo ENV_WEBROOT_FULL_URL ?>js/jquery.select2/select2.min.js" type="text/javascript"></script>
<script src="<?php echo ENV_WEBROOT_FULL_URL ?>js/skycons/skycons.js" type="text/javascript"></script>
<script src="<?php echo ENV_WEBROOT_FULL_URL ?>js/bootstrap.slider/js/bootstrap-slider.js" type="text/javascript"></script>
<script src="<?php echo ENV_WEBROOT_FULL_URL ?>js/intro/intro.js" type="text/javascript"></script>
<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&amp;sensor=false"></script>
<!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script type="text/javascript">
      $(document).ready(function(){
        //initialize the javascript
        App.init();
        App.dashBoard();        
        
          introJs().setOption('showBullets', false).start();
		
      });
    </script>
	
<script src="<?php echo ENV_WEBROOT_FULL_URL ?>js/behaviour/voice-commands.js"></script>
<script src="<?php echo ENV_WEBROOT_FULL_URL ?>js/bootstrap/dist/js/bootstrap.min.js"></script>
<script type="text/javascript" src="<?php echo ENV_WEBROOT_FULL_URL ?>js/jquery.flot/jquery.flot.js"></script>
<script type="text/javascript" src="<?php echo ENV_WEBROOT_FULL_URL ?>js/jquery.flot/jquery.flot.pie.js"></script>
<script type="text/javascript" src="<?php echo ENV_WEBROOT_FULL_URL ?>js/jquery.flot/jquery.flot.resize.js"></script>
<script type="text/javascript" src="<?php echo ENV_WEBROOT_FULL_URL ?>js/jquery.flot/jquery.flot.labels.js"></script>
<?php echo $this->Html->script('require');?>
<script>
	require.config({
		baseUrl: ENV_WEBROOT_FULL_URL+"js",
		paths: {
		},
		waitSeconds: 1,
		packages: [
			{
				name: "jquery.gritter",
				main: 'js/jquery.gritter.min'
			}
		]
	});
</script>
require(['internal'],function(){});
