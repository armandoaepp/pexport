<script>
	/**
	* ENVIRONMENT VARIABLES
	 */
	var ENV_WEBROOT_FULL_URL = '<?php echo $this->Html->url('/',array('full'=>true));?>';
	var COUNT_LIMIT_DOCUMENT_UPLOAD = '<?php echo Configure::read('BIT_COUNT_LIMIT_DOCUMENT_UPLOAD');?>';
	var ALLOWED_EXTENSIONS_DOCUMENT_UPLOAD = '<?php echo implode(',',Configure::read('ALLOWED_EXTENSIONS_DOCUMENT_UPLOAD'));?>';
	var MAX_SIZE_DOCUMENT_UPLOAD = '<?php echo Configure::read('MAX_SIZE_DOCUMENT_UPLOAD');?>';
</script>