<?php 
if(Configure::read('environment') == 'production'){
?>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-58514706-1', 'auto');
  <?php
  if(isset($obj_logged_user['Usuario']['id']) && $obj_logged_user['Usuario']['id']!=''){ 
  ?>
  ga('set', '&uid', '<?php echo $obj_logged_user['Usuario']['id'].'-'.$obj_logged_user['Usuario']['nomusuario'];?>'); // Establezca el ID de usuario mediante el user_id con el que haya iniciado sesión.
  <?php }?>
  ga('send', 'pageview');

</script>
<?php }?>