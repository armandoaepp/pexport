<?php 
/*
App::import('Vendor', 'libchart', true, array(), 'libchart' . DS . 'libchart' . DS .'classes' . DS . 'libchart.php');

$chart = new VerticalBarChart();
$dataSet = new XYDataSet();

foreach ($arr_suministros as $suministro){
	$dataSet->addPoint(new Point($suministro['0']["pfactura"], (int) $suministro['0']["consant"]));
}

$chart->setDataSet($dataSet);

$chart->setTitle("Consumo de los ultimos 6 meses.[Referencial]");
$chart->render("files/CHART/demo1.png");
*/

$meses = array(1 => 'Enero', 2 => 'Febrero', 3 => 'Marzo', 4 => 'Abril', 5 => 'Mayo', 6 => 'Junio', 7 => 'Julio', 8 => 'Agosto', 9 => 'Septiembre', 10 => 'Octubre', 11 => 'Noviembre', 12 => 'Diciembre');
?>

<head>
        <meta charset=utf-8 />
        <title>CARTA CIRCULAR</title>
        <style>
            body{
              font-family: Tahoma, Helvetica, Arial, sans-serif; 
              color: #333;
              font-size: 13.5px;
            }
            p{
                margin-bottom: -2px;
            }
        </style>

    </head>
    <body>


       <div class='content' style='' >

           
            <div align="right" style='position:absolute; left:75%; top:-10px;' >
              <img src="./images/pdf/ensa.png" width="180px" height="46px"   >              
             </div>

            <div align="right"style='position:absolute; left:75%; top:97%;' >
              <img src="./images/pdf/distriluz.png" width="160px" height="40px" >
            </div>


            <p>Chiclayo, <?php echo date("d");?> de <?php echo $meses[date("n")];?> del <?php echo date("Y");?></p> <br/>

            <p>
            
            <b>CARTA CIRCULAR Nº <?php echo $obj_lectura[0][0]['idciclo'];?> - <?php echo date('Y');?><?php echo str_pad($i, 5, "0", STR_PAD_LEFT);?> - <?php echo $obj_lectura[0][0]['lcorrelati'];?></b> <br/>
             
            </p>

            <p>
                Señor(a) <br/> 
                <b><?php echo utf8_decode($obj_lectura[0][0]['cliente']);?></b> <br/> 
                          
                <?php echo utf8_decode($obj_lectura[0][0]['direccion']);?> <br/>  
                 <b>Ciclo:    <?php echo $obj_lectura[0][0]['nombciclo'];?>   -  Sector: <?php echo $obj_lectura[0][0]['sector'];?> Ruta: <span style="font-size: 16px"><?php echo $obj_lectura[0][0]['idrutareparto'].' - '.$obj_lectura[0][0]['lcorrelatireparto'];?></span></b> <br/>
            </p>

            <p>
                 <?php 
                $pfactura_anio = substr($obj_lectura[0][0]['pfactura'],0,4);
                $pfactura_mes = (int) substr($obj_lectura[0][0]['pfactura'],4);
                $name_mes = $meses[$pfactura_mes];
                ?>

                Asunto   &nbsp; :  <b> Consumo facturado Periodo <?php echo $name_mes;?> <?php echo $pfactura_anio;?>  </b><br/>        
                Ref.&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;:  Suministro Nº <b><span style="font-size: 16px"><?php echo $obj_lectura[0][0]['suministro'];?></span></b>
            </p><br/> 

            <p>De mi especial consideración:</p>         

            <p style="text-align:justify;">
                Es grato dirigirme a usted, para saludarlo (a) y al mismo tiempo comunicarle que nuestra empresa se encuentra implementando nuevos avances tecnológicos en los procesos que involucran la gestión de nuestros clientes.
			</p>
			
			<table border="0" style="width: 100%; padding: 0px; margin-left: -2px;">
			<tr>
			<td>
			<p style="text-align:justify;">	
                Dentro de las mejoras implementadas, se tiene la utilización de equipos móviles para el registro de la lectura de su medidor, validación en línea de su consumo, GPS para el monitoreo de la actividad, tomas fotográficas para la comprobación de los datos obtenidos, así como el análisis de su consumo con la aplicación de herramientas predictivas, que nos permite garantizar un proceso de facturación de calidad, eliminando los errores propios de la operación manual.
			</p>
			<p style="text-align:justify;">	
                En este sentido, estamos implementado un sistema de previsión ante situaciones que pudieran determinar alguna consulta sobre su facturación emitida, motivo por el cual hemos realizado la evaluación de su consumo facturado teniendo como resultado una diferencia de <?php echo number_format(($obj_lectura[0][0]['lecturao1']-$obj_lectura[0][0]['lecant']),0);?> kWh entre su lectura del mes anterior (<?php echo number_format($obj_lectura[0][0]['lecant'],0);?>) y la lectura del presente proceso de facturación (<?php echo number_format($obj_lectura[0][0]['lecturao1'],0);?>), tal como se evidencia en la toma fotográfica adjunta, la cual consta de fecha y hora de registro.
            </p>
			</td>
			<td style="text-align: center;">
			<img width="340px" height="330px" style="margin-top: 11px;" src="data:image/png;base64,<?php echo $this->Image->applyWatermark(trim($obj_lectura[0][0]['foto1']),$obj_lectura[0][0]['fechaejecucion1'])?>">
			</td>
			</tr>
			</table>            
            

            <p style="text-align:justify;">
            <br>
                Agradeciéndole la atención brindada a la presente carta, aprovecho la oportunidad para expresarle los sentimientos de nuestra consideración, poniendo a vuestra disposición nuestros canales de atención para que usted desde la comodidad de su casa u oficina, pueda realizar las consultas necesarias ante cualquier inquietud del servicio que le brindamos mediante SERVILUZ al 481200 o nuestras oficinas de atención al cliente.
            </p>
            <br/>
           
            <p> Atentamente, </p>
     
           
            <div style="margin-left:55px;" >
                
                <img width="120px" height="100px" src="./images/pdf/firmaGerente.png">  
            </div>
            
            <div align="center" style="width:250px; border-top: 1px solid black; margin-top:-50px; margin-bottom:-60px;" >
                
                <b >Ing. Luis Eduardo Piscoya Salazar </b> <br/>
                Gerente Comercial

            </div>

            <div style=" position:absolute; top:96%; height:60px; border-top: 2px solid black; font-size:10.5px; margin-top:20px;" class="pie">
                
                <p>
                    Empresa Regional de Servicio Público de Electricidad del Norte S.A. - Electronorte S.A. <br/>
                    Ca. San Martin Nº 250 - Chiclayo,  Perú. <br/>
                    RUC: 20103117560 e-mail: electronorte@distriluz.com.pe Web: www.distriluz.com.pe/ensa<br/>
                    Telf: (074) 48-1210   Serviluz: (074) 48-1200 <br/>
                </p>

            </div>


        </div>


    </body>
