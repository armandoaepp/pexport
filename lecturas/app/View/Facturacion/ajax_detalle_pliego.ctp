<div class="container-fluid" id="pcont">

<h2 class="text-center" style="margin-top: 15px;">Detalle de Pliego: <?php echo $pliego;?></h2>

<div class='row'>
	
		<div class='col-sm-12 col-md-12'>
			<div class='block-flat'>
				<div class='content overflow-hidden'>
<table class='red'>
<thead>
<tr>
<th>Sistema Electrico</th>
<th>Sistema Electrico Abreviatura</th>
<th>Tarifa</th>
<th>Concepto</th>
<th>Precio Fose</th>
<th>Precio Sin Fose</th>
<th>Pliego</th>
</tr>
</thead>
<tbody>
<?php 
foreach ($arr_detalle_pliego as $k => $v){
	?>
	<tr>
	<td><?php echo $v[0]['nombresistemaelectrico'];?></td>
	<td><?php echo $v[0]['abreviaistemaelectrico'];?></td>
	<td><?php echo $v[0]['tarifa'];?></td>
	<td><?php echo $v[0]['nombreconcepto'];?></td>
	<td><?php echo $v[0]['preciofose'];?></td>
	<td><?php echo $v[0]['preciosinfose'];?></td>
	<td><?php echo $v[0]['fechadesde'];?></td>
	</tr>
	<?php 
}
?>
</tbody>
</table>
</div>
			</div>
		</div>
</div>
</div>