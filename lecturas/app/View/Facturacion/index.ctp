<?php 
if (isset($id_usuario) && $id_usuario == 25 || $id_usuario == 131){
	$permiso_lectura = 'false';
}else {
	$permiso_lectura = 'true';
}
echo $this->element('menu',array('active'=>'usuarios', 'open'=>'false', 'permiso_lectura' =>$permiso_lectura, 'id_usuario ' =>isset($id_usuario)?$id_usuario:null));

?>

<link rel="stylesheet" type="text/css" href="<?php echo ENV_WEBROOT_FULL_URL;?>css/demo2.css" />
<link rel="stylesheet" type="text/css" href="<?php echo ENV_WEBROOT_FULL_URL;?>css/common.css" />
<link rel="stylesheet" type="text/css" href="<?php echo ENV_WEBROOT_FULL_URL;?>css/style4.css" />

<style type="text/css">
	.big-text{
		font-size: 30px !important;
	}

	.fact-data > h2{
		font-size: 22px !important;
	}
	.fact-data-pro {
		width: 37% !important;
	}

	.fact-data > h3{
		font-size: 16px !important;
		font-weight: 600 !important;
		color: #428bca !important ; 
	}

	.block-flat{
		margin-bottom: 0px  !important;
	}

	.widget-block{
		margin-bottom: 10px!important ; 
		
	}
	.profile-info{
		background-color: #eee !important ; 
		
	}
	.icolor{
		color: #1f384d !important ; 
		
	}
	.profile-info2{
		background-color: #1f384d;
	}

	.titulo2{
		font-size: 20px; 
		margin-bottom: 5px;  
		margin-top: 6px;  
		font-family: 'Open Sans', sans-serif;   
		font-weight: 780 !important;  
		letter-spacing: 2px;
		color:#fff;
	}

</style>

<div class="container-fluid" id="pcont">
		

    	<div class="cl-mcont">
    		
    		<div class="block-flat profile-info2" style="margin-bottom:20px !important;">
				<h3 class="text-center titulo2" style=" ">MÓDULO DE FACTURACIÓN EN SITIO</h3>
	    	</div>

	    	<div class="block-flat profile-info">
	          
				<div class="row ">
				    <div class="col-sm-6 col-md-6 col-lg-3">
						<div class="widget-block">
							<div class="white-box">
								<div class="fact-data fact-data-pro">
									<i class="fa fa-list-alt fa-5x icolor" ></i>
								</div>
								<div class="fact-data no-padding text-shadow">
									<h3>FACTURAS</h3>
									<h2><?php echo isset($count_facturas)?$count_facturas:'';?></h2>
									
								</div>
							</div>
						</div>
					</div>

					<div class="col-sm-6 col-md-6 col-lg-3">
						<div class="widget-block">
							<div class="white-box">
								<div class="fact-data fact-data-pro">
									<i class="fa fa-stack-overflow fa-5x icolor" ></i>
								</div>
								<div class="fact-data no-padding text-shadow">
									<h3>PLIEGOS</h3>
									<h2><?php echo isset($count_pliegos)?$count_pliegos:'';?></h2>
									
								</div>
							</div>
						</div>
					</div>

					<div class="col-sm-6 col-md-6 col-lg-3">
						<div class="widget-block">
							<div class="white-box">
								<div class="fact-data fact-data-pro">
									<i class="fa  fa-money fa-5x icolor" ></i>
								</div>
								<div class="fact-data no-padding text-shadow">
									<h3>PRECIO UIT</h3>
									<h2>S/. <?php echo isset($uit_value)?number_format($uit_value,2):'';?></h2>
									
								</div>
							</div>
						</div>
					</div>

					<div class="col-sm-6 col-md-6 col-lg-3">
						<div class="widget-block">
							<div class="white-box">
								<div class="fact-data fact-data-pro">
									<i class="fa fa-tasks fa-5x icolor" ></i>
								</div>
								<div class="fact-data no-padding text-shadow">
									<h3>ALICUOTA</h3>
									<h2><?php echo isset($alicuota_value)?$alicuota_value:'';?></h2>
									
								</div>
							</div>
						</div>
					</div>

				</div>
	        </div>


		</div>
		<div class="cl-mcont">
	   		<div class="block-flat profile-info">

				<div class="row">
					<div class=" col-lg-12">

						<section class="main">
							<ul class="ch-grid">
								<li onclick="location.href='<?php echo ENV_WEBROOT_FULL_URL;?>Facturacion/wizard'">
									<div class="ch-item ch-img-1" style="cursor:pointer;">				
										<div class="ch-info-wrap">
											<div class="ch-info">
												<div class="ch-info-front ch-img-1"></div>
												<div class="ch-info-back">
													<h3>WIZARD</h3>
													<p>Importar Elementos</p>
												</div>	
											</div>
										</div>
									</div>
								</li>
								<li onclick="location.href='<?php echo ENV_WEBROOT_FULL_URL;?>Facturacion/pliegos'" >
									<div class="ch-item ch-img-2" style="cursor:pointer;">
										<div class="ch-info-wrap">
											<div class="ch-info">
												<div class="ch-info-front ch-img-2"></div>
												<div class="ch-info-back">
													<h3>PLIEGOS</h3>
													<p>Subir pliegos</p>
												
												</div>
											</div>
										</div>
									</div>
								</li>
								<li onclick="location.href='<?php echo ENV_WEBROOT_FULL_URL;?>Facturacion/list_alumbrado_publico';"  >
									<div class="ch-item ch-img-5" style="cursor:pointer;">				
										<div class="ch-info-wrap">
											<div class="ch-info">
												<div class="ch-info-front ch-img-5"></div>
												<div class="ch-info-back">
													<h3>Alumbrado público</h3>
													<p>Alumbrado Público detalle</p>

												</div>	
											</div>
										</div>
									</div>
								</li>
								<li onclick="location.href='<?php echo ENV_WEBROOT_FULL_URL;?>Facturacion/simulacion'"  >
									<div class="ch-item ch-img-3" style="cursor:pointer;">
										<div class="ch-info-wrap">
											<div class="ch-info">
												<div class="ch-info-front ch-img-3"></div>
												<div class="ch-info-back">
													<h3>SIMULACIÓN</h3>
													<p>ver demo de la app</p>
													
												</div>
											</div>
										</div>
									</div>
								</li>
								<li onclick="location.href='<?php echo ENV_WEBROOT_FULL_URL;?>Facturacion/list_facturas';"  >
									<div class="ch-item ch-img-4" style="cursor:pointer;">
										<div class="ch-info-wrap">
											<div class="ch-info">
												<div class="ch-info-front ch-img-4"></div>
												<div class="ch-info-back">
													<h3>SUMINISTROS</h3>
													<p>Suministros facturados</p>
													
												</div>
											</div>
										</div>
									</div>
								</li>
							</ul>
						</section>

					</div>

				</div>
			</div>

		</div>
    
</div>
    
