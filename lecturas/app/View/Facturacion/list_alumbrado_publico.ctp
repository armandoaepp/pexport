<?php 
if (!isset($NEGOCIO)) {
    header("Location:".ENV_WEBROOT_FULL_URL."/Usuarios/logout");
    die();
}
?>
<?php 
$ver_menu = 'activo';
if (isset($id_usuario) && $id_usuario == 25){
	$permiso_lectura = 'true';
}elseif (isset($id_usuario) && $id_usuario == 64) {
	$permiso_lectura = 'true';
	$ver_menu = 'no_activo';
}else{
	$permiso_lectura = 'true';
}
if($ver_menu =='activo'){
	echo $this->element('menu',array('active'=>'fac_wizard', 'open_fac'=>'true', 'permiso_lectura' =>$permiso_lectura, 'id_usuario ' =>isset($id_usuario)?$id_usuario:null));
}

?>
<div class="container-fluid" id="pcont">

<h2 class="text-center" style="margin-top: 15px;">Alumbrado Publico: <?php echo $periodo;?></h2>

<div class='row'>
	
		<div class='col-sm-12 col-md-12'>
			<div class='block-flat'>
				<div class='content overflow-hidden'>
<table class='red'>
                  	<thead>
                  	<tr>
                  	<th>Id</th>
                  	<th>Estala Group</th>
                  	<th>Escala Min</th>
                  	<th>Escala Max</th>
                  	<th>Alumbrado Publico</th>
                  	</tr>
                  	</thead>
                  	<tbody>
                    <?php 
	                foreach ($arr_aps as $key => $value){
					?>
					<tr>
					<td><?php echo $value[0]['idap'];?></td>
					<td><?php echo $value[0]['estalagroup'];?></td>
					<td><?php echo $value[0]['escalamin'];?></td>
					<td><?php echo $value[0]['escalamax'];?></td>
					<td><?php echo $value[0]['ap'];?></td>
					</tr>
					<?php 
					}
	                ?>
	                </tbody>
	                </table>
</div>
			</div>
		</div>
</div>
</div>