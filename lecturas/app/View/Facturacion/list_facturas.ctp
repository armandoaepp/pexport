<?php 
if (!isset($NEGOCIO)) {
    header("Location:".ENV_WEBROOT_FULL_URL."/Usuarios/logout");
    die();
}
?>
<?php 
$ver_menu = 'activo';
if (isset($id_usuario) && $id_usuario == 25){
	$permiso_lectura = 'true';
}elseif (isset($id_usuario) && $id_usuario == 64) {
	$permiso_lectura = 'true';
	$ver_menu = 'no_activo';
}else{
	$permiso_lectura = 'true';
}
if($ver_menu =='activo'){
	echo $this->element('menu',array('active'=>'fac_list_facturas', 'open_fac'=>'true', 'permiso_lectura' =>$permiso_lectura, 'id_usuario ' =>isset($id_usuario)?$id_usuario:null));
}

?>
<style>
#preview{
	position:absolute;
	border:1px solid #ccc;
	background:#333;
	padding:5px;
	display:none;
	color:#fff;
	}
</style>
<div class="container-fluid" id="pcont">

<h2 class="text-center" style="margin-top: 15px;">Facturas Emitidas</h2>

<div class='row'>
	
		<div class='col-sm-12 col-md-12'>
			<div class='block-flat'>
				<div class='content overflow-hidden'>
<table class='red'>
                  	<thead>
                  	<tr>
                  	<th>Suministro</th>
                  	<th>Cliente</th>
                  	<th>Direcci&oacute;n</th>
                  	<th>Fecha</th>
                  	<th>Lectura</th>
                  	<th>Obs</th>
                  	<th>Consumo</th>
                  	<th>Resultado</th>
                  	<th>Recibo</th>
                  	</tr>
                  	</thead>
                  	<tbody>
                    <?php 
	                foreach ($arr_fac as $key => $value){
					?>
					<tr>
					<td><?php echo $value[0]['suministro'];?></td>
					<td><?php echo $value[0]['cliente'];?></td>
					<td><?php echo $value[0]['direccion'];?></td>
					<td><?php echo $value[0]['fechaejecucion1'];?></td>
					<td><?php echo $value[0]['lecturao1'];?></td>
					<td><?php echo $value[0]['obs1'];?></td>
					<td><?php echo $value[0]['montoconsumo1'];?></td>
					<td><?php echo $value[0]['resultadoevaluacion'];?></td>
					<td>
					<a class="fancybox fancybox.iframe" href="<?= ENV_WEBROOT_FULL_URL?>Facturacion/view_img_factura/<?php echo $value[0]['suministro'];?>"><i class="fa fa-search"></i></a>
					</td>
					</tr>
					<?php 
					}
	                ?>
	                </tbody>
	                </table>
</div>
			</div>
		</div>
</div>
</div>
<script type="text/javascript" src="<?php echo ENV_WEBROOT_FULL_URL ?>js/jquery.imgpreview/jquery.imgpreview.full.js"></script>