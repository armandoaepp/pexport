﻿<?php 
if (!isset($NEGOCIO)) {
    header("Location:".ENV_WEBROOT_FULL_URL."/Usuarios/logout");
    die();
}
?>
<?php 
if ($id_usuario == 25 || $id_usuario == 131){
    $permiso_lectura = 'false';
}else {
    $permiso_lectura = 'true';
    
}
echo $this->element('menu',array('active'=>'fac_pliegos', 'open_fac'=>'true', 'permiso_lectura' =>$permiso_lectura, 'id_usuario ' =>$id_usuario));
?>
<script>
function tooglearbol(){
    $('#arbol').toggle();
}
</script>

<div id="arbol" class="page-aside app filters tree" style="">
    <div class="fixed nano nscroller has-scrollbar">
        <div class="content">
            <div class="header">
                <button class="navbar-toggle" data-target=".app-nav" data-toggle="collapse" type="button">
                    <span class="fa fa-chevron-down"></span>
                </button>
                <h2 class="page-title">Importar Pliego</h2>  
            </div>

            <div class="app-nav collapse">
                <form action="<?= ENV_WEBROOT_FULL_URL?>Facturacion/save_pliego" enctype="multipart/form-data"  method="post"  >
                    <div style="display:none;"><input type="hidden" name="_method" value="POST"></div><br/>
                    <input id="file" type="file" name="file">
                    </br>
                    <input type="submit" class="btn btn-success" value="Grabar">
                </form>
                
                     
                <h2 class="page-title">Filtros</h2>
                <p class="description">Criterios de Busqueda</p>
                
                <div class="form-group" >
                  <label class="control-label">Por :</label>
                  <input id="cbo_criterio_fac_pliego" name="cbo_criterio_fac_pliego" style="width:240px;" placeholder="Seleccione una opcion">
                </div>
                
                <div class="por_fecha_asignacion" style= >
                    <div class="form-group"  >
                      <label class="control-label">Por Fecha de Importaci&oacute;n (Inicio) :</label>
                      <div class="input-group date datetime" style= "width: 80%;" data-min-view="2" data-date-format="yyyy-mm-dd">
                        <input id="dpt_date_start" class="form-control" size="16" type="text" value="<?php echo date('Y-m-d', strtotime("-1 month"));?>" readonly>
                        <span class="input-group-addon btn btn-primary"><span class="glyphicon glyphicon-th"></span></span>
                      </div>
                    </div>

                    <div class="form-group">
                      <label class="control-label">Por Fecha de Importaci&oacute;n (Fin) :</label>
                      <div class="input-group date datetime"  style= "width: 80%;" data-min-view="2" data-date-format="yyyy-mm-dd">
                        <input id="dpt_date_end" class="form-control" size="16" type="text" value="<?php echo date('Y-m-d');?>" readonly>
                        <span class="input-group-addon btn btn-primary"><span class="glyphicon glyphicon-th"></span></span>
                      </div>    
                    </div>
                </div>
                
                <div class="por_ubicacion" style="display:none;" >    

                </div>

                <button class='btn btn-primary' id='buscar-listado-pliegos'>Buscar</button>
            </div>
            
        </div>
    </div>
</div>
    
<div class="container-fluid" id="pcont">
   
    <div class="page-head">
        <ol class="breadcrumb">
          <li><button type="button" class="btn-xs btn-default arbol-top-lecturas" >Arbol</button></li>
          <li><a href="#">Facturaci&oacute;n</a></li>
          <li class="active">Listado de Pliegos</li>
        </ol>
        
        <a class="btn btn-info btn-sm btn fancybox fancybox.iframe" style="float: right;margin-top: -28px;" href="<?= ENV_WEBROOT_FULL_URL?>Facturacion/grafico_pliegos">Gr&aacute;fico</a>
    </div>

    <div class="cl-mcont">
        <div class="col-sm-12 col-md-12"  style='height:100%;' >
            <div class="block">
                <div class="content">
                
	                <?php 
	                $success = $this->Session->read('wizard_status');
	                $msg = $this->Session->read('wizard_msg');
	                unset($_SESSION['wizard_status']);
	                unset($_SESSION['wizard_msg']);
	                if(isset($success)){
		                if($success){
		                	$msg_success = 'Success';
		                	$str_clase = 'alert-success';
		                }else{
		                	$msg_success = 'Error';
		                	$str_clase = 'alert-danger';
		                }
		                ?>
		                
		                <div class="alert <?php echo $str_clase;?> alert-white rounded">
							<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
							<div class="icon"><i class="fa fa-check"></i></div>
							<strong><?php echo $msg_success;?>!</strong> <?php echo $msg;?>
						 </div>
					<?php }?>
				
                    <div class="scroll-table-inconsistecia" style='position: relative; max-width: 300px; min-width: 100%; overflow-y: hidden; height: 500px; overflow-y: auto;'>
                        <div id="contenido_pliegos" name="contenido_pliegos" >
                            

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<script>
$('#contenido_pliegos').html('');

$("#contenido_pliegos").append('<table id="tabla_pliegos" class="table table-bordered tableWithFloatingHeader" style=""><thead><tr><th>Pliego</th><th>Detalle</th></tr></thead></table>');
var oTable = $('#tabla_pliegos').dataTable( {

"sAjaxSource": ENV_WEBROOT_FULL_URL+"Facturacion/ajax_listar_pliegos",
"fnServerData": function ( sSource, aoData, fnCallback ) {
     //     aoData.push( { "name": "ciclo", "value": ciclo } );
      //    aoData.push( { "name": "sector", "value": sector } );
       //   aoData.push( { "name": "ruta", "value": ruta } );
          $.ajax( {
              "dataType": 'json',
              "type": "POST",
              "url": sSource,
              "data": aoData,
              "success": fnCallback
          } );
      }
   });

var preload_data = [
	{ id: 'F', text: 'Fecha de Importación'}
	//, { id: 'S', text: 'Sector'}
    ];
                       
$('#cbo_criterio_fac_pliego').select2({
    multiple: true
    ,query: function (query){
        var data = {results: []};

        $.each(preload_data, function(){
            if(query.term.length == 0 || this.text.toUpperCase().indexOf(query.term.toUpperCase()) >= 0 ){
                data.results.push({id: this.id, text: this.text });
            }
        });

        query.callback(data);
    }
});
$('#cbo_criterio_fac_pliego').select2('data', [{ id: 'F', text: 'Fecha de Importación'}] )

$('#cbo_criterio_fac_pliego').on("change",function (e) { 
	console.log("change "+e.val);
	if(e.val.indexOf("F")>-1){
		$('.por_fecha_asignacion').show();					
	}else{
		$('.por_fecha_asignacion').hide();
	}
	if(e.val.indexOf("S")>-1){
		$('.por_ubicacion').show();
	}else{
		$('.por_ubicacion').hide();
	}
});

$('body').on('click','#buscar-listado-pliegos',function(e){

	criterio = $('#cbo_criterio_fac_pliego').val();
		date_start = $('#dpt_date_start').val();
		date_end = $('#dpt_date_end').val();
		//id_sector = $('#monitoreo_id_sector').val();
	
	if(oTable != null || typeof oTable != 'undefined') {
			$('#tabla_pliegos').dataTable().fnDestroy();
	}


	$('#tabla_pliegos').dataTable({
		"bProcessing": true,
		"bDeferRender": true,
		"bServerSide": false,
		"paging": false,
		"bSort": false,
		"iDisplayLength" : 30,
		"sAjaxSource": ENV_WEBROOT_FULL_URL+"Facturacion/ajax_listar_pliegos/"+criterio+"/"+date_start+"/"+date_end,
		"aLengthMenu": [
						[30,50,100],
						[30,50,100]
						]
	});

});
</script>