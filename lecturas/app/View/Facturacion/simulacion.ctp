<?php 
if (!isset($NEGOCIO)) {
    header("Location:".ENV_WEBROOT_FULL_URL."/Usuarios/logout");
    die();
}
?>
<?php 
$ver_menu = 'activo';
if (isset($id_usuario) && $id_usuario == 25){
	$permiso_lectura = 'true';
}elseif (isset($id_usuario) && $id_usuario == 64) {
	$permiso_lectura = 'true';
	$ver_menu = 'no_activo';
}else{
	$permiso_lectura = 'true';
}
if($ver_menu =='activo'){
	echo $this->element('menu',array('active'=>'rep_historico', 'open_report'=>'true', 'permiso_lectura' =>$permiso_lectura, 'id_usuario ' =>isset($id_usuario)?$id_usuario:null));
}

?>
<style>
/* Aliniear Modal a la Izquierda */
.fancybox-wrap {
	margin-left: -20%;
	opacity: 1 !important;
}
</style>
<div class="container-fluid" id="pcont">

	<div class="page-head" style="padding: 0px;">

		<ol class="breadcrumb">
			<li><button type="button"
					class="btn-xs btn-default arbol-top-lecturas">&Aacute;rbol</button></li>
			<li><a href="#">Lecturas</a></li>
			<li class="active">Facturaci&oacute;n de Consumos</li>
		</ol>
	</div>


	<div class="cl-mcont">
		<div class="col-sm-12 col-md-12">
			<div class="panel-group accordion accordion-color" id="accordion2">
				<div class="panel panel-default">
					<div class="panel-heading">
						<h4 class="panel-title">
							<a data-toggle="collapse" data-parent="#accordion2"
								href="#collapse-1" class=""> <i class="fa fa-angle-right"></i>
								Facturar: Ingresar Datos para Facturar
							</a>
						</h4>
					</div>
					<div id="collapse-1" class="panel-collapse collapse in"
						style="height: auto;">
						<div class="panel-body">
							<form
								action="<?= ENV_WEBROOT_FULL_URL?>facturacion/simulacion"
								id="ReporteHistoricoForm" method="post">
								<div style="display: none;">
									<input type="hidden" name="_method" value="POST">
								</div>

								<div class="form-group">
								<table class="no-border no-strip skills" style="color: black;">
								<tbody class="no-border-x no-border-y">
<tr>
<td>Suministro</td>
<td><input type="text" class="form-control"
										id="txt_suministro_facturacion"
										name="txt_suministro_facturacion"
										placeholder="Suministro"
										value="<?php if(isset($suministro) && $suministro!='0') {echo $suministro;}?>"></td>
<td>Consumo</td>
<td><input type="text" class="form-control"
										id="txt_consumo_facturacion"
										name="txt_consumo_facturacion"
										placeholder="Consumo"
										value="<?php if(isset($consumo)) {echo $consumo;}?>"></td>
</tr>
</tbody>
</table>
										
										
								</div>

								<input type="submit" class="btn btn-success btn-flat"
									value="Facturar">
							</form>
						</div>
					</div>
				</div>
			</div>
<?php if(count($arr_suministros)>0){?>
<div class="row">
				<div class="col-sm-12">
					<div class="tab-container">
						<ul class="nav nav-tabs">
							<li class="active"><a data-toggle="tab" href="#home">Informaci&oacute;n
									General</a></li>
							<li class=""><a data-toggle="tab" href="#settings">Pliego</a></li>
						</ul>
						<div class="tab-content">
							<div id="home" class="tab-pane cont active">
								<div class="content" style="overflow-x: scroll;">
								<div class="col-sm-6">
									<iframe src="<?= ENV_WEBROOT_FULL_URL?>facturacion/factura/<?php if(isset($suministro) && $suministro!='0') {echo $suministro;}?>/<?php if(isset($consumo) && $consumo!='') {echo $consumo;}?>" style="width: 471px;height: 596px;"></iframe>
								</div>
								
								<div class="col-sm-6">
									<div class="pull-right">
										
									</div>
									<strong>Datos Calculados de la Facturaci&oacute;n</strong>
									<hr>
									<table class="no-border no-strip skills">
														<tbody class="no-border-x no-border-y">
															<?php 
															foreach ($arr_suministros as $obj_facturacion){?>
															<tr>
																<th style="width: 70%;"><b><strong><?php echo $obj_facturacion[0]['etiquetaparaimpresion'];?> <?php echo $obj_facturacion[0]['concatenacionvar'];?></strong></b></th>
																<td><?php echo $obj_facturacion[0]['monto'];?></td>
															</tr>
															<?php } ?>
															</tbody>
													</table>
									
								</div>
								
								</div>
							</div>

							<div id="settings" class="tab-pane">
								<div class="content">
								<iframe src="<?= ENV_WEBROOT_FULL_URL?>Facturacion/ajax_pliegos" style="width: 1300px;height: 596px;"></iframe>
									
								</div>

							</div>
						</div>
					</div>
				</div>

			</div>
						
<?php }else{ ?>
			
	<?php
	$success = $this->Session->read('wizard_status');
    $msg = $this->Session->read('wizard_msg');
    unset($_SESSION['wizard_status']);
    unset($_SESSION['wizard_msg']);
    if(isset($success)){
			if($success){
				$msg_success = 'Success';
	        	$str_clase = 'alert-success';
	        }else{
	            $msg_success = 'Error';
	            $str_clase = 'alert-danger';
			}
	        ?>
                
            <div class="alert <?php echo $str_clase;?> alert-white rounded">
				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
				<div class="icon"><i class="fa fa-check"></i></div>
				<strong><?php echo $msg_success;?>!</strong> <?php echo $msg;?>
			</div>
	<?php }else{?>
	
	<div class="alert alert-info alert-white rounded">
		<button type="button" class="close" data-dismiss="alert"
			aria-hidden="true">x</button>
		<div class="icon">
			<i class="fa fa-info-circle"></i>
		</div>
		<strong>Informaci&oacute;n!</strong> No se encuentran datos
		registrados para el suministro.
	</div>
	<?php }?>
<?php 	
}?>							 
		
		</div>

	</div>
</div>

<script
	src="<?php echo ENV_WEBROOT_FULL_URL ?>js/fancyapps/source/jquery.fancybox.js"
	type="text/javascript"></script>
<script>
$(document).ready(function() {
	$('.fancybox-reporte-historico').fancybox({
	    helpers:  {
	        overlay : null
	    }
	});	
});
</script>