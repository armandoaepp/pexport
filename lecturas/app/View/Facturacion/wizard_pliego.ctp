<?php 
if (!isset($NEGOCIO)) {
    header("Location:".ENV_WEBROOT_FULL_URL."/Usuarios/logout");
    die();
}
?>
<?php 
$ver_menu = 'activo';
if (isset($id_usuario) && $id_usuario == 25){
	$permiso_lectura = 'true';
}elseif (isset($id_usuario) && $id_usuario == 64) {
	$permiso_lectura = 'true';
	$ver_menu = 'no_activo';
}else{
	$permiso_lectura = 'true';
}
if($ver_menu =='activo'){
	echo $this->element('menu',array('active'=>'fac_wizard', 'open_fac'=>'true', 'permiso_lectura' =>$permiso_lectura, 'id_usuario ' =>isset($id_usuario)?$id_usuario:null));
}

?>
<div class="page-head">
      <h2>Wizard Facturaci&oacute;n</h2>
      <ol class="breadcrumb">
        <li><a href="#">Home</a></li>
        <li><a href="#">Facturaci&oacute;n</a></li>
        <li class="active">Wizard</li>
      </ol>
    </div>
    <div class="cl-mcont">		
    <div class="row wizard-row">
      <div class="col-md-12 fuelux">
        <div class="block-wizard">
          <div id="wizard1" class="wizard wizard-ux">
            <?php echo $this->element('wizard_facturacion',array('active'=>'importar_pliego'));?>
            
          </div>
          <div class="step-content">
            <form class="form-horizontal group-border-dashed" action="<?= ENV_WEBROOT_FULL_URL?>Facturacion/wizard_confirm_pliego" enctype="multipart/form-data"  method="post"> 
              <div class="step-pane active" id="step1">
                <div class="form-group no-padding">
                  <div class="col-sm-7">
                    <h3 class="hthin">Importar Pliego</h3>
                  </div>
                </div>
                
                <?php
                $success = $this->Session->read('wizard_status');
                $msg = $this->Session->read('wizard_msg');
                unset($_SESSION['wizard_status']);
                unset($_SESSION['wizard_msg']);
                if(isset($success)){
	                if($success){
	                	$msg_success = 'Success';
	                	$str_clase = 'alert-success';
	                }else{
	                	$msg_success = 'Error';
	                	$str_clase = 'alert-danger';
	                }
	                ?>
	                
	                <div class="alert <?php echo $str_clase;?> alert-white rounded">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
						<div class="icon"><i class="fa fa-check"></i></div>
						<strong><?php echo $msg_success;?>!</strong> <?php echo $msg;?>
					 </div>
				<?php }?>
					 
                <div class="form-group">
                  <label class="col-sm-3 control-label">Subir Archivo Excel</label>
                  <div class="col-sm-6">
                    <input name="file" type="file" class="form-control" placeholder="Selecciona Archivo Excel">
                  </div>
                </div>
                <div class="form-group">
                  <div class="col-sm-offset-2 col-sm-10">
                    <button class="btn btn-primary" type="submit" onClick="javascript: $(this).attr('disabled','disabled');$('.form-horizontal').submit();">Continuar <i class="fa fa-caret-right"></i></button>
                  </div>
                </div>
              </div>
              <div class="step-pane" id="step2">
                <div class="form-group no-padding">
                  <div class="col-sm-7">
                    <h3 class="hthin">Notifications</h3>
                  </div>
                </div>
                
                
                	
                <div class="form-group">
                  <div class="col-sm-12">
                    <button data-wizard="#wizard1" class="btn btn-default wizard-previous"><i class="fa fa-caret-left"></i> Previous</button>
                    <button data-wizard="#wizard1" class="btn btn-primary wizard-next">Next Step <i class="fa fa-caret-right"></i></button>
                  </div>
                </div>	
              </div>
              <div class="step-pane" id="step3">
                <div class="form-group no-padding">
                  <div class="col-sm-7">
                    <h3 class="hthin">Configuration</h3>
                  </div>
                </div>
                
                
                
                <div class="form-group">
                  <div class="col-sm-12">
                    <button data-wizard="#wizard1" class="btn btn-default wizard-previous"><i class="fa fa-caret-left"></i> Previous</button>
                    <button data-wizard="#wizard1" class="btn btn-success wizard-next"><i class="fa fa-check"></i> Complete</button>
                  </div>
                </div>	
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
</div>
