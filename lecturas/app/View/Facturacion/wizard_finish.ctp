<?php 
if (!isset($NEGOCIO)) {
    header("Location:".ENV_WEBROOT_FULL_URL."/Usuarios/logout");
    die();
}
?>
<?php 
$ver_menu = 'activo';
if (isset($id_usuario) && $id_usuario == 25){
	$permiso_lectura = 'true';
}elseif (isset($id_usuario) && $id_usuario == 64) {
	$permiso_lectura = 'true';
	$ver_menu = 'no_activo';
}else{
	$permiso_lectura = 'true';
}
if($ver_menu =='activo'){
	echo $this->element('menu',array('active'=>'fac_wizard', 'open_fac'=>'true', 'permiso_lectura' =>$permiso_lectura, 'id_usuario ' =>isset($id_usuario)?$id_usuario:null));
}

?>
<div class="page-head">
      <h2>Wizard Facturaci&oacute;n</h2>
      <ol class="breadcrumb">
        <li><a href="#">Home</a></li>
        <li><a href="#">Facturaci&oacute;n</a></li>
        <li class="active">Wizard</li>
      </ol>
    </div>
    <div class="cl-mcont">		
    <div class="row wizard-row">
      <div class="col-md-12 fuelux">
        <div class="block-wizard">
          <div id="wizard1" class="wizard wizard-ux">
            <?php echo $this->element('wizard_facturacion',array('active'=>'finish'));?>

          </div>
          <div class="step-content">
            <form class="form-horizontal group-border-dashed" action="<?= ENV_WEBROOT_FULL_URL?>Facturacion/wizard_save_finish" enctype="multipart/form-data"  method="post"> 
              <div class="step-pane active" id="step1">
                <div class="form-group no-padding">
                  <div class="col-sm-7">
                    <h3 class="hthin">Fin del Proceso.</h3>
                    <h4 class="hthin">A continuaci&oacute;n un resumen de los datos importados.</h4>
                  </div>
                </div>

                <?php 
                $success = $this->Session->read('wizard_status');
                $msg = $this->Session->read('wizard_msg');
                unset($_SESSION['wizard_status']);
                unset($_SESSION['wizard_msg']);
                if(isset($success)){
	                if($success){
	                	$msg_success = 'Success';
	                	$str_clase = 'alert-success';
	                }else{
	                	$msg_success = 'Error';
	                	$str_clase = 'alert-danger';
	                }
	                ?>
	                
	                <div class="alert <?php echo $str_clase;?> alert-white rounded">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
						<div class="icon"><i class="fa fa-check"></i></div>
						<strong><?php echo $msg_success;?>!</strong> <?php echo $msg;?>
					 </div>
				<?php }?>
				
				<br>
				<div class="dash-cols">
				<div class="col-sm-6 col-md-6 col-lg-4">
					<div class="block">
						<div class="header">
							<h2><b><i class="fa fa-tasks"></i>Pliegos</b></h2>
						</div>
						<div class="content no-padding">
							<div class="fact-data text-center">
								<h3>Registros</h3>
								<h2><?php echo $count_pliegos;?></h2>
							</div>
							<div class="fact-data text-center">
								<h3>Desde <b><?php echo $min_date_pliegos;?></b></h3>
								<h3>Hasta <b><?php echo $max_date_pliegos;?></b></h3>
							</div>
						</div>
					</div>
				</div>	
				<div class="col-sm-6 col-md-6 col-lg-4">
					<div class="block">
						<div class="header">
							<h2><b><i class="fa fa-tasks"></i>Pliegos CRM</b></h2>
						</div>
						<div class="content no-padding">
							<div class="fact-data text-center">
								<h3>Registros</h3>
								<h2><?php echo $count_pliegos_crm;?></h2>
							</div>
							<div class="fact-data text-center">
								<h3>Desde <b><?php echo $min_date_pliegos_crm;?></b></h3>
								<h3>Hasta <b><?php echo $max_date_pliegos_crm;?></b></h3>
							</div>
						</div>
					</div>
				</div>
				<div class="col-sm-4 col-md-4 col-lg-4">
					<div class="block">
						<div class="header">
							<h2><b><i class="fa fa-calendar"></i>Cronograma</b></h2>
						</div>
						<div class="content no-padding">
							<div class="fact-data text-center">
								<h3>Registros</h3>
								<h2><?php echo $count_cronograma;?></h2>
							</div>
							<div class="fact-data text-center">
								<h3>Desde <b><?php echo $min_date_cronograma;?></b></h3>
								<h3>Hasta <b><?php echo $max_date_cronograma;?></b></h3>
							</div>
						</div>
					</div>
				</div>
				<div class="col-sm-4 col-md-4 col-lg-4">
					<div class="block">
						<div class="header">
							<h2><b><i class="fa fa-flag"></i>Constantes</b></h2>
						</div>
						<div class="content no-padding">
							<div class="fact-data text-center">
								<h3>UIT</h3>
								<h2><?php echo isset($uit_value)?$uit_value:'';?></h2>
							</div>
							<div class="fact-data text-center">
								<h3>Alicuota</h3>							
								<h2><?php echo isset($alicuota_value)?$alicuota_value:'';?></h2>
							</div>
						</div>
					</div>
				</div>
									
			</div>
                
                <div class="form-group">
                  <div class="col-sm-12">
                    <a class="btn btn-default" href="<?= ENV_WEBROOT_FULL_URL?>">Ir a Home</a>
                  </div>
                </div>
              </div>
              <div class="step-pane" id="step2">
                <div class="form-group no-padding">
                  <div class="col-sm-7">
                    <h3 class="hthin">Notifications</h3>
                  </div>
                </div>
                
                <div class="form-group">
                  <div class="col-sm-12">
                    <button data-wizard="#wizard1" class="btn btn-default wizard-previous"><i class="fa fa-caret-left"></i> Previous</button>
                    <button data-wizard="#wizard1" class="btn btn-primary wizard-next">Next Step <i class="fa fa-caret-right"></i></button>
                  </div>
                </div>	
              </div>
              <div class="step-pane" id="step3">
                <div class="form-group no-padding">
                  <div class="col-sm-7">
                    <h3 class="hthin">Configuration</h3>
                  </div>
                </div>
                
                <div class="form-group">
                  <div class="col-sm-12">
                    <button data-wizard="#wizard1" class="btn btn-default wizard-previous"><i class="fa fa-caret-left"></i> Previous</button>
                    <button data-wizard="#wizard1" class="btn btn-success wizard-next"><i class="fa fa-check"></i> Complete</button>
                  </div>
                </div>	
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
</div>
