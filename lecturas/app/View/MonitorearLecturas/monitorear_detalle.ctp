<?php 
if (!isset($NEGOCIO)) {
    header("Location:".ENV_WEBROOT_FULL_URL."/Usuarios/logout");
    die();
}
?>
<?php  echo $this->element('menu',array('active'=>'monitorearlectura', 'open'=>'true'));?>

		<div id="arbol" class="page-aside app filters">
      	<div>
        <div class="content">
          <button class="navbar-toggle" data-target=".app-nav" data-toggle="collapse" type="button">
            <span class="fa fa-chevron-down"></span>
          </button>          
          <h2 class="page-title">Filtros</h2>
          <p class="description">Criterios de Busqueda</p>
          
        </div>        
        <div class="app-nav collapse">
          <div class="content">
          
           	<div class="form-group">
              <label class="control-label">Tipo </label>
              <select class="select2" id = 'tipo_acciones' name = 'tipo_acciones'>
                   <option value="ALL">Por Todas</option>
                   <option value="LECTURAS">Por Lecturas</option>
                   <option value="RELECTURAS">Por ReLecturas</option>
              </select>
            </div>
            
             <div class="form-group">
              <label class="control-label">Seleccione</label>
              <select class="select2" id = 'tipo_acciones' name = 'tipo_acciones'>
                   <option value="FINALIZADAS">Por Finalizadas</option>
                   <option value="INCONSISTENTE">Por Inconsistentes</option>
                   <option value="CONSISTENTE">Por Consistentes</option>
                   <option value="RESTANTE">Por Restantes</option>
              </select>
            </div>
			
			<div class="form-group">
              <label class="control-label">Alternativo: Agrupar por</label>
              <select class="select2" id= 'groupby' name ='groupby'>
                   <option value="ALL">Todos</option>
                   <?php 
	                if($NEGOCIO == 'CIX'){
						echo '<option value="sector">Agrupados Por Sector</option>';
					}elseif ($NEGOCIO == 'TAC'){
						echo '<option value="sector">Agrupados Por Zona</option>';
					}else{
						echo '<option value="sector">Agrupados Por Sector</option>';
					}?>
                   <option value="ruta">Agrupados Por <?php if($NEGOCIO == 'CIX'){echo 'Ruta';}elseif ($NEGOCIO == 'TAC'){echo 'Libro';}else{echo 'Ruta';}?></option>
              </select>
            </div>
            
          </div>
        </div>
      </div>
</div>		

<div class="container-fluid" id="pcont">

		<div class="page-head">
				
				<ol class="breadcrumb">
				  <li><button type="button" class="btn-xs btn-default arbol-top-lecturas" >Arbol</button></li>
				  <li><a href="#">Ordenes de Lectura</a></li>
				  <li><a href="#">Monitorear</a></li>
				  <li class="active">Monitorear / Detalle Lecturas</li>
				</ol>
		</div>
	<div class="cl-mcont">
		<div class="col-sm-12 col-md-12" >
			<div class="block">
				


				<div class="col-sm-12 col-md-12">
					<div class="block">
						<div class="header no-border">
							<h2>Monitorear Lecturas (Lecturistas Detalle)</h2>
						</div>

						<div class="content no-padding">
							<table class="red">
								<thead>
									<tr>
										<th>Nombre</th>
										<th class="right"><span></span>Total Lecturas</th>
										<th class="right">Lecturas Finalizadas</th>
										<th class="right">Inconsistencias</th>
										<th class="right">Consistentes</th>
										<th class="right">Lecturas Restantes</th>
										<th class="right"><span></span>Ver Mapa</th>
									</tr>
								</thead>
								<tbody class="no-border-x">
								<?php foreach ($listar_empleados as $empleados){
								?>
									<tr>
										<td style="width:30%;" empleado_id = <?php echo $empleados['0']["lecturista_id"];?>><a href= "<?php echo ENV_WEBROOT_FULL_URL ?>MonitorearLecturas/monitorear_detalle/<?php echo $empleados['0']["lecturista_id"];?>" ><i class="fa fa-user"></i> <?php echo  $empleados['0']['lecturista']; ?></a></td>
										<td style="width:5%;"><i class=""></i><?php echo $empleados['0']["lecturas_asignadas"];?></td>
										<td style="width:40%;">
											<div class="progress progress-striped">
											  <div class="progress-bar progress-bar-success" style="width: <?php echo ($empleados['0']['lecturas_terminadas'] * 100) / $empleados['0']['lecturas_asignadas'].'% '?>"><?php echo ($empleados['0']['lecturas_terminadas'] * 100) / $empleados['0']['lecturas_asignadas'].'% '?>(<?php echo $empleados['0']['lecturas_terminadas'];?>)
											  </div> o <?php echo $empleados['0']['lecturas_terminadas'];?> Lecturas.
											</div>
										</td>
										<td style="width:40%;">
											<?php echo $empleados['0']['lecturas_inconsistentes'];?>
										</td>
										<td style="width:40%;">
											<?php echo $empleados['0']['lecturas_consistentes'];?>
										</td>
										<td class="text-right"><?php echo $empleados['0']["lecturas_restantes"];?></td>
										<td class="text-right"><a class="btn btn-info btn-sm btn fancybox fancybox.iframe" href="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d3961.9708934684763!2d-79.844905!3d-6.773396999999984!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x2ea9693065734f32!2sBanco+de+la+Naci%C3%B3n!5e0!3m2!1ses-419!2s!4v1400704156182"><i class="fa fa-globe"></i></td>
									</tr>
								<?php } ?>		
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
			</div>
         </div>
    </div>
    
</div>
<script type="text/javascript">														
	require(['ajax/monitorear'],function(){});
</script>