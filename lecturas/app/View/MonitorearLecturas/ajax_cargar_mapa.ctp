<?php 
if (!isset($NEGOCIO)) {
    header("Location:".ENV_WEBROOT_FULL_URL."/Usuarios/logout");
    die();
}
?>
<?php
$coordenadas = '';
$str_rutas = '';
$i = 1;
$total_rutas = 0;
$lecturista = '';
$fechaejecucion_tmp = '';

$latitud_center = '';
$longitud_center = '';

if($NEGOCIO == 'CIX'){
	$lbl_suministro = 'Suministro';
}elseif ($NEGOCIO == 'TAC'){
	$lbl_suministro = 'Contrato';
}else{
	$lbl_suministro = 'Suministro';
}


foreach ($arr_obj_coordenadas as $key => $value) {	
	if(isset($value['ComlecOrdenlectura']['latitud1']) && isset($value['ComlecOrdenlectura']['longitud1']) &&
			$value['ComlecOrdenlectura']['latitud1']!='' && $value['ComlecOrdenlectura']['longitud1']!='' && 
			$value['ComlecOrdenlectura']['latitud1']!='0' && $value['ComlecOrdenlectura']['longitud1']!='0'){
		if($i==1 || $i==count($arr_obj_coordenadas)){
			$color_flag = 'EF9D3F';	
		}else{
			$color_flag = '72BAF7';
		}		
		
		if($i>1){
			$latitud_center = $value['ComlecOrdenlectura']['latitud1'];
			$longitud_center = $value['ComlecOrdenlectura']['longitud1'];
		}
		
		$fechaejecucion = strtotime($value['ComlecOrdenlectura']['fechaejecucion1']);
		if($i>1){
			$tiempo = $fechaejecucion - $fechaejecucion_tmp;
		}else{
			$tiempo = '0';
		}
		$fechaejecucion_tmp = strtotime($value['ComlecOrdenlectura']['fechaejecucion1']);

		$horas              = floor ( $tiempo / 3600 );
		$minutes            = ( ( $tiempo / 60 ) % 60 );
		$seconds            = ( $tiempo % 60 );
		 
		$time['horas']      = str_pad( $horas, 2, "0", STR_PAD_LEFT );
		$time['minutes']    = str_pad( $minutes, 2, "0", STR_PAD_LEFT );
		$time['seconds']    = str_pad( $seconds, 2, "0", STR_PAD_LEFT );
		 
		$str_tiempo               = implode( ':', $time );

		$coordenadas .= ' /'.$value['ComlecOrdenlectura']['latitud1'].','.$value['ComlecOrdenlectura']['longitud1'].'';
		$str_rutas .="
		path.push(new google.maps.LatLng(".$value['ComlecOrdenlectura']['latitud1'].", ".$value['ComlecOrdenlectura']['longitud1']."));
		  var contentString = '<div id=\"content\"><div id=\"siteNotice\"></div><h1 id=\"firstHeading\" class=\"firstHeading\">".$lbl_suministro.": ".$value['ComlecOrdenlectura']['suministro']."</h1><div id=\"bodyContent\"><p>' +
		  		'Fecha de Ejecucion: <strong>".$value['ComlecOrdenlectura']['fechaejecucion1']."</strong><br>'+
		  		'Tiempo transcurrido desde el punto anterior: <strong>".$str_tiempo."</strong>'+
		  		'</p></div></div>';
		  var infowindow".($i+$page)." = new google.maps.InfoWindow({
		      content: contentString
		  });
		  var marker".($i+$page)." = new google.maps.Marker({
		    position: new google.maps.LatLng(".$value['ComlecOrdenlectura']['latitud1'].", ".$value['ComlecOrdenlectura']['longitud1']."),
		    title: '#' + path.getLength(),
		    map: map,
		    icon: 'https://chart.googleapis.com/chart?chst=d_map_pin_letter&chld=".($i+$page)."|".$color_flag."|000000',
		    size: 'tiny'
		  });
		  google.maps.event.addListener(marker".($i+$page).", 'click', function() {
			  infowindow".($i+$page).".open(map,marker".($i+$page).");
			});  		
		  ";
		$i++;
		
		$total_rutas = $value[0]['count'];
		$lecturista = $value['GE']['nombre'];
	}
}
?>
<!DOCTYPE html>
<html>
  <head>
    <meta name="viewport" content="initial-scale=1.0, user-scalable=no">
    <meta charset="utf-8">
    <title>Complex Polylines</title>
    <style>
      html, body, #map-canvas {
        height: 100%;
        margin: 0px;
        padding: 0px
      }
    </style>
    <script src="https://maps.googleapis.com/maps/api/js?v=3.exp"></script>
    <script>
// This example creates an interactive map which constructs a
// polyline based on user clicks. Note that the polyline only appears
// once its path property contains two LatLng coordinates.

var poly;
var map;
<?php if(isset($arr_obj_coordenadas) && count($arr_obj_coordenadas)>0){?>
function initialize() {
  var mapOptions = {
    zoom: 16,
    // Center the map on Chiclayo.
    center: new google.maps.LatLng(<?php echo isset($latitud_center)?$latitud_center:'-6.77361';?>, <?php echo isset($longitud_center)?$longitud_center:'-79.8417';?>)
  };

  map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);

  var iconsetngs = {
  	path: google.maps.SymbolPath.FORWARD_CLOSED_ARROW
  };

  var polyOptions = {
    strokeColor: '#000000',
    strokeOpacity: 1.0,
    strokeWeight: 2,
    icons: [{
		repeat: '70px',
		icon: iconsetngs,
		offset: '100%'}]
  };
  poly = new google.maps.Polyline(polyOptions);
  poly.setMap(map);

  var path = poly.getPath();

  /*
  // Because path is an MVCArray, we can simply append a new coordinate
  // and it will automatically appear.
  path.push(new google.maps.LatLng(42.879535, -90.624333));

  // Add a new marker at the new plotted point on the polyline.
  var marker = new google.maps.Marker({
    position: new google.maps.LatLng(42.879535, -90.624333),
    title: '#' + path.getLength(),
    map: map
  });
	*/

  	<?php echo $str_rutas;?>
  // Add a listener for the click event
  //google.maps.event.addListener(map, 'click', addLatLng);
}

google.maps.event.addDomListener(window, 'load', initialize);
<?php }?>

$('body').on('click','#generar-mapa',function(e){
  criterio = $('#cbo_criterio_mapa').val();
  date_start = $('#dpt_date_start').val();
  date_end = $('#dpt_date_end').val();
  rango = $('#rango_cantidades').val();
  unidadneg = $('#unidadneg').val();
  id_ciclo = $('#id_ciclo').val();
  id_sector = $('#monitoreo_id_sector').val();
  window.open("<?php echo ENV_WEBROOT_FULL_URL;?>MonitorearLecturas/ajax_cargar_mapa/<?php echo $id_empleado;?>/"+criterio+"/"+date_start+"/"+date_end+"/"+unidadneg+"/"+id_ciclo+"/"+id_sector+"/"+rango,"_top");
});

    </script>
  </head>
  <body>
  

    <div class="container-fluid" id="pcont" style="height: 100%;margin: 0px;padding: 0px;">
		<div id="map-canvas"></div>
	</div>

  <div id="arbol" class="page-aside app filters tree">
        <div class="fixed nano nscroller has-scrollbar">
        
        	<div class="content">

        		<div class="header">
	               <button class="navbar-toggle" data-target=".app-nav" data-toggle="collapse" type="button">
	                  <span class="fa fa-chevron-down"></span>
	               </button>          
	               <h2 class="page-title">Filtros</h2>
               	   <p class="description">Criterios de Busqueda</p>

          		</div>

		        <div class="app-nav collapse">
		           
		            <div class=" col-lg-11">

			          	<div class="form-group">
			              <label class="control-label">Lecturista:</label>
			              <br>
			              <span><strong><?php echo $lecturista;?></strong></span>  
			            </div>
			            
			            <div class="form-group">
			              <label class="control-label">Por Rango de Lecturas (Cantidad) :</label>

			              <div>
			              	
			              <input id="rango_cantidades" type="text" class="bslider form-control" value="" data-slider-min="0" data-slider-max="<?php echo $total_rutas;?>" data-slider-step="5" data-slider-value="[<?php echo isset($rango)?$rango:'0.25';?>]" style="width: 200px;">
			              
			              </div>
			            </div>
			            
			            <?php 
			            if(strpos($criterio, 'F') !== false){
							$show_tab2 = 'display: block;';
						}else{
							$show_tab2 = 'display:none;';
						}
						if(strpos($criterio, 'U') !== false){
							$show_tab1 = 'display: block;';
						}else{
							$show_tab1 = 'display:none;';
						}
			            ?>
			            
			            <div class="form-group">
			              <label class="control-label">Por :</label>
			              <input id="cbo_criterio_mapa" name="cbo_criterio_mapa" style="width:200px;" placeholder="Seleccione una opcion">
			            </div>

			            <div class="por_ubicacion" style="<?php echo $show_tab1;?>">
			                
			                <div class="form-group">
				              <label class="control-label">Por Unidad Negocio:</label>              
				              	<?php 
				        			  	echo '<select class="" name="unidadneg" id="unidadneg">
				        			  	<option value="0">Todos</option>';
			                                             //   var_dump($listar_unidadneg);exit;
				        				foreach ($listar_unidadneg as $v){
											if($unidadneg==$v['0']['unidadneg']){
												echo '<option value='.$v['0']['unidadneg'].' selected>'.$v['0']['nombreunidadnegocio'].'</option>';
											}else{
												echo '<option value='.$v['0']['unidadneg'].'>'.$v['0']['nombreunidadnegocio'].'</option>';
											}
				        				}
				        				echo '</select>';	
				        		?>        			
				            </div>
			                
				          	<div class="form-group">
				              <label class="control-label">Por Ciclo:<span class="ciclo_loading" style="display:none;"><img src="<?php echo ENV_WEBROOT_FULL_URL ?>/js/jquery.select2/select2-spinner.gif"></span></label>              
				              	<?php 
				        			  	echo '<select class="" name="id_ciclo" id="id_ciclo">
				        			  	<option value="0">Todos</option>';
				        				foreach ($listar_ciclo as $v){
											if($ciclo==$v['0']['idciclo']){
												echo '<option value='.$v['0']['idciclo'].' selected>'.$v['0']['idciclo'].'</option>';
											}else{
												echo '<option value='.$v['0']['idciclo'].'>'.$v['0']['idciclo'].'</option>';	
											}
				        				}
				        				echo '</select>';	
				        		?>        			
				            </div>

				          	<div class="form-group">
				              <label class="control-label">Por <?php if($NEGOCIO == 'CIX'){echo 'Sector';}elseif ($NEGOCIO == 'TAC'){echo 'Zona';}else{echo 'Sector';}?>: <span class="sector_loading" style="display:none;"><img src="<?php echo ENV_WEBROOT_FULL_URL ?>/js/jquery.select2/select2-spinner.gif"></span></label>              
				              	<?php 
				        			  	echo '<select class="" name="monitoreo_id_sector" id="monitoreo_id_sector">
				        			  	<option value="0">Todos</option>';
				        				foreach ($listar_sector as $v){
											if($sector==$v['0']['sector']){
												echo '<option value='.$v['0']['sector'].' selected>'.$v['0']['sector'].'</option>';
											}else{
												echo '<option value='.$v['0']['sector'].'>'.$v['0']['sector'].'</option>';
											}
				        				}
				        				echo '</select>';	
				        		?>        			
				            </div>
			            </div>
			            
			            <div class="por_fecha_asignacion" style="<?php echo $show_tab2;?>">
				            <div class="form-group">
				              <label class="control-label">Por Fecha de Asignaci&oacute;n (Inicio) :</label>
				              <div class="input-group date datetime" data-min-view="2" data-date-format="yyyy-mm-dd">
				                <input id="dpt_date_start" class="form-control" size="16" type="text" value="<?php echo $date_start;?>" readonly>
				                <span class="input-group-addon btn btn-primary"><span class="glyphicon glyphicon-th"></span></span>
				              </div>	
				            </div>

				            <div class="form-group">
				              <label class="control-label">Por Fecha de Asignaci&oacute;n (Fin) :</label>
				              <div class="input-group date datetime" data-min-view="2" data-date-format="yyyy-mm-dd">
				                <input id="dpt_date_end" class="form-control" size="16" type="text" value="<?php echo $date_end;?>" readonly>
				                <span class="input-group-addon btn btn-primary"><span class="glyphicon glyphicon-th"></span></span>
				              </div>	
				            </div>
				        </div>

			   
			            <button class='btn btn-primary' id='generar-mapa'>Generar Mapa</button>
			            <div class="col-lg-12">
			            	<br/>
			            	<br/>
			            	<br/>
			            	<br/>
			            	<br/>
			            	<br/>
			            </div>
	         		</div>
		        </div>
      		</div>
        </div>
    </div>


  </body>
</html>
<script type="text/javascript">
$('#rango_cantidades').slider();

var preload_data = [
    	            { id: 'F', text: 'Fecha de Asignación'}
    	            , { id: 'U', text: 'Ubicación Geográfica'}
    	          ];
               
                $('#cbo_criterio_mapa').select2({
                    multiple: true
                    ,query: function (query){
                        var data = {results: []};
               
                        $.each(preload_data, function(){
                            if(query.term.length == 0 || this.text.toUpperCase().indexOf(query.term.toUpperCase()) >= 0 ){
                                data.results.push({id: this.id, text: this.text });
                            }
                        });
               
                        query.callback(data);
                    }
                });

                $('#cbo_criterio_mapa').select2('data', [<?php if(strpos($criterio, 'F') !== false){ echo "{id: 'F', text: 'Fecha de Asignación'},";}if(strpos($criterio, 'U') !== false){echo "{id: 'U', text: 'Ubicación Geográfica'}";}?>] );
                
                $('#cbo_criterio_mapa').on("change",function (e) { 
    				console.log("change "+e.val);
    				if(e.val.indexOf("F")>-1){
    					$('.por_fecha_asignacion').show();					
    				}else{
    					$('.por_fecha_asignacion').hide();
    				}
    				if(e.val.indexOf("U")>-1){
    					$('.por_ubicacion').show();
    				}else{
    					$('.por_ubicacion').hide();
    				}
    			});

    $('#unidadneg').select2({
	width: '100%'
	});
$('#unidadneg').on("change",function (e) { 
	console.log("change "+e.val);
	$('.ciclo_loading').show();
	$('#id_ciclo').select2("enable", false);
	unidad_neg = e.val || 0;
	$.ajax({
                            url : ENV_WEBROOT_FULL_URL+"/ComlecOrdenlecturas/listarCicloPorUnidadneg/"+unidad_neg,
		type : "POST",
		dataType : 'html',
		success : function(data) {
			console.log(data);
			$('#id_ciclo').html(data);
			$("#id_ciclo").select2("val", "0");
			$('#id_ciclo').select2("enable", true);
			$('.ciclo_loading').hide();
		}
	});
});  
    $('#id_ciclo').select2({
	width: '100%'
	});
$('#id_ciclo').on("change",function (e) { 
	console.log("change "+e.val);
	$('.sector_loading').show();
	$('#monitoreo_id_sector').select2("enable", false);
	id_ciclo = e.val || 0;
	$.ajax({
		url : ENV_WEBROOT_FULL_URL+"/ComlecOrdenlecturas/listarSectorPorCiclo/"+id_ciclo,
		type : "POST",
		dataType : 'html',
		success : function(data) {
			console.log(data);
			$('#monitoreo_id_sector').html(data);
			$("#monitoreo_id_sector").select2("val", "0");
			$('#monitoreo_id_sector').select2("enable", true);
			$('.sector_loading').hide();
		}
	});
});
            
                       
            
$('#monitoreo_id_sector').select2({
	width: '100%'
});
</script>