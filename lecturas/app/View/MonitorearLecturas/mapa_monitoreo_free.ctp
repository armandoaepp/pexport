<?php 
if (!isset($NEGOCIO)) {
    header("Location:".ENV_WEBROOT_FULL_URL."/Usuarios/logout");
    die();
}
?>
<?php
if($NEGOCIO == 'CIX'){
	$lbl_suministro = 'Suministro';
}elseif ($NEGOCIO == 'TAC'){
	$lbl_suministro = 'Contrato';
}else{
	$lbl_suministro = 'Suministro';
}

$str_rutas = '';
$i = 1;
$page = 0; 
foreach ($arr_lecturistas_activos as $k =>$obj_lecturista){
  	if($obj_lecturista['0']["lecturas_descargadas"]>0 && $obj_lecturista['0']["lecturas_terminadas"]>0){
  		
	//$ultima_lectura = $ObjComlecOrdenlectura->getUltimaOrdenPorLecturista($obj_lecturista['0']['id']);
	foreach ($arr_ultima_lectura as $obj_ultima_lectura){
		if($obj_lecturista['0']['id']==$obj_ultima_lectura['0']['lecturista1_id']){
			$ultima_lectura = $obj_ultima_lectura;
			break;
		}
	}
	
	if($ultima_lectura['0']['latitud1']!='' && $ultima_lectura['0']['longitud1']!='' && $ultima_lectura['0']['latitud1']!='0' && $ultima_lectura['0']['longitud1']!='0'){
		
		$arr_lecturistas_activos[$k]['0']['mapa'] = 'OK';
		
	$color_flag = '72BAF7';
	

	$latitud_center = $ultima_lectura['0']['latitud1'];
	$longitud_center = $ultima_lectura['0']['longitud1'];
	
	$fechaejecucion = strtotime(date('Y-m-d H:i:s'));
	$fechaejecucion_tmp = strtotime($ultima_lectura['0']['fechaejecucion1']);
	$tiempo = $fechaejecucion - $fechaejecucion_tmp;

	$horas              = floor ( $tiempo / 3600 );
	$minutes            = ( ( $tiempo / 60 ) % 60 );
	$seconds            = ( $tiempo % 60 );
	 
	$time['horas']      = str_pad( $horas, 2, "0", STR_PAD_LEFT );
	$time['minutes']    = str_pad( $minutes, 2, "0", STR_PAD_LEFT );
	$time['seconds']    = str_pad( $seconds, 2, "0", STR_PAD_LEFT );
	 
	$str_tiempo               = implode( ':', $time );
	
	$fechainicio = strtotime($obj_lecturista['0']["fecha_hora_inicio"]);
	$fechafin = strtotime($obj_lecturista['0']["fecha_hora_fin"]);
	$tiempo_total = $fechafin - $fechainicio;
	
	$horas              = floor ( $tiempo_total / 3600 );
	$minutes            = ( ( $tiempo_total / 60 ) % 60 );
	$seconds            = ( $tiempo_total % 60 );
	
	$time['horas']      = str_pad( $horas, 2, "0", STR_PAD_LEFT );
	$time['minutes']    = str_pad( $minutes, 2, "0", STR_PAD_LEFT );
	$time['seconds']    = str_pad( $seconds, 2, "0", STR_PAD_LEFT );
	
	$TT               = implode( ':', $time );
	
	$inconsistencias_originales = $obj_lecturista['0']['lecturas_inconsistentes']+$obj_lecturista['0']['lecturas_evaluadas'];
	$inconsistencias_actuales = $inconsistencias_originales-$obj_lecturista['0']['lecturas_evaluadas'];
	if($inconsistencias_actuales==0){
		$style_inconcistencias_actuales = 'badge badge-success';
	}else{
		$style_inconcistencias_actuales = 'badge badge-danger';
	}
	
	$obs_total = $obj_lecturista['0']['obs_total'];
	$obs_sin_lectura = $obj_lecturista['0']['obs_sin_lectura'];
	$obs_con_lectura = $obj_lecturista['0']['obs_con_lectura'];
	
	$link_ver_mapa = '<a class=\"btn btn-info btn-sm btn\" target=\"_blank\" href=\"'.ENV_WEBROOT_FULL_URL.'MonitorearLecturas/ajax_cargar_mapa/'.$obj_lecturista['0']["id"].'/'.$criterio.'/'.$date_start.'/'.$date_end.'/'.$ciclo.'/'.$sector.'\"><i class=\"fa fa-globe\"></i> Ver Ruta de Lecturas</a>';
	
	$str_estado = '';
	if($obj_lecturista['0']["lecturas_asignadas"]>0 && $obj_lecturista['0']["lecturas_terminadas"]==0){
		$str_estado = 'Estado del Proceso: <strong>SIN COMENZAR</strong>';
		$str_icon = ENV_WEBROOT_FULL_URL.'images/markers/male-2-gris.png';
	}elseif($obj_lecturista['0']["lecturas_asignadas"]>0 && $obj_lecturista['0']["lecturas_terminadas"]>0 && 
			($obj_lecturista['0']["lecturas_asignadas"]-$obj_lecturista['0']["lecturas_terminadas"])>0 && 
			($tiempo / 60) > 60){
		$str_estado = 'Estado del Proceso: <strong>EN PROCESO</strong>';
		$str_icon = ENV_WEBROOT_FULL_URL.'images/markers/male-2-rojo.png';
	}elseif($obj_lecturista['0']["lecturas_asignadas"]>0 && $obj_lecturista['0']["lecturas_terminadas"]>0 &&
			($obj_lecturista['0']["lecturas_asignadas"]-$obj_lecturista['0']["lecturas_terminadas"])>0 &&
			($tiempo / 60) > 30){
		$str_estado = 'Estado del Proceso: <strong>EN PROCESO</strong>';
		$str_icon = ENV_WEBROOT_FULL_URL.'images/markers/male-2-naranja.png';
	}elseif($obj_lecturista['0']["lecturas_asignadas"]>0 && $obj_lecturista['0']["lecturas_terminadas"]>0 &&
			($obj_lecturista['0']["lecturas_asignadas"]-$obj_lecturista['0']["lecturas_terminadas"])>0 &&
			($tiempo / 60) > 10){
		$str_estado = 'Estado del Proceso: <strong>EN PROCESO</strong>';
		$str_icon = ENV_WEBROOT_FULL_URL.'images/markers/male-2-amarillo.png';
	}elseif($obj_lecturista['0']["lecturas_asignadas"]>0 && $obj_lecturista['0']["lecturas_terminadas"]>0 &&
			($obj_lecturista['0']["lecturas_asignadas"]-$obj_lecturista['0']["lecturas_terminadas"])>0){
		$str_estado = 'Estado del Proceso: <strong>EN PROCESO</strong>';
		$str_icon = ENV_WEBROOT_FULL_URL.'images/markers/male-2-azul.png';
	}elseif(($obj_lecturista['0']["lecturas_asignadas"]-$obj_lecturista['0']["lecturas_terminadas"])==0){
		$str_estado = 'Estado del Proceso: <strong>FINALIZADO</strong>';
		$str_icon = ENV_WEBROOT_FULL_URL.'images/markers/male-2-verde.png';
	}
	
	$str_rutas .="
		  var contentString = '<div id=\"content\"><div id=\"siteNotice\"></div><h1 id=\"firstHeading\" class=\"firstHeading\">".$ultima_lectura['0']['lecturista']."</h1><div id=\"bodyContent\"><p>' +
		  		'".$lbl_suministro.": <strong>".$ultima_lectura['0']['suministro']."</strong><br>'+
		  		'Direccion: <strong>".$ultima_lectura['0']['direccion']."</strong><br>'+
		  		'Fecha de Ultima Lectura: <strong>".$ultima_lectura['0']['fechaejecucion1']."</strong><br>'+
		  		'Tiempo Transcurrido desde la Ultima Lectura: <strong>".$str_tiempo."</strong>'+
		  		'<br><br>'+
		  		'Asignadas: <strong>".$obj_lecturista['0']["lecturas_asignadas"]."</strong> '+
                'Descargadas: <strong>".$obj_lecturista['0']["lecturas_descargadas"]."</strong><br>'+
		  		'Terminadas: <strong>".$obj_lecturista['0']["lecturas_terminadas"]."</strong> '+
                'Pendientes: <strong>".($obj_lecturista['0']["lecturas_asignadas"]-$obj_lecturista['0']["lecturas_terminadas"])."</strong><br>'+
                '".$str_estado."<br>'+
                'Hora Inicio: <strong>".substr($obj_lecturista['0']["fecha_hora_inicio"],11,8)."</strong> Hora Fin: <strong>".substr($obj_lecturista['0']["fecha_hora_fin"],11,8)."</strong><br> Tiempo Total: <strong>".$TT."</strong> Tiempo Promedio: <strong>".substr($obj_lecturista['0']["tiempo_promedio"],0,8)."</strong><br>'+
                'Inconsistencias Originales: <strong>".$inconsistencias_originales."</strong> Inconsistencias Actuales: <span class=\"".$style_inconcistencias_actuales."\">".$inconsistencias_actuales."</span><br>'+
                'Evaluadas: <strong>".$obj_lecturista['0']["lecturas_evaluadas"]."</strong> '+
                'Consistentes: <strong>".$obj_lecturista['0']["lecturas_consistentes"]."</strong><br>'+
                'Condigos de Observacion: <strong>T: ".$obs_total." SL: <span>".$obs_sin_lectura."</span> CL: <span>".$obs_con_lectura."</span> </strong><br>'+
                '".$link_ver_mapa."'+
		  		'</p></div></div>';
		  var infowindow".($i+$page)." = new google.maps.InfoWindow({
		      content: contentString
		  });
		  var marker".($i+$page)." = new google.maps.Marker({
		    position: new google.maps.LatLng(".$ultima_lectura['0']['latitud1'].", ".$ultima_lectura['0']['longitud1']."),
		    title: '# ".($i+$page)."',
		    map: map,
		    icon: '".$str_icon."',
		    size: 'tiny'
		  });
		     var label".($i+$page)." = new Label({
		       map: map
		     });
		     label".($i+$page).".bindTo('position', marker".($i+$page).", 'position');
		     label".($i+$page).".bindTo('text', marker".($i+$page).", 'title');
		     
		  google.maps.event.addListener(marker".($i+$page).", 'click', function() {
			  infowindow".($i+$page).".open(map,marker".($i+$page).");
			});
		  ";
	}
  	}
  	$i++;
}
?>
<!DOCTYPE html>
<html>
  <head>
    <meta name="viewport" content="initial-scale=1.0, user-scalable=no">
    <meta charset="utf-8">
    <title>Monitoreo de Lecturistas</title>
    <style>
      html, body, #map-canvas {
        height: 100%;
        margin: 0px;
        padding: 0px
      }
    </style>
    <script src="https://maps.googleapis.com/maps/api/js?v=3.exp"></script>
    <script src="<?php echo ENV_WEBROOT_FULL_URL;?>js/google-maps-label.js"></script>
  </head>
  <body>		
			<div style="display: none;">
			  <div id="legend" style="text-align: right;">
			  
			  <div class="">
              <label class="control-label" style="background: white;padding-left: 3px;padding-right: 3px;">Sin comenzar</label>
              <img src="<?php echo ENV_WEBROOT_FULL_URL;?>images/markers/male-2-gris.png">
              </div>
			  
			  <div class="">
              <label class="control-label" style="background: white;padding-left: 3px;padding-right: 3px;">En Progreso</label>
              <img src="<?php echo ENV_WEBROOT_FULL_URL;?>images/markers/male-2-azul.png">
              </div>
              
              <div class="">
              <label class="control-label" style="background: white;padding-left: 3px;padding-right: 3px;">Finalizado</label>
              <img src="<?php echo ENV_WEBROOT_FULL_URL;?>images/markers/male-2-verde.png">
              </div>
              
              <div class="">
              <label class="control-label" style="background: white;padding-left: 3px;padding-right: 3px;">Inactivo hace mas 10 min</label>
              <img src="<?php echo ENV_WEBROOT_FULL_URL;?>images/markers/male-2-amarillo.png">
              </div>
			  
			  <div class="">
              <label class="control-label" style="background: white;padding-left: 3px;padding-right: 3px;">Inactivo hace mas 30 min</label>
              <img src="<?php echo ENV_WEBROOT_FULL_URL;?>images/markers/male-2-naranja.png">
              </div>
              
              <div class="">
              <label class="control-label" style="background: white;padding-left: 3px;padding-right: 3px;">Inactivo hace mas de 1 hora</label>
              <img src="<?php echo ENV_WEBROOT_FULL_URL;?>images/markers/male-2-rojo.png">
              </div>
              
			  </div>
		  </div>

    <div id="map-canvas"></div>
  </body>
</html>

<script type="text/javascript">
//function initialize() {
  var myLatlng = new google.maps.LatLng(<?php echo isset($latitud_center)?$latitud_center:'-6.77361';?>, <?php echo isset($longitud_center)?$longitud_center:'-79.8417';?>);
  var mapOptions = {
    zoom: 14,
    center: myLatlng
  }
  var map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);

	<?php echo $str_rutas;?>

//}

//google.maps.event.addDomListener(window, 'load', initialize);

function openInfo(i){
	$('.item_lecturista').each(function(){
		$k = $(this).data('item');
		if(i==$k){     						
	    	eval('infowindow'+$k+'.open(map,marker'+$k+');');
		}else{
			eval('infowindow'+$k+'.close();');
		}
	});
}

map.controls[google.maps.ControlPosition.RIGHT_BOTTOM].push(document.getElementById('legend'));

</script>