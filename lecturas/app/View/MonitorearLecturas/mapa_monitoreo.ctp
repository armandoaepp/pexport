<?php 
if (!isset($NEGOCIO)) {
    header("Location:".ENV_WEBROOT_FULL_URL."/Usuarios/logout");
    die();
}
?>
<?php 
if (isset($id_usuario) && $id_usuario == 25){
	$permiso_lectura = 'true';
}else {
	$permiso_lectura = 'true';
}
echo $this->element('menu',array('active'=>'monitoreargps', 'open'=>'false', 'permiso_lectura' =>$permiso_lectura, 'id_usuario ' =>isset($id_usuario)?$id_usuario:null));
?>
<?php
if($NEGOCIO == 'CIX'){
	$lbl_suministro = 'Suministro';
}elseif ($NEGOCIO == 'TAC'){
	$lbl_suministro = 'Contrato';
}else{
	$lbl_suministro = 'Suministro';
}

$str_rutas = '';
$i = 1;
$page = 0; 
foreach ($arr_lecturistas_activos as $k =>$obj_lecturista){
  	if($obj_lecturista['0']["lecturas_descargadas"]>0 && $obj_lecturista['0']["lecturas_terminadas"]>0){
  		
	//$ultima_lectura = $ObjComlecOrdenlectura->getUltimaOrdenPorLecturista($obj_lecturista['0']['id']);
  	foreach ($arr_ultima_lectura as $obj_ultima_lectura){
  		if($obj_lecturista['0']['id']==$obj_ultima_lectura['0']['lecturista1_id']){
  			$ultima_lectura = $obj_ultima_lectura;
  			break;
  		}
  	}
  		
	if($ultima_lectura['0']['latitud1']!='' && $ultima_lectura['0']['longitud1']!='' && $ultima_lectura['0']['latitud1']!='0' && $ultima_lectura['0']['longitud1']!='0'){
		
		$arr_lecturistas_activos[$k]['0']['mapa'] = 'OK';
		
	$color_flag = '72BAF7';
	

	$latitud_center = $ultima_lectura['0']['latitud1'];
	$longitud_center = $ultima_lectura['0']['longitud1'];
	
	$fechaejecucion = strtotime(date('Y-m-d H:i:s'));
	$fechaejecucion_tmp = strtotime($ultima_lectura['0']['fechaejecucion1']);
	$tiempo = $fechaejecucion - $fechaejecucion_tmp;

	$horas              = floor ( $tiempo / 3600 );
	$minutes            = ( ( $tiempo / 60 ) % 60 );
	$seconds            = ( $tiempo % 60 );
	 
	$time['horas']      = str_pad( $horas, 2, "0", STR_PAD_LEFT );
	$time['minutes']    = str_pad( $minutes, 2, "0", STR_PAD_LEFT );
	$time['seconds']    = str_pad( $seconds, 2, "0", STR_PAD_LEFT );
	 
	$str_tiempo               = implode( ':', $time );
	
	$fechainicio = strtotime($obj_lecturista['0']["fecha_hora_inicio"]);
	$fechafin = strtotime($obj_lecturista['0']["fecha_hora_fin"]);
	$tiempo_total = $fechafin - $fechainicio;
	
	$horas              = floor ( $tiempo_total / 3600 );
	$minutes            = ( ( $tiempo_total / 60 ) % 60 );
	$seconds            = ( $tiempo_total % 60 );
	
	$time['horas']      = str_pad( $horas, 2, "0", STR_PAD_LEFT );
	$time['minutes']    = str_pad( $minutes, 2, "0", STR_PAD_LEFT );
	$time['seconds']    = str_pad( $seconds, 2, "0", STR_PAD_LEFT );
	
	$TT               = implode( ':', $time );
	
	$inconsistencias_originales = $obj_lecturista['0']['lecturas_inconsistentes']+$obj_lecturista['0']['lecturas_evaluadas'];
	$inconsistencias_actuales = $inconsistencias_originales-$obj_lecturista['0']['lecturas_evaluadas'];
	if($inconsistencias_actuales==0){
		$style_inconcistencias_actuales = 'badge badge-success';
	}else{
		$style_inconcistencias_actuales = 'badge badge-danger';
	}
	
	$obs_total = $obj_lecturista['0']['obs_total'];
	$obs_sin_lectura = $obj_lecturista['0']['obs_sin_lectura'];
	$obs_con_lectura = $obj_lecturista['0']['obs_con_lectura'];
	
	$link_ver_mapa = '<a class=\"btn btn-info btn-sm btn\" target=\"_blank\" href=\"'.ENV_WEBROOT_FULL_URL.'MonitorearLecturas/ajax_cargar_mapa/'.$obj_lecturista['0']["id"].'/'.$criterio.'/'.$date_start.'/'.$date_end.'/'.$unidadneg.'/'.$ciclo.'/'.$sector.'\"><i class=\"fa fa-globe\"></i> Ver Ruta de Lecturas</a>';
	
	$str_estado = '';
	if($obj_lecturista['0']["lecturas_asignadas"]>0 && $obj_lecturista['0']["lecturas_terminadas"]==0){
		$str_estado = 'Estado del Proceso: <strong>SIN COMENZAR</strong>';
		$str_icon = ENV_WEBROOT_FULL_URL.'images/markers/male-2-gris.png';
	}elseif($obj_lecturista['0']["lecturas_asignadas"]>0 && $obj_lecturista['0']["lecturas_terminadas"]>0 && 
			($obj_lecturista['0']["lecturas_asignadas"]-$obj_lecturista['0']["lecturas_terminadas"])>0 && 
			($tiempo / 60) > 60){
		$str_estado = 'Estado del Proceso: <strong>EN PROCESO</strong>';
		$str_icon = ENV_WEBROOT_FULL_URL.'images/markers/male-2-rojo.png';
	}elseif($obj_lecturista['0']["lecturas_asignadas"]>0 && $obj_lecturista['0']["lecturas_terminadas"]>0 &&
			($obj_lecturista['0']["lecturas_asignadas"]-$obj_lecturista['0']["lecturas_terminadas"])>0 &&
			($tiempo / 60) > 30){
		$str_estado = 'Estado del Proceso: <strong>EN PROCESO</strong>';
		$str_icon = ENV_WEBROOT_FULL_URL.'images/markers/male-2-naranja.png';
	}elseif($obj_lecturista['0']["lecturas_asignadas"]>0 && $obj_lecturista['0']["lecturas_terminadas"]>0 &&
			($obj_lecturista['0']["lecturas_asignadas"]-$obj_lecturista['0']["lecturas_terminadas"])>0 &&
			($tiempo / 60) > 10){
		$str_estado = 'Estado del Proceso: <strong>EN PROCESO</strong>';
		$str_icon = ENV_WEBROOT_FULL_URL.'images/markers/male-2-amarillo.png';
	}elseif($obj_lecturista['0']["lecturas_asignadas"]>0 && $obj_lecturista['0']["lecturas_terminadas"]>0 &&
			($obj_lecturista['0']["lecturas_asignadas"]-$obj_lecturista['0']["lecturas_terminadas"])>0){
		$str_estado = 'Estado del Proceso: <strong>EN PROCESO</strong>';
		$str_icon = ENV_WEBROOT_FULL_URL.'images/markers/male-2-azul.png';
	}elseif(($obj_lecturista['0']["lecturas_asignadas"]-$obj_lecturista['0']["lecturas_terminadas"])==0){
		$str_estado = 'Estado del Proceso: <strong>FINALIZADO</strong>';
		$str_icon = ENV_WEBROOT_FULL_URL.'images/markers/male-2-verde.png';
	}
	
	$str_rutas .="
		  var contentString = '<div id=\"content\"><div id=\"siteNotice\"></div><h1 id=\"firstHeading\" class=\"firstHeading\">".$ultima_lectura['0']['lecturista']."</h1><div id=\"bodyContent\"><p>' +
		  		'".$lbl_suministro.": <strong>".$ultima_lectura['0']['suministro']."</strong><br>'+
		  		'Direccion: <strong>".$ultima_lectura['0']['direccion']."</strong><br>'+
		  		'Fecha de Ultima Lectura: <strong>".$ultima_lectura['0']['fechaejecucion1']."</strong><br>'+
		  		'Tiempo Transcurrido desde la Ultima Lectura: <strong>".$str_tiempo."</strong>'+
		  		'<br><br>'+
		  		'Asignadas: <strong>".$obj_lecturista['0']["lecturas_asignadas"]."</strong> '+
                'Descargadas: <strong>".$obj_lecturista['0']["lecturas_descargadas"]."</strong><br>'+
		  		'Terminadas: <strong>".$obj_lecturista['0']["lecturas_terminadas"]."</strong> '+
                'Pendientes: <strong>".($obj_lecturista['0']["lecturas_asignadas"]-$obj_lecturista['0']["lecturas_terminadas"])."</strong><br>'+
                '".$str_estado."<br>'+
                'Hora Inicio: <strong>".substr($obj_lecturista['0']["fecha_hora_inicio"],11,8)."</strong> Hora Fin: <strong>".substr($obj_lecturista['0']["fecha_hora_fin"],11,8)."</strong><br> Tiempo Total: <strong>".$TT."</strong> Tiempo Promedio: <strong>".substr($obj_lecturista['0']["tiempo_promedio"],0,8)."</strong><br>'+
                'Inconsistencias Originales: <strong>".$inconsistencias_originales."</strong> Inconsistencias Actuales: <span class=\"".$style_inconcistencias_actuales."\">".$inconsistencias_actuales."</span><br>'+
                'Evaluadas: <strong>".$obj_lecturista['0']["lecturas_evaluadas"]."</strong> '+
                'Consistentes: <strong>".$obj_lecturista['0']["lecturas_consistentes"]."</strong><br>'+
                'Condigos de Observacion: <strong>T: ".$obs_total." SL: <span>".$obs_sin_lectura."</span> CL: <span>".$obs_con_lectura."</span> </strong><br>'+
                '".$link_ver_mapa."'+
		  		'</p></div></div>';
		  var infowindow".($i+$page)." = new google.maps.InfoWindow({
		      content: contentString
		  });
		  var marker".($i+$page)." = new google.maps.Marker({
		    position: new google.maps.LatLng(".$ultima_lectura['0']['latitud1'].", ".$ultima_lectura['0']['longitud1']."),
		    title: '# ".($i+$page)."',
		    map: map,
		    icon: '".$str_icon."',
		    size: 'tiny'
		  });
		     var label".($i+$page)." = new Label({
		       map: map
		     });
		     label".($i+$page).".bindTo('position', marker".($i+$page).", 'position');
		     label".($i+$page).".bindTo('text', marker".($i+$page).", 'title');
		     
		  google.maps.event.addListener(marker".($i+$page).", 'click', function() {
			  infowindow".($i+$page).".open(map,marker".($i+$page).");
			});
		  ";
	}
  	}
  	$i++;
}
?>
<!DOCTYPE html>
<html>
  <head>
    <meta name="viewport" content="initial-scale=1.0, user-scalable=no">
    <meta charset="utf-8">
    <title>Monitoreo de Lecturistas</title>
    <style>
      html, body, #map-canvas {
        height: 100%;
        margin: 0px;
        padding: 0px
      }
    </style>
    <script src="https://maps.googleapis.com/maps/api/js?v=3.exp"></script>
    <script src="<?php echo ENV_WEBROOT_FULL_URL;?>js/google-maps-label.js"></script>
  </head>
  <body>
   


	<div class="container-fluid" id="pcont" style="height: 100%;margin: 0px;padding: 0px;">
		<div id="map-canvas"></div>
	</div>

	    <div id="arbol" class="page-aside app filters tree">
       	<div class="panel-group accordion" id="accordion">
	       	<div class="panel panel-default">
	        	
	        	<div class="panel-heading">
				    <h4 class="panel-title">
						<a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseThree">
						    <i class="fa fa-angle-right"></i> Filtros
						</a>
				    </h4>
				</div>

				<div id="collapseThree" class="panel-collapse collapse" style="height: 0px;">
				    <div class="panel-body">
						
						<?php 
			            if(strpos($criterio, 'F') !== false){
							$show_tab2 = 'display: block;';
						}else{
							$show_tab2 = 'display:none;';
						}
						if(strpos($criterio, 'U') !== false){
							$show_tab1 = 'display: block;';
						}else{
							$show_tab1 = 'display:none;';
						}
			            ?>
	            
			            <div class="form-group">
			              <label class="control-label">Por :</label>
			              <input id="cbo_criterio_mapa" name="cbo_criterio_mapa" style="width:240px;" placeholder="Seleccione una opcion">
			            </div>

	           			<div class="por_ubicacion" style="<?php echo $show_tab1;?>">
	                
	                
			                <div class="form-group">
			                    <label class="control-label">Por Unidad Negocio:</label>              
			              	    <?php 
			        			  	echo '<select class="" name="unidadneg" id="unidadneg">
			        			  	<option value="0">Todos</option>';
			        				foreach ($listar_unidadneg as $v){
										if($unidadneg==$v['0']['unidadneg']){
											echo '<option value='.$v['0']['unidadneg'].' selected>'.$v['0']['nombreunidadnegocio'].'</option>';
										}else{
											echo '<option value='.$v['0']['unidadneg'].'>'.$v['0']['nombreunidadnegocio'].'</option>';
										}
			        				}
			        				echo '</select>';	
			        		    ?>        			
			                </div>
	                
				          	<div class="form-group">
				              <label class="control-label">Por Ciclo:<span class="ciclo_loading" style="display:none;"><img src="<?php echo ENV_WEBROOT_FULL_URL ?>/js/jquery.select2/select2-spinner.gif"></span></label>              
				              	<?php 
				        			  	echo '<select class="" name="id_ciclo" id="id_ciclo">
				        			  	<option value="0">Todos</option>';
				        				foreach ($listar_ciclo as $v){
											if($ciclo==$v['0']['idciclo']){
												echo '<option value='.$v['0']['idciclo'].' selected>'.$v['0']['idciclo'].'</option>';
											}else{
												echo '<option value='.$v['0']['idciclo'].'>'.$v['0']['idciclo'].'</option>';	
											}
				        				}
				        				echo '</select>';	
				        		?>        			
				            </div>

				          	<div class="form-group">
				              <label class="control-label">Por <?php if($NEGOCIO == 'CIX'){echo 'Sector';}elseif ($NEGOCIO == 'TAC'){echo 'Zona';}else{echo 'Sector';}?>: <span class="sector_loading" style="display:none;"><img src="<?php echo ENV_WEBROOT_FULL_URL ?>/js/jquery.select2/select2-spinner.gif"></span></label>              
				              	<?php 
				        			  	echo '<select class="" name="monitoreo_id_sector" id="monitoreo_id_sector">
				        			  	<option value="0">Todos</option>';
				        				foreach ($listar_sector as $v){
											if($sector==$v['0']['sector']){
												echo '<option value='.$v['0']['sector'].' selected>'.$v['0']['sector'].'</option>';
											}else{
												echo '<option value='.$v['0']['sector'].'>'.$v['0']['sector'].'</option>';
											}
				        				}
				        				echo '</select>';	
				        		?>        			
				            </div>
	            		</div>
	            
			            <div class="por_fecha_asignacion" style="<?php echo $show_tab2;?>">
				            <div class="form-group">
				              <label class="control-label">Por Fecha de Asignaci&oacute;n (Inicio) :</label>
				              <div class="input-group date datetime" data-min-view="2" data-date-format="yyyy-mm-dd" style="width:50%;">
				                <input id="dpt_date_start" class="form-control" size="16" type="text" value="<?php echo $date_start;?>" readonly>
				                <span class="input-group-addon btn btn-primary"><span class="glyphicon glyphicon-th"></span></span>
				              </div>	
				            </div>

				            <div class="form-group">
				              <label class="control-label">Por Fecha de Asignaci&oacute;n (Fin) :</label>
				              <div class="input-group date datetime" data-min-view="2" data-date-format="yyyy-mm-dd" style="width:50%;">
				                <input id="dpt_date_end" class="form-control" size="16" type="text" value="<?php echo $date_end;?>" readonly>
				                <span class="input-group-addon btn btn-primary"><span class="glyphicon glyphicon-th"></span></span>
				              </div>	
				            </div>
				        </div>

	   
	           			<button class='btn btn-primary' id='generar-mapa'>Generar Mapa</button>
				  	</div>
				</div>

			  	<div class="panel-heading">
					<h4 class="panel-title">
						<a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
						    <i class="fa fa-angle-right"></i> Lecturistas
						</a>
					</h4>
				</div>

				<div id="collapseOne" class="panel-collapse collapse in" style="height: auto;">
				    <div class="panel-body">
				  
					    <ol class="dd-list" style="max-height: 470px; overflow: auto;">
			            <?php 
			            foreach ($arr_lecturistas_activos as $k =>$obj_lecturista){
						?>
						<li class="dd-item" data-id="1">
		                  <div class="dd-handle">
		                  	<?php 
		                  	if($obj_lecturista['0']["lecturas_descargadas"]>0 && $obj_lecturista['0']["lecturas_terminadas"]>0 && isset($obj_lecturista['0']['mapa']) && $obj_lecturista['0']['mapa']=='OK'){
								$str_class_item = 'item_lecturista';
								$str_action = 'openInfo('.($k+1).');';
							}else{
								$str_class_item = '';
								$str_action = "alert('No hay datos para mostrar en el mapa');";
							}
							?>
		                  <a class="<?php echo $str_class_item;?>" data-item="<?php echo ($k+1);?>" href="javascript: <?php echo $str_action;?>">
		                  <div>
		                  <?php echo ($k+1).' - '.$obj_lecturista['0']['lecturista'];?>
		                    <div class="progress progress-striped" style="margin-bottom: 0px" title ='<?php echo  round(($obj_lecturista['0']['lecturas_terminadas'] * 100) / $obj_lecturista['0']['lecturas_asignadas'],2).'% '?> - <?php echo $obj_lecturista['0']['lecturas_terminadas'];?> <?php echo "Lecturas Terminadas" ?>'>
							  <div class="<?php if(round(($obj_lecturista['0']['lecturas_terminadas'] * 100) / $obj_lecturista['0']['lecturas_asignadas'],2) < '50'){ echo 'progress-bar progress-bar-danger';} else {echo 'progress-bar progress-bar-success';}?>" style="width: <?php echo  round(($obj_lecturista['0']['lecturas_terminadas'] * 100) / $obj_lecturista['0']['lecturas_asignadas'],2).'% '?>; color: black; font-weight: bold;"><?php echo  round(($obj_lecturista['0']['lecturas_terminadas'] * 100) / $obj_lecturista['0']['lecturas_asignadas'],2).'% '?>(<?php echo $obj_lecturista['0']['lecturas_terminadas'];?>)
							  </div>
							</div>
						  </div>
						  </a>
		                  </div>
		                </li>
						<?php 
						}
			            ?>    
			       		</ol>
				  
				    </div>
				</div>
				
				<div style="display: none;">
				    <div id="legend" style="text-align: right;">
				  
					  <div class="">
			              <label class="control-label" style="background: white;padding-left: 3px;padding-right: 3px;">Sin comenzar</label>
			              <img src="<?php echo ENV_WEBROOT_FULL_URL;?>images/markers/male-2-gris.png">
		              </div>
					  
					  <div class="">
			              <label class="control-label" style="background: white;padding-left: 3px;padding-right: 3px;">En Progreso</label>
			              <img src="<?php echo ENV_WEBROOT_FULL_URL;?>images/markers/male-2-azul.png">
		              </div>
		              
		              <div class="">
			              <label class="control-label" style="background: white;padding-left: 3px;padding-right: 3px;">Finalizado</label>
			              <img src="<?php echo ENV_WEBROOT_FULL_URL;?>images/markers/male-2-verde.png">
		              </div>
		              
		              <div class="">
			              <label class="control-label" style="background: white;padding-left: 3px;padding-right: 3px;">Inactivo hace mas 10 min</label>
			              <img src="<?php echo ENV_WEBROOT_FULL_URL;?>images/markers/male-2-amarillo.png">
		              </div>
					  
					  <div class="">
			              <label class="control-label" style="background: white;padding-left: 3px;padding-right: 3px;">Inactivo hace mas 30 min</label>
			              <img src="<?php echo ENV_WEBROOT_FULL_URL;?>images/markers/male-2-naranja.png">
		              </div>
		              
		              <div class="">
			              <label class="control-label" style="background: white;padding-left: 3px;padding-right: 3px;">Inactivo hace mas de 1 hora</label>
			              <img src="<?php echo ENV_WEBROOT_FULL_URL;?>images/markers/male-2-rojo.png">
		              </div>
				    </div>
			    </div>
			</div>
        </div>
    </div>

  </body>
</html>

<script type="text/javascript">
//function initialize() {
  var myLatlng = new google.maps.LatLng(<?php echo isset($latitud_center)?$latitud_center:'-6.77361';?>, <?php echo isset($longitud_center)?$longitud_center:'-79.8417';?>);
  var mapOptions = {
    zoom: 14,
    center: myLatlng
  }
  var map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);

	<?php echo $str_rutas;?>

//}

//google.maps.event.addDomListener(window, 'load', initialize);

function openInfo(i){
	$('.item_lecturista').each(function(){
		$k = $(this).data('item');
		if(i==$k){     						
	    	eval('infowindow'+$k+'.open(map,marker'+$k+');');
		}else{
			eval('infowindow'+$k+'.close();');
		}
	});
}

map.controls[google.maps.ControlPosition.RIGHT_BOTTOM].push(document.getElementById('legend'));

$('body').on('click','#generar-mapa',function(e){
  criterio = $('#cbo_criterio_mapa').val();
  date_start = $('#dpt_date_start').val();
  date_end = $('#dpt_date_end').val();
  unidadneg = $('#unidadneg').val();
  id_ciclo = $('#id_ciclo').val();
  id_sector = $('#monitoreo_id_sector').val();
  window.open("<?php echo ENV_WEBROOT_FULL_URL;?>MonitorearLecturas/mapa_monitoreo/"+criterio+"/"+date_start+"/"+date_end+"/"+unidadneg+"/"+id_ciclo+"/"+id_sector,"_top");
});

</script>
<script type="text/javascript">

var preload_data = [
    	            { id: 'F', text: 'Fecha de Asignación'}
    	            , { id: 'U', text: 'Ubicación Geográfica'}
    	          ];
               
                $('#cbo_criterio_mapa').select2({
                    multiple: true
                    ,query: function (query){
                        var data = {results: []};
               
                        $.each(preload_data, function(){
                            if(query.term.length == 0 || this.text.toUpperCase().indexOf(query.term.toUpperCase()) >= 0 ){
                                data.results.push({id: this.id, text: this.text });
                            }
                        });
               
                        query.callback(data);
                    }
                });

                $('#cbo_criterio_mapa').select2('data', [<?php if(strpos($criterio, 'F') !== false){ echo "{id: 'F', text: 'Fecha de Asignación'},";}if(strpos($criterio, 'U') !== false){echo "{id: 'U', text: 'Ubicación Geográfica'}";}?>] );
                
                $('#cbo_criterio_mapa').on("change",function (e) { 
    				console.log("change "+e.val);
    				if(e.val.indexOf("F")>-1){
    					$('.por_fecha_asignacion').show();					
    				}else{
    					$('.por_fecha_asignacion').hide();
    				}
    				if(e.val.indexOf("U")>-1){
    					$('.por_ubicacion').show();
    				}else{
    					$('.por_ubicacion').hide();
    				}
    			});

    $('#unidadneg').select2({
	width: '100%'
	});
$('#unidadneg').on("change",function (e) { 
	console.log("change "+e.val);
	$('.ciclo_loading').show();
	$('#id_ciclo').select2("enable", false);
	unidad_neg = e.val || 0;
	$.ajax({
                            url : ENV_WEBROOT_FULL_URL+"/ComlecOrdenlecturas/listarCicloPorUnidadneg/"+unidad_neg,
		type : "POST",
		dataType : 'html',
		success : function(data) {
			console.log(data);
			$('#id_ciclo').html(data);
			$("#id_ciclo").select2("val", "0");
			$('#id_ciclo').select2("enable", true);
			$('.ciclo_loading').hide();
		}
	});
});  
    $('#id_ciclo').select2({
	width: '100%'
	});
$('#id_ciclo').on("change",function (e) { 
	console.log("change "+e.val);
	$('.sector_loading').show();
	$('#monitoreo_id_sector').select2("enable", false);
	id_ciclo = e.val || 0;
	$.ajax({
		url : ENV_WEBROOT_FULL_URL+"/ComlecOrdenlecturas/listarSectorPorCiclo/"+id_ciclo,
		type : "POST",
		dataType : 'html',
		success : function(data) {
			console.log(data);
			$('#monitoreo_id_sector').html(data);
			$("#monitoreo_id_sector").select2("val", "0");
			$('#monitoreo_id_sector').select2("enable", true);
			$('.sector_loading').hide();
		}
	});
});
            
                       
            
$('#monitoreo_id_sector').select2({
	width: '100%'
});

$('.accordion a').on("click",function (e) {
	setTimeout(function(){$('#generar-mapa').focus();}, 400);
});
</script>