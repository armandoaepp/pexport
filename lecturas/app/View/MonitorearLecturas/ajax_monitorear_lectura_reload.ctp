<?php 
if (!isset($NEGOCIO)) {
    header("Location:".ENV_WEBROOT_FULL_URL."/Usuarios/logout");
    die();
}
?>
<!--

<div class="page-head floatHeader" style="padding: 5px;">
		
		<div class="col-sm-12 col-md-12 widget-monitoreo">
	    	
	    		<div class="col-lg-4 widget-monitoreo-block">
					<div class="widget-block" style="margin-bottom: 8px;">
						<div class="white-box">
							<div class="fact-data">
								<div class="epie-chart" <?php //echo $color_bar_suministros;?> data-percent="<?php //echo $percent_suministros;?>"><span><?php //echo $percent_suministros;?>%</span></div>
							</div>
							<div class="fact-data no-padding text-shadow" style="margin: 10px 0px;">
								<h3>Suministros</h3>
								<h2><?php //echo $count_suministros;?></h2>
							</div>
						</div>
					</div>
				</div>
				
				<div class="col-lg-4 widget-monitoreo-block">
					<div class="widget-block" style="margin-bottom: 8px;">
						<div class="white-box">
							<div class="fact-data">
								<div class="epie-chart" <?php //echo $color_bar_asignados;?> data-percent="<?php //echo $percent_asignados;?>"><span><?php //echo $percent_asignados;?>%</span></div>
							</div>
							<div class="fact-data no-padding text-shadow" style="margin: 10px 0px;">
								<h3>Asignados</h3>
								<h2><?php //echo $count_asignados;?></h2>
							</div>
						</div>
					</div>
				</div>
				
				<div class="col-lg-4 widget-monitoreo-block" style="">
					<div class="widget-block" style="margin-bottom: 8px;">
						<div class="white-box">
							<div class="fact-data" style="width: 58px !important">
								<div class="epie-chart" <?php //echo $color_bar_descargados;?> data-percent="<?php //echo $percent_descargados;?>"><span><?php //echo $percent_descargados;?>%</span></div>
							</div>
							<div class="fact-data no-padding text-shadow" style="margin: 10px 0px;">
								<h3>Descargados</h3>
								<h2><?php //echo $count_descargados;?></h2>
							</div>
						</div>
					</div>
				</div>
				
				<div class="col-lg-4 widget-monitoreo-block">
					<div class="widget-block" style="margin-bottom: 8px;">
						<div class="white-box">
							<div class="fact-data">
								<div class="epie-chart" <?php //echo $color_bar_finalizados;?> data-percent="<?php //echo $percent_finalizados;?>"><span><?php //echo $percent_finalizados;?>%</span></div>
							</div>
							<div class="fact-data no-padding text-shadow" style="margin: 10px 0px;">
								<h3>Finalizados</h3>
								<h2><?php //echo $count_finalizados;?></h2>
							</div>
						</div>
					</div>
				</div>
				
				<div class="col-lg-4 widget-monitoreo-block">
					<div class="widget-block" style="margin-bottom: 8px;">
						<div class="white-box">
							<div class="fact-data">
								<div class="epie-chart" <?php //echo $color_bar_enviados_distriluz;?> data-percent="<?php //echo $percent_enviados_distriluz;?>"><span><?php //echo $percent_enviados_distriluz;?>%</span></div>
							</div>
							<div class="fact-data no-padding text-shadow" style="margin: 10px 0px;">
								<h3>Distriluz</h3>
								<h2><?php //echo $count_enviados_distriluz;?></h2>
							</div>
						</div>
					</div>
				</div>
			</div>
				
				<ol class="breadcrumb" style="display: inline-block;">
				  <li><button type="button" class="btn-xs btn-default arbol-top-lecturas" >Arbol</button></li>
				  <li><button type="button" class="btn-xs btn-default arbol-head-lecturas" ><i class="fa fa-lg fa-arrows-v"></i></button></li>
				  <li><a href="#">Ordenes de Lectura</a></li>
				  <li><a href="#">Monitorear</a></li>
				  <li class="active">Monitorear Lecturas</li>
				</ol>

				<div class="actualizacion" style="display: inline-block; ">
                    | &Uacute;ltima actualizaci&oacute;n: 
                    <span class="badge badge-primary label_last_update"><?php //echo date('Y-m-d H:i:s');?></span>
                    <a href="javascript:void(0);" class="btn_play_stop_update_monitoreo" data-action="play" title="Detener actualizaci&oacute;n autom&aacute;tica."><i class="fa fa-stop"></i></a>
                </div>
				
		</div>
		-->
	    <div class="cl-mcont" style="">
	    	
		    <div class="col-sm-12 col-md-12" >
			    <div class="block">

				 <div class="content">
		          <div class="tabla">
		           <div id="monit" class=' tabla4' >

						<div class="header no-border">
							<h2>Monitorear Lecturas (Perspectiva Lecturistas)
	                            <span class="pull-right">
	                           		<button class='btn btn-primary' id='btn_mapa_monitoreo' >
	                           			<i class=" fa fa-globe" style="font-size:20px; color:#fff;"></i>
	                           		</button>
	                            </span>
                            </h2>
						</div>

						<div id="loading-monitoreo" class="content no-padding">			

	                        <div class='table-responsive'>  
	                            <div id="grid">
	    							<table class="red table table-bordered tableWithFloatingHeader" width="80%" style="background-color:#fff;" id="list-monitorear-lecturistas">
	                                        <thead>
	                                            <tr>
	                                                <th class="hidden">Cod_T</th>
	                                                <th class="hidden">Cod_SL</th>
	                                                <th class="hidden">Cod_CL</th>
	                                                <th class="hidden">Inconsistencias_originales</th>
	                                                <th class="hidden">Inconsistencias_actuales</th>
	                                                <th class="hidden">Lecturas_terminadas</th>
	                                                <th class="hidden">Lecturas_asignadas</th>
	                                                <th class="center">#</th>
	                                                <th class="center">LECTURISTA</th>
	                                                <th class="center">ASI <i class="fa fa-calendar-o"></i></th>
	                                                <th class="center">DES <i class="fa fa-download"></i></th>
	                                                <th class="center">TIEMPO <i class="fa fa-clock-o"></i></th>
	                                                <th class="center">FIN <i class="fa fa-upload"></i></th>
	                                                <th class="center">P <i class="fa fa-eraser"></i></th>
	                                                <th class="center">% AVANCE <i class="fa fa-tachometer"></i></th>
	                                                <th class="center">INC <i class="fa fa-search"></i></th>
	                                                <th class="center">EVA <i class="fa fa-check-square-o"></i></th>
	                                                <th class="center">CON <i class="fa fa-check"></i></th>
	                                                <th class="center">COD <i class="fa fa-bullseye"></i></th>                                      
	                                                <th class="center">GPS <i class="fa fa-map-marker"></i></th>
	                                            </tr>
	                                        </thead>

	                                        <tbody class="no-border-x">
	                                            <?php 
	                                            $i = 1;
	                                            foreach ($listar_empleados as $empleados){
	                                            ?>
	                                                <tr>
	                                                    <td class="hidden">
	                                                    <?php echo $empleados['0']['obs_total'];?> 
	                                                    </td>
	                                                    <td class="hidden">
	                                                    <?php echo $empleados['0']['obs_sin_lectura'];?> 
	                                                    </td>
	                                                    <td class="hidden">
	                                                    <?php echo $empleados['0']['obs_con_lectura'];?> 
	                                                    </td>
	                                                    <td class="hidden">
	                                                    <?php
	                                                    $inconsistencias_originales = $empleados['0']['lecturas_inconsistentes']+$empleados['0']['lecturas_evaluadas'];
	                                                    $inconsistencias_actuales = $inconsistencias_originales-$empleados['0']['lecturas_evaluadas'];
	                                                    if($inconsistencias_actuales==0){
	                                                        $style_inconcistencias_actuales = 'badge badge-success';
	                                                    }else{
	                                                        $style_inconcistencias_actuales = 'badge badge-danger';
	                                                    }
	                                                    echo $inconsistencias_originales;
	                                                    ?>
	                                                    </td>
	                                                    <td class="hidden">
	                                                    <?php
	                                                    echo $inconsistencias_actuales;
	                                                    ?>
	                                                    </td>
	                                                    <td class="hidden">
	                                                    <?php
	                                                    echo $empleados['0']['lecturas_terminadas'];
	                                                    ?>
	                                                    </td>
	                                                    <td class="hidden">
	                                                    <?php
	                                                    echo $empleados['0']['lecturas_asignadas'];
	                                                    ?>
	                                                    </td>
	                                                    <td>
	                                                    <?php echo $i++;?> 
	                                                    </td>
	                                                    <td class="text-left" empleado_id = <?php echo $empleados['0']["id"];?>><a target="_blank" href= "<?php echo ENV_WEBROOT_FULL_URL ?>EvaluarInconsistencias/evaluar_inconsistencia/L,<?php echo $criterio;?>/<?php echo $date_start;?>/<?php echo $date_end;?>/<?php echo $unidadneg;?>/<?php echo $ciclo;?>/<?php echo $sector;?>/0/<?php echo $empleados['0']["id"];?>" ><i class="fa fa-user"></i> <?php echo  $empleados['0']['lecturista']; ?></a></td>

	                                                    <td><?php echo $empleados['0']["lecturas_asignadas"];?></td>
	                                                    <td><?php echo $empleados['0']["lecturas_descargadas"];?></td>
	                                                    
	                                                    <?php 
	                                                    $fechainicio = strtotime($empleados['0']["fecha_hora_inicio"]);
	                                                    $fechafin = strtotime($empleados['0']["fecha_hora_fin"]);
	                                                    $tiempo_total = $fechafin - $fechainicio;
	                                                    
	                                                    $horas              = floor ( $tiempo_total / 3600 );
	                                                    $minutes            = ( ( $tiempo_total / 60 ) % 60 );
	                                                    $seconds            = ( $tiempo_total % 60 );
	                                                    
	                                                    $time['horas']      = str_pad( $horas, 2, "0", STR_PAD_LEFT );
	                                                    $time['minutes']    = str_pad( $minutes, 2, "0", STR_PAD_LEFT );
	                                                    $time['seconds']    = str_pad( $seconds, 2, "0", STR_PAD_LEFT );
	                                                    
	                                                    $TT               = implode( ':', $time );
	                                                    ?>
	                                                    <td><strong><?php echo "HI:&nbsp;".substr($empleados['0']["fecha_hora_inicio"],11,8).'<br>HF:&nbsp;'.substr($empleados['0']["fecha_hora_fin"],11,8).'<br>TT:&nbsp;'.$TT.'<br>TP:&nbsp;'.substr($empleados['0']["tiempo_promedio"],0,8);?></strong></td>
	                                                    

	                                                    <td style="font-weight: bold;" class="text-center"><?php echo $empleados['0']["lecturas_terminadas"];?>
	                                                    </td>

	                                                    <td class="text-center" style="font-weight: bold;"><?php echo $empleados['0']["lecturas_asignadas"]-$empleados['0']["lecturas_terminadas"];?></td>

	                                                    <td>
	                                                        <div class="progress progress-striped" title ='<?php echo  round(($empleados['0']['lecturas_terminadas'] * 100) / $empleados['0']['lecturas_asignadas'],2).'% '?> - <?php echo $empleados['0']['lecturas_terminadas'];?> <?php echo "Lecturas Terminadas" ?>'>
	                                                          <div class="<?php if(round(($empleados['0']['lecturas_terminadas'] * 100) / $empleados['0']['lecturas_asignadas'],2) < '50'){ echo 'progress-bar progress-bar-danger';} else {echo 'progress-bar progress-bar-success';}?>" style="width: <?php echo  round(($empleados['0']['lecturas_terminadas'] * 100) / $empleados['0']['lecturas_asignadas'],2).'% '?>; color: black; font-weight: bold;"><?php echo  round(($empleados['0']['lecturas_terminadas'] * 100) / $empleados['0']['lecturas_asignadas'],2).'% '?>(<?php echo $empleados['0']['lecturas_terminadas'];?>)
	                                                          </div>
	                                                        </div>
	                                                    </td>

	                                                    <td class="text-center">
	                                                        <?php                       
	                                                        echo 'Originales: '.$inconsistencias_originales.'<br>Actuales: <span class="'.$style_inconcistencias_actuales.'">'.$inconsistencias_actuales.'</span>';?>
	                                                    </td>
	                                                    
	                                                    <td>
	                                                        <?php echo $empleados['0']['lecturas_evaluadas'];?>
	                                                    </td>
	                                                    
	                                                    <td class="text-center">
	                                                        <?php echo $empleados['0']['lecturas_consistentes'];?>
	                                                    </td>
	                                                    
	                                                    <td class="text-center">
	                                                        <?php 
	                                                        $obs_total = $empleados['0']['obs_total'];
	                                                        $obs_sin_lectura = $empleados['0']['obs_sin_lectura'];
	                                                        $obs_con_lectura = $empleados['0']['obs_con_lectura'];
	                                                        
	                                                        
	                                                        echo '<strong>T: '.$obs_total.'<br>SL: <span>'.$obs_sin_lectura.'</span> '.'<br>CL: <span>'.$obs_con_lectura.'</span> </strong>';?>
	                                                    </td>
	                                                                                        

	                                                    <td class="text-center"><a class="btn btn-info btn-sm btn" target="_blank" href="<?php echo ENV_WEBROOT_FULL_URL;?>MonitorearLecturas/ajax_cargar_mapa/<?php echo $empleados['0']["id"];?>/<?php echo $criterio;?>/<?php echo $date_start;?>/<?php echo $date_end;?>/<?php echo $ciclo;?>/<?php echo $sector;?>"><i class="fa fa-globe"></i></td>
	                                                </tr>
	                                            <?php } ?>      
	                                        </tbody>

	                                        <tfoot>
	                                            <tr>
	                                            </tr>
	                                        </tfoot>

	                                </table>
	                            </div>
	                        </div>

						</div>
					</div>
				</div>
</div>
			</div>

			</div>
         </div>
    </div>