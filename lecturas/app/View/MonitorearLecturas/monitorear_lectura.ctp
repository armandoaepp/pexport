<?php 
if (!isset($NEGOCIO)) {
    header("Location:".ENV_WEBROOT_FULL_URL."/Usuarios/logout");
    die();
}
?>
<?php 
if ($id_usuario == 25){
    $permiso_lectura = 'true';
}else {
    $permiso_lectura = 'true';
}
echo $this->element('menu',array('active'=>'monitorearlectura', 'open'=>'true', 'permiso_lectura' =>$permiso_lectura, 'id_usuario ' =>$id_usuario));
?>
<style>

    .widget-monitoreo {
        padding: 0px !important;
    }
    .widget-monitoreo h2 {
        font-size: 20px !important;
    }
    .widget-monitoreo h3 {
        font-size: 15px !important;
    }
    .widget-monitoreo-block {
        width: 182px;
        padding: 3px;
        display: inline-table;
    }
    .widget-monitoreo-block .fact-data {
        margin: 4px 0px !important;
        padding-left: 7px !important;
        padding-right: 7px !important;
        width: 64px !important;
    }

</style>



<div class="container-fluid div_content_monitor_reload" id="pcont">
        <!--
        <div class="page-head floatHeader " style="padding: 5px; display:none;" data-spy="affix" data-offset-top="60" data-offset-bottom="200">
            
            <div class="col-sm-12 col-md-12 widget-monitoreo">
                <?php 
            
                ?>
                    <div class="col-sm-3 col-md-6 col-lg-4 widget-monitoreo-block">
                        <div class="widget-block" style="margin-bottom: 8px;">
                            <div class="white-box">
                                <div class="fact-data">
                                    <div class="epie-chart" <?php //echo $color_bar_suministros;?> data-percent="<?php //echo $percent_suministros;?>"><span><?php //echo $percent_suministros;?>%</span></div>
                                </div>
                                <div class="fact-data no-padding text-shadow" style="margin: 10px 0px;">
                                    <h3>Suministros</h3>
                                    <h2><?php //echo $count_suministros;?></h2>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    <div class="col-sm-3 col-md-6 col-lg-4 widget-monitoreo-block">
                        <div class="widget-block" style="margin-bottom: 8px;">
                            <div class="white-box">
                                <div class="fact-data">
                                    <div class="epie-chart" <?php //echo $color_bar_asignados;?> data-percent="<?php //echo $percent_asignados;?>"><span><?php //echo $percent_asignados;?>%</span></div>
                                </div>
                                <div class="fact-data no-padding text-shadow" style="margin: 10px 0px;">
                                    <h3>Asignados</h3>
                                    <h2><?php //echo $count_asignados;?></h2>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    <div class="col-sm-3 col-md-6 col-lg-4 widget-monitoreo-block" style="">
                        <div class="widget-block" style="margin-bottom: 8px;">
                            <div class="white-box">
                                <div class="fact-data" style="width: 58px !important">
                                    <div class="epie-chart" <?php //echo $color_bar_descargados;?> data-percent="<?php //echo $percent_descargados;?>"><span><?php //echo $percent_descargados;?>%</span></div>
                                </div>
                                <div class="fact-data no-padding text-shadow" style="margin: 10px 0px;">
                                    <h3>Descargados</h3>
                                    <h2><?php// echo $count_descargados;?></h2>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    <div class="col-sm-3 col-md-6 col-lg-4 widget-monitoreo-block">
                        <div class="widget-block" style="margin-bottom: 8px;">
                            <div class="white-box">
                                <div class="fact-data">
                                    <div class="epie-chart" <?php //echo $color_bar_finalizados;?> data-percent="<?php //echo $percent_finalizados;?>"><span><?php //echo $percent_finalizados;?>%</span></div>
                                </div>
                                <div class="fact-data no-padding text-shadow" style="margin: 10px 0px;">
                                    <h3>Finalizados</h3>
                                    <h2><?php //echo $count_finalizados;?></h2>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    <div class="col-sm-3 col-md-6 col-lg-4 widget-monitoreo-block">
                        <div class="widget-block" style="margin-bottom: 8px;">
                            <div class="white-box">
                                <div class="fact-data">
                                    <div class="epie-chart" <?php //echo $color_bar_enviados_distriluz;?> data-percent="<?php //echo $percent_enviados_distriluz;?>"><span><?php //echo $percent_enviados_distriluz;?>%</span></div>
                                </div>
                                <div class="fact-data no-padding text-shadow" style="margin: 10px 0px;">
                                    <h3>Distriluz</h3>
                                    <h2><?php //echo $count_enviados_distriluz;?></h2>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <br/>
                <ol class="breadcrumb" style="display: inline-block;">
                  <li><button type="button" class="btn-xs btn-default arbol-top-lecturas" >Arbol</button></li>
                  <li><button type="button" class="btn-xs btn-default arbol-head-lecturas" ><i class="fa fa-lg fa-arrows-v"></i></button></li>
                  <li><a href="#">Ordenes de Lectura</a></li>
                  <li><a href="#">Monitorear</a></li>
                  <li class="active">Monitorear Lecturas</li>
                </ol>

              
                <div class="actualizacion"  style="display: inline-block; ">
                    | &Uacute;ltima actualizaci&oacute;n: 
                    <span class="badge badge-primary label_last_update"><?php //echo date('Y-m-d H:i:s');?></span>
                    <a href="javascript:void(0);" class="btn_play_stop_update_monitoreo" data-action="play" title="Detener actualizaci&oacute;n autom&aacute;tica."><i class="fa fa-stop"></i></a>
                </div>
                
        </div>
        -->


        <div class="cl-mcont" style="">

            <div class="col-sm-12 col-md-12" >
                <div class="block">

                 <div class="content">
                  <div class="tabla">
                    <div id="monit" class=" tabla4" >
                        <div class="header no-border">
                            <h2>Monitorear Lecturas (Perspectiva Lecturistas)
                            <span class="pull-right">
                            <button class='btn btn-primary' id='btn_mapa_monitoreo' ><i class=" fa fa-globe" style="font-size:20px; color:#fff;"></i></button>
                            </span>
                            </h2>
                                                       
                            
                        </div>

                        <div id="loading-monitoreo" class="content no-padding">         

                        <div class='table-responsive '>  
                            <div id="grid" class="">

                                <table class="red table table-bordered tableWithFloatingHeader" width="100%" style="background-color:#fff;">
                                        <thead>
                                            <tr>
                                                <th class="hidden">Cod_T</th>
                                                <th class="hidden">Cod_SL</th>
                                                <th class="hidden">Cod_CL</th>
                                                <th class="hidden">Inconsistencias_originales</th>
                                                <th class="hidden">Inconsistencias_actuales</th>
                                                <th class="hidden">Lecturas_terminadas</th>
                                                <th class="hidden">Lecturas_asignadas</th>

                                                <th class="center">#</th>
                                                <th class="center">LECTURISTA</th>
                                                <th class="center">ASI </th>
                                                <th class="center">DES </th>
                                                <th class="center">TIEMPO </th>
                                                <th class="center">FIN </th>
                                                <th class="center">P </th>
                                                <th class="center">% AVANCE </th>
                                                <th class="center">INC </th>
                                                <th class="center">EVA </th>
                                                <th class="center">CON </th>
                                                <th class="center">COD </th>                                      
                                                <th class="center">GPS </th>
                                            </tr>
                                        </thead>

                                        <tbody class="no-border-x">
                                            <?php 
                                            $i = 1;
                                            foreach ($listar_empleados as $empleados){
                                            ?>
                                                <tr>
                                                    <td class="hidden">
                                                    <?php echo $empleados['0']['obs_total'];?> 
                                                    </td>
                                                    <td class="hidden">
                                                    <?php echo $empleados['0']['obs_sin_lectura'];?> 
                                                    </td>
                                                    <td class="hidden">
                                                    <?php echo $empleados['0']['obs_con_lectura'];?> 
                                                    </td>
                                                    <td class="hidden">
                                                    <?php
                                                    $inconsistencias_originales = $empleados['0']['lecturas_inconsistentes']+$empleados['0']['lecturas_evaluadas'];
                                                    $inconsistencias_actuales = $inconsistencias_originales-$empleados['0']['lecturas_evaluadas'];
                                                    if($inconsistencias_actuales==0){
                                                        $style_inconcistencias_actuales = 'badge badge-success';
                                                    }else{
                                                        $style_inconcistencias_actuales = 'badge badge-danger';
                                                    }
                                                    echo $inconsistencias_originales;
                                                    ?>
                                                    </td>
                                                    <td class="hidden">
                                                    <?php
                                                    echo $inconsistencias_actuales;
                                                    ?>
                                                    </td>
                                                    <td class="hidden">
                                                    <?php
                                                    echo $empleados['0']['lecturas_terminadas'];
                                                    ?>
                                                    </td>
                                                    <td class="hidden">
                                                    <?php
                                                    echo $empleados['0']['lecturas_asignadas'];
                                                    ?>
                                                    </td>
                                                    <td>
                                                    <?php echo $i++;?> 
                                                    </td>
                                                    <td class="text-left" empleado_id = <?php echo $empleados['0']["id"];?>><a target="_blank" href= "<?php echo ENV_WEBROOT_FULL_URL ?>EvaluarInconsistencias/evaluar_inconsistencia/L,<?php echo $criterio;?>/<?php echo $date_start;?>/<?php echo $date_end;?>/<?php echo $unidadneg;?>/<?php echo $ciclo;?>/<?php echo $sector;?>/0/<?php echo $empleados['0']["id"];?>" ><i class="fa fa-user"></i> <?php echo  $empleados['0']['lecturista']; ?></a></td>

                                                    <td><?php echo $empleados['0']["lecturas_asignadas"];?></td>
                                                    <td><?php echo $empleados['0']["lecturas_descargadas"];?></td>
                                                    
                                                    <?php 
                                                    $fechainicio = strtotime($empleados['0']["fecha_hora_inicio"]);
                                                    $fechafin = strtotime($empleados['0']["fecha_hora_fin"]);
                                                    $tiempo_total = $fechafin - $fechainicio;
                                                    
                                                    $horas              = floor ( $tiempo_total / 3600 );
                                                    $minutes            = ( ( $tiempo_total / 60 ) % 60 );
                                                    $seconds            = ( $tiempo_total % 60 );
                                                    
                                                    $time['horas']      = str_pad( $horas, 2, "0", STR_PAD_LEFT );
                                                    $time['minutes']    = str_pad( $minutes, 2, "0", STR_PAD_LEFT );
                                                    $time['seconds']    = str_pad( $seconds, 2, "0", STR_PAD_LEFT );
                                                    
                                                    $TT               = implode( ':', $time );
                                                    ?>
                                                    <td><strong><?php echo "HI:&nbsp;".substr($empleados['0']["fecha_hora_inicio"],11,8).'<br>HF:&nbsp;'.substr($empleados['0']["fecha_hora_fin"],11,8).'<br>TT:&nbsp;'.$TT.'<br>TP:&nbsp;'.substr($empleados['0']["tiempo_promedio"],0,8);?></strong></td>
                                                    

                                                    <td style="font-weight: bold;" class="text-center"><?php echo $empleados['0']["lecturas_terminadas"];?>
                                                    </td>

                                                    <td class="text-center" style="font-weight: bold;"><?php echo $empleados['0']["lecturas_asignadas"]-$empleados['0']["lecturas_terminadas"];?></td>

                                                    <td>
                                                        <div class="progress progress-striped" title ='<?php echo  round(($empleados['0']['lecturas_terminadas'] * 100) / $empleados['0']['lecturas_asignadas'],2).'% '?> - <?php echo $empleados['0']['lecturas_terminadas'];?> <?php echo "Lecturas Terminadas" ?>'>
                                                          <div class="<?php if(round(($empleados['0']['lecturas_terminadas'] * 100) / $empleados['0']['lecturas_asignadas'],2) < '50'){ echo 'progress-bar progress-bar-danger';} else {echo 'progress-bar progress-bar-success';}?>" style="width: <?php echo  round(($empleados['0']['lecturas_terminadas'] * 100) / $empleados['0']['lecturas_asignadas'],2).'% '?>; color: black; font-weight: bold;"><?php echo  round(($empleados['0']['lecturas_terminadas'] * 100) / $empleados['0']['lecturas_asignadas'],2).'% '?>(<?php echo $empleados['0']['lecturas_terminadas'];?>)
                                                          </div>
                                                        </div>
                                                    </td>

                                                    <td class="text-center">
                                                        <?php                       
                                                        echo 'Originales: '.$inconsistencias_originales.'<br>Actuales: <span class="'.$style_inconcistencias_actuales.'">'.$inconsistencias_actuales.'</span>';?>
                                                    </td>
                                                    
                                                    <td>
                                                        <?php echo $empleados['0']['lecturas_evaluadas'];?>
                                                    </td>
                                                    
                                                    <td class="text-center">
                                                        <?php echo $empleados['0']['lecturas_consistentes'];?>
                                                    </td>
                                                    
                                                    <td class="text-center">
                                                        <?php 
                                                        $obs_total = $empleados['0']['obs_total'];
                                                        $obs_sin_lectura = $empleados['0']['obs_sin_lectura'];
                                                        $obs_con_lectura = $empleados['0']['obs_con_lectura'];
                                                        
                                                        
                                                        echo '<strong>T: '.$obs_total.'<br>SL: <span>'.$obs_sin_lectura.'</span> '.'<br>CL: <span>'.$obs_con_lectura.'</span> </strong>';?>
                                                    </td>
                                                                                        

                                                    <td class="text-center"><a class="btn btn-info btn-sm btn" target="_blank" href="<?php echo ENV_WEBROOT_FULL_URL;?>MonitorearLecturas/ajax_cargar_mapa/<?php echo $empleados['0']["id"];?>/<?php echo $criterio;?>/<?php echo $date_start;?>/<?php echo $date_end;?>/<?php echo $ciclo;?>/<?php echo $sector;?>"><i class="fa fa-globe"></i></td>
                                                </tr>
                                            <?php } ?>      
                                        </tbody>

                                        <tfoot>
                                            <tr>
                                            </tr>
                                        </tfoot>

                                </table>
                            </div>
                        </div>

                        </div>
                    </div>
                </div>
                </div>
            </div>
            </div>
         </div>
    </div>
    

<div id="arbol" class="page-aside app filters tree" >
    <div class="fixed nano nscroller has-scrollbar">
        <div class="content"> 
           <div class="header">
               <button class="navbar-toggle" data-target=".app-nav" data-toggle="collapse" type="button">
                  <span class="fa fa-chevron-down"></span>
               </button>          
               <h2 class="page-title">Filtros</h2>
               <p class="description">Criterios de Busqueda</p>
           </div>

           <div class="app-nav collapse" style="margin-bottom: 130px;">
                <div class="form-group">
                  <label class="control-label">Por :</label>
                  <input id="cbo_criterio" name="cbo_criterio" style="width:240px;" placeholder="Seleccione una opcion">
                </div>
                
                <div class="por_fecha_asignacion">
                    <div class="form-group" style= "width: 90%;" >
                      <label class="control-label">Por Fecha de Asignaci&oacute;n (Inicio) :</label>
                      <div class="input-group date datetime" data-min-view="2" data-date-format="yyyy-mm-dd"  style= "width: 50%;">
                        <input id="dpt_date_start" class="form-control" size="16" type="text" value="<?php echo date('Y-m-d');?>" readonly>
                        <span class="input-group-addon btn btn-primary"><span class="glyphicon glyphicon-th"></span></span>
                      </div>    
                    </div>

                    <div class="form-group" style= "width: 90%;" >
                      <label class="control-label">Por Fecha de Asignaci&oacute;n (Fin) :</label>
                      <div class="input-group date datetime" data-min-view="2" data-date-format="yyyy-mm-dd"  style= "width: 50%;">
                        <input id="dpt_date_end" class="form-control"  size="16" type="text" value="<?php echo date('Y-m-d');?>" readonly>
                        <span class="input-group-addon btn btn-primary"><span class="glyphicon glyphicon-th"></span></span>
                      </div>    
                    </div>
                </div>
                
                <div class="por_ubicacion" style="display:none;">
                    
                    
                    <div class="form-group">
                      <label class="control-label">Por Unidad Negocio:</label>              
                        <?php 
                                echo '<select class="" name="unidadneg" id="unidadneg">
                                <option value="0">Todos</option>';
                                                 //   var_dump($listar_unidadneg);exit;
                                foreach ($listar_unidadneg as $v){
                                echo '<option value='.$v['0']['unidadneg'].'>'.$v['0']['nombreunidadnegocio'].'</option>';
                                }
                                echo '</select>';   
                        ?>                  
                    </div>
                    
                    <div class="form-group">
                      <label class="control-label">Por Ciclo:<span class="ciclo_loading" style="display:none;"><img src="<?php echo ENV_WEBROOT_FULL_URL ?>/js/jquery.select2/select2-spinner.gif"></span></label>              
                        <?php 
                                echo '<select class="" name="id_ciclo" id="id_ciclo">
                                <option value="0">Todos</option>';
                                foreach ($listar_ciclo as $v){
                                    $str_ciclo_actual = '';
                                    foreach ($arr_ciclos_actuales as $ca){
                                        if($v['0']['idciclo']==$ca['0']['idciclo']){
                                            $str_ciclo_actual = ' (Actual)';
                                        }
                                    }
                                    echo '<option value='.$v['0']['idciclo'].'>'.$v['0']['idciclo'].' - '.$v['0']['nombciclo'].$str_ciclo_actual.'</option>';
                                }
                                echo '</select>';   
                        ?>                  
                    </div>

                    <div class="form-group">
                      <label class="control-label"><?php if($NEGOCIO == 'CIX'){echo 'Por Sector';}elseif ($NEGOCIO == 'TAC'){echo 'Por Zona';}else{echo 'Por Sector';}?><span class="sector_loading" style="display:none;"><img src="<?php echo ENV_WEBROOT_FULL_URL ?>/js/jquery.select2/select2-spinner.gif"></span></label>              
                        <?php 
                                echo '<select class="" name="monitoreo_id_sector" id="monitoreo_id_sector">
                                <option value="0">Todos</option>';
                                foreach ($listar_sector as $v){
                                echo '<option value='.$v['0']['sector'].'>'.$v['0']['sector'].'</option>';
                                }
                                echo '</select>';   
                        ?>                  
                    </div>
                </div>

                <button class='btn btn-primary' id='buscar-monitoreo'>Buscar</button>
            
            </div>
        </div>        
    </div>
</div>


<script type="text/javascript">                                                     
    require(['ajax/monitorear'],function(){
        /*Jquery Easy Pie Chart*/
          $('.epie-chart').easyPieChart({
            barColor: '#FD9C35',
            trackColor: '#EFEFEF',
            lineWidth: 7,
            animate: 600,
            size: 55,
            onStep: function(val){//Update current value while animation
              $("span", this.$el).html(parseInt(val) + "%");
            }
          });


        $(".arbol-head-lecturas").click(function(e){
          
                if ( $('.widget-monitoreo').hasClass('visible')) {

                    $('.floatHeader').css('width','100%');
                    $('.floatHeader').css('margin-top','0px');
                    $('.cl-mcont').css('margin-top','120px');
                    $('.widget-monitoreo').css("visibility", "visible");
                    $('.dato').css("visibility", "visible");
                    $('.widget-monitoreo').removeClass('visible');
                    $('.widget-monitoreo').addClass('invisible');

                }else{

                    $('.floatHeader').css('width','100%');
                    $('.floatHeader').css('margin-top','-80px');
                    $('.cl-mcont').css('margin-top','40px');
                    $('.dato').css("visibility", "visible");
                    $('.widget-monitoreo').removeClass('invisible');
                    $('.widget-monitoreo').addClass('visible');
                }            

            
        });

    });
     
</script>





