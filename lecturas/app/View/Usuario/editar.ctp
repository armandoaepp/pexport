<?php 
if (isset($id_usuario) && $id_usuario == 25 || $id_usuario == 131){
	$permiso_lectura = 'true';
}else {
	$permiso_lectura = 'true';
}
echo $this->element('menu',array('active'=>'monitorearlectura', 'open'=>'true', 'permiso_lectura' =>$permiso_lectura, 'id_usuario ' =>isset($id_usuario)?$id_usuario:null));
echo $this->element('Usuarios/tree');
?>

<div class="container-fluid" id="pcont">

	<div class="page-head">

		<ol class="breadcrumb">
			<li><button type="button"
					class="btn-xs btn-default arbol-top-lecturas">Arbol</button></li>
			<li><a href="<?php echo ENV_WEBROOT_FULL_URL ?>usuarios/listado">Listado
					de Usuarios</a></li>
			<li class="active">Nuevo</li>
		</ol>
	</div>

	<div class="cl-mcont">
		<div class="col-sm-12 col-md-12">
			<div class="block">

				<div class="content">
				
					<input type="hidden" name="data[Usuario][id]" id="data[Usuario][id]" value="<?php echo $obj_user['Usuario']['id'];?>">
				
					<div class="form-group">
						<label class="control-label">Nombre Usuario</label> 
						<input type="input" name="data[Usuario][usuario]" id="data[Usuario][usuario]" value="<?php echo $obj_user['Usuario']['nomusuario'];?>">
					</div>
					
					<div class="form-group">
						<label class="control-label">Clave</label> 
						<input type="input" name="data[Usuario][pass]" id="data[Usuario][pass]" value="">
					</div>
					
					<a class='btn btn-primary' id='grabar-usuario' href="<?php echo ENV_WEBROOT_FULL_URL ?>usuarios/grabar">Grabar</a>

				</div>
			</div>

		</div>
	</div>
</div>

</div>