<?php 
if (isset($id_usuario) && $id_usuario == 25 || $id_usuario == 131){
	$permiso_lectura = 'false';
}else {
	$permiso_lectura = 'true';
}
echo $this->element('menu',array('active'=>'usuarios', 'open'=>'false', 'permiso_lectura' =>$permiso_lectura, 'id_usuario ' =>isset($id_usuario)?$id_usuario:null));
echo $this->element('Usuarios/tree');
?>

<div class="container-fluid" id="pcont">

		<div class="page-head">
				
				<ol class="breadcrumb">
				  <li><button type="button" class="btn-xs btn-default arbol-top-lecturas" >Arbol</button></li>
				  <li class="active">Listado de Usuarios</li>
				</ol>
		</div>

	<div class="cl-mcont">
		<div class="col-sm-12 col-md-12" >
			<div class="block">
				<a class='btn btn-primary' id='nuevo-usuario' href="<?php echo ENV_WEBROOT_FULL_URL ?>usuarios/nuevo">Nuevo</a>


				 <div class="content">
		          <div class="tabla">
		            <div id="monit" class='farmsmall2'>
						<div class="header no-border">
							<h2>Listado de Usuarios</h2>
						</div>

						<div class="content no-padding">
							<table class="red table table-bordered dataTable" id = "list-monitorear-lecturistas">
								<thead>
									<tr>
										<th>#</th>
										<th>USUARIO</th>
										<th></th>
										<th></th>
									</tr>
								</thead>

								<tfoot>
						            <tr>
						            </tr>
						        </tfoot>
										        
								<tbody class="no-border-x">
								<?php 
								$i = 1;
								foreach ($arr_usuarios as $usuario){
								?>
									<tr>
										<td>
										<?php echo $i++;?> 
										</td>
										<td><?php echo $usuario['0']["nomusuario"];?></td>
										<td><a href="<?php echo ENV_WEBROOT_FULL_URL ?>usuarios/editar/<?php echo $usuario['0']["id"];?>">Editar</a></td>
										<td><a href="<?php echo ENV_WEBROOT_FULL_URL ?>usuarios/eliminar/<?php echo $usuario['0']["id"];?>">Eliminar</a></td>
									</tr>
								<?php } ?>		
								</tbody>

							</table>
						</div>
					</div>
				</div>
</div>
			</div>

			</div>
         </div>
    </div>
    
</div>