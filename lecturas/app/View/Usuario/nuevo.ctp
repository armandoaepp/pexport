<?php 
if ((isset($id_usuario) && $id_usuario == 25) || (isset($id_usuario) && $id_usuario == 131)){
	$permiso_lectura = 'false';
}else {
	$permiso_lectura = 'true';
}
echo $this->element('menu',array('active'=>'usuarios', 'open'=>'false', 'permiso_lectura' =>$permiso_lectura, 'id_usuario ' =>isset($id_usuario)?$id_usuario:null));
echo $this->element('Usuarios/tree');

?>

<div class="container-fluid" id="pcont">

	<div class="page-head">

		<ol class="breadcrumb">
			<li><button type="button"
					class="btn-xs btn-default arbol-top-lecturas">Arbol</button></li>
			<li><a href="<?php echo ENV_WEBROOT_FULL_URL ?>usuarios/listado">Listado
					de Usuarios</a></li>
			<li class="active">Nuevo</li>
		</ol>
	</div>

	<div class="cl-mcont">
		<div class="col-sm-12 col-md-12">
			<div class="block">

				<div class="content">
				<form action="<?php echo ENV_WEBROOT_FULL_URL ?>usuarios/grabar" method="post" accept-charset="utf-8">
					<div class="form-group">
						<label class="control-label">Nombre Usuario</label> 
						<input type="input" name="data[Usuario][usuario]" id="data[Usuario][usuario]" value="">
					</div>
					
					<div class="form-group">
						<label class="control-label">Clave</label> 
						<input type="input" name="data[Usuario][pass]" id="data[Usuario][pass]" value="">
					</div>
					 <?php 
        			  //print_r($listar_empleados);
        			  	echo '<select class="select2" name="data[Usuario][glomas_empleado_id]" id="data[Usuario][glomas_empleado_id]">';
        				foreach ($listar_empleados as $empleados){
        				echo '<option value='.$empleados['0']["id"].'>'.$empleados['0']["nombre"].' - '.$empleados['0']["nomusuario"].'</option>';
        				}
        				echo '</select>';	
        			?>
        			<p></p>
					<button type="submit" class="btn btn-primary">Crear Usuario</button>
					</form>
				</div>
			</div>

		</div>
	</div>
</div>

</div>