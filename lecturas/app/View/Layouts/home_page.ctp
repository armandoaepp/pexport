<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="en"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang="en"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang="en"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="en"> <!--<![endif]-->
<head>
<?php 
  //if(isset($obj_logged_user) && is_object($obj_logged_user) && $obj_logged_user->getID()){
  	echo $this->element('layout_head');
  //}else{
  //	echo $this->element('layout_head_public');
  //  }
  ?>
</head>

<body>
  <!-- Fixed navbar -->
  <div id="head-nav" class="navbar navbar-default navbar-fixed-top">
    <div class="container-fluid">
      <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
          <span class="fa fa-gear"></span>
        </button>
        <a class="navbar-brand" href="#"><span>Pexport S.A.C</span></a>
      </div>
      <div class="navbar-collapse collapse">
        <ul class="nav navbar-nav">
          <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">Arbol<b class="caret"></b></a>
            <ul class="dropdown-menu">
              <li><a href="#" onclick="ocultararbol()">Ocultar</a></li>
              <li><a href="#" onclick="mostrararbol()">Mostrar</a></li>
            </ul>
          </li>
        </ul>
    <ul class="nav navbar-nav navbar-right user-nav">
      <li class="dropdown profile_menu">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown"><img alt="Avatar" src="<?php echo ENV_WEBROOT_FULL_URL ?>images/avatar_yo2.jpg" /><span>Jonatan Sanchez</span> <b class="caret"></b></a>
        <ul class="dropdown-menu">
          <li><a href="#">Actividad</a></li>
          <li><a href="#">Mensajes</a></li>
          <li class="divider"></li>
          <li><a href="cerrar_sesion.php">Salir</a></li>
        </ul>
      </li>
    </ul>     
    <ul class="nav navbar-nav navbar-right not-nav">

      <li class="button dropdown">
        <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-globe"></i><span class="bubble">3</span></a>
        <ul class="dropdown-menu">
          <li>
            <div class="nano nscroller">
              <div class="content">
                <ul>
                  <li><a href="#"><i class="fa fa-cloud-upload info"></i><b>Distriluz Tecnico</b> Te ha enviado mensaje <span class="date">2 minutos atras.</span></a></li>
                  <li><a href="#"><i class="fa fa-male success"></i> <b>Usuario A</b> requiere aprobacion <span class="date">15 minutos atras.</span></a></li>
                  <li><a href="#"><i class="fa fa-bug warning"></i> <b>Distriluz Call Center</b> Modifico un Evento <span class="date">30 minutos atras.</span></a></li>
                </ul>
              </div>
            </div>
            <ul class="foot"><li><a href="#">Ver mensajes anteriores </a></li></ul>           
          </li>
        </ul>
      </li>
           
    </ul>

      </div><!--/.nav-collapse animate-collapse -->
    </div>
  </div>

	 <div id="cl-wrapper" class="fixed-menu">
    <div class="cl-sidebar">
      <div class="cl-toggle"><i class="fa fa-bars"></i></div>
      <div class="cl-navblock">
        <div class="menu-space">
          <div class="content">
            <div class="side-user">
              <div class="avatar"><img src="<?php echo ENV_WEBROOT_FULL_URL ?>images/avatar_yo.jpg" alt="Avatar" /></div>
              <div class="info">
                <a href="#">Jonatan Sanchez</a>
                <img src="<?php echo ENV_WEBROOT_FULL_URL ?>images/state_online.png" alt="Status" /> <span>Online</span>
              </div>
            </div>
            <ul class="cl-vnavigation">
              <li class="" id="madre" onclick="list(this)"><a href="#" onclick="showDash()"><i class="fa fa-home"></i><span>Dashboard</span></a></li>
              <li class=""><a href="#"><i class="fa fa-desktop"></i><span>Ordenes de Lectura</span></a>
                <ul class="sub-menu">
                  <li class="" onclick="list(this)"><a href="<?php echo ENV_WEBROOT_FULL_URL ?>ComlecOrdenlecturas/form">Importar Ordenes</a></li>
                  <li class="" onclick="list(this)"><a href="<?php echo ENV_WEBROOT_FULL_URL ?>ComlecOrdenlecturas/listarordenlecturas" >Asignar Ordenes</a></li>
                  <li class="" onclick="list(this)"><a href="#" onclick="showMon(2,'open')">Monitorear Ordenes</a></li>
				  <li class="" onclick="list(this)"><a href="#" onclick="showMon(2,'open')">Digitar Lecturas</a></li>
                  <li class="" onclick="list(this)"><a href="#" onclick="showEme();">Evaluar Inconsistencias</a></li>
                </ul>
              </li>
              <li><a href="#"><i class="fa fa-bar-chart-o"></i><span>Estadistica</span></a>
                <ul class="sub-menu">
                  <li class="" onclick="list(this)"><a href="#" onclick="showImp()">Reportes</a></li>
                </ul>
              </li>
              <li><a href="#"><i class="fa fa-briefcase"></i><span>Sistema</span></a>
                <ul class="sub-menu">
                  <li class="" onclick="list(this)"><a href="#" onclick="showMas1()">Maestros</a></li>
                  <li class="" onclick="list(this)"><a href="#" onclick="showUsu1()">Usuarios</a></li>
                  <li class="" onclick="list(this)"><a href="#" onclick="showEnl1()">Enlaces</a></li>
				  <li class="" onclick="list(this)"><a href="#" onclick="showEnl1()">Reglas de Consistencia</a></li>
                </ul>
              </li>
            </ul>
          </div>
        </div>
        <div class="text-right collapse-button" style="padding:7px 9px;">
          <input type="text" class="form-control search" placeholder="Search..." />
          <button id="sidebar-collapse" class="btn btn-default" style=""><i style="color:#fff;" class="fa fa-angle-left"></i></button>
        </div>
      </div>
    </div>
	
		<div class="container-fluid" id="pcont">
		<!-- CONTENT-->
         
			<?php 
				echo $this->fetch('content'); 
			?>
		<!-- /CONTENT -->
		</div> 
		
	</div>

	
	
<?php
	//echo $this->element('sql_dump'); 
?>
<?php echo $this->element('layout_footer');?>

<script type="text/javascript">
  var LHCChatOptions = {};

  LHCChatOptions.opt = {widget_height:340,widget_width:300,popup_height:520,popup_width:500,domain:'54.200.245.17'};
  (function() {
    
    var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
    var refferer = (document.referrer) ? encodeURIComponent(document.referrer.substr(document.referrer.indexOf('://')+1)) : '';
    var location  = (document.location) ? encodeURIComponent(window.location.href.substring(window.location.protocol.length)) : '';
    po.src = '//54.200.245.17/livehelperchat/lhc_web/index.php/esp/chat/getstatus/(click)/internal/(position)/bottom_right/(ma)/br/(top)/350/(units)/pixels/(leaveamessage)/true/(department)/1?r='+refferer+'&l='+location;
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
  })();
</script>

<?php echo $this->element('ga');?>
  </body>
</html>