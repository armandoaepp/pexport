<?php
App::import('Vendor', 'dompdf', true, array(), 'dompdf' . DS . 'dompdf_config.inc.php');
$dompdf = new DOMPDF();
$dompdf->load_html($this->fetch('content'));
$dompdf->set_paper("A4", "portrait");
$dompdf->render();
//$dompdf->stream('ReportExample_'.date('d_M_Y').'.pdf');
$dompdf->stream("joel.pdf", array("Attachment" => 0));