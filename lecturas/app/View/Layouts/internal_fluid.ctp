<div id="wrap">
	<a name="top"></a>
	<header id="header" class="main navbar navbar-fixed-top" role="banner">
		<div class="navbar-inner" id="container_top_bar">
		<!-- ajax will load the top bar in here -->
			<div class="container-fluid">
				<div class="brand">
					<h1><a href="/"><span class="hidden">Living Alpha</span> <img alt="Dream It... Live It... Preserve It..." src="<?php echo ENV_WEBROOT; ?>/img/logo-living-alpha.png" style="width:150px;height:49px;"></a></h1>
				</div>

			</div>
		</div>
	</header>

	<section class="main" role="main">
		<div class="container-fluid" id="container_main">
			<div class="signup span9 offset1 signup-offset container-loading">
				<?php echo $this->Html->image('loading_white_small.gif');?> <?php echo __('Loading your Alpha World, please wait.') ?>
			</div>
		</div>
	</section>
	<div id="push"></div>
</div>

<footer id="footer" class="main navbar-fixed-bottom">
<?php echo $this->element('public_footer');?>
</footer>

<?php
	echo $this->element('sql_dump'); 
	echo $this->MySession->flash('flash', array('element' => 'alertify'));	
	echo $this->element('google-analytics'); 
?>
