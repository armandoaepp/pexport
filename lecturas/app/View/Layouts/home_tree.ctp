<?php 
if ((isset($id_usuario) && $id_usuario == 25) || (isset($id_usuario) && $id_usuario == 131)){
  //$id_usuario 25 Usuario 1
  $modulo_permiso = array("dashboard","monitorearlectura",'monitoreargps','extranet','rep_historico','rep_comparar_periodo','rep_consumo_por_dia_y_ubicacion','rep_detallado_ciclo','cronograma');
}elseif (isset($id_usuario) && $id_usuario == 69){//jsanchez
  $modulo_permiso = array("dashboard","evaluarinconsistencia", "extranet", "importar", "listarciclosector", "monitorearlectura",'monitoreargps', "digitarlecturas",'usuarios','rep_observaciones','rep_personal_para_lectura','rep_historico','rep_ordenes_por_observacion','rep_mineria','rep_mineria2','rep_comparar_periodo','rep_consumo_por_dia_y_ubicacion','rep_suministros_cronograma','rep_detallado_ciclo','reporte_ordenes_historico','importarlecturas','cronograma','fac_wizard','fac_pliegos','fac_list_facturas');
}elseif (isset($id_usuario) && $id_usuario >= 132 && $id_usuario <= 141){//digitadores trux
  $modulo_permiso = array("digitarlecturas");
}else {
  $modulo_permiso = array("dashboard","evaluarinconsistencia", "extranet", "importar", "listarciclosector", "monitorearlectura",'monitoreargps', "digitarlecturas",'usuarios','rep_observaciones','rep_personal_para_lectura','rep_historico','rep_ordenes_por_observacion','rep_comparar_periodo','rep_consumo_por_dia_y_ubicacion','rep_suministros_cronograma','rep_detallado_ciclo','reporte_ordenes_historico','importarlecturas','cronograma','fac_wizard','fac_pliegos','fac_list_facturas');
}

  ?>
  
<!DOCTYPE html>
<html lang="es">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="D&D ANALYTICS">
  <meta name="author" content="D&D ANALYTICS">
  <?php echo $this->element('environment_variables'); ?>
  <link rel="shortcut icon" href="<?php echo ENV_WEBROOT_FULL_URL ?>images/favicon.png">

  <title>Pexport</title>
  
  <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,400italic,700,800' rel='stylesheet' type='text/css'>
  <link href='http://fonts.googleapis.com/css?family=Raleway:300,200,100' rel='stylesheet' type='text/css'>

  <!-- Bootstrap core CSS -->
  <link href="<?php echo ENV_WEBROOT_FULL_URL ?>js/bootstrap/dist/css/bootstrap.css" rel="stylesheet">
  <link rel="stylesheet" type="text/css" href="<?php echo ENV_WEBROOT_FULL_URL ?>js/bootstrap.slider/css/slider.css" />
  <link rel="stylesheet" type="text/css" href="<?php echo ENV_WEBROOT_FULL_URL ?>js/jquery.gritter/css/jquery.gritter.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo ENV_WEBROOT_FULL_URL ?>css/jquery.jgrowl.css" />
  <link rel="stylesheet" href="<?php echo ENV_WEBROOT_FULL_URL ?>fonts/font-awesome-4/css/font-awesome.min.css">

  <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!--[if lt IE 9]>
    <script src="../../assets/js/html5shiv.js"></script>
    <script src="../../assets/js/respond.min.js"></script>
  <![endif]-->
  <link rel="stylesheet" type="text/css" href="<?php echo ENV_WEBROOT_FULL_URL ?>js/jquery.nanoscroller/nanoscroller.css" />
  <link rel="stylesheet" type="text/css" href="<?php echo ENV_WEBROOT_FULL_URL ?>js/jquery.easypiechart/jquery.easy-pie-chart.css" />
  <link rel="stylesheet" type="text/css" href="<?php echo ENV_WEBROOT_FULL_URL ?>js/bootstrap.switch/bootstrap-switch.css" />
  <link rel="stylesheet" type="text/css" href="<?php echo ENV_WEBROOT_FULL_URL ?>js/bootstrap.datetimepicker/css/bootstrap-datetimepicker.min.css" />
  <link rel="stylesheet" type="text/css" href="<?php echo ENV_WEBROOT_FULL_URL ?>js/jquery.select2/select2.css" />
  <link rel="stylesheet" type="text/css" href="<?php echo ENV_WEBROOT_FULL_URL ?>js/jquery.niftymodals/css/component.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo ENV_WEBROOT_FULL_URL ?>js/jquery.magnific-popup/dist/magnific-popup.css" />
  <link rel="stylesheet" type="text/css" href="<?php echo ENV_WEBROOT_FULL_URL ?>js/bootstrap.summernote/dist/summernote.css" />  
    <link href="<?php echo ENV_WEBROOT_FULL_URL ?>js/jquery.datatables/extras/TableTools/media/css/TableTools.css" media="all" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" type="text/css" href="<?php echo ENV_WEBROOT_FULL_URL ?>js/intro.js/introjs.css" />
  <!-- Custom styles for this template -->
  <link href="<?php echo ENV_WEBROOT_FULL_URL ?>css/style.css" rel="stylesheet" />
  
  <link href="<?php echo ENV_WEBROOT_FULL_URL ?>js/jquery.icheck/skins/square/blue.css" rel="stylesheet">

  
  <link rel="stylesheet" href="//code.jquery.com/ui/1.11.0/themes/smoothness/jquery-ui.css">
  <link rel="stylesheet" href="<?php echo ENV_WEBROOT_FULL_URL ?>js/preloader/css/normalize.css">
  <link rel="stylesheet" href="<?php echo ENV_WEBROOT_FULL_URL ?>js/preloader/css/main.css">
  <script src="<?php echo ENV_WEBROOT_FULL_URL ?>js/preloader/js/vendor/modernizr-2.6.2.min.js"></script>
  
  <script src="<?php echo ENV_WEBROOT_FULL_URL ?>js/jquery.js"></script>
  <script type="text/javascript" src="<?php echo ENV_WEBROOT_FULL_URL ?>js/bootstrap-fileupload.js"></script>
  <script type="text/javascript" src="<?php echo ENV_WEBROOT_FULL_URL ?>js/jquery.gritter/js/jquery.gritter.min.js"></script>
  <script type="text/javascript" src="<?php echo ENV_WEBROOT_FULL_URL ?>js/jquery.nanoscroller/jquery.nanoscroller.js"></script>
  <script type="text/javascript" src="<?php echo ENV_WEBROOT_FULL_URL ?>js/behaviour/general.js"></script>
  <script type="text/javascript" src="<?php echo ENV_WEBROOT_FULL_URL ?>js/jquery.sparkline/jquery.sparkline.min.js"></script>
  <script type="text/javascript" src="<?php echo ENV_WEBROOT_FULL_URL ?>js/jquery.easypiechart/jquery.easy-pie-chart.js"></script>  
  <script type="text/javascript" src="<?php echo ENV_WEBROOT_FULL_URL ?>js/jquery.nestable/jquery.nestable.js"></script>
  <script type="text/javascript" src="<?php echo ENV_WEBROOT_FULL_URL ?>js/bootstrap.switch/bootstrap-switch.min.js"></script>
  <script type="text/javascript" src="<?php echo ENV_WEBROOT_FULL_URL ?>js/bootstrap.datetimepicker/js/bootstrap-datetimepicker.min.js"></script>
  <script src="<?php echo ENV_WEBROOT_FULL_URL ?>js/jquery.select2/select2.min.js" type="text/javascript"></script>
  <script src="<?php echo ENV_WEBROOT_FULL_URL ?>js/skycons/skycons.js" type="text/javascript"></script>
  <script type="text/javascript" src="<?php echo ENV_WEBROOT_FULL_URL ?>js/jquery.niftymodals/js/jquery.modalEffects.js"></script> 
  <script type="text/javascript" src="<?php echo ENV_WEBROOT_FULL_URL ?>js/bootstrap.summernote/dist/summernote.min.js"></script>
  <script  type="text/javascript" src="<?php echo ENV_WEBROOT_FULL_URL ?>js/intro.js/intro.js"></script>
  <script type="text/javascript" src="<?php echo ENV_WEBROOT_FULL_URL ?>js/tables/jquery.dataTables.columnFilter.js"></script>
  <script src="<?php echo ENV_WEBROOT_FULL_URL ?>js/preloader/js/main.js"></script>
  <script type="text/javascript">


    function sintesis(){
        $('#sidebar-collapse').click();
      }

    function tech(){

       var navegador = navigator.userAgent;
        if (navigator.userAgent.indexOf('MSIE') !=-1) {
          /*
          alert('está usando Internet Explorer ...');
          */
        } else if (navigator.userAgent.indexOf('Firefox') !=-1) {

        } else if (navigator.userAgent.indexOf('Chrome') !=-1) {
          /*
          alert('está usando Google Chrome ...');
          */
          var camb = $('#monit');
          camb.removeClass('farmsmall2');
          camb.addClass('farmsmall2');
           
          var camb2 = $('#asig');
          camb2.removeClass('farmsmall2');
          camb2.addClass('farmsmall2');

          var camb3 = $('#asig2');
          camb3.removeClass('farmsmall2');
          camb3.addClass('farmsmall2');

          var camb4 = $('#digitar');
          camb4.removeClass('farmsmall2');
          camb4.addClass('farmsmall2');

          var camb5 = $('#inco');
          camb5.removeClass('farmsmall2');
          camb5.addClass('farmsmall2');


        } else if (navigator.userAgent.indexOf('Opera') !=-1) {
          alert('está usando Opera ...');
        } else {
          /*
          alert('Debes estar usando Internet Explorer...');
          
          var elemento = document.querySelector('#monit');
          elemento.className = "farmsmall";
          var elementoo = document.querySelector('#asig');
          elementoo.className = "farmsmall";  
          */
          var camb = $('#monit');
          camb.removeClass('farmsmall2');
          camb.addClass('farmsmall');
           
          var camb2 = $('#asig');
          camb2.removeClass('farmsmall2');
          camb2.addClass('farmsmall');

          var camb3 = $('#asig2');
          camb3.removeClass('farmsmall2');
          camb3.addClass('farmsmall');

          var camb4 = $('#digitar');
          camb4.removeClass('farmsmall2');
          camb4.addClass('farmsmalldigi');

          var camb5 = $('#inco');
          camb5.removeClass('farmsmall2');
          camb5.addClass('farmsmallinco');

 
        }
    }


    $(document).ready(function(){
      //initialize the javascript
      App.init();
      //App.dashBoard();
      tech();

      sintesis();    
      introJs().setOption('showBullets', false).start();
      
      $('.image-zoom').magnificPopup({ 
        type: 'image',
        mainClass: 'mfp-with-zoom', // this class is for CSS animation below
        zoom: {
          enabled: true, // By default it's false, so don't forget to enable it
          duration: 300, // duration of the effect, in milliseconds
          easing: 'ease-in-out', // CSS transition easing function 
          opener: function(openerElement) {
            var parent = $(openerElement);
            return parent;
          }
        }
      });
     


    $('.md-trigger').modalEffects();
    
      $('label.tree-toggler').click(function () {
        var icon = $(this).children(".fa");
          if(icon.hasClass("fa-folder-o")){
            icon.removeClass("fa-folder-o").addClass("fa-folder-open-o");
          }else{
            icon.removeClass("fa-folder-open-o").addClass("fa-folder-o");
          }        
          
        $(this).parent().children('ul.tree').toggle(300,function(){
          $(this).parent().toggleClass("open");
          $(".tree .nscroller").nanoScroller({ preventPageScrolling: true });
        });
        
      });

      $('input').iCheck({
          checkboxClass: 'icheckbox_square-blue checkbox',
          radioClass: 'iradio_square-blue'
        });

    });
  </script>

<!-- Bootstrap core JavaScript
  ================================================== -->
  <!-- Placed at the end of the document so the pages load faster -->
  <script src="<?php echo ENV_WEBROOT_FULL_URL ?>js/behaviour/voice-commands.js"></script>
  <script src="<?php echo ENV_WEBROOT_FULL_URL ?>js/bootstrap/dist/js/bootstrap.min.js"></script>
  <script src="<?php echo ENV_WEBROOT_FULL_URL ?>js/jquery.ui/jquery-ui.js" type="text/javascript"></script>
  <script type="text/javascript" src="<?php echo ENV_WEBROOT_FULL_URL ?>js/jquery.flot/jquery.flot.js"></script>
  <script type="text/javascript" src="<?php echo ENV_WEBROOT_FULL_URL ?>js/jquery.flot/jquery.flot.pie.js"></script>
  <script type="text/javascript" src="<?php echo ENV_WEBROOT_FULL_URL ?>js/jquery.flot/jquery.flot.resize.js"></script>
  <script type="text/javascript" src="<?php echo ENV_WEBROOT_FULL_URL ?>js/jquery.flot/jquery.flot.labels.js"></script>
  <script type="text/javascript" src="http://www.flotcharts.org/flot/jquery.flot.time.js"></script>
  <script type="text/javascript" src="<?php echo ENV_WEBROOT_FULL_URL ?>js/jquery.magnific-popup/dist/jquery.magnific-popup.min.js"></script>
  <script src="<?php echo ENV_WEBROOT_FULL_URL ?>js/bootstrap.slider/js/bootstrap-slider.js" type="text/javascript"></script>
  <script type="text/javascript" src="<?php echo ENV_WEBROOT_FULL_URL ?>js/jquery.datatables/jquery.datatables.min.js"></script>
  <script type="text/javascript" src="<?php echo ENV_WEBROOT_FULL_URL ?>js/jquery.datatables/bootstrap-adapter/js/datatables.js"></script>
  <!-- <script  src="<?php /*echo ENV_WEBROOT_FULL_URL */?>js/jquery.datatables/extras/ColVis/media/js/ColVis.js" type="text/javascript"></script> -->
  <script src="<?php echo ENV_WEBROOT_FULL_URL ?>js/jquery.datatables/extras/TableTools/media/js/TableTools.js" type="text/javascript"></script>
  <script src="<?php echo ENV_WEBROOT_FULL_URL ?>js/jquery.datatables/extras/TableTools/media/js/ZeroClipboard.js" type="text/javascript"></script>
  <link rel="stylesheet" type="text/css" href="<?php echo ENV_WEBROOT_FULL_URL ?>js/fancyapps/source/jquery.fancybox.css?v=2.1.5" media="screen" />
  <link rel="stylesheet" type="text/css" href="<?php echo ENV_WEBROOT_FULL_URL ?>js/loadmask/jquery.loadmask.css" media="screen" />
  <script type="text/javascript" src="<?php echo ENV_WEBROOT_FULL_URL ?>js/jquery.icheck/icheck.min.js"></script>
  <?php echo $this->Html->script('require');?>
  <script type="text/javascript">
  require.config({
    baseUrl: ENV_WEBROOT_FULL_URL+"js",
    paths: {
    },
    waitSeconds: 15,
    packages: [
        {
        name: "jquery.datatables",
        main: 'bootstrap-adapter/js/datatables'
      },
      {
        name: "fancyapps",
        main: 'source/jquery.fancybox'
      },
      {
        name: "loadmask",
        main: 'jquery.loadmask.min'
      }
    ]
  });
  require(['internal'],function(){});
  </script>

</head>

<body>

 <!-- Fixed navbar -->
  <div id="head-nav" class="navbar navbar-default navbar-fixed-top" style="zoom:1; <?php if($NEGOCIO == 'TAC'){echo 'background-color:#d14836;';}?>">
    <div class="container-fluid" >

      <div class="navbar-collapse collapse">

          <style type="text/css">

            .navbar{
              background-color:  #555 !important;
              color: #fff !important;
            }

            .navbar li:hover, {
              background-color:  #444 !important;
              color: #fff !important;

            }

            .navbar-default .navbar-nav > .open > a, .navbar-default .navbar-nav > .open > a:hover, .navbar-default .navbar-nav > .open > a:focus {
              background-color:  #333 !important;
              color: #fff !important;
            }

            .navbar a:focus {
              background-color:  #333 !important;
              color: #fff !important;

            }

            .dropdown-menu > li > a:hover, .dropdown-menu li > a:hover {
              background-color:  #333 !important;
              color: #fff 
            }


            .navbar-default .navbar-nav > .open > a:hover,.navbar-nav > .open > a:focus{
              background-color:  #333 !important;
              color: #fff !important;
            }

            .dropdown-menu > .active > a, .dropdown-menu > .active > a:hover, .dropdown-menu > .active > a:focus{
               color: #fff !important;  
               background-color:  #333 !important;
            }

            .dropdown-menu > li.active {
               color: #fff !important;  
               background-color:  #333 !important;
            }

            .gallery-cont .item .over {
              top: 0;
              opacity: 0;
              position: absolute;
              height: 100%;
              width: 100%;
              background: rgba(0, 0, 0, 0.5) !important;
              transition: opacity 300ms ease;
              -webkit-transition: opacity 300ms ease;
              cursor: auto;
            }

             .dropdown-menu  li a span {
              margin-left:15px !important;
            }

            .navbar-default .navbar-brand  {
              background-image: none;
              font-weight: bold;
            }


          </style>

          <ul class="nav navbar-nav">
                <!--
                <li class="" id="madre" onclick="list(this)">
                  <a href="<?php echo ENV_WEBROOT_FULL_URL ?>home" onclick="showDash()"><i class="fa fa-home"></i><span> Home</span></a>
                </li>
                <?php //if(in_array("dashboard", $modulo_permiso)){ ?>
                  <li class="" id="" onclick="list(this)">
                    <a href="<?php echo ENV_WEBROOT_FULL_URL ?>dashboard" onclick="showDash()"><i class="fa fa-tachometer"></i><span> Dashboard</span></a>
                  </li>
                <?php //}?>
                -->
                <li class="dropdown">
                  <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                    <i class="fa fa-desktop"></i><span> Opciones del sistema </span><b class="caret"></b>
                  </a>

                  <ul class="dropdown-menu"  >

                    <!--<?php //if(in_array("importar", $modulo_permiso)){ ?>-->
                    <li class="">
                      <a href="<?php echo ENV_WEBROOT_FULL_URL ?>ComlecOrdenlecturas/form">Upload OT</a>
                    </li>
                     <!--<?php //} ?>-->

                     <!--<?php // if(in_array("listarciclosector", $modulo_permiso)){ ?> -->
                    <li class="">
                      <a href="<?php echo ENV_WEBROOT_FULL_URL ?>ComlecOrdenlecturas/listar_asignacion" onclick="sintesis()">Asignar OT</a>
                    </li>
                     <!--<?php // } ?>-->

                     <!--<?php // if(in_array("monitorearlectura", $modulo_permiso)){ ?> -->
                    <li class="">
                      <a href="<?php echo ENV_WEBROOT_FULL_URL ?>MonitorearLecturas/monitorear_lectura">Ver Avance</a>
                    </li>
                     <!--<?php // } ?>-->

                     <!--<?php // if(in_array("monitoreargps", $modulo_permiso)){ ?> -->
                    <li class="">
                      <a href="<?php echo ENV_WEBROOT_FULL_URL ?>MonitorearLecturas/mapa_monitoreo">Ver GPS</a>
                    </li>
                     <!--<?php // } ?>-->

                     <!--<?php // if(in_array("digitarlecturas", $modulo_permiso)){ ?> -->
                    <li class="">
                      <a href="<?php echo ENV_WEBROOT_FULL_URL ?>ComlecOrdenlecturas/digitarlecturas">Digitar Lecturas</a>
                    </li>
                     <!--<?php // } ?>-->

                     <!--<?php // if(in_array("evaluarinconsistencia", $modulo_permiso)){ ?>-->
                    <li class="">
                      <a href="<?php echo ENV_WEBROOT_FULL_URL ?>EvaluarInconsistencias/evaluar_inconsistencia">Inconsistencias</a>
                    </li>
                     <!--<?php // } ?>-->

                     <!--<?php // if(in_array("extranet", $modulo_permiso)){ ?> -->
                    <li class="">
                      <a href="<?php echo ENV_WEBROOT_FULL_URL ?>EvaluarInconsistencias/extranet">Extranet</a>
                    </li>
                     <!--<?php // } ?>-->

                     <!--<?php // if(in_array("importarlecturas", $modulo_permiso)){ ?> -->
					<li class="<?php //if($active=='rep_historico'){echo 'active';} ?>" >
                      <a href="<?php echo ENV_WEBROOT_FULL_URL ?>reportes/reporte_historico">En el Tiempo</a>
                    </li>
					<li class="<?php //if($active=='rep_observaciones'){echo 'active';} ?>" >
                      <a href="<?php echo ENV_WEBROOT_FULL_URL ?>dashboard/reporte_observaciones">Reporte de Codigos</a>
                    </li>
					<li class="<?php //if($active=='rep_ordenes_por_observacion'){echo 'active';} ?>" >
                      <a href="<?php echo ENV_WEBROOT_FULL_URL ?>reportes/reporte_ordenes_por_observacion">Reporte de codigos avanzado</a>
                    </li>
					<li class="<?php //if($active=='reporte_ordenes_historico'){echo 'active';} ?>" >
                      <a href="<?php echo ENV_WEBROOT_FULL_URL ?>reportes/ordenes_historico">Base Datos General</a>
                    </li>
                     <!--<?php // } ?>-->
                  </ul>
                </li>

                <!--
                <li class="dropdown">

                  <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-bar-chart-o"></i><span> Reportes <b class="caret"></b></span></a>

                  <ul  class="dropdown-menu" <?php //if(isset($open_report) && $open_report=='true'){echo 'style="display: block;"';}else{ echo 'style="display: none;"';} ?> >

                    <?php //if(in_array("rep_comparar_periodo", $modulo_permiso)){ ?> 
                    <li class="<?php //if($active=='rep_comparar_periodo'){echo 'active';} ?>" >
                      <a href="<?php //echo ENV_WEBROOT_FULL_URL ?>dashboard/comparar_periodo">Comparar Periodos</a>
                    </li>
                    <?php //} ?>

                    <?php //if(in_array("rep_observaciones", $modulo_permiso)){ ?>
                    <li class="<?php //if($active=='rep_observaciones'){echo 'active';} ?>" >
                      <a href="<?php //echo ENV_WEBROOT_FULL_URL ?>dashboard/reporte_observaciones">Reporte de Observaciones</a>
                    </li>
                    <?php //} ?>

                    <?php //if(in_array("rep_personal_para_lectura", $modulo_permiso)){ ?> 
                    <li class="<?php //if($active=='rep_personal_para_lectura'){echo 'active';} ?>" >
                      <a href="<?php //echo ENV_WEBROOT_FULL_URL ?>ComlecOrdenlecturas/listar_personal_para_lecturas">Reporte de Personal para Lecturas</a>
                    </li>
                    <?php //} ?>

                    <?php //if(in_array("rep_historico", $modulo_permiso)){ ?> 
                    <li class="<?php //if($active=='rep_historico'){echo 'active';} ?>" >
                      <a href="<?php //echo ENV_WEBROOT_FULL_URL ?>reportes/reporte_historico">Hist&oacute;rico por Suministro</a>
                    </li>
                    <?php //} ?>

                    <?php //if(in_array("rep_ordenes_por_observacion", $modulo_permiso)){ ?> 
                    <li class="<?php //if($active=='rep_ordenes_por_observacion'){echo 'active';} ?>" >
                      <a href="<?php //echo ENV_WEBROOT_FULL_URL ?>reportes/reporte_ordenes_por_observacion">Reporte de Ordenes por Observaci&oacute;n</a>
                    </li>
                    <?php //} ?>

                    <?php //if(in_array("rep_mineria", $modulo_permiso)){ ?> 
                    <li class="<?php //if($active=='rep_mineria'){echo 'active';} ?>" >
                      <a href="<?php //echo ENV_WEBROOT_FULL_URL ?>reportes/mineria">Reporte Mineria</a>
                    </li>
                    <?php //} ?>

                    <?php //if(in_array("rep_mineria2", $modulo_permiso)){ ?> 
                    <li class="<?php //if($active=='rep_mineria2'){echo 'active';} ?>" >
                      <a href="<?php //echo ENV_WEBROOT_FULL_URL ?>reportes/mineria2">Reporte Mineria 2</a>
                    </li>
                    <?php //} ?>

                    <?php //if(in_array("rep_consumo_por_dia_y_ubicacion", $modulo_permiso)){ ?> 
                    <li class="<?php //if($active=='rep_consumo_por_dia_y_ubicacion'){echo 'active';} ?>" >
                      <a href="<?php //echo ENV_WEBROOT_FULL_URL ?>dashboard/resumen_consumo_por_dia_y_ubicacion">Reporte de Consumo por Día y Ubicación</a>
                    </li>
                    <?php //} ?>

                    <?php //if(in_array("rep_suministros_cronograma", $modulo_permiso)){ ?> 
                    <li class="<?php //if($active=='rep_suministros_cronograma'){echo 'active';} ?>" >
                      <a href="<?php //echo ENV_WEBROOT_FULL_URL ?>reportes/resumen_suministros_cronograma">Reporte de Suministros Cronograma</a>
                    </li>
                    <?php //} ?>

                    <?php //if(in_array("rep_detallado_ciclo", $modulo_permiso)){ ?> 
                    <li class="<?php //if($active=='rep_detallado_ciclo'){echo 'active';} ?>" >
                      <a href="<?php //echo ENV_WEBROOT_FULL_URL ?>dashboard/resumen_por_ciclos?ajax=false">Reporte Detallado por Ciclo</a>
                    </li>
                    <?php //} ?>

                    <?php //if(in_array("reporte_ordenes_historico", $modulo_permiso)){ ?> 
                    <li class="<?php //if($active=='reporte_ordenes_historico'){echo 'active';} ?>" >
                      <a href="<?php //echo ENV_WEBROOT_FULL_URL ?>reportes/ordenes_historico">Reporte Ordenes Historico</a>
                    </li>
                    <?php //} ?>

                  </ul>
                </li>
                <li class="dropdown">
                  <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-briefcase"></i><span> Sistema</span> <b class="caret"></b></a>
                  <ul  class="dropdown-menu" <?php //if(isset($open_report) && $open_report=='true'){echo 'style="display: block;"';}else{ echo 'style="display: none;"';} ?>>
                    <li class="" onclick="list(this)"><a href="#" onclick="showMas1()">Maestros</a></li>
                   
                        <?php //if(in_array("usuarios", $modulo_permiso)){ ?> 
                        <li class="<?php //if($active=='usuarios'){echo 'active';} ?>" >
                          <a href="<?php //echo ENV_WEBROOT_FULL_URL ?>Usuarios/listado">Usuario</a></li>
                        <?php //} ?>

                    <li class="" onclick="list(this)"><a href="#" onclick="showEnl1()">Enlaces</a></li>
                    <li class="" onclick="list(this)"><a href="#" onclick="showEnl1()">Reglas de Consistencia</a></li>

                  </ul>
                </li>

                <li class="dropdown"> <a href="#"><i class="fa fa-list-alt"></i><span>Facturaci&oacute;n</span></a>
                  <ul  class="dropdown-menu" <?php //if(isset($open_fac) && $open_fac=='true'){echo 'style="display: block;"';}else{ echo 'style="display: none;"';} ?>>
                    <li class="" onclick="list(this)">
                      <a href="<?php //echo ENV_WEBROOT_FULL_URL ?>Facturacion/">Home</a>
                    </li>
                    <?php //if(in_array("fac_wizard", $modulo_permiso)){ ?> <li class="<?php //if($active=='fac_wizard'){echo 'active';} ?>" ><a href="<?php //echo ENV_WEBROOT_FULL_URL ?>Facturacion/wizard">Wizard</a></li><?php //} ?>
                    <?php //if(in_array("fac_pliegos", $modulo_permiso)){ ?> <li class="<?php //if($active=='fac_pliegos'){echo 'active';} ?>" ><a href="<?php //echo ENV_WEBROOT_FULL_URL ?>Facturacion/pliegos">Pliegos</a></li><?php //} ?>
                    <li class="" onclick="list(this)">
                      <a href="<?php //echo ENV_WEBROOT_FULL_URL ?>Facturacion/simulacion">Simular</a>
                    </li>

                    <?php //if(in_array("fac_list_facturas", $modulo_permiso)){ ?> 
                    <li class="<?php //if($active=='fac_list_facturas'){echo 'active';} ?>" >
                      <a href="<?php //echo ENV_WEBROOT_FULL_URL ?>Facturacion/list_facturas">Facturas Emitidas</a>
                    </li>
                    <?php //} ?>
                  </ul>
                </li>
               -->

              </ul>

               <ul class="nav navbar-nav navbar-right user-nav">
                   <div class="navbar-header" >
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                      <span class="fa fa-gear"></span>
                    </button>
                    <a class="navbar-brand" href="<?php echo ENV_WEBROOT_FULL_URL ?>home"><span>Pexport <!--- <?php //echo CakeSession::read('NEGOCIO_empresa');?> --></span></a>
                  </div>

                  <li class="" id="" onclick="list(this)" style="background-color:#444;">
                       <a href="  <?php echo ENV_WEBROOT_FULL_URL ?>/Usuarios/logout" > <i class="fa fa-close" ></i>  Cerrar</a></li>
                  </li>
              </ul>    


      </div><!--/.nav-collapse animate-collapse -->
    </div>
  </div>


<div id="cl-wrapper" class="fixed-menu">
     <!-- CONTENT-->
      <?php 
        echo $this->fetch('content'); 
      ?>
  <!-- /CONTENT --> 
</div>

  <!--Prueba-->

  <script type="text/javascript">

      if(navigator.appName.indexOf("MSIE")!=-1){
        //console.log("ie11");
      }else{ 
        function UpdateTableHeaders3() {
            //yeah, he's using IE
                  
            $("div.divTableWithFloatingHeader3").each(function() {
                var originalHeaderRow = $(".tableFloatingHeaderOriginal", this);
                var floatingHeaderRow = $(".tableFloatingHeader3", this);
                var offset = $(this).offset();
                var scrollTop = $(window).scrollTop();

                var navegador = navigator.userAgent;
                var inicioScroll = scrollTop+ 110;
                var marginheader = 95;
                if (navigator.userAgent.indexOf('MSIE') !=-1) {
                 
                } else if (navigator.userAgent.indexOf('Firefox') !=-1) {

                  if (navigator.platform === "Android"){  
                    inicioScroll = scrollTop + 110;
                    marginheader = 95; 
                  }
                  if( $('.widget-monitoreo').hasClass('visible') ){
                    inicioScroll = scrollTop + 70 //30  --->-30 + 140
                    marginheader = 105; //55  -> 95
                  }
                  else { 
                    inicioScroll = scrollTop + 70; //30  --->-30 + 140
                    marginheader = 188; //55  -> 95
                  }

                } else if (navigator.userAgent.indexOf('Chrome') !=-1) {

                  if (navigator.platform === "Android"){  
                    inicioScroll = scrollTop + 110;
                     marginheader = 95; 
                  }

                  if( $('.widget-monitoreo').hasClass('visible') ){
                    inicioScroll = scrollTop + 60 //30  --->-30 + 140
                    marginheader = 100; //55  -> 95
                  }
                  else { 
                    inicioScroll = scrollTop + 160; //30  --->-30 + 140
                    marginheader = 200; //55  -> 95
                  }

                } else if (navigator.userAgent.indexOf('Opera') !=-1) {

                  if (navigator.platform === "Android"){  
                    inicioScroll = scrollTop + 110;
                    marginheader = 95; 
                  }else { 
                    inicioScroll = scrollTop + 110;
                    marginheader = 125;
                  }

                } else {
                
                }

                if (( inicioScroll > offset.top ) && (scrollTop < offset.top + $(this).height())) {
                    floatingHeaderRow.css("visibility", "visible");
                    floatingHeaderRow.css("top", (Math.min(scrollTop - offset.top, $(this).height() - floatingHeaderRow.height()))+ marginheader  + "px");

                    // Copy cell widths from original header
                    $("th", floatingHeaderRow).each(function(index) {
                        var cellWidth = $("th", originalHeaderRow).eq(index).css('width');
                        $(this).css('width', cellWidth);
                    });

                    // Copy row width from whole table
                    floatingHeaderRow.css("width", $(this).css("width"));
                }
                else {
                    floatingHeaderRow.css("visibility", "hidden");
                    floatingHeaderRow.css("top", "0px");
                    floatingHeaderRow.css("background-color", "#F8F8F8");
                }
            });
        }

        $(document).ready(function() {

                $("table.tableWithFloatingHeader3").each(function() {
                  $(this).wrap("<div class=\"divTableWithFloatingHeader3\" style=\"position:relative\"></div>");

                  var originalHeaderRow = $("tr:first", this)
                  originalHeaderRow.before(originalHeaderRow.clone());
                  var clonedHeaderRow = $("tr:first", this)

                  clonedHeaderRow.addClass("tableFloatingHeader3");
                  clonedHeaderRow.css("position", "absolute");
                  clonedHeaderRow.css("top", "0px");
                  clonedHeaderRow.css("left", $(this).css("margin-left"));
                  clonedHeaderRow.css("visibility", "hidden");
                  clonedHeaderRow.css("background-color", "#F8F8F8");

                  originalHeaderRow.addClass("tableFloatingHeaderOriginal");
                });

              UpdateTableHeaders3();
              $(window).scroll(UpdateTableHeaders3);
              $(window).resize(UpdateTableHeaders3);
            
        });
      }

</script>
<!--Fin Prueba-->
  <!--Prueba-->

  <script type="text/javascript">

      if(navigator.appName.indexOf("MSIE")!=-1){
        //console.log("ie11");

      }else{ 
        function UpdateTableHeaders() {
               //yeah, he's using IE
                  
            $("div.divTableWithFloatingHeader").each(function() {
                var originalHeaderRow = $(".tableFloatingHeaderOriginal", this);
                var floatingHeaderRow = $(".tableFloatingHeader", this);
                var offset = $(this).offset();
                var scrollTop = $(window).scrollTop();

                var navegador = navigator.userAgent;
                var inicioScroll = scrollTop-30;
                var marginheader = 30;
                if (navigator.userAgent.indexOf('MSIE') !=-1) {
                 
                } else if (navigator.userAgent.indexOf('Firefox') !=-1) {

                  if (navigator.platform === "Android"){  
                    inicioScroll = scrollTop-30;
                    marginheader = 0; 
                  }else { 
                    inicioScroll = scrollTop-50;
                    marginheader = 50;
                  }

                } else if (navigator.userAgent.indexOf('Chrome') !=-1) {

                  if (navigator.platform === "Android"){  
                    inicioScroll = scrollTop-30;
                     marginheader = 0; 
                  }else { 
                    inicioScroll = scrollTop-30;
                    marginheader = 55;
                  }

                } else if (navigator.userAgent.indexOf('Opera') !=-1) {

                  if (navigator.platform === "Android"){  
                    inicioScroll = scrollTop-30;
                    marginheader = 0; 
                  }else { 
                    inicioScroll = scrollTop-30;
                    marginheader = 30;
                  }

                } else {
                
                }

                if (( inicioScroll > offset.top ) && (scrollTop < offset.top + $(this).height())) {
                    floatingHeaderRow.css("visibility", "visible");
                    floatingHeaderRow.css("top", (Math.min(scrollTop - offset.top, $(this).height() - floatingHeaderRow.height()))+ marginheader  + "px");

                    // Copy cell widths from original header
                    $("th", floatingHeaderRow).each(function(index) {
                        var cellWidth = $("th", originalHeaderRow).eq(index).css('width');
                        $(this).css('width', cellWidth);
                    });

                    // Copy row width from whole table
                    floatingHeaderRow.css("width", $(this).css("width"));
                }
                else {
                    floatingHeaderRow.css("visibility", "hidden");
                    floatingHeaderRow.css("top", "0px");
                    floatingHeaderRow.css("background-color", "#F8F8F8");
                }
            });
        }

        $(document).ready(function() {

                $("table.tableWithFloatingHeader").each(function() {
                  $(this).wrap("<div class=\"divTableWithFloatingHeader\" style=\"position:relative\"></div>");

                  var originalHeaderRow = $("tr:first", this)
                  originalHeaderRow.before(originalHeaderRow.clone());
                  var clonedHeaderRow = $("tr:first", this)

                  clonedHeaderRow.addClass("tableFloatingHeader");
                  clonedHeaderRow.css("position", "absolute");
                  clonedHeaderRow.css("top", "0px");
                  clonedHeaderRow.css("left", $(this).css("margin-left"));
                  clonedHeaderRow.css("visibility", "hidden");
                  clonedHeaderRow.css("background-color", "#F8F8F8");

                  originalHeaderRow.addClass("tableFloatingHeaderOriginal");
                });

              UpdateTableHeaders();
              $(window).scroll(UpdateTableHeaders);
              $(window).resize(UpdateTableHeaders);
            
        });
      }

</script>
<!--Fin Prueba-->

<!--Prueba-->
<script type="text/javascript">
    if(navigator.appName.indexOf("MSIE")!=-1){
        console.log("ie11");
    }else{
        function UpdateTableHeaders2() {
            $("div.divTableWithFloatingHeader2").each(function() {
                
                var originalHeaderRow = $(".tableFloatingHeaderOriginal", this);
                var floatingHeaderRow = $(".tableFloatingHeader", this);
                var offset = $(this).offset();
                var scrollTop = $(window).scrollTop();
                
                var navegador = navigator.userAgent;
                var inicioScroll = scrollTop-140;
                var marginheader = 30;
                if (navigator.userAgent.indexOf('MSIE') !=-1) {
                } else if (navigator.userAgent.indexOf('Firefox') !=-1) {

                    if (navigator.platform === "Android"){  
                      inicioScroll = scrollTop-30;
                      marginheader = 0; 
                    }else { 
                      inicioScroll = scrollTop-145;
                      marginheader = 50;
                    }

                } else if (navigator.userAgent.indexOf('Chrome') !=-1) {
                  

                    if (navigator.platform === "Android"){  
                      inicioScroll = scrollTop-30;
                      marginheader = 0; 
                    }else { 
                      inicioScroll = scrollTop-140;
                      marginheader = 55;
                    }

                } else if (navigator.userAgent.indexOf('Opera') !=-1) {
                 
                  if (navigator.platform === "Android"){  
                      inicioScroll = scrollTop-30;
                      marginheader = 0; 
                    }else { 
                      inicioScroll = scrollTop-140;
                      marginheader = 30;
                    }
                } else {
                }

                if ((inicioScroll > offset.top) && (scrollTop < offset.top + $(this).height())) {

                    console.log("valor:" + $(this).height());
                    console.log("valor 2:" + (scrollTop < offset.top + $(this).height()));
                    console.log("offset.top:" + offset.top);
                    console.log("scrollTop:  " + scrollTop);
                    floatingHeaderRow.css("visibility", "visible");
                    floatingHeaderRow.css("top", (Math.min(scrollTop - offset.top, $(this).height() - floatingHeaderRow.height()))+marginheader+ "px");

                    // Copy cell widths from original header
                    $("th", floatingHeaderRow).each(function(index) {
                        var cellWidth = $("th", originalHeaderRow).eq(index).css('width');
                        $(this).css('width', cellWidth);
                    });

                    // Copy row width from whole table
                    floatingHeaderRow.css("width", $(this).css("width"));
                }
                else {
                    floatingHeaderRow.css("visibility", "hidden");
                    floatingHeaderRow.css("top", "0px");
                    floatingHeaderRow.css("background-color", "#F8F8F8");
                }
            });
        }

        $(document).ready(function() {
            $("table.tableWithFloatingHeader2").each(function() {
                $(this).wrap("<div class=\"divTableWithFloatingHeader2\" style=\"position:relative\"></div>");

                var originalHeaderRow = $("tr:first", this)
                originalHeaderRow.before(originalHeaderRow.clone());
                var clonedHeaderRow = $("tr:first", this)

                clonedHeaderRow.addClass("tableFloatingHeader");
                clonedHeaderRow.css("position", "absolute");
                clonedHeaderRow.css("top", "0px");
                clonedHeaderRow.css("left", $(this).css("margin-left"));
                clonedHeaderRow.css("visibility", "hidden");
                clonedHeaderRow.css("background-color", "#F8F8F8");
                clonedHeaderRow.css("z-index", 5);
                originalHeaderRow.addClass("tableFloatingHeaderOriginal");
            });
            UpdateTableHeaders2();
            $(window).scroll(UpdateTableHeaders2);
            $(window).resize(UpdateTableHeaders2);
        });
      }
  </script>
  <!--Fin Prueba-->

<?php echo $this->element('ga');?>
</body>
</html>
