<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<meta name="description" content="Sistema de gestion de consumos, sistema de lecturas, gestion de lecturas, gestion de consumos">
	<meta name="author" content="D&D ANALYTICS">
	<link rel="shortcut icon" href="images/favicon.png">

	<title>sistema de lectura -      </title>

	<?php echo $this->element('environment_variables'); ?>
	<link href="http://fonts.googleapis.com/css?family=Roboto:100,300,100italic,400,300italic" rel="stylesheet" type="text/css">

	<!-- Bootstrap core CSS -->
	<link href="<?php echo ENV_WEBROOT_FULL_URL ?>js/bootstrap/dist/css/bootstrap.css" rel="stylesheet">

	<link rel="stylesheet" href="<?php echo ENV_WEBROOT_FULL_URL ?>fonts/font-awesome-4/css/font-awesome.min.css">
	
	<link rel="stylesheet" href="<?php echo ENV_WEBROOT_FULL_URL ?>landingpage/css/styles.css">
	<link rel="stylesheet" href="<?php echo ENV_WEBROOT_FULL_URL ?>landingpage/css/owl.theme.css">
	<link rel="stylesheet" href="<?php echo ENV_WEBROOT_FULL_URL ?>landingpage/css/owl.carousel.css">
	<link rel="stylesheet" href="<?php echo ENV_WEBROOT_FULL_URL ?>landingpage/css/nivo-lightbox.css">
	<link rel="stylesheet" href="<?php echo ENV_WEBROOT_FULL_URL ?>landingpage/css/colors/blue.css" title="blue">
	<link rel="stylesheet" type="text/css" href="<?php echo ENV_WEBROOT_FULL_URL ?>landingpage/fancy/source/jquery.fancybox.css?v=2.1.5" media="screen" />
	<link href="<?php echo ENV_WEBROOT_FULL_URL ?>fonts/font-awesome-4/css/font-awesome.min.css" rel="stylesheet">
	</head>

<header class="header" data-stellar-background-ratio="0.5" id="home">

<div class="color-overlay">

	<div class="container">
		<!-- ONLY LOGO ON HEADER -->
		<div class="only-logo">
			<div class="navbar">
				<div class="navbar-header">
					<img src="<?php echo ENV_WEBROOT_FULL_URL ?>landingpage/img/logo.jpg" alt="">
				</div>
			</div>
		</div> <!-- /END ONLY LOGO ON HEADER -->

		<div class="row home-contents">

			<div class="col-md-6 col-sm-6">
				
				<!-- HEADING AND BUTTONS -->
				<div class="intro-section">
					
					<!-- WELCOM MESSAGE -->
					<h1 class="intro">sistema de lectura -      </h1>
					<h5>Bienvenidos, para ingresar al sistema, click en el boton Iniciar Sesi&oacute;n.</h5>
					
					<?php 
						echo $this->fetch('content'); 
					?>
					
					
				</div>
				<!-- /END HEADNING AND BUTTONS -->
				
			</div>
			
			
			<div class="col-md-6 col-sm-6 hidden-xs">
			    
			    <!-- PHONE IMAGE WILL BE HIDDEN IN TABLET PORTRAIT AND MOBILE-->
			    <div class="phone-image">
			    <a href="<?php echo ENV_WEBROOT_FULL_URL ?>usuarios/login" class="fancybox fancybox.iframe"><img src="<?php echo ENV_WEBROOT_FULL_URL ?>landingpage/img/2-iphone-right.png" class="img-responsive" alt=""></a>
			    
			    </div>
			    
			</div>
			
		</div>

	</div>
	</div>

<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="<?php echo ENV_WEBROOT_FULL_URL ?>js/jquery.js"></script>
<script type="text/javascript" src="<?php echo ENV_WEBROOT_FULL_URL ?>landingpage/fancy/source/jquery.fancybox.js?v=2.1.5"></script>
<script type="text/javascript">
	$('.fancybox').fancybox();

</script>

<?php echo $this->element('ga');?>
</body>
</html>
