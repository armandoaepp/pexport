

<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="description" content="D&D ANALYTICS">
	<meta name="author" content="D&D ANALYTICS">
	<?php echo $this->element('environment_variables'); ?>
	<link rel="shortcut icon" href="<?php echo ENV_WEBROOT_FULL_URL ?>images/favicon.png">

	<title>sistema de lectura -      </title>
	
	<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,400italic,700,800' rel='stylesheet' type='text/css'>
	<link href='http://fonts.googleapis.com/css?family=Raleway:300,200,100' rel='stylesheet' type='text/css'>
	<link href='http://fonts.googleapis.com/css?family=Open+Sans+Condensed:300,700' rel='stylesheet' type='text/css'>

	<!-- Bootstrap core CSS -->
  <link href="<?php echo ENV_WEBROOT_FULL_URL ?>js/bootstrap/dist/css/bootstrap.css" rel="stylesheet" />
  <link rel="stylesheet" type="text/css" href="<?php echo ENV_WEBROOT_FULL_URL ?>js/jquery.gritter/css/jquery.gritter.css" />
	<link rel="stylesheet" href="<?php echo ENV_WEBROOT_FULL_URL ?>fonts/font-awesome-4/css/font-awesome.min.css">

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <![endif]-->	    

  <link rel="stylesheet" type="text/css" href="<?php echo ENV_WEBROOT_FULL_URL ?>js/jquery.nanoscroller/nanoscroller.css" />
  <link rel="stylesheet" type="text/css" href="<?php echo ENV_WEBROOT_FULL_URL ?>js/jquery.easypiechart/jquery.easy-pie-chart.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo ENV_WEBROOT_FULL_URL ?>js/bootstrap.switch/bootstrap-switch.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo ENV_WEBROOT_FULL_URL ?>js/bootstrap.datetimepicker/css/bootstrap-datetimepicker.min.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo ENV_WEBROOT_FULL_URL ?>js/jquery.select2/select2.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo ENV_WEBROOT_FULL_URL ?>js/jquery.niftymodals/css/component.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo ENV_WEBROOT_FULL_URL ?>js/bootstrap.slider/css/slider.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo ENV_WEBROOT_FULL_URL ?>js/intro.js/introjs.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo ENV_WEBROOT_FULL_URL ?>js/jquery.magnific-popup/dist/magnific-popup.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo ENV_WEBROOT_FULL_URL ?>js/loadmask/jquery.loadmask.css" media="screen" />
	
  <!-- Custom styles for this template -->
  <link href="<?php echo ENV_WEBROOT_FULL_URL ?>css/style.css" rel="stylesheet" />
	
	<script type="text/javascript" src="<?php echo ENV_WEBROOT_FULL_URL ?>js/jquery.js"></script>
    <script type="text/javascript" src="<?php echo ENV_WEBROOT_FULL_URL ?>js/jquery.gritter/js/jquery.gritter.js"></script>

  <script type="text/javascript" src="<?php echo ENV_WEBROOT_FULL_URL ?>js/jquery.nanoscroller/jquery.nanoscroller.js"></script>
  <script src="<?php echo ENV_WEBROOT_FULL_URL ?>js/jquery.ui/jquery-ui.js" type="text/javascript"></script>
	<script type="text/javascript" src="<?php echo ENV_WEBROOT_FULL_URL ?>js/jquery.sparkline/jquery.sparkline.min.js"></script>
	<script type="text/javascript" src="<?php echo ENV_WEBROOT_FULL_URL ?>js/jquery.easypiechart/jquery.easy-pie-chart.js"></script>
	<script type="text/javascript" src="<?php echo ENV_WEBROOT_FULL_URL ?>js/jquery.nestable/jquery.nestable.js"></script>
	<script type="text/javascript" src="<?php echo ENV_WEBROOT_FULL_URL ?>js/bootstrap.switch/bootstrap-switch.min.js"></script>
	<script type="text/javascript" src="<?php echo ENV_WEBROOT_FULL_URL ?>js/bootstrap.datetimepicker/js/bootstrap-datetimepicker.min.js"></script>
  <script src="<?php echo ENV_WEBROOT_FULL_URL ?>js/jquery.select2/select2.min.js" type="text/javascript"></script>
  <script src="<?php echo ENV_WEBROOT_FULL_URL ?>js/skycons/skycons.js" type="text/javascript"></script>
  <script src="<?php echo ENV_WEBROOT_FULL_URL ?>js/bootstrap.slider/js/bootstrap-slider.js" type="text/javascript"></script>
  <script type="text/javascript" src="<?php echo ENV_WEBROOT_FULL_URL ?>js/masonry.js"></script>
  <script type="text/javascript" src="<?php echo ENV_WEBROOT_FULL_URL ?>js/jquery.magnific-popup/dist/jquery.magnific-popup.min.js"></script>
  <script src="<?php echo ENV_WEBROOT_FULL_URL ?>js/intro.js/intro.js" type="text/javascript"></script>
  <script src="https://maps.googleapis.com/maps/api/js?v=3.exp&amp;sensor=false"></script>
  <script type="text/javascript" src="<?php echo ENV_WEBROOT_FULL_URL ?>js/jquery.niftymodals/js/jquery.modalEffects.js"></script>

<!-- Bootstrap core JavaScript
    ================================================== -->
    
    <script src="http://code.highcharts.com/highcharts.js"></script>
<script src="http://code.highcharts.com/modules/data.js"></script>
<script src="http://code.highcharts.com/modules/drilldown.js"></script>
<script src="http://code.highcharts.com/modules/exporting.js"></script>
    
    <script src="<?php echo ENV_WEBROOT_FULL_URL ?>js/behaviour/voice-commands.js"></script>
  <script src="<?php echo ENV_WEBROOT_FULL_URL ?>js/bootstrap/dist/js/bootstrap.min.js"></script>
	<script type="text/javascript" src="<?php echo ENV_WEBROOT_FULL_URL ?>js/jquery.flot/jquery.flot.js"></script>
	<script type="text/javascript" src="<?php echo ENV_WEBROOT_FULL_URL ?>js/jquery.flot/jquery.flot.pie.js"></script>
	<script type="text/javascript" src="<?php echo ENV_WEBROOT_FULL_URL ?>js/jquery.flot/jquery.flot.resize.js"></script>
	<script type="text/javascript" src="<?php echo ENV_WEBROOT_FULL_URL ?>js/jquery.flot/jquery.flot.labels.js"></script>
    
	<?php echo $this->Html->script('require'); ?>

	<script type="text/javascript">
	require.config({
		baseUrl: ENV_WEBROOT_FULL_URL+"js",
		paths: {
		},
		waitSeconds: 350,
		packages: [
		{
			name: "loadmask",
			main: 'jquery.loadmask.min'
		}]
	});

	</script>
</head>

<body>
 <!-- Fixed navbar -->
  <div id="head-nav" class="navbar navbar-default navbar-fixed-top" style="zoom:1">
    <div class="container-fluid" >

      <div class="navbar-collapse collapse">

          <style type="text/css">

            .navbar{
              background-color:  #555 !important;
              color: #fff !important;
            }

            .navbar li:hover, {
              background-color:  #444 !important;
              color: #fff !important;

            }

            .navbar-default .navbar-nav > .open > a, .navbar-default .navbar-nav > .open > a:hover, .navbar-default .navbar-nav > .open > a:focus {
              background-color:  #333 !important;
              color: #fff !important;
            }

            .navbar a:focus {
              background-color:  #333 !important;
              color: #fff !important;

            }

            .dropdown-menu > li > a:hover, .dropdown-menu li > a:hover {
              background-color:  #333 !important;
              color: #fff 
            }


            .navbar-default .navbar-nav > .open > a:hover,.navbar-nav > .open > a:focus{
              background-color:  #333 !important;
              color: #fff !important;
            }

            .dropdown-menu > .active > a, .dropdown-menu > .active > a:hover, .dropdown-menu > .active > a:focus{
               color: #fff !important;  
               background-color:  #333 !important;
            }

            .dropdown-menu > li.active {
               color: #fff !important;  
               background-color:  #333 !important;
            }

            .gallery-cont .item .over {
              top: 0;
              opacity: 0;
              position: absolute;
              height: 100%;
              width: 100%;
              background: rgba(0, 0, 0, 0.5) !important;
              transition: opacity 300ms ease;
              -webkit-transition: opacity 300ms ease;
              cursor: auto;
            }

             .dropdown-menu  li a span {
              margin-left:15px !important;
            }

            .navbar-default .navbar-brand  {
              background-image: none;
              font-weight: bold;
            }


          </style>

          <ul class="nav navbar-nav">
                <!--
                <li class="" id="madre" onclick="list(this)">
                  <a href="<?php echo ENV_WEBROOT_FULL_URL ?>home" onclick="showDash()"><i class="fa fa-home"></i><span> Home</span></a>
                </li>
                <?php //if(in_array("dashboard", $modulo_permiso)){ ?>
                  <li class="" id="" onclick="list(this)">
                    <a href="<?php echo ENV_WEBROOT_FULL_URL ?>dashboard" onclick="showDash()"><i class="fa fa-tachometer"></i><span> Dashboard</span></a>
                  </li>
                <?php //}?>
                -->
                <li class="dropdown">
                  <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                    <i class="fa fa-desktop"></i><span> Opciones del sistema </span><b class="caret"></b>
                  </a>

                  <ul class="dropdown-menu" <?php //if(isset($open) && $open=='true'){echo 'style="display: block;"';}else{ echo 'style="display: none;"';} ?> >

                    <!--<?php //if(in_array("importar", $modulo_permiso)){ ?>-->
                    <li class="">
                      <a href="<?php echo ENV_WEBROOT_FULL_URL ?>ComlecOrdenlecturas/form">Importar Ordenes</a>
                    </li>
                     <!--<?php //} ?>-->

                     <!--<?php //if(in_array("cronograma", $modulo_permiso)){ ?> -->
                    <li class="">
                      <a href="<?php echo ENV_WEBROOT_FULL_URL ?>ComlecOrdenlecturas/cronograma">Cronograma</a>
                    </li>
                     <!--<?php //} ?>-->

                     <!--<?php // if(in_array("listarciclosector", $modulo_permiso)){ ?> -->
                    <li class="">
                      <a href="<?php echo ENV_WEBROOT_FULL_URL ?>ComlecOrdenlecturas/listar_asignacion" onclick="sintesis()">Asignar Ordenes</a>
                    </li>
                     <!--<?php // } ?>-->

                     <!--<?php // if(in_array("monitorearlectura", $modulo_permiso)){ ?> -->
                    <li class="">
                      <a href="<?php echo ENV_WEBROOT_FULL_URL ?>MonitorearLecturas/monitorear_lectura">Monitorear Lecturas</a>
                    </li>
                     <!--<?php // } ?>-->

                     <!--<?php // if(in_array("monitoreargps", $modulo_permiso)){ ?> -->
                    <li class="">
                      <a href="<?php echo ENV_WEBROOT_FULL_URL ?>MonitorearLecturas/mapa_monitoreo">Monitorear GPS</a>
                    </li>
                     <!--<?php // } ?>-->

                     <!--<?php // if(in_array("digitarlecturas", $modulo_permiso)){ ?> -->
                    <li class="">
                      <a href="<?php echo ENV_WEBROOT_FULL_URL ?>ComlecOrdenlecturas/digitarlecturas">Digitar Lecturas</a>
                    </li>
                     <!--<?php // } ?>-->

                     <!--<?php // if(in_array("evaluarinconsistencia", $modulo_permiso)){ ?>-->
                    <li class="">
                      <a href="<?php echo ENV_WEBROOT_FULL_URL ?>EvaluarInconsistencias/evaluar_inconsistencia">Evaluar Inconsistencias</a>
                    </li>
                     <!--<?php // } ?>-->

                     <!--<?php // if(in_array("extranet", $modulo_permiso)){ ?> -->
                    <li class="">
                      <a href="<?php echo ENV_WEBROOT_FULL_URL ?>EvaluarInconsistencias/extranet">Extranet</a>
                    </li>
                     <!--<?php // } ?>-->

                     <!--<?php // if(in_array("importarlecturas", $modulo_permiso)){ ?> -->
                    <li class="">
                      <a href="<?php echo ENV_WEBROOT_FULL_URL ?>ComlecOrdenlecturas/importar_digitacion">Importar Lecturas desde Xls</a>
                    </li> 
                     <!--<?php // } ?>-->
                  </ul>
                </li>

                <!--
                <li class="dropdown">

                  <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-bar-chart-o"></i><span> Reportes <b class="caret"></b></span></a>

                  <ul  class="dropdown-menu" <?php //if(isset($open_report) && $open_report=='true'){echo 'style="display: block;"';}else{ echo 'style="display: none;"';} ?> >

                    <?php //if(in_array("rep_comparar_periodo", $modulo_permiso)){ ?> 
                    <li class="<?php //if($active=='rep_comparar_periodo'){echo 'active';} ?>" >
                      <a href="<?php //echo ENV_WEBROOT_FULL_URL ?>dashboard/comparar_periodo">Comparar Periodos</a>
                    </li>
                    <?php //} ?>

                    <?php //if(in_array("rep_observaciones", $modulo_permiso)){ ?>
                    <li class="<?php //if($active=='rep_observaciones'){echo 'active';} ?>" >
                      <a href="<?php //echo ENV_WEBROOT_FULL_URL ?>dashboard/reporte_observaciones">Reporte de Observaciones</a>
                    </li>
                    <?php //} ?>

                    <?php //if(in_array("rep_personal_para_lectura", $modulo_permiso)){ ?> 
                    <li class="<?php //if($active=='rep_personal_para_lectura'){echo 'active';} ?>" >
                      <a href="<?php //echo ENV_WEBROOT_FULL_URL ?>ComlecOrdenlecturas/listar_personal_para_lecturas">Reporte de Personal para Lecturas</a>
                    </li>
                    <?php //} ?>

                    <?php //if(in_array("rep_historico", $modulo_permiso)){ ?> 
                    <li class="<?php //if($active=='rep_historico'){echo 'active';} ?>" >
                      <a href="<?php //echo ENV_WEBROOT_FULL_URL ?>reportes/reporte_historico">Hist&oacute;rico por Suministro</a>
                    </li>
                    <?php //} ?>

                    <?php //if(in_array("rep_ordenes_por_observacion", $modulo_permiso)){ ?> 
                    <li class="<?php //if($active=='rep_ordenes_por_observacion'){echo 'active';} ?>" >
                      <a href="<?php //echo ENV_WEBROOT_FULL_URL ?>reportes/reporte_ordenes_por_observacion">Reporte de Ordenes por Observaci&oacute;n</a>
                    </li>
                    <?php //} ?>

                    <?php //if(in_array("rep_mineria", $modulo_permiso)){ ?> 
                    <li class="<?php //if($active=='rep_mineria'){echo 'active';} ?>" >
                      <a href="<?php //echo ENV_WEBROOT_FULL_URL ?>reportes/mineria">Reporte Mineria</a>
                    </li>
                    <?php //} ?>

                    <?php //if(in_array("rep_mineria2", $modulo_permiso)){ ?> 
                    <li class="<?php //if($active=='rep_mineria2'){echo 'active';} ?>" >
                      <a href="<?php //echo ENV_WEBROOT_FULL_URL ?>reportes/mineria2">Reporte Mineria 2</a>
                    </li>
                    <?php //} ?>

                    <?php //if(in_array("rep_consumo_por_dia_y_ubicacion", $modulo_permiso)){ ?> 
                    <li class="<?php //if($active=='rep_consumo_por_dia_y_ubicacion'){echo 'active';} ?>" >
                      <a href="<?php //echo ENV_WEBROOT_FULL_URL ?>dashboard/resumen_consumo_por_dia_y_ubicacion">Reporte de Consumo por Día y Ubicación</a>
                    </li>
                    <?php //} ?>

                    <?php //if(in_array("rep_suministros_cronograma", $modulo_permiso)){ ?> 
                    <li class="<?php //if($active=='rep_suministros_cronograma'){echo 'active';} ?>" >
                      <a href="<?php //echo ENV_WEBROOT_FULL_URL ?>reportes/resumen_suministros_cronograma">Reporte de Suministros Cronograma</a>
                    </li>
                    <?php //} ?>

                    <?php //if(in_array("rep_detallado_ciclo", $modulo_permiso)){ ?> 
                    <li class="<?php //if($active=='rep_detallado_ciclo'){echo 'active';} ?>" >
                      <a href="<?php //echo ENV_WEBROOT_FULL_URL ?>dashboard/resumen_por_ciclos?ajax=false">Reporte Detallado por Ciclo</a>
                    </li>
                    <?php //} ?>

                    <?php //if(in_array("reporte_ordenes_historico", $modulo_permiso)){ ?> 
                    <li class="<?php //if($active=='reporte_ordenes_historico'){echo 'active';} ?>" >
                      <a href="<?php //echo ENV_WEBROOT_FULL_URL ?>reportes/ordenes_historico">Reporte Ordenes Historico</a>
                    </li>
                    <?php //} ?>

                  </ul>
                </li>
                <li class="dropdown">
                  <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-briefcase"></i><span> Sistema</span> <b class="caret"></b></a>
                  <ul  class="dropdown-menu" <?php //if(isset($open_report) && $open_report=='true'){echo 'style="display: block;"';}else{ echo 'style="display: none;"';} ?>>
                    <li class="" onclick="list(this)"><a href="#" onclick="showMas1()">Maestros</a></li>
                   
                        <?php //if(in_array("usuarios", $modulo_permiso)){ ?> 
                        <li class="<?php //if($active=='usuarios'){echo 'active';} ?>" >
                          <a href="<?php //echo ENV_WEBROOT_FULL_URL ?>Usuarios/listado">Usuario</a></li>
                        <?php //} ?>

                    <li class="" onclick="list(this)"><a href="#" onclick="showEnl1()">Enlaces</a></li>
                    <li class="" onclick="list(this)"><a href="#" onclick="showEnl1()">Reglas de Consistencia</a></li>

                  </ul>
                </li>

                <li class="dropdown"> <a href="#"><i class="fa fa-list-alt"></i><span>Facturaci&oacute;n</span></a>
                  <ul  class="dropdown-menu" <?php //if(isset($open_fac) && $open_fac=='true'){echo 'style="display: block;"';}else{ echo 'style="display: none;"';} ?>>
                    <li class="" onclick="list(this)">
                      <a href="<?php //echo ENV_WEBROOT_FULL_URL ?>Facturacion/">Home</a>
                    </li>
                    <?php //if(in_array("fac_wizard", $modulo_permiso)){ ?> <li class="<?php //if($active=='fac_wizard'){echo 'active';} ?>" ><a href="<?php //echo ENV_WEBROOT_FULL_URL ?>Facturacion/wizard">Wizard</a></li><?php //} ?>
                    <?php //if(in_array("fac_pliegos", $modulo_permiso)){ ?> <li class="<?php //if($active=='fac_pliegos'){echo 'active';} ?>" ><a href="<?php //echo ENV_WEBROOT_FULL_URL ?>Facturacion/pliegos">Pliegos</a></li><?php //} ?>
                    <li class="" onclick="list(this)">
                      <a href="<?php //echo ENV_WEBROOT_FULL_URL ?>Facturacion/simulacion">Simular</a>
                    </li>

                    <?php //if(in_array("fac_list_facturas", $modulo_permiso)){ ?> 
                    <li class="<?php //if($active=='fac_list_facturas'){echo 'active';} ?>" >
                      <a href="<?php //echo ENV_WEBROOT_FULL_URL ?>Facturacion/list_facturas">Facturas Emitidas</a>
                    </li>
                    <?php //} ?>
                  </ul>
                </li>
               -->

              </ul>

               <ul class="nav navbar-nav navbar-right user-nav">
                   <div class="navbar-header" >
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                      <span class="fa fa-gear"></span>
                    </button>
                    <a class="navbar-brand" href="<?php echo ENV_WEBROOT_FULL_URL ?>home"><span>Pexport <!--- <?php //echo CakeSession::read('NEGOCIO_empresa');?> --></span></a>
                  </div>

                  <li class="" id="" onclick="list(this)" style="background-color:#444;">
                       <a href="  <?php echo ENV_WEBROOT_FULL_URL ?>/Usuarios/logout" > <i class="fa fa-close" ></i>  Cerrar</a></li>
                  </li>
              </ul>    

  

  
  

      </div><!--/.nav-collapse animate-collapse -->
    </div>
  </div>
  <!-- class="sb-collapsed" -->
<div id="cl-wrapper" class="fixed-menu sb-collapsed">
     <!-- CONTENT-->
			<?php 
				echo $this->fetch('content'); 
			?>
	<!-- /CONTENT -->	
</div>

<script type="text/javascript" src="<?php echo ENV_WEBROOT_FULL_URL ?>js/jquery.flot/jquery.flot.categories.min.js"></script>
<script type="text/javascript" src="<?php echo ENV_WEBROOT_FULL_URL ?>js/behaviour/general_dashboard.js"></script>
<!-- Placed at the end of the document so the pages load faster -->
    <script type="text/javascript">
      $(document).ready(function(){
        //initialize the javascript
        App.init();
        App.dashBoard();        
        
          //introJs().setOption('showBullets', false).start();

      });
    </script>
	<script type="text/javascript">
var LHCChatOptions = {};
LHCChatOptions.opt = {widget_height:340,widget_width:300,popup_height:520,popup_width:500,domain:'54.200.245.17'};
(function() {
var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
var refferer = (document.referrer) ? encodeURIComponent(document.referrer.substr(document.referrer.indexOf('://')+1)) : '';
var location  = (document.location) ? encodeURIComponent(window.location.href.substring(window.location.protocol.length)) : '';
po.src = '//54.200.245.17/livehelperchat/lhc_web/index.php/esp/chat/getstatus/(click)/internal/(position)/bottom_right/(ma)/br/(top)/350/(units)/pixels/(leaveamessage)/true/(department)/1?r='+refferer+'&l='+location;
var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
})();
</script>

<?php echo $this->element('ga');?>
</body>
</html>
