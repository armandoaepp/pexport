<!DOCTYPE html>
<html lang="es">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="D&D ANALYTICS">
  <meta name="author" content="D&D ANALYTICS">
  <?php echo $this->element('environment_variables'); ?>
  <link rel="shortcut icon" href="<?php echo ENV_WEBROOT_FULL_URL ?>images/favicon.png">

  <title>sistema de lectura -      </title>
  
  <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,400italic,700,800' rel='stylesheet' type='text/css'>
  <link href='http://fonts.googleapis.com/css?family=Raleway:300,200,100' rel='stylesheet' type='text/css'>

  <!-- Bootstrap core CSS -->
  <link href="<?php echo ENV_WEBROOT_FULL_URL ?>js/bootstrap/dist/css/bootstrap.css" rel="stylesheet">
  <link href="<?php echo ENV_WEBROOT_FULL_URL ?>js/fuelux/css/fuelux.css" rel="stylesheet">
  <link href="<?php echo ENV_WEBROOT_FULL_URL ?>js/fuelux/css/fuelux-responsive.min.css" rel="stylesheet">
  <link rel="stylesheet" type="text/css" href="<?php echo ENV_WEBROOT_FULL_URL ?>js/bootstrap.slider/css/slider.css" />
  <link rel="stylesheet" type="text/css" href="<?php echo ENV_WEBROOT_FULL_URL ?>js/jquery.gritter/css/jquery.gritter.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo ENV_WEBROOT_FULL_URL ?>css/jquery.jgrowl.css" />
  <link rel="stylesheet" href="<?php echo ENV_WEBROOT_FULL_URL ?>fonts/font-awesome-4/css/font-awesome.min.css">

  <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!--[if lt IE 9]>
    <script src="../../assets/js/html5shiv.js"></script>
    <script src="../../assets/js/respond.min.js"></script>
  <![endif]-->
  <link rel="stylesheet" type="text/css" href="<?php echo ENV_WEBROOT_FULL_URL ?>js/jquery.nanoscroller/nanoscroller.css" />
  <link rel="stylesheet" type="text/css" href="<?php echo ENV_WEBROOT_FULL_URL ?>js/jquery.easypiechart/jquery.easy-pie-chart.css" />
  <link rel="stylesheet" type="text/css" href="<?php echo ENV_WEBROOT_FULL_URL ?>js/bootstrap.switch/bootstrap-switch.css" />
  <link rel="stylesheet" type="text/css" href="<?php echo ENV_WEBROOT_FULL_URL ?>js/bootstrap.datetimepicker/css/bootstrap-datetimepicker.min.css" />
  <link rel="stylesheet" type="text/css" href="<?php echo ENV_WEBROOT_FULL_URL ?>js/jquery.select2/select2.css" />
  <link rel="stylesheet" type="text/css" href="<?php echo ENV_WEBROOT_FULL_URL ?>js/jquery.niftymodals/css/component.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo ENV_WEBROOT_FULL_URL ?>js/jquery.magnific-popup/dist/magnific-popup.css" />
  <link rel="stylesheet" type="text/css" href="<?php echo ENV_WEBROOT_FULL_URL ?>js/bootstrap.summernote/dist/summernote.css" />  
    <link href="<?php echo ENV_WEBROOT_FULL_URL ?>js/jquery.datatables/extras/TableTools/media/css/TableTools.css" media="all" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" type="text/css" href="<?php echo ENV_WEBROOT_FULL_URL ?>js/intro.js/introjs.css" />
  <!-- Custom styles for this template -->
  <link href="<?php echo ENV_WEBROOT_FULL_URL ?>css/style.css" rel="stylesheet" />
  
  <link href="<?php echo ENV_WEBROOT_FULL_URL ?>js/jquery.icheck/skins/square/blue.css" rel="stylesheet">

  
  <link rel="stylesheet" href="//code.jquery.com/ui/1.11.0/themes/smoothness/jquery-ui.css">
  <link rel="stylesheet" href="<?php echo ENV_WEBROOT_FULL_URL ?>js/preloader/css/normalize.css">
  <link rel="stylesheet" href="<?php echo ENV_WEBROOT_FULL_URL ?>js/preloader/css/main.css">
  <script src="<?php echo ENV_WEBROOT_FULL_URL ?>js/preloader/js/vendor/modernizr-2.6.2.min.js"></script>
  
  <script src="<?php echo ENV_WEBROOT_FULL_URL ?>js/jquery.js"></script>
  <script type="text/javascript" src="<?php echo ENV_WEBROOT_FULL_URL ?>js/bootstrap-fileupload.js"></script>
  <script type="text/javascript" src="<?php echo ENV_WEBROOT_FULL_URL ?>js/jquery.gritter/js/jquery.gritter.min.js"></script>
  <script type="text/javascript" src="<?php echo ENV_WEBROOT_FULL_URL ?>js/jquery.nanoscroller/jquery.nanoscroller.js"></script>
  <script type="text/javascript" src="<?php echo ENV_WEBROOT_FULL_URL ?>js/fuelux/loader.min.js"></script>
  <script type="text/javascript" src="<?php echo ENV_WEBROOT_FULL_URL ?>js/behaviour/general.js"></script>
  <script type="text/javascript" src="<?php echo ENV_WEBROOT_FULL_URL ?>js/jquery.sparkline/jquery.sparkline.min.js"></script>
  <script type="text/javascript" src="<?php echo ENV_WEBROOT_FULL_URL ?>js/jquery.easypiechart/jquery.easy-pie-chart.js"></script>  
  <script type="text/javascript" src="<?php echo ENV_WEBROOT_FULL_URL ?>js/jquery.nestable/jquery.nestable.js"></script>
  <script type="text/javascript" src="<?php echo ENV_WEBROOT_FULL_URL ?>js/bootstrap.switch/bootstrap-switch.min.js"></script>
  <script type="text/javascript" src="<?php echo ENV_WEBROOT_FULL_URL ?>js/bootstrap.datetimepicker/js/bootstrap-datetimepicker.min.js"></script>
  <script src="<?php echo ENV_WEBROOT_FULL_URL ?>js/jquery.select2/select2.min.js" type="text/javascript"></script>
  <script src="<?php echo ENV_WEBROOT_FULL_URL ?>js/skycons/skycons.js" type="text/javascript"></script>
  <script type="text/javascript" src="<?php echo ENV_WEBROOT_FULL_URL ?>js/jquery.niftymodals/js/jquery.modalEffects.js"></script> 
  <script type="text/javascript" src="<?php echo ENV_WEBROOT_FULL_URL ?>js/bootstrap.summernote/dist/summernote.min.js"></script>
  <script  type="text/javascript" src="<?php echo ENV_WEBROOT_FULL_URL ?>js/intro.js/intro.js"></script>
  <script type="text/javascript" src="<?php echo ENV_WEBROOT_FULL_URL ?>js/tables/jquery.dataTables.columnFilter.js"></script>
  <script src="<?php echo ENV_WEBROOT_FULL_URL ?>js/preloader/js/main.js"></script>
  <script type="text/javascript">


    function sintesis(){
        $('#sidebar-collapse').click();
      }

    function tech(){

       var navegador = navigator.userAgent;
        if (navigator.userAgent.indexOf('MSIE') !=-1) {
          /*
          alert('está usando Internet Explorer ...');
          */
        } else if (navigator.userAgent.indexOf('Firefox') !=-1) {

        } else if (navigator.userAgent.indexOf('Chrome') !=-1) {
          /*
          alert('está usando Google Chrome ...');
          */
          var camb = $('#monit');
          camb.removeClass('farmsmall2');
          camb.addClass('farmsmall2');
           
          var camb2 = $('#asig');
          camb2.removeClass('farmsmall2');
          camb2.addClass('farmsmall2');

          var camb3 = $('#asig2');
          camb3.removeClass('farmsmall2');
          camb3.addClass('farmsmall2');

          var camb4 = $('#digitar');
          camb4.removeClass('farmsmall2');
          camb4.addClass('farmsmall2');

          var camb5 = $('#inco');
          camb5.removeClass('farmsmall2');
          camb5.addClass('farmsmall2');


        } else if (navigator.userAgent.indexOf('Opera') !=-1) {
          alert('está usando Opera ...');
        } else {
          /*
          alert('Debes estar usando Internet Explorer...');
          
          var elemento = document.querySelector('#monit');
          elemento.className = "farmsmall";
          var elementoo = document.querySelector('#asig');
          elementoo.className = "farmsmall";  
          */
          var camb = $('#monit');
          camb.removeClass('farmsmall2');
          camb.addClass('farmsmall');
           
          var camb2 = $('#asig');
          camb2.removeClass('farmsmall2');
          camb2.addClass('farmsmall');

          var camb3 = $('#asig2');
          camb3.removeClass('farmsmall2');
          camb3.addClass('farmsmall');

          var camb4 = $('#digitar');
          camb4.removeClass('farmsmall2');
          camb4.addClass('farmsmalldigi');

          var camb5 = $('#inco');
          camb5.removeClass('farmsmall2');
          camb5.addClass('farmsmallinco');

 
        }
    }


    $(document).ready(function(){
      //initialize the javascript
      App.init();
      App.wizard();
      //App.dashBoard();
      tech();

      sintesis();    
      introJs().setOption('showBullets', false).start();
      
      $('.image-zoom').magnificPopup({ 
        type: 'image',
        mainClass: 'mfp-with-zoom', // this class is for CSS animation below
        zoom: {
          enabled: true, // By default it's false, so don't forget to enable it
          duration: 300, // duration of the effect, in milliseconds
          easing: 'ease-in-out', // CSS transition easing function 
          opener: function(openerElement) {
            var parent = $(openerElement);
            return parent;
          }
        }
      });
     


    $('.md-trigger').modalEffects();
    
      $('label.tree-toggler').click(function () {
        var icon = $(this).children(".fa");
          if(icon.hasClass("fa-folder-o")){
            icon.removeClass("fa-folder-o").addClass("fa-folder-open-o");
          }else{
            icon.removeClass("fa-folder-open-o").addClass("fa-folder-o");
          }        
          
        $(this).parent().children('ul.tree').toggle(300,function(){
          $(this).parent().toggleClass("open");
          $(".tree .nscroller").nanoScroller({ preventPageScrolling: true });
        });
        
      });

      $('input').iCheck({
          checkboxClass: 'icheckbox_square-blue checkbox',
          radioClass: 'iradio_square-blue'
        });

    });
  </script>

<!-- Bootstrap core JavaScript
  ================================================== -->
  <!-- Placed at the end of the document so the pages load faster -->
  <script src="<?php echo ENV_WEBROOT_FULL_URL ?>js/behaviour/voice-commands.js"></script>
  <script src="<?php echo ENV_WEBROOT_FULL_URL ?>js/bootstrap/dist/js/bootstrap.min.js"></script>
  <script src="<?php echo ENV_WEBROOT_FULL_URL ?>js/jquery.ui/jquery-ui.js" type="text/javascript"></script>
  <script type="text/javascript" src="<?php echo ENV_WEBROOT_FULL_URL ?>js/jquery.flot/jquery.flot.js"></script>
  <script type="text/javascript" src="<?php echo ENV_WEBROOT_FULL_URL ?>js/jquery.flot/jquery.flot.pie.js"></script>
  <script type="text/javascript" src="<?php echo ENV_WEBROOT_FULL_URL ?>js/jquery.flot/jquery.flot.resize.js"></script>
  <script type="text/javascript" src="<?php echo ENV_WEBROOT_FULL_URL ?>js/jquery.flot/jquery.flot.labels.js"></script>
  <script type="text/javascript" src="http://www.flotcharts.org/flot/jquery.flot.time.js"></script>
  <script type="text/javascript" src="<?php echo ENV_WEBROOT_FULL_URL ?>js/jquery.magnific-popup/dist/jquery.magnific-popup.min.js"></script>
  <script src="<?php echo ENV_WEBROOT_FULL_URL ?>js/bootstrap.slider/js/bootstrap-slider.js" type="text/javascript"></script>
  <script type="text/javascript" src="<?php echo ENV_WEBROOT_FULL_URL ?>js/jquery.datatables/jquery.datatables.min.js"></script>
  <script type="text/javascript" src="<?php echo ENV_WEBROOT_FULL_URL ?>js/jquery.datatables/bootstrap-adapter/js/datatables.js"></script>
  <!-- <script  src="<?php /*echo ENV_WEBROOT_FULL_URL */?>js/jquery.datatables/extras/ColVis/media/js/ColVis.js" type="text/javascript"></script> -->
  <script src="<?php echo ENV_WEBROOT_FULL_URL ?>js/jquery.datatables/extras/TableTools/media/js/TableTools.js" type="text/javascript"></script>
  <script src="<?php echo ENV_WEBROOT_FULL_URL ?>js/jquery.datatables/extras/TableTools/media/js/ZeroClipboard.js" type="text/javascript"></script>
  <link rel="stylesheet" type="text/css" href="<?php echo ENV_WEBROOT_FULL_URL ?>js/fancyapps/source/jquery.fancybox.css?v=2.1.5" media="screen" />
  <link rel="stylesheet" type="text/css" href="<?php echo ENV_WEBROOT_FULL_URL ?>js/loadmask/jquery.loadmask.css" media="screen" />
  <script type="text/javascript" src="<?php echo ENV_WEBROOT_FULL_URL ?>js/jquery.icheck/icheck.min.js"></script>
  <?php echo $this->Html->script('require');?>
  <script type="text/javascript">
  require.config({
    baseUrl: ENV_WEBROOT_FULL_URL+"js",
    paths: {
    },
    waitSeconds: 15,
    packages: [
        {
        name: "jquery.datatables",
        main: 'bootstrap-adapter/js/datatables'
      },
      {
        name: "fancyapps",
        main: 'source/jquery.fancybox'
      },
      {
        name: "loadmask",
        main: 'jquery.loadmask.min'
      }
    ]
  });
  require(['internal'],function(){});
  </script>

</head>

<body>

 <!-- Fixed navbar -->
  <div id="head-nav" class="navbar navbar-default navbar-fixed-top" style="zoom:1; <?php if($NEGOCIO == 'TAC'){echo 'background-color:#d14836;';}?>">
    <div class="container-fluid" >
      <div class="navbar-header" >
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
          <span class="fa fa-gear"></span>
        </button>
        <a class="navbar-brand" href="<?php echo ENV_WEBROOT_FULL_URL ?>home"><span>Distriluz - <?php echo CakeSession::read('NEGOCIO_empresa');?></span></a>
      </div>
      <div class="navbar-collapse collapse">
        <ul class="nav navbar-nav">
          <li></li>
        </ul>
    <ul class="nav navbar-nav navbar-right user-nav">
      <li class="dropdown profile_menu">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown"><img alt="Avatar" src="<?php echo ENV_WEBROOT_FULL_URL ?>images/us.png" width="30" height="30" /><span><?php echo $_SESSION['username'];?></span> <b class="caret"></b></a>
        <ul class="dropdown-menu">
          <li><a href="#">Actividad</a></li>
          <li><a href="#">Mensajes</a></li>
          <li class="divider"></li>
          <li><a href="<?php echo ENV_WEBROOT_FULL_URL ?>/Usuarios/logout">Salir</a></li>
        </ul>
      </li>
    </ul>     
    <ul class="nav navbar-nav navbar-right not-nav">

      <li class="button dropdown">
        <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-globe"></i><span class="bubble">3</span></a>
        <ul class="dropdown-menu">
          <li>
            <div class="nano nscroller">
              <div class="content">
                <ul>
                  <li><a href="#"><i class="fa fa-cloud-upload info"></i><b>Tecnico</b> Te ha enviado mensaje <span class="date">2 minutos atras.</span></a></li>
                  <li><a href="#"><i class="fa fa-male success"></i> <b>Usuario A</b> requiere aprobacion <span class="date">15 minutos atras.</span></a></li>
                  <li><a href="#"><i class="fa fa-bug warning"></i> <b>Call Center</b> Modifico un Evento <span class="date">30 minutos atras.</span></a></li>
                </ul>
              </div>
            </div>
            <ul class="foot"><li><a href="#">Ver mensajes anteriores </a></li></ul>           
          </li>
        </ul>
      </li>
           
    </ul>

      </div><!--/.nav-collapse animate-collapse -->
    </div>
  </div>
<!-- Preloader-->
<div id="loader-wrapper">

  
  <?php 
  if (isset($_SERVER["HTTP_USER_AGENT"]) && strstr($_SERVER["HTTP_USER_AGENT"], "MSIE")){
    ?>
    <div id="loader2">
    <img alt="loading" src="<?php echo ENV_WEBROOT_FULL_URL;?>images/loadingweb.gif">
    </div>
  <?php }else{?>
    <div id="loader"></div>
  <?php }?>
  <div class="loader-section section-left"></div>
  <div class="loader-section section-right"></div>      
</div>
<!-- /preloader -->     
<div id="cl-wrapper" class="fixed-menu">
     <!-- CONTENT-->
      <?php 
        echo $this->fetch('content'); 
      ?>
  <!-- /CONTENT --> 
</div>

  <!--Prueba-->

  <script type="text/javascript">

      if(navigator.appName.indexOf("MSIE")!=-1){
        //console.log("ie11");
      }else{ 
        function UpdateTableHeaders3() {
            //yeah, he's using IE
                  
            $("div.divTableWithFloatingHeader3").each(function() {
                var originalHeaderRow = $(".tableFloatingHeaderOriginal", this);
                var floatingHeaderRow = $(".tableFloatingHeader3", this);
                var offset = $(this).offset();
                var scrollTop = $(window).scrollTop();

                var navegador = navigator.userAgent;
                var inicioScroll = scrollTop+ 110;
                var marginheader = 95;
                if (navigator.userAgent.indexOf('MSIE') !=-1) {
                 
                } else if (navigator.userAgent.indexOf('Firefox') !=-1) {

                  if (navigator.platform === "Android"){  
                    inicioScroll = scrollTop + 110;
                    marginheader = 95; 
                  }
                  if( $('.widget-monitoreo').hasClass('visible') ){
                    inicioScroll = scrollTop + 70 //30  --->-30 + 140
                    marginheader = 105; //55  -> 95
                  }
                  else { 
                    inicioScroll = scrollTop + 70; //30  --->-30 + 140
                    marginheader = 188; //55  -> 95
                  }

                } else if (navigator.userAgent.indexOf('Chrome') !=-1) {

                  if (navigator.platform === "Android"){  
                    inicioScroll = scrollTop + 110;
                     marginheader = 95; 
                  }

                  if( $('.widget-monitoreo').hasClass('visible') ){
                    inicioScroll = scrollTop + 60 //30  --->-30 + 140
                    marginheader = 100; //55  -> 95
                  }
                  else { 
                    inicioScroll = scrollTop + 160; //30  --->-30 + 140
                    marginheader = 200; //55  -> 95
                  }

                } else if (navigator.userAgent.indexOf('Opera') !=-1) {

                  if (navigator.platform === "Android"){  
                    inicioScroll = scrollTop + 110;
                    marginheader = 95; 
                  }else { 
                    inicioScroll = scrollTop + 110;
                    marginheader = 125;
                  }

                } else {
                
                }

                if (( inicioScroll > offset.top ) && (scrollTop < offset.top + $(this).height())) {
                    floatingHeaderRow.css("visibility", "visible");
                    floatingHeaderRow.css("top", (Math.min(scrollTop - offset.top, $(this).height() - floatingHeaderRow.height()))+ marginheader  + "px");

                    // Copy cell widths from original header
                    $("th", floatingHeaderRow).each(function(index) {
                        var cellWidth = $("th", originalHeaderRow).eq(index).css('width');
                        $(this).css('width', cellWidth);
                    });

                    // Copy row width from whole table
                    floatingHeaderRow.css("width", $(this).css("width"));
                }
                else {
                    floatingHeaderRow.css("visibility", "hidden");
                    floatingHeaderRow.css("top", "0px");
                    floatingHeaderRow.css("background-color", "#F8F8F8");
                }
            });
        }

        $(document).ready(function() {

                $("table.tableWithFloatingHeader3").each(function() {
                  $(this).wrap("<div class=\"divTableWithFloatingHeader3\" style=\"position:relative\"></div>");

                  var originalHeaderRow = $("tr:first", this)
                  originalHeaderRow.before(originalHeaderRow.clone());
                  var clonedHeaderRow = $("tr:first", this)

                  clonedHeaderRow.addClass("tableFloatingHeader3");
                  clonedHeaderRow.css("position", "absolute");
                  clonedHeaderRow.css("top", "0px");
                  clonedHeaderRow.css("left", $(this).css("margin-left"));
                  clonedHeaderRow.css("visibility", "hidden");
                  clonedHeaderRow.css("background-color", "#F8F8F8");

                  originalHeaderRow.addClass("tableFloatingHeaderOriginal");
                });

              UpdateTableHeaders3();
              $(window).scroll(UpdateTableHeaders3);
              $(window).resize(UpdateTableHeaders3);
            
        });
      }

</script>
<!--Fin Prueba-->
  <!--Prueba-->

  <script type="text/javascript">

      if(navigator.appName.indexOf("MSIE")!=-1){
        //console.log("ie11");

      }else{ 
        function UpdateTableHeaders() {
               //yeah, he's using IE
                  
            $("div.divTableWithFloatingHeader").each(function() {
                var originalHeaderRow = $(".tableFloatingHeaderOriginal", this);
                var floatingHeaderRow = $(".tableFloatingHeader", this);
                var offset = $(this).offset();
                var scrollTop = $(window).scrollTop();

                var navegador = navigator.userAgent;
                var inicioScroll = scrollTop-30;
                var marginheader = 30;
                if (navigator.userAgent.indexOf('MSIE') !=-1) {
                 
                } else if (navigator.userAgent.indexOf('Firefox') !=-1) {

                  if (navigator.platform === "Android"){  
                    inicioScroll = scrollTop-30;
                    marginheader = 0; 
                  }else { 
                    inicioScroll = scrollTop-50;
                    marginheader = 50;
                  }

                } else if (navigator.userAgent.indexOf('Chrome') !=-1) {

                  if (navigator.platform === "Android"){  
                    inicioScroll = scrollTop-30;
                     marginheader = 0; 
                  }else { 
                    inicioScroll = scrollTop-30;
                    marginheader = 55;
                  }

                } else if (navigator.userAgent.indexOf('Opera') !=-1) {

                  if (navigator.platform === "Android"){  
                    inicioScroll = scrollTop-30;
                    marginheader = 0; 
                  }else { 
                    inicioScroll = scrollTop-30;
                    marginheader = 30;
                  }

                } else {
                
                }

                if (( inicioScroll > offset.top ) && (scrollTop < offset.top + $(this).height())) {
                    floatingHeaderRow.css("visibility", "visible");
                    floatingHeaderRow.css("top", (Math.min(scrollTop - offset.top, $(this).height() - floatingHeaderRow.height()))+ marginheader  + "px");

                    // Copy cell widths from original header
                    $("th", floatingHeaderRow).each(function(index) {
                        var cellWidth = $("th", originalHeaderRow).eq(index).css('width');
                        $(this).css('width', cellWidth);
                    });

                    // Copy row width from whole table
                    floatingHeaderRow.css("width", $(this).css("width"));
                }
                else {
                    floatingHeaderRow.css("visibility", "hidden");
                    floatingHeaderRow.css("top", "0px");
                    floatingHeaderRow.css("background-color", "#F8F8F8");
                }
            });
        }

        $(document).ready(function() {

                $("table.tableWithFloatingHeader").each(function() {
                  $(this).wrap("<div class=\"divTableWithFloatingHeader\" style=\"position:relative\"></div>");

                  var originalHeaderRow = $("tr:first", this)
                  originalHeaderRow.before(originalHeaderRow.clone());
                  var clonedHeaderRow = $("tr:first", this)

                  clonedHeaderRow.addClass("tableFloatingHeader");
                  clonedHeaderRow.css("position", "absolute");
                  clonedHeaderRow.css("top", "0px");
                  clonedHeaderRow.css("left", $(this).css("margin-left"));
                  clonedHeaderRow.css("visibility", "hidden");
                  clonedHeaderRow.css("background-color", "#F8F8F8");

                  originalHeaderRow.addClass("tableFloatingHeaderOriginal");
                });

              UpdateTableHeaders();
              $(window).scroll(UpdateTableHeaders);
              $(window).resize(UpdateTableHeaders);
            
        });
      }

</script>
<!--Fin Prueba-->

<!--Prueba-->
<script type="text/javascript">
    if(navigator.appName.indexOf("MSIE")!=-1){
        console.log("ie11");
    }else{
        function UpdateTableHeaders2() {
            $("div.divTableWithFloatingHeader2").each(function() {
                
                var originalHeaderRow = $(".tableFloatingHeaderOriginal", this);
                var floatingHeaderRow = $(".tableFloatingHeader", this);
                var offset = $(this).offset();
                var scrollTop = $(window).scrollTop();
                
                var navegador = navigator.userAgent;
                var inicioScroll = scrollTop-140;
                var marginheader = 30;
                if (navigator.userAgent.indexOf('MSIE') !=-1) {
                } else if (navigator.userAgent.indexOf('Firefox') !=-1) {

                    if (navigator.platform === "Android"){  
                      inicioScroll = scrollTop-30;
                      marginheader = 0; 
                    }else { 
                      inicioScroll = scrollTop-145;
                      marginheader = 50;
                    }

                } else if (navigator.userAgent.indexOf('Chrome') !=-1) {
                  

                    if (navigator.platform === "Android"){  
                      inicioScroll = scrollTop-30;
                      marginheader = 0; 
                    }else { 
                      inicioScroll = scrollTop-140;
                      marginheader = 55;
                    }

                } else if (navigator.userAgent.indexOf('Opera') !=-1) {
                 
                  if (navigator.platform === "Android"){  
                      inicioScroll = scrollTop-30;
                      marginheader = 0; 
                    }else { 
                      inicioScroll = scrollTop-140;
                      marginheader = 30;
                    }
                } else {
                }

                if ((inicioScroll > offset.top) && (scrollTop < offset.top + $(this).height())) {

                    console.log("valor:" + $(this).height());
                    console.log("valor 2:" + (scrollTop < offset.top + $(this).height()));
                    console.log("offset.top:" + offset.top);
                    console.log("scrollTop:  " + scrollTop);
                    floatingHeaderRow.css("visibility", "visible");
                    floatingHeaderRow.css("top", (Math.min(scrollTop - offset.top, $(this).height() - floatingHeaderRow.height()))+marginheader+ "px");

                    // Copy cell widths from original header
                    $("th", floatingHeaderRow).each(function(index) {
                        var cellWidth = $("th", originalHeaderRow).eq(index).css('width');
                        $(this).css('width', cellWidth);
                    });

                    // Copy row width from whole table
                    floatingHeaderRow.css("width", $(this).css("width"));
                }
                else {
                    floatingHeaderRow.css("visibility", "hidden");
                    floatingHeaderRow.css("top", "0px");
                    floatingHeaderRow.css("background-color", "#F8F8F8");
                }
            });
        }

        $(document).ready(function() {
            $("table.tableWithFloatingHeader2").each(function() {
                $(this).wrap("<div class=\"divTableWithFloatingHeader2\" style=\"position:relative\"></div>");

                var originalHeaderRow = $("tr:first", this)
                originalHeaderRow.before(originalHeaderRow.clone());
                var clonedHeaderRow = $("tr:first", this)

                clonedHeaderRow.addClass("tableFloatingHeader");
                clonedHeaderRow.css("position", "absolute");
                clonedHeaderRow.css("top", "0px");
                clonedHeaderRow.css("left", $(this).css("margin-left"));
                clonedHeaderRow.css("visibility", "hidden");
                clonedHeaderRow.css("background-color", "#F8F8F8");
                clonedHeaderRow.css("z-index", 5);
                originalHeaderRow.addClass("tableFloatingHeaderOriginal");
            });
            UpdateTableHeaders2();
            $(window).scroll(UpdateTableHeaders2);
            $(window).resize(UpdateTableHeaders2);
        });
      }
  </script>
  <!--Fin Prueba-->
<script type="text/javascript">
var LHCChatOptions = {};
LHCChatOptions.opt = {widget_height:340,widget_width:300,popup_height:520,popup_width:500,domain:'54.200.245.17'};
(function() {
var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
var refferer = (document.referrer) ? encodeURIComponent(document.referrer.substr(document.referrer.indexOf('://')+1)) : '';
var location  = (document.location) ? encodeURIComponent(window.location.href.substring(window.location.protocol.length)) : '';
po.src = '//54.200.245.17/livehelperchat/lhc_web/index.php/esp/chat/getstatus/(click)/internal/(position)/bottom_right/(ma)/br/(top)/350/(units)/pixels/(leaveamessage)/true/(department)/1?r='+refferer+'&l='+location;
var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
})();
</script>

<?php echo $this->element('ga');?>
</body>
</html>
