<?php 
if (isset($id_usuario) && $id_usuario == 25 || $id_usuario == 131){
	$permiso_lectura = 'false';
}else {
	$permiso_lectura = 'true';
}
echo $this->element('menu',array('active'=>'usuarios', 'open'=>'false', 'permiso_lectura' =>$permiso_lectura, 'id_usuario ' =>isset($id_usuario)?$id_usuario:null));
echo $this->element('Usuarios/tree');

?>

<div class="container-fluid" id="pcont">

	<div class="page-head">

		<ol class="breadcrumb">
			<li><button type="button"
					class="btn-xs btn-default arbol-top-lecturas">Arbol</button></li>
			<li><a href="<?php echo ENV_WEBROOT_FULL_URL ?>personas/listado">Listado
					de Personas</a></li>
			<li class="active">Nuevo</li>
		</ol>
	</div>

	<div class="cl-mcont">
		<div class="col-sm-12 col-md-12">
			<div class="block">

				<div class="content">
					<form action="<?php echo ENV_WEBROOT_FULL_URL ?>personas/grabar" method = "post" >
						<div class="form-group">
							<label class="control-label">Nombre y Apellidos</label> 
							<input type="input" name="data[GlomasEmpleado][nombre]" id="data[GlomasEmpleado][nombre]" value="">
						</div>
						
						<div class="form-group">
							<label class="control-label">Tipo Persona</label> 						
								
							<select class="select2" name="data[GlomasEmpleado][glomas_tipoempleado_id]" id="data[GlomasEmpleado][glomas_tipoempleado_id]">
				                <option value="1">Lecturista</option>
				                <option value="2">Sistema Web</option>
				              </select>				
						</div>
						<input class='btn btn-primary' type="submit" value = "Grabar Persona">
					</form>
				</div>
			</div>

		</div>
	</div>
</div>
</div>