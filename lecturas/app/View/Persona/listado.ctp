<?php 
if (isset($id_usuario) && $id_usuario == 25 || $id_usuario == 131){
	$permiso_lectura = 'false';
}else {
	$permiso_lectura = 'true';
}
echo $this->element('menu',array('active'=>'usuarios', 'open'=>'false', 'permiso_lectura' =>$permiso_lectura, 'id_usuario ' =>isset($id_usuario)?$id_usuario:null));
echo $this->element('Usuarios/tree');
?>

<div class="container-fluid" id="pcont">

		<div class="page-head">
				
				<ol class="breadcrumb">
				  <li><button type="button" class="btn-xs btn-default arbol-top-lecturas" >Arbol</button></li>
				  <li class="active">Listado de Personas</li>
				</ol>
		</div>

	<div class="cl-mcont">
		<div class="col-sm-12 col-md-12" >
			<div class="block">
				<a class='btn btn-primary' id='nuevo-usuario' href="<?php echo ENV_WEBROOT_FULL_URL ?>personas/nuevo">Nuevo</a>


				 <div class="content">
		          <div class="tabla">
		            <div id="monit" class='farmsmall2'>
						<div class="header no-border">
							<h2>Listado de Personas</h2>
						</div>

						<div class="content no-padding">
							<table class="red table table-bordered dataTable" id = "list-monitorear-lecturistas">
								<thead>
									<tr>
										<th>#</th>
										<th>Nombre y Apellidos</th>
										<th>Tipo Empleado</th>
									</tr>
								</thead>

								<tfoot>
						            <tr>
						            </tr>
						        </tfoot>
										        
								<tbody class="no-border-x">
								<?php 
								$i = 1;
								foreach ($arr_personas as $persona){
								?>
									<tr>
										<td>
										<?php echo $i++;?> 
										</td>
										<td><?php echo $persona['0']["nombre"];?></td>
										<td><?php echo $persona['0']["descripcion"];?></td>
									</tr>
								<?php } ?>		
								</tbody>

							</table>
						</div>
					</div>
				</div>
</div>
			</div>

			</div>
         </div>
    </div>
    
</div>