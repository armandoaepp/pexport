<?php 
if (!isset($NEGOCIO)) {
    header("Location:".ENV_WEBROOT_FULL_URL."/Usuarios/logout");
    die();
}
?>
<?php 
if (isset($id_usuario) && $id_usuario == 69){
	$permiso_lectura = 'true';
}else {
	$permiso_lectura = 'true';
}
echo $this->element('menu',array('active'=>'rep_mineria2', 'open_report'=>'false', 'permiso_lectura' =>$permiso_lectura, 'id_usuario ' =>isset($id_usuario)?$id_usuario:null));
?>

<style>
#preview{
	position:absolute;
	border:1px solid #ccc;
	background:#333;
	padding:5px;
	display:none;
	color:#fff;
	}
div.DTTT_container {
	margin-top: -50px;
}
</style>
<div class="container-fluid" id="pcont">

<div class="page-head" style="padding-bottom: 6px;">
		<div class="row">
			<div class="col-xs-2">
				<h3 style="margin-top: 0px">Filtros:</h3>
			</div>
		  <div class="col-xs-1">
		    <label class="control-label" style="padding-top: 8px;">Por Ciclo:</label>
		  </div>
		  <div class="col-xs-2">    
		  		<span class="ciclo_loading"
							style="display: none;"><img
								src="<?php echo ENV_WEBROOT_FULL_URL ?>/js/jquery.select2/select2-spinner.gif"></span></label>              
	              	<?php 
	        			  	echo '<select class="" name="id_ciclo" id="id_ciclo">
	        			  	<option value="0">Todos</option>';
	        				foreach ($listar_ciclo as $v){
								if($idciclo==$v['0']['idciclo']){
									echo '<option value='.$v['0']['idciclo'].' selected>'.$v['0']['idciclo'].' - '.$v['0']['nombciclo'].$str_ciclo_actual.'</option>';
								}else{
									echo '<option value='.$v['0']['idciclo'].'>'.$v['0']['idciclo'].' - '.$v['0']['nombciclo'].$str_ciclo_actual.'</option>';	
								}
	        				}
	        				echo '</select>';	
	        		?>        
		  </div>
		  <div class="col-xs-1">
		    <label class="control-label">Factor de Correcci&oacute;n:</label>
		  </div>
		  <div class="col-xs-2">
		  	<input class="form-control" type="text" name="txt_factor" id="txt_factor" value="<?php echo $fator_correccion;?>">
		  </div>
		  <div class="col-xs-2">
		    <button class="btn btn-primary" id="btn_reload_mineria">Buscar</button>
		    <button class="btn btn-success" id="btn_generar_cartas_muestra">Generar Muestra</button>
		    <button class="btn btn-success" id="btn_generar_todas_cartas">Generar Todas</button>
		  </div>
		</div>
		<div class="row">
			<div class="col-xs-1" style="width: 200px;"></div>
			<div class="col-xs-1">
			P&aacute;gina inicio:
			</div>
			<div class="col-xs-1">
		    <input class="form-control" style="width: 80px;" type="text" name="txt_page_start" id="txt_page_start" value="1">
		    </div>
		    <div class="col-xs-1">
			P&aacute;gina fin:
			</div>
			<div class="col-xs-1">
		  	<input class="form-control" style="width: 80px;" type="text" name="txt_page_end" id="txt_page_end" value="<?php echo $count_target2;?>">
		  	</div>
		  	<div class="col-xs-2">
			<button class="btn btn-success" id="btn_generar_cartas">Generar Cartas</button>
			</div>
			<div class="col-xs-1">
			Suministro:
			</div>
			<div class="col-xs-1">
		  	<input class="form-control" style="width: 80px;" type="text" name="txt_suministro" id="txt_suministro" value="">
		  	</div>
		  	<div class="col-xs-1">
			<button class="btn btn-success" id="btn_generar_carta_by_suministro">Generar Carta</button>
			</div>
		</div>
    </div>

	<h2 class="text-center" style="margin-top: 5px;">Analisis Avanzado con Mineria de Datos</h2>
	<div class="cl-mcont">
		<div class="col-sm-12 col-md-12 text-center">
			Cantidad Total: <label><?php echo $count_total;?></label>
			Cantidad Target 1: <label><?php echo $count_target1;?> (<?php echo number_format(($count_total>0?($count_target1*100)/$count_total:0),2);?>%)</label>
			Cantidad Target 2: <label><?php echo $count_target2;?> (<?php echo number_format(($count_total>0?($count_target2*100)/$count_total:0),2);?>%)</label>
			
			<div class="alert alert-info alerta_guardando" style="display: none;">
				<i class="fa fa-info-circle sign"></i><strong>Info!</strong> Estamos guardando las cartas en la base de datos, por favor, no cierre esta ventana. guardando... <img
						src="<?php echo ENV_WEBROOT_FULL_URL ?>/js/jquery.select2/select2-spinner.gif">
			</div>
			
			<div class="alert alert-success alerta_guardando_ok" style="display: none;">
				<i class="fa fa-check sign"></i><strong>Success!</strong> Cartas guardadas correctamente.
			</div>
			 
			<div class="alert alert-info alerta_info" style="position: absolute;z-index: 1;top: 128px;left: 37px;">
				<button type="button" class="close" data-dismiss="alert" aria-hidden="true"><i class="fa fa-check-square"></i></button>
				<i class="fa fa-info-circle sign"></i><strong>Info!</strong> Selecciona los suministros que no deseas incluir en las cartas.
			</div>
		</div>
	</div>

	<div class="cl-mcont">
		<div class="col-sm-12 col-md-12" style='height: 100%;'>
			<div class="block">


				<div class="content">
					<div class="tabla">
						<div id="digitar" class='farmsmall2'>
							<table id="tabla_reporte_mineria" class="table table-bordered display"
								style="background-color: #fff; color: #000; font-weight: bold; font-size: 14px;">
								<thead>
									<tr>
										<th>Selecciona</th>
										<th>Periodo</th>
										<th>Suministro</th>
										<th>Promedio</th>
										<th>Consumo Ant</th>
										<th>Consumo Actual</th>
										<th>Lectura Ant</th>
										<th>Lectura Actual</th>
										<th>Metodo</th>
										<th>Maxima</th>
										<th>Target 1</th>
										<th>Factor Correcci&oacute;n</th>
										<th>Target 2</th>
										<th>Foto</th>
									</tr>
								</thead>
								<tbody>
									<?php
									$i=1;									
									foreach ($listado as $obj_orden_lectura){
												?>
									<tr>
										<td><input type="checkbox" class="chk_suministro" data-suministro="<?php echo $obj_orden_lectura[0]['suministro']?>"></td>
										<td><?php echo $obj_orden_lectura[0]['pfactura']?></td>
										<td><?php echo $obj_orden_lectura[0]['suministro']?></td>
										<td><?php echo $obj_orden_lectura[0]['promedio']?></td>										
										<td><?php echo number_format($obj_orden_lectura[0]['mes_anterior'],2)?></td>
										<td><?php echo number_format($obj_orden_lectura[0]['mes_actual'],2)?></td>	
										<td><?php echo number_format( $obj_orden_lectura[0]['lecant'],2)?></td>	
										<td><?php echo (int) $obj_orden_lectura[0]['lecturao1']?></td>										
										<td><?php echo $obj_orden_lectura[0]['metodofinal1']?></td>
										<td><?php echo $obj_orden_lectura[0]['maxima']?></td>
										<td><?php echo $obj_orden_lectura[0]['target']?></td>
										<td><?php echo $obj_orden_lectura[0]['factor_correccion']?></td>
										<td><?php echo $obj_orden_lectura[0]['target2']?></td>
										<td><a class="preview" href="data:image/png;base64,<?php echo trim($obj_orden_lectura[0]['foto1']);?>"><img src="data:image/png;base64,<?php echo trim($obj_orden_lectura[0]['foto1']);?>" class="img-thumbnail" style="height:64px;"></a></td>
									</tr>
									<?php
												}
												?>
								</tbody>
							</table>
							<br>
							<br>
							<br>
							<br>
							<br>
							<br>
							<br>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

</div>

<script type="text/javascript">

var oTable = $('#tabla_reporte_mineria').dataTable({
	"bProcessing": true,
	"bDeferRender": true,
	"bServerSide": true,
	"paging": false,
	"bSort": false,
	"iDisplayLength" : 20,
	"sAjaxSource": ENV_WEBROOT_FULL_URL+"Reportes/ajax_reclamos/<?php echo $idciclo;?>/<?php echo $fator_correccion;?>",
	"aLengthMenu": [
					[20,50,100,300,500,800],
					[20,50,100,300,500,800]
					],
	"fnDrawCallback" : fnEachTd,
	"sDom": '<"top"p>T<"clear"><"datatables_filter"lrf><"bottom">ti',
 	"oTableTools": {
 		"sSwfPath":  ENV_WEBROOT_FULL_URL+"js/jquery.datatables/extras/TableTools/media/swf/copy_csv_xls_pdf.swf"
	},
	});

	function fnEachTd(oSettings){

		imagePreview();
		$('input').iCheck({
	        checkboxClass: 'icheckbox_square-blue checkbox',
	        radioClass: 'iradio_square-blue'
	    });
		$('input').on('ifClicked', function(event){
	    	$.ajax({
				url : ENV_WEBROOT_FULL_URL+"reportes/no_carta_reclamos/"+$(this).data('suministro'),
				type : "POST",
				dataType : 'json',
				success : function(data, textStatus, jqXHR) {
					if(data.success==true){
						console.log(data.success);
					}
				}
			}); 
	    });
	}

	$('#id_ciclo').select2({
    	width: '100%'
    	});

	$('#btn_reload_mineria').on('click',function(){
    	if($('#id_ciclo').val()!='0'){
    		window.open(ENV_WEBROOT_FULL_URL+'reportes/reclamos/'+$('#id_ciclo').val()+'/'+$('#txt_factor').val(),'_top');
    	}else{
    		window.open(ENV_WEBROOT_FULL_URL+'reportes/reclamos','_top');
    	}
    	
    });

	$('#btn_generar_cartas').on('click',function(){
    	if($('#id_ciclo').val()!='0'){
    		window.open(ENV_WEBROOT_FULL_URL+'reportes/generar_carta_pdf_multiple/'+$('#id_ciclo').val()+'/'+$('#txt_factor').val()+'/'+$('#txt_page_start').val()+'/'+$('#txt_page_end').val()+'.pdf','_blank');
    		if(confirm('Desea guardar las cartas en la base de datos?')){
        		$('.alerta_guardando').show();
        		$('.alerta_info').css('top','182px');
	    		$.ajax({
					url : ENV_WEBROOT_FULL_URL+'reportes/guardar_carta_reclamos/'+$('#id_ciclo').val()+'/'+$('#txt_factor').val()+'/'+$('#txt_page_start').val()+'/'+$('#txt_page_end').val(),
					type : "POST",
					dataType : 'json',
					success : function(data, textStatus, jqXHR) {
						if(data.success==true){
							$('.alerta_guardando').hide();
							$('.alerta_guardando_ok').show();
						}
					}
				});
    		}
    	}else{
    		alert('Seleccione un ciclo.');
    	}
    	
    });

	$('#btn_generar_cartas_muestra').on('click',function(){
    	if($('#id_ciclo').val()!='0'){
    		window.open(ENV_WEBROOT_FULL_URL+'reportes/generar_carta_pdf_multiple/'+$('#id_ciclo').val()+'/'+$('#txt_factor').val()+'/1/1.pdf','_blank');
    	}else{
    		alert('Seleccione un ciclo.');
    	}
    	
    });

	$('#btn_generar_todas_cartas').on('click',function(){
    	if($('#id_ciclo').val()!='0'){
    		window.open(ENV_WEBROOT_FULL_URL+'reportes/generar_carta_pdf_multiple/'+$('#id_ciclo').val()+'/'+$('#txt_factor').val()+'/1/<?php echo $count_target2;?>.pdf','_blank');
    		if(confirm('Desea guardar las cartas en la base de datos?')){
    			$('.alerta_guardando').show();
    			$('.alerta_info').css('top','182px');
	    		$.ajax({
					url : ENV_WEBROOT_FULL_URL+'reportes/guardar_carta_reclamos/'+$('#id_ciclo').val()+'/'+$('#txt_factor').val()+'/1/<?php echo $count_target2;?>',
					type : "POST",
					dataType : 'json',
					success : function(data, textStatus, jqXHR) {
						if(data.success==true){
							$('.alerta_guardando').hide();
							$('.alerta_guardando_ok').show();
						}
					}
				});
    		}
    	}else{
    		alert('Seleccione un ciclo.');
    	}
    	
    });

	
	$('#btn_generar_carta_by_suministro').on('click',function(){
    	if($('#txt_suministro').val()!='0'){
    		window.open(ENV_WEBROOT_FULL_URL+'reportes/carta_pdf/'+$('#txt_suministro').val()+'.pdf','_blank');
    	}else{
    		alert('Ingrese un Suministro.');
    	}
    	
    });
</script>
<script type="text/javascript" src="<?php echo ENV_WEBROOT_FULL_URL ?>js/jquery.imgpreview/jquery.imgpreview.full.js"></script>