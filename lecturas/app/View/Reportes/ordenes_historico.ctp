<?php 
if (!isset($NEGOCIO)) {
    header("Location:".ENV_WEBROOT_FULL_URL."/Usuarios/logout");
    die();
}
?>
<?php 
if ($id_usuario == 25 || $id_usuario == 131){
	$permiso_lectura = 'false';
}else {
	$permiso_lectura = 'true';
}
echo $this->element('menu',array('active'=>'reporte_ordenes_historico', 'open_report'=>'true', 'permiso_lectura' =>$permiso_lectura, 'id_usuario' =>$id_usuario));
?>
<style>
/* Aliniear Modal a la Izquierda */
.fancybox-wrap {
	left: 52px !important;
	width: 532px !important;	
}
</style>
<div id="arbol" class="page-aside app filters">
	<div class="fixed nano nscroller has-scrollbar">
		<div class="content">
			<div class="header">
			
				<button class="navbar-toggle" data-target=".app-nav"
					data-toggle="collapse" type="button">
					<span class="fa fa-chevron-down"></span>
				</button>
				<h2 class="page-title">Filtros</h2>
			</div>
			
			<div class="app-nav collapse">

				
					<div class="form-group">
		              <label class="control-label">Por Periodo de Factura:</label>              
		              	<?php 
			        			  	echo '<select class="select2" name="pfactura" id="pfactura">
			        			  	<option value="0">Actual</option>';
		                                             //   var_dump($listar_unidadneg);exit;
			        				foreach ($listar_pfacrura as $v){
									if($pfactura==$v['0']['pfactura']){
										$selected = 'selected';
									}else{
										$selected = '';
									}
			        				echo '<option value="'.$v['0']['pfactura'].'" '.$selected.'>'.$v['0']['pfactura'].'</option>';
			        				}
			        				echo '</select>';	
			        		?>        			
		            </div>
		          	<?php 
		            if(strpos($criterio, 'F') !== false){
						$show_tab2 = 'display: block;';
					}else{
						$show_tab2 = 'display:none;';
					}
					if(strpos($criterio, 'U') !== false){
						$show_tab1 = 'display: block;';
					}else{
						$show_tab1 = 'display:none;';
					}
					if(strpos($criterio, 'L') !== false){
						$show_tab_lecturista = 'display: block;';
					}else{
						$show_tab_lecturista = 'display:none;';
					}
					if(strpos($criterio, 'S') !== false){
						$show_tab_suministro = 'display: block;';
					}else{
						$show_tab_suministro = 'display:none;';
					}
		            ?>
	            
	            	<div class="form-group">
						<label class="control-label">Por :</label> <input
							id="cbo_criterio_evaluacion" name="cbo_criterio_evaluacion"
							style="width: 240px;" placeholder="Seleccione una opcion">
					</div>

					<div class="por_ubicacion" style="<?php echo $show_tab1;?>">


						<div class="form-group">
							<label class="control-label">Por Unidad Negocio:</label>              
		              	<?php 
		        			  	echo '<select class="" name="unidadneg" id="unidadneg">
		        			  	<option value="0">Todos</option>';
		        				foreach ($listar_unidadneg as $v){
									if($unidadneg==$v['0']['unidadneg']){
										echo '<option value='.$v['0']['unidadneg'].' selected>'.$v['0']['nombreunidadnegocio'].'</option>';
									}else{
										echo '<option value='.$v['0']['unidadneg'].'>'.$v['0']['nombreunidadnegocio'].'</option>';
									}
		        				}
		        				echo '</select>';	
		        		?>        			
		            </div>

						<div class="form-group">
							<label class="control-label">Por Ciclo:<span class="ciclo_loading"
								style="display: none;"><img
									src="<?php echo ENV_WEBROOT_FULL_URL ?>/js/jquery.select2/select2-spinner.gif"></span></label>              
		              	<?php 
		        			  	echo '<select class="" name="id_ciclo" id="id_ciclo">
		        			  	<option value="0">Todos</option>';
		        				foreach ($listar_ciclo as $v){
									$str_ciclo_actual = '';
									foreach ($arr_ciclos_actuales as $ca){
										if($v['0']['idciclo']==$ca['0']['idciclo']){
											$str_ciclo_actual = ' (Actual)';
										}
									}
									if($ciclo==$v['0']['idciclo']){
										echo '<option value='.$v['0']['idciclo'].' selected>'.$v['0']['idciclo'].' - '.$v['0']['nombciclo'].$str_ciclo_actual.'</option>';
									}else{
										echo '<option value='.$v['0']['idciclo'].'>'.$v['0']['idciclo'].' - '.$v['0']['nombciclo'].$str_ciclo_actual.'</option>';	
									}
		        				}
		        				echo '</select>';	
		        		?>        			
		            </div>

						<div class="form-group">
							<label class="control-label">Por <?php if($NEGOCIO == 'CIX'){echo 'Sector';}elseif ($NEGOCIO == 'TAC'){echo 'Zona';}else{echo 'Sector';}?>: <span
								class="sector_loading" style="display: none;"><img
									src="<?php echo ENV_WEBROOT_FULL_URL ?>/js/jquery.select2/select2-spinner.gif"></span></label>              
		              	<?php 
		        			  	echo '<select class="" name="monitoreo_id_sector" id="monitoreo_id_sector">
		        			  	<option value="0">Todos</option>';
		        				foreach ($listar_sector as $v){
									if($sector==$v['0']['sector']){
										echo '<option value='.$v['0']['sector'].' selected>'.$v['0']['sector'].'</option>';
									}else{
										echo '<option value='.$v['0']['sector'].'>'.$v['0']['sector'].'</option>';
									}
		        				}
		        				echo '</select>';	
		        		?>        			
		            </div>
		            
		            <div class="form-group">
		              <label class="control-label"><?php if($NEGOCIO == 'CIX'){echo 'Por Ruta';}elseif ($NEGOCIO == 'TAC'){echo 'Por Libro';}else{echo 'Por Ruta';}?><span class="ruta_loading" style="display:none;"><img src="<?php echo ENV_WEBROOT_FULL_URL;?>/js/jquery.select2/select2-spinner.gif"></span></label>
		              <?php 
		
		        			  	echo '<select class="" name="id_ruta" id="id_ruta">
		        			  	<option value="0">Todos</option>';
		        				foreach ($listar_ruta as $v){
		        					if($ruta==$v['0']['ruta']){
		        						echo '<option value='.$v['0']['ruta'].' selected>'.$v['0']['ruta'].' - '.$v['0']['nombruta'].'</option>';
		        					}else{
		        						echo '<option value='.$v['0']['ruta'].'>'.$v['0']['ruta'].' - '.$v['0']['nombruta'].'</option>';
		        					}
		        				}
		        				echo '</select>';	
		        		?>
		              
		            </div>
					</div>

					<div class="por_fecha_asignacion" style="<?php echo $show_tab2;?>">
						<div class="form-group">
							<label class="control-label">Por Fecha de Asignaci&oacute;n
								(Inicio) :</label>
							<div class="input-group date datetime" data-min-view="2"
								data-date-format="yyyy-mm-dd">
								<input id="dpt_date_start" class="form-control" size="16"
									type="text" value="<?php echo $date_start;?>" readonly> <span
									class="input-group-addon btn btn-primary"><span
									class="glyphicon glyphicon-th"></span></span>
							</div>
						</div>

						<div class="form-group">
							<label class="control-label">Por Fecha de Asignaci&oacute;n (Fin)
								:</label>
							<div class="input-group date datetime" data-min-view="2"
								data-date-format="yyyy-mm-dd">
								<input id="dpt_date_end" class="form-control" size="16"
									type="text" value="<?php echo $date_end;?>" readonly> <span
									class="input-group-addon btn btn-primary"><span
									class="glyphicon glyphicon-th"></span></span>
							</div>
						</div>
					</div>

					<div class="por_lecturista" style="<?php echo $show_tab_lecturista;?>">
						<div class="form-group">
							<label class="control-label">Por Lecturista:</label>
		             <?php 
		        			  	echo '<select class="select2" name="lecturista" id="id_lecturista">
		        			  	<option value="0">Todos</option>';
		        				foreach ($listar_empleados as $empleados){
									if($lecturista==$empleados['0']['id']){
										echo '<option value='.$empleados['0']['id'].' selected>'.$empleados['0']["nombre"] .' - '.$empleados['0']["nomusuario"].'</option>';
									}else{
										echo '<option value='.$empleados['0']["id"].'>'.$empleados['0']["nombre"] .' - '.$empleados['0']["nomusuario"].'</option>';
									}        				
		        				}
		        				echo '</select>';	
		        			?>
		            </div>
					</div>

					<div class="por_suministro" style="<?php echo $show_tab_suministro;?>">
						<div class="form-group" style="margin-bottom: 0px;">
							<label class="control-label"><?php if($NEGOCIO == 'cix'){echo 'Por Suministro';}elseif ($NEGOCIO == 'tac'){echo 'Por Contrato';}else{echo 'Por Suministro';}?></label>
							<input type="text" class="form-control"
								id="txt_search_by_suministro" name="txt_search_by_suministro"
								placeholder="Suministro"
								value="<?php echo ($suministro!='0'?$suministro:'');?>">
						</div>
					</div>

					<div class="panel-group accordion" id="accordion"
						style="margin-bottom: 0px;">
						<div class="">
							<div class="panel-heading">
								<h4 class="panel-title">
									<a data-toggle="collapse" data-parent="#accordion"
										href="#collapseOne" class="collapsed"> <i
										class="fa fa-angle-right"></i> Filtros Avanzados
									</a>
								</h4>
							</div>
							<div id="collapseOne" class="panel-collapse collapse"
								style="height: auto;">
								<div class="">
									<input id="filter_advance" name="filter_advance"
										style="width: 240px;" placeholder="Seleccione una opcion">

									<div class="form-group">
										<label class="control-label">Por Consumo</label><br> Desde: <input
											type="text" class="form-control" id="txt_consumo_min"
											name="txt_consumo_min" placeholder="Minimo" value="<?php echo (($consumo_min!='0')?$consumo_min:'');?>"><br> Hasta: &nbsp;<input
											type="text" class="form-control" id="txt_consumo_max"
											name="txt_consumo_max" placeholder="Maximo" value="<?php echo (($consumo_max!='0')?$consumo_max:'');?>">
									</div>
								</div>
							</div>
						</div>
					</div>

					<div class="form-group">
						<input
						type="hidden" class="form-control" id="txt_id_xml"
						name="txt_id_xml" placeholder="Id XmL" value="<?php echo (($id_xml!='0')?$id_xml:'');?>">

						<button class='btn btn-success' id='buscar-inconsistencia-lectura'>Buscar</button>
					</div>
					<br/>
					<br/>
					<br/>
					<br/>
					<br/>
					<br/>


			</div>
		</div>
	</div>
</div>


<div class="container-fluid" id="pcont">
	<div class="page-head" style="padding: 0px;">
		<ol class="breadcrumb">
			<li><button type="button"
					class="btn-xs btn-default arbol-top-lecturas">Arbol Filtros</button></li>
			<li><a href="#">Ordenes de Lectura</a></li>
			<li><a href="#">Historico</a></li>
			<li class="active">Lecturas</li>
		</ol>
	</div>
	<!-- <div class="cl-mcont">-->
	<div class="" style="margin-top:20px;">
		<div class="col-md-12 col-md-12">
			<div class="block-flat" >

				<div id="inco" class='farmsmall2'>
					<div class="content" style="padding-top: 0px;">
					
						<button class="btn btn-info" id="exportar-xls">Exportar Excel</button>
					
						<div class="content" id="contenido_digitar2"></div>
						
						<div class="scroll-table-inconsistecia" style='position: relative; max-width: 300px; min-width: 100%; overflow-y: hidden; height: 500px; overflow-y: auto;'>
							<table id="tabla-reporte-ordenes-historico" class="table table-bordered tableWithFloatingHeader2"
								style="width: 100%; background-color: #fff; color: #000; font-weight: bold; font-size: 5px;">
								<thead>
									<tr>
										<th><a href="javascript:void(0);" class="btn-tooltip"
											data-toggle="tooltip" data-placement="top"
											title="Orden de Lecturas">ID</a></th>
										<th>pfactura</th>
										<th><?php if($NEGOCIO == 'cix'){echo 'Suministro';}elseif ($NEGOCIO == 'tac'){echo 'Contrato';}else{echo 'Suministro';}?></th>
										<th>Medidor</th>
										<th>Cliente</th>
										<th>Direcci&oacute;n</th>
										<th>Lecturista</th>
										<th>Ciclo</th>
										<th>Sector</th>
										<th>Ruta</th>
										<th>Lectura</th>
										<th>Obs.</th>
										<th>Monto Consumo</th>
										<th>Fecha Ejecuci&oacute;n</th>
										<th>Fecha Asignaci&oacute;n</th>
										<th>Orden</th>
										<th>Orden Ruta</th>
										<th>Resultado</th>
										<th>lcorrelati</th>
										<th>OT</th>
									</tr>
								</thead>
							</table>
						</div>
					</div>
				</div>

			</div>
		</div>
		<div id='detalle_lectura_suministro' class="col-sm-6 col-md-6"></div>

	</div>
</div>

<?php echo $this->element('dialogo_suministro');?>

<script type="text/javascript">														
	require(['ajax/inconsistencia'],function(){

		<?php 
		if(isset ( $criterio ) && $criterio != '') {
		?>
			$('#cbo_criterio_evaluacion').select2('data', [
			<?php if(strpos($criterio, 'F') !== false){ echo "{id: 'F', text: 'Fecha de Asignación'},";}
			if(strpos($criterio, 'U') !== false){echo "{id: 'U', text: 'Ubicación Geográfica'},";}
			if(strpos($criterio, 'L') !== false){echo "{id: 'L', text: 'Lecturista'},";}
			if(strpos($criterio, 'S') !== false){echo "{id: 'S', text: 'Suministro'}";}?>
			] );
			<?php
		}
		?>

		<?php 
		if(isset ( $filter_advance ) && $filter_advance != '') {
		?>
			$('#filter_advance').select2('data', [
			<?php if(strpos($filter_advance, 'LSC') !== false){ echo "{ id: 'LSC', text: 'Pendiente de Lectura'},";}
			if(strpos($filter_advance, 'OBSSL') !== false){echo "{ id: 'OBSSL', text: 'Observaciones Sin Lectura'},";}
			if(strpos($filter_advance, 'OBSCL') !== false){echo "{ id: 'OBSCL', text: 'Observaciones Con Lectura'},";}
			if(strpos($filter_advance, 'OBS99') !== false){echo "{ id: 'OBS99', text: 'Observaciones 99'},";}
			if(strpos($filter_advance, 'LAMA') !== false){echo "{ id: 'LAMA', text: 'Lectura Actual Menor que la Anterior'},";}
			if(strpos($filter_advance, 'CFR') !== false){echo "{ id: 'CFR', text: 'Consumo Fuera de Rango'},";}
			if(strpos($filter_advance, 'OBSSC') !== false){echo "{ id: 'OBS', text: 'Con Observacion sin corregir' },";}
			if(strpos($filter_advance, 'CORR') !== false){echo "{ id: 'CORR', text: 'Corregidos' },";}
			if(strpos($filter_advance, 'I') !== false){echo "{ id: 'I', text: 'Inconsistentes' },";}
			if(strpos($filter_advance, 'CC') !== false){echo "{ id: 'CC', text: 'Consumo cero' },";}
			if(strpos($filter_advance, 'C50MA') !== false){echo "{ id: 'C50MA', text: 'Consumo 50% menor que el anterior' },";}
			if(strpos($filter_advance, 'C100MA') !== false){echo "{ id: 'C100MA', text: 'Consumo 100% mayor que el anterior' },";}
			if(strpos($filter_advance, 'CN') !== false){echo "{ id: 'CN', text: 'Clientes Nuevos' }";}?>
			] );
			<?php
		}
		?>

		<?php 
		if(isset ( $criterio ) && $criterio != '' && $criterio != 'U') {
		?>
		$("#buscar-inconsistencia-lectura").click();
		<?php }?>

		var oTable = $('#tabla-reporte-ordenes-historico').dataTable();
		$('body').off('click','#buscar-inconsistencia-lectura');
      	$('body').on('click','#buscar-inconsistencia-lectura',function(e){
			e.stopImmediatePropagation();
			e.preventDefault();

			pfactura = $('#pfactura').val();
			if($.trim(pfactura) == ''){
				pfactura = '0';
			}
			
			criterio = $('#cbo_criterio_evaluacion').val();
			if($.trim(criterio) == ''){
				criterio = '0';
			}
	 		date_start = $('#dpt_date_start').val();
	 		date_end = $('#dpt_date_end').val();
	 		unidadneg = $('#unidadneg').val();
	 		id_ciclo = $('#id_ciclo').val();
	 		id_sector = $('#monitoreo_id_sector').val();
	 		id_ruta = $('#id_ruta').val();
               		
      		lecturista = $("#id_lecturista").val();
      		search_suministro = $( "#txt_search_by_suministro" ).val();
			
			if(search_suministro == ''){
			search_suministro = '0';
			}else{
			search_suministro = $( "#txt_search_by_suministro" ).val()
			}
			
			consumo_min = $( "#txt_consumo_min" ).val();
			if($.trim(consumo_min) == ''){
				consumo_min = '0';
			}
			consumo_max = $( "#txt_consumo_max" ).val();
			if($.trim(consumo_max) == ''){
				consumo_max = '0';
			}
			id_xml = $( "#txt_id_xml" ).val();
			if($.trim(id_xml) == ''){
				id_xml = '0';
			}
			
			filter_advance = $('#filter_advance').val();
			if($.trim(filter_advance) == ''){
				filter_advance = '0';
			}
      		
      		if(oTable != null || typeof oTable != 'undefined') {
				$('#tabla-reporte-ordenes-historico').dataTable().fnDestroy();
			}
      				     
			$('#tabla-reporte-ordenes-historico').dataTable({
				"bProcessing": true,
				"bDeferRender": true,
				"bServerSide": true,
				"paging": false,
				"bSort": false,
				"iDisplayLength" : 50,							
				"sAjaxSource": ENV_WEBROOT_FULL_URL+"Reportes/ajax_ordenes_historico/"+criterio+"/"+date_start+"/"+date_end+"/"+unidadneg+"/"+id_ciclo+"/"+id_sector+"/"+id_ruta+"/"+search_suministro+"/"+lecturista+"/"+consumo_min+"/"+consumo_max+"/"+filter_advance+"/"+id_xml+"/"+pfactura,
				"aLengthMenu": [
								[50,100, 200, 500, 1000, 5000],
								[50,100, 200, 500, 1000, 5000]
								],
				"fnDrawCallback" : fnEachTd,
				"sDom": '<"top"p>T<"clear"><"datatables_filter"lrf><"bottom">ti',
	         	"oTableTools": {
	         		"sSwfPath":  ENV_WEBROOT_FULL_URL+"js/jquery.datatables/extras/TableTools/media/swf/copy_csv_xls_pdf.swf"
				},
			});
			function fnEachTd(oSettings){
        		/*$('#tabla-reporte-ordenes-historico tbody').find('tr').each(function(){     						
          		    var td1 = $(this).children('td').eq(0);
          		    td1.addClass('hidden');

          		});*/
        		$('.btn-tooltip').tooltip();
        		$('[data-popover="popover"]').popover({
        	        html : true});

        		if(search_suministro != 0){
        			$("#tabla-reporte-ordenes-historico tr:contains('" + $( "#txt_search_by_suministro" ).val() + "')").css('background-color','#2494F2');

        			console.log($("#tabla-reporte-ordenes-historico tr:contains('" + $( "#txt_search_by_suministro" ).val(search_suministro) + "')"));

        			console.log(search_suministro);
        			
        		}
    		}
	        $('#tabla-reporte-ordenes-historico').css( "width","100%" ); 

	        $('.arbol-top-lecturas').click();
      	});
                  	

		$('.md-trigger').modalEffects();

		$('.dataTables_filter input').addClass('form-control').attr('placeholder', 'Algo que buscar?');
		$('.dataTables_length select').addClass('form-control');


		$('body').off('click','#exportar-xls');
      	$('body').on('click','#exportar-xls',function(e){
			e.stopImmediatePropagation();
			e.preventDefault();

			pfactura = $('#pfactura').val();
			if($.trim(pfactura) == ''){
				pfactura = '0';
			}
			
			criterio = $('#cbo_criterio_evaluacion').val();
			if($.trim(criterio) == ''){
				criterio = '0';
			}
	 		date_start = $('#dpt_date_start').val();
	 		date_end = $('#dpt_date_end').val();
	 		unidadneg = $('#unidadneg').val();
	 		id_ciclo = $('#id_ciclo').val();
	 		id_sector = $('#monitoreo_id_sector').val();
	 		id_ruta = $('#id_ruta').val();
               		
      		lecturista = $("#id_lecturista").val();
      		search_suministro = $( "#txt_search_by_suministro" ).val();
			
			if(search_suministro == ''){
			search_suministro = '0';
			}else{
			search_suministro = $( "#txt_search_by_suministro" ).val()
			}
			
			consumo_min = $( "#txt_consumo_min" ).val();
			if($.trim(consumo_min) == ''){
				consumo_min = '0';
			}
			consumo_max = $( "#txt_consumo_max" ).val();
			if($.trim(consumo_max) == ''){
				consumo_max = '0';
			}
			id_xml = $( "#txt_id_xml" ).val();
			if($.trim(id_xml) == ''){
				id_xml = '0';
			}
			
			filter_advance = $('#filter_advance').val();
			if($.trim(filter_advance) == ''){
				filter_advance = '0';
			}

			window.open(ENV_WEBROOT_FULL_URL+"Reportes/ordenes_historico_xls/"+criterio+"/"+date_start+"/"+date_end+"/"+unidadneg+"/"+id_ciclo+"/"+id_sector+"/"+id_ruta+"/"+search_suministro+"/"+lecturista+"/"+consumo_min+"/"+consumo_max+"/"+filter_advance+"/"+id_xml+"/"+pfactura,"_new");
      	});
		
	});
</script>
