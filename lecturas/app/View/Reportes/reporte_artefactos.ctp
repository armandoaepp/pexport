<?php 
if (!isset($NEGOCIO)) {
    header("Location:".ENV_WEBROOT_FULL_URL."/Usuarios/logout");
    die();
}
?>
<?php 
$ver_menu = 'activo';
if (isset($id_usuario) && $id_usuario == 25){
	$permiso_lectura = 'true';
}elseif (isset($id_usuario) && $id_usuario == 64) {
	$permiso_lectura = 'true';
	$ver_menu = 'no_activo';
}else{
	$permiso_lectura = 'true';
}
if($ver_menu =='activo'){
	echo $this->element('menu',array('active'=>'rep_historico', 'open_report'=>'true', 'permiso_lectura' =>$permiso_lectura, 'id_usuario ' =>isset($id_usuario)?$id_usuario:null));
}

?>

<div class="container-fluid" id="pcont">
	
	<div class="page-head" style="padding:0px;">
				
				<ol class="breadcrumb">
				  <li><button type="button" class="btn-xs btn-default arbol-top-lecturas" >&Aacute;rbol</button></li>
				  <li><a href="#">Lecturas</a></li>
				  <li class="active">Hist&oacute;rico de suministros</li>
				</ol>
	</div>
	

	<div class="cl-mcont">
		<div class="col-sm-12 col-md-12">
			<div class="panel-group accordion accordion-color" id="accordion2">
					  <div class="panel panel-default">
						<div class="panel-heading">
						  <h4 class="panel-title">
							<a data-toggle="collapse" data-parent="#accordion2" href="#collapse-1" class="">
								<i class="fa fa-angle-right"></i> B&uacute;squeda: Ingresar Suministro(s)
							</a>
						  </h4>
						</div>
						<div id="collapse-1" class="panel-collapse collapse in" style="height: auto;">
						  <div class="panel-body">
							<form
								action="<?= ENV_WEBROOT_FULL_URL?>reportes/reporte_artefactos"
								id="ReporteHistoricoForm" method="post">
								<div style="display: none;">
									<input type="hidden" name="_method" value="POST">
								</div>
		
								<div class="form-group">					                    
					              	<input type="text" class="form-control" id="txt_search_by_suministro_artefacto" name="txt_search_by_suministro_artefacto" placeholder="Suministro">  
					              	<input type="hidden" class="form-control" id="modulo" name="modulo" value = "reporte_suministro_artefacto">       			
					            </div>
			        		
								 <input
									type="submit" class="btn btn-success btn-flat" value="Buscar Suministro">
							</form>
						  </div>
						</div>
					  </div>
					</div>
<?php if(count($arr_suministros)>0){?>
<div class="row">
      <div class="col-sm-12">
        <div class="content" style=" overflow-x: scroll;">
								<table class="table table-bordered" id="datatable-reporte-historico" >
									<thead class="primary-emphasis">
										<tr>
											<th>Pfactura</th>
											<th>Lectura Anterior Ensa</th>
											<th>Lecturao</th>
											<th>Consumo Anterior</th>
											<th>Promedio</th>
											<th>Consumo Actual</th>
											<th>Fecha Lectura</th>
											<th>Tipo Lectura</th>
											<th>Rlecturao</th>
											<th>Foto</th>
										</tr>
									</thead>
									<tbody>
								<?php 
									foreach ($arr_suministros as $suministro){
								?>
									<tr>
										<td><?php echo $suministro['0']["pfactura"];?></td>
										<td><?php echo (int) $suministro['0']["lecant"];?></td>
										<td><?php echo (int) $suministro['0']["lecturao1"];?></td>
										<td><?php echo (int) $suministro['0']["consant"];?></td>
										<td><?php echo (int) $suministro['0']["promedio"];?></td>
										<td><?php echo (int) $suministro['0']["montoconsumo1"];?></td>
										<td><?php echo substr($suministro['0']["fechaejecucion1"],0,19);?></td>
										<td><?php echo $suministro['0']["tipolectura"];?></td>
										<td><?php if($suministro['0']["lecturao2"]==''){}else{ echo (int) $suministro['0']["lecturao2"]; }?></td>
										<?php 
											if($suministro[0]['foto1']!='' or $suministro[0]['foto2']!='' or $suministro[0]['foto3']!=''){
												$btn_foto = '<a class="btn btn-info btn-sm btn fancybox fancybox.iframe" href="'.ENV_WEBROOT_FULL_URL.'/EvaluarInconsistencias/ajax_imagen_por_inconsistencia/'.$suministro[0]['suministro'].'"><i class="fa fa-camera"></i></a>';
											}else{
												$btn_foto = '<a class="btn btn-sm" disabled><i class="fa fa-camera"></i></a>';
											}
										?>
										<td><?php echo $btn_foto;?></td>
									</tr>
								<?php } ?>										
									</tbody>
								</table>							   
					</div>
      </div>    

    </div>
						
<?php }else{ ?>
	
	<div class="alert alert-info alert-white rounded">
	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>
	<div class="icon"><i class="fa fa-info-circle"></i></div>
	<strong>Informaci&oacute;n!</strong> No se encuentran datos registrados para el/los suministro.
	</div>
<?php 	
}?>							 
		
		</div>
		
	</div>
</div>

<script>
$('#datatable-reporte-historico').dataTable({
		"iDisplayLength" : 10,
		"aLengthMenu": [
						[10,20,50],
						[10,20,50]
						],
	});
</script>