<?php 
if (!isset($NEGOCIO)) {
    header("Location:".ENV_WEBROOT_FULL_URL."/Usuarios/logout");
    die();
}
?>
<?php 
if (isset($id_usuario) && $id_usuario == 69){
	$permiso_lectura = 'true';
}else {
	$permiso_lectura = 'false';
}
echo $this->element('menu',array('active'=>'rep_mineria2', 'open_report'=>'true', 'permiso_lectura' =>$permiso_lectura, 'id_usuario ' =>isset($id_usuario)?$id_usuario:null));
?>


<div class="container-fluid" id="pcont">

<div class="page-head" style="padding-bottom: 6px;">
		<div class="row">
			<div class="col-xs-2">
				<h3 style="margin-top: 0px">Filtros:</h3>
			</div>
		  <div class="col-xs-2">
		    <label class="control-label" style="padding-top: 8px;">Por Periodo:</label>
		  </div>
		  <div class="col-xs-2">    
		  		<select class="" name="cbo_pfactura" id="cbo_pfactura">
	        	  	<option value="201407" <?php if($pfactura=='201407'){ echo 'selected';}?>>201407</option>
	        	  	<option value="201405" <?php if($pfactura=='201405'){ echo 'selected';}?>>201405</option>
	        	</select>          
		  </div>
		  <div class="col-xs-2">
		    <button class="btn btn-primary" id="btn_reload_mineria">Buscar</button>
		  </div>
		</div>
    </div>

	<h2 class="text-center" style="margin-top: 5px;">Analisis Avanzado con Mineria de Datos</h2>

	<div class="cl-mcont">
		<div class="col-sm-12 col-md-12" style='height: 100%;'>
			<div class="block">


				<div class="content">
					<div class="tabla">
						<div id="digitar" class='farmsmall2'>
							<table id="tabla_reporte_mineria" class="table table-bordered display"
								style="background-color: #fff; color: #000; font-weight: bold; font-size: 14px;">
								<thead>
									<tr>
										<th>Suministro</th>
										<th>Periodo</th>
										<th>Lectura</th>
										<th>Monto Consumo</th>
										<th>Metodo 1 Pexport</th>
										<th>Metodo 2 Pexport</th>
										<th>Metodo ENSA</th>
										<th>Metodo Mineria 80</th>
										<th>Metodo Mineria 95</th>
										<th>Fuerza Bruta</th>
										<th>Reclamo</th>
										<th>Ver historico</th>
									</tr>
								</thead>
								<tbody>
									<?php
									$i=1;									
									foreach ($listado as $obj_orden_lectura){
												?>
									<tr>
										<td><?php echo $obj_orden_lectura[0]['suministro']?></td>
										<td><?php echo $obj_orden_lectura[0]['pfactura']?></td>
										<td><?php echo number_format($obj_orden_lectura[0]['lecturao'],2)?></td>
										<td><?php echo number_format($obj_orden_lectura[0]['demandaact'],2)?></td>										
										<td><?php echo $obj_orden_lectura[0]['metodo1_pex']?></td>
										<td><?php echo $obj_orden_lectura[0]['metodo2_pex']?></td>
										<td><?php echo $obj_orden_lectura[0]['metodoENSA']?></td>
										<td><?php echo $obj_orden_lectura[0]['metodo_mineria_80']?></td>
										<td><?php echo $obj_orden_lectura[0]['metodo_mineria_95']?></td>
										<td><?php echo $obj_orden_lectura[0]['fuerzabruta']?></td>
										<td><?php echo $obj_orden_lectura[0]['reclamo']?></td>
										<td><a href="<?php echo ENV_WEBROOT_FULL_URL;?>reportes/reporte_historico/<?php echo $obj_orden_lectura[0]['suministro'];?>" target="_blank">ver historico</a></td>
									</tr>
									<?php
												}
												?>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

</div>
<script type="text/javascript">

var oTable = $('#tabla_reporte_mineria').dataTable({
	"bProcessing": true,
	"bDeferRender": true,
	"bServerSide": true,
	"paging": false,
	"bSort": false,
	"iDisplayLength" : 20,
	"sAjaxSource": ENV_WEBROOT_FULL_URL+"Reportes/ajax_mineria2/<?php echo $pfactura;?>",
	"aLengthMenu": [
					[20,50,100,300,500,800],
					[20,50,100,300,500,800]
					],
	"fnDrawCallback" : fnEachTd,
	"sDom": '<"top"p>T<"clear"><"datatables_filter"lrf><"bottom">ti',
 	"oTableTools": {
 		"sSwfPath":  ENV_WEBROOT_FULL_URL+"js/jquery.datatables/extras/TableTools/media/swf/copy_csv_xls_pdf.swf"
	},
	});

	function fnEachTd(oSettings){
	}

	$('#cbo_pfactura').select2({
    	width: '100%'
    	});

	$('#btn_reload_mineria').on('click',function(){
    	if($('#cbo_pfactura').val()!='0'){
    		window.open(ENV_WEBROOT_FULL_URL+'reportes/mineria2/'+$('#cbo_pfactura').val(),'_top');
    	}else{
    		window.open(ENV_WEBROOT_FULL_URL+'reportes/mineria2','_top');
    	}
    	
    });

</script>
