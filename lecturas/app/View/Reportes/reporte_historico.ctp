<?php 
if (!isset($NEGOCIO)) {
    header("Location:".ENV_WEBROOT_FULL_URL."/Usuarios/logout");
    die();
}
?>
<?php 
$ver_menu = 'activo';
if (isset($id_usuario) && $id_usuario == 25){
	$permiso_lectura = 'true';
}elseif (isset($id_usuario) && $id_usuario == 64) {
	$permiso_lectura = 'true';
	$ver_menu = 'no_activo';
}else{
	$permiso_lectura = 'true';
}
if($ver_menu =='activo'){
	echo $this->element('menu',array('active'=>'rep_historico', 'open_report'=>'true', 'permiso_lectura' =>$permiso_lectura, 'id_usuario ' =>isset($id_usuario)?$id_usuario:null));
}

?>
<style>
/* Aliniear Modal a la Izquierda */
.fancybox-wrap {
	margin-left: -20%;
	opacity: 1 !important;
}
</style>
<div class="container-fluid" id="pcont">

	<div class="page-head" style="padding: 0px;">

		<ol class="breadcrumb">
			<li><button type="button"
					class="btn-xs btn-default arbol-top-lecturas">&Aacute;rbol</button></li>
			<li><a href="#">Lecturas</a></li>
			<li class="active">Hist&oacute;rico de Consumos</li>
		</ol>
	</div>


	<div class="cl-mcont">
		<div class="col-sm-12 col-md-12">
			<div class="panel-group accordion accordion-color" id="accordion2">
				<div class="panel panel-default">
					<div class="panel-heading">
						<h4 class="panel-title">
							<a data-toggle="collapse" data-parent="#accordion2"
								href="#collapse-1" class=""> <i class="fa fa-angle-right"></i>
								B&uacute;squeda: Ingresar Suministro
							</a>
						</h4>
					</div>
					<div id="collapse-1" class="panel-collapse collapse in"
						style="height: auto;">
						<div class="panel-body">
							<form
								action="<?= ENV_WEBROOT_FULL_URL?>reportes/reporte_historico"
								id="ReporteHistoricoForm" method="post">
								<div style="display: none;">
									<input type="hidden" name="_method" value="POST">
								</div>

								<div class="form-group">
									<input type="text" class="form-control"
										id="txt_search_by_suministro_historico"
										name="txt_search_by_suministro_historico"
										placeholder="Suministro"
										value="<?php if(isset($suministro) && $suministro!='0') {echo $suministro;}?>">
									<input type="hidden" class="form-control" id="modulo"
										name="modulo" value="reporte_historico">
								</div>

								<input type="submit" class="btn btn-success btn-flat"
									value="Buscar Suministro">
							</form>
						</div>
					</div>
				</div>
			</div>
<?php if(count($arr_suministros)>0 || count($obj_suministro)>0){?>
<div class="row">
				<div class="col-sm-12">
					<div class="tab-container">
						<ul class="nav nav-tabs">
							<li class="active"><a data-toggle="tab" href="#home">Informaci&oacute;n
									General</a></li>
							<li class=""><a data-toggle="tab" href="#settings">Hist&oacute;rico
									de Consumos y Lecturas</a></li>
						</ul>
						<div class="tab-content">
							<div id="home" class="tab-pane cont active">
								<div class="content" style="overflow-x: scroll;">
								<div class="col-sm-7">
									<div class="pull-right">
										<a href="<?php echo ENV_WEBROOT_FULL_URL;?>reportes/resumen_historico_pdf/<?php echo (count($obj_suministro)>0?$obj_suministro[0]['0']['suministro']:$arr_suministros[0][0]['suministro']);?>.pdf" target="_blank">
										<i class="glyphicon glyphicon-print"></i> Imprimir Cargo</a>
									</div>
									<table class="no-border no-strip information">
										<tbody class="no-border-x no-border-y">
											<tr>
												<td style="width: 20%;" class="category"><strong>B&uacute;squeda</strong></td>
												<td>
													<table class="no-border no-strip skills">
														<tbody class="no-border-x no-border-y">
															<tr>
																<td style="width: 20%;"><b>Suministro</b></td>
																<td><?php echo (count($obj_suministro)>0?$obj_suministro[0]['0']['suministro']:$arr_suministros[0][0]['suministro']);?></td>
															</tr>
															<tr>
																<td style="width: 20%;"><b>Direcci&oacute;n</b></td>
																<td><?php echo (count($obj_suministro)>0?$obj_suministro[0]['0']['direccion']:$arr_suministros[0][0]['direccion']);?></td>
															</tr>
															<tr>
																<td style="width: 20%;"><b>Medidor</b></td>
																<td><?php echo (count($obj_suministro)>0?$obj_suministro[0]['0']['seriefab']:$arr_suministros[0][0]['seriefab']);?></td>
															</tr>
														</tbody>
													</table>
												</td>
											</tr>
											<tr>
												<td style="width: 20%;" class="category"><strrrong>Datos</strong></td>
												<td>
													<table class="no-border no-strip skills">
														<tbody class="no-border-x no-border-y">
															<tr>
																<td style="width: 20%;"><b>Nombre</b></td>
																<td><?php echo (count($obj_suministro)>0?$obj_suministro[0]['0']['cliente']:$arr_suministros[0][0]['cliente']);?></td>
															</tr>
															<tr>
																<td style="width: 20%;"><b>Sector</b></td>
																<td><?php echo (count($obj_suministro)>0?$obj_suministro[0]['0']['sector']:$arr_suministros[0][0]['sector']);?></td>
															</tr>
															<tr>
																<td style="width: 20%;"><b>Ruta</b></td>
																<td><?php echo (count($obj_suministro)>0?$obj_suministro[0]['0']['ruta']:$arr_suministros[0][0]['ruta']);?></td>
															</tr>
															<tr>
																<td style="width: 20%;"><b>Periodo Inicio</b></td>
																<td><?php echo (count($obj_suministro)>0?$obj_suministro[0]['0']['pinicio']:$arr_suministros[0][0]['pinicio']);?></td>
															</tr>
														</tbody>
													</table>
												</td>
											</tr>
										</tbody>
									</table>
									<div id="comentarios" class="chat-wi" suministro="<?php echo (count($obj_suministro)>0?$obj_suministro[0]['0']['suministro']:$arr_suministros[0][0]['suministro']);?>" pfactura="<?php echo (count($obj_suministro)>0?$obj_suministro[0]['0']['pfactura']:$arr_suministros[0][0]['pfactura']);?>">
									<div id="comentario-reaload">
									<div>Comentarios:</div>
									<ul class="timeline">
										<?php echo $this->element('ReporteHistorico/comentario');?>
									</ul>
									<div class="chat-in" style="border: 1px solid #DADADA">
										<input type="submit" value="Enviar" class="btn btn-info pull-right button-comentario" />
										<div class="input">
											<input type="text" class="text-comentario" placeholder="Enviar un Comentario" name="msg" />
											<div class='fileupload fileupload-new' data-provides='fileupload'>
												<div class='fileupload-preview thumbnail' style='width:20%;height:20%;'></div>
												<div class='uneditable-input span4'><i class='icon-file fileupload-exists'></i>
													<label class="btn btn-primary" for="archivoImage">
													    <input id="archivoImage" type="file" style="display:none;">
													    Escoja una imagen
													</label>
												</div>
									            <br>
									        	<a href='#' class='btn btn-danger fileupload-exists' data-dismiss='fileupload'>Remover Archivo</a>
											</div>
										</div>
									</div>
									<ul class="parsley-error-list"><li class="required" style="display: list-item;"></li></ul>
									</div>
									</div>
								</div>
								<div class="col-sm-5">

									<table>
										<thead class="primary-emphasis">
											<tr>
												<th><b>Suministro</b></th>
												<th><?php echo (count($obj_suministro)>0?$obj_suministro[0]['0']['suministro']:$arr_suministros[0][0]['suministro']);?></th>
											</tr>
										</thead>
									</table>
									<div id="container" style="width: 100%;height: 300px;"></div>
								
									<?php if((count($arr_suministros)>0 && $arr_suministros[0][0]['latitud1']!='') || (count($obj_suministro)>0 && $obj_suministro[0]['0']['latitud1']!='')){ ?>
        							<div class="block widget-notes">
										<div class="header dark">
											<h4>Mapa del Predio</h4>
										</div>
          								<div style="text-align: center;">
							           <?php
										echo $this->element ( 'mapa_suministro', array (
											'latitud' => (count($obj_suministro)>0?$obj_suministro[0]['0']['latitud1']:$arr_suministros[0][0]['latitud1']),
											'longitud' => (count($obj_suministro)>0?$obj_suministro[0]['0']['longitud1']:$arr_suministros[0][0]['longitud1'])));
										?>
										</div>
							       
							        </div>
							        <?php
							        }?>
								</div>
								</div>
							</div>

							<div id="settings" class="tab-pane">
								<div class="content" style="overflow-x: scroll;">
									<table class="table table-bordered"
										id="datatable-reporte-historico">
										<thead class="primary-emphasis">
											<tr>
												<th>Pfactura</th>
												<th>Lectura Anterior Ensa</th>
												<th>Lecturao</th>
												<th>Consumo Anterior</th>
												<th>Promedio</th>
												<th>Consumo Actual</th>
												<th>C&oacute;digo Actual</th>
												<th>Fecha Lectura</th>
												<th>Tipo Lectura</th>
												<th>Rlecturao</th>
												<th>T&eacute;cnico</th>
												<th>Origen</th>
												<th>Foto</th>
												<th>Comentario</th>
											</tr>
										</thead>
										<tbody>
								<?php 
								if(isset($obj_suministro) and count($obj_suministro)>0){
									?>
									<tr>
												<td><?php echo $obj_suministro[0]['0']["pfactura"];?></td>
												<td><?php echo  $obj_suministro[0]['0']["lecant"];?></td>
												<td><?php echo  $obj_suministro[0]['0']["lecturao1"];?></td>
												<td><?php echo  $obj_suministro[0]['0']["consant"];?></td>
												<td><?php echo  $obj_suministro[0]['0']["promedio"];?></td>
												<td><?php echo  $obj_suministro[0]['0']["montoconsumo1"];?></td>
												<td><a href="javascript:void(0);" class="btn-tooltip"
													data-toggle="tooltip" data-placement="top"
													title="<?php echo $obj_suministro[0]['0']["descripcion"];?>"><?php echo $obj_suministro[0]['0']["codigofac"];?></a></td>
												<td><?php echo substr($obj_suministro[0]['0']["fechaejecucion1"],0,19);?></td>
												<td><?php echo $obj_suministro[0]['0']["tipolectura"];?></td>
												<td><?php if($obj_suministro[0]['0']["lecturao2"]==''){}else{ echo $obj_suministro[0]['0']["lecturao2"]; }?></td>
												<td><?php echo $obj_suministro[0]['0']["nombre"];?></td>
												<td><?php echo ($obj_suministro[0]['0']["up1"]=='1'?'Movil':'Manual');?></td>
										<?php 
											if((isset($obj_suministro[0]['0']['foto1']) && $obj_suministro[0]['0']['foto1']!='') or (isset($obj_suministro[0]['0']['foto2']) && $obj_suministro[0]['0']['foto2']!='') or (isset($obj_suministro[0]['0']['foto3']) && $obj_suministro[0]['0']['foto3']!='')){
												$btn_foto = '<a class="btn btn-info btn-sm btn fancybox-reporte-historico fancybox.iframe" href="'.ENV_WEBROOT_FULL_URL.'EvaluarInconsistencias/ajax_imagen_por_inconsistencia/'.$obj_suministro[0]['0']['suministro'].'"><i class="fa fa-camera"></i></a>';
											}else{
												$btn_foto = '<a class="btn btn-sm" disabled><i class="fa fa-camera"></i></a>';
											}
										?>
										<td><?php echo $btn_foto;?></td>
										<td><?php if(count($obj_comentarios)>0){?>
											<a href="javascript:void(0);" id="btn-historico-comentario-suministro" class="btn btn-info" val_suministro = "<?php echo $obj_suministro[0]['0']['suministro'];?>" val_pfactura="<?php echo $obj_suministro[0]['0']["pfactura"];?>"><i class='fa fa-comment'></i></a>
											<?php }else{ echo "<i class='fa fa-comment-o'></i>"; }?></td>
									</tr>
								<?php } ?>
								
								<?php
									foreach ($arr_suministros as $suministro){
								?>
									<tr>
												<td><?php echo $suministro['0']["pfactura"];?></td>
												<td><?php echo  $suministro['0']["lecant"];?></td>
												<td><?php echo  $suministro['0']["lecturao1"];?></td>
												<td><?php echo  $suministro['0']["consant"];?></td>
												<td><?php echo  $suministro['0']["promedio"];?></td>
												<td><?php echo  $suministro['0']["montoconsumo1"];?></td>
												<td><a href="javascript:void(0);" class="btn-tooltip"
													data-toggle="tooltip" data-placement="top"
													title="<?php echo $suministro['0']["descripcion"];?>"><?php echo $suministro['0']["codigofac"];?></a></td>
												<td><?php echo substr($suministro['0']["fechaejecucion1"],0,19);?></td>
												<td><?php echo $suministro['0']["tipolectura"];?></td>
												<td><?php if($suministro['0']["lecturao2"]==''){}else{ echo $suministro['0']["lecturao2"]; }?></td>
												<td><?php echo $suministro['0']["nombre"];?></td>
												<td><?php echo ($suministro['0']["up1"]=='1'?'Movil':'Manual');?></td>
										<?php 
											if((isset($suministro[0]['foto1']) && $suministro[0]['foto1']!='') or (isset($suministro[0]['foto2']) && $suministro[0]['foto2']!='') or (isset($suministro[0]['foto3']) && $suministro[0]['foto3']!='')){
												$btn_foto = '<a class="btn btn-info btn-sm btn fancybox-reporte-historico fancybox.iframe" href="'.ENV_WEBROOT_FULL_URL.'EvaluarInconsistencias/ajax_imagen_por_inconsistencia/'.$suministro[0]['suministro'].'/'.$suministro[0]['pfactura'].'"><i class="fa fa-camera"></i></a>';
											}else{
												$btn_foto = '<a class="btn btn-sm" disabled><i class="fa fa-camera"></i></a>';
											}
										?>
										<td><?php echo $btn_foto;?></td>
										<td><?php if(isset($arr_comentarios['0']["count_comentario"]) && $arr_comentarios['0']["count_comentario"]>0){?>
											<a href="javascript:void(0);" id="btn-historico-comentario-suministro" class="btn btn-info" val_suministro = "<?php echo $suministro['0']["suministro"];?>" val_pfactura = "<?php echo $suministro['0']["pfactura"];?>"><i class='fa fa-comment'></i></a>
											<?php }else{ echo "<i class='fa fa-comment-o'></i>"; }?></td>
											</tr>
								<?php } ?>										
									</tbody>
									</table>
								</div>

							</div>
						</div>
					</div>
				</div>

			</div>
						
<?php }else{ ?>
	
	<div class="alert alert-info alert-white rounded">
				<button type="button" class="close" data-dismiss="alert"
					aria-hidden="true">x</button>
				<div class="icon">
					<i class="fa fa-info-circle"></i>
				</div>
				<strong>Informaci&oacute;n!</strong> No se encuentran datos
				registrados para el suministro.
			</div>
<?php 	
}?>							 
		
		</div>

	</div>
</div>

<div id="dialog-comentarios" title="Comentarios por Suministro">
	<span class="dialog_loading" style="display: none;"><img
		src="<?php echo ENV_WEBROOT_FULL_URL ?>/js/jquery.select2/select2-spinner.gif">
		Cargando...</span>
	<div id='comentarios-suministro'></div>
</div>

<script src="http://code.highcharts.com/highcharts.js"></script>
<script src="http://code.highcharts.com/modules/exporting.js"></script>
<script
	src="<?php echo ENV_WEBROOT_FULL_URL ?>js/fancyapps/source/jquery.fancybox.js"
	type="text/javascript"></script>
<script>
$(document).ready(function() {
	$('.fancybox-reporte-historico').fancybox({
	    helpers:  {
	        overlay : null
	    }
	});	
	$('#container').highcharts({
        chart: {
            type: 'column'
        },
        title: {
            text: 'Consumos del suministro: <?php echo (count($obj_suministro)>0?$obj_suministro[0]['0']['suministro']:$arr_suministros[0][0]['suministro']);?>'
        },
        subtitle: {
            text: 'Source: www.pexport.com.pe'
        },
        xAxis: {
            categories: [
		<?php 
		if(isset($obj_suministro) and count($obj_suministro)>0){
			echo $obj_suministro[0]['0']["pfactura"].',';
		}
		foreach ($arr_suministros as $suministro){
				echo $suministro['0']["pfactura"].',';
		}
		?>
            ]
        },
        yAxis: {
            min: 0,
            title: {
                text: 'Consumos(KW)'
            }
        },
        tooltip: {
            headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
            pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                '<td style="padding:0"><b>{point.y:.1f} kw</b></td></tr>',
            footerFormat: '</table>',
            shared: true,
            useHTML: true
        },
        plotOptions: {
            column: {
                pointPadding: 0.2,
                borderWidth: 0
            }
        },
        credits: {
            enabled: false
        },
        series: [{
            name: 'Consumo',
            data: [<?php 
            if(isset($obj_suministro) and count($obj_suministro)>0){
            	echo $obj_suministro[0]['0']["montoconsumo1"].',';
            }
			foreach ($arr_suministros as $suministro){
					echo (int) $suministro['0']["montoconsumo1"].',';
			}
			?>]

        }]
    });       
	$body = $('body');
	$body.off('click','#comentarios .button-comentario');
	$body.on('click','#comentarios .button-comentario',function(){
		var comentarios = $(this).parents('#comentarios');
		comentario = comentarios.find('.text-comentario').val();
		suministro = comentarios.attr('suministro');
		pfactura = comentarios.attr('pfactura');

		var inputFileImage = document.getElementById("archivoImage");
		var file = inputFileImage.files[0];
		var data = new FormData();
		
		data.append('comentario',comentario);
		data.append('suministro',suministro);
		data.append('pfactura',pfactura);
		data.append('archivo',file);
		
		if(comentario!=''){
			comentarios.find('#comentario-reaload').mask("Estamos insertando tu comentario, por favor espere un momento...");
			var targetOffset =comentarios.find('ul.timeline').offset().top-330;
			$('html,body').animate({scrollTop: targetOffset}, 700);
		}
	
		$.ajax({
			url: ENV_WEBROOT_FULL_URL+'reportes/insertar_comentario',
			type: 'post',
			contentType:false,
			data: data,
			processData:false,
			cache:false,
			dataType: 'json'
		}).done(function(data){
			if(data.success == true){
				comentarios.parents('.chat-in').removeClass('parsley-error');
				comentarios.find('.parsley-error-list').children('.required').html('');
				comentarios.find('.text-comentario').val('');
				//$('#comentarios').mask("Estamos listando tu comentario, por favor espere un momento...");
				comentarios.find('ul.timeline').load(ENV_WEBROOT_FULL_URL+'reportes/ajax_listar_comentarios/'+suministro+'/'+pfactura,function(){ 
					comentarios.find('#comentario-reaload').unmask();
				});
				comentarios.$('.fileupload-exists').click();
			}else{
				comentarios.find('#comentario-reaload').unmask();
				$('.text-comentario').parents('.chat-in').addClass('parsley-error');
				$('.text-comentario').parents('#comentarios').find('.parsley-error-list').children('.required').html(data.msg);
			}
		});
	});

	$body.off('click','#btn-historico-comentario-suministro');
	$body.on('click','#btn-historico-comentario-suministro',function(){
		$('#datatable-reporte-historico tr').css({background:''});
		$(this).parents('tr:first').css({background:'#28A1C4'});
	
		var suministro = $(this).attr('val_suministro');
		var pfactura = $(this).attr('val_pfactura');
	
		$('.dialog_loading').show();
		$("#dialog-comentarios" ).dialog({ minWidth: 1000});
		$('#comentarios-suministro').html('');
		$('#comentarios-suministro').load(ENV_WEBROOT_FULL_URL+"reportes/comentarios_por_suministro/"+suministro+'/'+pfactura, function(){
			$('.dialog_loading').hide();	
		});
	});
});
</script>