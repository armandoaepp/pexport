<?php 
if (!isset($NEGOCIO)) {
    header("Location:".ENV_WEBROOT_FULL_URL."/Usuarios/logout");
    die();
}
?>
<?php 
if (isset($id_usuario) && $id_usuario == 25){
	$permiso_lectura = 'false';
}else {
	$permiso_lectura = 'true';
}
echo $this->element('menu',array('active'=>'rep_ordenes_por_observacion', 'open_report'=>'true', 'permiso_lectura' =>$permiso_lectura, 'id_usuario ' =>isset($id_usuario)?$id_usuario:null));
?>

		<div id="arbol" class="page-aside app filters" style="zoom:1;">
      	<div>
        <div class="content">
          <button class="navbar-toggle" data-target=".app-nav" data-toggle="collapse" type="button">
            <span class="fa fa-chevron-down"></span>
          </button>          
          <h2 class="page-title">Filtros</h2>
          <p class="description">Criterios de Busqueda</p>
        </div>
        
        <div class="app-nav collapse">
          <div class="content">
          
          <form	action="<?= ENV_WEBROOT_FULL_URL?>reportes/reporte_ordenes_por_observacion" id="ReporteObsForm" method="post">
						<div style="display: none;">
							<input type="hidden" name="_method" value="POST">
						</div>

						<div class="form-group">
			              <label class="control-label">Por Unidad Negocio:</label>              
			              	<?php 
			        			  	echo '<select class="select2" name="unidadneg" id="unidadneg">
			        			  	<option value="0">Todos</option>';
		                                             //   var_dump($listar_unidadneg);exit;
			        				foreach ($listar_unidadneg as $v){
									if($unidadneg==$v['0']['unidadneg']){
										$selected = 'selected';
									}else{
										$selected = '';
									}
			        				echo '<option value="'.$v['0']['unidadneg'].'" '.$selected.'>'.$v['0']['nombreunidadnegocio'].'</option>';
			        				}
			        				echo '</select>';	
			        		?>        			
			            </div>
			            
			            <div class="form-group">
			              <label class="control-label">Por Periodo de Factura:</label>              
			              	<?php 
			        			  	echo '<select class="select2" name="pfactura" id="pfactura">
			        			  	<option value="0">Actual</option>';
		                                             //   var_dump($listar_unidadneg);exit;
			        				foreach ($listar_pfacrura as $v){
									if($pfactura==$v['0']['pfactura']){
										$selected = 'selected';
									}else{
										$selected = '';
									}
			        				echo '<option value="'.$v['0']['pfactura'].'" '.$selected.'>'.$v['0']['pfactura'].'</option>';
			        				}
			        				echo '</select>';	
			        		?>        			
			            </div>
			            
			            <div class="form-group">
			              <label class="control-label">Por C&oacute;digo de Observaci&oacute;n:</label>              
			              	<?php 
			        			  	echo '<select class="select2" name="obs" id="obs">';
			        				foreach ($arr_codigos_observacion as $v){
									if($obs==$v['0']['obs1']){
										$selected = 'selected';
									}else{
										$selected = '';
									}
			        				echo '<option value="'.$v['0']['obs1'].'" '.$selected.'>'.$v['0']['obs1'].'</option>';
			        				}
			        				echo '</select>';	
			        		?>        			
			            </div>
	        		
						 <input
							type="submit" class="btn btn-info" value="Buscar">
					</form>
          
     </div>

          
        </div>
      </div>
</div>

<div class="container-fluid" id="pcont">

	<div class="page-head" style="padding: 0px;">

		<ol class="breadcrumb">
			<li><button type="button"
					class="btn-xs btn-default arbol-top-lecturas">Arbol</button></li>
			<li><a href="#">Ordenes de Lectura</a></li>
			<li><a href="#">Reporte</a></li>
			<li class="active">Listado de ordenes por C&oacute;digo de Observaci&oacute;n</li>
		</ol>
	</div>



	<div class="cl-mcont" style="width: 1020px;">
		<div class="col-sm-12 col-md-12" style='height: 100%;'>
			<div class="block">


				<div class="content">
					<div class="tabla">
						<div id="digitar" class='farmsmall2'>
							<table id="tabla_listar_personal_para_lecturas" class="table table-bordered display"
								style="background-color: #fff; color: #000; font-weight: bold; font-size: 14px;">
								<thead>
									<tr>
										<th>Ord.</th>
										<th>Sector</th>
										<th>Ruta</th>
										<th>lcorrelatic</th>
										<th>orden ruta</th>
										<th>Cliente</th>
										<th>Direccion</th>
										<th>Suministro</th>
										<th>Medidor</th>
										<th>Lectura</th>
										<th>Obs</th>
									</tr>
								</thead>
								<tbody>
									<?php
									$i=1;									
									foreach ($arr_ordenes_por_observacion as $obj_orden_lectura){
												?>
									<tr>
										<td><?php echo $i++;?></td>
										<td><?php echo $obj_orden_lectura[0]['sector']?></td>
										<td><?php echo $obj_orden_lectura[0]['ruta']?></td>
										<td><?php echo $obj_orden_lectura[0]['lcorrelati']?></td>
										<td><?php echo $obj_orden_lectura[0]['ordenruta']?></td>
										<td><?php echo $obj_orden_lectura[0]['cliente']?></td>
										<td><?php echo $obj_orden_lectura[0]['direccion']?></td>
										<td><?php echo $obj_orden_lectura[0]['suministro']?></td>
										<td><?php echo $obj_orden_lectura[0]['seriefab']?></td>
										<td><?php echo $obj_orden_lectura[0]['lecturao1']?></td>
										<td><?php echo $obj_orden_lectura[0]['obs1']?></td>
									</tr>
									<?php
												}
												?>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

</div>
