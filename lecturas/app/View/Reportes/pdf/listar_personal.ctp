    <div class="header" style=" background-color: #fff;  padding-top: 30px;  padding-bottom: 20px;  padding-left: 20px;  padding-right: 20px; margin-top: -10px; margin-left: -20px; width:700px; border-bottom:1px solid #eee;">

      <span style=" font-family: verdana; font-weight:bold; margin-left:20px;" >sistema de lectura</span> 
     
      <img src="./images<?php echo $logo_contratista;?>" width="150" style="position:absolute; margin-left: 40px;, margin-top: -20px;"> 
    </div>

    <br>

    <table class="table table-bordered display" style="background-color: #fff; color: #000; font-weight: bold; font-size: 14px; margin:15px; border:none; width: 100%;"><tr>
      <td style="border:none; background-color:#eee; font-family:verdana; color:#222; padding:10px 15px; font-size: 17px; width: 320px;">Reporte de asignación de personal</td>
      </tr>
    </table>

        <?php 
        $str_head = '<table id="tabla_listar_personal_para_lecturas" class="table table-bordered display" style="background-color: #fff; color: #000; font-weight: bold; font-size: 14px; margin:15px; border:none;">
        <thead>
          <tr>
            <th style="border:none; background-color:#004e90; font-family:verdana; color:#fff; padding:6px;">Ord.</th>
            <th style="border:none; background-color:#004e90; font-family:verdana; color:#fff; padding:6px;">Nombres</th>
            <th style="border:none; background-color:#004e90; font-family:verdana; color:#fff; padding:6px;">Ciclo</th>
            <th style="border:none; background-color:#004e90; font-family:verdana; color:#fff; padding:6px;">Sector</th>
            <th style="border:none; background-color:#004e90; font-family:verdana; color:#fff; padding:6px;">Ruta</th>
    		<th style="border:none; background-color:#004e90; font-family:verdana; color:#fff; padding:6px;">Fecha Programada</th>
            <th style="border:none; background-color:#004e90; font-family:verdana; color:#fff; padding:6px;">Lecturas Estimadas</th>
            <th style="border:none; background-color:#004e90; font-family:verdana; color:#fff; padding:6px;">Direcciones Referenciales de Sector</th>
          </tr>
        </thead>
        <tbody>';
        $i = 1;
        $tmp_unidadnegocio_id = '';
        $sum_lecturas = 0;
        if(count($listar_orden_lecturas)>0){
        foreach ($listar_orden_lecturas as $obj1){
        	if($tmp_unidadnegocio_id != $obj1[0]['glomas_unidadnegocio_id']){
        		$tmp_unidadnegocio_id = $obj1[0]['glomas_unidadnegocio_id'];
        		$tmp_unidadnegocio = $obj1[0]['glomas_unidadnegocio'];
        		if($i>1){
        		echo '<tr>
	        	<td colspan="6" style="border:none; background-color:#eee; font-family:verdana; color:#222; padding:10px 5px; text-align:center;">Total</td>
	        	<th style="border:none; background-color:#004e90; font-family:verdana; color:#fff; padding:6px; font-size: 18px;">'.$sum_lecturas.'</th>
	        	</tr>';
        		}        		
        		echo '<table class="table table-bordered display" style="background-color: #fff; color: #000; font-weight: bold; font-size: 14px; margin:15px; border:none;"><tr>
	      		<th style="border:none; background-color:#004e90; font-family:verdana; color:#fff; padding:6px; width: 120px;">Unidad Neg.</th>
	      		<td style="border:none; background-color:#eee; font-family:verdana; color:#222; padding:10px 15px; text-align:center;">'.$tmp_unidadnegocio_id.' - '.$tmp_unidadnegocio.'</td>
	        	<th style="border:none; background-color:#004e90; font-family:verdana; color:#fff; padding:6px; width: 120px;">PFactura</th>
	      		<td style="border:none; background-color:#eee; font-family:verdana; color:#222; padding:10px 15px; text-align:center;">'.$obj1[0]['pfactura'].'</td>
	    		</tr></table>'.$str_head;
        	}
        	echo '<tr><td style="border:none; background-color:#eee; font-family:verdana; color:#222; padding:10px 5px; text-align:center;">'.$i.'</td>';
        	echo '<td style="border:none; background-color:#eee; font-family:verdana; color:#222; padding:10px 5px; text-align:center;">'.trim($obj1[0]['lecturista']).'</td>';
        	echo '<td style="border:none; background-color:#eee; font-family:verdana; color:#222; padding:10px 5px; text-align:center;">'.$obj1[0]['idciclo'].'</td>';
        	echo '<td style="border:none; background-color:#eee; font-family:verdana; color:#222; padding:10px 5px; text-align:center;">'.$obj1[0]['sector'].'</td>';
        	echo '<td style="border:none; background-color:#eee; font-family:verdana; color:#222; padding:10px 5px; text-align:center;">'.$obj1[0]['ruta'].'</td>';
        	echo '<td style="border:none; background-color:#eee; font-family:verdana; color:#222; padding:10px 5px; text-align:center;">'.substr($obj1[0]['fecha'],0,10).'</td>';
        	echo '<td style="border:none; background-color:#eee; font-family:verdana; color:#222; padding:10px 5px; text-align:center;">'.$obj1[0]['cantidad'].'</td>';
        	$arr_dires = explode(',',$obj1[0]['direccion']);
        	echo '<td style="border:none; background-color:#eee; font-family:verdana; color:#222; padding:10px 5px; text-align:center;">'.utf8_decode($arr_dires[0]).'</td></tr>';
        	$i++;
        	$sum_lecturas += $obj1[0]['cantidad'];
        }
        echo '<tr>
        	<td colspan="6" style="border:none; background-color:#eee; font-family:verdana; color:#222; padding:10px 5px; text-align:center;">Total</td>
        	<th style="border:none; background-color:#004e90; font-family:verdana; color:#fff; padding:6px; font-size: 18px;">'.$sum_lecturas.'</th>
        	</tr>';
        }
        ?>
        </tbody>
      </table>
