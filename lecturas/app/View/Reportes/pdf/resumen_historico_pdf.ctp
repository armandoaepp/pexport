<head>
<meta charset=utf-8 />
<title>Resumen Historico</title>
<style>
body {
	font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif;
	color: #333;
	font-size: 13.5px;
}

p {
	margin-bottom: -2px;
}
</style>
</head>
<body>
	<?php $meses = array(1 => 'Enero', 2 => 'Febrero', 3 => 'Marzo', 4 => 'Abril', 5 => 'Mayo', 6 => 'Junio', 7 => 'Julio', 8 => 'Agosto', 9 => 'Septiembre', 10 => 'Octubre', 11 => 'Noviembre', 12 => 'Diciembre');?>
	<div class='content'>

		<div align="right" style='position: absolute; left: 60%; top: -10px;'>
		<?php 
     	if($logo_negocio=='ensa.png'){
     		$logo_width = '100';
     	}elseif($logo_negocio=='hdna.png'){
			$logo_width = '150';
		}else{
			$logo_width = '100';
		}?>
			<img src="./images/pdf/<?php echo $logo_negocio;?>" width="<?php echo $logo_width;?>" style="position:absolute; margin-left: 150px;">
		</div>

		<p>
			Chiclayo,
			<?php echo date("d");?>
			de
			<?php echo $meses[date("n")];?>
			del
			<?php echo date("Y");?>
		</p>
		<br />
		<table class="no-border no-strip information" style="padding-top: 60px;padding-bottom: 30px;">
			<tbody class="no-border-x no-border-y">
				<tr>
					<td style="width: 20%;" class="category"><strong>B&uacute;squeda</strong>
					</td>
					<td>
						<table class="no-border no-strip skills">
							<tbody class="no-border-x no-border-y">
								<tr>
									<td style="width: 20%;"><b>Suministro</b></td>
									<td><?php echo (count($obj_suministro)>0?$obj_suministro[0]['0']['suministro']:$arr_suministros[0][0]['suministro']);?>
									</td>
								</tr>
								<tr>
									<td style="width: 20%;"><b>Direcci&oacute;n</b></td>
									<td><?php echo (count($obj_suministro)>0?$obj_suministro[0]['0']['direccion']:$arr_suministros[0][0]['direccion']);?>
									</td>
								</tr>
								<tr>
									<td style="width: 20%;"><b>Medidor</b></td>
									<td><?php echo (count($obj_suministro)>0?$obj_suministro[0]['0']['seriefab']:$arr_suministros[0][0]['seriefab']);?>
									</td>
								</tr>
							</tbody>
						</table>
					</td>
				</tr>
				<tr>
					<td style="width: 20%;" class="category"><strong>Datos</strong>
					</td>
					<td>
						<table class="no-border no-strip skills">
							<tbody class="no-border-x no-border-y">
								<tr>
									<td style="width: 20%;"><b>Nombre</b></td>
									<td><?php echo (count($obj_suministro)>0?$obj_suministro[0]['0']['cliente']:$arr_suministros[0][0]['cliente']);?>
									</td>
								</tr>
								<tr>
									<td style="width: 20%;"><b>Sector</b></td>
									<td><?php echo (count($obj_suministro)>0?$obj_suministro[0]['0']['sector']:$arr_suministros[0][0]['sector']);?>
									</td>
								</tr>
								<tr>
									<td style="width: 20%;"><b>Ruta</b></td>
									<td><?php echo (count($obj_suministro)>0?$obj_suministro[0]['0']['ruta']:$arr_suministros[0][0]['ruta']);?>
									</td>
								</tr>
								<tr>
									<td style="width: 20%;"><b>Periodo Inicio</b></td>
									<td><?php echo (count($obj_suministro)>0?$obj_suministro[0]['0']['pinicio']:$arr_suministros[0][0]['pinicio']);?>
									</td>
								</tr>
							</tbody>
						</table>
					</td>
				</tr>
			</tbody>
		</table>
		<div><strong>Comentarios:</strong></div>
		<?php echo $this->element('ReporteHistorico/comentario_pdf');?>
	</div>

</body>
