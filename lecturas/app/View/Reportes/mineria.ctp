<?php 
if (!isset($NEGOCIO)) {
    header("Location:".ENV_WEBROOT_FULL_URL."/Usuarios/logout");
    die();
}
?>
<?php 
if (isset($id_usuario) && $id_usuario == 69){
	$permiso_lectura = 'true';
}else {
	$permiso_lectura = 'false';
}
echo $this->element('menu',array('active'=>'rep_mineria', 'open_report'=>'true', 'permiso_lectura' =>$permiso_lectura, 'id_usuario ' =>isset($id_usuario)?$id_usuario:null));
?>


<div class="container-fluid" id="pcont">

	<div class="page-head" style="padding: 0px;">

		<ol class="breadcrumb">
			<li><button type="button"
					class="btn-xs btn-default arbol-top-lecturas">Arbol</button></li>
			<li class="active">Analisis Avanzado con Mineria de Datos</li>
		</ol>
	</div>


	<div class="stats_bar">
			<div class="butpro butstyle">
				<div class="sub"><h2>SUMINISTROS</h2><span id="total_clientes"><?php echo number_format($count_total);?></span></div>
				<div class="stat"><span class="equal"> 100%</span></div>
			</div>
			<div class="butpro butstyle">
				<div class="sub"><h2>SUMINISTROS METODO 2</h2><span id="total_clientes"><?php echo number_format($count_total_metodo2);?></span></div>
				<div class="stat"><span class="equal"> <?php echo number_format($count_total_metodo2*100/$count_total,2);?>%</span></div>
			</div>
			<div class="butpro butstyle">
				<div class="sub"><h2>CONSISTENTES 80</h2><span id="total_clientes"><?php echo number_format($count_consistentes_80);?></span></div>
				<div class="stat"><span class="<?php if(($count_consistentes_80*100/$count_total)>($count_inconsistentes_80*100/$count_total)){ echo 'up';}else{echo 'down';}?>"> <?php echo number_format($count_consistentes_80*100/$count_total,2);?>%</span></div>
			</div>
			<div class="butpro butstyle">
				<div class="sub"><h2>INCONSISTENTES 80</h2><span id="total_clientes"><?php echo number_format($count_inconsistentes_80);?></span></div>
				<div class="stat"><span class="<?php if(($count_inconsistentes_80*100/$count_total)>($count_consistentes_80*100/$count_total)){ echo 'up';}else{echo 'down';}?>"> <?php echo number_format($count_inconsistentes_80*100/$count_total,2);?>%</span></div>
			</div>
			<div class="butpro butstyle">
				<div class="sub"><h2>CONSISTENTES 95</h2><span id="total_clientes"><?php echo number_format($count_consistentes_95);?></span></div>
				<div class="stat"><span class="<?php if(($count_consistentes_95*100/$count_total)>($count_inconsistentes_95*100/$count_total)){ echo 'up';}else{echo 'down';}?>"> <?php echo number_format($count_consistentes_95*100/$count_total,2);?>%</span></div>
			</div>
			<div class="butpro butstyle">
				<div class="sub"><h2>INCONSISTENTES 95</h2><span id="total_clientes"><?php echo number_format($count_inconsistentes_95);?></span></div>
				<div class="stat"><span class="<?php if(($count_inconsistentes_95*100/$count_total)>($count_consistentes_95*100/$count_total)){ echo 'up';}else{echo 'down';}?>"> <?php echo number_format($count_inconsistentes_95*100/$count_total,2);?>%</span></div>
			</div>
	</div>

	<div class="cl-mcont">
		<div class="col-sm-12 col-md-12" style='height: 100%;'>
			<div class="block">


				<div class="content">
					<div class="tabla">
						<div id="digitar" class='farmsmall2'>
							<table id="tabla_reporte_mineria" class="table table-bordered display"
								style="background-color: #fff; color: #000; font-weight: bold; font-size: 14px;">
								<thead>
									<tr>
										<th>Suministro</th>
										<th>Periodo</th>
										<th>Monto Consumo</th>
										<th>Lectura</th>
										<th>Metodo</th>
										<th>Validaci&oacute;n</th>
										<th>Sanear Movil</th>
										<th>Primera</th>
										<th>Demanda</th>
										<th>Pronostico</th>
										<th>Minima80</th>
										<th>Maxima80</th>
										<th>Consistencia 80</th>
										<th>Minima95</th>
										<th>Maxima95</th>
										<th>Consistencia 95</th>
										<th>Gr&aacute;fico</th>
									</tr>
								</thead>
								<tbody>
									<?php
									$i=1;									
									foreach ($listado as $obj_orden_lectura){
												?>
									<tr>
										<td><?php echo $obj_orden_lectura[0]['Suministro']?></td>
										<td><?php echo $obj_orden_lectura[0]['Periodo']?></td>
										<td><?php echo number_format($obj_orden_lectura[0]['lecturao'],2)?></td>										
										<td><?php echo $obj_orden_lectura[0]['metodofinal1']?></td>
										<td><?php echo $obj_orden_lectura[0]['validacion']?></td>
										<td><?php echo $obj_orden_lectura[0]['sanearmovil']?></td>
										<td><?php echo $obj_orden_lectura[0]['primera']?></td>
										<td><?php echo number_format($obj_orden_lectura[0]['demandaact'],2)?></td>
										<td><?php echo number_format($obj_orden_lectura[0]['pronostico'],2)?></td>
										<td><?php echo number_format($obj_orden_lectura[0]['Minima80'],2)?></td>
										<td><?php echo number_format($obj_orden_lectura[0]['Maxima80'],2)?></td>
										<td><?php echo $obj_orden_lectura[0]['Consistencia80']?></td>
										<td><?php echo number_format($obj_orden_lectura[0]['Minima95'],2)?></td>
										<td><?php echo number_format($obj_orden_lectura[0]['Maxima95'],2)?></td>
										<td><?php echo $obj_orden_lectura[0]['Consistencia95']?></td>
										<td>
										<div class="spk-suministro-<?php echo $obj_orden_lectura[0]['Suministro']?> text-center"></div>
										<script type="text/javascript">
										<?php 
										$str_plot = '';
										foreach ($arr_suministros[$obj_orden_lectura[0]['Suministro']] as $suministro){
												$str_plot .= $suministro['0']["demandaact"].',';
										}
										?>
										$(".spk-suministro-<?php echo $obj_orden_lectura[0]['Suministro']?>").sparkline([<?php echo $str_plot;?>], { type: 'bar', width: '80px', height: '30px',barColor: '#4AA3DF'});
										</script>
										</td>
									</tr>
									<?php
												}
												?>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

</div>
<script type="text/javascript">

var oTable = $('#tabla_reporte_mineria').dataTable({
	"bProcessing": true,
	"bDeferRender": true,
	"bServerSide": true,
	"paging": false,
	"bSort": false,
	"iDisplayLength" : 20,
	"sAjaxSource": ENV_WEBROOT_FULL_URL+"Reportes/ajax_mineria",
	"aLengthMenu": [
					[20,50,100,300,500,800],
					[20,50,100,300,500,800]
					],
	"fnDrawCallback" : fnEachTd,
	"sDom": '<"top"p>T<"clear"><"datatables_filter"lrf><"bottom">ti',
 	"oTableTools": {
 		"sSwfPath":  ENV_WEBROOT_FULL_URL+"js/jquery.datatables/extras/TableTools/media/swf/copy_csv_xls_pdf.swf"
	},
	});

	function fnEachTd(oSettings){
		$( ".spk-suministro" ).each(function( i ) {
		    $(this).sparkline(eval('['+$(this).data('plot')+']'), { type: 'bar', width: '80px', height: '30px',barColor: '#4AA3DF'});
		});
	}

</script>
