<script>
    
    function verLecturas(target,ciclo, sector, ruta,nombruta,unidadnegocio){
    	$('#contenido_modal').html('');
        $("#desc_ciclo").html(ciclo);
        $("#desc_sector").html(sector);
        $("#desc_nombruta").html(nombruta);
        $("#desc_negocio").html(unidadnegocio);
        $("#ciclo").attr('value',ciclo);
        $("#sector").attr('value',sector);
        $("#ruta").attr('value',ruta);
        $("#negocio").attr('value',unidadnegocio);
    	if(!$(target).data('cronograma')){
        	$('#formulario_asignar_no').show();
        	$('#formulario_asignar').hide();        	
    	}else{
    		$('#dpt_date_end').val($(target).data('cronograma_fecha'));
    		$('#formulario_asignar_no').hide();
        	$('#formulario_asignar').show();
      
     	$("#contenido_modal").append('<table id="tabla_modal" class="table table-bordered  " style="background-color:#fff;color:#000;font-weight: bold; "    ><thead>                                 <tr><th width="20" >Item</th><th width="30">Orden Sector</th><th width="50">Suministro</th><th>Cliente</th><th>Direccion</th><th width="10">Libro</th><th width="30">Lecturista</th><th width="10">Estado</th>  </tr>                              </thead> </table>');
       	$('#tabla_modal').dataTable().fnDestroy(); 
        $('#tabla_modal').dataTable( {
        
        "sAjaxSource":  ENV_WEBROOT_FULL_URL+"/ComlecOrdenlecturas/listacreatelibro2",
          
            "fnServerData": function ( sSource, aoData, fnCallback ) {
            	aoData.push( { "name": "tipo", "value": "<?php echo $tipo;?>" } );
                aoData.push( { "name": "ciclo", "value": ciclo } );
                aoData.push( { "name": "sector", "value": sector } );
                aoData.push( { "name": "ruta", "value": ruta } );
                $.ajax( {
                    "dataType": 'json',
                    "type": "POST",
                    "url": sSource,
                    "data": aoData,
                    "success": fnCallback
                } );
            }
         });
     $(".datetime").datetimepicker({autoclose: true});
    	}
    }
    function showFormAsignacion(target,ciclo, sector, ruta,nombruta,unidadnegocio){
        $("#lbl_ciclo").html(ciclo);
        $("#lbl_sector").html(sector);
        $("#lbl_nombruta").html(nombruta);
        $("#lbl_negocio").html(unidadnegocio);
    	if(!$(target).data('cronograma')){
        	$('#formulario_asignar_nuevo_no').show();
        	$('#formulario_asignar_nuevo').hide();        	
    	}else{
    		$('#dpt_date_end').val($(target).data('cronograma_fecha'));
    		$('#formulario_asignar_nuevo_no').hide();
        	$('#formulario_asignar_nuevo').show();
        	$("#formulario_asignar_nuevo").mask("Cargando...");
        	$.ajax({
				url : ENV_WEBROOT_FULL_URL+'ComlecOrdenlecturas/form_asignar_basado_historico/'+unidadnegocio+'/'+ciclo+'/'+sector+'/'+ruta,
				type: "POST",
				success:function(data, textStatus, jqXHR){
					$('#formulario_asignar_nuevo').html(data);
					$(".select2",$('#formulario_asignar_nuevo')).select2({
				          width: '200px'
				         });
					$('.bslider').slider();
					$(".datetime").datetimepicker({autoclose: true});
					$("#formulario_asignar_nuevo").unmask();
				}
			});
    	}
    }
	
    </script>
	<style>
		.modal-footer {
		padding: 0px 3px 7px;
		text-align: left;
		border-bottom: 1px solid #e5e5e5;
		border-top: #FFFFFF;
		margin-right: 20px; 
		margin-left: 20px; 
		margin-top: -20px; 
		}
	</style>
	<!-- TABLA 2 DE BUSQUEDA-->
<?php 
if (!isset($NEGOCIO)) {
    header("Location:".ENV_WEBROOT_FULL_URL."/Usuarios/logout");
    die();
}
$obj_evento_cronograma = $model_ComlecOrdenlectura->existeEventoCronograma(null,null,$ciclo,$sector);
if(count($obj_evento_cronograma)>0){
	$str_cronograma_si = 'data-cronograma="true" data-cronograma_fecha="'.substr($obj_evento_cronograma[0]['0']['fecha'],0,10).'"';
}else{
	$str_cronograma_si = 'data-cronograma="false"';
}
?>
		<div class="col-sm-12 col-md-12">
										
				
				<div class="panel-group accordion accordion" id="accordion5">
				<!--div class="panel-group accordion accordion-semi" id="accordion4"-->
					  <div class="panel panel-default">
						<div class="panel-heading">
						<!--div class="panel-heading success"-->
						  <h4 class="panel-title">
							<a data-toggle="collapse" data-parent="#accordion5" href="#ac5-1">
								<i class="fa fa-angle-right"></i> <?php if($NEGOCIO == 'CIX'){echo 'Rutas';}elseif ($NEGOCIO == 'TAC'){echo 'Libros';}else{echo 'Rutas';}?> por Ciclo/<?php if($NEGOCIO == 'CIX'){echo 'Sector';}elseif ($NEGOCIO == 'TAC'){echo 'Zona';}else{echo 'Sector';}?> seleccionado: <?php echo $ciclo.'/'.$sector;?>
							</a>
						  </h4>
						</div>
				
				<div id="ac5-1" class="panel-collapse collapse in">
						  <div class="panel-body">
				
										<div class="content">
											<div class='tabla'>
												<div id='asig2' class='farmsmall2'>
													<div class='table-responsive'>
														
														<div id="grid2">
															<table class="table table-bordered dataTable" id="ciclosector" >
															<thead >
															<tr>
															<th>pfactura</th>
                                                            <th>Ciclo</th>
															<th>Sector</th>
															<th>Ruta</th>
															<th>Nombre Ruta</th>
															<th>Total Sum.</th>
															<th>Total Sum. Asignados</th>
															<th>% Total Sum. Asignados</th>
															<th>Cant. Lecturistas</th>
															<th>Asignar</th>
															</tr>
															</thead>
															<tbody>
															<?php									
															foreach ($buscar_ciclo_sector as $obj_orden_lectura){
																?>
																<tr class="odd gradeX">
																<td><?php echo $obj_orden_lectura[0]['pfactura']?></td>		
																<td><?php echo $obj_orden_lectura[0]['idciclo']?></td>
																<td><?php echo $obj_orden_lectura[0]['sector']?></td>
																<td><?php echo $obj_orden_lectura[0]['ruta']?></td>
																<td><?php echo $obj_orden_lectura[0]['nombruta']?></td>
																<td><?php echo $obj_orden_lectura[0]['total']?></td>
																<td><?php echo $obj_orden_lectura[0]['total_asignados']?></td>
																<td><div class="progress progress-striped active">
																	<?php 
																	if($obj_orden_lectura[0]['total_asignados']>0){
																		$porcentaje = ($obj_orden_lectura[0]['total_asignados']*100)/$obj_orden_lectura[0]['total'];
																	}else{
																		$porcentaje = 0;
																	}
																	$porcentaje_restante = 100 - $porcentaje;
																	$str_class_porcentaje = 'progress-bar-success';
																	$str_class_porcentaje_2 = 'progress-bar-danger';
																	?>
																	  <div class="progress-bar <?php echo $str_class_porcentaje;?>" style="width: <?php echo $porcentaje;?>%"><?php echo number_format($porcentaje,2);?>%</div>
																	  <div class="progress-bar <?php echo $str_class_porcentaje_2;?>" style="width: <?php echo $porcentaje_restante;?>%"><?php echo ($porcentaje_restante==100)?number_format($porcentaje,2).'%':'';?></div>
																	</div></td>
																<td><?php echo $obj_orden_lectura[0]['total_lecturistas']?></td>
																<td>
																<a href="javascript:;" <?php echo $str_cronograma_si;?>  class="btn btn-info btn-sm btn-flat md-trigger"  data-modal="colored-success"   onclick="verLecturas(this,'<?php echo $obj_orden_lectura[0]['idciclo']?>','<?php echo $obj_orden_lectura[0]['sector']?>','<?php echo $obj_orden_lectura[0]['ruta']?>','<?php echo $obj_orden_lectura[0]['nombruta']?>','<?php echo $obj_orden_lectura[0]['unidad_neg']?>');"  >Detallado</a>															 
																<a href="javascript:;" <?php echo $str_cronograma_si;?>  class="btn btn-primary btn-sm btn-flat md-trigger"  data-modal="modal-asignar-nuevo"   onclick="showFormAsignacion(this,'<?php echo $obj_orden_lectura[0]['idciclo']?>','<?php echo $obj_orden_lectura[0]['sector']?>','<?php echo $obj_orden_lectura[0]['ruta']?>','<?php echo $obj_orden_lectura[0]['nombruta']?>','<?php echo $obj_orden_lectura[0]['unidad_neg']?>');"  >Generico</a>
																</td>
                                                                </tr>
															<?php
															}
															?>
															</tbody>
															</table>
														</div>
													</div>
												</div>
											</div>
										</div>		

					</div>
						</div>				
						
		</div>

		<!-- style="-webkit-perspective: none; height: 260px; top: -10%; 
                                       position: absolute; width: 120%; margin-left: -10%;"-->


		<div class="md-modal colored-header custom-width success md-effect-10"
			id="colored-success" style="width: 100%;">

			<div class="md-content">
				<div class="modal-header">
					<h3>
						Unidad Negocio:
						<button type="button" class="btn btn-success" id="desc_negocio"></button>
						Ciclo:
						<button type="button" class="btn btn-success" id="desc_ciclo"></button>
						<?php if($NEGOCIO == 'CIX'){
							echo 'Sector';
						}elseif ($NEGOCIO == 'TAC'){
							echo 'Zona';
							}else{echo 'Sector';
							}?>
						:
						<button type="button" class="btn btn-success" id="desc_sector"></button>
						<?php if($NEGOCIO == 'CIX'){
							echo 'Ruta';
						}elseif ($NEGOCIO == 'TAC'){
							echo 'Libro';
							}else{echo 'Ruta';
							}?>
						:
						<button type="button" class="btn btn-success" id="desc_nombruta"></button>
					</h3>
					<button type="button" class="close md-close" data-dismiss="modal"
						aria-hidden="true">&times;</button>

				</div>

				<input type="hidden" name="ciclo" id="ciclo" /> <input type="hidden"
					name="sector" id="sector" /> <input type="hidden" name="rutas"
					id="ruta" /> <input type="hidden" name="negocio" id="negocio" /> </br>
				
				<div id="formulario_asignar">
					<div class="modal-footer">
						<div class="row ">
                           <div class="col-sm-12  col-lg-12 " style="margin: 0px 20px;">
								<div class="col-lg-2 col-md-2 col-sm-6">
									<div class="form-group" style="display: inline; ">
			                            <div class="input-group date datetime" data-min-view="2" data-date-format="yyyy-mm-dd" >
											<input id="dpt_date_end" class="form-control" size="16" type="text" value="<?php echo date('Y-m-d');?>" readonly>
											<span class="input-group-addon btn btn-primary"><span class="glyphicon glyphicon-th"></span></span>
										</div>
		                      		</div>
								</div>
		                      
								<div class="col-lg-4 col-md-3 col-sm-6" >
									<div class="form-group" style="display: inline;">
										<label class=" control-label"><strong>Item desde: </strong></label>
										<input type="text" name="desde" id="desde" value="1"
											style="background-color: #60c060; font-weight: bold; width: 43px;" />
		                        	
										<strong> Item hasta: </strong> <input type="text" name="hasta"
										id="hasta" style=" background-color: #60c060; font-weight: bold; width: 43px;" />|- <input type="text" name="total-lecturistas" id="total-lecturistas"
										style="background-color: #cccccc; font-weight: bold; width: 43px;" />
		                     		</div>
								</div>
		                      
								<div class="col-lg-6 col-md-6 col-sm-12" >
									<div class="form-group" style="display: inline; ">
			                            <label class=" control-label"><strong>Lecturista Asignado:</strong>
										</label>
										
										<?php 
										echo '<select name="lecturista" id="lecturista">';
										foreach ($listar_empleados as $empleados){
			                              echo '<option value='.$empleados['0']["id"].'>'.$empleados['0']["nombre"].' - '.$empleados['0']["nomusuario"].'</option>';
			                              }
			                              echo '</select>';
			                              ?>
										<button style="margin-top: 10px;" type="button" id="asignar_lecturista_libro"
											name="asignar_lecturista_libro" class="btn btn-primary btn-flat"
											data-dismiss="modal">Grabar</button>
										<!-- <button type="button" class="btn btn-default btn-flat md-close" data-dismiss="modal">Cancel</button> -->
		                      		</div>
								</div>
							</div>
                        </div>
					</div>

					<div class="modal-body form"
						style="height: 400px; /* position: absolute; */ overflow: auto;"
						id="contenido_modal">
					</div>	
				</div>

				<div id="formulario_asignar_no">
					<div class="alert alert-warning alert-white rounded">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
						<div class="icon"><i class="fa fa-warning"></i></div>
						<strong>Alerta!</strong> No se puede Asignar. Usted debe agregar el ciclo y sector al cronograma <a href="<?php echo ENV_WEBROOT_FULL_URL;?>comlec_ordenlecturas/cronograma">aqu&iacute;</a>.
					 </div>
					 <br>
				</div>
			</div>
		
				<!-- Modal Asignacion Nuevo - Geynen -->
                <div class="md-modal colored-header custom-width md-effect-10" id="modal-asignar-nuevo" style="width: 100%;top: 142px;">
                    <div class="md-content">
                      <div class="modal-header">
                        <h3>
							Unidad Negocio:
							<button type="button" class="btn btn-success" id="lbl_negocio"></button>
							Ciclo:
							<button type="button" class="btn btn-success" id="lbl_ciclo"></button>
							<?php if($NEGOCIO == 'CIX'){
								echo 'Sector';
							}elseif ($NEGOCIO == 'TAC'){
								echo 'Zona';
							}else{echo 'Sector';
							}?>
							:
							<button type="button" class="btn btn-success" id="lbl_sector"></button>
							<?php if($NEGOCIO == 'CIX'){
								echo 'Ruta';
							}elseif ($NEGOCIO == 'TAC'){
								echo 'Libro';
							}else{echo 'Ruta';
							}?>
							:
							<button type="button" class="btn btn-success" id="lbl_nombruta"></button>
						</h3>
                        <button type="button" class="close md-close" data-dismiss="modal" aria-hidden="true">&times;</button>
                      </div>
                      <div class="modal-body">                        
                        <div id="formulario_asignar_nuevo">
	                        <div class="text-center">
	                        <br><br>
	                          <div class="i-circle primary"><i class="fa fa-check"></i></div>
	                          <h4>sistema de lectura</h4>
	                          <p>Estamos mejorando para hacerte la vida m&aacute;s f&aacute;cil!</p>                          
	                        </div>
                        </div>
                        <div id="formulario_asignar_nuevo_no">
							<div class="alert alert-warning alert-white rounded">
								<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
								<div class="icon"><i class="fa fa-warning"></i></div>
								<strong>Alerta!</strong> No se puede Asignar. Usted debe agregar el ciclo y sector al cronograma <a href="<?php echo ENV_WEBROOT_FULL_URL;?>comlec_ordenlecturas/cronograma">aqu&iacute;</a>.
							 </div>
							 <br>
						</div>
                      </div>
                      <div class="modal-footer">
                      </div>
                    </div>
                </div>


		<div class="md-overlay" style="margin-top: 30px;"></div>