<?php 
if (!isset($NEGOCIO)) {
    header("Location:".ENV_WEBROOT_FULL_URL."/Usuarios/logout");
    die();
}
?>
<?php 
if ($id_usuario == 25 || $id_usuario == 131){
    $permiso_lectura = 'false';
}else {
    $permiso_lectura = 'true';
    
}
echo $this->element('menu',array('active'=>'importar', 'open'=>'true', 'permiso_lectura' =>$permiso_lectura, 'id_usuario ' =>$id_usuario));
?>
<script>
function tooglearbol(){
    $('#arbol').toggle();
}
</script>


    
<div class="container-fluid" id="pcont">
   <!--
    <div class="page-head">
        <ol class="breadcrumb">
          <li><button type="button" class="btn-xs btn-default arbol-top-lecturas" >Arbol</button></li>
          <li><a href="#">Ordenes de Lectura</a></li>
          <li class="active">Listado de XMLs</li>
        </ol>
    </div>
    -->

    <div class="cl-mcont">
        <div class="col-sm-12 col-md-12"  style='height:100%;' >
            <div class="block">
                <div class="content">
                    <div class="scroll-table-inconsistecia" style='position: relative; max-width: 300px; min-width: 100%; overflow-y: hidden; height: 500px; overflow-y: auto;'>
                        <div id="contenido_xml" name="contenido_xml" >
                            

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="arbol" class="page-aside app filters tree" style="">
    <div class="fixed nano nscroller has-scrollbar">
        <div class="content">

            <div class="header">
                <button class="navbar-toggle" data-target=".app-nav" data-toggle="collapse" type="button">
                    <span class="fa fa-chevron-down"></span>
                </button>
                <h2 class="page-title">Upload OT</h2>  
            </div>

            <div class="app-nav collapse">

                <form action="<?= ENV_WEBROOT_FULL_URL?>ComlecOrdenlecturas/saveForm" id="ComlecOrdenlecturaFormForm" enctype="multipart/form-data"  method="post"  >
                    <div style="display:none;"><input type="hidden" name="_method" value="POST"></div><br/>
                    <label class="radio-inline" style="padding-left: 0px;">
                    <input type="radio" name="txt_tipo" value="" checked> Suministros
                    </label>
                    <br><br>
                    <input id="file" type="file" name="file">
                    </br>
                    <input type="submit" class="btn btn-success" value="Grabar">
                </form>
                
                     
                
                <div class="form-group" >
                  <label class="control-label">Por :</label>
                  <input id="cbo_criterio" name="cbo_criterio" style="width:240px;" placeholder="Seleccione una opcion">
                </div>
                
                <div class="por_fecha_asignacion" style= >
                    <div class="form-group"  >
                      <label class="control-label">Por Fecha de Importaci&oacute;n (Inicio) :</label>

                      <div class="input-group date datetime" style= "width: 50%;" data-min-view="2" data-date-format="yyyy-mm-dd">
                        <input id="dpt_date_start" class="form-control" size="16" type="text" value="<?php echo date('Y-m-d', strtotime("-1 day"));?>" readonly>
                        <span class="input-group-addon btn btn-primary"><span class="glyphicon glyphicon-th"></span></span>
                      </div>
                    </div>

                    <div class="form-group">
                      <label class="control-label">Por Fecha de Importaci&oacute;n (Fin) :</label>
                      <div class="input-group date datetime"  style= "width: 50%;" data-min-view="2" data-date-format="yyyy-mm-dd">
                        <input id="dpt_date_end" class="form-control" size="16" type="text" value="<?php echo date('Y-m-d');?>" readonly>
                        <span class="input-group-addon btn btn-primary"><span class="glyphicon glyphicon-th"></span></span>
                      </div>    
                    </div>
                </div>
                
                <div class="por_ubicacion" style="display:none;" >    
                    <div class="form-group">
                      <label class="control-label"><?php if($NEGOCIO == 'CIX'){echo 'Por Sector';}elseif ($NEGOCIO == 'TAC'){echo 'Por Zona';}else{echo 'Por Sector';}?><span class="sector_loading" style="display:none;"><img src="<?php echo ENV_WEBROOT_FULL_URL ?>/js/jquery.select2/select2-spinner.gif"></span></label>              
                        <?php 
                            echo '<select class="" name="monitoreo_id_sector" id="monitoreo_id_sector">
                            <option value="0">Todos</option>';
                            foreach ($listar_sector as $v){
                            echo '<option value='.$v['0']['sector'].'>'.$v['0']['sector'].'</option>';
                            }
                            echo '</select>';   
                        ?>                  
                    </div>
                </div>

                <button class='btn btn-primary' id='buscar-listado-xml'>Buscar</button>
                <br>
                <br>
                <br>
                <br>
            </div>
            
        </div>
    </div>
</div>