<?php 
if (!isset($NEGOCIO)) {
    header("Location:".ENV_WEBROOT_FULL_URL."/Usuarios/logout");
    die();
}
?>
<script>
    function verLecturas(ciclo, sector, ruta,nombruta,unidadnegocio){
      $('#contenido_modal').html('');
      $("#desc_ciclo").html(ciclo);
      $("#desc_sector").html(sector);
      $("#desc_nombruta").html(nombruta);
      $("#desc_negocio").html(unidadnegocio);
      $("#ciclo").attr('value',ciclo);
      $("#sector").attr('value',sector);
      $("#ruta").attr('value',ruta);
      $("#negocio").attr('value',unidadnegocio);
      
     $("#contenido_modal").append('<table id="tabla_modal" class="table table-bordered  " style="background-color:#fff;color:#000;font-weight: bold; "    ><thead>                                 <tr><th width="20" >Item</th><th width="30">Orden Sector</th><th width="50">Suministro</th><th>Cliente</th><th>Direccion</th><th width="10">Libro</th><th width="30">Lecturista</th><th width="10">Estado</th>  </tr>                              </thead>                                         </table>');
     $('#tabla_modal').dataTable().fnDestroy(); 
     $('#tabla_modal').dataTable( {
        	"sDom": '<"top"p>T<"clear"><"datatables_filter"lrf><"bottom">ti',
         	"oTableTools": {
			"sSwfPath": "../../js/jquery.datatables/extras/TableTools/media/swf/copy_csv_xls_pdf.swf"
				},
         "sAjaxSource":  ENV_WEBROOT_FULL_URL+"/ComlecOrdenlecturas/listacreatelibrorelectura",
         "fnServerData": function ( sSource, aoData, fnCallback ) {
                aoData.push( { "name": "ciclo", "value": ciclo } );
                aoData.push( { "name": "sector", "value": sector } );
                aoData.push( { "name": "ruta", "value": ruta } );
                $.ajax( {
                    "dataType": 'json',
                    "type": "POST",
                    "url": sSource,
                    "data": aoData,
                    "success": fnCallback
            	});
            }
         });       
    }   
	
    </script>
<!-- TABLA 2 DE BUSQUEDA-->

<div class="col-sm-12 col-md-12">


	<div class="panel-group accordion accordion" id="accordion5">
		<!--div class="panel-group accordion accordion-semi" id="accordion4"-->
		<div class="panel panel-default">
			<div class="panel-heading">
				<!--div class="panel-heading success"-->
				<h4 class="panel-title">
					<a data-toggle="collapse" data-parent="#accordion5" href="#ac5-1">
						<i class="fa fa-angle-right"></i> Rutas por Ciclo/Sector
						seleccionado
					</a>
				</h4>
			</div>

			<div id="ac5-1" class="panel-collapse collapse in">
				<div class="panel-body">

					<div class="content">
						<div class='tabla'>
							<div class='farmsmall'>
								<div class='table-responsive'>

									<div id="grid2">
										<table class="no-border" id="ciclosector">
											<thead>
												<tr>
													<th>Ciclo</th>
													<th>Sector</th>
													<th>Ruta</th>
													<th>Lecturistas</th>
													<th>Total Suministros</th>
													<th>Accion</th>
													<th>Ruta</th>
													<th>pfactura</th>
												</tr>
											</thead>
											<tbody>
												<?php									
												foreach ($buscar_ciclo_sector as $obj_orden_lectura){
															?>
												<tr class="odd gradeX">
													<td><?php echo $obj_orden_lectura[0]['idciclo']?></td>
													<td><?php echo $obj_orden_lectura[0]['sector']?></td>
													<td><?php echo $obj_orden_lectura[0]['nombruta']?></td>
													<td>Lecturista</td>

													<td><?php echo $obj_orden_lectura[0]['total']?></td>
													<td><a href="javascript:;"
														class="btn btn-primary btn-flat md-trigger"
														data-modal="colored-success"
														onclick="verLecturas('<?php echo $obj_orden_lectura[0]['idciclo']?>','<?php echo $obj_orden_lectura[0]['sector']?>','<?php echo $obj_orden_lectura[0]['ruta']?>','<?php echo $obj_orden_lectura[0]['nombruta']?>','<?php echo $obj_orden_lectura[0]['unidad_neg']?>');">Ver
															Lecturas</a></td>
													<td><?php echo $obj_orden_lectura[0]['ruta']?></td>
													<td><?php echo $obj_orden_lectura[0]['pfactura']?></td>
												</tr>
												<?php
															}
															?>
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
					</div>

				</div>
			</div>

		</div>

		<!-- style="-webkit-perspective: none; height: 260px; top: -10%; 
                                       position: absolute; width: 120%; margin-left: -10%;"-->


		<div class="md-modal colored-header custom-width success md-effect-10"
			id="colored-success" style="width: 100%;">

			<div class="md-content">
				<div class="modal-header">
					<h3>
						Unidad Negocio:
						<button type="button" class="btn btn-success" id="desc_negocio"></button>
						Ciclo:
						<button type="button" class="btn btn-success" id="desc_ciclo"></button>
						Sector:
						<button type="button" class="btn btn-success" id="desc_sector"></button>
						Ruta:
						<button type="button" class="btn btn-success" id="desc_nombruta"></button>
					</h3>
					<button type="button" class="close md-close" data-dismiss="modal"
						aria-hidden="true">&times;</button>
				</div>
				<div class="modal-body form"
					style="height: 400px; /* position: absolute; */ overflow: auto;"
					id="contenido_modal"></div>
				<div class="modal-footer">
					<input type="hidden" name="ciclo" id="ciclo" /> <input
						type="hidden" name="sector" id="sector" /> <input type="hidden"
						name="rutas" id="ruta" /> <input type="hidden" name="negocio"
						id="negocio" /> <strong>Item desde: </strong><input type="text"
						name="desde" id="desde" value="1"
						style="text-align: center; background-color: #60c060; font-weight: bold;" />
					<strong> Item hasta: </strong><input type="text" name="hasta"
						id="hasta"
						style="text-align: center; background-color: #60c060; font-weight: bold;" />
					|- <input type="text" name="total-lecturistas"
						id="total-lecturistas"
						style="text-align: center; background-color: #cccccc; font-weight: bold;" /><strong>
						Lecturista Asignado: </strong>
					<?php 
					//print_r($listar_empleados);
					echo '<select name="lecturista" id="lecturista">';
					foreach ($listar_empleados as $empleados){
							                              echo '<option value='.$empleados['0']["id"].'>'.$empleados['0']["nombre"].' - '.$empleados['0']["nomusuario"].'</option>';
							                              }
							                              echo '</select>';
							                              ?>
					<button type="button" id="asignar_lecturista_libro_relecturas"
						name="asignar_lecturista_libro_relecturas" class="btn btn-primary btn-flat"
						data-dismiss="modal">Grabar</button>
					<!-- <button type="button" class="btn btn-default btn-flat md-close" data-dismiss="modal">Cancel</button> -->
				</div>

			</div>
		</div>

		<div class="md-overlay"></div>