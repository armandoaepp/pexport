<?php 
if (!isset($NEGOCIO)) {
    header("Location:".ENV_WEBROOT_FULL_URL."/Usuarios/logout");
    die();
}
?>
<?php 
if ($id_usuario == 25 || $id_usuario == 131){
	$permiso_lectura = 'false';
}else {
	$permiso_lectura = 'true';
}
echo $this->element('menu',array('active'=>'rep_personal_para_lectura', 'open_report'=>'true', 'permiso_lectura' =>$permiso_lectura, 'id_usuario ' =>$id_usuario));
?>

		<div id="arbol" class="page-aside app filters" style="zoom:1;">
      	<div>
        <div class="content">
          <button class="navbar-toggle" data-target=".app-nav" data-toggle="collapse" type="button">
            <span class="fa fa-chevron-down"></span>
          </button>          
          <h2 class="page-title">Filtros</h2>
          <p class="description">Criterios de Busqueda</p>
        </div>
        
        <div class="app-nav collapse">
          <div class="content">
          
            <div class="form-group">
              <label class="control-label" >Tipo de Lectura</label>
              <select class="" name="id_tipolectura" id="id_tipolectura">
              <option value="" <?php if($tipo==''){ echo 'selected';}?>>Todos</option>
                <option value="L" <?php if($tipo=='L'){ echo 'selected';}?>>Lectura</option>
                <option value="R" <?php if($tipo=='R'){ echo 'selected';}?>>Relectura</option>
              </select>
            </div>

            <div class="por_ubicacion">
                
                
              	<div class="form-group">
	              <label class="control-label">Por Unidad Negocio:</label>              
	              	<?php 
	        			  	echo '<select class="" name="unidadneg" id="unidadneg">
	        			  	<option value="0">Todos</option>';
                                             //   var_dump($listar_unidadneg);exit;
	        				foreach ($listar_unidadneg as $v){
								if($unidadneg==$v['0']['unidadneg']){
	        						echo '<option value='.$v['0']['unidadneg'].' selected>'.$v['0']['unidadneg'].' - '.$v['0']['nombreunidadnegocio'].'</option>';
	        					}else{
									echo '<option value='.$v['0']['unidadneg'].'>'.$v['0']['unidadneg'].' - '.$v['0']['nombreunidadnegocio'].'</option>';
								}
	        				}
	        				echo '</select>';	
	        		?>        			
	            </div>
                
	          	<div class="form-group">
	              <label class="control-label">Por Ciclo:<span class="ciclo_loading" style="display:none;"><img src="<?php echo ENV_WEBROOT_FULL_URL ?>js/jquery.select2/select2-spinner.gif"></span></label>              
	              	<?php 
	        			  	echo '<select class="" name="id_ciclo" id="id_ciclo">
	        			  	<option value="0">Todos</option>';
	        				foreach ($listar_ciclo as $v){
								if($id_ciclo==$v['0']['idciclo']){
	        						echo '<option value='.$v['0']['idciclo'].' selected>'.$v['0']['idciclo'].'</option>';
		        				}else{
		        					echo '<option value='.$v['0']['idciclo'].'>'.$v['0']['idciclo'].'</option>';
		        				}
	        				}
	        				echo '</select>';	
	        		?>        			
	            </div>

	          	<div class="form-group">
	              <label class="control-label"><?php if($NEGOCIO == 'CIX'){echo 'Por Sector';}elseif ($NEGOCIO == 'TAC'){echo 'Por Zona';}else{echo 'Por Sector';}?><span class="sector_loading" style="display:none;"><img src="<?php echo ENV_WEBROOT_FULL_URL ?>js/jquery.select2/select2-spinner.gif"></span></label>              
	              	<?php 
	        			  	echo '<select class="" name="monitoreo_id_sector" id="monitoreo_id_sector">
	        			  	<option value="0">Todos</option>';
	        				foreach ($listar_sector as $v){
								if($id_sector==$v['0']['sector']){
	        						echo '<option value='.$v['0']['sector'].' selected>'.$v['0']['sector'].'</option>';
	        					}else{
									echo '<option value='.$v['0']['sector'].'>'.$v['0']['sector'].'</option>';
								}
	        				}
	        				echo '</select>';	
	        		?>        			
	            </div>
            </div>

            <button class='btn btn-primary' id='buscar-personal-para-lectura'>Buscar</button>
            <button class='btn btn-info' id='generar-pdf-cronograma'>Generar PDF</button>
          </div>

          
        </div>
      </div>
</div>

<div class="container-fluid" id="pcont">
	<!--
	<div class="page-head" style="padding: 0px;">

		<ol class="breadcrumb">
			<li><button type="button"
					class="btn-xs btn-default arbol-top-lecturas">Arbol</button></li>
			<li><a href="#">Ordenes de Lectura</a></li>
			<li><a href="#">Digitar</a></li>
			<li class="active">Listado del Personal para toma de lectura de
				medidores</li>
		</ol>
	</div>
-->

	<div class="cl-mcont" style="width: 100%">
		<div class="col-sm-12 col-md-12" style='height: 100%;'>
			<div class="block">


				<div class="content">
				    

					<div class="tabla">
						<div id="digitar" class='farmsmall2'>
							<table id="tabla_listar_personal_para_lecturas" class="table table-bordered display"
								style="background-color: #fff; color: #000; font-weight: bold; font-size: 14px;">
								<thead>
									<tr>
										<th>Ord.</th>

										<th>Nombres</th>
										<th>Ciclo</th>
										<th>Sector</th>
										<th>Ruta</th>
										<th>Lecturas Estimadas</th>
										<th>Direcciones Referenciales de Sector</th>
									</tr>
								</thead>
								<tbody>
									<?php
									$i=1;									
									foreach ($listar_orden_lecturas as $obj_orden_lectura){
												?>
									<tr>
										<td><?php echo $i++;?></td>
										<td><?php echo $obj_orden_lectura[0]['lecturista']?></td>
										<td><?php echo $obj_orden_lectura[0]['idciclo']?></td>
										<td><?php echo $obj_orden_lectura[0]['sector']?></td>
										<td><?php echo $obj_orden_lectura[0]['ruta']?></td>
										<td><?php echo $obj_orden_lectura[0]['cantidad']?></td>
										<td><?php $arr_dires = explode(',',$obj_orden_lectura[0]['direccion']); echo $arr_dires[0];?></td>
									</tr>
									<?php
												}
												?>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

</div>
