<div class="cl-mcont">
	
	<div class="col-sm-12 col-md-12">
	
		<div class="block-flat">
			<div class="header">
				<h3>Resumen Cronograma: <?php echo $id_cronograma;?> Ciclo: <?php echo $obj_cronograma[0][0]['idciclo'];?> Sector: <?php echo $obj_cronograma[0][0]['idsector'];?> Fecha Programada: <?php echo substr($obj_cronograma[0][0]['fecha'],0,10);?></h3>
				<a class="btn btn-danger btn_eliminar_evento" href="javascript: void(0);" onclick="javascript: eliminar_evento(<?php echo $id_cronograma;?>);" style="float: right;margin-top: -40px;">Eliminar Evento</a>
			</div>
			<div class="content">
				<table class="">
					<thead class="no-border">
						<tr>
							<th></th>
							<th class="text-center">Actual</th>
							<th class="text-center">Pendiente</th>
							<th class="text-center">Estado</th>
						</tr>
					</thead>
					<tbody class="">
						<tr>
							<td style="width:30%;">Suministros</td>
							<td class="text-center"><span class="badge"><?php echo $cantidad_suministros;?></span></td>
							<td class="text-center"></td>
							<td class="text-center"></td>
						</tr>
						<tr>
							<td style="width:30%;">Asignadas</td>
							<td class="text-center"><span class="badge"><?php echo $cantidad_asignados;?></span></td>
							<td class="text-center"><span class="badge badge-info"><?php echo ($cantidad_suministros-$cantidad_asignados);?></span></td>
							<td class="text-center">
							<?php 
							if(($cantidad_suministros-$cantidad_asignados)==0){
							?>
							<span class="badge badge-success">OK</span>
							<?php }else{?>
							<span class="badge badge-danger">NO</span>
							<?php }?>
							</td>
						</tr>
						<tr>
							<td>Descargadas</td>
							<td class="text-center"><span class="badge"><?php echo $cantidad_descargados;?></span></td>
							<td class="text-center"><span class="badge badge-info"><?php echo ($cantidad_asignados-$cantidad_descargados);?></span></td>
							<td class="text-center">
							<?php 
							if(($cantidad_asignados-$cantidad_descargados)==0){
							?>
							<span class="badge badge-success">OK</span>
							<?php }else{?>
							<span class="badge badge-danger">NO</span>
							<?php }?>
							</td>
						</tr>
						<tr>
							<td>Finalizadas</td>
							<td class="text-center"><span class="badge badge-info"><?php echo $cantidad_finalizados;?></span></td>
							<td class="text-center">
							<?php if(($cantidad_suministros-$cantidad_finalizados)==0){?>
							<span class="badge badge-info"><?php echo ($cantidad_suministros-$cantidad_finalizados);?></span>
							<?php }else{?>
							<a href="<?php echo ENV_WEBROOT_FULL_URL;?>comlec_ordenlecturas/digitarlecturas/<?php echo $obj_cronograma[0][0]['idsector'];?>" target="_blank">
							<span class="badge badge-info"><?php echo ($cantidad_suministros-$cantidad_finalizados);?></span>
							</a>
							<a href="<?php echo ENV_WEBROOT_FULL_URL;?>comlec_ordenlecturas/digitarlecturas/<?php echo $obj_cronograma[0][0]['idsector'];?>" target="_blank">
							Ir a digitaci&oacute;n.
							</a>
							<?php }?>
							</td>
							<td class="text-center">
							<?php 
							if(($cantidad_suministros-$cantidad_finalizados)==0){
							?>
							<span class="badge badge-success">OK</span>
							<?php }else{?>
							<span class="badge badge-danger">NO</span>
							<?php }?>
							</td>
						</tr>
						<tr>
							<td>Consistentes</td>
							<td class="text-center"><span class="badge badge-info"><?php echo $cantidad_consistentes;?></span></td>
							<td class="text-center"><span class="badge badge-info"><?php echo ($cantidad_suministros-$cantidad_consistentes);?></span></td>
							<td class="text-center">
							<?php 
							if(($cantidad_suministros-$cantidad_consistentes)==0){
							?>
							<span class="badge badge-success">OK</span>
							<?php }else{?>
							<span class="badge badge-danger">NO</span>
							<?php }?>
							</td>
						</tr>
						<tr>
							<td>Inconsistentes</td>
							<td class="text-center">
							<?php if($cantidad_inconsistentes > 0){?>
							<a href="<?php echo ENV_WEBROOT_FULL_URL;?>EvaluarInconsistencias/evaluar_inconsistencia/U/<?php echo date('Y-m-d');?>/<?php echo date('Y-m-d');?>/0/<?php echo $obj_cronograma[0][0]['idciclo'];?>/<?php echo $obj_cronograma[0][0]['idsector'];?>/0/0/0/0/0/" target="_blank">
							<span class="badge badge-info"><?php echo $cantidad_inconsistentes;?></span>
							</a>
							<a href="<?php echo ENV_WEBROOT_FULL_URL;?>EvaluarInconsistencias/evaluar_inconsistencia/U/<?php echo date('Y-m-d');?>/<?php echo date('Y-m-d');?>/0/<?php echo $obj_cronograma[0][0]['idciclo'];?>/<?php echo $obj_cronograma[0][0]['idsector'];?>/0/0/0/0/0/" target="_blank">
							ver
							</a>
							<?php }else{?>
							<span class="badge badge-info"><?php echo $cantidad_inconsistentes;?></span>
							<?php }?>
							</td>
							<td class="text-center"><span class="badge badge-info"><?php echo $cantidad_inconsistentes;?></span></td>
							<td class="text-center">
							<?php 
							if($cantidad_inconsistentes==0){
							?>
							<span class="badge badge-success">OK</span>
							<?php }else{?>
							<a href="<?php echo ENV_WEBROOT_FULL_URL;?>EvaluarInconsistencias/evaluar_inconsistencia/U/<?php echo date('Y-m-d');?>/<?php echo date('Y-m-d');?>/0/<?php echo $obj_cronograma[0][0]['idciclo'];?>/<?php echo $obj_cronograma[0][0]['idsector'];?>/0/0/0/0/0/" target="_blank">
							<span class="badge badge-danger">NO</span>
							</a>
							<?php }?>
							</td>
						</tr>
						<tr>
							<td>Enviadas a Distriluz <span class="lbl_estado_envio_distriluz">
							<?php 
							if(isset($obj_cronograma[0][0]['fechaexportacion']) && ($cantidad_consistentes-$cantidad_enviadas_distriluz)==0){
								echo '(finalizado)';
							}elseif(isset($obj_cronograma[0][0]['fechaexportacion']) && ($cantidad_consistentes-$cantidad_enviadas_distriluz)!=0){
								echo '(detenido)';
								?>
								<!-- <a href="javascript:void(0);" id="btn_reiniciar_envio" onclick="reiniciar_envio(<?php echo $id_cronograma;?>)">reiniciar envio</a> -->
								<?php 
							}else{
								echo '(enviando...)';
							}
							?>
							</span>
							</td>
							<td class="text-center"><span class="badge badge-info"><?php echo $cantidad_enviadas_distriluz;?></span></td>
							<td class="text-center"><span class="badge badge-info"><?php echo ($cantidad_consistentes-$cantidad_enviadas_distriluz);?></span></td>
							<td class="text-center">
							<?php 
							if(($cantidad_consistentes-$cantidad_enviadas_distriluz)==0){
							?>
							<span class="badge badge-success">OK</span>
							<?php }else{?>
							<span class="badge badge-danger">NO</span>
							<?php }?>
							</td>
						</tr>
					</tbody>
				</table>
				<table class="">
					<thead class="no-border">
						<tr>
							<th></th>
							<th></th>
							<th></th>
							<th></th>
						</tr>
					</thead>
					<tbody class="">
						<!-- 
						<tr>
							<td>Sin lectura y observaci&oacute;n</td>
							<td class="text-center"><span class="badge badge-warning"><?php //echo $cantidad_sin_lectura_y_obs;?></span></td>
							<td class="text-center"></td>
							<td class="text-center"></td>
						</tr>
						 -->
						<tr>
							<td>Observaciones sin lectura</td>
							<td class="text-center"><a href="<?php echo ENV_WEBROOT_FULL_URL;?>EvaluarInconsistencias/evaluar_inconsistencia/U/<?php echo date('Y-m-d');?>/<?php echo date('Y-m-d');?>/0/<?php echo $obj_cronograma[0][0]['idciclo'];?>/<?php echo $obj_cronograma[0][0]['idsector'];?>/0/0/0/0/OBSSL" target="_blank"><?php echo $cantidad_obs_sin_lectura;?></a></td>
							<td>Observaciones con lectura</td>
							<td class="text-center"><a href="<?php echo ENV_WEBROOT_FULL_URL;?>EvaluarInconsistencias/evaluar_inconsistencia/U/<?php echo date('Y-m-d');?>/<?php echo date('Y-m-d');?>/0/<?php echo $obj_cronograma[0][0]['idciclo'];?>/<?php echo $obj_cronograma[0][0]['idsector'];?>/0/0/0/0/OBSCL" target="_blank"><?php echo $cantidad_obs_con_lectura;?></a></td>
						</tr>
						<tr>
							<td>Observaciones total</td>
							<td class="text-center"><a href="<?php echo ENV_WEBROOT_FULL_URL;?>EvaluarInconsistencias/evaluar_inconsistencia/U/<?php echo date('Y-m-d');?>/<?php echo date('Y-m-d');?>/0/<?php echo $obj_cronograma[0][0]['idciclo'];?>/<?php echo $obj_cronograma[0][0]['idsector'];?>/0/0/0/0/OBSSL,OBSCL" target="_blank"><?php echo $cantidad_obs_sin_lectura+$cantidad_obs_con_lectura;?></a></td>
							<td>Observaci&oacute;n 99</td>
							<td class="text-center"><a href="<?php echo ENV_WEBROOT_FULL_URL;?>EvaluarInconsistencias/evaluar_inconsistencia/U/<?php echo date('Y-m-d');?>/<?php echo date('Y-m-d');?>/0/<?php echo $obj_cronograma[0][0]['idciclo'];?>/<?php echo $obj_cronograma[0][0]['idsector'];?>/0/0/0/0/OBS99" target="_blank"><?php echo $cantidad_obs_99;?></a></td>
						</tr>
						<tr>
							<td>Consumo cero</td>
							<td class="text-center"><a href="<?php echo ENV_WEBROOT_FULL_URL;?>EvaluarInconsistencias/evaluar_inconsistencia/U/<?php echo date('Y-m-d');?>/<?php echo date('Y-m-d');?>/0/<?php echo $obj_cronograma[0][0]['idciclo'];?>/<?php echo $obj_cronograma[0][0]['idsector'];?>/0/0/0/0/CC" target="_blank"><?php echo $cantidad_consumo_cero;?></a></td>
							<td>Consumo menor que 35</td>
							<td class="text-center"><a href="<?php echo ENV_WEBROOT_FULL_URL;?>EvaluarInconsistencias/evaluar_inconsistencia/U/<?php echo date('Y-m-d');?>/<?php echo date('Y-m-d');?>/0/<?php echo $obj_cronograma[0][0]['idciclo'];?>/<?php echo $obj_cronograma[0][0]['idsector'];?>/0/0/0/35/0" target="_blank"><?php echo $cantidad_consumo_menor35;?></a></td>					
						</tr>
						<tr>
							<td>Consumo mayor 100% que el anterior</td>
							<td class="text-center"><a href="<?php echo ENV_WEBROOT_FULL_URL;?>EvaluarInconsistencias/evaluar_inconsistencia/U/<?php echo date('Y-m-d');?>/<?php echo date('Y-m-d');?>/0/<?php echo $obj_cronograma[0][0]['idciclo'];?>/<?php echo $obj_cronograma[0][0]['idsector'];?>/0/0/0/0/C100MA" target="_blank"><?php echo $cantidad_consumo_mayor100_porciento;?></a></td>
							<td>Consumo mayor que 1000</td>
							<td class="text-center"><a href="<?php echo ENV_WEBROOT_FULL_URL;?>EvaluarInconsistencias/evaluar_inconsistencia/U/<?php echo date('Y-m-d');?>/<?php echo date('Y-m-d');?>/0/<?php echo $obj_cronograma[0][0]['idciclo'];?>/<?php echo $obj_cronograma[0][0]['idsector'];?>/0/0/1000/0/0" target="_blank"><?php echo $cantidad_consumo_mayor1000;?></a></td>
						</tr>
					</tbody>
				</table>
			</div>
		</div>
		
	</div>
	
</div>
<script>
function exportar_fecha(id){

	if(confirm('¿Esta seguro de Exportar y Cerrar el Proceso para esta fecha?')){
		$("#btn_exportar"+id).attr("href",'');
		var ref=ENV_WEBROOT_FULL_URL+'ComlecOrdenlecturas/exportarlecturas?id='+id;
		
		var fecha=$("#input_exportar"+id).val();
		          
		$("#btn_exportar"+id).attr("href",ref+'&fechaexportacion='+fecha);
	
	}          
}
function solo_exportar(id){

	$("#btn_solo_exportar"+id).attr("href",'');
	var ref=ENV_WEBROOT_FULL_URL+'ComlecOrdenlecturas/exportarlecturas_cronograma?id='+id;
	$("#btn_solo_exportar"+id).attr("href",ref);
}
function reiniciar_envio(id){
	$.ajax({
		url : ENV_WEBROOT_FULL_URL+"ComlecOrdenlecturas/reiniciar_envio_distriluz?id="+id,
		type : "GET",
		dataType : 'html'
	}).done(function(data){
		alert('El envio automatico a Distriluz se reiniciara maximo en 5 minutos, por favor espere!!');
		$('.lbl_estado_envio_distriluz').html('(enviando...)');
	});
}
function eliminar_evento(id){
	if(confirm('¿Esta seguro que desea eliminar este evento del cronograma?')){
		$.ajax({
			url : ENV_WEBROOT_FULL_URL+"ComlecOrdenlecturas/eliminar_evento_cronograma/"+id,
			type : "GET",
			dataType : 'json',
			success:function(data, textStatus, jqXHR){
				if(data.success){
					parent.$.fancybox.close();
					parent.$('#buscar-listado-cronograma').click();
				}
			}
		});
	}
}
</script>