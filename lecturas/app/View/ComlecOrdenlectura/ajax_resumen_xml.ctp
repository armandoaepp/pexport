<div class="cl-mcont">

	<div class="col-sm-4 col-md-4">
	
		<div class="block">
			<div class="content">
				<h2 class="page-title">Exportar Archivo</h2>
			</div>
			<div class="app-nav">
				<div class="content">
					<div class="input-group date datetime" data-min-view="1"
						data-date-format="yyyy-mm-dd">
						<input id="input_exportar<?php echo $id_xml;?>"
							class="form-control" size="16" type="text"
							value="<?php echo date('Y-m-d');?>" readonly> <span
							class="input-group-addon btn btn-primary"><span
							class="glyphicon glyphicon-th"></span></span>
					</div>
					<a onclick="exportar_fecha(<?php echo $id_xml;?>)"
						id="btn_exportar<?php echo $id_xml;?>"
						href="javascript:void(0);"
						class="btn btn-success"
						data-popover="popover"
						data-original-title="Exportar y Cerrar Proceso" 
						data-content="Es muy importante y se debe ejecutar solo cuando estamos seguros que deseamos cerrar el proceso. Ademas detendra el envio autom&aacute;tico a <?php echo $this->Session->read('NEGOCIO_empresa');?> para este XML." 
						data-placement="top" 
						data-trigger="hover">Exportar y Cerrar Proceso</a>
					<br>
					<a onclick="solo_exportar(<?php echo $id_xml;?>)"
					id="btn_solo_exportar<?php echo $id_xml;?>"
					href="javascript:void(0);"
					class="btn btn-info"
					data-popover="popover"
					data-original-title="Solo Exportar" 
					data-content="Permite descargar un archivo xml sin afectar el proceso." 
					data-placement="right" 
					data-trigger="hover">Solo Exportar</a>
				</div>
			</div>
		</div>
	
	</div>
	
	<div class="col-sm-8 col-md-8">
	
		<div class="block-flat">
			<div class="header">
				<h3>Resumen Xml: <?php echo $id_xml;?></h3>
			</div>
			<div class="content">
				<table class="">
					<thead class="no-border">
						<tr>
							<th></th>
							<th class="text-center">Actual</th>
							<th class="text-center">Pendiente</th>
							<th class="text-center">Estado</th>
						</tr>
					</thead>
					<tbody class="">
						<tr>
							<td style="width:30%;">Suministros</td>
							<td class="text-center"><span class="badge"><?php echo $cantidad_suministros;?></span></td>
							<td class="text-center"></td>
							<td class="text-center"></td>
						</tr>
						<tr>
							<td style="width:30%;">Asignadas</td>
							<td class="text-center"><span class="badge"><?php echo $cantidad_asignados;?></span></td>
							<td class="text-center"><span class="badge badge-info"><?php echo ($cantidad_suministros-$cantidad_asignados);?></span></td>
							<td class="text-center">
							<?php 
							if(($cantidad_suministros-$cantidad_asignados)==0){
							?>
							<span class="badge badge-success">OK</span>
							<?php }else{?>
							<span class="badge badge-danger">NO</span>
							<?php }?>
							</td>
						</tr>
						<tr>
							<td>Descargadas</td>
							<td class="text-center"><span class="badge"><?php echo $cantidad_descargados;?></span></td>
							<td class="text-center"><span class="badge badge-info"><?php echo ($cantidad_asignados-$cantidad_descargados);?></span></td>
							<td class="text-center">
							<?php 
							if(($cantidad_asignados-$cantidad_descargados)==0){
							?>
							<span class="badge badge-success">OK</span>
							<?php }else{?>
							<span class="badge badge-danger">NO</span>
							<?php }?>
							</td>
						</tr>
						<tr>
							<td>Finalizadas</td>
							<td class="text-center"><span class="badge badge-info"><?php echo $cantidad_finalizados;?></span></td>
							<td class="text-center"><span class="badge badge-info"><?php echo ($cantidad_suministros-$cantidad_finalizados);?></span></td>
							<td class="text-center">
							<?php 
							if(($cantidad_suministros-$cantidad_finalizados)==0){
							?>
							<span class="badge badge-success">OK</span>
							<?php }else{?>
							<span class="badge badge-danger">NO</span>
							<?php }?>
							</td>
						</tr>
						<tr>
							<td>Consistentes</td>
							<td class="text-center"><span class="badge badge-info"><?php echo $cantidad_consistentes;?></span></td>
							<td class="text-center"><span class="badge badge-info"><?php echo ($cantidad_suministros-$cantidad_consistentes);?></span></td>
							<td class="text-center">
							<?php 
							if(($cantidad_suministros-$cantidad_consistentes)==0){
							?>
							<span class="badge badge-success">OK</span>
							<?php }else{?>
							<span class="badge badge-danger">NO</span>
							<?php }?>
							</td>
						</tr>
						<tr>
							<td>Inconsistentes</td>
							<td class="text-center">
							<?php if($cantidad_inconsistentes > 0){?>
							<a href="<?php echo ENV_WEBROOT_FULL_URL;?>EvaluarInconsistencias/evaluar_inconsistencia/U/<?php echo date('Y-m-d');?>/<?php echo date('Y-m-d');?>/0/0/0/0/0/0/0/0/<?php echo $id_xml;?>" target="_blank">
							<span class="badge badge-info"><?php echo $cantidad_inconsistentes;?></span>
							</a>
							<a href="<?php echo ENV_WEBROOT_FULL_URL;?>EvaluarInconsistencias/evaluar_inconsistencia/U/<?php echo date('Y-m-d');?>/<?php echo date('Y-m-d');?>/0/0/0/0/0/0/0/0/<?php echo $id_xml;?>" target="_blank">
							ver
							</a>
							<?php }else{?>
							<span class="badge badge-info"><?php echo $cantidad_inconsistentes;?></span>
							<?php }?>
							</td>
							<td class="text-center"><span class="badge badge-info"><?php echo $cantidad_inconsistentes;?></span></td>
							<td class="text-center">
							<?php 
							if($cantidad_inconsistentes==0){
							?>
							<span class="badge badge-success">OK</span>
							<?php }else{?>
							<a href="<?php echo ENV_WEBROOT_FULL_URL;?>EvaluarInconsistencias/evaluar_inconsistencia/U/<?php echo date('Y-m-d');?>/<?php echo date('Y-m-d');?>/0/0/0/0/0/0/0/0/<?php echo $id_xml;?>" target="_blank">
							<span class="badge badge-danger">NO</span>
							</a>
							<?php }?>
							</td>
						</tr>
						<tr>
							<td>Enviadas a Distriluz <span class="lbl_estado_envio_distriluz">
							<?php 
							if(isset($obj_xml[0][0]['fechaexportacion']) && ($cantidad_consistentes-$cantidad_enviadas_distriluz)==0){
								echo '(finalizado)';
							}elseif(isset($obj_xml[0][0]['fechaexportacion']) && ($cantidad_consistentes-$cantidad_enviadas_distriluz)!=0){
								echo '(detenido)';
								?>
								<a href="javascript:void(0);" id="btn_reiniciar_envio" onclick="reiniciar_envio(<?php echo $id_xml;?>)">reiniciar envio</a>
								<?php 
							}else{
								echo '(enviando...)';
							}
							?>
							</span>
							</td>
							<td class="text-center"><span class="badge badge-info"><?php echo $cantidad_enviadas_distriluz;?></span></td>
							<td class="text-center"><span class="badge badge-info"><?php echo ($cantidad_consistentes-$cantidad_enviadas_distriluz);?></span></td>
							<td class="text-center">
							<?php 
							if(($cantidad_consistentes-$cantidad_enviadas_distriluz)==0){
							?>
							<span class="badge badge-success">OK</span>
							<?php }else{?>
							<span class="badge badge-danger">NO</span>
							<?php }?>
							</td>
						</tr>
					</tbody>
				</table>
				<table class="">
					<thead class="no-border">
						<tr>
							<th></th>
							<th></th>
							<th></th>
							<th></th>
						</tr>
					</thead>
					<tbody class="">
						<!-- 
						<tr>
							<td>Sin lectura y observaci&oacute;n</td>
							<td class="text-center"><span class="badge badge-warning"><?php //echo $cantidad_sin_lectura_y_obs;?></span></td>
							<td class="text-center"></td>
							<td class="text-center"></td>
						</tr>
						 -->
						<tr>
							<td>Observaciones sin lectura</td>
							<td class="text-center"><a href="<?php echo ENV_WEBROOT_FULL_URL;?>EvaluarInconsistencias/evaluar_inconsistencia/U/<?php echo date('Y-m-d');?>/<?php echo date('Y-m-d');?>/0/0/0/0/0/0/0/OBSSL/<?php echo $id_xml;?>" target="_blank"><?php echo $cantidad_obs_sin_lectura;?></a></td>
							<td>Observaciones con lectura</td>
							<td class="text-center"><a href="<?php echo ENV_WEBROOT_FULL_URL;?>EvaluarInconsistencias/evaluar_inconsistencia/U/<?php echo date('Y-m-d');?>/<?php echo date('Y-m-d');?>/0/0/0/0/0/0/0/OBSCL/<?php echo $id_xml;?>" target="_blank"><?php echo $cantidad_obs_con_lectura;?></a></td>
						</tr>
						<tr>
							<td>Observaciones total</td>
							<td class="text-center"><a href="<?php echo ENV_WEBROOT_FULL_URL;?>EvaluarInconsistencias/evaluar_inconsistencia/U/<?php echo date('Y-m-d');?>/<?php echo date('Y-m-d');?>/0/0/0/0/0/0/0/OBSSL,OBSCL/<?php echo $id_xml;?>" target="_blank"><?php echo $cantidad_obs_sin_lectura+$cantidad_obs_con_lectura;?></a></td>
							<td>Observaci&oacute;n 99</td>
							<td class="text-center"><a href="<?php echo ENV_WEBROOT_FULL_URL;?>EvaluarInconsistencias/evaluar_inconsistencia/U/<?php echo date('Y-m-d');?>/<?php echo date('Y-m-d');?>/0/0/0/0/0/0/0/OBS99/<?php echo $id_xml;?>" target="_blank"><?php echo $cantidad_obs_99;?></a></td>
						</tr>
						<tr>
							<td>Consumo cero</td>
							<td class="text-center"><a href="<?php echo ENV_WEBROOT_FULL_URL;?>EvaluarInconsistencias/evaluar_inconsistencia/U/<?php echo date('Y-m-d');?>/<?php echo date('Y-m-d');?>/0/0/0/0/0/0/0/CC/<?php echo $id_xml;?>" target="_blank"><?php echo $cantidad_consumo_cero;?></a></td>
							<td>Consumo menor que 35</td>
							<td class="text-center"><a href="<?php echo ENV_WEBROOT_FULL_URL;?>EvaluarInconsistencias/evaluar_inconsistencia/U/<?php echo date('Y-m-d');?>/<?php echo date('Y-m-d');?>/0/0/0/0/0/0/35/0/<?php echo $id_xml;?>" target="_blank"><?php echo $cantidad_consumo_menor35;?></a></td>					
						</tr>
						<tr>
							<td>Consumo mayor 100% que el anterior</td>
							<td class="text-center"><a href="<?php echo ENV_WEBROOT_FULL_URL;?>EvaluarInconsistencias/evaluar_inconsistencia/U/<?php echo date('Y-m-d');?>/<?php echo date('Y-m-d');?>/0/0/0/0/0/0/0/C100MA/<?php echo $id_xml;?>" target="_blank"><?php echo $cantidad_consumo_mayor100_porciento;?></a></td>
							<td>Consumo mayor que 1000</td>
							<td class="text-center"><a href="<?php echo ENV_WEBROOT_FULL_URL;?>EvaluarInconsistencias/evaluar_inconsistencia/U/<?php echo date('Y-m-d');?>/<?php echo date('Y-m-d');?>/0/0/0/0/0/1000/0/0/<?php echo $id_xml;?>" target="_blank"><?php echo $cantidad_consumo_mayor1000;?></a></td>
						</tr>
					</tbody>
				</table>
			</div>
		</div>
		
	</div>
	
</div>
<script>
function exportar_fecha(id){

	if(confirm('¿Esta seguro de Exportar y Cerrar el Proceso para esta fecha?')){
		$("#btn_exportar"+id).attr("href",'');
		var ref=ENV_WEBROOT_FULL_URL+'ComlecOrdenlecturas/exportarlecturas?id='+id;
		
		var fecha=$("#input_exportar"+id).val();
		          
		$("#btn_exportar"+id).attr("href",ref+'&fechaexportacion='+fecha);
	
	}          
}
function solo_exportar(id){

	$("#btn_solo_exportar"+id).attr("href",'');
	var ref=ENV_WEBROOT_FULL_URL+'ComlecOrdenlecturas/exportarlecturas_xml?id='+id;
	$("#btn_solo_exportar"+id).attr("href",ref);
}
function reiniciar_envio(id){
	$.ajax({
		url : ENV_WEBROOT_FULL_URL+"/ComlecOrdenlecturas/reiniciar_envio_distriluz?id="+id,
		type : "GET",
		dataType : 'html'
	}).done(function(data){
		alert('El envio automatico a Distriluz se reiniciara maximo en 5 minutos, por favor espere!!');
		$('.lbl_estado_envio_distriluz').html('(enviando...)');
	});
}
</script>