<?php 
if (!isset($NEGOCIO)) {
    header("Location:".ENV_WEBROOT_FULL_URL."/Usuarios/logout");
    die();
}
?>
<?php 
if ($id_usuario == 25 || $id_usuario == 131){
	$permiso_lectura = 'false';
}else {
	$permiso_lectura = 'true';
}
echo $this->element('menu',array('active'=>'listarciclosector', 'open'=>'true', 'permiso_lectura' =>$permiso_lectura, 'id_usuario ' =>$id_usuario));

if (!isset($NEGOCIO)) {
    header("Location:/Usuarios/logout");
    die();
}
?>


<div class="container-fluid" id="pcont">
	<!--
	<div class="page-head" style="padding: 0px;">

		<ol class="breadcrumb">
			<li><button type="button"
					class="btn-xs btn-default arbol-top-lecturas">Arbol</button></li>
			<li><a href="#">Ordenes de Lectura</a></li>
			<li><a href="#">Asignar</a></li>
			<li class="active">Lecturas</li>
		</ol>
	</div>
	-->

	<div class="cl-mcont">
		<div class="col-sm-12 col-md-12">

			<div class="panel-group accordion accordion" id="accordion4">
				<!--div class="panel-group accordion accordion-semi" id="accordion4"-->
				<div class="panel panel-default">
					<div class="panel-heading">
						<!--div class="panel-heading success"-->
						<h4 class="panel-title">
							<a data-toggle="collapse" data-parent="#accordion4" href="#ac4-1"
								id="accordion-tab1"> <i class="fa fa-angle-right"></i> Ordenes Ciclo/<?php if($NEGOCIO == 'CIX'){echo 'Por Sector';}elseif ($NEGOCIO == 'TAC'){echo 'Por Zona';}else{echo 'Por Sector';}?>
							</a>
						</h4>
					</div>
					<div id="ac4-1" class="panel-collapse collapse in">
						<div class="panel-body">
							<div class="content">
								<div class='tabla'>
									<div id='asig' class='farmsmall2'>
										<div class='table-responsive'>

											<div id="grid">
												<table class="table table-bordered dataTable"
													id="listar_asignacion">
													<!--thead class="primary-emphasis"-->
													<thead class="primary-emphasis">
														<tr>
															<th>Unidad de Negocio</th>
															<th>Ciclo</th>
															<th><?php if($NEGOCIO == 'CIX'){echo 'Sector';}elseif ($NEGOCIO == 'TAC'){echo 'Zona';}else{echo 'Sector';}?></th>
															<th>% Ordenes Asignadas</th>
															<th>Total Ordenes</th>
															<th>Asignados</th>
															<th>Restantes</th>
															<th>Acci&oacute;n</th>
														</tr>
													</thead>
													<tfoot style="font-size: 14px;">
														<tr>
														</tr>
													</tfoot>
													<tbody>
												<?php									
												foreach ($listar_orden_lecturas as $obj_orden_lectura){
												?>
													<tr style="cursor: pointer;" class="odd gradeX"
															id="orden_lectura"
															idciclo="<?php echo $obj_orden_lectura[0]['idciclo']?>"
															idsector="<?php echo $obj_orden_lectura[0]['sector']?>">
															<td><?php echo $obj_orden_lectura[0]['unidad_neg'].' - '.$obj_orden_lectura[0]['nombreunidadnegocio']?></td>
															<td><?php echo $obj_orden_lectura[0]['idciclo']?></td>
															<td><?php echo $obj_orden_lectura[0]['sector']?></td>
															<td><div class="progress progress-striped active">
													<?php 
													$porcentaje = $obj_orden_lectura[0]['porcentaje'];
													if($porcentaje == '100'){
														$str_class_porcentaje = 'progress-bar-success';
													}elseif($porcentaje >= '80'){
														$str_class_porcentaje = 'progress-bar-info';
													}elseif($porcentaje >= '40'){
														$str_class_porcentaje = 'progress-bar-warning';
													}else{
														$str_class_porcentaje = 'progress-bar-danger';
													}
													?>
														  <div class="progress-bar <?php echo $str_class_porcentaje;?>" style="width: <?php echo $porcentaje;?>%"><?php echo $porcentaje;?>%</div>
																</div></td>
															<td><?php echo $obj_orden_lectura[0]['total']?></td>
															<td><?php echo $obj_orden_lectura[0]['total_asignados']?></td>
															<td><?php echo $obj_orden_lectura[0]['total'] - $obj_orden_lectura[0]['total_asignados']?></td>
															<td>
																<?php 
																if($obj_orden_lectura[0]['total_asignados']==0){
																	$obj_evento_cronograma = $model_ComlecOrdenlectura->existeEventoCronograma($obj_orden_lectura[0]['unidad_neg'],null,$obj_orden_lectura[0]['idciclo'],$obj_orden_lectura[0]['sector']);
																	if(count($obj_evento_cronograma)>0){
																		$str_cronograma_si = 'data-cronograma="true" data-cronograma_fecha="'.substr($obj_evento_cronograma[0]['0']['fecha'],0,10).'"';
																	}else{
																		$str_cronograma_si = 'data-cronograma="false"';
																	}?>
																	<a href="javascript:;"
																 	<?php echo $str_cronograma_si;?>
																	class="btn btn-primary btn-sm btn-flat md-trigger"
																	data-modal="modal-asignar-por-sector"
																	onclick="showFormAsignacionSector(this,'<?php echo $obj_orden_lectura[0]['idciclo']?>','<?php echo $obj_orden_lectura[0]['sector']?>','<?php echo $obj_orden_lectura[0]['unidad_neg']?>');event.stopPropagation();">Asignar</a>
																<?php }else{?>
																	<a href="javascript:;" disabled
																	class="btn btn-sm btn-flat md-trigger"
																	>Asignar</a>
																<?php }?>
															</td>
														</tr>
												<?php
												}
												?>
												</tbody>
												</table>
											</div>
										</div>
									</div>
								</div>
							</div>

						</div>
					</div>
				</div>
			</div>

			<div class="block"> 				
			
				<?php 
					echo $this->element('ComlecOrdenlectura/modal_comlec_ordenlectura');
				?>
								
			</div>
		</div>

		<div class="col-sm-12 col-md-12">
			<span class="tabla_buscar_ciclo_sector_loading"
				style="display: none;"><img
				src="<?php echo ENV_WEBROOT_FULL_URL ?>/js/jquery.select2/select2-spinner.gif">
				Cargando...</span>
		</div>
		<div id="tabla_buscar_ciclo_sector"></div>
	</div>
</div>
<div id="arbol" class="page-aside app filters tree" style="zoom: 1;">
	<div class="fixed nano nscroller has-scrollbar">
		<div class="content">
			<div class="header">
				<button class="navbar-toggle" data-target=".app-nav"
					data-toggle="collapse" type="button">
					<span class="fa fa-chevron-down"></span>
				</button>
				<h2 class="page-title">Asignar</h2>
				<p class="description">Criterios de Busqueda</p>
			</div>

			<div class="app-nav collapse">
				<div class="form-group" style="width: 90%;">
					<label class="control-label">Tipo de Lectura</label> <select
						class="" name="id_tipolectura" id="id_tipolectura">
						<option value="L" <?php if($tipo=='L'){ echo 'selected';}?>>Lectura</option>
						<option value="R" <?php if($tipo=='R'){ echo 'selected';}?>>Relectura</option>
					</select>
				</div>

				<div class="por_ubicacion" style="width: 90%;">

					<div class="form-group">
						<label class="control-label">Por Unidad Negocio:</label>              
		              	<?php 
		        			  	echo '<select class="" name="unidadneg" id="unidadneg">
		        			  	<option value="0">Todos</option>';
	                                             //   var_dump($listar_unidadneg);exit;
		        				foreach ($listar_unidadneg as $v){
									if($unidadneg==$v['0']['unidadneg']){
		        						echo '<option value='.$v['0']['unidadneg'].' selected>'.$v['0']['unidadneg'].' - '.$v['0']['nombreunidadnegocio'].'</option>';
		        					}else{
										echo '<option value='.$v['0']['unidadneg'].'>'.$v['0']['unidadneg'].' - '.$v['0']['nombreunidadnegocio'].'</option>';
									}
		        				}
		        				echo '</select>';	
		        		?>        			
		            </div>

					<div class="form-group">
						<label class="control-label">Por Ciclo:<span class="ciclo_loading"
							style="display: none;"><img
								src="<?php echo ENV_WEBROOT_FULL_URL ?>/js/jquery.select2/select2-spinner.gif"></span></label>              
		              	<?php 
		        			  	echo '<select class="" name="id_ciclo" id="id_ciclo">
		        			  	<option value="0">Todos</option>';
		        				foreach ($listar_ciclo as $v){
									if($id_ciclo==$v['0']['idciclo']){
		        						echo '<option value='.$v['0']['idciclo'].' selected>'.$v['0']['idciclo'].' - '.$v['0']['nombciclo'].'</option>';
			        				}else{
			        					echo '<option value='.$v['0']['idciclo'].'>'.$v['0']['idciclo'].' - '.$v['0']['nombciclo'].'</option>';
			        				}
		        				}
		        				echo '</select>';	
		        		?>        			
		            </div>

					<div class="form-group">
						<label class="control-label"><?php if($NEGOCIO == 'CIX'){echo 'Por Sector';}elseif ($NEGOCIO == 'TAC'){echo 'Por Zona';}else{echo 'Por Sector';}?><span
							class="sector_loading" style="display: none;"><img
								src="<?php echo ENV_WEBROOT_FULL_URL ?>/js/jquery.select2/select2-spinner.gif"></span></label>              
		              	<?php 
		        			  	echo '<select class="" name="monitoreo_id_sector" id="monitoreo_id_sector">
		        			  	<option value="0">Todos</option>';
		        				foreach ($listar_sector as $v){
									if($id_sector==$v['0']['sector']){
		        						echo '<option value='.$v['0']['sector'].' selected>'.$v['0']['sector'].'</option>';
		        					}else{
										echo '<option value='.$v['0']['sector'].'>'.$v['0']['sector'].'</option>';
									}
		        				}
		        				echo '</select>';	
		        		?>        			
		            </div>
				</div>

				<button class='btn btn-primary' id='buscar-listar-asignacion'>Buscar</button>
			</div>
		</div>
	</div>
</div>
<!-- Modal Asignacion Nuevo - Geynen -->
<div class="md-modal colored-header custom-width success md-effect-10"
	id="modal-asignar-por-sector" style="width: 100%; top: 182px;">
	<div class="md-content">
		<div class="modal-header">
			<h3>
				Unidad Negocio:
				<button type="button" class="btn btn-success lbl_negocio"></button>
				Ciclo:
				<button type="button" class="btn btn-success lbl_ciclo"></button>
							<?php
							
							if ($NEGOCIO == 'CIX') {
								echo 'Sector';
							} elseif ($NEGOCIO == 'TAC') {
								echo 'Zona';
							} else {
								echo 'Sector';
							}
							?>
							:
							<button type="button" class="btn btn-success lbl_sector"></button>
						</h3>
			<button type="button" class="close md-close" data-dismiss="modal"
				aria-hidden="true">&times;</button>
		</div>
		<div class="modal-body">
			<div id="formulario_asignar_por_sector">
				<div class="text-center">
					<br>
					<br>
					<div class="i-circle primary">
						<i class="fa fa-check"></i>
					</div>
					<h4>sistema de lectura</h4>
					<p>Estamos mejorando para hacerte la vida m&aacute;s f&aacute;cil!</p>
				</div>
			</div>
			<div id="formulario_asignar_por_sector_no">
				<div class="alert alert-warning alert-white rounded">
					<button type="button" class="close" data-dismiss="alert"
						aria-hidden="true">×</button>
					<div class="icon">
						<i class="fa fa-warning"></i>
					</div>
					<strong>Alerta!</strong> No se puede Asignar. Usted debe agregar el
					ciclo y sector al cronograma <a
						href="<?php echo ENV_WEBROOT_FULL_URL;?>comlec_ordenlecturas/cronograma">aqu&iacute;</a>.
				</div>
				<br>
			</div>
		</div>
		<div class="modal-footer"></div>
	</div>
</div>


<div class="md-overlay" style="margin-top: 30px;"></div>
<script>
function showFormAsignacionSector(target, ciclo, sector, unidadnegocio){

    $(".lbl_ciclo").html(ciclo);
    $(".lbl_sector").html(sector);
    $(".lbl_negocio").html(unidadnegocio);

	if(!$(target).data('cronograma')){
    	$('#formulario_asignar_por_sector_no').show();
    	$('#formulario_asignar_por_sector').hide();        	
	}else{
		$('#dpt_date_end').val($(target).data('cronograma_fecha'));
		$('#formulario_asignar_por_sector_no').hide();
    	$('#formulario_asignar_por_sector').show();

    	$("#formulario_asignar_por_sector").mask("Cargando...");
    	$.ajax({
			url : ENV_WEBROOT_FULL_URL+'ComlecOrdenlecturas/form_asignar_por_sector/'+unidadnegocio+'/'+ciclo+'/'+sector,
			type: "POST",
			success:function(data, textStatus, jqXHR){
				$('#formulario_asignar_por_sector').html(data);

				$(".cbo_lecturista",$('#formulario_asignar_por_sector')).select2({
		        	width: '200px'
		        });
				$('.bslider').slider();
				$(".datetime").datetimepicker({autoclose: true});
				$("#formulario_asignar_por_sector").unmask();
			}
		});
	}
}
</script>