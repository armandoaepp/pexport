<?php 
if (!isset($NEGOCIO)) {
   header("Location:".ENV_WEBROOT_FULL_URL."/Usuarios/logout");
    die();
}
?>

<?php 
if ((isset($id_usuario) && $id_usuario == 25) || (isset($id_usuario) && $id_usuario == 131)){
	$permiso_lectura = 'false';
}else {
	$permiso_lectura = 'true';
}
echo $this->element('menu',array('active'=>'digitarlecturas', 'open'=>'true', 'permiso_lectura' =>$permiso_lectura, 'id_usuario ' =>$id_usuario));
?>

<div class="container-fluid" id="pcont">
  <!--
    <div class="page-head" style="padding:0px;">
        
        <ol class="breadcrumb">
          <li><button type="button" class="btn-xs btn-default arbol-top-lecturas" >Arbol</button></li>
          <li><a href="#">Ordenes de Lectura</a></li>
          <li><a href="#">Digitar</a></li>
          <li class="active">Proceso de Digitación de Lecturas</li>
        </ol>
    </div>
  -->

	<div class="cl-mcont">
		<div class="col-sm-12 col-md-12"  style='height:100%;' >
			<div class="block">
        <div class="content">
          
          <div id="loading-digitar" class="tabla4">
            <div id="digitar" class='farmsmall2'>
    			<table id="tabla_digitar" class="table table-bordered tableWithFloatingHeader" style="background-color:#fff;color:#000;font-weight: bold;font-size:14px; width:85%;">
                  <thead>
                    <tr>
                      <th width="10px" class="hidden">Codigo</th>
                      <th width="10px"><?php if($NEGOCIO == 'CIX'){echo 'Orden';}elseif ($NEGOCIO == 'TAC'){echo 'Suministro';}else{echo 'Orden';}?></th>
                      
                      <!--
                      <th width="10px" ><?php if($NEGOCIO == 'CIX'){echo 'Libro';}elseif ($NEGOCIO == 'TAC'){echo 'Folio';}else{echo 'Libro';}?></th>
                      
                      <th width="20" >Ciclo</th>
                      <th width="20" >Sector</th>
                      <th width="20" >Ruta</th>
                      
                      <th width="20" >Cliente</th>
                      <th width="20" >Medidor</th>
                      -->

                      <th width="15px" ><?php if($NEGOCIO == 'CIX'){echo 'Suministro';}elseif ($NEGOCIO == 'TAC'){echo 'Contrato';}else{echo 'Suministro';}?></th>
                      <!--
                      <th width="15" >Cliente</th>
                      -->
                      <th width="15px">Direccion</th>
                      <th width="10px">Lec Ant Ant</th>
                      <th width="10px">Lec Ant</th>
                      <th width="10px">Lec Actual</th> 
                      <th width="10px">Codigo</th>
                      <!-- 
                      <th width="10">Monto Consumo</th> 
                      -->
                      <th width="10px">Consumo</th>   
                      <th width="10px">R</th> 
                      <th width="10px">Promedio Consumo</th> 
                    </tr>                              
                  </thead>                	
           </table>
          </div>            
        </div>
       </div>

			</div>
    </div>
  </div>
</div>

  <div id="arbol" class="page-aside app filters tree" >
      <div class="fixed nano nscroller has-scrollbar">
        <div class="content">

          <div class="header">
             <button class="navbar-toggle" data-target=".app-nav" data-toggle="collapse" type="button">
                <span class="fa fa-chevron-down"></span>
             </button>          
             <h2 class="page-title">Filtros</h2>
             <p class="description">Criterios de Busqueda</p>
          </div>

          <div class="app-nav collapse ">
            <div class="form-group " style= "width: 80%;" >
              <label class="control-label" >Tipo de Lectura</label>
              <select class="" name="id_tipolectura" id="id_tipolectura">
                <option value="L">Lectura</option>
                <option value="R">Relectura</option>
              </select>
            </div>

            <div class="form-group" style= "width: 80%;" >

              <label class="control-label" ><?php if($NEGOCIO == 'CIX'){echo 'Por Sector';}elseif ($NEGOCIO == 'TAC'){echo 'Por Zona';}else{echo 'Por Sector';}?></label>
              <?php 

                  echo '<select class="" name="id_sector" id="id_sector">
                  <option value="0">Todos</option>';
                foreach ($listar_sector as $sector){
                  if($idsector==$sector['0']['sector']){
                    echo '<option value='.$sector['0']['sector'].' selected>'.$sector['0']['sector'].'</option>';
                  }else{
                    echo '<option value='.$sector['0']['sector'].'>'.$sector['0']['sector'].'</option>';
                  }
                }
                echo '</select>'; 
              ?>
            </div>


            <div class="form-group" style= "width: 80%;" >
              <label class="control-label"><?php if($NEGOCIO == 'CIX'){echo 'Por Ruta';}elseif ($NEGOCIO == 'TAC'){echo 'Por Libro';}else{echo 'Por Ruta';}?><span class="ruta_loading" style="display:none;"><img src="<?php echo ENV_WEBROOT_FULL_URL;?>/js/jquery.select2/select2-spinner.gif"></span></label>
                <?php 

                  echo '<select class="" name="id_ruta" id="id_ruta">
                  <option value="0">Todos</option>';
                foreach ($listar_ruta as $ruta){
                echo '<option value='.$ruta['0']['ruta'].'>'.$ruta['0']['ruta'].' - '.$ruta['0']['nombruta'].'</option>';
                }
                echo '</select>'; 
                ?>
              
            </div>


            <div class="form-group" style= "width: 80%;" >
              <label class="control-label">Por Lecturista: <span class="lecturista_loading" style="display:none;"><img src="<?php echo ENV_WEBROOT_FULL_URL;?>/js/jquery.select2/select2-spinner.gif"></span></label>
             <?php 
                //print_r($listar_empleados);
                  echo '<select class="select2" name="lecturista" id="id_lecturista">
                  <option value="0">Todos</option>';
                foreach ($listar_empleados as $empleados){
                echo '<option value='.$empleados['0']["id"].'>'.$empleados['0']["nombre"].' - '.$empleados['0']["nomusuario"].'</option>';
                }
                echo '</select>'; 
              ?>
            </div>
            
            <div class="form-group" style= "width: 80%;" >
                <label class="control-label"><?php if($NEGOCIO == 'cix'){echo 'Por Suministro';}elseif ($NEGOCIO == 'tac'){echo 'Por Contrato';}else{echo 'Por Suministro';}?></label>
                <input type="text" class="form-control" id="txt_search_by_suministro" name="txt_search_by_suministro" placeholder="Suministro">
            </div>

            <div class="form-group" style= "width: 80%;" >

              <label class="control-label" >Busqueda Especial (Individual)</label>
              <select class="select2" name="id_lectespecial" id="id_lectespecial">
                <option value="0">Ninguno</option>
                <?php
                $str_selected_combo = '';
        if(isset($idsector)){
          $str_selected_combo = 'selected';
        }
        ?>
                <option value="SA">Lectura Sin Asignar</option>
                <option value="SR">Lectura Sin Resolver</option>
                <option value="SC" <?php echo $str_selected_combo;?>>Lectura Sin Resultado</option>
              </select>
            </div>
            <!--
            <div class="form-group">
              <label class="control-label">Por Rango:</label>
              <p>Valores Entre <strong id="price1">250 KW</strong> y <strong id="price2">450 KW</strong></p>
              <input id="price-range" class="bslider form-control" type="text" data-slider-tooltip="hide" data-slider-value="[250,450]" data-slider-step="5" data-slider-max="1000" data-slider-min="10" value="" />
            </div>
            -->
            <button class='btn btn-primary' id='buscar-lectura'>Buscar Lecturas</button>
            <br/>
            <br/>
            <br/>
            <br/>
            <br/>
            <br/>
          </div>
        </div>
      </div>
   </div>   

   
<script>
<?php 
if(isset($idsector)){
?>
$('#buscar-lectura').click();
<?php 
}
?>
</script>




