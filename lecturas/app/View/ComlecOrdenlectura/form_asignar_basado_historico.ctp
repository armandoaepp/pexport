<div class="col-sm-4 col-md-4 col-lg-4" style="max-height: 500px;overflow-y: auto;">
	<div class="block-flat">
		<div class="header">
			<h3 class="visible-sm visible-md">Asignaci&oacute;n Periodo Anterior</h3>
			<h3 class="visible-lg">Asignaci&oacute;n Periodo Anterior</h3>
			<h3 class="visible-xs">Asignaci&oacute;n Periodo Anterior</h3>
		</div>
		<div class="content">

			<table>
				<thead>
					<tr>
						<th>Nro Suministros</th>
						<th>Desde - Hasta</th>
						<th>Lecturista</th>
					</tr>
				</thead>
				<tbody>
					<?php 
					foreach ($listarLibros_anterior as $obj1){
					?>
					<tr>
						<td><?php echo $obj1[0]['count'];?></td>
						<td><?php echo $obj1[0]['min'];?> - <?php echo $obj1[0]['max'];?></td>
						<td><?php echo $obj1[0]['lecturista'];?></td>
					</tr>
					<?php 
					}
					?>
				</tbody>
			</table>
		</div>
	</div>
</div>

<div class="col-sm-8 col-md-8 col-lg-8" style="max-height: 500px;overflow-y: auto;">
	<div class="block-flat">
		<div class="header">
			<h3 class="visible-sm visible-md">Asignaci&oacute;n Periodo Actual</h3>
			<h3 class="visible-lg">Asignaci&oacute;n Periodo Actual</h3>
			<h3 class="visible-xs">Asignaci&oacute;n Periodo Actual</h3>
		</div>
		<div class="content">

			<table id="table_asignar_lecturistas_nuevo_form">
				<thead>
					<tr>
						<th>Nro Suministros</th>
						<th>Desde - Hasta</th>
						<th>Fecha</th>
						<th>Lecturista</th>
						<th>Estado</th>
					</tr>
				</thead>
				<tbody>
					<?php 
					if($sum_asignados_periodo_actual>0 || ($sum_asignados_periodo_actual==0 && count($listarLibros_anterior)==0)){
						// Si tiene suministros asignados o no tienen asignados y no tiene datos periodo anterior
						$i=1;
						foreach ($listarLibros as $obj1){
							$enabled_control = '';
							$estado_asignacion = 'pendiente';
							if($obj1[0]['libro']!=''){
								$enabled_control = 'disabled';
								$estado_asignacion = '';
							}
							?>
						<tr class="<?php echo $estado_asignacion;?>">
							<td><?php echo $obj1[0]['count'];?></td>
							<td>		
								<input class="bslider<?php echo $i;?> bslider form-control" <?php echo $enabled_control;?> type="text" data-slider-value="[<?php echo $obj1[0]['min'];?>,<?php echo $obj1[0]['max'];?>]" data-slider-step="1" data-slider-max="<?php echo $max_orden_periodo_actual;?>" data-slider-min="1" data-slider-selection="after" value="" />
								<input class="bslider-minimo<?php echo $i;?> bslider-minimo form-control" <?php echo $enabled_control;?> type="text" value="<?php echo $obj1[0]['min'];?>" style="width: 47%; display: inline;">
								<input class="bslider-maximo<?php echo $i;?> bslider-maximo form-control" <?php echo $enabled_control;?> type="text" value="<?php echo $obj1[0]['max'];?>" style="width: 47%; display: inline;">
								<?php 
								if($enabled_control!=''){
									echo "<script>$('.bslider".$i."').slider('disable');</script>";
								}
								?>
							</td>
							<td>
								<div class="input-group date datetime" data-min-view="2"
									data-date-format="yyyy-mm-dd" style="width: 118px; margin-bottom: 0px;">
									<input class="form-control dtp_fecha_asignacion" size="16" <?php echo $enabled_control;?>
										type="text" value="<?php echo isset($obj1[0]['fechaasignado1'])?$obj1[0]['fechaasignado1']:date('Y-m-d');?>" readonly> <span
										class="input-group-addon btn btn-primary" <?php echo $enabled_control;?>><span
										class="glyphicon glyphicon-th"></span></span>
								</div>
							</td>
							<td>
							<?php
							echo '<select class="select2 cbo_lecturista" '.$enabled_control.'>';
							foreach ( $listar_empleados as $empleados ) {
								if($obj1[0]['lecturista1_id']==$empleados['0']["id"]){
									echo '<option value='.$empleados['0']["id"].' selected>'.$empleados['0']["nombre"].' - '.$empleados['0']["nomusuario"].'</option>';
								}else{
									echo '<option value='.$empleados['0']["id"].'>'.$empleados['0']["nombre"].' - '.$empleados['0']["nomusuario"].'</option>';
								}							
							}
							echo '</select>';
							?>
							</td>
							<td>
							<?php 
							if($obj1[0]['libro']!=''){
								echo '<span class="badge badge-success">Asignado</span>';
							}else{
								echo '<span class="badge badge-warning">Pendiente</span>';
							}
							?>
							</td>
						</tr>
						<?php 
						$i++;
						}
					}else{
						//Si no tiene asignaciones se muestra la sugerencia
						$obj_evento_cronograma = $model_ComlecOrdenlectura->existeEventoCronograma($unidad_neg,null,$idciclo,$idsector);
						if(count($obj_evento_cronograma)>0){
							$fechaasignado_cronograma = substr($obj_evento_cronograma[0]['0']['fecha'],0,10);
						}
						$i=1;
						foreach ($listarLibros_anterior as $obj1){
							$enabled_control = '';
							$estado_asignacion = 'pendiente';
							?>
						<tr class="<?php echo $estado_asignacion;?>">
							<td><?php echo $obj1[0]['count'];?></td>
							<td>		
								<input class="bslider<?php echo $i;?> bslider form-control" <?php echo $enabled_control;?> type="text" data-slider-value="[<?php echo $obj1[0]['min'];?>,<?php echo $obj1[0]['max'];?>]" data-slider-step="1" data-slider-max="<?php echo $max_orden_periodo_actual;?>" data-slider-min="1" data-slider-selection="after" value="" />
								<input class="bslider-minimo<?php echo $i;?> bslider-minimo form-control" <?php echo $enabled_control;?> type="text" value="<?php echo $obj1[0]['min'];?>" style="width: 47%; display: inline;">
								<input class="bslider-maximo<?php echo $i;?> bslider-maximo form-control" <?php echo $enabled_control;?> type="text" value="<?php echo $obj1[0]['max'];?>" style="width: 47%; display: inline;">
							</td>
							<td>
								<div class="input-group date datetime" data-min-view="2"
									data-date-format="yyyy-mm-dd" style="width: 118px; margin-bottom: 0px;">
									<input class="form-control dtp_fecha_asignacion" size="16" <?php echo $enabled_control;?>
										type="text" value="<?php echo isset($fechaasignado_cronograma)?$fechaasignado_cronograma:date('Y-m-d');?>" readonly> <span
										class="input-group-addon btn btn-primary" <?php echo $enabled_control;?>><span
										class="glyphicon glyphicon-th"></span></span>
								</div>
							</td>
							<td>
							<?php
							echo '<select class="select2 cbo_lecturista" '.$enabled_control.'>';
							foreach ( $listar_empleados as $empleados ) {
								if($obj1[0]['lecturista1_id']==$empleados['0']["id"]){
									echo '<option value='.$empleados['0']["id"].' selected>'.$empleados['0']["nombre"].' - '.$empleados['0']["nomusuario"].'</option>';
								}else{
									echo '<option value='.$empleados['0']["id"].'>'.$empleados['0']["nombre"].' - '.$empleados['0']["nomusuario"].'</option>';
								}							
							}
							echo '</select>';
							?>
							</td>
							<td>
							<span class="badge badge-warning">Pendiente</span><br>
							<span class="badge badge-info" style="margin-right: -10px;padding-bottom: 0px;"><button type="button" class="close md-close" onclick="$(this).parents('tr').remove();" style="margin-top: -4px;padding: 0px;">&times;</button> Sugerido</span>
							</td>
						</tr>
						<?php 
						$i++;
						}
					}
					if($sum_asignados_periodo_actual>0 && $sum_asignados_periodo_actual==$sum_periodo_actual){
					//Asignacion 100%
					?>
					<tr><td colspan="5">Reasignar</td></tr>
					<tr class="pendiente">
						<td><?php echo $sum_periodo_actual;?></td>
						<td>		
							<input class="bslider<?php echo $i;?> bslider form-control" type="text" data-slider-value="[1,<?php echo $sum_periodo_actual;?>]" data-slider-step="1" data-slider-max="<?php echo $max_orden_periodo_actual;?>" data-slider-min="1" data-slider-selection="after" value="" />
							<input class="bslider-minimo<?php echo $i;?> bslider-minimo form-control" type="text" value="1" style="width: 47%; display: inline;">
							<input class="bslider-maximo<?php echo $i;?> bslider-maximo form-control" type="text" value="<?php echo $max_orden_periodo_actual;?>" style="width: 47%; display: inline;">
						</td>
						<td>
							<div class="input-group date datetime" data-min-view="2"
								data-date-format="yyyy-mm-dd" style="width: 118px; margin-bottom: 0px;">
								<input class="form-control dtp_fecha_asignacion" size="16"
									type="text" value="<?php echo date('Y-m-d');?>" readonly> <span
									class="input-group-addon btn btn-primary" ><span
									class="glyphicon glyphicon-th"></span></span>
							</div>
						</td>
						<td>
						<?php
						echo '<select class="select2 cbo_lecturista">';
						foreach ( $listar_empleados as $empleados ) {
							echo '<option value='.$empleados['0']["id"].'>'.$empleados['0']["nombre"].' - '.$empleados['0']["nomusuario"].'</option>';
						}
						echo '</select>';
						?>
						</td>
						<td>
							<span class="badge badge-warning">Pendiente</span>
						</td>
					</tr>
					<?php }?>
				</tbody>
			</table>

		</div>
	</div>
</div>

<div style="text-align: right;">
	<button type="button" class="btn btn-default btn-flat md-close" data-dismiss="modal" onclick="$('#modal-asignar-nuevo').removeClass('md-show');">Cerrar</button>
    <button id="btn_asignar_lecturista_libro" type="button" class="btn btn-primary btn-flat md-close" data-dismiss="modal">Grabar</button>
</div>
<script>
$('.bslider').slider()
.on('slide', function(ev){
	var parent = $(ev.currentTarget).parents('td');
	$('.bslider-minimo', parent).val(ev.value[0]);
	$('.bslider-maximo', parent).val(ev.value[1]);
});

$('body').off('click','#btn_asignar_lecturista_libro');
$('body').on('click','#btn_asignar_lecturista_libro',function(e){

	$("#formulario_asignar_nuevo").mask("Asignando Lecturas...");
	
	$('#btn_asignar_lecturista_libro').removeClass("btn-primary");
	$('#btn_asignar_lecturista_libro').addClass("btn-success");
	$('#btn_asignar_lecturista_libro').html('Asignando Lecturas...');	

	var ciclo = '<?php echo $idciclo;?>';
	var sector = '<?php echo $idsector;?>';
	var ruta = '<?php echo $idruta;?>';
	var negocio = '<?php echo $unidad_neg;?>';
	
	$( "#table_asignar_lecturistas_nuevo_form tbody tr.pendiente" ).each(function( index ) {
		var desde = $('.bslider-minimo',$(this)).val();
		var hasta = $('.bslider-maximo',$(this)).val();
		var lecturista = $('select.cbo_lecturista',$(this)).val();
		var fecha = $('.dtp_fecha_asignacion',$(this)).val();
		console.log(desde);
		console.log(hasta);
		console.log(lecturista);
		console.log(fecha);

		$.ajax({
			url : ENV_WEBROOT_FULL_URL+'ComlecOrdenlecturas/guardarlecturistalibro',
			type: "POST",
			data : {desde: desde, hasta: hasta ,lecturista: lecturista, ciclo: ciclo, sector:sector, ruta: ruta, negocio: negocio, fecha: fecha },
			success:function(data, textStatus, jqXHR){
				
				$('#btn_asignar_lecturista_libro').removeClass("btn-success");
				$('#btn_asignar_lecturista_libro').addClass("btn-primary");
				$('#btn_asignar_lecturista_libro').html('Grabar');

				$("#formulario_asignar_nuevo").unmask();

				$("#formulario_asignar_nuevo").mask("Cargando Asignaciones...");
	        	$.ajax({
					url : ENV_WEBROOT_FULL_URL+'ComlecOrdenlecturas/form_asignar_basado_historico/'+negocio+'/'+ciclo+'/'+sector+'/'+ruta,
					type: "POST",
					success:function(data, textStatus, jqXHR){
						$('#formulario_asignar_nuevo').html(data);

						$(".select2",$('#formulario_asignar_nuevo')).select2({
					          width: '200px'
					         });
						$('.bslider').slider();
						$(".datetime").datetimepicker({autoclose: true});
						$("#formulario_asignar_nuevo").unmask();
					}
				});
			}
		});
	});
});
</script>