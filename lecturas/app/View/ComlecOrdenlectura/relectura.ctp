<?php 
if (!isset($NEGOCIO)) {
    header("Location:".ENV_WEBROOT_FULL_URL."/Usuarios/logout");
    die();
}
?>
<?php  echo $this->element('menu',array('active'=>'listarciclosector', 'open'=>'true'));?>
<div id="arbol" class="page-aside tree">
	<div class="fixed nano nscroller">
		<div class="content">
			<div class="title">
				<h2>Asignar</h2>
			</div>
			<ul class="nav nav-list treeview">
				<li class="open"><label class="tree-toggler nav-header"><i
						class="fa fa-folder-open-o"></i> Ordenes de Lectura</label>
					<ul class="nav nav-list tree">
						<li><a href="<?php echo ENV_WEBROOT_FULL_URL ?>/ComlecOrdenlecturas/listarciclosector"> Lecturas</a></li>
						<li><a href="<?php echo ENV_WEBROOT_FULL_URL ?>/ComlecOrdenlecturas/relectura"> Re Lecturas</a></li>
						<li><a href="#"> Re Lecturas Supervisor</a></li>

					</ul>
				</li>
			</ul>
		</div>
	</div>
</div>


<div class="container-fluid" id="pcont">
	<!--
	<div class="page-head">

		<ol class="breadcrumb">
			<li><button type="button"
					class="btn-xs btn-default arbol-top-lecturas">Arbol</button></li>
			<li><a href="#">Ordenes de Lectura</a></li>
			<li><a href="#">Asignar</a></li>
			<li class="active">Re Lecturas</li>
		</ol>
	</div>
	-->


	<div class="cl-mcont">
		<div class="col-sm-12 col-md-12">

			<div class="panel-group accordion accordion" id="accordion4">
				<!--div class="panel-group accordion accordion-semi" id="accordion4"-->
				<div class="panel panel-default">
					<div class="panel-heading">
						<!--div class="panel-heading success"-->
						<h4 class="panel-title">
							<a data-toggle="collapse" data-parent="#accordion4" href="#ac4-1">
								<i class="fa fa-angle-right"></i> Ordenes Ciclo/Sector
							</a>
						</h4>
					</div>
					<div id="ac4-1" class="panel-collapse collapse in">
						<div class="panel-body">

							<div class="content">
								<div class='tabla'>
									<div class='farmsmall'>
										<div class='table-responsive'>
											<div id="grid-re-lectura">
												<table class="no-border" id="re-lectura">
													<!--thead class="primary-emphasis"-->
													<thead class="primary-emphasis">
														<tr>
															<th>Unidad de Negocio</th>
															<th>Ciclo</th>
															<th>Sector</th>
															<th>% Ordenes Asignadas</th>
															<th>Asignados / Total</th>
														</tr>
													</thead>
													<tbody>
													<?php									
														foreach ($listar_orden_lecturas as $obj_orden_lectura){
													?>
														<tr style="cursor: pointer;" class="odd gradeX"
															id="orden_lectura"
															idciclo="<?php echo $obj_orden_lectura[0]['idciclo']?>"
															idsector="<?php echo $obj_orden_lectura[0]['sector']?>">
															<td><?php echo $obj_orden_lectura[0]['unidad_neg']?></td>
															<td><?php echo $obj_orden_lectura[0]['idciclo']?></td>
															<td><?php echo $obj_orden_lectura[0]['sector']?></td>
															<td><div class="progress progress-striped active">
																	<div class="progress-bar progress-bar-danger" style="width: <?php echo $obj_orden_lectura[0]['porcentaje']?>%">
																		<?php echo $obj_orden_lectura[0]['porcentaje']?>
																		%
																	</div>
																</div></td>
															<td><?php echo $obj_orden_lectura[0]['total_asignados']?>
																/ <?php echo $obj_orden_lectura[0]['total']?></td>
														</tr>
														<?php
														}
														?>
													</tbody>
												</table>
											</div>
										</div>
									</div>
								</div>
							</div>

						</div>
					</div>
				</div>
			</div>


			<div class="block">


				<?php 
				echo $this->element('ComlecOrdenlectura/modal_comlec_ordenlectura');
				?>

			</div>
		</div>


		<div id="tabla_buscar_ciclo_sector_re_lectura"></div>


	</div>
</div>
