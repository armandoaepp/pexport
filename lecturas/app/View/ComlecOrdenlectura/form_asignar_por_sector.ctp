<div class="tab-container">
	<ul class="nav nav-tabs">
	  <li class="active"><a href="#home" data-toggle="tab">Asignaci&oacute;n por Sector Completo</a></li>
	  <li><a href="#profile" data-toggle="tab">Asignaci&oacute;n Por Rutas</a></li>
	</ul>
	<div class="tab-content">
	  <div class="tab-pane active cont" id="home">
	  
		<div class="" style="max-height: 420px;overflow-y: auto;">
				
			<table id="table_asignar_lecturistas_por_sector">
					<thead>
						<tr>
							<th>Nro Sumi.</th>
							<th>Fecha</th>
							<th>Lecturista</th>
							<th>Estado</th>
						</tr>
					</thead>
					<tbody>
						<?php 
						$enabled_control = '';
						$estado_asignacion = 'pendiente';
						if($sum_periodo_actual==$sum_asignados_periodo_actual){
							$enabled_control = 'disabled';
							$estado_asignacion = '';
						}
						?>
						<tr class="<?php echo $estado_asignacion;?>">
							<td><?php echo $sum_periodo_actual;?></td>
							<td>
								<div class="input-group date datetime" data-min-view="2"
									data-date-format="yyyy-mm-dd" style="width: 118px; margin-bottom: 0px;">
									<input class="form-control dtp_fecha_asignacion" size="16" <?php echo $enabled_control;?>
										type="text" value="<?php echo date('Y-m-d');?>" readonly> <span
										class="input-group-addon btn btn-primary" <?php echo $enabled_control;?>><span
										class="glyphicon glyphicon-th"></span></span>
								</div>
							</td>
							<td>
							<?php
							echo '<select class="select2 cbo_lecturista" '.$enabled_control.'>';
							foreach ( $listar_empleados as $empleados ) {
								echo '<option value='.$empleados['0']["id"].'>'.$empleados['0']["nombre"].' - '.$empleados['0']["nomusuario"].'</option>';						
							}
							echo '</select>';
							?>
							</td>
							<td>
							<?php 
							if($sum_periodo_actual==$sum_asignados_periodo_actual){
								echo '<span class="badge badge-success">Asignado</span>';
							}else{
								echo '<span class="badge badge-warning">Pendiente</span>';
							}
							?>
							</td>
						</tr>
					</tbody>
				</table>

		</div>
		
		<div style="text-align: right;">
			<br>
			<button type="button" class="btn btn-default btn-flat md-close" data-dismiss="modal" onclick="$('#modal-asignar-por-sector').removeClass('md-show');">Cerrar</button>
		    <button id="btn_asignar_lecturista_por_sector" type="button" class="btn btn-primary btn-flat md-close" data-dismiss="modal">Grabar</button>
		</div>

	  </div>
	  <div class="tab-pane cont" id="profile">
			
		<div class="" style="max-height: 420px;overflow-y: auto;">
		
			<table id="table_asignar_lecturistas_por_rutas">
				<thead>
					<tr>
						<th>Ruta</th>
						<th>Nombre Ruta</th>
						<th>Nro Sumi.</th>
						<th>Desde - Hasta</th>
						<th>Fecha</th>
						<th>Lecturista</th>
						<th>Estado</th>
					</tr>
				</thead>
				<tbody>
					<?php 
					
					// Si tiene suministros asignados
					$i=1;
					foreach ($listarLibros as $obj1){
						$enabled_control = '';
						$estado_asignacion = 'pendiente';
						if($obj1[0]['lecturista1_id']!=''){
							$enabled_control = 'disabled';
							$estado_asignacion = '';
						}
						
						$min_orden_ruta = $model_ComlecOrdenlectura->getMinOrdenRuta(array('unidad_neg'=>$unidad_neg,'idciclo'=>$idciclo,'idsector'=>$idsector,'idruta'=>$obj1[0]['ruta']));
						$max_orden_ruta = $model_ComlecOrdenlectura->getMaxOrdenRuta(array('unidad_neg'=>$unidad_neg,'idciclo'=>$idciclo,'idsector'=>$idsector,'idruta'=>$obj1[0]['ruta']));
						?>
					<tr class="<?php echo $estado_asignacion;?>">
						<td><span class="idruta"><?php echo $obj1[0]['ruta'];?></span></td>
						<td><?php echo $obj1[0]['nombreruta'];?></td>
						<td><?php echo $obj1[0]['count'];?></td>
						<td style="text-align: center;">		
							<input class="bslider<?php echo $i;?> bslider form-control" <?php echo $enabled_control;?> type="text" data-slider-value="[<?php echo $obj1[0]['min'];?>,<?php echo $obj1[0]['max'];?>]" data-slider-step="1" data-slider-max="<?php echo $max_orden_ruta;?>" data-slider-min="<?php echo $min_orden_ruta;?>" data-slider-selection="after" value="" />
							<br>
							<input class="bslider-minimo<?php echo $i;?> bslider-minimo form-control" <?php echo $enabled_control;?> type="text" value="<?php echo $obj1[0]['min'];?>" style="width: 70px; display: inline; float: left;">
							<input class="bslider-maximo<?php echo $i;?> bslider-maximo form-control" <?php echo $enabled_control;?> type="text" value="<?php echo $obj1[0]['max'];?>" style="width: 70px; display: inline; float: right;">
							<?php 
							if($enabled_control!=''){
								echo "<script>$('.bslider".$i."').slider('disable');</script>";
							}
							?>
						</td>
						<td>
							<div class="input-group date datetime" data-min-view="2"
								data-date-format="yyyy-mm-dd" style="width: 118px; margin-bottom: 0px;">
								<input class="form-control dtp_fecha_asignacion" size="16" <?php echo $enabled_control;?>
									type="text" value="<?php echo isset($obj1[0]['fechaasignado1'])?$obj1[0]['fechaasignado1']:date('Y-m-d');?>" readonly> <span
									class="input-group-addon btn btn-primary" <?php echo $enabled_control;?>><span
									class="glyphicon glyphicon-th"></span></span>
							</div>
						</td>
						<td>
						<?php
						echo '<select class="select2 cbo_lecturista" '.$enabled_control.'>';
						foreach ( $listar_empleados as $empleados ) {
							if($obj1[0]['lecturista1_id']==$empleados['0']["id"]){
								echo '<option value='.$empleados['0']["id"].' selected>'.$empleados['0']["nombre"].' - '.$empleados['0']["nomusuario"].'</option>';
							}else{
								echo '<option value='.$empleados['0']["id"].'>'.$empleados['0']["nombre"].' - '.$empleados['0']["nomusuario"].'</option>';
							}							
						}
						echo '</select>';
						?>
						</td>
						<td>
						<?php 
						if($obj1[0]['lecturista1_id']!=''){
							echo '<span class="badge badge-success">Asignado</span>';
						}else{
							echo '<span class="badge badge-warning">Pendiente</span>';
						}
						?>
						</td>
					</tr>
					<?php 
					$i++;
					}
					?>
				</tbody>
			</table>
		
		</div>
		
		<div style="text-align: right;">
			<br>
			<button type="button" class="btn btn-default btn-flat md-close" data-dismiss="modal" onclick="$('#modal-asignar-por-sector').removeClass('md-show');">Cerrar</button>
		    <button id="btn_asignar_lecturista_por_ruta" type="button" class="btn btn-primary btn-flat md-close" data-dismiss="modal">Grabar</button>
		</div>
			
	  </div>
	</div>
</div>


<script>
$('.bslider').slider()
.on('slide', function(ev){
	var parent = $(ev.currentTarget).parents('td');
	$('.bslider-minimo', parent).val(ev.value[0]);
	$('.bslider-maximo', parent).val(ev.value[1]);
});

$('body').off('click','#btn_asignar_lecturista_por_sector');
$('body').on('click','#btn_asignar_lecturista_por_sector',function(e){

	$("#formulario_asignar_por_sector").mask("Asignando Lecturas...");
	
	$('#btn_asignar_lecturista_por_sector').removeClass("btn-primary");
	$('#btn_asignar_lecturista_por_sector').addClass("btn-success");
	$('#btn_asignar_lecturista_por_sector').html('Asignando Lecturas...');	

	var ciclo = '<?php echo $idciclo;?>';
	var sector = '<?php echo $idsector;?>';
	var negocio = '<?php echo $unidad_neg;?>';
	
	$( "#table_asignar_lecturistas_por_sector tbody tr.pendiente" ).each(function( index ) {
		var lecturista = $('select.cbo_lecturista',$(this)).val();
		var fecha = $('.dtp_fecha_asignacion',$(this)).val();
		console.log(lecturista);
		console.log(fecha);

		$.ajax({
			url : ENV_WEBROOT_FULL_URL+'ComlecOrdenlecturas/guardarlecturistalibro_por_sector',
			type: "POST",
			data : {lecturista: lecturista, ciclo: ciclo, sector:sector, negocio: negocio, fecha: fecha },
			success:function(data, textStatus, jqXHR){
				
				$('#btn_asignar_lecturista_por_sector').removeClass("btn-success");
				$('#btn_asignar_lecturista_por_sector').addClass("btn-primary");
				$('#btn_asignar_lecturista_por_sector').html('Grabar');

				$("#formulario_asignar_por_sector").unmask();

				$("#formulario_asignar_por_sector").mask("Cargando Asignaciones...");
	        	$.ajax({
					url : ENV_WEBROOT_FULL_URL+'ComlecOrdenlecturas/form_asignar_por_sector/'+negocio+'/'+ciclo+'/'+sector,
					type: "POST",
					success:function(data, textStatus, jqXHR){
						$('#formulario_asignar_por_sector').html(data);

						$('select.cbo_lecturista',$(this)).val(lecturista);
						$(".cbo_lecturista",$('#formulario_asignar_por_sector')).select2({
				        	width: '200px'
				        });
						$('.bslider').slider();
						$('.dtp_fecha_asignacion',$(this)).val(fecha);
						$(".datetime").datetimepicker({autoclose: true});
						$("#formulario_asignar_por_sector").unmask();
					}
				});
			}
		});
	});
});

$('body').off('click','#btn_asignar_lecturista_por_ruta');
$('body').on('click','#btn_asignar_lecturista_por_ruta',function(e){

	$("#formulario_asignar_por_sector").mask("Asignando Lecturas...");
	
	$('#btn_asignar_lecturista_por_ruta').removeClass("btn-primary");
	$('#btn_asignar_lecturista_por_ruta').addClass("btn-success");
	$('#btn_asignar_lecturista_por_ruta').html('Asignando Lecturas...');	

	var ciclo = '<?php echo $idciclo;?>';
	var sector = '<?php echo $idsector;?>';
	var negocio = '<?php echo $unidad_neg;?>';
	
	$( "#table_asignar_lecturistas_por_rutas tbody tr.pendiente" ).each(function( index ) {
		var ruta = $('.idruta',$(this)).html();
		var desde = $('.bslider-minimo',$(this)).val();
		var hasta = $('.bslider-maximo',$(this)).val();
		var lecturista = $('select.cbo_lecturista',$(this)).val();
		var fecha = $('.dtp_fecha_asignacion',$(this)).val();
		console.log(desde);
		console.log(hasta);
		console.log(lecturista);
		console.log(fecha);

		$.ajax({
			url : ENV_WEBROOT_FULL_URL+'ComlecOrdenlecturas/guardarlecturistalibro',
			type: "POST",
			data : {desde: desde, hasta: hasta ,lecturista: lecturista, ciclo: ciclo, sector:sector, ruta: ruta, negocio: negocio, fecha: fecha },
			success:function(data, textStatus, jqXHR){
				
				$('#btn_asignar_lecturista_por_ruta').removeClass("btn-success");
				$('#btn_asignar_lecturista_por_ruta').addClass("btn-primary");
				$('#btn_asignar_lecturista_por_ruta').html('Grabar');

				$("#formulario_asignar_por_sector").unmask();

				$("#formulario_asignar_por_sector").mask("Cargando Asignaciones...");
	        	$.ajax({
					url : ENV_WEBROOT_FULL_URL+'ComlecOrdenlecturas/form_asignar_por_sector/'+negocio+'/'+ciclo+'/'+sector,
					type: "POST",
					success:function(data, textStatus, jqXHR){
						$('#formulario_asignar_por_sector').html(data);
						$('.nav-tabs a').eq(1).click();

						$(".cbo_lecturista",$('#formulario_asignar_por_sector')).select2({
				        	width: '200px'
				        });
						$('.bslider').slider();
						$(".datetime").datetimepicker({autoclose: true});
						$("#formulario_asignar_por_sector").unmask();
					}
				});
			}
		});
	});
});
</script>