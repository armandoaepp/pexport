<?php 
if (!isset($NEGOCIO)) {
    header("Location:".ENV_WEBROOT_FULL_URL."/Usuarios/logout");
    die();
}
?>
<center>
<?php 

date_default_timezone_set("America/Lima");
$fecha_envio = date("Ym"); 

?>
<strong>Padron de Lecturas:: </strong> <strong>Empresa PEXPORT <?php CakeSession::read('NEGOCIO_empresa');?> - <?php echo $fecha_envio;?></strong>
</center>
Lecturista: <?php echo $lecturista['0']['0']['nombre'].'-'.$lecturista['0']['0']['lecturista1_id']?> / <?php if($NEGOCIO == 'CIX'){echo 'Sector/Ruta';}elseif ($NEGOCIO == 'TAC'){echo 'Libro';}else{echo 'Sector/Ruta';}?> : <?php echo $sector.'/'.$lecturista['0']['0']['ruta']; ?>
<br><a href="javascript:window.print();" title="Imprimir" class="impre">Imprimir Este Documento</a>
<table id="exportar-inconsistencias" class="table table-bordered " style="font-size: 10px; width: 744px;">
							<thead >
								<tr>  
									<th><?php if($NEGOCIO == 'CIX'){echo 'Orden';}elseif ($NEGOCIO == 'TAC'){echo 'Lcorrelati';}else{echo 'Orden';}?></th>
									<th>Cliente</th>
									<th >Direccion</th>									
									<th >Seriefab</th>
									<th ><?php if($NEGOCIO == 'CIX'){echo 'Suministro';}elseif ($NEGOCIO == 'TAC'){echo 'Contrato';}else{echo 'Suministro';}?></th>
									<th >Lectura</th>
									<th >Cod</th>
								</tr>
							</thead>
						</table>
<link  rel="stylesheet" type="text/css" href="<?php echo ENV_WEBROOT_FULL_URL ?>js/jquery.datatables/extras/TableTools/media/css/TableTools.css" media="all" rel="stylesheet" type="text/css" />
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.8.1/jquery.min.js"></script>					
<!-- Bootstrap core JavaScript
	================================================== -->
	<!-- Placed at the end of the document so the pages load faster -->
	<script src="<?php echo ENV_WEBROOT_FULL_URL ?>js/behaviour/voice-commands.js"></script>
	<script src="<?php echo ENV_WEBROOT_FULL_URL ?>js/bootstrap/dist/js/bootstrap.min.js"></script>
	<script type="text/javascript" src="<?php echo ENV_WEBROOT_FULL_URL ?>js/jquery.flot/jquery.flot.js"></script>
	<script type="text/javascript" src="<?php echo ENV_WEBROOT_FULL_URL ?>js/jquery.flot/jquery.flot.pie.js"></script>
	<script type="text/javascript" src="<?php echo ENV_WEBROOT_FULL_URL ?>js/jquery.flot/jquery.flot.resize.js"></script>
	<script type="text/javascript" src="<?php echo ENV_WEBROOT_FULL_URL ?>js/jquery.flot/jquery.flot.labels.js"></script>
	<script type="text/javascript" src="http://www.flotcharts.org/flot/jquery.flot.time.js"></script>
	
	<script type="text/javascript" src="<?php echo ENV_WEBROOT_FULL_URL ?>js/jquery.datatables/jquery.datatables.min.js"></script>
	<script type="text/javascript" src="<?php echo ENV_WEBROOT_FULL_URL ?>js/jquery.datatables/bootstrap-adapter/js/datatables.js"></script>
	<!-- <script  src="<?php /*echo ENV_WEBROOT_FULL_URL */?>js/jquery.datatables/extras/ColVis/media/js/ColVis.js" type="text/javascript"></script> -->
	<script src="<?php echo ENV_WEBROOT_FULL_URL ?>js/jquery.datatables/extras/TableTools/media/js/TableTools.js" type="text/javascript"></script>
	<script src="<?php echo ENV_WEBROOT_FULL_URL ?>js/jquery.datatables/extras/TableTools/media/js/ZeroClipboard.js" type="text/javascript"></script>
	<link rel="stylesheet" type="text/css" href="<?php echo ENV_WEBROOT_FULL_URL ?>js/fancyapps/source/jquery.fancybox.css?v=2.1.5" media="screen" />
<script>

$('#exportar-inconsistencias').dataTable({
     "bProcessing": true,
     "bDeferRender": true,                    	
     "paging": false,
     "bPaginate": false,
     "bFilter": false,
     "bSort": false,         			    
     "sAjaxSource": "<?php echo ENV_WEBROOT_FULL_URL ?>ComlecOrdenlecturas/pdf_listar_libros/<?php echo $libro?>/<?php echo $ruta?>",
     "fnDrawCallback" : fnEachTd,
 });
function fnEachTd(oSettings){
	$("#exportar-inconsistencias thead tr th").css({"width": ""});
	$("#exportar-inconsistencias tr td").css({"height": "24px"});
}
</script> 
<style>
@media all {
   div.saltopagina{
      display: none;
   }
}
   
@media print{
   div.saltopagina{ 
      display:block; 
      page-break-before:always;
   }
}

@media print {
.impre {display:none}
}
</style>
