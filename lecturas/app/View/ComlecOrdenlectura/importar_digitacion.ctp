<?php 
if (!isset($NEGOCIO)) {
    header("Location:".ENV_WEBROOT_FULL_URL."/Usuarios/logout");
    die();
}
?>
<?php 
if ($id_usuario == 25 || $id_usuario == 131){
    $permiso_lectura = 'false';
}else {
    $permiso_lectura = 'true';
    
}
echo $this->element('menu',array('active'=>'importarlecturas', 'open'=>'true', 'permiso_lectura' =>$permiso_lectura, 'id_usuario ' =>$id_usuario));
?>

    
<div class="container-fluid" id="pcont">

    <div class="cl-mcont">
        <div class="col-sm-12 col-md-12">
            <div class="block">
            <div class="content">
            <div class="header">
                <h2 class="page-title">Importar Digitaci&oacute;n de Lecturas desde Archivo Zip o Xls</h2>  
            </div>

            <div class="app-nav">
            	<br>
                <form action="<?= ENV_WEBROOT_FULL_URL?>ComlecOrdenlecturas/importar_file_digitacion" id="ComlecOrdenlecturaFormForm" enctype="multipart/form-data"  method="post" >
                    <div style="display:none;"><input type="hidden" name="_method" value="POST"></div>
                    <input id="file" type="file" name="file">
                    </br>
                    El Archivo Excel debe tener el encabezado en la primera fila (Suministro, Lectura, Observaci&oacute;n) y los datos a partir de la segunda fila.
                    <br>
                    <br>
                    <input type="submit" class="btn btn-success" value="Grabar">
                </form>
                
            </div>
            
        </div>
            </div>
        </div>
    </div>

</div>
