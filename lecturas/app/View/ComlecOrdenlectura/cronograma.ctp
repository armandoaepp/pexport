<?php 
if (!isset($NEGOCIO)) {
    header("Location:".ENV_WEBROOT_FULL_URL."/Usuarios/logout");
    die();
}
?>
<?php 
if ($id_usuario == 25 || $id_usuario == 131){
    $permiso_lectura = 'false';
}else {
    $permiso_lectura = 'true';
    
}
echo $this->element('menu',array('active'=>'importar', 'open'=>'true', 'permiso_lectura' =>$permiso_lectura, 'id_usuario ' =>$id_usuario));
?>
<script>
function tooglearbol(){
    $('#arbol').toggle();
}
</script>


    
<div class="container-fluid" id="pcont">
   <!--
    <div class="page-head" style="padding: 0px 0px;">
        <ol class="breadcrumb">
          <li><button type="button" class="btn-xs btn-default arbol-top-lecturas" >Arbol</button></li>
          <li><a href="#">Ordenes de Lectura</a></li>
          <li class="active">Cronograma</li>
        </ol>
    </div>

-->
    <div class="cl-mcont">
        <div class="col-sm-12 col-md-12"  style='height:100%;' >
        
        	<div class="tab-container">
				<ul class="nav nav-tabs">
				  <li class="active"><a href="#profile" data-toggle="tab">Calendario</a></li>
				  <li><a href="#home" data-toggle="tab">Tabla</a></li>
				</ul>
				<div class="tab-content">
				  <div class="tab-pane cont" id="home">
						<div class="block">
			                <div class="content">
			                    <div class="scroll-table-inconsistecia" style='position: relative; max-width: 300px; min-width: 100%; overflow-y: hidden; height: 500px; overflow-y: auto;'>
			                        <div id="contenido_cronograma" name="contenido_cronograma" >
			                            
			
			                        </div>
			                    </div>
			                </div>
			            </div>
				  </div>
				  <div class="tab-pane active cont" id="profile">
						<div id='div_calendar' style="min-height: 50px;"></div>
				  </p></div>
				</div>
			</div>
					
            
        </div>
    </div>
</div>

<div id="arbol" class="page-aside app filters tree" style="margin-right:0px;">
    <div class="fixed nano nscroller has-scrollbar">
        <div class="content">
            <div class="header">
                <button class="navbar-toggle" data-target=".app-nav" data-toggle="collapse" type="button">
                    <span class="fa fa-chevron-down"></span>
                </button>
                <h2 class="page-title">Cronograma</h2>  
            </div>

            <div class="app-nav collapse">
                
                <p class="description">Criterios de Busqueda</p>
                
                <div class="form-group" >
                  <label class="control-label">Por :</label>
                  <input id="cbo_criterio_cronograma" name="cbo_criterio_cronograma" style="width:240px;" placeholder="Seleccione una opcion">
                </div>
                
                <div class="por_fecha_asignacion" style="display:none;" >
                    <div class="form-group"  >
                      <label class="control-label">Por Fecha (Inicio) :</label>
                      <div class="input-group date datetime" style= "width: 80%;" data-min-view="2" data-date-format="yyyy-mm-dd">
                        <input id="dpt_date_start" class="form-control" size="16" type="text" value="<?php echo date('Y-m-d', strtotime("-1 day"));?>" readonly>
                        <span class="input-group-addon btn btn-primary"><span class="glyphicon glyphicon-th"></span></span>
                      </div>
                    </div>

                    <div class="form-group">
                      <label class="control-label">Por Fecha (Fin) :</label>
                      <div class="input-group date datetime"  style= "width: 80%;" data-min-view="2" data-date-format="yyyy-mm-dd">
                        <input id="dpt_date_end" class="form-control" size="16" type="text" value="<?php echo date('Y-m-d');?>" readonly>
                        <span class="input-group-addon btn btn-primary"><span class="glyphicon glyphicon-th"></span></span>
                      </div>    
                    </div>
                </div>
                
                <div class="por_ubicacion" style="display:none;">
                    
                    
                    <div class="form-group">
                      <label class="control-label">Por Unidad Negocio:</label>              
                        <?php 
                                echo '<select class="" name="unidadneg" id="unidadneg">
                                <option value="0">Todos</option>';
                                                 //   var_dump($listar_unidadneg);exit;
                                foreach ($listar_unidadneg as $v){
                                echo '<option value='.$v['0']['unidadneg'].'>'.$v['0']['nombreunidadnegocio'].'</option>';
                                }
                                echo '</select>';   
                        ?>                  
                    </div>
                    
                    <div class="form-group">
                      <label class="control-label">Por Ciclo:<span class="ciclo_loading" style="display:none;"><img src="<?php echo ENV_WEBROOT_FULL_URL ?>/js/jquery.select2/select2-spinner.gif"></span></label>              
                        <?php 
                                echo '<select class="" name="id_ciclo" id="id_ciclo">
                                <option value="0">Todos</option>';
                                foreach ($listar_ciclo as $v){
                                    $str_ciclo_actual = '';
                                    foreach ($arr_ciclos_actuales as $ca){
                                        if($v['0']['idciclo']==$ca['0']['idciclo']){
                                            $str_ciclo_actual = ' (Actual)';
                                        }
                                    }
                                    echo '<option value='.$v['0']['idciclo'].'>'.$v['0']['idciclo'].' - '.$v['0']['nombciclo'].$str_ciclo_actual.'</option>';
                                }
                                echo '</select>';   
                        ?>                  
                    </div>

                    <div class="form-group">
                      <label class="control-label"><?php if($NEGOCIO == 'CIX'){echo 'Por Sector';}elseif ($NEGOCIO == 'TAC'){echo 'Por Zona';}else{echo 'Por Sector';}?><span class="sector_loading" style="display:none;"><img src="<?php echo ENV_WEBROOT_FULL_URL ?>/js/jquery.select2/select2-spinner.gif"></span></label>              
                        <?php 
                                echo '<select class="" name="monitoreo_id_sector" id="monitoreo_id_sector">
                                <option value="0">Todos</option>';
                                foreach ($listar_sector as $v){
                                echo '<option value='.$v['0']['sector'].'>'.$v['0']['sector'].'</option>';
                                }
                                echo '</select>';   
                        ?>                  
                    </div>
                </div>

                <button class='btn btn-primary' id='buscar-listado-cronograma'>Buscar</button>
            </div>
            
            <div class="app-nav collapse">
            <h2 class="page-title">Importar Cronograma</h2>  
            <form action="<?= ENV_WEBROOT_FULL_URL?>ComlecOrdenlecturas/save_cronograma" id="ComlecOrdenlecturaFormForm" enctype="multipart/form-data"  method="post" onsubmit="return validar(this)">
                    <div style="display:none;"><input type="hidden" name="_method" value="POST"></div><br/>
                    <input id="file" type="file" name="file">
                    </br>
                    <input type="submit" class="btn btn-success" value="Grabar">
                    <p>
                    descargar formato <a href="<?= ENV_WEBROOT_FULL_URL?>files/formatos/formato_cronograma_sistema de lectura.xls">aqu&iacute;</a>
                    </p>
                </form>
            </div>
        </div>
    </div>
</div>

<div id="dialog-evento-calendario" title="Programar Lectura" style="display: none;">
	<span class="dialog_loading" style="display: none;"><img
		src="<?php echo ENV_WEBROOT_FULL_URL ?>/js/jquery.select2/select2-spinner.gif">
		Cargando...</span>
	<div id='dialog-contenido'>
		
		<div class="form-group" style= "width: 80%;" >
              <label class="control-label">Unidad Negocio:</label>              
              	<?php 
	        			  	echo '<select class="" name="form_cronograma_unidadneg" id="form_cronograma_unidadneg">';
	        				foreach ($listar_unidadneg as $v){
								echo '<option value='.$v['0']['unidadneg'].'>'.$v['0']['unidadneg'].' - '.$v['0']['nombreunidadnegocio'].'</option>';
	        				}
	        				echo '</select>';	
	        		?>        			
            </div>
		            
		<div class="form-group" style= "width: 80%;" >
			<label class="control-label">Ciclo:<span class="form_cronograma_ciclo_loading"
				style="display: none;"><img
					src="<?php echo ENV_WEBROOT_FULL_URL ?>/js/jquery.select2/select2-spinner.gif"></span></label>              
              	<?php 
        			  	echo '<select class="" name="form_cronograma_id_ciclo" id="form_cronograma_id_ciclo">';
        				foreach ($listar_ciclo as $v){
							$str_ciclo_actual = '';
							foreach ($arr_ciclos_actuales as $ca){
								if($v['0']['idciclo']==$ca['0']['idciclo']){
									$str_ciclo_actual = ' (Actual)';
								}
							}
							echo '<option value='.$v['0']['idciclo'].'>'.$v['0']['idciclo'].' - '.$v['0']['nombciclo'].$str_ciclo_actual.'</option>';	
        				}
        				echo '</select>';	
        		?>        			
        </div>

		<div class="form-group" style= "width: 80%;" >
			<label class="control-label"><?php if($NEGOCIO == 'CIX'){echo 'Sector';}elseif ($NEGOCIO == 'TAC'){echo 'Zona';}else{echo 'Sector';}?>: <span
				class="form_cronograma_sector_loading" style="display: none;"><img
					src="<?php echo ENV_WEBROOT_FULL_URL ?>/js/jquery.select2/select2-spinner.gif"></span></label>              
              	<?php 
        			  	echo '<select class="" name="form_cronograma_id_sector" id="form_cronograma_id_sector">';
        				foreach ($listar_sector as $v){
							echo '<option value='.$v['0']['sector'].'>'.$v['0']['sector'].'</option>';
        				}
        				echo '</select>';	
        		?>        			
         </div>
         
         <div class="form-group" style= "width: 80%;" >
			<label class="control-label">Fecha:
			</label>
			<div class="input-group date datetime" style= "width: 100%;" data-min-view="2" data-date-format="yyyy-mm-dd">
            	<input id="form_cronograma_fecha" class="form-control" size="16" type="text" value="<?php echo date('Y-m-d');?>" readonly>
                <span class="input-group-addon btn btn-primary"><span class="glyphicon glyphicon-th"></span></span>
            </div>
		</div>
         
         <button class='btn btn-primary' id='guardar-evento-cronograma'>Guardar</button>
         <button class='btn' id='cancelar-evento-cronograma' onclick="javascript: $('#dialog-evento-calendario').dialog('close');">Cancelar</button>
	</div>
</div>

<script>
require(['ajax/ordenlecturas'],function(Ordenlecturas){
	Ordenlecturas.listarCronograma();
});
function validar(f){
    if((f.file.value)==""){
        alert("Seleccione archivo");
        return false;
    }
    return true;
}
</script>