<div id='calendar'></div>

<style>
.fc-event-inner {
	margin-bottom: -41px;
}
</style>

<?php 
$str_fechas = "";
foreach ($listarXml as $obj1){
	if($arr_kpi_cronograma[$obj1[0]['id']][0]['count_suministros']>0){
		$porcentaje = number_format(($arr_kpi_cronograma[$obj1[0]['id']][0]['count_finalizados']*100)/$arr_kpi_cronograma[$obj1[0]['id']][0]['count_suministros'],2);
		$html_porcentaje = "<div style=\"text-align: center;margin-bottom: 9px;\"><div class=\"progress progress-striped\"><div class=\"progress-bar progress-bar-success\" style=\"width: ".$porcentaje."%; margin-botton: 0px;\">".$porcentaje."%</div></div>";
		if($porcentaje>=100){
			$str_disable_buttons = 'disabled';
		}else{
			$str_disable_buttons = '';
		}
		$html_buttons = '<div class="btn-group" style="margin-top: -30px;"><button type="button" '.$str_disable_buttons.' class="btn btn-default btn-xs btn_reprogramar" title="Reprogramar" data-unidadneg="'.$obj1[0]['glomas_unidadnegocio_id'].'" data-idciclo="'.$obj1[0]['idciclo'].'" data-idsector="'.$obj1[0]['idsector'].'" data-start="'.substr($obj1[0]['fecha'],0,10).'"><i class="fa fa-edit"></i></button><a class="btn btn-default btn-xs fancybox fancybox.iframe" title="Ver detalles" href="'.ENV_WEBROOT_FULL_URL.'ComlecOrdenlecturas/ajax_resumen_cronograma/'.$obj1[0]['id'].'"><i class="fa fa-bar-chart-o"></i></a></div></div>';
	}else{
		$html_porcentaje = "<div style=\"margin-bottom: 20px;text-align: center;\"><span class=\"label label-warning\"><i class=\"fa fa-warning\"></i> No Suministros</span>";
		$html_buttons = '<div class="btn-group" style="margin-top: 4px;"><button type="button" class="btn btn-default btn-xs btn_reprogramar" title="Reprogramar" data-unidadneg="'.$obj1[0]['glomas_unidadnegocio_id'].'" data-idciclo="'.$obj1[0]['idciclo'].'" data-idsector="'.$obj1[0]['idsector'].'" data-start="'.substr($obj1[0]['fecha'],0,10).'"><i class="fa fa-edit"></i></button><a class="btn btn-default btn-xs fancybox fancybox.iframe" title="Ver detalles" href="'.ENV_WEBROOT_FULL_URL.'ComlecOrdenlecturas/ajax_resumen_cronograma/'.$obj1[0]['id'].'"><i class="fa fa-bar-chart-o"></i></a></div></div>';
	}
	
	$str_fechas .= "{
		            title: 'Ciclo:".$obj1[0]['idciclo']." Sector:".$obj1[0]['idsector']." ".$html_porcentaje.$html_buttons."',
		            start: new Date(".substr($obj1[0]['fecha'],0,4).", ".(substr($obj1[0]['fecha'],5,2)-1).", ".substr($obj1[0]['fecha'],8,2)."),
		            editable: false,
		            //url: '".ENV_WEBROOT_FULL_URL."ComlecOrdenlecturas/ajax_resumen_cronograma/".$obj1[0]['id']."',
		            //className: 'fancybox fancybox.iframe',
		            backgroundColor: '#2494F2' 
		          },";
}

foreach ($arr_no_programados as $obj1){
	
	$html_porcentaje = "<div style=\"margin-bottom: 20px;text-align: center;\"><span class=\"label label-danger\"><i class=\"fa fa-warning\"></i> No Programado</span>";
	$html_buttons = '<div class="btn-group" style="margin-top: 4px;"><button type="button" class="btn btn-default btn-xs btn_reprogramar" title="Reprogramar" data-unidadneg="'.$obj1[0]['glomas_unidadnegocio_id'].'" data-idciclo="'.$obj1[0]['idciclo'].'" data-idsector="'.$obj1[0]['idsector'].'" data-start="'.substr($obj1[0]['fecha'],0,10).'"><i class="fa fa-edit"></i></button><a class="btn btn-default btn-xs" title="Ver detalles" href="#" disabled><i class="fa fa-bar-chart-o"></i></a></div></div>';

	$str_fechas .= "{
		            title: 'Ciclo:".$obj1[0]['idciclo']." Sector:".$obj1[0]['idsector']." ".$html_porcentaje.$html_buttons."',
		            start: new Date(".substr($obj1[0]['fecha'],0,4).", ".(substr($obj1[0]['fecha'],5,2)-1).", ".substr($obj1[0]['fecha'],8,2)."),
		            editable: false,
		            backgroundColor: '#5DC4EA'
		          },";
}
$str_fechas = substr($str_fechas,0,-1);
?>

<link rel='stylesheet' type='text/css' href='<?php echo ENV_WEBROOT_FULL_URL?>js/jquery.fullcalendar/fullcalendar/fullcalendar.css' />
<link rel='stylesheet' type='text/css' href='<?php echo ENV_WEBROOT_FULL_URL?>js/jquery.fullcalendar/fullcalendar/fullcalendar.print.css'  media='print' />
<script type='text/javascript' src='<?php echo ENV_WEBROOT_FULL_URL?>js/jquery.fullcalendar/fullcalendar/fullcalendar.js'></script>
  <script type="text/javascript">
    $(document).ready(function() {

        // page is now ready, initialize the calendar...
        
        /* initialize the external events
      -----------------------------------------------------------------*/
    
      var date = new Date();
      var d = date.getDate();
      var m = date.getMonth();
      var y = date.getFullYear();

      $('#calendar').fullCalendar({
        header: {
          left: 'title',
          center: '',
          right: 'month,agendaWeek,agendaDay, today, prev,next',
        },
        lang: 'es',
        editable: false,
        events: [
          <?php echo $str_fechas;?>
        ],
        droppable: false,
        selectable: true,
		selectHelper: true,
		select: function(start, end) {
		
			$('.dialog_loading').show();
			$("#dialog-evento-calendario").dialog({ minWidth: 300, title: "Programar Lectura" });
			var mes = start.getMonth()+1;
			if(mes<10){
				mes = "0"+mes;
			}
			var dia = start.getDate();
			if(dia<10){
				dia = "0"+dia;
			}
			$('#form_cronograma_fecha').val(start.getFullYear()+'-'+ mes +'-'+ dia);
			$('.dialog_loading').hide();

			var eventData;
			
       		$('#guardar-evento-cronograma').off("click");
       	 	$('#guardar-evento-cronograma').on("click",function (e) {

       	 		$('#dialog-contenido').mask('Guardando evento de lectura.');
       	 		fecha = $("#form_cronograma_fecha").val();
	       	 	idunidadneg = $("#form_cronograma_unidadneg").val();
	       		
	       		if(idunidadneg == '' || idunidadneg == '0'){
	       			alert('Selecciona una unidad de negocio');
	       			return;
	       		}

				idciclo = $("#form_cronograma_id_ciclo").val();
	       		
	       		if(idciclo == '' || idciclo == '0'){
	       			alert('Selecciona un Ciclo');
	       			return;
	       		}
	
	       		sectorid = $("#form_cronograma_id_sector").val();
	       		
	       		if(sectorid == '' || sectorid == '0'){
	       			alert('Selecciona un Sector');
	       			return;
	       		}
	
	       		var title = 'Ciclo: ' + idciclo + ' Sector: ' + sectorid + ' <div class=\"progress progress-striped\"><div class=\"progress-bar progress-bar-success\" style=\"width: 0%; margin-botton: 0px;\">0%</div></div>';

	       		$.ajax({
					url : ENV_WEBROOT_FULL_URL+'ComlecOrdenlecturas/save_programar_evento_cronograma/'+idunidadneg+'/'+fecha+'/'+idciclo+'/'+sectorid,
					dataType: 'json',
					success:function(data, textStatus, jqXHR){

						if(data.success){
							if (title) {
								eventData = {
									title: title,
									start: start,
									end: end,
						            backgroundColor: '#2494F2'
								};
								$('#calendar').fullCalendar('renderEvent', eventData, true); // stick? = true
							}
							$('#calendar').fullCalendar('unselect');
							$('#dialog-contenido').unmask();
							$('#dialog-evento-calendario').dialog('close');
						}else{
							if(confirm(data.message+' ¿Desea actualizarlo?')){
								$.ajax({
									url : ENV_WEBROOT_FULL_URL+'ComlecOrdenlecturas/save_actualizar_evento_cronograma/'+data.id_cronograma+'/'+idunidadneg+'/'+fecha+'/'+idciclo+'/'+sectorid,
									dataType: 'json',
									success:function(data, textStatus, jqXHR){

										if(data.success){
											if (title) {
												eventData = {
													title: title,
													start: start,
													end: end,
										            backgroundColor: '#2494F2'
												};
												$('#calendar').fullCalendar('renderEvent', eventData, true); // stick? = true
											}
											$('#calendar').fullCalendar('unselect');
											$('#dialog-contenido').unmask();
											$('#dialog-evento-calendario').dialog('close');
											$('#buscar-listado-cronograma').click();
										}else{
											alert(data.message);
											$('#dialog-contenido').unmask();
										}
									}
					       		});
							}else{
								$('#dialog-contenido').unmask();
							}
						}
					}
	       		});

       	 	});
		},
		eventAfterAllRender: function( view ) {
			$('.btn_reprogramar').on('click',function(){
			  	$('.dialog_loading').show();
				$("#dialog-evento-calendario").dialog({ minWidth: 300, title: "Programar Lectura" });
				start = $(this).data('start');

				$('#form_cronograma_fecha').val(start);
				//$('#form_cronograma_fecha').datetimepicker( "setDate", new  Date(start.substring(0,4), (start.substring(5,7)-1), start.substring(8,10), 0, 0, 0, 0));

				$('#form_cronograma_unidadneg').val([$(this).data('unidadneg')]).select2({
		        	width: '100%'
			       	});
				$('#form_cronograma_id_ciclo').val([$(this).data('idciclo')]).select2({
		        	width: '100%'
			       	});
				$('#form_cronograma_id_sector').val([$(this).data('idsector')]).select2({
		        	width: '100%'
			       	});
				
				$('.dialog_loading').hide();

				var eventData;
				
				$('#guardar-evento-cronograma').off("click");
			 	$('#guardar-evento-cronograma').on("click",function (e) {

			 		$('#dialog-contenido').mask('Guardando evento de lectura.');
			 		fecha = $("#form_cronograma_fecha").val();
		       	 	idunidadneg = $("#form_cronograma_unidadneg").val();
		       		
		       		if(idunidadneg == '' || idunidadneg == '0'){
		       			alert('Selecciona una unidad de negocio');
		       			return;
		       		}

					idciclo = $("#form_cronograma_id_ciclo").val();
		       		
		       		if(idciclo == '' || idciclo == '0'){
		       			alert('Selecciona un Ciclo');
		       			return;
		       		}

		       		sectorid = $("#form_cronograma_id_sector").val();
		       		
		       		if(sectorid == '' || sectorid == '0'){
		       			alert('Selecciona un Sector');
		       			return;
		       		}

		       		var title = 'Ciclo: ' + idciclo + ' Sector: ' + sectorid + ' <div class=\"progress progress-striped\"><div class=\"progress-bar progress-bar-success\" style=\"width: 0%; margin-botton: 0px;\">0%</div></div>';

		       		$.ajax({
						url : ENV_WEBROOT_FULL_URL+'ComlecOrdenlecturas/save_programar_evento_cronograma/'+idunidadneg+'/'+fecha+'/'+idciclo+'/'+sectorid,
						dataType: 'json',
						success:function(data, textStatus, jqXHR){

							if(data.success){
								if (title) {
									eventData = {
										title: title,
										start: start,
							            backgroundColor: '#2494F2'
									};
									$('#calendar').fullCalendar('renderEvent', eventData, true); // stick? = true
								}
								$('#calendar').fullCalendar('unselect');
								$('#dialog-contenido').unmask();
								$('#dialog-evento-calendario').dialog('close');
								$('#buscar-listado-cronograma').click();
							}else{
								if(confirm(data.message+' ¿Desea actualizarlo?')){
									$.ajax({
										url : ENV_WEBROOT_FULL_URL+'ComlecOrdenlecturas/save_actualizar_evento_cronograma/'+data.id_cronograma+'/'+idunidadneg+'/'+fecha+'/'+idciclo+'/'+sectorid,
										dataType: 'json',
										success:function(data, textStatus, jqXHR){

											if(data.success){
												if (title) {
													eventData = {
														title: title,
														start: start,
											            backgroundColor: '#2494F2'
													};
													$('#calendar').fullCalendar('renderEvent', eventData, true); // stick? = true
												}
												$('#calendar').fullCalendar('unselect');
												$('#dialog-contenido').unmask();
												$('#dialog-evento-calendario').dialog('close');
												$('#buscar-listado-cronograma').click();
											}else{
												alert(data.message);
												$('#dialog-contenido').unmask();
											}
										}
						       		});
								}else{
									$('#dialog-contenido').unmask();
								}
							}
						}
		       		});

			 	});
		}); 
		}
    
        
      });

    });
  </script>