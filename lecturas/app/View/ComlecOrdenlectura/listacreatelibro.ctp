<?php 
if (!isset($NEGOCIO)) {
    header("Location:".ENV_WEBROOT_FULL_URL."/Usuarios/logout");
    die();
}
?>
<table class="no-border" id="createlibro">
	<thead class="primary-emphasis">
	<tr>
	<th>Item</th>
	<th>Orden R</th>
	<th><?php if($NEGOCIO == 'CIX'){echo 'Suministro';}elseif ($NEGOCIO == 'TAC'){echo 'Contrato';}else{echo 'Suministro';}?></th>
	<th>Cliente</th>
	<th>Direccion</th>
	<th><?php if($NEGOCIO == 'CIX'){echo 'Libro';}elseif ($NEGOCIO == 'TAC'){echo 'Folio';}else{echo 'Libro';}?></th>
	<th>Lecturista</th>
	<th>Estado</th>
	</tr>
	</thead>
	<tbody>
	<?php
	$i=1;
	foreach ($listar_create_libro as $obj_orden_lectura){
		//var_dump($obj_orden_lectura[0]['idciclo']);exit;
		$ciclo = $obj_orden_lectura[0]['idciclo'];
		$sector = $obj_orden_lectura[0]['sector'];
		$ruta = $obj_orden_lectura[0]['ruta'];
		$negocio = $obj_orden_lectura[0]['glomas_unidadnegocio_id'];
	?>
		<tr style="cursor: pointer;" class="odd gradeX" id="orden_lectura" idciclo="<?php echo $obj_orden_lectura[0]['idciclo']?>" idsector="<?php echo $obj_orden_lectura[0]['sector']?>" >									
		<td><?php echo $i++ ?> </td>
		<td><?php echo $obj_orden_lectura[0]['orden']?></td>
		<td><?php echo $obj_orden_lectura[0]['suministro']?></td>
		<td><?php echo stripslashes(htmlspecialchars($obj_orden_lectura[0]['cliente'], ENT_QUOTES))?></td>
		<td><?php echo stripslashes(htmlspecialchars($obj_orden_lectura[0]['direccion'], ENT_QUOTES)) ?></td>
		<td><?php echo $obj_orden_lectura[0]['libro']?></td>
		<td><?php echo stripslashes(htmlspecialchars($obj_orden_lectura[0]['nombre'], ENT_QUOTES)) ?></td>
		<td>P/A</td>
		</tr>
	<?php
	}
	?>
	</tbody>
</table>
<input type="hidden" name="ciclo" id="ciclo" value = "<?php echo $ciclo ?>" />
<input type="hidden" name="sector" id="sector" value = "<?php echo $sector ?>" />
<input type="hidden" name="rutas" id="ruta" value = "<?php echo $ruta ?>"/>
<input type="hidden" name="negocio" id="negocio" value = "<?php echo $negocio ?>"/>

<div class="modal-footer">
  <strong>Item desde: </strong><input type="text" name="desde" id="desde" /> <strong> Item hasta: </strong><input type="text" name="hasta" id="hasta" /> <strong> Lecturista Asignado: </strong>
  <?php 
  //print_r($listar_empleados);
  	echo '<select name="lecturista" id="lecturista">';
	foreach ($listar_empleados as $empleados){
	echo '<option value='.$empleados['0']["id"].'>'.$empleados['0']["nombre"].' - '.$empleados['0']["nomusuario"].'</option>';
	}
	echo '</select>';	
	?>
<button type="button" id="asignar_lecturista_libro" name="asignar_lecturista_libro" class="btn btn-primary btn-flat" data-dismiss="modal">Grabar</button>
<!-- <button type="button" class="btn btn-default btn-flat md-close" data-dismiss="modal">Cancel</button> -->
	
 </div>
