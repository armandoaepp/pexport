<?php 
if (!isset($NEGOCIO)) {
    header("Location:".ENV_WEBROOT_FULL_URL."/Usuarios/logout");
    die();
}
?>
<?php 
if ($id_usuario == 25){
	$permiso_lectura = 'true';
}else {
	$permiso_lectura = 'true';
	
}
echo $this->element('menu',array('active'=>'extranet', 'open'=>'true', 'permiso_lectura' =>$permiso_lectura, 'id_usuario ' =>$id_usuario));
?>

		


<div class="container-fluid" id="pcont">
	<!--
	<div class="page-head" style="padding:0px;">
		<ol class="breadcrumb">
			<li><button type="button"
					class="btn-xs btn-default arbol-top-lecturas">Arbol</button></li>
			<li><a href="#">Ordenes de Lectura</a></li>
			<li><a href="#">Evaluar Inconsistencias</a></li>
			<li class="active">Lecturas</li>
		</ol>
	</div>
-->
	<!-- <div class="cl-mcont">-->
	<div class="" style="margin-top:20px;">
		<div class="col-md-12 col-md-12" >
			<div class="block-flat">
				
				<div class="content" id="contenido_digitar2"></div>
					<div class="scroll-table-inconsistecia" style='position: relative; max-width: 300px; min-width: 100%; overflow-y: hidden; height: 500px; overflow-y: auto;'>
						<table id="tabla-inconsistencias-extranet"
							class="table table-bordered " 							
							style="background-color: #fff; color: #000; font-weight: bold; font-size: 5px; ">
							<thead>
								<tr>
									<th >ID</th>
									<th >Serie</th>
									<th >Cliente</th>
									<th ><?php if($NEGOCIO == 'CIX'){echo 'Suministro';}elseif ($NEGOCIO == 'TAC'){echo 'Contrato';}else{echo 'Suministro';}?></th>
									<th >L.A.P</th>
									<th >L.A.E</th>
									<th >L.A</th>
									<th >L.R</th>
									<th >L.RR</th>
									<th >O.A.P</th>
									<th >O.A</th>									
									<th >O.R</th>
									<th >C.A.E</th>
									<th >Promedio</th>
									<th >C.A</th>
									<th >Result</th>
									<th >Validacion </th>
								</tr>
							</thead>
						</table>
					</div>
			</div>
		</div>
		<div id='detalle_lectura_suministro' class="col-sm-6 col-md-6"></div>

	</div>
</div>


<div id="arbol" class="page-aside app filters tree" >
    <div class="fixed nano nscroller has-scrollbar">
        <div class="content">
          
	        <div class="header">
		        <button class="navbar-toggle" data-target=".app-nav" data-toggle="collapse" type="button">
		            <span class="fa fa-chevron-down"></span>
		        </button>          
		        <h2 class="page-title">Filtros</h2>   
	        </div>


	        <div class="app-nav collapse">
				
	            <div class="form-group" style= "width:80%;" >
	              <label class="control-label"><?php if($NEGOCIO == 'CIX'){echo 'Por Sector';}elseif ($NEGOCIO == 'TAC'){echo 'Por Zona';}else{echo 'Por Sector';}?></label>
	              
	              	<?php 

	        			  	echo '<select class="select2" name="id_sector" id="id_sector">
	        			  	<option value="0">Todos</option>';
	        				foreach ($listar_sector as $sector){
	        				echo '<option value='.$sector['0']['sector'].'>'.$sector['0']['sector'].'</option>';
	        				}
	        				echo '</select>';	
	        		?>
	        			
	            </div>

	            <div class="form-group" style= "width: 80%;" >
	              <label class="control-label">Por Lecturista:</label>
	             <?php 
	        			  	echo '<select class="select2" name="lecturista" id="id_lecturista">
	        			  	<option value="0">Todos</option>';
	        				foreach ($listar_empleados as $empleados){
	        				echo '<option value='.$empleados['0']["id"].'>'.$empleados['0']["nombre"] .' - '.$empleados['0']["nomusuario"].'</option>';
	        				}
	        				echo '</select>';	
	        			?>
	            </div>

	            <div class="form-group" style= "width: 80%;" >
					<label class="control-label"><?php if($NEGOCIO == 'CIX'){echo 'Por Suministro';}elseif ($NEGOCIO == 'TAC'){echo 'Por Contrato';}else{echo 'Por Suministro';}?></label>
					<input type="text" id="txt_search_by_suministro" name="txt_search_by_suministro" placeholder="<?php if($NEGOCIO == 'CIX'){echo 'Por Suministro';}elseif ($NEGOCIO == 'TAC'){echo 'Por Contrato';}else{echo 'Por Suministro';}?>">
	            </div>
				<!--
	            <div class="form-group">
	              <label class="control-label">Opciones Avanzadas</label>
	             <select class="select2" name="inconsistenciaforzada" id="inconsistenciaforzada">
	        		<option value="true">Forzadas</option>
	        		<option value="false">No forzadas</option>
	        	</select>
	        	<p></p>
	        	<select class="select2" name="inconsistenciacodigo" id="inconsistenciacodigo">
	        		<option value="true">Con Codigo</option>
	        		<option value="false">Sin Codigo</option>
	        	</select>
	        	<p></p>
	        	Desde: <input type="text" id="inconsistenciadesde" > <p></p> Hasta: <input type="text" id="inconsistenciahasta" >
	            </div>          
				-->	
					

	            <div class="form-group" >
	              <button class='btn btn-success'
							id='intranet-inconsistencia-lectura'>Buscar</button>
	            </div>
	         
	        </div>

        </div>        

    </div>
</div>

<script type="text/javascript">														
	require(['ajax/inconsistencia'],function(){});
</script>

<script language="JavaScript">
if (screen.width<1024)
   console.log("Pequeña")
else
   
   console.log("Grande")
</script> 