<?php 
if (!isset($NEGOCIO)) {
    header("Location:".ENV_WEBROOT_FULL_URL."/Usuarios/logout");
    die();
}
?>
<?php 
date_default_timezone_set("America/Lima");
$fecha_envio = date("Ym");
?>
<center>
<h3><strong>Padron de Lecturas</strong></h3>
<h3><strong>Distriluz - <?php echo CakeSession::read('NEGOCIO_empresa');?> - <?php echo $fecha_envio;?></strong></h3>
</center>
<a href="javascript:window.print();" title="Imprimir" class="impre">Imprimir Este Documento</a>
<table id="exportar-inconsistencias"
							class="table table-bordered " style="
    font-size: 10px;
">
							<thead >
								<tr>    
									<th><?php if($NEGOCIO == 'CIX'){echo 'Ruta';}elseif ($NEGOCIO == 'TAC'){echo 'Libro';}else{echo 'Ruta';}?></th>
									<th><?php if($NEGOCIO == 'CIX'){echo 'Orden';}elseif ($NEGOCIO == 'TAC'){echo 'Lcorrelati';}else{echo 'Orden';}?></th>
									<th>cliente</th>
									<th >direccion</th>
									<th ><?php if($NEGOCIO == 'CIX'){echo 'Suministro';}elseif ($NEGOCIO == 'TAC'){echo 'Contrato';}else{echo 'Suministro';}?></th>
									<th >seriefab</th>
									<th >Lectura</th>
									<th >Cod</th>

								</tr>
							</thead>
						</table>
<link  rel="stylesheet" type="text/css" href="<?php echo ENV_WEBROOT_FULL_URL ?>js/jquery.datatables/extras/TableTools/media/css/TableTools.css" media="all" rel="stylesheet" type="text/css" />
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.8.1/jquery.min.js"></script>					
<!-- Bootstrap core JavaScript
	================================================== -->
	<!-- Placed at the end of the document so the pages load faster -->
	<script src="<?php echo ENV_WEBROOT_FULL_URL ?>js/behaviour/voice-commands.js"></script>
	<script src="<?php echo ENV_WEBROOT_FULL_URL ?>js/bootstrap/dist/js/bootstrap.min.js"></script>
	<script type="text/javascript" src="<?php echo ENV_WEBROOT_FULL_URL ?>js/jquery.flot/jquery.flot.js"></script>
	<script type="text/javascript" src="<?php echo ENV_WEBROOT_FULL_URL ?>js/jquery.flot/jquery.flot.pie.js"></script>
	<script type="text/javascript" src="<?php echo ENV_WEBROOT_FULL_URL ?>js/jquery.flot/jquery.flot.resize.js"></script>
	<script type="text/javascript" src="<?php echo ENV_WEBROOT_FULL_URL ?>js/jquery.flot/jquery.flot.labels.js"></script>
	<script type="text/javascript" src="http://www.flotcharts.org/flot/jquery.flot.time.js"></script>
	
	<script type="text/javascript" src="<?php echo ENV_WEBROOT_FULL_URL ?>js/jquery.datatables/jquery.datatables.min.js"></script>
	<script type="text/javascript" src="<?php echo ENV_WEBROOT_FULL_URL ?>js/jquery.datatables/bootstrap-adapter/js/datatables.js"></script>
	<!-- <script  src="<?php /*echo ENV_WEBROOT_FULL_URL */?>js/jquery.datatables/extras/ColVis/media/js/ColVis.js" type="text/javascript"></script> -->
	<script src="<?php echo ENV_WEBROOT_FULL_URL ?>js/jquery.datatables/extras/TableTools/media/js/TableTools.js" type="text/javascript"></script>
	<script src="<?php echo ENV_WEBROOT_FULL_URL ?>js/jquery.datatables/extras/TableTools/media/js/ZeroClipboard.js" type="text/javascript"></script>
	<link rel="stylesheet" type="text/css" href="<?php echo ENV_WEBROOT_FULL_URL ?>js/fancyapps/source/jquery.fancybox.css?v=2.1.5" media="screen" />
<script>

$('#exportar-inconsistencias').dataTable({
     "bProcessing": true,
     "bDeferRender": true,                    	
     "paging": false,
     "bPaginate": false,
     "bFilter": false,
     "bSort": false,         			    
     "sAjaxSource": "<?php echo ENV_WEBROOT_FULL_URL ?>EvaluarInconsistencias/pdf_inconsistenciaintro/<?php echo $sector?>/<?php echo $lecturista?>",
 });	
</script> 
<style>
@media all {
   div.saltopagina{
      display: none;
   }
}
   
@media print{
   div.saltopagina{ 
      display:block; 
      page-break-before:always;
   }
}

@media print {
.impre {display:none}
}
</style>

