<?php 
if (!isset($NEGOCIO)) {
   header("Location:".ENV_WEBROOT_FULL_URL."/Usuarios/logout");
    die();
}
?>
<table class="table table-bordered" id="datatable-reporte-historico" >
	<thead class="primary-emphasis">
		<tr>
			<th>Validaci&oacute;n</th>
			<th>Lecturao</th>
			<th>ReLecturao</th>
			<th>Resultado Evaluaci&oacute;n</th>
			<th>obs1</th>
			<th>obs2</th>
			<th>Tipo Lectura</th>
			<th>Fecha Actualizaci&oacute;n</th>
			<th>T&eacute;cnico</th>
		</tr>
	</thead>
	<tbody>
<?php 
	foreach ($arr_suministros as $suministro){
?>
	<tr>
		<td><?php echo $suministro['0']["validacion"];?></td>
		<td><?php echo $suministro['0']["lecturao1"];?></td>
		<td><?php echo $suministro['0']["lecturao2"];?></td>
		<td><?php echo $suministro['0']["resultadoevaluacion"];?></td>
		<td><?php echo $suministro['0']["obs1"];?></td>
		<td><?php echo $suministro['0']["obs2"];?></td>
		<td><?php echo $suministro['0']["tipolectura"];?></td>
		<td><?php echo substr($suministro['0']["fecha_actualizacion"],0,19);?></td>
		<td><?php echo $suministro['0']["nombre"];?></td>
	<tr>
<?php } ?>										
	</tbody>
</table>