<?php 
if (!isset($NEGOCIO)) {
   header("Location:".ENV_WEBROOT_FULL_URL."/Usuarios/logout");
    die();
}
?>
<table class="table table-bordered" id="datatable-reporte-historico" >
	<thead class="primary-emphasis">
		<tr>
			<th>Pfactura</th>
			<th>Lectura Anterior Ensa</th>
			<th>Lecturao</th>
			<th>Consumo Anterior</th>
			<th>Promedio</th>
			<th>Consumo Actual</th>
			<th>C&oacute;digo Actual</th>
			<th>Fecha Lectura</th>
			<th>Tipo Lectura</th>
			<th>Rlecturao</th>
			<th>T&eacute;cnico</th>
			<th>Foto</th>
		</tr>
	</thead>
	<tbody>
<?php 
	foreach ($arr_suministros as $suministro){
?>
	<tr>
		<td><?php echo $suministro['0']["pfactura"];?></td>
		<td><?php echo (int) $suministro['0']["lecant"];?></td>
		<td><?php echo (int) $suministro['0']["lecturao1"];?></td>
		<td><?php echo (int) $suministro['0']["consant"];?></td>
		<td><?php echo (int) $suministro['0']["promedio"];?></td>
		<td><?php echo (int) $suministro['0']["montoconsumo1"];?></td>
		<td><a href="javascript:void(0);" class="btn-tooltip" data-toggle="tooltip" data-placement="top" title="<?php echo $suministro['0']["descripcion"];?>"><?php echo $suministro['0']["codigofac"];?></a></td>
		<td><?php echo substr($suministro['0']["fechaejecucion1"],0,19);?></td>
		<td><?php echo $suministro['0']["tipolectura"];?></td>
		<td><?php if($suministro['0']["lecturao2"]==''){}else{ echo (int) $suministro['0']["lecturao2"]; }?></td>
		<td><?php echo $suministro['0']["nombre"];?></td>
		<?php 
			if($suministro[0]['foto1']!='' or $suministro[0]['foto2']!='' or $suministro[0]['foto3']!=''){
				$btn_foto = '<a class="btn btn-info btn-sm btn fancybox-inconsistencia fancybox.iframe" href="'.ENV_WEBROOT_FULL_URL.'/EvaluarInconsistencias/ajax_imagen_por_inconsistencia/'.$suministro[0]['suministro'].'/'.$suministro[0]['pfactura'].'"><i class="fa fa-camera"></i></a>';
			}else{
				$btn_foto = '<a class="btn btn-sm" disabled><i class="fa fa-camera"></i></a>';
			}
		?>
		<td><?php echo $btn_foto;?></td>
	</tr>
<?php } ?>										
	</tbody>
</table>