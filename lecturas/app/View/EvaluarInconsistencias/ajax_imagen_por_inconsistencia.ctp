<?php 
if (!isset($NEGOCIO)) {
    header("Location:".ENV_WEBROOT_FULL_URL."/Usuarios/logout");
    die();
}
?>
<script type="text/javascript" src="<?php echo ENV_WEBROOT_FULL_URL ?>js/jquery.js"></script>
<script type="text/javascript" src="<?php echo ENV_WEBROOT_FULL_URL ?>js/jquery.rotate/jQueryRotateCompressed.js"></script>
<div style="font-family: 'Open Sans', sans-serif;font-size: 13px;">
<?php 
if(count($arr_obj_incosistencia)>0){
	foreach ($arr_obj_incosistencia as $obj_incosistencia){
		?>
		Suministro: <?php echo $obj_incosistencia[0]['suministro'];?> Medidor: <?php echo $obj_incosistencia[0]['seriefab'];?>
		<?php
		$boolean_foto1 = false;
		$boolean_foto2 = false;
		$boolean_foto3 = false;
		if(trim($obj_incosistencia[0]['foto1'])!=''){
			$boolean_foto1 = true;
			?>
			 Lectura: <?php echo $obj_incosistencia[0]['lecturao1'];?><br>
			<label>Foto de Lectura:</label>
			<img id="foto_lectura_1" style="width: 90%;" src="data:image/png;base64,<?php echo $this->Image->applyWatermark(trim($obj_incosistencia[0]['foto1']),$obj_incosistencia[0]['fechaejecucion1'])?>" />
		<?php 
		}else{
			$msg1 = 'No Hay Foto de Lectura';
		}
		if(trim($obj_incosistencia[0]['foto2'])!=''){
			$boolean_foto2 = true;
			?>
			 Lectura: <?php echo $obj_incosistencia[0]['lecturao2'];?><br>
			<label>Foto de Re - Lectura:</label>
			<img id="foto_lectura_2" style="width: 90%;" src="data:image/png;base64,<?php echo $this->Image->applyWatermark(trim($obj_incosistencia[0]['foto2']),$obj_incosistencia[0]['fechaejecucion2'])?>" />
		<?php 
		}else{
			$msg2 = 'No Hay Foto de Re - Lectura';
		}
		if(trim($obj_incosistencia[0]['foto3'])!=''){
			$boolean_foto3 = true;
			?>
			 Lectura: <?php echo $obj_incosistencia[0]['lecturao3'];?><br>
			<label>Foto de Re - Lectura (nivel 2):</label>
			<img id="foto_lectura_3" style="width: 90%;" src="data:image/png;base64,<?php echo $this->Image->applyWatermark(trim($obj_incosistencia[0]['foto3']),$obj_incosistencia[0]['fechaejecucion3'])?>" />
		<?php 
		}else{
			$msg3 = 'No Hay Foto de Re - Lectura (nivel 2)';
		}
		if(!$boolean_foto1 && !$boolean_foto2 && !$boolean_foto3){
			echo '<label>'.$msg1.'</label>';
		}elseif(!$boolean_foto1 && !$boolean_foto2 && $boolean_foto3){
			echo '<label>'.$msg1.'</label><br>';
			echo '<label>'.$msg2.'</label>';
		}elseif(!$boolean_foto1 && $boolean_foto2 && !$boolean_foto3){
			echo '<label>'.$msg1.'</label>';
		}
	}
}else{?>
<label>No Hay Foto</label>
<?php }?>
</div>
<script>
var angulo_foto1 = 90;
$("#foto_lectura_1").click(function(){
	$("#foto_lectura_1").rotate(angulo_foto1);
	angulo_foto1 += 90;
	if(angulo_foto1==360){angulo_foto1 = 0;}
});
var angulo_foto2 = 90;
$("#foto_lectura_2").click(function(){
	$("#foto_lectura_2").rotate(angulo_foto2);
	angulo_foto2 += 90;
	if(angulo_foto2==360){angulo_foto2 = 0;}
});
var angulo_foto3 = 90;
$("#foto_lectura_3").click(function(){
	$("#foto_lectura_3").rotate(angulo_foto3);
	angulo_foto3 += 90;
	if(angulo_foto3==360){angulo_foto3 = 0;}
});
</script>