<style>
.table-formula th {
padding: 2px 2px;
}
.table-formula td {
padding: 2px 2px;
}
</style>
<div class="col-sm-12 col-md-12 col-lg-12">
<h2>
<?php 
if(isset($suministro)){
?>
Suministro: <strong><?php echo $suministro;?></strong>
Lectura: <strong><?php echo $lectura;?></strong>
Consumo: <strong><?php echo $ca;?></strong>
<?php if($resultadoevaluacion=='CONSISTENTE'){?>
Resultado: <span class="badge badge-success"><?php echo $resultadoevaluacion;?></span>
<?php }else{?>
Resultado: <span class="badge badge-danger"><?php echo $resultadoevaluacion;?></span>
<?php }?>
<?php if($forzado){?>
Forzado: <span class="badge badge-info">SI</span>
<?php }else{?>
Forzado: NO
<?php }?>
<?php }?>
</h2>
</div>
<?php
function calculate_string( $mathString ){
	$mathString = trim($mathString);     // trim white spaces
	$mathString = ereg_replace ('[^0-9\+-\*\/\(\) ]', '', $mathString);    // remove any non-numbers chars; exception for math operators

	$compute = create_function("", "return (" . $mathString . ");" );
	return 0 + $compute();
}

$lista="
<div class='col-sm-6 col-md-6 col-lg-6'>
<div class='title'>
<h3>Metodo 1 - Consistencia por el Promedio de los 6 meses</h3>
<p>Promedio = $pro</p>
</div>
<table class='table-formula' style='width:100%'>
<thead class='primary-emphasis'>
<tr>
<th>Rango</th>
<th>Min</th>
<th>Max</th>
<th>Factor Min</th>
<th>Factor Max</th>
</tr>
</thead>";

foreach ($getRangos as $rangos){
	$var= $rangos['0']['id'];
	$var1= $rangos['0']['r1'];
	$var2= $rangos['0']['r2'];
	$var3= $rangos['0']['max'];
	$var4= $rangos['0']['min'];

	if ($pro >=$var1 && $pro <=$var2) {
		$color="background-color:yellow;";
		$varrango=$var;
	} else {
		$color='';
	}

	$lista.="
	<tr style='$color'>
	<td>$var</td>
	<td>$var1</td>
	<td>$var2</td>
	<td>$var3</td>
	<td>$var4</td>
	</tr>";
}

$lista.="</table><br>
		<table class='table-formula' style='width:100%'>
		<thead class='primary-emphasis'>
		<tr>
		<th>Metodo</th>
		<th>Rango</th>
		<th>Promedio</th>
		<th>Formula Min</th>
		<th>Formula Max</th>
		</tr>
		</thead>";

$obj_metodos = $ObjMetodos->getMetodo(1,$varrango);

foreach ($obj_metodos as $metodos){
	$m= $metodos['0']['comlec_metodo_id'];
	$r= $metodos['0']['comlec_rango_id'];
	$min= $metodos['0']['formula_min'];
	$max= $metodos['0']['formula_max'];

	$lista.="<tr>
	<td>$m</td>
	<td>$r</td>
	<td>$pro</td>
	<td>$min</td>
	<td>$max</td>
	</tr>";
}

$mini=str_replace("promedio","$pro","$min");
$mani=str_replace("promedio","$pro","$max");
$calculomin= calculate_string($mini);
$calculomax= calculate_string($mani);
if ($ca >=$calculomin && $ca <=$calculomax) {
	$str_label_result = '<span class="label label-success">CONSISTENTE</span>';
}else{
	$str_label_result = '<span class="label label-danger">INCONSISTENTE</span>';
}

$lista.="<tr>
<td>$m</td>
<td>$r</td>
<td>$pro</td>
<td>$mini</td>
<td>$mani</td>
</tr>
<tr>
<td>$m</td>
<td>$r</td>
<td>$pro</td>
<td>$calculomin</td>
<td>$calculomax</td>
</tr>
<tr style='text-align: center;'>
<td colspan='3'>Resultado</td>
<td colspan='2'>$str_label_result</td>
</tr>
</table>
</div>";

$lista.="
<div class='col-sm-6 col-md-6 col-lg-6'>
<div class='title'>
<h3>Metodo 2 - Consistencia por el Consumo Mes Anterior</h3>
<p>Consumo Mes Anterior = $cac</p>
</div>
<table class='table-formula' style='width:100%'>
<thead class='primary-emphasis'>
<tr>
<th>Rango</th>
<th>Min</th>
<th>Max</th>
<th>Factor Min</th>
<th>Factor Max</th>
</tr>
</thead>";
foreach ($getRangos as $rangos){
	$var= $rangos['0']['id'];
	$var1= $rangos['0']['r1'];
	$var2= $rangos['0']['r2'];
	$var3= $rangos['0']['max'];
	$var4= $rangos['0']['min'];

	if ($cac >=$var1 && $cac <=$var2) {
		$color="background-color:yellow;";
		$varrango=$var;
	} else {
		$color='';
	}

	$lista.="
	<tr style='$color'>
	<td>$var</td>
	<td>$var1</td>
	<td>$var2</td>
	<td>$var3</td>
	<td>$var4</td>
	</tr>";
}
$lista.="</table><br>
		<table class='table-formula' style='width:100%'>
		<thead class='primary-emphasis'>
		<tr>
		<th>Metodo</th>
		<th>Rango</th>
		<th>Consumo Anterior</th>
		<th>Formula Min</th>
		<th>Formula Max</th>
		</tr>
		</thead>
		";

$obj_metodos2 = $ObjMetodos->getMetodo(2,$varrango);
foreach ($obj_metodos2 as $metodos2){
	$m=  $metodos2['0']['comlec_metodo_id'];
	$r=  $metodos2['0']['comlec_rango_id'];
	$min=  $metodos2['0']['formula_min'];
	$max=  $metodos2['0']['formula_max'];
	$str_min=str_replace("cast(consant as numeric)","consant","$min");
	$str_max=str_replace("cast(consant as numeric)","consant","$max");

	$lista.="<tr>
	<td>$m</td>
	<td>$r</td>
	<td>$cac</td>
	<td>$str_min</td>
	<td>$str_max</td>
	</tr>";
}
$mini=str_replace("cast(consant as numeric)","$cac","$min");
$mani=str_replace("cast(consant as numeric)","$cac","$max");
$calculomin= calculate_string($mini);
$calculomax= calculate_string($mani);
if ($ca >=$calculomin && $ca <=$calculomax) {
	$str_label_result = '<span class="label label-success">CONSISTENTE</span>';
}else{
	$str_label_result = '<span class="label label-danger">INCONSISTENTE</span>';
}

$lista.="
<tr>
<td>$m</td>
<td>$r</td>
<td>$cac</td>
<td>$mini</td>
<td>$mani</td>
</tr>
<tr>
<td>$m</td>
<td>$r</td>
<td>$cac</td>
<td>$calculomin</td>
<td>$calculomax</td>
</tr>
<tr style='text-align: center;'>
<td colspan='3'>Resultado</td>
<td colspan='2'>$str_label_result</td>
</tr>
</table></div>";
echo $lista;
