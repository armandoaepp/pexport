<?php 
if (!isset($NEGOCIO)) {
	header("Location:".ENV_WEBROOT_FULL_URL."/Usuarios/logout");
	die();
}
?>

<script>
function tooglearbol(){
	$('#arbol').toggle();
}
</script>

<div id="arbol" class="page-aside app filters" style="zoom: 1;">
	<div class="content">
		<h2 class="page-title">Importar Excel o Zip Excel de Inconsistencias</h2>
	</div>
	<div class="app-nav collapse">
		<div class="content">
			<form action="<?= ENV_WEBROOT_FULL_URL?>EvaluarInconsistencias/show_excel_inconsistencia"
				id="excel_inconsistencia" enctype="multipart/form-data"
				method="post">
				<div style="display: none;">
					<input type="hidden" name="_method" value="POST">
				</div>
				<input id="file" type="file" name="file"> </br> <input type="submit"
					class="btn btn-success" value="Comparar Inconsistencia">
			</form>
		</div>
	</div>
</div>

<div class="container-fluid" id="pcont">
	<!--
	<div class="page-head">
		<ol class="breadcrumb">
			<li><button type="button"
					class="btn-xs btn-default arbol-top-lecturas">Arbol</button></li>
			<li><a href="#">Validar Inconsistencias</a></li>
			<li class="active">Listado de XLS</li>
		</ol>
	</div>
	-->

	<div class="cl-mcont">
		<div class="col-sm-12 col-md-12">

			<div class="block">

				<div id="contenido_xls" name="contenido_xls">contenido_xls</div>
			</div>

		</div>
	</div>
</div>
