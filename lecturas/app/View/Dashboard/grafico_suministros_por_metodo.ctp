<?php
$rs=count($arr);

$serie='';
if ($rs > 0) {
	foreach ($arr as $k => $obj){
		$serie .= "['".(isset($obj[0]['metodofinal1'])?'Método '.$obj[0]['metodofinal1']:'Con Obs')."',".$obj[0]['total']."],";
	}
	$serie = substr($serie,0,-1);
}else{
	echo 'No hay datos';
	exit();
}
?>
<!DOCTYPE html>
<html lang="es" >
<head>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta http-equiv="content-type" content="text/html; charset=UTF-8">
<title>Grafico Pexport</title>

<script type="text/javascript" src="//code.jquery.com/jquery-1.9.1.js"></script>



<script type="text/javascript">//<![CDATA[ 

$(function () {
    $('#container').highcharts({
        chart: {
            plotBackgroundColor: null,
            plotShadow: false
        },
        title: {
            text: 'Suministros por Método de Evaluación'
        },
        tooltip: {
            pointFormat: '{series.name}: <b>{point.y}</b> ({point.percentage:.1f}%)'
        },
        plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                dataLabels: {
                    enabled: true,
                    format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                    style: {
                        color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                    }
                }
            }
        },
        credits: {
            enabled: false
        },
        series: [{
            type: 'pie',
            name: 'Cantidad de Suministros',
            data: [
                <?php echo $serie;?>
            ]
        }]
    });
});
//]]>  

</script>


</head>
<body>
	<!-- 
	<script src="http://code.highcharts.com/highcharts.js"></script>
	<script src="http://code.highcharts.com/modules/exporting.js"></script>
	 -->
	
	<script src="http://code.highcharts.com/highcharts.js"></script>
	<script src="http://code.highcharts.com/modules/exporting.js"></script>

	<div id="container" style="width: 100%; height: 100%; margin: 0 auto">
	</div>
	
</body>
</html>