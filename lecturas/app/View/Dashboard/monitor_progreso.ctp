<?php 
if(isset($unidad_neg) && isset($idciclo)){
	$url_completa = '/'.$unidad_neg.'/'.$idciclo;
}else{
	$url_completa = '';
}
?>
<!DOCTYPE html>
<html lang="es">
<head>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta http-equiv="content-type" content="text/html; charset=UTF-8">
<title>Grafico Pexport</title>

<script type="text/javascript" src="//code.jquery.com/jquery-1.9.1.js"></script>



<script type="text/javascript">//<![CDATA[

$(function () {
	$(function () {

	    var gaugeOptions = {

	        chart: {
	            type: 'solidgauge'
	        },

	        title: null,

	        pane: {
	            center: ['50%', '85%'],
	            size: '140%',
	            startAngle: -90,
	            endAngle: 90,
	            background: {
	                backgroundColor: (Highcharts.theme && Highcharts.theme.background2) || '#EEE',
	                innerRadius: '60%',
	                outerRadius: '100%',
	                shape: 'arc'
	            }
	        },

	        tooltip: {
	            enabled: false
	        },

	        // the value axis
	        yAxis: {
	            stops: [
	                [0.1, '#55BF3B'], // green
	                [0.5, '#DDDF0D'], // yellow
	                [0.9, '#DF5353'] // red
	            ],
	            lineWidth: 0,
	            minorTickInterval: null,
	            tickPixelInterval: 400,
	            tickWidth: 0,
	            title: {
	                y: -70
	            },
	            labels: {
	                y: 16
	            }
	        },

	        plotOptions: {
	            solidgauge: {
	                dataLabels: {
	                    y: 5,
	                    borderWidth: 0,
	                    useHTML: true
	                }
	            }
	        }
	    };

	 // The speed gauge
	    $('#container-1').highcharts(Highcharts.merge(gaugeOptions, {
	        yAxis: {
	            min: 0,
	            max: <?php echo $cantidad_ordenes_dia_actual;?>,
	            title: {
	                text: 'Progreso'
	            }
	        },

	        credits: {
	            enabled: false
	        },

	        series: [{
	            name: 'Progreso',
	            data: [<?php echo $cantidad_avanzados_dia_actual;?>],
	            dataLabels: {
	                format: '<div style="text-align:center"><span style="font-size:25px;color:' +
	                    ((Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black') + '">{y}</span><br/>' +
	                       '<span style="font-size:12px;color:silver">Suministros/Día</span></div>'
	            },
	            tooltip: {
	                valueSuffix: ' Suministros/Día'
	            }
	        }]

	    }));

	    // The RPM gauge
	    $('#container-2').highcharts(Highcharts.merge(gaugeOptions, {
	        yAxis: {
	            min: 0,
	            max: <?php echo $cantidad_avanzados_dia_actual;?>,
	            title: {
	                text: 'Inconsistentes'
	            }
	        },

	        credits: {
	            enabled: false
	        },

	        series: [{
	            name: 'Inconsistentes',
	            data: [<?php echo $cantidad_inconsistentes_dia_actual;?>],
	            dataLabels: {
	                format: '<div style="text-align:center"><span style="font-size:25px;color:' +
	                    ((Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black') + '">{y}</span><br/>' +
	                       '<span style="font-size:12px;color:silver">Suministros/Día</span></div>'
	            },
	            tooltip: {
	                valueSuffix: ' Suministros/Día'
	            }
	        }]

	    }));

	    // The speed gauge
	    $('#container-3').highcharts(Highcharts.merge(gaugeOptions, {
	        yAxis: {
	            min: 0,
	            max: <?php echo $cantidad_ordenes;?>,
	            title: {
	                text: 'Progreso'
	            }
	        },

	        credits: {
	            enabled: false
	        },

	        series: [{
	            name: 'Progreso',
	            data: [<?php echo $cantidad_avanzados;?>],
	            dataLabels: {
	                format: '<div style="text-align:center"><span style="font-size:25px;color:' +
	                    ((Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black') + '">{y}</span><br/>' +
	                       '<span style="font-size:12px;color:silver">Suministros/Periodo</span></div>'
	            },
	            tooltip: {
	                valueSuffix: ' Suministros/Periodo'
	            }
	        }]

	    }));

	    // The RPM gauge
	    $('#container-4').highcharts(Highcharts.merge(gaugeOptions, {
	        yAxis: {
	            min: 0,
	            max: <?php echo $cantidad_avanzados;?>,
	            title: {
	                text: 'Inconsistentes'
	            }
	        },

	        credits: {
	            enabled: false
	        },

	        series: [{
	            name: 'Inconsistentes',
	            data: [<?php echo $cantidad_inconsistentes;?>],
	            dataLabels: {
	                format: '<div style="text-align:center"><span style="font-size:25px;color:' +
	                    ((Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black') + '">{y}</span><br/>' +
	                       '<span style="font-size:12px;color:silver">Suministros/Periodo</span></div>'
	            },
	            tooltip: {
	                valueSuffix: ' Suministros/Periodo'
	            }
	        }]

	    }));

	    // Bring life to the dials
	    setInterval(function () {



	    	$.ajax({
          	  url: "monitor_progreso/true<?php echo $url_completa;?>",
          	  dataType: 'json',
          	  success: function(data) {

          		// The speed gauge
            	    $('#container-1').highcharts(Highcharts.merge(gaugeOptions, {
            	        yAxis: {
            	            min: 0,
            	            max: parseInt(data.Total_dia_actual),
            	            title: {
            	                text: 'Progreso'
            	            }
            	        },

            	        credits: {
            	            enabled: false
            	        },

            	        series: [{
            	            name: 'Progreso',
            	            data: [parseInt(data.Progreso_dia_actual)],
            	            dataLabels: {
            	                format: '<div style="text-align:center"><span style="font-size:25px;color:' +
            	                    ((Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black') + '">{y}</span><br/>' +
            	                       '<span style="font-size:12px;color:silver">Suministros/Día</span></div>'
            	            },
            	            tooltip: {
            	                valueSuffix: ' Suministros/Día'
            	            }
            	        }]

            	    }));

            	    // The RPM gauge
            	    $('#container-2').highcharts(Highcharts.merge(gaugeOptions, {
            	        yAxis: {
            	            min: 0,
            	            max: parseInt(data.Progreso_dia_actual),
            	            title: {
            	                text: 'Inconsistentes'
            	            }
            	        },

            	        credits: {
            	            enabled: false
            	        },

            	        series: [{
            	            name: 'Inconsistentes',
            	            data: [parseInt(data.Inconsistentes_dia_actual)],
            	            dataLabels: {
            	                format: '<div style="text-align:center"><span style="font-size:25px;color:' +
            	                    ((Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black') + '">{y}</span><br/>' +
            	                       '<span style="font-size:12px;color:silver">Suministros/Día</span></div>'
            	            },
            	            tooltip: {
            	                valueSuffix: ' Suministros/Día'
            	            }
            	        }]

            	    }));

          		// The speed gauge
          	    $('#container-3').highcharts(Highcharts.merge(gaugeOptions, {
          	        yAxis: {
          	            min: 0,
          	            max: parseInt(data.Total),
          	            title: {
          	                text: 'Progreso'
          	            }
          	        },

          	        credits: {
          	            enabled: false
          	        },

          	        series: [{
          	            name: 'Progreso',
          	            data: [parseInt(data.Progreso)],
          	            dataLabels: {
          	                format: '<div style="text-align:center"><span style="font-size:25px;color:' +
          	                    ((Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black') + '">{y}</span><br/>' +
          	                       '<span style="font-size:12px;color:silver">Suministros/Periodo</span></div>'
          	            },
          	            tooltip: {
          	                valueSuffix: ' Suministros/Periodo'
          	            }
          	        }]

          	    }));

          	    // The RPM gauge
          	    $('#container-4').highcharts(Highcharts.merge(gaugeOptions, {
          	        yAxis: {
          	            min: 0,
          	            max: parseInt(data.Progreso),
          	            title: {
          	                text: 'Inconsistentes'
          	            }
          	        },

          	        credits: {
          	            enabled: false
          	        },

          	        series: [{
          	            name: 'Inconsistentes',
          	            data: [parseInt(data.Inconsistentes)],
          	            dataLabels: {
          	                format: '<div style="text-align:center"><span style="font-size:25px;color:' +
          	                    ((Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black') + '">{y}</span><br/>' +
          	                       '<span style="font-size:12px;color:silver">Suministros/Periodo</span></div>'
          	            },
          	            tooltip: {
          	                valueSuffix: ' Suministros/Periodo'
          	            }
          	        }]

          	    }));

          	  }
	    	});
	    }, 60000);


	});
});
//]]>

</script>

<style>
.outer-div {
	position: relative;
	float: right;
	right: 50%;
}

.inner-div {
	position: relative;
	float: right;
	right: -50%;
}

.floating-div {
	float: left;
	border: 1px solid red;
	margin: 0 1.5em;
}

.clearfix:before, .clearfix:after {
	content: " ";
	display: table;
}

.clearfix:after {
	clear: both;
}

.clearfix {
	*zoom: 1;
}
</style>


</head>
<body style="text-align: center;">
	<!--
	<script src="http://code.highcharts.com/highcharts.js"></script>
	<script src="http://code.highcharts.com/modules/exporting.js"></script>
	 -->

	<script src="http://code.highcharts.com/highcharts.js"></script>
	<script src="http://code.highcharts.com/modules/exporting.js"></script>
	<script src="http://code.highcharts.com/highcharts-more.js"></script>
	<script src="http://code.highcharts.com/modules/solid-gauge.src.js"></script>

	<div class="clearfix">
		<div class="outer-div">
			<div class="inner-div">

				<div id="container-1"
					style="width: 250px; height: 180px; float: left;"></div>
				<div id="container-2"
					style="width: 250px; height: 180px; float: left;"></div>

				<div id="container-3"
					style="width: 250px; height: 180px; float: left;"></div>
				<div id="container-4"
					style="width: 250px; height: 180px; float: left;"></div>
			</div>
		</div>

	</div>

</body>
</html>