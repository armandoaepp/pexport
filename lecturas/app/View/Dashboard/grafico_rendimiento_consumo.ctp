<?php
if(count($arr_ordenes)>0){
	$unidad_negocio_id_tmp = '';
	$idciclo_tmp = '';
	$sector_tmp = '';
	
	$str_unidadneg = '';
	$str_ciclo = '';
	$str_sector = '';
	$str_data = '';
	
	$consumo_ciclo = 0;
	
	 foreach ($arr_ordenes as $k => $obj){
	 	$unidad_negocio_id = $obj[0]['nombreunidadnegocio'];
	 	$idciclo = $obj[0]['nombciclo'];
	 	$sector = $obj[0]['sector'];
	 	$consumo = ($obj[0]['suma_consumo'])/1000000;//millon
	 	
	 	if($unidad_negocio_id_tmp!=$unidad_negocio_id){
	 		$str_unidadneg .= '"'.$unidad_negocio_id.'",';
	 		$unidad_negocio_id_tmp = $unidad_negocio_id;
	 	}
	 	
	 	if($idciclo_tmp!=$idciclo){
	 		$str_ciclo .= '"'.$idciclo.'",';
	 		$str_data = str_replace('%consumo_ciclo%',$consumo_ciclo,$str_data);
	 		$str_data .= '["'.$unidad_negocio_id.'",%consumo_ciclo%,"'.$idciclo.'"],';
	 		$consumo_ciclo = 0;
	 		$idciclo_tmp = $idciclo;
	 	}
	 	
	 	if($sector_tmp!=$sector){
	 		$str_sector .= '"'.$sector.'",';
	 		$str_data .= '["'.$idciclo.'",'.$consumo.',"'.$sector.'"],';
	 		$sector_tmp = $sector;
	 	}
	 	$consumo_ciclo += $consumo;
	 }
	 $str_data = str_replace('%consumo_ciclo%',$consumo_ciclo,$str_data);
	 $str_unidadneg = substr($str_unidadneg,0,-1);
	 $str_ciclo = substr($str_ciclo,0,-1);
	 $str_sector = substr($str_sector,0,-1);
	 $str_data = substr($str_data,0,-1);
}else{
	echo 'No hay datos';
	exit();
}
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html lang='en' xml:lang='en' xmlns='http://www.w3.org/1999/xhtml'>
  <head>
    <meta content='text/html; charset=utf-8' http-equiv='Content-Type' />  
    <title>Pexport SAC - Distribuci&oacute;n de Consumo</title>
    <script src="<?php echo ENV_WEBROOT_FULL_URL ?>js/Sankey/ext/raphael.js" type="text/javascript"></script>
    <script src="<?php echo ENV_WEBROOT_FULL_URL ?>js/Sankey/ext/jquery.js" type="text/javascript"></script>
    <script src="<?php echo ENV_WEBROOT_FULL_URL ?>js/Sankey/js/sankey.js" type="text/javascript"></script>    
				<link rel="stylesheet" href="<?php echo ENV_WEBROOT_FULL_URL ?>js/Sankey/examples/css/style.css" />
  </head>
  <body>
    <script type='text/javascript'>
      $(document).ready(function() {
        var sankey = new Sankey();
      /*
        sankey.stack(0,["Top","Bottom"]);
        sankey.stack(1,["Merge"]);
        sankey.stack(2,["Good","Bad"]);
      
        sankey.setData([["Top",100,"Merge"],["Bottom",50,"Merge"],["Merge",70,"Good"],["Merge",80,"Bad"]]);
        
        sankey.setBubbles([["Merge",200],["Bad",-100]])
*/        

		sankey.stack(0,[<?php echo $str_unidadneg;?>]);
		sankey.stack(1,[<?php echo $str_ciclo;?>]);
		sankey.stack(2,[<?php echo $str_sector;?>]);

		sankey.setData([<?php echo $str_data;?>]);

        sankey.convert_bubble_values_callback = function(size) {
          return size * 1.5; // Pixels per unit
        };
        
        sankey.convert_bubble_labels_callback = function(size) {
          return "c. "+size;
        }
        
        sankey.bubbleColor = "#0F0"
        sankey.bubbleLabelColor = "#fff"
        sankey.negativeBubbleColor = "#F00"
        sankey.negativeBubbleLabelColor = "#000"
        sankey.draw();
    });
      
    </script>
    <h1 style='width:100%; text-align: center; margin-bottom: 0'>Distribuci&oacute;n de Consumo</h1>
    <div style='width:100%; text-align: center; margin-top: 0'>Expresada en millones de Kwh</div>
    <div id='sankey' style="width:100%;height:140%">
      &nbsp;
    </div>
  </body>
</html>
