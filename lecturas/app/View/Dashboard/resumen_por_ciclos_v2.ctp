<?php 
if(isset($this->request->query['ajax']) && $this->request->query['ajax']=='false'){
	if (isset($id_usuario) && $id_usuario == 25){
		$permiso_lectura = 'true';
	}else {
		$permiso_lectura = 'true';
	}
	echo $this->element('menu',array('active'=>'rep_detallado_ciclo', 'open_report'=>'true', 'permiso_lectura' =>$permiso_lectura, 'id_usuario ' =>isset($id_usuario)?$id_usuario:null));
	?>
	<h2 class="text-center" style="margin-top: 5px;">Resumen Detallado por Ciclo</h2>
	<?php 
}
?>
<table class='red tableWithFloatingHeader'>
	<thead>
		<tr>
			<th>Ciclo</th>
			<th><span></span>Sector</th>
			<th class='right'><span></span>Total Suministros</th>
			<th class='right' title="Asignados"><span></span>Asig.</th>
			<th class='right' title="Descargados"><span></span>Desc.</th>
			<th class='right' title="Finalizados"><span></span>Final.</th>
			<th class='right' title="Pendientes"><span></span>Pend.</th>
			<th class='right' title="Consistentes"><span></span>Cons.</th>
			<th class='right' title="Inconsistentes"><span></span>Inc.</th>
			<th class='right'><span></span>Enviados a Distriluz</th>
			<th class='right' title="Observaciones con lectura"><span></span>Obs. con lectura</th>
			<th class='right' title="Observaciones sin lectura"><span></span>Obs. sin lectura</th>
			<th class='right' title="Consumo cero"><span></span>Cons. cero</th>
			<th class='right' title="Consumo menor que 35"><span></span>Cons. > 35</th>
			<th class='right' title="Consumo mayor que 1000"><span></span>Cons. > 1000</th>
			<th class='right' title="Consumo mayor 100% que el anterior"><span></span>Cons. > 100% que el anterior</th>
		</tr>
	</thead>
	<tbody class='no-border-x'>
		<?php
		$sum_suministros = 0;
		$sum_asignados = 0;
		$sum_descargados = 0;
		$sum_finalizados = 0;
		$sum_pendientes = 0;
		$sum_consistentes = 0;
		$sum_inconsistentes_actuales = 0;
		$sum_inconsistentes_evaluadas = 0;
		$sum_observaciones_con_lectura = 0;
		$sum_observaciones_sin_lectura = 0;
		$sum_consumo_cero = 0;
		$sum_consumo_menor_35 = 0;
		$sum_consumo_mayor_1000 = 0;
		$sum_consumo_mayor_100_porciento = 0;
		$sum_enviados_distriluz = 0;
		foreach ($arr_ciclos as $k => $obj){
			$str_style = '';
			foreach ($arr_ciclos_actuales as $k => $obj1){
				if($obj[0]['idciclo']==$obj1[0]['idciclo']){
					$str_style = 'background-color: yellow;';		
				}
			}
			?>
			<tr style="<?php echo $str_style;?>">
			<td><?php echo $obj[0]['idciclo'];?></td>
			<td><?php echo $obj[0]['sectores'];?></td>
			<td class='right'><?php echo number_format($obj[0]['count_suministros']);?></td>
			<td class='right'><?php echo number_format($obj[0]['count_asignados']);?></td>
			<td class='right'><?php echo number_format($obj[0]['count_descargados']);?></td>
			<td class='right'><?php echo number_format($obj[0]['count_finalizados']);?></td>
			<td class='right'>
			<?php 
			if(($obj[0]['count_suministros']-$obj[0]['count_finalizados'])==0){
			?>
			<span class="badge badge-success">0</span>
			<?php }else{?>
			<span class="badge badge-danger"><?php echo number_format($obj[0]['count_suministros']-$obj[0]['count_finalizados']);?></span>
			<?php }?>
			</td>
			<td>
			<?php echo number_format($obj[0]['count_consistentes']);?>
			</td>
			<?php 
			$style_badge_inconsistente = '';
			if($obj[0]['count_inconsistentes_actuales']>0){
				$style_badge_inconsistente = 'badge badge-danger';
			}
			?>
			<td class='right'>
			<a href="<?php echo ENV_WEBROOT_FULL_URL;?>EvaluarInconsistencias/evaluar_inconsistencia/U/<?php echo date('Y-m-d');?>/<?php echo date('Y-m-d');?>/0/<?php echo $obj[0]['idciclo'];?>/0/0/0/0/0/" target="_blank">
			<span class="<?php echo $style_badge_inconsistente;?>"><?php echo number_format($obj[0]['count_inconsistentes_actuales']);?></span></a> de <?php echo number_format($obj[0]['count_inconsistentes_evaluadas']+$obj[0]['count_inconsistentes_actuales']);?></td>
			<td class='right'>
			<span title="Pendientes: <?php echo number_format($obj[0]['count_consistentes']-$obj[0]['count_enviados_distriluz']);?>"><?php echo number_format($obj[0]['count_enviados_distriluz']);?></span>
			</td>
			<td class='right'>
			<a href="<?php echo ENV_WEBROOT_FULL_URL;?>EvaluarInconsistencias/evaluar_inconsistencia/U/<?php echo date('Y-m-d');?>/<?php echo date('Y-m-d');?>/0/<?php echo $obj[0]['idciclo'];?>/0/0/0/0/0/OBSCL" target="_blank">
			<?php echo number_format($obj[0]['count_observaciones_con_lectura']);?>
			</a>
			</td>
			<td class='right'>
			<a href="<?php echo ENV_WEBROOT_FULL_URL;?>EvaluarInconsistencias/evaluar_inconsistencia/U/<?php echo date('Y-m-d');?>/<?php echo date('Y-m-d');?>/0/<?php echo $obj[0]['idciclo'];?>/0/0/0/0/0/OBSSL" target="_blank">
			<?php echo number_format($obj[0]['count_observaciones_sin_lectura']);?>
			</a>
			</td>
			<td class='right'>
			<a href="<?php echo ENV_WEBROOT_FULL_URL;?>EvaluarInconsistencias/evaluar_inconsistencia/U/<?php echo date('Y-m-d');?>/<?php echo date('Y-m-d');?>/0/<?php echo $obj[0]['idciclo'];?>/0/0/0/0/0/CC" target="_blank">
			<?php echo number_format($obj[0]['count_consumo_cero']);?>
			</a>
			</td>
			<td class='right'>
			<a href="<?php echo ENV_WEBROOT_FULL_URL;?>EvaluarInconsistencias/evaluar_inconsistencia/U/<?php echo date('Y-m-d');?>/<?php echo date('Y-m-d');?>/0/<?php echo $obj[0]['idciclo'];?>/0/0/0/0/34/" target="_blank">
			<?php echo number_format($obj[0]['count_consumo_menor_35']);?>
			</a>
			</td>
			<td class='right'>
			<a href="<?php echo ENV_WEBROOT_FULL_URL;?>EvaluarInconsistencias/evaluar_inconsistencia/U/<?php echo date('Y-m-d');?>/<?php echo date('Y-m-d');?>/0/<?php echo $obj[0]['idciclo'];?>/0/0/0/1001/0/" target="_blank">
			<?php echo number_format($obj[0]['count_consumo_mayor_1000']);?>
			</a>
			</td>
			<td class='right'>
			<a href="<?php echo ENV_WEBROOT_FULL_URL;?>EvaluarInconsistencias/evaluar_inconsistencia/U/<?php echo date('Y-m-d');?>/<?php echo date('Y-m-d');?>/0/<?php echo $obj[0]['idciclo'];?>/0/0/0/0/0/C100MA" target="_blank">
			<?php echo number_format($obj[0]['count_consumo_mayor_100_porciento']);?>
			</a>
			</td>
			</tr>
			<?php
			$sum_suministros += $obj[0]['count_suministros'];
			$sum_asignados += $obj[0]['count_asignados'];
			$sum_descargados += $obj[0]['count_descargados'];
			$sum_finalizados += $obj[0]['count_finalizados'];
			$sum_pendientes += ($obj[0]['count_suministros']-$obj[0]['count_finalizados']);
			$sum_consistentes += $obj[0]['count_consistentes'];
			$sum_inconsistentes_actuales += $obj[0]['count_inconsistentes_actuales'];
			$sum_inconsistentes_evaluadas += $obj[0]['count_inconsistentes_evaluadas'];
			$sum_observaciones_con_lectura += $obj[0]['count_observaciones_con_lectura'];
			$sum_observaciones_sin_lectura += $obj[0]['count_observaciones_sin_lectura'];
			$sum_consumo_cero += $obj[0]['count_consumo_cero'];
			$sum_consumo_menor_35 += $obj[0]['count_consumo_menor_35'];
			$sum_consumo_mayor_1000 += $obj[0]['count_consumo_mayor_1000'];
			$sum_consumo_mayor_100_porciento += $obj[0]['count_consumo_mayor_100_porciento'];
			$sum_enviados_distriluz += $obj[0]['count_enviados_distriluz'];
		} 
		?>		
	</tbody>
	<tfoot>
		<tr>
			<td colspan="2">Total</td>
			<td class='right'><?php echo number_format($sum_suministros);?></td>
			<td class='right'><?php echo number_format($sum_asignados);?></td>
			<td class='right'><?php echo number_format($sum_descargados);?></td>
			<td class='right'><?php echo number_format($sum_finalizados);?></td>
			<td class='right'>
			<?php 
			if($sum_pendientes==0){
			?>
			<span class="badge badge-success">0</span>
			<?php }else{?>
			<span class="badge badge-danger"><?php echo number_format($sum_pendientes);?></span>
			<?php }?>
			</td>
			<?php 
			$style_badge_inconsistente = '';
			if($sum_inconsistentes_actuales>0){
				$style_badge_inconsistente = 'badge badge-danger';
			}
			?>
			<td class='right'><?php echo number_format($sum_consistentes);?></td>
			<td class='right'><span class="<?php echo $style_badge_inconsistente;?>"><?php echo number_format($sum_inconsistentes_actuales);?></span> de <?php echo number_format($sum_inconsistentes_evaluadas+$sum_inconsistentes_actuales);?></td>
			<td class='right'><span title="Pendientes: <?php echo number_format($sum_consistentes-$sum_enviados_distriluz);?>"><?php echo number_format($sum_enviados_distriluz);?></span></td>
			<td class='right'><?php echo number_format($sum_observaciones_con_lectura);?></td>
			<td class='right'><?php echo number_format($sum_observaciones_sin_lectura);?></td>
			<td class='right'><?php echo number_format($sum_consumo_cero);?></td>
			<td class='right'><?php echo number_format($sum_consumo_menor_35);?></td>
			<td class='right'><?php echo number_format($sum_consumo_mayor_1000);?></td>
			<td class='right'><?php echo number_format($sum_consumo_mayor_100_porciento);?></td>
		</tr>
	</tfoot>
</table>


  <script type="text/javascript">

        function UpdateTableHeaders() {
            $("div.divTableWithFloatingHeader").each(function() {
                var originalHeaderRow = $(".tableFloatingHeaderOriginal", this);
                var floatingHeaderRow = $(".tableFloatingHeader", this);
                var offset = $(this).offset();
                var scrollTop = $(window).scrollTop();


                if (( scrollTop > offset.top ) && (scrollTop < offset.top + $(this).height())) {
                    floatingHeaderRow.css("visibility", "visible");
                    floatingHeaderRow.css("top", (Math.min(scrollTop - offset.top, $(this).height() - floatingHeaderRow.height()))  + "px");

                    // Copy cell widths from original header
                    $("th", floatingHeaderRow).each(function(index) {
                        var cellWidth = $("th", originalHeaderRow).eq(index).css('width');
                        $(this).css('width', cellWidth);
                    });

                    // Copy row width from whole table
                    floatingHeaderRow.css("width", $(this).css("width"));
                }
                else {
                    floatingHeaderRow.css("visibility", "hidden");
                    floatingHeaderRow.css("top", "0px");
                    floatingHeaderRow.css("background-color", "#F8F8F8");
                }
            });
        }

        $(document).ready(function() {
            $("table.tableWithFloatingHeader").each(function() {
                $(this).wrap("<div class=\"divTableWithFloatingHeader\" style=\"position:relative\"></div>");

                var originalHeaderRow = $("tr:first", this)
                originalHeaderRow.before(originalHeaderRow.clone());
                var clonedHeaderRow = $("tr:first", this)

                clonedHeaderRow.addClass("tableFloatingHeader");
                clonedHeaderRow.css("position", "absolute");
                clonedHeaderRow.css("top", "0px");
                clonedHeaderRow.css("left", $(this).css("margin-left"));
                clonedHeaderRow.css("visibility", "hidden");
                clonedHeaderRow.css("background-color", "#F8F8F8");

                originalHeaderRow.addClass("tableFloatingHeaderOriginal");
            });

            UpdateTableHeaders();
            $(window).scroll(UpdateTableHeaders);
            $(window).resize(UpdateTableHeaders);
        });
    

  </script>