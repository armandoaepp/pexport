<style>

    .widget-monitoreo {
        padding: 0px !important;
        text-align: center;
    }
    .widget-monitoreo h2 {
        font-size: 20px !important;
    }
    .widget-monitoreo h3 {
        font-size: 15px !important;
    }
    .widget-monitoreo-block {
        width: 200px;
        padding: 3px;
        text-align: left;
        display:inline-block;
    	float:none;
    }
    .widget-monitoreo-block .fact-data {
        margin: 4px 0px !important;
        padding-left: 7px !important;
        padding-right: 7px !important;
        width: 64px !important;
    }

</style>

<?php 
function getColorBar($percent){
	if($percent>=70){
		$color_bar = 'data-barcolor="#60C060"';
	}elseif($percent>=50){
		$color_bar = 'data-barcolor="#4D90FD"';
	}elseif($percent>=30){
		$color_bar = 'data-barcolor="#FC9700"';
	}else{
		$color_bar = '';
	}
	return $color_bar;
}
?>

<div class="col-sm-6 col-md-6 widget-monitoreo">

	<div class="header" style="text-align: center;margin-right: 20px;">
		<h2>KPI - D&iacute;a Actual</h2>
	</div>
	<br>

	<div class="col-lg-4 widget-monitoreo-block">
		<div class="widget-block" style="margin-bottom: 8px;">
			<div class="white-box">
				<div class="fact-data">
					<div class="epie-chart" <?php echo getColorBar(100);?> data-percent="100"><span>100%</span></div>
				</div>
				<div class="fact-data no-padding text-shadow" style="margin: 10px 0px;">
					<h3>Suministros</h3>
					<h2><?php echo $cantidad_ordenes_dia_actual;?></h2>
				</div>
			</div>
		</div>
	</div>
	
	<div class="col-lg-4 widget-monitoreo-block">
		<div class="widget-block" style="margin-bottom: 8px;">
			<div class="white-box">
				<div class="fact-data">
					<div class="epie-chart" <?php echo getColorBar($porcentaje_progreso_dia_actual);?> data-percent="<?php echo $porcentaje_progreso_dia_actual;?>"><span><?php echo $porcentaje_progreso_dia_actual;?>%</span></div>
				</div>
				<div class="fact-data no-padding text-shadow" style="margin: 10px 0px;">
					<h3>Finalizados</h3>
					<h2><?php echo $cantidad_avanzados_dia_actual;?></h2>
				</div>
			</div>
		</div>
	</div>
	
	<div class="col-lg-4 widget-monitoreo-block">
		<div class="widget-block" style="margin-bottom: 8px;">
			<div class="white-box">
				<div class="fact-data">
					<div class="epie-chart" <?php echo getColorBar($porcentaje_pendiente_dia_actual);?> data-percent="<?php echo $porcentaje_pendiente_dia_actual;?>"><span><?php echo $porcentaje_pendiente_dia_actual;?>%</span></div>
				</div>
				<div class="fact-data no-padding text-shadow" style="margin: 10px 0px;">
					<h3>Pendientes</h3>
					<h2><?php echo $pendientes_dia_actual;?></h2>
				</div>
			</div>
		</div>
	</div>
	
	<div class="col-lg-4 widget-monitoreo-block">
		<div class="widget-block" style="margin-bottom: 8px;">
			<div class="white-box">
				<div class="fact-data">
					<div class="epie-chart" <?php echo getColorBar($porcentaje_consistencia_dia_actual);?> data-percent="<?php echo $porcentaje_consistencia_dia_actual;?>"><span><?php echo $porcentaje_consistencia_dia_actual;?>%</span></div>
				</div>
				<div class="fact-data no-padding text-shadow" style="margin: 10px 0px;">
					<h3>Consistentes</h3>
					<h2><?php echo $consistencia_dia_actual;?></h2>
				</div>
			</div>
		</div>
	</div>
	
	<div class="col-lg-4 widget-monitoreo-block">
		<div class="widget-block" style="margin-bottom: 8px;">
			<div class="white-box">
				<div class="fact-data">
					<div class="epie-chart" <?php echo getColorBar($porcentaje_inconsistencia_dia_actual);?> data-percent="<?php echo $porcentaje_inconsistencia_dia_actual;?>"><span><?php echo $porcentaje_inconsistencia_dia_actual;?>%</span></div>
				</div>
				<div class="fact-data no-padding text-shadow" style="margin: 10px 0px;">
					<h3>Inconsistentes</h3>
					<h2><?php echo $cantidad_inconsistentes_dia_actual;?></h2>
				</div>
			</div>
		</div>
	</div>
	
	<div class="col-lg-4 widget-monitoreo-block">
		<div class="widget-block" style="margin-bottom: 8px;">
			<div class="white-box">
				<div class="fact-data">
					<div class="epie-chart" <?php echo getColorBar($porcentaje_distriluz_dia_actual);?> data-percent="<?php echo $porcentaje_distriluz_dia_actual;?>"><span><?php echo $porcentaje_distriluz_dia_actual;?>%</span></div>
				</div>
				<div class="fact-data no-padding text-shadow" style="margin: 10px 0px;">
					<h3>Distriluz</h3>
					<h2 title="Pendientes: <?php echo ($consistencia_dia_actual-$cantidad_distriluz_dia_actual);?>"><?php echo $cantidad_distriluz_dia_actual;?></h2>
				</div>
			</div>
		</div>
	</div>
	
</div>

<div class="col-sm-6 col-md-6 widget-monitoreo" style="padding-left: 15px !important;">

	<div class="header" style="text-align: center;margin-left: 10px;">
		<h2>KPI - Periodo Actual</h2>
	</div>
	<br>

	<div class="col-lg-4 widget-monitoreo-block">
		<div class="widget-block" style="margin-bottom: 8px;">
			<div class="white-box">
				<div class="fact-data">
					<div class="epie-chart" <?php echo getColorBar(100);?> data-percent="100"><span>100%</span></div>
				</div>
				<div class="fact-data no-padding text-shadow" style="margin: 10px 0px;">
					<h3>Suministros</h3>
					<h2><?php echo $cantidad_ordenes;?></h2>
				</div>
			</div>
		</div>
	</div>
	
	<div class="col-lg-4 widget-monitoreo-block">
		<div class="widget-block" style="margin-bottom: 8px;">
			<div class="white-box">
				<div class="fact-data">
					<div class="epie-chart" <?php echo getColorBar($porcentaje_progreso);?> data-percent="<?php echo $porcentaje_progreso;?>"><span><?php echo $porcentaje_progreso;?>%</span></div>
				</div>
				<div class="fact-data no-padding text-shadow" style="margin: 10px 0px;">
					<h3>Finalizados</h3>
					<h2><?php echo $cantidad_avanzados;?></h2>
				</div>
			</div>
		</div>
	</div>
	
	<div class="col-lg-4 widget-monitoreo-block">
		<div class="widget-block" style="margin-bottom: 8px;">
			<div class="white-box">
				<div class="fact-data">
					<div class="epie-chart" <?php echo getColorBar($porcentaje_pendiente);?> data-percent="<?php echo $porcentaje_pendiente;?>"><span><?php echo $porcentaje_pendiente;?>%</span></div>
				</div>
				<div class="fact-data no-padding text-shadow" style="margin: 10px 0px;">
					<h3>Pendientes</h3>
					<h2><?php echo $pendientes;?></h2>
				</div>
			</div>
		</div>
	</div>
	
	<div class="col-lg-4 widget-monitoreo-block">
		<div class="widget-block" style="margin-bottom: 8px;">
			<div class="white-box">
				<div class="fact-data">
					<div class="epie-chart" <?php echo getColorBar($porcentaje_consistencia);?> data-percent="<?php echo $porcentaje_consistencia;?>"><span><?php echo $porcentaje_consistencia;?>%</span></div>
				</div>
				<div class="fact-data no-padding text-shadow" style="margin: 10px 0px;">
					<h3>Consistentes</h3>
					<h2><?php echo $consistencia;?></h2>
				</div>
			</div>
		</div>
	</div>
	
	<div class="col-lg-4 widget-monitoreo-block">
		<div class="widget-block" style="margin-bottom: 8px;">
			<div class="white-box">
				<div class="fact-data">
					<div class="epie-chart" <?php echo getColorBar($porcentaje_inconsistencia);?> data-percent="<?php echo $porcentaje_inconsistencia;?>"><span><?php echo $porcentaje_inconsistencia;?>%</span></div>
				</div>
				<div class="fact-data no-padding text-shadow" style="margin: 10px 0px;">
					<h3>Inconsistentes</h3>
					<h2><?php echo $cantidad_inconsistentes;?></h2>
				</div>
			</div>
		</div>
	</div>
	
	<div class="col-lg-4 widget-monitoreo-block">
		<div class="widget-block" style="margin-bottom: 8px;">
			<div class="white-box">
				<div class="fact-data">
					<div class="epie-chart" <?php echo getColorBar($porcentaje_distriluz);?> data-percent="<?php echo $porcentaje_distriluz;?>"><span><?php echo $porcentaje_distriluz;?>%</span></div>
				</div>
				<div class="fact-data no-padding text-shadow" style="margin: 10px 0px;">
					<h3>Distriluz</h3>
					<h2 title="Pendientes: <?php echo ($consistencia-$cantidad_distriluz);?>"><?php echo $cantidad_distriluz;?></h2>
				</div>
			</div>
		</div>
	</div>
	
</div>

<script type="text/javascript">
	$('.epie-chart').easyPieChart({
        barColor: '#FD9C35',
        trackColor: '#EFEFEF',
        lineWidth: 7,
        animate: 600,
        size: 55,
        onStep: function(val){//Update current value while animation
          $("span", this.$el).html(parseInt(val) + "%");
        }
      });
</script>
