<!DOCTYPE html>
<html lang="es" >
<head>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta http-equiv="content-type" content="text/html; charset=UTF-8">
<title>Grafico Pexport</title>

<script type="text/javascript" src="//code.jquery.com/jquery-1.9.1.js"></script>
<script src="http://code.highcharts.com/highcharts.js"></script>
<script src="http://code.highcharts.com/modules/data.js"></script>
<script src="http://code.highcharts.com/modules/drilldown.js"></script>
<script src="http://code.highcharts.com/modules/exporting.js"></script>


<script type="text/javascript">
$(function () {

    Highcharts.data({
        csv: document.getElementById('tsv').innerHTML,
        itemDelimiter: '\t',
        parsed: function (columns) {

            var brands = {},
                brandsData = [],
                versions = {},
                drilldownSeries = [];

            <?php 
        	$data = array();
        	if(count($arr_codigos_observacion)>0){
				foreach ($arr_ciclos as $obj_ciclo){
					$var = '';
					$i = 0;
            		foreach ($arr_codigos_observacion as $obj_codigo_obs1){
						if($obj_ciclo[0]['idciclo']==$obj_codigo_obs1[0]['idciclo']){
							echo "if (!versions['".$obj_ciclo[0]['idciclo']." - ".$obj_ciclo[0]['nombciclo']."']) {versions['".$obj_ciclo[0]['idciclo']." - ".$obj_ciclo[0]['nombciclo']."'] = [];}";
							echo "versions['".$obj_ciclo[0]['idciclo']." - ".$obj_ciclo[0]['nombciclo']."'].push(['".$obj_codigo_obs1[0]['obs1']." - ".$obj_codigo_obs1[0]['descripcion']."', ".$obj_codigo_obs1[0]['cantidad']."]);";
							$i += $obj_codigo_obs1[0]['cantidad'];
						}
            		}
            		echo "brandsData.push({name: '".$obj_ciclo[0]['idciclo']." - ".$obj_ciclo[0]['nombciclo']."', y: ".$i.", drilldown: '".$obj_ciclo[0]['idciclo']." - ".$obj_ciclo[0]['nombciclo']."'});";
            		echo "drilldownSeries.push({name: '".$obj_ciclo[0]['idciclo']." - ".$obj_ciclo[0]['nombciclo']."', id: '".$obj_ciclo[0]['idciclo']." - ".$obj_ciclo[0]['nombciclo']."', data: versions['".$obj_ciclo[0]['idciclo']." - ".$obj_ciclo[0]['nombciclo']."']});";
            		echo $var;
        		}
        	}

        	?>

        	console.log(brandsData);console.log(drilldownSeries);

            // Create the chart
            $('#container').highcharts({
                chart: {
                    type: 'bar'
                },
                title: {
                    text: 'Resumen de Codigos de Observacion por Ciclos'
                },
                subtitle: {
                    text: 'Click en la columna para ver los Codigos de Observacion'
                },
                xAxis: {
                    type: 'category'
                },
                yAxis: {
                    title: {
                        text: 'Cantidad de Suministros'
                    }
                },
                legend: {
                    enabled: true
                },
                plotOptions: {
                    series: {
                        borderWidth: 0,
                        dataLabels: {
                            enabled: true,
                            format: '{point.y:f}'
                        }
                    }
                },

                tooltip: {
                    headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
                    pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:f}</b> Suministros<br/>'
                }, 
                credits: {
    	            enabled: false
    	        },
                series: [{
                    name: 'Ciclos',
                    colorByPoint: true,
                    data: brandsData
                }],
                drilldown: {
                    series: drilldownSeries
                }
            })

        }
    });
});
</script>


</head>
<body>
	<!-- 
	<script src="http://code.highcharts.com/highcharts.js"></script>
	<script src="http://code.highcharts.com/modules/exporting.js"></script>
	 -->

	<div id="container" style="width: 100%; height: 440px; margin: 0 auto">
	</div>
	<pre id="tsv" style="display:none">Browser Version	Total Market Share
Microsoft Internet Explorer 8.0	26.61%
</pre>
	
</body>
</html>