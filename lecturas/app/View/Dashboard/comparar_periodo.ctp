<?php 
if (!isset($NEGOCIO)) {
    header("Location:".ENV_WEBROOT_FULL_URL."/Usuarios/logout");
    die();
}
?>
<?php 
if (isset($id_usuario) && $id_usuario == 25 || $id_usuario == 131){
	$permiso_lectura = 'true';
}else {
	$permiso_lectura = 'true';
}
echo $this->element('menu',array('active'=>'rep_comparar_periodo', 'open'=>'false', 'permiso_lectura' =>$permiso_lectura, 'id_usuario ' =>isset($id_usuario)?$id_usuario:null));
?>

<?php 
$data = array();
if(count($arr_codigos_observacion)>0){
	foreach ($arr_codigos_observacion as $obj_codigo_obs1){
		$data[] = array($obj_codigo_obs1[0]['obs1'],$obj_codigo_obs1[0]['cantidad']);
	}
}
?>
<input type="hidden" name= "data-codigos-de-observacion" value='<?php echo json_encode($data)?>' id= "data-codigos-de-observacion">

<!-- DATA 1 -->

<div class="container-fluid" id="pcont">
		  <div class="cl-mcont">
		  
		  <div class="form-group">
              <label class="control-label">Periodo de Factura:</label>              
              	<?php 
	        			  	echo '<select class="select2" name="pfactura" id="pfactura">
	        			  	<option value="0">Actual</option>';
                                             //   var_dump($listar_unidadneg);exit;
	        				foreach ($listar_pfacrura as $v){
							if($pfactura==$v['0']['pfactura']){
								$selected = 'selected';
							}else{
								$selected = '';
							}
	        				echo '<option value="'.$v['0']['pfactura'].'" '.$selected.'>'.$v['0']['pfactura'].'</option>';
	        				}
	        				echo '</select>';	
	        		?>        			
            </div>
		  
			<div class="stats_bar">
				<div class="butpro butstyle" style="width: 30%;">
					<div class="sub"><h2>SUMINISTROS</h2><span id="total_clientes"><?php echo number_format($cantidad_ordenes);?></span></div>
					<div class="stat"><span class="equal"> 100%</span></div>
				</div>
				<div class="butpro butstyle" style="width: 30%;">
					<div class="sub"><h2>AVANZADOS</h2><span><?php echo number_format($cantidad_avanzados);?></span></div>
					<div class="stat"><span class="fa fa-ellipsis-h"> <?php echo number_format($cantidad_avanzados*100/$cantidad_ordenes,2);?>%</span></div>
				</div>
				<div class="butpro butstyle" style="width: 30%;">
					<div class="sub"><h2>CONSISTENTES</h2><span><?php echo number_format($cantidad_consistentes);?></span></div>
					<div class="stat"><span class="<?php if(($cantidad_consistentes*100/$cantidad_ordenes)>($cantidad_consistentes2*100/$cantidad_ordenes2)){ echo 'up';}else{echo 'down';}?>"> <?php echo number_format($cantidad_consistentes*100/$cantidad_ordenes,2);?>%</span></div>
				</div>
				<div class="butpro butstyle" style="width: 30%;">
					<div class="sub"><h2>INCONSISTENTES ACTUALES</h2><span><?php echo number_format($cantidad_inconsistentes_actuales);?></span></div>
					<div class="stat"><span class="<?php if(($cantidad_inconsistentes_actuales*100/$cantidad_ordenes)>($cantidad_inconsistentes_actuales2*100/$cantidad_ordenes2)){ echo 'up';}else{echo 'down';}?>"> <?php echo number_format($cantidad_inconsistentes_actuales*100/$cantidad_ordenes,2);?>%</span></div>
				</div>				
				<div class="butpro butstyle" style="width: 30%;">
					<div class="sub"><h2>INCONSISTENTES EVALUADAS</h2><span><?php echo number_format($cantidad_inconsistentes_evaluadas);?></span></div>
					<div class="stat"><span class="<?php if(($cantidad_inconsistentes_evaluadas*100/$cantidad_ordenes)>($cantidad_inconsistentes_evaluadas2*100/$cantidad_ordenes2)){ echo 'up';}else{echo 'down';}?>"> <?php echo number_format($cantidad_inconsistentes_evaluadas*100/$cantidad_ordenes,2);?>%</span></div>
				</div>
				<div class="butpro butstyle" style="width: 30%;">
					<div class="sub"><h2>INCONSISTENTES ORIGINALES</h2><span><?php echo number_format($cantidad_inconsistentes_actuales+$cantidad_inconsistentes_evaluadas);?></span></div>
					<div class="stat"><span class="<?php if((($cantidad_inconsistentes_actuales+$cantidad_inconsistentes_evaluadas)*100/$cantidad_ordenes)>(($cantidad_inconsistentes_actuales2+$cantidad_inconsistentes_evaluadas2)*100/$cantidad_ordenes2)){ echo 'up';}else{echo 'down';}?>"> <?php echo number_format(($cantidad_inconsistentes_actuales+$cantidad_inconsistentes_evaluadas)*100/$cantidad_ordenes,2);?>%</span></div>
				</div>
				<div class="butpro butstyle" style="width: 30%;">
					<div class="sub"><h2>OBSERVACIONES SIN LECTURA</h2><span><?php echo number_format($cantidad_observaciones_sin_lectura);?></span></div>
					<div class="stat"><span class="<?php if(($cantidad_observaciones_sin_lectura*100/$cantidad_ordenes)>($cantidad_observaciones_sin_lectura2*100/$cantidad_ordenes2)){ echo 'up';}else{echo 'down';}?>"></span><?php echo number_format($cantidad_observaciones_sin_lectura*100/$cantidad_ordenes,2);?>%</span></div>
				</div>
				<div class="butpro butstyle" style="width: 30%;">
					<div class="sub"><h2>OBSERVACIONES CON LECTURA</h2><span><?php echo number_format($cantidad_observaciones_con_lectura);?></span></div>
					<div class="stat"><span class="<?php if(($cantidad_observaciones_con_lectura*100/$cantidad_ordenes)>($cantidad_observaciones_con_lectura2*100/$cantidad_ordenes2)){ echo 'up';}else{echo 'down';}?>"></span><?php echo number_format($cantidad_observaciones_con_lectura*100/$cantidad_ordenes,2);?>%</span></div>
				</div>
				<div class="butpro butstyle" style="width: 30%;">
					<div class="sub"><h2>TOTAL OBSERVACIONES</h2><span><?php echo number_format($cantidad_observaciones);?></span></div>
					<div class="stat"><span class="<?php if(($cantidad_observaciones*100/$cantidad_ordenes)>($cantidad_observaciones2*100/$cantidad_ordenes2)){ echo 'up';}else{echo 'down';}?>"></span><?php echo number_format($cantidad_observaciones*100/$cantidad_ordenes,2);?>%</span></div>
				</div>
				<div class="butpro butstyle" style="width: 30%;">
					<div class="sub"><h2>CONSUMO CERO</h2><span><?php echo number_format($cantidad_consumo_cero);?></span></div>
					<div class="stat"><span class="<?php if(($cantidad_consumo_cero*100/$cantidad_ordenes)>($cantidad_consumo_cero2*100/$cantidad_ordenes2)){ echo 'up';}else{echo 'down';}?>"></span><?php echo number_format($cantidad_consumo_cero*100/$cantidad_ordenes,2);?>%</span></div>
				</div>
				<div class="butpro butstyle" style="width: 30%;">
					<div class="sub"><h2>CONSUMO MENOR 35</h2><span><?php echo number_format($cantidad_consumo_menor35);?></span></div>
					<div class="stat"><span class="<?php if(($cantidad_consumo_menor35*100/$cantidad_ordenes)>($cantidad_consumo_menor352*100/$cantidad_ordenes2)){ echo 'up';}else{echo 'down';}?>"></span><?php echo number_format($cantidad_consumo_menor35*100/$cantidad_ordenes,2);?>%</span></div>
				</div>
				<div class="butpro butstyle" style="width: 30%;">
					<div class="sub"><h2>TIEMPO EJECUCION</h2><span><?php echo substr($cantidad_tiempoejecucion,0,8);?></span></div>
					<div class="stat"><span class="spk2"></span></div>
				</div>
			</div>	

			<div class="row dash-cols">
			
				<div class="col-sm-12 col-md-12">
					<div class="block">
						<div class="header no-border">
							<h2>C&oacute;digos de Observaci&oacute;n</h2>
						</div>
						<div class="content blue-chart">
							<div id="container_grahp_codigos_observacion" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
							<pre id="tsv" style="display:none">Browser Version	Total Market Share
Microsoft Internet Explorer 8.0	26.61%
</pre>
<script type="text/javascript">
$(function () {

    Highcharts.data({
        csv: document.getElementById('tsv').innerHTML,
        itemDelimiter: '\t',
        parsed: function (columns) {

            var brands = {},
                brandsData = [],
                versions = {},
                drilldownSeries = [];

            <?php 
        	$data = array();
        	if(count($arr_codigos_observacion)>0){
				foreach ($arr_ciclos as $obj_ciclo){
					$var = '';
					$i = 0;
            		foreach ($arr_codigos_observacion as $obj_codigo_obs1){
						if($obj_ciclo[0]['idciclo']==$obj_codigo_obs1[0]['idciclo']){
							echo "if (!versions['".$obj_ciclo[0]['idciclo']."']) {versions['".$obj_ciclo[0]['idciclo']."'] = [];}";
							echo "versions['".$obj_ciclo[0]['idciclo']."'].push(['".$obj_codigo_obs1[0]['obs1']."', ".$obj_codigo_obs1[0]['cantidad']."]);";
							$i += $obj_codigo_obs1[0]['cantidad'];
						}
            		}
            		echo "brandsData.push({name: '".$obj_ciclo[0]['idciclo']."', y: ".$i.", drilldown: '".$obj_ciclo[0]['idciclo']."'});";
            		echo "drilldownSeries.push({name: '".$obj_ciclo[0]['idciclo']."', id: '".$obj_ciclo[0]['idciclo']."', data: versions['".$obj_ciclo[0]['idciclo']."']});";
            		echo $var;
        		}
        	}

        	?>

        	console.log(brandsData);console.log(drilldownSeries);

            // Create the chart
            $('#container_grahp_codigos_observacion').highcharts({
                chart: {
                    type: 'column'
                },
                title: {
                    text: 'Resumen de Codigos de Observacion por Ciclos'
                },
                subtitle: {
                    text: 'Click en la columna para ver los Codigos de Observacion'
                },
                xAxis: {
                    type: 'category'
                },
                yAxis: {
                    title: {
                        text: 'Cantidad de Suministros'
                    }
                },
                legend: {
                    enabled: true
                },
                plotOptions: {
                    series: {
                        borderWidth: 0,
                        dataLabels: {
                            enabled: true,
                            format: '{point.y:f}'
                        }
                    }
                },

                tooltip: {
                    headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
                    pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:f}</b> Suministros<br/>'
                }, 
                credits: {
    	            enabled: false
    	        },
                series: [{
                    name: 'Ciclos',
                    colorByPoint: true,
                    data: brandsData
                }],
                drilldown: {
                    series: drilldownSeries
                }
            })

        }
    });
});
</script>
						</div>
						<div class="content">
							<div class="stat-data">
								<div class="stat-blue">
									<h2><?php echo number_format($cantidad_ordenes);?></h2>
									<span>Total de Ordenes de Lectura</span>
								</div>
							</div>
							<div class="stat-data">
								<div class="stat-number">
									<div><h2><?php echo number_format($cantidad_observaciones*100/$cantidad_ordenes,2);?>%</h2></div>
									<div>Porcentaje Observaciones<br /><span></span></div>
								</div>
								<div class="stat-number">
									<div><h2><?php echo number_format($cantidad_observaciones);?></h2></div>
									<div>Total Observaciones<br /><span><a href="<?php echo ENV_WEBROOT_FULL_URL ?>dashboard/reporte_observaciones">ver tabla</a></span></div>
								</div>
							</div>
							<div class="clear"></div>
							<table class="red">
								<thead>
									<tr>
										<th>Ciclos</th>
										<th class="left">Sectores</th>
									</tr>
								</thead>
								<tbody class="no-border-x">
								<?php foreach ($leyenda_ciclos as $obj_leyenda_ciclos){?>
									<tr>
										<td style="width:40%;"><?php echo $obj_leyenda_ciclos[0]['idciclo']?></td>
										<td class="text-left"><?php echo $obj_leyenda_ciclos[0]['sectores']?></td>
									</tr>
								<?php } ?>									
								</tbody>
							</table>
							
						</div>
					</div>
				</div>	
			</div>
		
		  </div>
		</div>

<!-- DATA 2 -->
		
<div class="container-fluid" id="pcont">
		  <div class="cl-mcont">
		  
		  <div class="form-group">
              <label class="control-label">Periodo de Factura:</label>              
              	<?php 
	        			  	echo '<select class="select2" name="pfactura2" id="pfactura2">
	        			  	<option value="0">Actual</option>';
                                             //   var_dump($listar_unidadneg);exit;
	        				foreach ($listar_pfacrura as $v){
							if($pfactura2==$v['0']['pfactura']){
								$selected = 'selected';
							}else{
								$selected = '';
							}
	        				echo '<option value="'.$v['0']['pfactura'].'" '.$selected.'>'.$v['0']['pfactura'].'</option>';
	        				}
	        				echo '</select>';	
	        		?>        			
            </div>
			<div class="stats_bar">
				<div class="butpro butstyle" style="width: 30%;">
					<div class="sub"><h2>SUMINISTROS</h2><span id="total_clientes"><?php echo number_format($cantidad_ordenes2);?></span></div>
					<div class="stat"><span class="equal"> 100%</span></div>
				</div>
				<div class="butpro butstyle" style="width: 30%;">
					<div class="sub"><h2>AVANZADOS</h2><span><?php echo number_format($cantidad_avanzados2);?></span></div>
					<div class="stat"><span class="fa fa-ellipsis-h"> <?php echo number_format($cantidad_avanzados2*100/$cantidad_ordenes2,2);?>%</span></div>
				</div>
				<div class="butpro butstyle" style="width: 30%;">
					<div class="sub"><h2>CONSISTENTES</h2><span><?php echo number_format($cantidad_consistentes2);?></span></div>
					<div class="stat"><span class="<?php if(($cantidad_consistentes*100/$cantidad_ordenes)>($cantidad_consistentes2*100/$cantidad_ordenes2)){ echo 'down';}else{echo 'up';}?>"> <?php echo number_format($cantidad_consistentes2*100/$cantidad_ordenes2,2);?>%</span></div>
				</div>
				<div class="butpro butstyle" style="width: 30%;">
					<div class="sub"><h2>INCONSISTENTES ACTUALES</h2><span><?php echo number_format($cantidad_inconsistentes_actuales2);?></span></div>
					<div class="stat"><span class="<?php if(($cantidad_inconsistentes_actuales*100/$cantidad_ordenes)>($cantidad_inconsistentes_actuales2*100/$cantidad_ordenes2)){ echo 'down';}else{echo 'up';}?>"> <?php echo number_format($cantidad_inconsistentes_actuales2*100/$cantidad_ordenes2,2);?>%</span></div>
				</div>				
				<div class="butpro butstyle" style="width: 30%;">
					<div class="sub"><h2>INCONSISTENTES EVALUADAS</h2><span><?php echo number_format($cantidad_inconsistentes_evaluadas2);?></span></div>
					<div class="stat"><span class="<?php if(($cantidad_inconsistentes_evaluadas*100/$cantidad_ordenes)>($cantidad_inconsistentes_evaluadas2*100/$cantidad_ordenes2)){ echo 'down';}else{echo 'up';}?>"> <?php echo number_format($cantidad_inconsistentes_evaluadas2*100/$cantidad_ordenes2,2);?>%</span></div>
				</div>
				<div class="butpro butstyle" style="width: 30%;">
					<div class="sub"><h2>INCONSISTENTES ORIGINALES</h2><span><?php echo number_format($cantidad_inconsistentes_actuales2+$cantidad_inconsistentes_evaluadas2);?></span></div>
					<div class="stat"><span class="<?php if((($cantidad_inconsistentes_actuales+$cantidad_inconsistentes_evaluadas)*100/$cantidad_ordenes)>(($cantidad_inconsistentes_actuales2+$cantidad_inconsistentes_evaluadas2)*100/$cantidad_ordenes2)){ echo 'down';}else{echo 'up';}?>"> <?php echo number_format(($cantidad_inconsistentes_actuales2+$cantidad_inconsistentes_evaluadas2)*100/$cantidad_ordenes2,2);?>%</span></div>
				</div>
				<div class="butpro butstyle" style="width: 30%;">
					<div class="sub"><h2>OBSERVACIONES SIN LECTURA</h2><span><?php echo number_format($cantidad_observaciones_sin_lectura2);?></span></div>
					<div class="stat"><span class="<?php if(($cantidad_observaciones_sin_lectura*100/$cantidad_ordenes)>($cantidad_observaciones_sin_lectura2*100/$cantidad_ordenes2)){ echo 'down';}else{echo 'up';}?>"></span><?php echo number_format($cantidad_observaciones_sin_lectura2*100/$cantidad_ordenes2,2);?>%</span></div>
				</div>
				<div class="butpro butstyle" style="width: 30%;">
					<div class="sub"><h2>OBSERVACIONES CON LECTURA</h2><span><?php echo number_format($cantidad_observaciones_con_lectura2);?></span></div>
					<div class="stat"><span class="<?php if(($cantidad_observaciones_con_lectura*100/$cantidad_ordenes)>($cantidad_observaciones_con_lectura2*100/$cantidad_ordenes2)){ echo 'down';}else{echo 'up';}?>"></span><?php echo number_format($cantidad_observaciones_con_lectura2*100/$cantidad_ordenes2,2);?>%</span></div>
				</div>
				<div class="butpro butstyle" style="width: 30%;">
					<div class="sub"><h2>TOTAL OBSERVACIONES</h2><span><?php echo number_format($cantidad_observaciones2);?></span></div>
					<div class="stat"><span class="<?php if(($cantidad_observaciones*100/$cantidad_ordenes)>($cantidad_observaciones2*100/$cantidad_ordenes2)){ echo 'down';}else{echo 'up';}?>"></span><?php echo number_format($cantidad_observaciones2*100/$cantidad_ordenes2,2);?>%</span></div>
				</div>
				<div class="butpro butstyle" style="width: 30%;">
					<div class="sub"><h2>CONSUMO CERO</h2><span><?php echo number_format($cantidad_consumo_cero2);?></span></div>
					<div class="stat"><span class="<?php if(($cantidad_consumo_cero*100/$cantidad_ordenes)>($cantidad_consumo_cero2*100/$cantidad_ordenes2)){ echo 'down';}else{echo 'up';}?>"></span><?php echo number_format($cantidad_consumo_cero2*100/$cantidad_ordenes2,2);?>%</span></div>
				</div>
				<div class="butpro butstyle" style="width: 30%;">
					<div class="sub"><h2>CONSUMO MENOR 35</h2><span><?php echo number_format($cantidad_consumo_menor352);?></span></div>
					<div class="stat"><span class="<?php if(($cantidad_consumo_menor35*100/$cantidad_ordenes)>($cantidad_consumo_menor352*100/$cantidad_ordenes2)){ echo 'down';}else{echo 'up';}?>"></span><?php echo number_format($cantidad_consumo_menor352*100/$cantidad_ordenes2,2);?>%</span></div>
				</div>
				<div class="butpro butstyle" style="width: 30%;">
					<div class="sub"><h2>TIEMPO EJECUCION</h2><span><?php echo substr($cantidad_tiempoejecucion2,0,8);?></span></div>
					<div class="stat"><span class="spk2"></span></div>
				</div>
			</div>	

			<div class="row dash-cols">
			
				<div class="col-sm-12 col-md-12">
					<div class="block">
						<div class="header no-border">
							<h2>C&oacute;digos de Observaci&oacute;n</h2>
						</div>
						<div class="content blue-chart">
							<div id="container_grahp_codigos_observacion2" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
							<pre id="tsv" style="display:none">Browser Version	Total Market Share
Microsoft Internet Explorer 8.0	26.61%
</pre>
<script type="text/javascript">
$(function () {

    Highcharts.data({
        csv: document.getElementById('tsv').innerHTML,
        itemDelimiter: '\t',
        parsed: function (columns) {

            var brands = {},
                brandsData = [],
                versions = {},
                drilldownSeries = [];

            <?php 
        	$data = array();
        	if(count($arr_codigos_observacion2)>0){
				foreach ($arr_ciclos2 as $obj_ciclo){
					$var = '';
					$i = 0;
            		foreach ($arr_codigos_observacion2 as $obj_codigo_obs1){
						if($obj_ciclo[0]['idciclo']==$obj_codigo_obs1[0]['idciclo']){
							echo "if (!versions['".$obj_ciclo[0]['idciclo']."']) {versions['".$obj_ciclo[0]['idciclo']."'] = [];}";
							echo "versions['".$obj_ciclo[0]['idciclo']."'].push(['".$obj_codigo_obs1[0]['obs1']."', ".$obj_codigo_obs1[0]['cantidad']."]);";
							$i += $obj_codigo_obs1[0]['cantidad'];
						}
            		}
            		echo "brandsData.push({name: '".$obj_ciclo[0]['idciclo']."', y: ".$i.", drilldown: '".$obj_ciclo[0]['idciclo']."'});";
            		echo "drilldownSeries.push({name: '".$obj_ciclo[0]['idciclo']."', id: '".$obj_ciclo[0]['idciclo']."', data: versions['".$obj_ciclo[0]['idciclo']."']});";
            		echo $var;
        		}
        	}

        	?>

        	console.log(brandsData);console.log(drilldownSeries);

            // Create the chart
            $('#container_grahp_codigos_observacion2').highcharts({
                chart: {
                    type: 'column'
                },
                title: {
                    text: 'Resumen de Codigos de Observacion por Ciclos'
                },
                subtitle: {
                    text: 'Click en la columna para ver los Codigos de Observacion'
                },
                xAxis: {
                    type: 'category'
                },
                yAxis: {
                    title: {
                        text: 'Cantidad de Suministros'
                    }
                },
                legend: {
                    enabled: true
                },
                plotOptions: {
                    series: {
                        borderWidth: 0,
                        dataLabels: {
                            enabled: true,
                            format: '{point.y:f}'
                        }
                    }
                },

                tooltip: {
                    headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
                    pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:f}</b> Suministros<br/>'
                }, 
                credits: {
    	            enabled: false
    	        },
                series: [{
                    name: 'Ciclos',
                    colorByPoint: true,
                    data: brandsData
                }],
                drilldown: {
                    series: drilldownSeries
                }
            })

        }
    });
});
</script>
						</div>
						<div class="content">
							<div class="stat-data">
								<div class="stat-blue">
									<h2><?php echo number_format($cantidad_ordenes2);?></h2>
									<span>Total de Ordenes de Lectura</span>
								</div>
							</div>
							<div class="stat-data">
								<div class="stat-number">
									<div><h2><?php echo number_format($cantidad_observaciones2*100/$cantidad_ordenes2,2);?>%</h2></div>
									<div>Porcentaje Observaciones<br /><span></span></div>
								</div>
								<div class="stat-number">
									<div><h2><?php echo number_format($cantidad_observaciones2);?></h2></div>
									<div>Total Observaciones<br /><span><a href="<?php echo ENV_WEBROOT_FULL_URL ?>dashboard/reporte_observaciones">ver tabla</a></span></div>
								</div>
							</div>
							<div class="clear"></div>
							
							<table class="red">
								<thead>
									<tr>
										<th>Ciclos</th>
										<th class="left">Sectores</th>
									</tr>
								</thead>
								<tbody class="no-border-x">
								<?php foreach ($leyenda_ciclos2 as $obj_leyenda_ciclos2){?>
									<tr>
										<td style="width:40%;"><?php echo $obj_leyenda_ciclos2[0]['idciclo']?></td>
										<td class="text-left"><?php echo $obj_leyenda_ciclos2[0]['sectores']?></td>
									</tr>
								<?php } ?>									
								</tbody>
							</table>
							
							
						</div>
					</div>
				</div>	
			</div>
		
		  </div>
		</div>
		
		</div> 
<script type="text/javascript">														
	require(['ajax/dashboard'],function(){

		$('#pfactura').on("change",function (e) { 
			console.log("change "+e.val);
			if(confirm('Esta seguro de comparar estos periodos?')){
				window.open(ENV_WEBROOT_FULL_URL+'dashboard/comparar_periodo/'+e.val+'/'+$('#pfactura2').val(),'_top');
			}
		});

		$('#pfactura2').on("change",function (e) { 
			console.log("change "+e.val);
			if(confirm('Esta seguro de comparar estos periodos?')){
				window.open(ENV_WEBROOT_FULL_URL+'dashboard/comparar_periodo/'+$('#pfactura').val()+'/'+e.val,'_top');
			}
		});
		
	});
</script>