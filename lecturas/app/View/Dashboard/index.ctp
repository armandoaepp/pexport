<?php 
if (!isset($NEGOCIO)) {
    header("Location:".ENV_WEBROOT_FULL_URL."/Usuarios/logout");
    die();
}
?>
<?php 
if (isset($id_usuario) && $id_usuario == 25){
	$permiso_lectura = 'true';
}else {
	$permiso_lectura = 'true';
}
echo $this->element('menu',array('active'=>'monitorearlectura', 'open'=>'true', 'permiso_lectura' =>$permiso_lectura, 'id_usuario ' =>isset($id_usuario)?$id_usuario:null));

if(isset($unidad_neg) && isset($idciclo)){
	$url_completa = '/'.$unidad_neg.'/'.$idciclo;
}else{
	$url_completa = '';
}
?>
<script type="text/javascript">														
	require(['ajax/dashboard']);
</script>

<div class="container-fluid" id="pcont">

	<div class="page-head" style="padding-bottom: 6px;">
		<div class="row">
			<div class="col-xs-2">
				<h3 style="margin-top: 0px">Filtros:</h3>
			</div>
		  <div class="col-xs-2">
		    <label class="control-label" style="padding-top: 8px;">Por Unidad Negocio:</label>
		  </div>
		  <div class="col-xs-2">              
              	<?php 
	        			  	echo '<select class="" name="unidadneg" id="unidadneg">
	        			  	<option value="0">Todos</option>';
                                             //   var_dump($listar_unidadneg);exit;
	        				foreach ($listar_unidadneg as $v){
		        				if($unidad_neg==$v['0']['unidadneg']){
		        					echo '<option value='.$v['0']['unidadneg'].' selected>'.$v['0']['nombreunidadnegocio'].'</option>';
		        				}else{
		        					echo '<option value='.$v['0']['unidadneg'].'>'.$v['0']['nombreunidadnegocio'].'</option>';
		        				}
	        				}
	        				echo '</select>';	
	        		?> 
		  </div>
		  <div class="col-xs-1">
		    <label class="control-label" style="padding-top: 8px;">Por Ciclo:<span class="ciclo_loading" style="display:none;"><img src="<?php echo ENV_WEBROOT_FULL_URL ?>/js/jquery.select2/select2-spinner.gif"></span></label>
		  </div>
		  <div class="col-xs-2">              
              	<?php 
        			  	echo '<select class="" name="id_ciclo" id="id_ciclo">
        			  	<option value="0">Todos</option>';
        				foreach ($listar_ciclo as $v){
							$str_ciclo_actual = '';
							foreach ($arr_ciclos_actuales as $ca){
								if($v['0']['idciclo']==$ca['0']['idciclo']){
									$str_ciclo_actual = ' (Actual)';
								}
							}
							if($idciclo==$v['0']['idciclo']){
								echo '<option value='.$v['0']['idciclo'].' selected>'.$v['0']['idciclo'].' - '.$v['0']['nombciclo'].$str_ciclo_actual.'</option>';
							}else{
								echo '<option value='.$v['0']['idciclo'].'>'.$v['0']['idciclo'].' - '.$v['0']['nombciclo'].$str_ciclo_actual.'</option>';
							}
        				}
        				echo '</select>';	
        		?> 
		  </div>
		  <div class="col-xs-2">
		    <button class="btn btn-primary" id="btn_reload_dashboard">Buscar</button>
		  </div>
		</div>
    </div>

	<h2 class="text-center" style="margin-top: 5px;">DASHBOARD DE CONTROL DE LECTURAS</h2>

	<div class='row'>
	
		<div class='col-sm-12 col-md-12'>
			<div class='block-flat'>
				<div class='content overflow-hidden'>
					<iframe class='preview-pane'
						src='<?php echo ENV_WEBROOT_FULL_URL;?>dashboard/monitor_progreso_semi_pie/false<?php echo $url_completa;?>' width='100%'
						height='245' frameborder='0' scrolling='no'></iframe>
				</div>
			</div>
		</div>

		<div class='col-sm-6 col-md-6'>

			<div class='block-flat'>
				<div class='header'>
					<h3>KPI 1 - Indicadores Clave de Proceso</h3>
				</div>
				<div class='content'>
					<div class='content no-padding'>
					<table class='red'>
						<thead>
							<tr>
								<th></th>
								<th class='right'><span></span>D&iacute;a Actual</th>
								<th class='right'><span></span>Periodo Actual</th>
								<th class='right'><span></span>KPI</th>
								<th class='right'><span></span>Periodo Anterior</th>
								<th class='right'><span></span>KPI</th>
							</tr>
						</thead>
						<tbody class='no-border-x'>
							<tr>
								<td style='width: 40%;'><i class='fa fa-sitemap'></i> Suministros</td>
								<td class='text-right'><?php echo number_format($cantidad_ordenes_dia_actual);?></td>
								<td class='text-right'><?php echo number_format($cantidad_ordenes_periodo_actual);?></td>
								<td class='text-right'>
								<?php 
								if($cantidad_inconsistentes_periodo_actual==0){
									echo "<img src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAB8ElEQVR42mNgoDb4/OfD/9UPpv6vv5z4v+C83/+sM17/c0+F/595o/f/u+/v/uPVfOHrgf8114L+L33f+H/j997/s98V/O95Hvu/5UHI/5wzLv+dtuv+3/Vy23+cmuvvRvw/9H/h/6U/qv53fgj93/DM93/VA/f/Jbcd/xdctfmffcH8v+kaqf/bnqMZAnJ22RW///v+zP0/52f+/76vUXADah97gQ3IvWz1P/OcKRgrLZNG9c6K+1P+z3lZDrcZhFtfB4E1V9xz/V980wFsQNoZ4/9xJ/T/W62T+d9+oh1hQOm5iP8bPvX9b30ZCMfozgfZnAjUHHlM+7/HdpX/NkusEQYknHACuwCkAWQrzGYQBmkGYZjtQYc0/1tvlv0vN1kSYUDkYdv/Ex6lgG1DxjDNyLb77FcDB6T0BAmEATGHvP933ouCa4BhUKiDMLJmj70q/7Xmi/63mm2JMKD7Yis4nmGhjIzjoJoDD2qANYNsl53C+79lbwvCAFCUGG5QBfsTZBsMgzSC/AyzGeR3ldlC/yXbRDFT5bon68DxC9OErNFpjyLYZpBmgWbO/+uurcOeGtc8WPNfbr44OJ6dtyuCMUij1kJRsLPFW0Vwa4aBF19f/K/eW/3faLbBf9FuATA2mqz/v2ZrzX+QHNVzLwAjy7LsIPIjLQAAAABJRU5ErkJggg=='>";
								}elseif($cantidad_inconsistentes_periodo_actual>0){
									echo "<img src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAABrElEQVR42q2TyytEURzH/QFW1JRHIxsTUiNWHgtCFOWRPEpZyAJZkKSZhccgsVBIFqRkZdKUZCFFNLEQJUnJQk0eGcQCk+bj/O6dyZ3rsXLr272dcz7f3/d3zrlRUf/98PIIK1Mw0gzOKnCU6+/FUYJPD/wNH23DcB14F+DcA7sz4B5ScK8+XmuDvXV+h6da4GkfzpZhywWePljqhLlWmGiEwWoCFdbvJlpsNcm1MjmZVwnGfzaQdpT8RdbIdrSe16fhcFEHRWsOtRfdsNCuJ5MWxKCrgMeadPzTowYDp5o8dutQWAKb4tNTqhm8NmdxWZVnMFCDbM3ogFECTzZFVKctl7eGDE6zEwwGalDbbQHCkthSOQyHqstaaeHYHmcw6CjSj0oAoyS2CaY1i6vCJI7Kcr4MgrMD+mK5MEYJaIKlusS/GHMZDNSRvFWmfFUzgyE4UJ/GZW48ezYLAf+96S5srvJckhgJhUCRFBDYmxyNf83z820Mbrjx5cVpMeWoZLfl26cujsTeSbH8Doefj7sbfCMOToozOUyN5cAWw0G+nbN+J+93t/z73/sJB/zttLkU5gUAAAAASUVORK5CYII='>";
								}else{
									echo "<img src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAABpUlEQVR42q2TyytEURzH/QFWRCFTVhJF2WFla0XJwkKxlJ2VRxI1lA2RlRRWnqVGiRqPmJJXYqKGPEOGYdIwg/Nxzpm55ozByq1v3c6538fve85NSPjvhzcfnPTBYQ3slyP2ymC3Ajx21N7fZN8S4qgKAkO8M4N4HQBvB9w26nXhzIabOX4lc1qHn3WCjMFHJ7w0S4EGvc5JtRYJztviRXRsGTeAU5KH+KALQh0xAsJTiXCXadyN22S6h6iImlk89krX4bCz5f7YKN3qowK6j1IeHLncu+yGgCopNBEmWVBkI75KqMgKgeVCPCMlpkBppLCGWFzXaugC3WF3tovxL+aw05NmCMhF3XaE8AVVXGR2y53NQj1CrIDakEelo5pQsY3o2kgKnI3a2OovigqIg/boxyYsokH2ObJw96fgcXQaAvJI1FxWy3HECPllKY/j4XRW2lIJPXu/3YWLaZ5mM+IcLShnRV5rTcS7O/3zbRSnk5yPpOk0odV87ajeL8czdWxnU+rvZOt5f77haqGZvcECNuzJuNqTcHUX4J5qIei/5d//3k/ScyAkq+CgnAAAAABJRU5ErkJggg=='>";
								}
								?>
								</td>
								<td class='text-right'><?php echo number_format($cantidad_ordenes_periodo_anterior);?></td>
								<td class='text-right'>
								<?php 
								if($cantidad_inconsistentes_periodo_anterior==0){
									echo "<img src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAB8ElEQVR42mNgoDb4/OfD/9UPpv6vv5z4v+C83/+sM17/c0+F/595o/f/u+/v/uPVfOHrgf8114L+L33f+H/j997/s98V/O95Hvu/5UHI/5wzLv+dtuv+3/Vy23+cmuvvRvw/9H/h/6U/qv53fgj93/DM93/VA/f/Jbcd/xdctfmffcH8v+kaqf/bnqMZAnJ22RW///v+zP0/52f+/76vUXADah97gQ3IvWz1P/OcKRgrLZNG9c6K+1P+z3lZDrcZhFtfB4E1V9xz/V980wFsQNoZ4/9xJ/T/W62T+d9+oh1hQOm5iP8bPvX9b30ZCMfozgfZnAjUHHlM+7/HdpX/NkusEQYknHACuwCkAWQrzGYQBmkGYZjtQYc0/1tvlv0vN1kSYUDkYdv/Ex6lgG1DxjDNyLb77FcDB6T0BAmEATGHvP933ouCa4BhUKiDMLJmj70q/7Xmi/63mm2JMKD7Yis4nmGhjIzjoJoDD2qANYNsl53C+79lbwvCAFCUGG5QBfsTZBsMgzSC/AyzGeR3ldlC/yXbRDFT5bon68DxC9OErNFpjyLYZpBmgWbO/+uurcOeGtc8WPNfbr44OJ6dtyuCMUij1kJRsLPFW0Vwa4aBF19f/K/eW/3faLbBf9FuATA2mqz/v2ZrzX+QHNVzLwAjy7LsIPIjLQAAAABJRU5ErkJggg=='>";
								}elseif($cantidad_inconsistentes_periodo_anterior>0){
									echo "<img src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAABrElEQVR42q2TyytEURzH/QFW1JRHIxsTUiNWHgtCFOWRPEpZyAJZkKSZhccgsVBIFqRkZdKUZCFFNLEQJUnJQk0eGcQCk+bj/O6dyZ3rsXLr272dcz7f3/d3zrlRUf/98PIIK1Mw0gzOKnCU6+/FUYJPD/wNH23DcB14F+DcA7sz4B5ScK8+XmuDvXV+h6da4GkfzpZhywWePljqhLlWmGiEwWoCFdbvJlpsNcm1MjmZVwnGfzaQdpT8RdbIdrSe16fhcFEHRWsOtRfdsNCuJ5MWxKCrgMeadPzTowYDp5o8dutQWAKb4tNTqhm8NmdxWZVnMFCDbM3ogFECTzZFVKctl7eGDE6zEwwGalDbbQHCkthSOQyHqstaaeHYHmcw6CjSj0oAoyS2CaY1i6vCJI7Kcr4MgrMD+mK5MEYJaIKlusS/GHMZDNSRvFWmfFUzgyE4UJ/GZW48ezYLAf+96S5srvJckhgJhUCRFBDYmxyNf83z820Mbrjx5cVpMeWoZLfl26cujsTeSbH8Doefj7sbfCMOToozOUyN5cAWw0G+nbN+J+93t/z73/sJB/zttLkU5gUAAAAASUVORK5CYII='>";
								}else{
									echo "<img src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAABpUlEQVR42q2TyytEURzH/QFWRCFTVhJF2WFla0XJwkKxlJ2VRxI1lA2RlRRWnqVGiRqPmJJXYqKGPEOGYdIwg/Nxzpm55ozByq1v3c6538fve85NSPjvhzcfnPTBYQ3slyP2ymC3Ajx21N7fZN8S4qgKAkO8M4N4HQBvB9w26nXhzIabOX4lc1qHn3WCjMFHJ7w0S4EGvc5JtRYJztviRXRsGTeAU5KH+KALQh0xAsJTiXCXadyN22S6h6iImlk89krX4bCz5f7YKN3qowK6j1IeHLncu+yGgCopNBEmWVBkI75KqMgKgeVCPCMlpkBppLCGWFzXaugC3WF3tovxL+aw05NmCMhF3XaE8AVVXGR2y53NQj1CrIDakEelo5pQsY3o2kgKnI3a2OovigqIg/boxyYsokH2ObJw96fgcXQaAvJI1FxWy3HECPllKY/j4XRW2lIJPXu/3YWLaZ5mM+IcLShnRV5rTcS7O/3zbRSnk5yPpOk0odV87ajeL8czdWxnU+rvZOt5f77haqGZvcECNuzJuNqTcHUX4J5qIei/5d//3k/ScyAkq+CgnAAAAABJRU5ErkJggg=='>";
								}
								?>
								</td>
							</tr>
							<tr>
								<td style='width: 40%;'><i class='fa fa-sitemap'></i> Asignadas</td>
								<td class='text-right'><?php echo number_format($cantidad_asignados_dia_actual);?></td>
								<td class='text-right'><?php echo number_format($cantidad_asignados_periodo_actual);?></td>
								<td class='text-right'>
								<?php 
								if($cantidad_ordenes_periodo_actual==$cantidad_asignados_periodo_actual){
									echo "<img src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAB8ElEQVR42mNgoDb4/OfD/9UPpv6vv5z4v+C83/+sM17/c0+F/595o/f/u+/v/uPVfOHrgf8114L+L33f+H/j997/s98V/O95Hvu/5UHI/5wzLv+dtuv+3/Vy23+cmuvvRvw/9H/h/6U/qv53fgj93/DM93/VA/f/Jbcd/xdctfmffcH8v+kaqf/bnqMZAnJ22RW///v+zP0/52f+/76vUXADah97gQ3IvWz1P/OcKRgrLZNG9c6K+1P+z3lZDrcZhFtfB4E1V9xz/V980wFsQNoZ4/9xJ/T/W62T+d9+oh1hQOm5iP8bPvX9b30ZCMfozgfZnAjUHHlM+7/HdpX/NkusEQYknHACuwCkAWQrzGYQBmkGYZjtQYc0/1tvlv0vN1kSYUDkYdv/Ex6lgG1DxjDNyLb77FcDB6T0BAmEATGHvP933ouCa4BhUKiDMLJmj70q/7Xmi/63mm2JMKD7Yis4nmGhjIzjoJoDD2qANYNsl53C+79lbwvCAFCUGG5QBfsTZBsMgzSC/AyzGeR3ldlC/yXbRDFT5bon68DxC9OErNFpjyLYZpBmgWbO/+uurcOeGtc8WPNfbr44OJ6dtyuCMUij1kJRsLPFW0Vwa4aBF19f/K/eW/3faLbBf9FuATA2mqz/v2ZrzX+QHNVzLwAjy7LsIPIjLQAAAABJRU5ErkJggg=='>";
								}elseif($cantidad_asignados_periodo_actual<$cantidad_ordenes_periodo_actual){
									echo "<img src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAABrElEQVR42q2TyytEURzH/QFW1JRHIxsTUiNWHgtCFOWRPEpZyAJZkKSZhccgsVBIFqRkZdKUZCFFNLEQJUnJQk0eGcQCk+bj/O6dyZ3rsXLr272dcz7f3/d3zrlRUf/98PIIK1Mw0gzOKnCU6+/FUYJPD/wNH23DcB14F+DcA7sz4B5ScK8+XmuDvXV+h6da4GkfzpZhywWePljqhLlWmGiEwWoCFdbvJlpsNcm1MjmZVwnGfzaQdpT8RdbIdrSe16fhcFEHRWsOtRfdsNCuJ5MWxKCrgMeadPzTowYDp5o8dutQWAKb4tNTqhm8NmdxWZVnMFCDbM3ogFECTzZFVKctl7eGDE6zEwwGalDbbQHCkthSOQyHqstaaeHYHmcw6CjSj0oAoyS2CaY1i6vCJI7Kcr4MgrMD+mK5MEYJaIKlusS/GHMZDNSRvFWmfFUzgyE4UJ/GZW48ezYLAf+96S5srvJckhgJhUCRFBDYmxyNf83z820Mbrjx5cVpMeWoZLfl26cujsTeSbH8Doefj7sbfCMOToozOUyN5cAWw0G+nbN+J+93t/z73/sJB/zttLkU5gUAAAAASUVORK5CYII='>";
								}else{
									echo "<img src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAABpUlEQVR42q2TyytEURzH/QFWRCFTVhJF2WFla0XJwkKxlJ2VRxI1lA2RlRRWnqVGiRqPmJJXYqKGPEOGYdIwg/Nxzpm55ozByq1v3c6538fve85NSPjvhzcfnPTBYQ3slyP2ymC3Ajx21N7fZN8S4qgKAkO8M4N4HQBvB9w26nXhzIabOX4lc1qHn3WCjMFHJ7w0S4EGvc5JtRYJztviRXRsGTeAU5KH+KALQh0xAsJTiXCXadyN22S6h6iImlk89krX4bCz5f7YKN3qowK6j1IeHLncu+yGgCopNBEmWVBkI75KqMgKgeVCPCMlpkBppLCGWFzXaugC3WF3tovxL+aw05NmCMhF3XaE8AVVXGR2y53NQj1CrIDakEelo5pQsY3o2kgKnI3a2OovigqIg/boxyYsokH2ObJw96fgcXQaAvJI1FxWy3HECPllKY/j4XRW2lIJPXu/3YWLaZ5mM+IcLShnRV5rTcS7O/3zbRSnk5yPpOk0odV87ajeL8czdWxnU+rvZOt5f77haqGZvcECNuzJuNqTcHUX4J5qIei/5d//3k/ScyAkq+CgnAAAAABJRU5ErkJggg=='>";
								}
								?>
								</td>
								<td class='text-right'><?php echo number_format($cantidad_asignados_periodo_anterior);?></td>
								<td class='text-right'>
								<?php 
								if($cantidad_ordenes_periodo_anterior==$cantidad_asignados_periodo_anterior){
									echo "<img src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAB8ElEQVR42mNgoDb4/OfD/9UPpv6vv5z4v+C83/+sM17/c0+F/595o/f/u+/v/uPVfOHrgf8114L+L33f+H/j997/s98V/O95Hvu/5UHI/5wzLv+dtuv+3/Vy23+cmuvvRvw/9H/h/6U/qv53fgj93/DM93/VA/f/Jbcd/xdctfmffcH8v+kaqf/bnqMZAnJ22RW///v+zP0/52f+/76vUXADah97gQ3IvWz1P/OcKRgrLZNG9c6K+1P+z3lZDrcZhFtfB4E1V9xz/V980wFsQNoZ4/9xJ/T/W62T+d9+oh1hQOm5iP8bPvX9b30ZCMfozgfZnAjUHHlM+7/HdpX/NkusEQYknHACuwCkAWQrzGYQBmkGYZjtQYc0/1tvlv0vN1kSYUDkYdv/Ex6lgG1DxjDNyLb77FcDB6T0BAmEATGHvP933ouCa4BhUKiDMLJmj70q/7Xmi/63mm2JMKD7Yis4nmGhjIzjoJoDD2qANYNsl53C+79lbwvCAFCUGG5QBfsTZBsMgzSC/AyzGeR3ldlC/yXbRDFT5bon68DxC9OErNFpjyLYZpBmgWbO/+uurcOeGtc8WPNfbr44OJ6dtyuCMUij1kJRsLPFW0Vwa4aBF19f/K/eW/3faLbBf9FuATA2mqz/v2ZrzX+QHNVzLwAjy7LsIPIjLQAAAABJRU5ErkJggg=='>";
								}elseif($cantidad_asignados_periodo_anterior<$cantidad_ordenes_periodo_anterior){
									echo "<img src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAABrElEQVR42q2TyytEURzH/QFW1JRHIxsTUiNWHgtCFOWRPEpZyAJZkKSZhccgsVBIFqRkZdKUZCFFNLEQJUnJQk0eGcQCk+bj/O6dyZ3rsXLr272dcz7f3/d3zrlRUf/98PIIK1Mw0gzOKnCU6+/FUYJPD/wNH23DcB14F+DcA7sz4B5ScK8+XmuDvXV+h6da4GkfzpZhywWePljqhLlWmGiEwWoCFdbvJlpsNcm1MjmZVwnGfzaQdpT8RdbIdrSe16fhcFEHRWsOtRfdsNCuJ5MWxKCrgMeadPzTowYDp5o8dutQWAKb4tNTqhm8NmdxWZVnMFCDbM3ogFECTzZFVKctl7eGDE6zEwwGalDbbQHCkthSOQyHqstaaeHYHmcw6CjSj0oAoyS2CaY1i6vCJI7Kcr4MgrMD+mK5MEYJaIKlusS/GHMZDNSRvFWmfFUzgyE4UJ/GZW48ezYLAf+96S5srvJckhgJhUCRFBDYmxyNf83z820Mbrjx5cVpMeWoZLfl26cujsTeSbH8Doefj7sbfCMOToozOUyN5cAWw0G+nbN+J+93t/z73/sJB/zttLkU5gUAAAAASUVORK5CYII='>";
								}else{
									echo "<img src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAABpUlEQVR42q2TyytEURzH/QFWRCFTVhJF2WFla0XJwkKxlJ2VRxI1lA2RlRRWnqVGiRqPmJJXYqKGPEOGYdIwg/Nxzpm55ozByq1v3c6538fve85NSPjvhzcfnPTBYQ3slyP2ymC3Ajx21N7fZN8S4qgKAkO8M4N4HQBvB9w26nXhzIabOX4lc1qHn3WCjMFHJ7w0S4EGvc5JtRYJztviRXRsGTeAU5KH+KALQh0xAsJTiXCXadyN22S6h6iImlk89krX4bCz5f7YKN3qowK6j1IeHLncu+yGgCopNBEmWVBkI75KqMgKgeVCPCMlpkBppLCGWFzXaugC3WF3tovxL+aw05NmCMhF3XaE8AVVXGR2y53NQj1CrIDakEelo5pQsY3o2kgKnI3a2OovigqIg/boxyYsokH2ObJw96fgcXQaAvJI1FxWy3HECPllKY/j4XRW2lIJPXu/3YWLaZ5mM+IcLShnRV5rTcS7O/3zbRSnk5yPpOk0odV87ajeL8czdWxnU+rvZOt5f77haqGZvcECNuzJuNqTcHUX4J5qIei/5d//3k/ScyAkq+CgnAAAAABJRU5ErkJggg=='>";
								}
								?>
								</td>
							</tr>
							<tr>
								<td><i class='fa fa-tasks'></i> Descargadas</td>
								<td class='text-right'><?php echo number_format($cantidad_descargados_dia_actual);?></td>
								<td class='text-right'><?php echo number_format($cantidad_descargados_periodo_actual);?></td>
								<td class='text-right'>
								<?php 
								if($cantidad_ordenes_periodo_actual==$cantidad_descargados_periodo_actual){
									echo "<img src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAB8ElEQVR42mNgoDb4/OfD/9UPpv6vv5z4v+C83/+sM17/c0+F/595o/f/u+/v/uPVfOHrgf8114L+L33f+H/j997/s98V/O95Hvu/5UHI/5wzLv+dtuv+3/Vy23+cmuvvRvw/9H/h/6U/qv53fgj93/DM93/VA/f/Jbcd/xdctfmffcH8v+kaqf/bnqMZAnJ22RW///v+zP0/52f+/76vUXADah97gQ3IvWz1P/OcKRgrLZNG9c6K+1P+z3lZDrcZhFtfB4E1V9xz/V980wFsQNoZ4/9xJ/T/W62T+d9+oh1hQOm5iP8bPvX9b30ZCMfozgfZnAjUHHlM+7/HdpX/NkusEQYknHACuwCkAWQrzGYQBmkGYZjtQYc0/1tvlv0vN1kSYUDkYdv/Ex6lgG1DxjDNyLb77FcDB6T0BAmEATGHvP933ouCa4BhUKiDMLJmj70q/7Xmi/63mm2JMKD7Yis4nmGhjIzjoJoDD2qANYNsl53C+79lbwvCAFCUGG5QBfsTZBsMgzSC/AyzGeR3ldlC/yXbRDFT5bon68DxC9OErNFpjyLYZpBmgWbO/+uurcOeGtc8WPNfbr44OJ6dtyuCMUij1kJRsLPFW0Vwa4aBF19f/K/eW/3faLbBf9FuATA2mqz/v2ZrzX+QHNVzLwAjy7LsIPIjLQAAAABJRU5ErkJggg=='>";
								}elseif($cantidad_descargados_periodo_actual<$cantidad_ordenes_periodo_actual){
									echo "<img src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAABrElEQVR42q2TyytEURzH/QFW1JRHIxsTUiNWHgtCFOWRPEpZyAJZkKSZhccgsVBIFqRkZdKUZCFFNLEQJUnJQk0eGcQCk+bj/O6dyZ3rsXLr272dcz7f3/d3zrlRUf/98PIIK1Mw0gzOKnCU6+/FUYJPD/wNH23DcB14F+DcA7sz4B5ScK8+XmuDvXV+h6da4GkfzpZhywWePljqhLlWmGiEwWoCFdbvJlpsNcm1MjmZVwnGfzaQdpT8RdbIdrSe16fhcFEHRWsOtRfdsNCuJ5MWxKCrgMeadPzTowYDp5o8dutQWAKb4tNTqhm8NmdxWZVnMFCDbM3ogFECTzZFVKctl7eGDE6zEwwGalDbbQHCkthSOQyHqstaaeHYHmcw6CjSj0oAoyS2CaY1i6vCJI7Kcr4MgrMD+mK5MEYJaIKlusS/GHMZDNSRvFWmfFUzgyE4UJ/GZW48ezYLAf+96S5srvJckhgJhUCRFBDYmxyNf83z820Mbrjx5cVpMeWoZLfl26cujsTeSbH8Doefj7sbfCMOToozOUyN5cAWw0G+nbN+J+93t/z73/sJB/zttLkU5gUAAAAASUVORK5CYII='>";
								}else{
									echo "<img src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAABpUlEQVR42q2TyytEURzH/QFWRCFTVhJF2WFla0XJwkKxlJ2VRxI1lA2RlRRWnqVGiRqPmJJXYqKGPEOGYdIwg/Nxzpm55ozByq1v3c6538fve85NSPjvhzcfnPTBYQ3slyP2ymC3Ajx21N7fZN8S4qgKAkO8M4N4HQBvB9w26nXhzIabOX4lc1qHn3WCjMFHJ7w0S4EGvc5JtRYJztviRXRsGTeAU5KH+KALQh0xAsJTiXCXadyN22S6h6iImlk89krX4bCz5f7YKN3qowK6j1IeHLncu+yGgCopNBEmWVBkI75KqMgKgeVCPCMlpkBppLCGWFzXaugC3WF3tovxL+aw05NmCMhF3XaE8AVVXGR2y53NQj1CrIDakEelo5pQsY3o2kgKnI3a2OovigqIg/boxyYsokH2ObJw96fgcXQaAvJI1FxWy3HECPllKY/j4XRW2lIJPXu/3YWLaZ5mM+IcLShnRV5rTcS7O/3zbRSnk5yPpOk0odV87ajeL8czdWxnU+rvZOt5f77haqGZvcECNuzJuNqTcHUX4J5qIei/5d//3k/ScyAkq+CgnAAAAABJRU5ErkJggg=='>";
								}
								?>
								</td>
								<td class='text-right'><?php echo number_format($cantidad_descargados_periodo_anterior);?></td>
								<td class='text-right'>
								<?php 
								if($cantidad_ordenes_periodo_anterior==$cantidad_descargados_periodo_anterior){
									echo "<img src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAB8ElEQVR42mNgoDb4/OfD/9UPpv6vv5z4v+C83/+sM17/c0+F/595o/f/u+/v/uPVfOHrgf8114L+L33f+H/j997/s98V/O95Hvu/5UHI/5wzLv+dtuv+3/Vy23+cmuvvRvw/9H/h/6U/qv53fgj93/DM93/VA/f/Jbcd/xdctfmffcH8v+kaqf/bnqMZAnJ22RW///v+zP0/52f+/76vUXADah97gQ3IvWz1P/OcKRgrLZNG9c6K+1P+z3lZDrcZhFtfB4E1V9xz/V980wFsQNoZ4/9xJ/T/W62T+d9+oh1hQOm5iP8bPvX9b30ZCMfozgfZnAjUHHlM+7/HdpX/NkusEQYknHACuwCkAWQrzGYQBmkGYZjtQYc0/1tvlv0vN1kSYUDkYdv/Ex6lgG1DxjDNyLb77FcDB6T0BAmEATGHvP933ouCa4BhUKiDMLJmj70q/7Xmi/63mm2JMKD7Yis4nmGhjIzjoJoDD2qANYNsl53C+79lbwvCAFCUGG5QBfsTZBsMgzSC/AyzGeR3ldlC/yXbRDFT5bon68DxC9OErNFpjyLYZpBmgWbO/+uurcOeGtc8WPNfbr44OJ6dtyuCMUij1kJRsLPFW0Vwa4aBF19f/K/eW/3faLbBf9FuATA2mqz/v2ZrzX+QHNVzLwAjy7LsIPIjLQAAAABJRU5ErkJggg=='>";
								}elseif($cantidad_descargados_periodo_anterior<$cantidad_ordenes_periodo_anterior){
									echo "<img src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAABrElEQVR42q2TyytEURzH/QFW1JRHIxsTUiNWHgtCFOWRPEpZyAJZkKSZhccgsVBIFqRkZdKUZCFFNLEQJUnJQk0eGcQCk+bj/O6dyZ3rsXLr272dcz7f3/d3zrlRUf/98PIIK1Mw0gzOKnCU6+/FUYJPD/wNH23DcB14F+DcA7sz4B5ScK8+XmuDvXV+h6da4GkfzpZhywWePljqhLlWmGiEwWoCFdbvJlpsNcm1MjmZVwnGfzaQdpT8RdbIdrSe16fhcFEHRWsOtRfdsNCuJ5MWxKCrgMeadPzTowYDp5o8dutQWAKb4tNTqhm8NmdxWZVnMFCDbM3ogFECTzZFVKctl7eGDE6zEwwGalDbbQHCkthSOQyHqstaaeHYHmcw6CjSj0oAoyS2CaY1i6vCJI7Kcr4MgrMD+mK5MEYJaIKlusS/GHMZDNSRvFWmfFUzgyE4UJ/GZW48ezYLAf+96S5srvJckhgJhUCRFBDYmxyNf83z820Mbrjx5cVpMeWoZLfl26cujsTeSbH8Doefj7sbfCMOToozOUyN5cAWw0G+nbN+J+93t/z73/sJB/zttLkU5gUAAAAASUVORK5CYII='>";
								}else{
									echo "<img src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAABpUlEQVR42q2TyytEURzH/QFWRCFTVhJF2WFla0XJwkKxlJ2VRxI1lA2RlRRWnqVGiRqPmJJXYqKGPEOGYdIwg/Nxzpm55ozByq1v3c6538fve85NSPjvhzcfnPTBYQ3slyP2ymC3Ajx21N7fZN8S4qgKAkO8M4N4HQBvB9w26nXhzIabOX4lc1qHn3WCjMFHJ7w0S4EGvc5JtRYJztviRXRsGTeAU5KH+KALQh0xAsJTiXCXadyN22S6h6iImlk89krX4bCz5f7YKN3qowK6j1IeHLncu+yGgCopNBEmWVBkI75KqMgKgeVCPCMlpkBppLCGWFzXaugC3WF3tovxL+aw05NmCMhF3XaE8AVVXGR2y53NQj1CrIDakEelo5pQsY3o2kgKnI3a2OovigqIg/boxyYsokH2ObJw96fgcXQaAvJI1FxWy3HECPllKY/j4XRW2lIJPXu/3YWLaZ5mM+IcLShnRV5rTcS7O/3zbRSnk5yPpOk0odV87ajeL8czdWxnU+rvZOt5f77haqGZvcECNuzJuNqTcHUX4J5qIei/5d//3k/ScyAkq+CgnAAAAABJRU5ErkJggg=='>";
								}
								?>
								</td>
							</tr>
							<tr>
								<td><i class='fa fa-signal'></i> Finalizadas</td>
								<td class='text-right'><?php echo number_format($cantidad_avanzados_dia_actual);?></td>
								<td class='text-right'><?php echo number_format($cantidad_avanzados_periodo_actual);?></td>
								<td class='text-right'>
								<?php 
								if($cantidad_ordenes_periodo_actual==$cantidad_avanzados_periodo_actual){
									echo "<img src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAB8ElEQVR42mNgoDb4/OfD/9UPpv6vv5z4v+C83/+sM17/c0+F/595o/f/u+/v/uPVfOHrgf8114L+L33f+H/j997/s98V/O95Hvu/5UHI/5wzLv+dtuv+3/Vy23+cmuvvRvw/9H/h/6U/qv53fgj93/DM93/VA/f/Jbcd/xdctfmffcH8v+kaqf/bnqMZAnJ22RW///v+zP0/52f+/76vUXADah97gQ3IvWz1P/OcKRgrLZNG9c6K+1P+z3lZDrcZhFtfB4E1V9xz/V980wFsQNoZ4/9xJ/T/W62T+d9+oh1hQOm5iP8bPvX9b30ZCMfozgfZnAjUHHlM+7/HdpX/NkusEQYknHACuwCkAWQrzGYQBmkGYZjtQYc0/1tvlv0vN1kSYUDkYdv/Ex6lgG1DxjDNyLb77FcDB6T0BAmEATGHvP933ouCa4BhUKiDMLJmj70q/7Xmi/63mm2JMKD7Yis4nmGhjIzjoJoDD2qANYNsl53C+79lbwvCAFCUGG5QBfsTZBsMgzSC/AyzGeR3ldlC/yXbRDFT5bon68DxC9OErNFpjyLYZpBmgWbO/+uurcOeGtc8WPNfbr44OJ6dtyuCMUij1kJRsLPFW0Vwa4aBF19f/K/eW/3faLbBf9FuATA2mqz/v2ZrzX+QHNVzLwAjy7LsIPIjLQAAAABJRU5ErkJggg=='>";
								}elseif($cantidad_avanzados_periodo_actual<$cantidad_ordenes_periodo_actual){
									echo "<img src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAABrElEQVR42q2TyytEURzH/QFW1JRHIxsTUiNWHgtCFOWRPEpZyAJZkKSZhccgsVBIFqRkZdKUZCFFNLEQJUnJQk0eGcQCk+bj/O6dyZ3rsXLr272dcz7f3/d3zrlRUf/98PIIK1Mw0gzOKnCU6+/FUYJPD/wNH23DcB14F+DcA7sz4B5ScK8+XmuDvXV+h6da4GkfzpZhywWePljqhLlWmGiEwWoCFdbvJlpsNcm1MjmZVwnGfzaQdpT8RdbIdrSe16fhcFEHRWsOtRfdsNCuJ5MWxKCrgMeadPzTowYDp5o8dutQWAKb4tNTqhm8NmdxWZVnMFCDbM3ogFECTzZFVKctl7eGDE6zEwwGalDbbQHCkthSOQyHqstaaeHYHmcw6CjSj0oAoyS2CaY1i6vCJI7Kcr4MgrMD+mK5MEYJaIKlusS/GHMZDNSRvFWmfFUzgyE4UJ/GZW48ezYLAf+96S5srvJckhgJhUCRFBDYmxyNf83z820Mbrjx5cVpMeWoZLfl26cujsTeSbH8Doefj7sbfCMOToozOUyN5cAWw0G+nbN+J+93t/z73/sJB/zttLkU5gUAAAAASUVORK5CYII='>";
								}else{
									echo "<img src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAABpUlEQVR42q2TyytEURzH/QFWRCFTVhJF2WFla0XJwkKxlJ2VRxI1lA2RlRRWnqVGiRqPmJJXYqKGPEOGYdIwg/Nxzpm55ozByq1v3c6538fve85NSPjvhzcfnPTBYQ3slyP2ymC3Ajx21N7fZN8S4qgKAkO8M4N4HQBvB9w26nXhzIabOX4lc1qHn3WCjMFHJ7w0S4EGvc5JtRYJztviRXRsGTeAU5KH+KALQh0xAsJTiXCXadyN22S6h6iImlk89krX4bCz5f7YKN3qowK6j1IeHLncu+yGgCopNBEmWVBkI75KqMgKgeVCPCMlpkBppLCGWFzXaugC3WF3tovxL+aw05NmCMhF3XaE8AVVXGR2y53NQj1CrIDakEelo5pQsY3o2kgKnI3a2OovigqIg/boxyYsokH2ObJw96fgcXQaAvJI1FxWy3HECPllKY/j4XRW2lIJPXu/3YWLaZ5mM+IcLShnRV5rTcS7O/3zbRSnk5yPpOk0odV87ajeL8czdWxnU+rvZOt5f77haqGZvcECNuzJuNqTcHUX4J5qIei/5d//3k/ScyAkq+CgnAAAAABJRU5ErkJggg=='>";
								}
								?>
								</td>
								<td class='text-right'><?php echo number_format($cantidad_avanzados_periodo_anterior);?></td>
								<td class='text-right'>
								<?php 
								if($cantidad_ordenes_periodo_anterior==$cantidad_avanzados_periodo_anterior){
									echo "<img src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAB8ElEQVR42mNgoDb4/OfD/9UPpv6vv5z4v+C83/+sM17/c0+F/595o/f/u+/v/uPVfOHrgf8114L+L33f+H/j997/s98V/O95Hvu/5UHI/5wzLv+dtuv+3/Vy23+cmuvvRvw/9H/h/6U/qv53fgj93/DM93/VA/f/Jbcd/xdctfmffcH8v+kaqf/bnqMZAnJ22RW///v+zP0/52f+/76vUXADah97gQ3IvWz1P/OcKRgrLZNG9c6K+1P+z3lZDrcZhFtfB4E1V9xz/V980wFsQNoZ4/9xJ/T/W62T+d9+oh1hQOm5iP8bPvX9b30ZCMfozgfZnAjUHHlM+7/HdpX/NkusEQYknHACuwCkAWQrzGYQBmkGYZjtQYc0/1tvlv0vN1kSYUDkYdv/Ex6lgG1DxjDNyLb77FcDB6T0BAmEATGHvP933ouCa4BhUKiDMLJmj70q/7Xmi/63mm2JMKD7Yis4nmGhjIzjoJoDD2qANYNsl53C+79lbwvCAFCUGG5QBfsTZBsMgzSC/AyzGeR3ldlC/yXbRDFT5bon68DxC9OErNFpjyLYZpBmgWbO/+uurcOeGtc8WPNfbr44OJ6dtyuCMUij1kJRsLPFW0Vwa4aBF19f/K/eW/3faLbBf9FuATA2mqz/v2ZrzX+QHNVzLwAjy7LsIPIjLQAAAABJRU5ErkJggg=='>";
								}elseif($cantidad_avanzados_periodo_anterior<$cantidad_ordenes_periodo_anterior){
									echo "<img src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAABrElEQVR42q2TyytEURzH/QFW1JRHIxsTUiNWHgtCFOWRPEpZyAJZkKSZhccgsVBIFqRkZdKUZCFFNLEQJUnJQk0eGcQCk+bj/O6dyZ3rsXLr272dcz7f3/d3zrlRUf/98PIIK1Mw0gzOKnCU6+/FUYJPD/wNH23DcB14F+DcA7sz4B5ScK8+XmuDvXV+h6da4GkfzpZhywWePljqhLlWmGiEwWoCFdbvJlpsNcm1MjmZVwnGfzaQdpT8RdbIdrSe16fhcFEHRWsOtRfdsNCuJ5MWxKCrgMeadPzTowYDp5o8dutQWAKb4tNTqhm8NmdxWZVnMFCDbM3ogFECTzZFVKctl7eGDE6zEwwGalDbbQHCkthSOQyHqstaaeHYHmcw6CjSj0oAoyS2CaY1i6vCJI7Kcr4MgrMD+mK5MEYJaIKlusS/GHMZDNSRvFWmfFUzgyE4UJ/GZW48ezYLAf+96S5srvJckhgJhUCRFBDYmxyNf83z820Mbrjx5cVpMeWoZLfl26cujsTeSbH8Doefj7sbfCMOToozOUyN5cAWw0G+nbN+J+93t/z73/sJB/zttLkU5gUAAAAASUVORK5CYII='>";
								}else{
									echo "<img src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAABpUlEQVR42q2TyytEURzH/QFWRCFTVhJF2WFla0XJwkKxlJ2VRxI1lA2RlRRWnqVGiRqPmJJXYqKGPEOGYdIwg/Nxzpm55ozByq1v3c6538fve85NSPjvhzcfnPTBYQ3slyP2ymC3Ajx21N7fZN8S4qgKAkO8M4N4HQBvB9w26nXhzIabOX4lc1qHn3WCjMFHJ7w0S4EGvc5JtRYJztviRXRsGTeAU5KH+KALQh0xAsJTiXCXadyN22S6h6iImlk89krX4bCz5f7YKN3qowK6j1IeHLncu+yGgCopNBEmWVBkI75KqMgKgeVCPCMlpkBppLCGWFzXaugC3WF3tovxL+aw05NmCMhF3XaE8AVVXGR2y53NQj1CrIDakEelo5pQsY3o2kgKnI3a2OovigqIg/boxyYsokH2ObJw96fgcXQaAvJI1FxWy3HECPllKY/j4XRW2lIJPXu/3YWLaZ5mM+IcLShnRV5rTcS7O/3zbRSnk5yPpOk0odV87ajeL8czdWxnU+rvZOt5f77haqGZvcECNuzJuNqTcHUX4J5qIei/5d//3k/ScyAkq+CgnAAAAABJRU5ErkJggg=='>";
								}
								?>
								</td>
							</tr>
							<tr>
								<td><i class='fa fa-bolt'></i> Inconsistentes</td>
								<td class='text-right'><a href="<?php echo ENV_WEBROOT_FULL_URL;?>EvaluarInconsistencias/evaluar_inconsistencia<?php echo ($url_completa!=''?'/U/'.date('Y-m-d').'/'.date('Y-m-d').$url_completa:'');?>" target="_blank"><?php echo number_format($cantidad_inconsistentes_dia_actual);?></a> de <?php echo number_format($cantidad_inconsistentes_dia_actual+$cantidad_inconsistentes_evaluadas_dia_actual);?></td>
								<td class='text-right'><a href="<?php echo ENV_WEBROOT_FULL_URL;?>EvaluarInconsistencias/evaluar_inconsistencia<?php echo ($url_completa!=''?'/U/'.date('Y-m-d').'/'.date('Y-m-d').$url_completa:'');?>" target="_blank"><?php echo number_format($cantidad_inconsistentes_periodo_actual);?></a> de <?php echo number_format($cantidad_inconsistentes_periodo_actual+$cantidad_inconsistentes_evaluadas_periodo_actual);?></td>
								<td class='text-right'>
								<?php 
								if($cantidad_inconsistentes_periodo_actual==0){
									echo "<img src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAB8ElEQVR42mNgoDb4/OfD/9UPpv6vv5z4v+C83/+sM17/c0+F/595o/f/u+/v/uPVfOHrgf8114L+L33f+H/j997/s98V/O95Hvu/5UHI/5wzLv+dtuv+3/Vy23+cmuvvRvw/9H/h/6U/qv53fgj93/DM93/VA/f/Jbcd/xdctfmffcH8v+kaqf/bnqMZAnJ22RW///v+zP0/52f+/76vUXADah97gQ3IvWz1P/OcKRgrLZNG9c6K+1P+z3lZDrcZhFtfB4E1V9xz/V980wFsQNoZ4/9xJ/T/W62T+d9+oh1hQOm5iP8bPvX9b30ZCMfozgfZnAjUHHlM+7/HdpX/NkusEQYknHACuwCkAWQrzGYQBmkGYZjtQYc0/1tvlv0vN1kSYUDkYdv/Ex6lgG1DxjDNyLb77FcDB6T0BAmEATGHvP933ouCa4BhUKiDMLJmj70q/7Xmi/63mm2JMKD7Yis4nmGhjIzjoJoDD2qANYNsl53C+79lbwvCAFCUGG5QBfsTZBsMgzSC/AyzGeR3ldlC/yXbRDFT5bon68DxC9OErNFpjyLYZpBmgWbO/+uurcOeGtc8WPNfbr44OJ6dtyuCMUij1kJRsLPFW0Vwa4aBF19f/K/eW/3faLbBf9FuATA2mqz/v2ZrzX+QHNVzLwAjy7LsIPIjLQAAAABJRU5ErkJggg=='>";
								}elseif($cantidad_inconsistentes_periodo_actual>0){
									echo "<img src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAABrElEQVR42q2TyytEURzH/QFW1JRHIxsTUiNWHgtCFOWRPEpZyAJZkKSZhccgsVBIFqRkZdKUZCFFNLEQJUnJQk0eGcQCk+bj/O6dyZ3rsXLr272dcz7f3/d3zrlRUf/98PIIK1Mw0gzOKnCU6+/FUYJPD/wNH23DcB14F+DcA7sz4B5ScK8+XmuDvXV+h6da4GkfzpZhywWePljqhLlWmGiEwWoCFdbvJlpsNcm1MjmZVwnGfzaQdpT8RdbIdrSe16fhcFEHRWsOtRfdsNCuJ5MWxKCrgMeadPzTowYDp5o8dutQWAKb4tNTqhm8NmdxWZVnMFCDbM3ogFECTzZFVKctl7eGDE6zEwwGalDbbQHCkthSOQyHqstaaeHYHmcw6CjSj0oAoyS2CaY1i6vCJI7Kcr4MgrMD+mK5MEYJaIKlusS/GHMZDNSRvFWmfFUzgyE4UJ/GZW48ezYLAf+96S5srvJckhgJhUCRFBDYmxyNf83z820Mbrjx5cVpMeWoZLfl26cujsTeSbH8Doefj7sbfCMOToozOUyN5cAWw0G+nbN+J+93t/z73/sJB/zttLkU5gUAAAAASUVORK5CYII='>";
								}else{
									echo "<img src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAABpUlEQVR42q2TyytEURzH/QFWRCFTVhJF2WFla0XJwkKxlJ2VRxI1lA2RlRRWnqVGiRqPmJJXYqKGPEOGYdIwg/Nxzpm55ozByq1v3c6538fve85NSPjvhzcfnPTBYQ3slyP2ymC3Ajx21N7fZN8S4qgKAkO8M4N4HQBvB9w26nXhzIabOX4lc1qHn3WCjMFHJ7w0S4EGvc5JtRYJztviRXRsGTeAU5KH+KALQh0xAsJTiXCXadyN22S6h6iImlk89krX4bCz5f7YKN3qowK6j1IeHLncu+yGgCopNBEmWVBkI75KqMgKgeVCPCMlpkBppLCGWFzXaugC3WF3tovxL+aw05NmCMhF3XaE8AVVXGR2y53NQj1CrIDakEelo5pQsY3o2kgKnI3a2OovigqIg/boxyYsokH2ObJw96fgcXQaAvJI1FxWy3HECPllKY/j4XRW2lIJPXu/3YWLaZ5mM+IcLShnRV5rTcS7O/3zbRSnk5yPpOk0odV87ajeL8czdWxnU+rvZOt5f77haqGZvcECNuzJuNqTcHUX4J5qIei/5d//3k/ScyAkq+CgnAAAAABJRU5ErkJggg=='>";
								}
								?>
								</td>
								<td class='text-right'><?php echo number_format($cantidad_inconsistentes_periodo_anterior);?> de <?php echo number_format($cantidad_inconsistentes_periodo_anterior+$cantidad_inconsistentes_evaluadas_periodo_anterior);?></td>
								<td class='text-right'>
								<?php 
								if($cantidad_inconsistentes_periodo_anterior==0){
									echo "<img src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAB8ElEQVR42mNgoDb4/OfD/9UPpv6vv5z4v+C83/+sM17/c0+F/595o/f/u+/v/uPVfOHrgf8114L+L33f+H/j997/s98V/O95Hvu/5UHI/5wzLv+dtuv+3/Vy23+cmuvvRvw/9H/h/6U/qv53fgj93/DM93/VA/f/Jbcd/xdctfmffcH8v+kaqf/bnqMZAnJ22RW///v+zP0/52f+/76vUXADah97gQ3IvWz1P/OcKRgrLZNG9c6K+1P+z3lZDrcZhFtfB4E1V9xz/V980wFsQNoZ4/9xJ/T/W62T+d9+oh1hQOm5iP8bPvX9b30ZCMfozgfZnAjUHHlM+7/HdpX/NkusEQYknHACuwCkAWQrzGYQBmkGYZjtQYc0/1tvlv0vN1kSYUDkYdv/Ex6lgG1DxjDNyLb77FcDB6T0BAmEATGHvP933ouCa4BhUKiDMLJmj70q/7Xmi/63mm2JMKD7Yis4nmGhjIzjoJoDD2qANYNsl53C+79lbwvCAFCUGG5QBfsTZBsMgzSC/AyzGeR3ldlC/yXbRDFT5bon68DxC9OErNFpjyLYZpBmgWbO/+uurcOeGtc8WPNfbr44OJ6dtyuCMUij1kJRsLPFW0Vwa4aBF19f/K/eW/3faLbBf9FuATA2mqz/v2ZrzX+QHNVzLwAjy7LsIPIjLQAAAABJRU5ErkJggg=='>";
								}elseif($cantidad_inconsistentes_periodo_anterior>0){
									echo "<img src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAABrElEQVR42q2TyytEURzH/QFW1JRHIxsTUiNWHgtCFOWRPEpZyAJZkKSZhccgsVBIFqRkZdKUZCFFNLEQJUnJQk0eGcQCk+bj/O6dyZ3rsXLr272dcz7f3/d3zrlRUf/98PIIK1Mw0gzOKnCU6+/FUYJPD/wNH23DcB14F+DcA7sz4B5ScK8+XmuDvXV+h6da4GkfzpZhywWePljqhLlWmGiEwWoCFdbvJlpsNcm1MjmZVwnGfzaQdpT8RdbIdrSe16fhcFEHRWsOtRfdsNCuJ5MWxKCrgMeadPzTowYDp5o8dutQWAKb4tNTqhm8NmdxWZVnMFCDbM3ogFECTzZFVKctl7eGDE6zEwwGalDbbQHCkthSOQyHqstaaeHYHmcw6CjSj0oAoyS2CaY1i6vCJI7Kcr4MgrMD+mK5MEYJaIKlusS/GHMZDNSRvFWmfFUzgyE4UJ/GZW48ezYLAf+96S5srvJckhgJhUCRFBDYmxyNf83z820Mbrjx5cVpMeWoZLfl26cujsTeSbH8Doefj7sbfCMOToozOUyN5cAWw0G+nbN+J+93t/z73/sJB/zttLkU5gUAAAAASUVORK5CYII='>";
								}else{
									echo "<img src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAABpUlEQVR42q2TyytEURzH/QFWRCFTVhJF2WFla0XJwkKxlJ2VRxI1lA2RlRRWnqVGiRqPmJJXYqKGPEOGYdIwg/Nxzpm55ozByq1v3c6538fve85NSPjvhzcfnPTBYQ3slyP2ymC3Ajx21N7fZN8S4qgKAkO8M4N4HQBvB9w26nXhzIabOX4lc1qHn3WCjMFHJ7w0S4EGvc5JtRYJztviRXRsGTeAU5KH+KALQh0xAsJTiXCXadyN22S6h6iImlk89krX4bCz5f7YKN3qowK6j1IeHLncu+yGgCopNBEmWVBkI75KqMgKgeVCPCMlpkBppLCGWFzXaugC3WF3tovxL+aw05NmCMhF3XaE8AVVXGR2y53NQj1CrIDakEelo5pQsY3o2kgKnI3a2OovigqIg/boxyYsokH2ObJw96fgcXQaAvJI1FxWy3HECPllKY/j4XRW2lIJPXu/3YWLaZ5mM+IcLShnRV5rTcS7O/3zbRSnk5yPpOk0odV87ajeL8czdWxnU+rvZOt5f77haqGZvcECNuzJuNqTcHUX4J5qIei/5d//3k/ScyAkq+CgnAAAAABJRU5ErkJggg=='>";
								}
								?>
								</td>
							</tr>
							<tr>
								<td><i class='fa fa-tasks'></i> Consistentes</td>
								<td class='text-right'><?php echo number_format($cantidad_consistentes_dia_actual);?></td>
								<td class='text-right'><?php echo number_format($cantidad_consistentes_periodo_actual);?></td>
								<td class='text-right'>
								<?php 
								if($cantidad_ordenes_periodo_actual==$cantidad_consistentes_periodo_actual){
									echo "<img src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAB8ElEQVR42mNgoDb4/OfD/9UPpv6vv5z4v+C83/+sM17/c0+F/595o/f/u+/v/uPVfOHrgf8114L+L33f+H/j997/s98V/O95Hvu/5UHI/5wzLv+dtuv+3/Vy23+cmuvvRvw/9H/h/6U/qv53fgj93/DM93/VA/f/Jbcd/xdctfmffcH8v+kaqf/bnqMZAnJ22RW///v+zP0/52f+/76vUXADah97gQ3IvWz1P/OcKRgrLZNG9c6K+1P+z3lZDrcZhFtfB4E1V9xz/V980wFsQNoZ4/9xJ/T/W62T+d9+oh1hQOm5iP8bPvX9b30ZCMfozgfZnAjUHHlM+7/HdpX/NkusEQYknHACuwCkAWQrzGYQBmkGYZjtQYc0/1tvlv0vN1kSYUDkYdv/Ex6lgG1DxjDNyLb77FcDB6T0BAmEATGHvP933ouCa4BhUKiDMLJmj70q/7Xmi/63mm2JMKD7Yis4nmGhjIzjoJoDD2qANYNsl53C+79lbwvCAFCUGG5QBfsTZBsMgzSC/AyzGeR3ldlC/yXbRDFT5bon68DxC9OErNFpjyLYZpBmgWbO/+uurcOeGtc8WPNfbr44OJ6dtyuCMUij1kJRsLPFW0Vwa4aBF19f/K/eW/3faLbBf9FuATA2mqz/v2ZrzX+QHNVzLwAjy7LsIPIjLQAAAABJRU5ErkJggg=='>";
								}elseif($cantidad_consistentes_periodo_actual<$cantidad_ordenes_periodo_actual){
									echo "<img src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAABrElEQVR42q2TyytEURzH/QFW1JRHIxsTUiNWHgtCFOWRPEpZyAJZkKSZhccgsVBIFqRkZdKUZCFFNLEQJUnJQk0eGcQCk+bj/O6dyZ3rsXLr272dcz7f3/d3zrlRUf/98PIIK1Mw0gzOKnCU6+/FUYJPD/wNH23DcB14F+DcA7sz4B5ScK8+XmuDvXV+h6da4GkfzpZhywWePljqhLlWmGiEwWoCFdbvJlpsNcm1MjmZVwnGfzaQdpT8RdbIdrSe16fhcFEHRWsOtRfdsNCuJ5MWxKCrgMeadPzTowYDp5o8dutQWAKb4tNTqhm8NmdxWZVnMFCDbM3ogFECTzZFVKctl7eGDE6zEwwGalDbbQHCkthSOQyHqstaaeHYHmcw6CjSj0oAoyS2CaY1i6vCJI7Kcr4MgrMD+mK5MEYJaIKlusS/GHMZDNSRvFWmfFUzgyE4UJ/GZW48ezYLAf+96S5srvJckhgJhUCRFBDYmxyNf83z820Mbrjx5cVpMeWoZLfl26cujsTeSbH8Doefj7sbfCMOToozOUyN5cAWw0G+nbN+J+93t/z73/sJB/zttLkU5gUAAAAASUVORK5CYII='>";
								}else{
									echo "<img src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAABpUlEQVR42q2TyytEURzH/QFWRCFTVhJF2WFla0XJwkKxlJ2VRxI1lA2RlRRWnqVGiRqPmJJXYqKGPEOGYdIwg/Nxzpm55ozByq1v3c6538fve85NSPjvhzcfnPTBYQ3slyP2ymC3Ajx21N7fZN8S4qgKAkO8M4N4HQBvB9w26nXhzIabOX4lc1qHn3WCjMFHJ7w0S4EGvc5JtRYJztviRXRsGTeAU5KH+KALQh0xAsJTiXCXadyN22S6h6iImlk89krX4bCz5f7YKN3qowK6j1IeHLncu+yGgCopNBEmWVBkI75KqMgKgeVCPCMlpkBppLCGWFzXaugC3WF3tovxL+aw05NmCMhF3XaE8AVVXGR2y53NQj1CrIDakEelo5pQsY3o2kgKnI3a2OovigqIg/boxyYsokH2ObJw96fgcXQaAvJI1FxWy3HECPllKY/j4XRW2lIJPXu/3YWLaZ5mM+IcLShnRV5rTcS7O/3zbRSnk5yPpOk0odV87ajeL8czdWxnU+rvZOt5f77haqGZvcECNuzJuNqTcHUX4J5qIei/5d//3k/ScyAkq+CgnAAAAABJRU5ErkJggg=='>";
								}
								?>
								</td>
								<td class='text-right'><?php echo number_format($cantidad_consistentes_periodo_anterior);?></td>
								<td class='text-right'>
								<?php 
								if($cantidad_ordenes_periodo_anterior==$cantidad_consistentes_periodo_anterior){
									echo "<img src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAB8ElEQVR42mNgoDb4/OfD/9UPpv6vv5z4v+C83/+sM17/c0+F/595o/f/u+/v/uPVfOHrgf8114L+L33f+H/j997/s98V/O95Hvu/5UHI/5wzLv+dtuv+3/Vy23+cmuvvRvw/9H/h/6U/qv53fgj93/DM93/VA/f/Jbcd/xdctfmffcH8v+kaqf/bnqMZAnJ22RW///v+zP0/52f+/76vUXADah97gQ3IvWz1P/OcKRgrLZNG9c6K+1P+z3lZDrcZhFtfB4E1V9xz/V980wFsQNoZ4/9xJ/T/W62T+d9+oh1hQOm5iP8bPvX9b30ZCMfozgfZnAjUHHlM+7/HdpX/NkusEQYknHACuwCkAWQrzGYQBmkGYZjtQYc0/1tvlv0vN1kSYUDkYdv/Ex6lgG1DxjDNyLb77FcDB6T0BAmEATGHvP933ouCa4BhUKiDMLJmj70q/7Xmi/63mm2JMKD7Yis4nmGhjIzjoJoDD2qANYNsl53C+79lbwvCAFCUGG5QBfsTZBsMgzSC/AyzGeR3ldlC/yXbRDFT5bon68DxC9OErNFpjyLYZpBmgWbO/+uurcOeGtc8WPNfbr44OJ6dtyuCMUij1kJRsLPFW0Vwa4aBF19f/K/eW/3faLbBf9FuATA2mqz/v2ZrzX+QHNVzLwAjy7LsIPIjLQAAAABJRU5ErkJggg=='>";
								}elseif($cantidad_consistentes_periodo_anterior<$cantidad_ordenes_periodo_anterior){
									echo "<img src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAABrElEQVR42q2TyytEURzH/QFW1JRHIxsTUiNWHgtCFOWRPEpZyAJZkKSZhccgsVBIFqRkZdKUZCFFNLEQJUnJQk0eGcQCk+bj/O6dyZ3rsXLr272dcz7f3/d3zrlRUf/98PIIK1Mw0gzOKnCU6+/FUYJPD/wNH23DcB14F+DcA7sz4B5ScK8+XmuDvXV+h6da4GkfzpZhywWePljqhLlWmGiEwWoCFdbvJlpsNcm1MjmZVwnGfzaQdpT8RdbIdrSe16fhcFEHRWsOtRfdsNCuJ5MWxKCrgMeadPzTowYDp5o8dutQWAKb4tNTqhm8NmdxWZVnMFCDbM3ogFECTzZFVKctl7eGDE6zEwwGalDbbQHCkthSOQyHqstaaeHYHmcw6CjSj0oAoyS2CaY1i6vCJI7Kcr4MgrMD+mK5MEYJaIKlusS/GHMZDNSRvFWmfFUzgyE4UJ/GZW48ezYLAf+96S5srvJckhgJhUCRFBDYmxyNf83z820Mbrjx5cVpMeWoZLfl26cujsTeSbH8Doefj7sbfCMOToozOUyN5cAWw0G+nbN+J+93t/z73/sJB/zttLkU5gUAAAAASUVORK5CYII='>";
								}else{
									echo "<img src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAABpUlEQVR42q2TyytEURzH/QFWRCFTVhJF2WFla0XJwkKxlJ2VRxI1lA2RlRRWnqVGiRqPmJJXYqKGPEOGYdIwg/Nxzpm55ozByq1v3c6538fve85NSPjvhzcfnPTBYQ3slyP2ymC3Ajx21N7fZN8S4qgKAkO8M4N4HQBvB9w26nXhzIabOX4lc1qHn3WCjMFHJ7w0S4EGvc5JtRYJztviRXRsGTeAU5KH+KALQh0xAsJTiXCXadyN22S6h6iImlk89krX4bCz5f7YKN3qowK6j1IeHLncu+yGgCopNBEmWVBkI75KqMgKgeVCPCMlpkBppLCGWFzXaugC3WF3tovxL+aw05NmCMhF3XaE8AVVXGR2y53NQj1CrIDakEelo5pQsY3o2kgKnI3a2OovigqIg/boxyYsokH2ObJw96fgcXQaAvJI1FxWy3HECPllKY/j4XRW2lIJPXu/3YWLaZ5mM+IcLShnRV5rTcS7O/3zbRSnk5yPpOk0odV87ajeL8czdWxnU+rvZOt5f77haqGZvcECNuzJuNqTcHUX4J5qIei/5d//3k/ScyAkq+CgnAAAAABJRU5ErkJggg=='>";
								}
								?>
								</td>
							</tr>
							<tr>
								<td><i class='fa fa-tasks'></i> Pendientes</td>
								<td class='text-right'><?php echo number_format($cantidad_ordenes_dia_actual-$cantidad_avanzados_dia_actual);?></td>
								<td class='text-right'><?php echo number_format($cantidad_ordenes_periodo_actual-$cantidad_avanzados_periodo_actual);?></td>
								<td class='text-right'>
								<?php 
								if(($cantidad_ordenes_periodo_actual-$cantidad_avanzados_periodo_actual)==0){
									echo "<img src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAB8ElEQVR42mNgoDb4/OfD/9UPpv6vv5z4v+C83/+sM17/c0+F/595o/f/u+/v/uPVfOHrgf8114L+L33f+H/j997/s98V/O95Hvu/5UHI/5wzLv+dtuv+3/Vy23+cmuvvRvw/9H/h/6U/qv53fgj93/DM93/VA/f/Jbcd/xdctfmffcH8v+kaqf/bnqMZAnJ22RW///v+zP0/52f+/76vUXADah97gQ3IvWz1P/OcKRgrLZNG9c6K+1P+z3lZDrcZhFtfB4E1V9xz/V980wFsQNoZ4/9xJ/T/W62T+d9+oh1hQOm5iP8bPvX9b30ZCMfozgfZnAjUHHlM+7/HdpX/NkusEQYknHACuwCkAWQrzGYQBmkGYZjtQYc0/1tvlv0vN1kSYUDkYdv/Ex6lgG1DxjDNyLb77FcDB6T0BAmEATGHvP933ouCa4BhUKiDMLJmj70q/7Xmi/63mm2JMKD7Yis4nmGhjIzjoJoDD2qANYNsl53C+79lbwvCAFCUGG5QBfsTZBsMgzSC/AyzGeR3ldlC/yXbRDFT5bon68DxC9OErNFpjyLYZpBmgWbO/+uurcOeGtc8WPNfbr44OJ6dtyuCMUij1kJRsLPFW0Vwa4aBF19f/K/eW/3faLbBf9FuATA2mqz/v2ZrzX+QHNVzLwAjy7LsIPIjLQAAAABJRU5ErkJggg=='>";
								}elseif(($cantidad_ordenes_periodo_actual-$cantidad_avanzados_periodo_actual)>0){
									echo "<img src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAABrElEQVR42q2TyytEURzH/QFW1JRHIxsTUiNWHgtCFOWRPEpZyAJZkKSZhccgsVBIFqRkZdKUZCFFNLEQJUnJQk0eGcQCk+bj/O6dyZ3rsXLr272dcz7f3/d3zrlRUf/98PIIK1Mw0gzOKnCU6+/FUYJPD/wNH23DcB14F+DcA7sz4B5ScK8+XmuDvXV+h6da4GkfzpZhywWePljqhLlWmGiEwWoCFdbvJlpsNcm1MjmZVwnGfzaQdpT8RdbIdrSe16fhcFEHRWsOtRfdsNCuJ5MWxKCrgMeadPzTowYDp5o8dutQWAKb4tNTqhm8NmdxWZVnMFCDbM3ogFECTzZFVKctl7eGDE6zEwwGalDbbQHCkthSOQyHqstaaeHYHmcw6CjSj0oAoyS2CaY1i6vCJI7Kcr4MgrMD+mK5MEYJaIKlusS/GHMZDNSRvFWmfFUzgyE4UJ/GZW48ezYLAf+96S5srvJckhgJhUCRFBDYmxyNf83z820Mbrjx5cVpMeWoZLfl26cujsTeSbH8Doefj7sbfCMOToozOUyN5cAWw0G+nbN+J+93t/z73/sJB/zttLkU5gUAAAAASUVORK5CYII='>";
								}else{
									echo "<img src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAABpUlEQVR42q2TyytEURzH/QFWRCFTVhJF2WFla0XJwkKxlJ2VRxI1lA2RlRRWnqVGiRqPmJJXYqKGPEOGYdIwg/Nxzpm55ozByq1v3c6538fve85NSPjvhzcfnPTBYQ3slyP2ymC3Ajx21N7fZN8S4qgKAkO8M4N4HQBvB9w26nXhzIabOX4lc1qHn3WCjMFHJ7w0S4EGvc5JtRYJztviRXRsGTeAU5KH+KALQh0xAsJTiXCXadyN22S6h6iImlk89krX4bCz5f7YKN3qowK6j1IeHLncu+yGgCopNBEmWVBkI75KqMgKgeVCPCMlpkBppLCGWFzXaugC3WF3tovxL+aw05NmCMhF3XaE8AVVXGR2y53NQj1CrIDakEelo5pQsY3o2kgKnI3a2OovigqIg/boxyYsokH2ObJw96fgcXQaAvJI1FxWy3HECPllKY/j4XRW2lIJPXu/3YWLaZ5mM+IcLShnRV5rTcS7O/3zbRSnk5yPpOk0odV87ajeL8czdWxnU+rvZOt5f77haqGZvcECNuzJuNqTcHUX4J5qIei/5d//3k/ScyAkq+CgnAAAAABJRU5ErkJggg=='>";
								}
								?>
								</td>
								<td class='text-right'><?php echo number_format($cantidad_ordenes_periodo_anterior-$cantidad_avanzados_periodo_anterior);?></td>
								<td class='text-right'>
								<?php 
								if(($cantidad_ordenes_periodo_anterior-$cantidad_avanzados_periodo_anterior)==0){
									echo "<img src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAB8ElEQVR42mNgoDb4/OfD/9UPpv6vv5z4v+C83/+sM17/c0+F/595o/f/u+/v/uPVfOHrgf8114L+L33f+H/j997/s98V/O95Hvu/5UHI/5wzLv+dtuv+3/Vy23+cmuvvRvw/9H/h/6U/qv53fgj93/DM93/VA/f/Jbcd/xdctfmffcH8v+kaqf/bnqMZAnJ22RW///v+zP0/52f+/76vUXADah97gQ3IvWz1P/OcKRgrLZNG9c6K+1P+z3lZDrcZhFtfB4E1V9xz/V980wFsQNoZ4/9xJ/T/W62T+d9+oh1hQOm5iP8bPvX9b30ZCMfozgfZnAjUHHlM+7/HdpX/NkusEQYknHACuwCkAWQrzGYQBmkGYZjtQYc0/1tvlv0vN1kSYUDkYdv/Ex6lgG1DxjDNyLb77FcDB6T0BAmEATGHvP933ouCa4BhUKiDMLJmj70q/7Xmi/63mm2JMKD7Yis4nmGhjIzjoJoDD2qANYNsl53C+79lbwvCAFCUGG5QBfsTZBsMgzSC/AyzGeR3ldlC/yXbRDFT5bon68DxC9OErNFpjyLYZpBmgWbO/+uurcOeGtc8WPNfbr44OJ6dtyuCMUij1kJRsLPFW0Vwa4aBF19f/K/eW/3faLbBf9FuATA2mqz/v2ZrzX+QHNVzLwAjy7LsIPIjLQAAAABJRU5ErkJggg=='>";
								}elseif(($cantidad_ordenes_periodo_anterior-$cantidad_avanzados_periodo_anterior)>0){
									echo "<img src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAABrElEQVR42q2TyytEURzH/QFW1JRHIxsTUiNWHgtCFOWRPEpZyAJZkKSZhccgsVBIFqRkZdKUZCFFNLEQJUnJQk0eGcQCk+bj/O6dyZ3rsXLr272dcz7f3/d3zrlRUf/98PIIK1Mw0gzOKnCU6+/FUYJPD/wNH23DcB14F+DcA7sz4B5ScK8+XmuDvXV+h6da4GkfzpZhywWePljqhLlWmGiEwWoCFdbvJlpsNcm1MjmZVwnGfzaQdpT8RdbIdrSe16fhcFEHRWsOtRfdsNCuJ5MWxKCrgMeadPzTowYDp5o8dutQWAKb4tNTqhm8NmdxWZVnMFCDbM3ogFECTzZFVKctl7eGDE6zEwwGalDbbQHCkthSOQyHqstaaeHYHmcw6CjSj0oAoyS2CaY1i6vCJI7Kcr4MgrMD+mK5MEYJaIKlusS/GHMZDNSRvFWmfFUzgyE4UJ/GZW48ezYLAf+96S5srvJckhgJhUCRFBDYmxyNf83z820Mbrjx5cVpMeWoZLfl26cujsTeSbH8Doefj7sbfCMOToozOUyN5cAWw0G+nbN+J+93t/z73/sJB/zttLkU5gUAAAAASUVORK5CYII='>";
								}else{
									echo "<img src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAABpUlEQVR42q2TyytEURzH/QFWRCFTVhJF2WFla0XJwkKxlJ2VRxI1lA2RlRRWnqVGiRqPmJJXYqKGPEOGYdIwg/Nxzpm55ozByq1v3c6538fve85NSPjvhzcfnPTBYQ3slyP2ymC3Ajx21N7fZN8S4qgKAkO8M4N4HQBvB9w26nXhzIabOX4lc1qHn3WCjMFHJ7w0S4EGvc5JtRYJztviRXRsGTeAU5KH+KALQh0xAsJTiXCXadyN22S6h6iImlk89krX4bCz5f7YKN3qowK6j1IeHLncu+yGgCopNBEmWVBkI75KqMgKgeVCPCMlpkBppLCGWFzXaugC3WF3tovxL+aw05NmCMhF3XaE8AVVXGR2y53NQj1CrIDakEelo5pQsY3o2kgKnI3a2OovigqIg/boxyYsokH2ObJw96fgcXQaAvJI1FxWy3HECPllKY/j4XRW2lIJPXu/3YWLaZ5mM+IcLShnRV5rTcS7O/3zbRSnk5yPpOk0odV87ajeL8czdWxnU+rvZOt5f77haqGZvcECNuzJuNqTcHUX4J5qIei/5d//3k/ScyAkq+CgnAAAAABJRU5ErkJggg=='>";
								}
								?>
								</td>
							</tr>
						</tbody>
					</table>
				</div>

				</div>
			</div>
			
			<div class='block-flat'>
				<div class='header'>
					<h3>KPI 2 - Indicadores Clave de Proceso</h3>
				</div>
				<div class='content'>
					<iframe class='preview-pane'
						src='<?php echo ENV_WEBROOT_FULL_URL;?>dashboard/resumen_lecturas_kpi2<?php echo $url_completa;?>'
						width='100%' height='375' frameborder='0' scrolling='yes'></iframe>
				</div>
			</div>
			
			<div class='block-flat'>
				<div class='header'>
					<h3>Monitor 2 - Control de Acumulaci&oacute;n de Consumos por D&iacute;a</h3>
				</div>
				<div class='content overflow-hidden'>
					<iframe class='preview-pane'
						src='<?php echo ENV_WEBROOT_FULL_URL;?>dashboard/grafico_consumo_por_dia<?php echo $url_completa;?>' width='100%'
						height='305' frameborder='0' scrolling='yes'></iframe>
					<a href="<?php echo ENV_WEBROOT_FULL_URL;?>dashboard/grafico_consumo_por_dia<?php echo $url_completa;?>" target="_blank">ver gr&aacute;fico completo</a> | 
					<a href="<?php echo ENV_WEBROOT_FULL_URL;?>dashboard/resumen_consumo_por_dia_y_ubicacion<?php echo $url_completa;?>" target="_blank">ver resumen</a> | 
					<a href="<?php echo ENV_WEBROOT_FULL_URL;?>reportes/resumen_suministros_cronograma<?php echo $url_completa;?>" target="_blank">ver resumen detallado</a>
				</div>
			</div>
		</div>


		<div class='col-sm-6 col-md-6'>
			<div class='block-flat'>
				<div class='header'>
					<h3>Monitor GPS - Ubicacion en Tiempo Real</h3>
				</div>
				<div class='content'>
					<iframe class='preview-pane'
						src='<?php echo ENV_WEBROOT_FULL_URL;?>MonitorearLecturas/mapa_monitoreo_free/F/<?php echo date('Y-m-d');?>/<?php echo date('Y-m-d');?><?php echo $url_completa;?>'
						width='100%' height='320' frameborder='0' scrolling='no'></iframe>
					<a href="<?php echo ENV_WEBROOT_FULL_URL;?>MonitorearLecturas/mapa_monitoreo/F/<?php echo date('Y-m-d');?>/<?php echo date('Y-m-d');?><?php echo $url_completa;?>" target="_blank">ver mapa completo</a>
				</div>
			</div>
			
			<div class='block-flat'>
				<div class='header'>
					<h3>Monitor 1 - Resumen Observaciones por Ciclo</h3>
				</div>
				<div class='content'>
					<iframe class='preview-pane'
						src='<?php echo ENV_WEBROOT_FULL_URL;?>dashboard/grafico_observaciones_por_ciclo<?php echo $url_completa;?>'
						width='100%' height='445' frameborder='0' scrolling='no'></iframe>
					<a href="<?php echo ENV_WEBROOT_FULL_URL ?>dashboard/grafico_observaciones_por_ciclo<?php echo $url_completa;?>" target="_blank">ver gr&aacute;fico completo</a> | 
					<a href="<?php echo ENV_WEBROOT_FULL_URL ?>dashboard/reporte_observaciones" target="_blank">ver tabla</a>
				</div>
			</div>
			
			<div class='block-flat'>
				<div class='header'>
					<h3>Monitor 3 - Suministros por M&eacute;todo de Evaluaci&oacute;n</h3>
				</div>
				<div class='content overflow-hidden'>
					<iframe class='preview-pane'
						src='<?php echo ENV_WEBROOT_FULL_URL;?>dashboard/grafico_suministros_por_metodo<?php echo $url_completa;?>' width='100%'
						height='250' frameborder='0' scrolling='yes'></iframe>
					<a href="<?php echo ENV_WEBROOT_FULL_URL;?>dashboard/grafico_suministros_por_metodo<?php echo $url_completa;?>" target="_blank">ver gr&aacute;fico completo</a>
				</div>
			</div>

		</div>

		<div class='col-sm-12 col-md-12'>
		
			<div class='block-flat'>
				<div class='header'>
					<h3>Monitor 4 - Resumen Detallado por Ciclo</h3>
				</div>
				<div class='content overflow-hidden'>
					<iframe class='preview-pane'
						src='<?php echo ENV_WEBROOT_FULL_URL;?>dashboard/resumen_por_ciclos<?php echo $url_completa;?>' width='100%'
						height='415' frameborder='0' scrolling='yes'></iframe>
						<a href="<?php echo ENV_WEBROOT_FULL_URL;?>reportes/resumen_suministros_cronograma" target="_blank">ver resumen detallado cronograma</a>
				</div>
			</div>
			
			<div class='block-flat'>
				<div class='header'>
					<h3>Monitor 5 - Control de Productividad Semanal de Lecturistas</h3>
				</div>
				<div class='content overflow-hidden'>
					<iframe class='preview-pane'
						src='<?php echo ENV_WEBROOT_FULL_URL;?>dashboard/grafico_lecturistas_por_semana<?php echo $url_completa;?>' width='100%'
						height='415' frameborder='0' scrolling='no'></iframe>
				</div>
			</div>
			
			<div class='block-flat'>
				<div class='header'>
					<h3>Monitor 6 - Control de Productividad de Lecturistas por Periodo Actual</h3>
				</div>
				<div class='content overflow-hidden'>
					<iframe class='preview-pane'
						src='<?php echo ENV_WEBROOT_FULL_URL;?>dashboard/grafico_lecturistas_por_periodo<?php echo $url_completa;?>' width='100%'
						height='415' frameborder='0' scrolling='yes'></iframe>
					<a href="<?php echo ENV_WEBROOT_FULL_URL;?>dashboard/grafico_lecturistas_por_periodo<?php echo $url_completa;?>" target="_blank">ver gr&aacute;fico completo</a>
				</div>
			</div>
			
			<div class='block-flat'>
				<div class='header'>
					<h3>Monitor 7 - Control de Productividad de Digitadores por Periodo Actual</h3>
				</div>
				<div class='content overflow-hidden'>
					<iframe class='preview-pane'
						src='<?php echo ENV_WEBROOT_FULL_URL;?>dashboard/grafico_digitadores_por_periodo<?php echo $url_completa;?>' width='100%'
						height='305' frameborder='0' scrolling='yes'></iframe>
					<a href="<?php echo ENV_WEBROOT_FULL_URL;?>dashboard/grafico_digitadores_por_periodo<?php echo $url_completa;?>" target="_blank">ver gr&aacute;fico completo</a>
				</div>
			</div>
			
			<div class='block-flat'>
				<div class='header'>
					<h3>Monitor 8 - Control de Distribuci&oacute;n de Consumos</h3>
				</div>
				<div class='content overflow-hidden'>
					<iframe class='preview-pane'
						src='<?php echo ENV_WEBROOT_FULL_URL;?>dashboard/grafico_rendimiento_consumo<?php echo $url_completa;?>' width='100%'
						height='700' frameborder='0' scrolling='yes'></iframe>
					<a href="<?php echo ENV_WEBROOT_FULL_URL;?>dashboard/grafico_rendimiento_consumo<?php echo $url_completa;?>" target="_blank">ver gr&aacute;fico completo</a>
				</div>
			</div>
			
		</div>

	</div>
</div>