<?php 
if (!isset($NEGOCIO)) {
    header("Location:".ENV_WEBROOT_FULL_URL."/Usuarios/logout");
    die();
}
?>
<?php 
if (isset($id_usuario) && $id_usuario == 25){
	$permiso_lectura = 'true';
}else {
	$permiso_lectura = 'true';
}
echo $this->element('menu',array('active'=>'rep_consumo_por_dia_y_ubicacion', 'open_report'=>'true', 'permiso_lectura' =>$permiso_lectura, 'id_usuario ' =>isset($id_usuario)?$id_usuario:null));
?>

<div class="container-fluid" id="pcont">

<h2 class="text-center" style="margin-top: 5px;">Resumen de Consumo por D&iacute;a y Ubicaci&oacute;n</h2>

<div class='row'>
	
		<div class='col-sm-12 col-md-12'>
			<div class='block-flat'>
				<div class='content overflow-hidden'>
					
<table class='red'>
	<thead>
		<tr>
			<th>Nro D&iacute;a</th>
			<th>Fecha</th>
			<th>Ciclo</th>
			<th>Sector</th>
			<th class='right'><span></span>Consumo (Kwh)</th>
		</tr>
	</thead>
	<tbody class='no-border-x'>
		<?php
		$i = 0;
		$tmp_fecha = '';
		$sum_consumo = 0;
		foreach ($arr_data as $k => $obj){
			$str_style = '';
			foreach ($arr_ciclos_actuales as $k => $obj1){
				if($obj[0]['idciclo']==$obj1[0]['idciclo']){
					$str_style = 'background-color: yellow;';		
				}
			}
			if($tmp_fecha!=$obj[0]['fecha']){
				$tmp_fecha = $obj[0]['fecha'];
				$i++;
			}
			?>
			<tr style="<?php echo $str_style;?>">
			<td><?php echo $i;?></td>
			<td><?php echo substr($obj[0]['fecha'],0,10);?></td>
			<td><?php echo $obj[0]['idciclo'].' - '.$obj[0]['nombciclo'];?></td>
			<td><?php echo $obj[0]['sectores'];?></td>
			<td class='right'><?php echo number_format($obj[0]['montoconsumo']);?></td>
			</tr>
			<?php
			$sum_consumo += $obj[0]['montoconsumo'];
		} 
		?>		
	</tbody>
	<tfoot>
		<tr style="<?php echo $str_style;?>">
			<td colspan="4">Total</td>
			<td class='right'><?php echo number_format($sum_consumo);?></td>
		</tr>
	</tfoot>
</table>

				</div>
			</div>
		</div>
</div>
</div>