<?php 
if (!isset($NEGOCIO)) {
    header("Location:".ENV_WEBROOT_FULL_URL."/Usuarios/logout");
    die();
}
?>
<?php 
if (isset($id_usuario) && $id_usuario == 25){
	$permiso_lectura = 'true';
}else {
	$permiso_lectura = 'true';
}
echo $this->element('menu',array('active'=>'rep_observaciones', 'open_report'=>'true', 'permiso_lectura' =>$permiso_lectura, 'id_usuario ' =>isset($id_usuario)?$id_usuario:null));
?>
<div class="container-fluid" id="pcont">
	<!--
	<div class="page-head" style="padding:0px;">
				
				<ol class="breadcrumb">
				  <li><button type="button" class="btn-xs btn-default arbol-top-lecturas" >Arbol</button></li>
				  <li><a href="#">Lecturas</a></li>
				  <li class="active">Reporte de Observaciones</li>
				</ol>
	</div>
-->
	

	<div class="cl-mcont">
		<div class="col-sm-12 col-md-12">

			<div class="block">
				<div class="header no-border">
					<h2>Filtros</h2>
				</div>

				<div class="content">
					<form
						action="<?= ENV_WEBROOT_FULL_URL?>dashboard/reporte_observaciones"
						id="ReporteObsForm" method="post">
						<div style="display: none;">
							<input type="hidden" name="_method" value="POST">
						</div>

						<div class="form-group">
			              <label class="control-label">Por Unidad Negocio:</label>              
			              	<?php 
			        			  	echo '<select class="" name="unidadneg" id="unidadneg">
			        			  	<option value="0">Todos</option>';
		                                             //   var_dump($listar_unidadneg);exit;
			        				foreach ($listar_unidadneg as $v){
									if($unidadneg==$v['0']['unidadneg']){
										$selected = 'selected';
									}else{
										$selected = '';
									}
			        				echo '<option value="'.$v['0']['unidadneg'].'" '.$selected.'>'.$v['0']['nombreunidadnegocio'].'</option>';
			        				}
			        				echo '</select>';	
			        		?>        			
			            </div>
			            
			            <div class="form-group">
			              <label class="control-label">Por Periodo de Factura:</label>              
			              	<?php 
			        			  	echo '<select class="select2" name="pfactura" id="pfactura">
			        			  	<option value="0">Actual</option>';
		                                             //   var_dump($listar_unidadneg);exit;
			        				foreach ($listar_pfacrura as $v){
									if($pfactura==$v['0']['pfactura']){
										$selected = 'selected';
									}else{
										$selected = '';
									}
			        				echo '<option value="'.$v['0']['pfactura'].'" '.$selected.'>'.$v['0']['pfactura'].'</option>';
			        				}
			        				echo '</select>';	
			        		?>        			
			            </div>
	        		
						 <input
							type="submit" class="btn btn-info" value="Buscar">
					</form>
				</div>
			</div>
		
			<div class="panel-group accordion accordion" id="accordion4">
			<!--div class="panel-group accordion accordion-semi" id="accordion4"-->
					  <div class="panel panel-default">
						<div class="panel-heading">
						<!--div class="panel-heading success"-->
						  <h4 class="panel-title">
							<a data-toggle="collapse" data-parent="#accordion4" href="#ac4-1" id="accordion-tab1">
								<i class="fa fa-angle-right"></i> Observaciones por Ciclo/<?php if($NEGOCIO == 'CIX'){echo 'Sector';}elseif ($NEGOCIO == 'TAC'){echo 'Zona';}else{echo 'Sector';}?>
							</a>
						  </h4>
						</div>
						<div id="ac4-1" class="panel-collapse collapse in">
						  <div class="panel-body">							
							<div class="content">
								<div class='tabla'>
									<div id='asig' class='farmsmall2'>
										<div class='table-responsive'>		
																			
											<div id="grid">
												<table class="table table-bordered dataTable" id="tabla_reporte_observaciones" >
												<!--thead class="primary-emphasis"-->
												<thead class="primary-emphasis">
												<tr>
												<th>Ciclo</th>
												<th>Sector</th>
												<?php 
												foreach ($arr_codigos_observacion as $codigo){?>
													<th title="<?php echo $codigo[0]['descripcion'];?>"><?php echo $codigo[0]['obs1'].' - '.$codigo[0]['codigofac'];?></th>
												<?php 
												}
												?>
												</tr>
												</thead>
												<tfoot style="font-size: 14px;">
										            <tr>
										            </tr>
										        </tfoot>
												<tbody>
												<?php
												$idciclo_tmp = '';
												$sector_tmp = '';
												foreach ($arr_count_codigos_observacion as $obj_orden_lectura){
													if($idciclo_tmp!=$obj_orden_lectura[0]['idciclo'] || $sector_tmp!=$obj_orden_lectura[0]['sector']){
													if($idciclo_tmp!=''){
														if($count_obs<count($arr_codigos_observacion)){
														for ($x=0; $x<count($arr_codigos_observacion)-$count_obs; $x++){
															?><td>0</td><?php
														}
														}
													?>
													</tr>
													<?php }?>
													<tr style="cursor: pointer;" class="odd gradeX" id="orden_lectura" idciclo="<?php echo $obj_orden_lectura[0]['idciclo'];?>" idsector="<?php echo $obj_orden_lectura[0]['sector'];?>" >									
													<td><?php echo $obj_orden_lectura[0]['idciclo'];?></td>
													<td><?php echo $obj_orden_lectura[0]['sector'];?></td>
													<?php 
													$idciclo_tmp = $obj_orden_lectura[0]['idciclo'];
													$sector_tmp = $obj_orden_lectura[0]['sector'];
													$last_codigo = 0;
													$count_obs = 0;
													}
													foreach ($arr_codigos_observacion as $codigo){
														if($codigo[0]['obs1']>$last_codigo && $codigo[0]['obs1']==$obj_orden_lectura[0]['obs1']){
														$last_codigo = $codigo[0]['obs1'];
														$count_obs++;
														?>
														<td><?php echo $obj_orden_lectura[0]['cantidad'];?></td>
														<?php 
														}elseif($codigo[0]['obs1']>$last_codigo && $codigo[0]['obs1']<$obj_orden_lectura[0]['obs1']){
														$last_codigo = $codigo[0]['obs1'];
														$count_obs++;
														?>
														<td>0</td>
														<?php 
														}
													}
												}
												if($idciclo_tmp!=''){
													if($count_obs<count($arr_codigos_observacion)){
														for ($x=0; $x<count($arr_codigos_observacion)-$count_obs; $x++){
															?><td>0</td><?php
														}
														}
													?>
													</tr>
												<?php }?>
												</tbody>
												<tfoot>
												</tfoot>
												</table>
											</div>
										</div>
									</div>
								</div>
							</div>				
							
						  </div>
						</div>
					  </div>
				</div>
				
				
				<div class="panel-group accordion accordion" id="accordion5">
			<!--div class="panel-group accordion accordion-semi" id="accordion4"-->
					  <div class="panel panel-default">
						<div class="panel-heading">
						<!--div class="panel-heading success"-->
						  <h4 class="panel-title">
							<a data-toggle="collapse" data-parent="#accordion5" href="#ac5-1" id="accordion-tab1">
								<i class="fa fa-angle-right"></i> Observaciones por Ciclo/<?php if($NEGOCIO == 'CIX'){echo 'Sector';}elseif ($NEGOCIO == 'TAC'){echo 'Zona';}else{echo 'Sector';}?>/Ruta
							</a>
						  </h4>
						</div>
						<div id="ac5-1" class="panel-collapse collapse in">
						  <div class="panel-body">							
							<div class="content">
								<div class='tabla'>
									<div id='asig' class='farmsmall2'>
										<div class='table-responsive'>		
																			
											<div id="grid">
												<table class="table table-bordered dataTable" id="tabla_reporte_observaciones_sector_ruta" >
												<!--thead class="primary-emphasis"-->
												<thead class="primary-emphasis">
												<tr>
												<th>Ciclo</th>
												<th>Sector</th>
												<th>Ruta</th>
												<?php 
												foreach ($arr_codigos_observacion as $codigo){?>
													<th title="<?php echo $codigo[0]['descripcion'];?>"><?php echo $codigo[0]['obs1'].' - '.$codigo[0]['codigofac'];?></th>
												<?php 
												}
												?>
												</tr>
												</thead>
												<tfoot style="font-size: 14px;">
										            <tr>
										            </tr>
										        </tfoot>
												<tbody>
												<?php
												$idciclo_tmp = '';
												$sector_tmp = '';
												$ruta_tmp = '';
												foreach ($arr_count_codigos_observacion_sector_ruta as $obj_orden_lectura){
													if($idciclo_tmp!=$obj_orden_lectura[0]['idciclo'] || $sector_tmp!=$obj_orden_lectura[0]['sector'] || $ruta_tmp!=$obj_orden_lectura[0]['ruta']){
													if($idciclo_tmp!=''){
														if($count_obs<count($arr_codigos_observacion)){
														for ($x=0; $x<count($arr_codigos_observacion)-$count_obs; $x++){
															?><td>0</td><?php
														}
														}
													?>
													</tr>
													<?php }?>
													<tr style="cursor: pointer;" class="odd gradeX" id="orden_lectura" idciclo="<?php echo $obj_orden_lectura[0]['idciclo'];?>" idsector="<?php echo $obj_orden_lectura[0]['sector'];?>" idruta="<?php echo $obj_orden_lectura[0]['ruta'];?>" >									
													<td><?php echo $obj_orden_lectura[0]['idciclo'];?></td>
													<td><?php echo $obj_orden_lectura[0]['sector'];?></td>
													<td><?php echo $obj_orden_lectura[0]['ruta'];?></td>
													<?php 
													$idciclo_tmp = $obj_orden_lectura[0]['idciclo'];
													$sector_tmp = $obj_orden_lectura[0]['sector'];
													$ruta_tmp = $obj_orden_lectura[0]['ruta'];
													$last_codigo = 0;
													$count_obs = 0;
													}
													foreach ($arr_codigos_observacion as $codigo){
														if($codigo[0]['obs1']>$last_codigo && $codigo[0]['obs1']==$obj_orden_lectura[0]['obs1']){
														$last_codigo = $codigo[0]['obs1'];
														$count_obs++;
														?>
														<td><?php echo $obj_orden_lectura[0]['cantidad'];?></td>
														<?php 
														}elseif($codigo[0]['obs1']>$last_codigo && $codigo[0]['obs1']<$obj_orden_lectura[0]['obs1']){
														$last_codigo = $codigo[0]['obs1'];
														$count_obs++;
														?>
														<td>0</td>
														<?php 
														}
													}
												}
												if($idciclo_tmp!=''){
													if($count_obs<count($arr_codigos_observacion)){
														for ($x=0; $x<count($arr_codigos_observacion)-$count_obs; $x++){
															?><td>0</td><?php
														}
														}
													?>
													</tr>
												<?php }?>
												</tbody>
												</table>
											</div>
										</div>
									</div>
								</div>
							</div>				
							
						  </div>
						</div>
					  </div>
				</div>		
						
		</div>
		
	</div>
</div>

<script>
var oTable = $('#tabla_reporte_observaciones').dataTable({
		"iDisplayLength" : 50,
		"aLengthMenu": [
						[50,100,300,500,800],
						[50,100,300,500,800]
						],
	"sDom": '<"top"p>T<"clear"><"datatables_filter"lrf><"bottom">ti',
 	"oTableTools": {
 		"sSwfPath":  ENV_WEBROOT_FULL_URL+"js/jquery.datatables/extras/TableTools/media/swf/copy_csv_xls_pdf.swf"
	},
	"fnFooterCallback": function( nFoot, aData, iStart, iEnd, aiDisplay ){

		$(nFoot).html('');
		$(nFoot).append('<td colspan="2" style="text-align:right"><strong>Total:</strong></td>');


		<?php 
		$x = 2;
		foreach ($arr_codigos_observacion as $codigo){
		?>
			var col = <?php echo $x;?>;
			var tot = 0;
			var tot_filter = 0;
			for ( var i = 0; i < aData.length; i++) {
				tot += aData[i][col] * 1;
			}
			for ( var i = iStart; i < iEnd; i++) {
				tot_filter += aData[aiDisplay[i]][col] * 1;
			}
			$(nFoot).append('<td><strong>' + tot_filter + ' (' + tot + '  total)</strong></td>');
			<?php 
			$x++;
		}
		?>

	}
	});

var oTable = $('#tabla_reporte_observaciones_sector_ruta').dataTable({
	"iDisplayLength" : 50,
	"aLengthMenu": [
					[50,100,300,500,800],
					[50,100,300,500,800]
					],
	"sDom": '<"top"p>T<"clear"><"datatables_filter"lrf><"bottom">ti',
		"oTableTools": {
			"sSwfPath":  ENV_WEBROOT_FULL_URL+"js/jquery.datatables/extras/TableTools/media/swf/copy_csv_xls_pdf.swf"
	},
	"fnFooterCallback": function( nFoot, aData, iStart, iEnd, aiDisplay ){
	
		$(nFoot).html('');
		$(nFoot).append('<td colspan="3" style="text-align:right"><strong>Total:</strong></td>');
	
	
		<?php 
		$x = 3;
		foreach ($arr_codigos_observacion as $codigo){
		?>
			var col = <?php echo $x;?>;
			var tot = 0;
			var tot_filter = 0;
			for ( var i = 0; i < aData.length; i++) {
				tot += aData[i][col] * 1;
			}
			for ( var i = iStart; i < iEnd; i++) {
				tot_filter += aData[aiDisplay[i]][col] * 1;
			}
			$(nFoot).append('<td><strong>' + tot_filter + ' (' + tot + '  total)</strong></td>');
			<?php 
			$x++;
		}
		?>
	
	}
});
</script>