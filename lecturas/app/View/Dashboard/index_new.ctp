<?php 
if (!isset($NEGOCIO)) {
    header("Location:".ENV_WEBROOT_FULL_URL."/Usuarios/logout");
    die();
}
?>
<?php 
if (isset($id_usuario) && $id_usuario == 25){
	$permiso_lectura = 'true';
}else {
	$permiso_lectura = 'true';
}
echo $this->element('menu',array('active'=>'monitorearlectura', 'open'=>'true', 'permiso_lectura' =>$permiso_lectura, 'id_usuario ' =>isset($id_usuario)?$id_usuario:null));

if(isset($unidad_neg) && isset($idciclo)){
	$url_completa = '/'.$unidad_neg.'/'.$idciclo;
}else{
	$url_completa = '';
}
?>
<script type="text/javascript">														
	require(['ajax/dashboard']);
</script>

<div class="container-fluid" id="pcont">
	
	<div class="page-head" style="padding-bottom: 6px;">
		<div class="row">
			<div class="col-xs-2">
				<h3 style="margin-top: 0px">Filtros:</h3>
			</div>
			<div class="col-xs-2">
				<label class="control-label" style="padding-top: 8px;">Por Unidad
					Negocio:</label>
			</div>
			<div class="col-xs-2">              
              	<?php 
	        			  	echo '<select class="" name="unidadneg" id="unidadneg">
	        			  	<option value="0">Todos</option>';
                                             //   var_dump($listar_unidadneg);exit;
	        				foreach ($listar_unidadneg as $v){
		        				if($unidad_neg==$v['0']['unidadneg']){
		        					echo '<option value='.$v['0']['unidadneg'].' selected>'.$v['0']['nombreunidadnegocio'].'</option>';
		        				}else{
		        					echo '<option value='.$v['0']['unidadneg'].'>'.$v['0']['nombreunidadnegocio'].'</option>';
		        				}
	        				}
	        				echo '</select>';	
	        		?> 
		  </div>
			<div class="col-xs-1">
				<label class="control-label" style="padding-top: 8px;">Por Ciclo:<span
					class="ciclo_loading" style="display: none;"><img
						src="<?php echo ENV_WEBROOT_FULL_URL ?>/js/jquery.select2/select2-spinner.gif"></span></label>
			</div>
			<div class="col-xs-2">              
              	<?php 
        			  	echo '<select class="" name="id_ciclo" id="id_ciclo">
        			  	<option value="0">Todos</option>';
        				foreach ($listar_ciclo as $v){
							$str_ciclo_actual = '';
							foreach ($arr_ciclos_actuales as $ca){
								if($v['0']['idciclo']==$ca['0']['idciclo']){
									$str_ciclo_actual = ' (Actual)';
								}
							}
							if($idciclo==$v['0']['idciclo']){
								echo '<option value='.$v['0']['idciclo'].' selected>'.$v['0']['idciclo'].' - '.$v['0']['nombciclo'].$str_ciclo_actual.'</option>';
							}else{
								echo '<option value='.$v['0']['idciclo'].'>'.$v['0']['idciclo'].' - '.$v['0']['nombciclo'].$str_ciclo_actual.'</option>';
							}
        				}
        				echo '</select>';	
        		?> 
		  </div>
			<div class="col-xs-2">
				<button class="btn btn-primary" id="btn_reload_dashboard">Buscar</button>
			</div>
		</div>
	</div>

	<h2 class="text-center" style="margin-top: 5px;">DASHBOARD DE CONTROL
		DE LECTURAS</h2>

	<div class='row'>
		
		<div class='col-sm-12 col-md-12'>
			<div class='block-flat' style="padding-top: 0px;">
				<div id="preview_panel_monitoreo" class='content overflow-hidden'>
					<div style="text-align: center;">
						<img src="<?php echo ENV_WEBROOT_FULL_URL ?>/js/jquery.select2/select2-spinner.gif">
					</div>
				</div>
				<div class="actualizacion"  style="text-align: right;">
                    &Uacute;ltima actualizaci&oacute;n: 
                    <span class="badge badge-primary label_last_update"><?php echo date('Y-m-d H:i:s');?></span>
                </div>
				<script type="text/javascript">
				$('#preview_panel_monitoreo').load(ENV_WEBROOT_FULL_URL+"Dashboard/monitor_progreso_epie<?php echo $url_completa;?>");
				setInterval(function(){
					$.ajax({
						url : ENV_WEBROOT_FULL_URL+'ComlecOrdenlecturas/get_last_event_update_lectura',
						type: "POST",
						dataType: 'json',
						success:function(data, textStatus, jqXHR){
							console.log(data);
							if(data.modified==true){
								$('#preview_panel_monitoreo').load(ENV_WEBROOT_FULL_URL+"Dashboard/monitor_progreso_epie<?php echo $url_completa;?>");
								if(data.str_tiempo==''){
									$('.label_last_update').html('hace instantes.');
								}else{
									$('.label_last_update').html('hace '+data.str_tiempo);
								}
							}else{
								if(data.str_tiempo==''){
									$('.label_last_update').html('hace instantes.');
								}else{
									$('.label_last_update').html('hace '+data.str_tiempo);
								}
							}
						}
					});
				}, 15000);
				</script>
			</div>
		</div>

		<div class='col-sm-6 col-md-6'>

			<div class='block-flat'>
				<div class='header'>
					<h3>KPI 1 - Indicadores Clave de Proceso</h3>
				</div>
				<div class='content'>
					<div class='content no-padding'>
						<table class='red'>
							<thead>
								<tr>
									<th></th>
									<th class='right'><span></span>D&iacute;a Actual</th>
									<th class='right'><span></span>Periodo Actual</th>
									<th class='right'><span></span>KPI</th>
									<th class='right'><span></span>Periodo Anterior</th>
									<th class='right'><span></span>KPI</th>
								</tr>
							</thead>
							<tbody class='no-border-x'>
								<tr>
									<td style='width: 40%;'><i class='fa fa-tasks'></i> Suministros</td>
									<td class='text-right'><?php echo number_format($cantidad_ordenes_dia_actual);?></td>
									<td class='text-right'><?php echo number_format($cantidad_ordenes_periodo_actual);?></td>
									<td class='text-right'>
								<?php 
								if($cantidad_inconsistentes_periodo_actual==0){
									echo "<img src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAB8ElEQVR42mNgoDb4/OfD/9UPpv6vv5z4v+C83/+sM17/c0+F/595o/f/u+/v/uPVfOHrgf8114L+L33f+H/j997/s98V/O95Hvu/5UHI/5wzLv+dtuv+3/Vy23+cmuvvRvw/9H/h/6U/qv53fgj93/DM93/VA/f/Jbcd/xdctfmffcH8v+kaqf/bnqMZAnJ22RW///v+zP0/52f+/76vUXADah97gQ3IvWz1P/OcKRgrLZNG9c6K+1P+z3lZDrcZhFtfB4E1V9xz/V980wFsQNoZ4/9xJ/T/W62T+d9+oh1hQOm5iP8bPvX9b30ZCMfozgfZnAjUHHlM+7/HdpX/NkusEQYknHACuwCkAWQrzGYQBmkGYZjtQYc0/1tvlv0vN1kSYUDkYdv/Ex6lgG1DxjDNyLb77FcDB6T0BAmEATGHvP933ouCa4BhUKiDMLJmj70q/7Xmi/63mm2JMKD7Yis4nmGhjIzjoJoDD2qANYNsl53C+79lbwvCAFCUGG5QBfsTZBsMgzSC/AyzGeR3ldlC/yXbRDFT5bon68DxC9OErNFpjyLYZpBmgWbO/+uurcOeGtc8WPNfbr44OJ6dtyuCMUij1kJRsLPFW0Vwa4aBF19f/K/eW/3faLbBf9FuATA2mqz/v2ZrzX+QHNVzLwAjy7LsIPIjLQAAAABJRU5ErkJggg=='>";
								}elseif($cantidad_inconsistentes_periodo_actual>0){
									echo "<img src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAABrElEQVR42q2TyytEURzH/QFW1JRHIxsTUiNWHgtCFOWRPEpZyAJZkKSZhccgsVBIFqRkZdKUZCFFNLEQJUnJQk0eGcQCk+bj/O6dyZ3rsXLr272dcz7f3/d3zrlRUf/98PIIK1Mw0gzOKnCU6+/FUYJPD/wNH23DcB14F+DcA7sz4B5ScK8+XmuDvXV+h6da4GkfzpZhywWePljqhLlWmGiEwWoCFdbvJlpsNcm1MjmZVwnGfzaQdpT8RdbIdrSe16fhcFEHRWsOtRfdsNCuJ5MWxKCrgMeadPzTowYDp5o8dutQWAKb4tNTqhm8NmdxWZVnMFCDbM3ogFECTzZFVKctl7eGDE6zEwwGalDbbQHCkthSOQyHqstaaeHYHmcw6CjSj0oAoyS2CaY1i6vCJI7Kcr4MgrMD+mK5MEYJaIKlusS/GHMZDNSRvFWmfFUzgyE4UJ/GZW48ezYLAf+96S5srvJckhgJhUCRFBDYmxyNf83z820Mbrjx5cVpMeWoZLfl26cujsTeSbH8Doefj7sbfCMOToozOUyN5cAWw0G+nbN+J+93t/z73/sJB/zttLkU5gUAAAAASUVORK5CYII='>";
								}else{
									echo "<img src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAABpUlEQVR42q2TyytEURzH/QFWRCFTVhJF2WFla0XJwkKxlJ2VRxI1lA2RlRRWnqVGiRqPmJJXYqKGPEOGYdIwg/Nxzpm55ozByq1v3c6538fve85NSPjvhzcfnPTBYQ3slyP2ymC3Ajx21N7fZN8S4qgKAkO8M4N4HQBvB9w26nXhzIabOX4lc1qHn3WCjMFHJ7w0S4EGvc5JtRYJztviRXRsGTeAU5KH+KALQh0xAsJTiXCXadyN22S6h6iImlk89krX4bCz5f7YKN3qowK6j1IeHLncu+yGgCopNBEmWVBkI75KqMgKgeVCPCMlpkBppLCGWFzXaugC3WF3tovxL+aw05NmCMhF3XaE8AVVXGR2y53NQj1CrIDakEelo5pQsY3o2kgKnI3a2OovigqIg/boxyYsokH2ObJw96fgcXQaAvJI1FxWy3HECPllKY/j4XRW2lIJPXu/3YWLaZ5mM+IcLShnRV5rTcS7O/3zbRSnk5yPpOk0odV87ajeL8czdWxnU+rvZOt5f77haqGZvcECNuzJuNqTcHUX4J5qIei/5d//3k/ScyAkq+CgnAAAAABJRU5ErkJggg=='>";
								}
								?>
								</td>
									<td class='text-right'><?php echo number_format($cantidad_ordenes_periodo_anterior);?></td>
									<td class='text-right'>
								<?php 
								if($cantidad_inconsistentes_periodo_anterior==0){
									echo "<img src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAB8ElEQVR42mNgoDb4/OfD/9UPpv6vv5z4v+C83/+sM17/c0+F/595o/f/u+/v/uPVfOHrgf8114L+L33f+H/j997/s98V/O95Hvu/5UHI/5wzLv+dtuv+3/Vy23+cmuvvRvw/9H/h/6U/qv53fgj93/DM93/VA/f/Jbcd/xdctfmffcH8v+kaqf/bnqMZAnJ22RW///v+zP0/52f+/76vUXADah97gQ3IvWz1P/OcKRgrLZNG9c6K+1P+z3lZDrcZhFtfB4E1V9xz/V980wFsQNoZ4/9xJ/T/W62T+d9+oh1hQOm5iP8bPvX9b30ZCMfozgfZnAjUHHlM+7/HdpX/NkusEQYknHACuwCkAWQrzGYQBmkGYZjtQYc0/1tvlv0vN1kSYUDkYdv/Ex6lgG1DxjDNyLb77FcDB6T0BAmEATGHvP933ouCa4BhUKiDMLJmj70q/7Xmi/63mm2JMKD7Yis4nmGhjIzjoJoDD2qANYNsl53C+79lbwvCAFCUGG5QBfsTZBsMgzSC/AyzGeR3ldlC/yXbRDFT5bon68DxC9OErNFpjyLYZpBmgWbO/+uurcOeGtc8WPNfbr44OJ6dtyuCMUij1kJRsLPFW0Vwa4aBF19f/K/eW/3faLbBf9FuATA2mqz/v2ZrzX+QHNVzLwAjy7LsIPIjLQAAAABJRU5ErkJggg=='>";
								}elseif($cantidad_inconsistentes_periodo_anterior>0){
									echo "<img src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAABrElEQVR42q2TyytEURzH/QFW1JRHIxsTUiNWHgtCFOWRPEpZyAJZkKSZhccgsVBIFqRkZdKUZCFFNLEQJUnJQk0eGcQCk+bj/O6dyZ3rsXLr272dcz7f3/d3zrlRUf/98PIIK1Mw0gzOKnCU6+/FUYJPD/wNH23DcB14F+DcA7sz4B5ScK8+XmuDvXV+h6da4GkfzpZhywWePljqhLlWmGiEwWoCFdbvJlpsNcm1MjmZVwnGfzaQdpT8RdbIdrSe16fhcFEHRWsOtRfdsNCuJ5MWxKCrgMeadPzTowYDp5o8dutQWAKb4tNTqhm8NmdxWZVnMFCDbM3ogFECTzZFVKctl7eGDE6zEwwGalDbbQHCkthSOQyHqstaaeHYHmcw6CjSj0oAoyS2CaY1i6vCJI7Kcr4MgrMD+mK5MEYJaIKlusS/GHMZDNSRvFWmfFUzgyE4UJ/GZW48ezYLAf+96S5srvJckhgJhUCRFBDYmxyNf83z820Mbrjx5cVpMeWoZLfl26cujsTeSbH8Doefj7sbfCMOToozOUyN5cAWw0G+nbN+J+93t/z73/sJB/zttLkU5gUAAAAASUVORK5CYII='>";
								}else{
									echo "<img src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAABpUlEQVR42q2TyytEURzH/QFWRCFTVhJF2WFla0XJwkKxlJ2VRxI1lA2RlRRWnqVGiRqPmJJXYqKGPEOGYdIwg/Nxzpm55ozByq1v3c6538fve85NSPjvhzcfnPTBYQ3slyP2ymC3Ajx21N7fZN8S4qgKAkO8M4N4HQBvB9w26nXhzIabOX4lc1qHn3WCjMFHJ7w0S4EGvc5JtRYJztviRXRsGTeAU5KH+KALQh0xAsJTiXCXadyN22S6h6iImlk89krX4bCz5f7YKN3qowK6j1IeHLncu+yGgCopNBEmWVBkI75KqMgKgeVCPCMlpkBppLCGWFzXaugC3WF3tovxL+aw05NmCMhF3XaE8AVVXGR2y53NQj1CrIDakEelo5pQsY3o2kgKnI3a2OovigqIg/boxyYsokH2ObJw96fgcXQaAvJI1FxWy3HECPllKY/j4XRW2lIJPXu/3YWLaZ5mM+IcLShnRV5rTcS7O/3zbRSnk5yPpOk0odV87ajeL8czdWxnU+rvZOt5f77haqGZvcECNuzJuNqTcHUX4J5qIei/5d//3k/ScyAkq+CgnAAAAABJRU5ErkJggg=='>";
								}
								?>
								</td>
								</tr>
								<tr>
									<td style='width: 40%;'><i class='fa fa-sitemap'></i> Asignadas</td>
									<td class='text-right'><?php echo number_format($cantidad_asignados_dia_actual);?></td>
									<td class='text-right'><?php echo number_format($cantidad_asignados_periodo_actual);?></td>
									<td class='text-right'>
								<?php 
								if($cantidad_ordenes_periodo_actual==$cantidad_asignados_periodo_actual){
									echo "<img src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAB8ElEQVR42mNgoDb4/OfD/9UPpv6vv5z4v+C83/+sM17/c0+F/595o/f/u+/v/uPVfOHrgf8114L+L33f+H/j997/s98V/O95Hvu/5UHI/5wzLv+dtuv+3/Vy23+cmuvvRvw/9H/h/6U/qv53fgj93/DM93/VA/f/Jbcd/xdctfmffcH8v+kaqf/bnqMZAnJ22RW///v+zP0/52f+/76vUXADah97gQ3IvWz1P/OcKRgrLZNG9c6K+1P+z3lZDrcZhFtfB4E1V9xz/V980wFsQNoZ4/9xJ/T/W62T+d9+oh1hQOm5iP8bPvX9b30ZCMfozgfZnAjUHHlM+7/HdpX/NkusEQYknHACuwCkAWQrzGYQBmkGYZjtQYc0/1tvlv0vN1kSYUDkYdv/Ex6lgG1DxjDNyLb77FcDB6T0BAmEATGHvP933ouCa4BhUKiDMLJmj70q/7Xmi/63mm2JMKD7Yis4nmGhjIzjoJoDD2qANYNsl53C+79lbwvCAFCUGG5QBfsTZBsMgzSC/AyzGeR3ldlC/yXbRDFT5bon68DxC9OErNFpjyLYZpBmgWbO/+uurcOeGtc8WPNfbr44OJ6dtyuCMUij1kJRsLPFW0Vwa4aBF19f/K/eW/3faLbBf9FuATA2mqz/v2ZrzX+QHNVzLwAjy7LsIPIjLQAAAABJRU5ErkJggg=='>";
								}elseif($cantidad_asignados_periodo_actual<$cantidad_ordenes_periodo_actual){
									echo "<img src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAABrElEQVR42q2TyytEURzH/QFW1JRHIxsTUiNWHgtCFOWRPEpZyAJZkKSZhccgsVBIFqRkZdKUZCFFNLEQJUnJQk0eGcQCk+bj/O6dyZ3rsXLr272dcz7f3/d3zrlRUf/98PIIK1Mw0gzOKnCU6+/FUYJPD/wNH23DcB14F+DcA7sz4B5ScK8+XmuDvXV+h6da4GkfzpZhywWePljqhLlWmGiEwWoCFdbvJlpsNcm1MjmZVwnGfzaQdpT8RdbIdrSe16fhcFEHRWsOtRfdsNCuJ5MWxKCrgMeadPzTowYDp5o8dutQWAKb4tNTqhm8NmdxWZVnMFCDbM3ogFECTzZFVKctl7eGDE6zEwwGalDbbQHCkthSOQyHqstaaeHYHmcw6CjSj0oAoyS2CaY1i6vCJI7Kcr4MgrMD+mK5MEYJaIKlusS/GHMZDNSRvFWmfFUzgyE4UJ/GZW48ezYLAf+96S5srvJckhgJhUCRFBDYmxyNf83z820Mbrjx5cVpMeWoZLfl26cujsTeSbH8Doefj7sbfCMOToozOUyN5cAWw0G+nbN+J+93t/z73/sJB/zttLkU5gUAAAAASUVORK5CYII='>";
								}else{
									echo "<img src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAABpUlEQVR42q2TyytEURzH/QFWRCFTVhJF2WFla0XJwkKxlJ2VRxI1lA2RlRRWnqVGiRqPmJJXYqKGPEOGYdIwg/Nxzpm55ozByq1v3c6538fve85NSPjvhzcfnPTBYQ3slyP2ymC3Ajx21N7fZN8S4qgKAkO8M4N4HQBvB9w26nXhzIabOX4lc1qHn3WCjMFHJ7w0S4EGvc5JtRYJztviRXRsGTeAU5KH+KALQh0xAsJTiXCXadyN22S6h6iImlk89krX4bCz5f7YKN3qowK6j1IeHLncu+yGgCopNBEmWVBkI75KqMgKgeVCPCMlpkBppLCGWFzXaugC3WF3tovxL+aw05NmCMhF3XaE8AVVXGR2y53NQj1CrIDakEelo5pQsY3o2kgKnI3a2OovigqIg/boxyYsokH2ObJw96fgcXQaAvJI1FxWy3HECPllKY/j4XRW2lIJPXu/3YWLaZ5mM+IcLShnRV5rTcS7O/3zbRSnk5yPpOk0odV87ajeL8czdWxnU+rvZOt5f77haqGZvcECNuzJuNqTcHUX4J5qIei/5d//3k/ScyAkq+CgnAAAAABJRU5ErkJggg=='>";
								}
								?>
								</td>
									<td class='text-right'><?php echo number_format($cantidad_asignados_periodo_anterior);?></td>
									<td class='text-right'>
								<?php 
								if($cantidad_ordenes_periodo_anterior==$cantidad_asignados_periodo_anterior){
									echo "<img src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAB8ElEQVR42mNgoDb4/OfD/9UPpv6vv5z4v+C83/+sM17/c0+F/595o/f/u+/v/uPVfOHrgf8114L+L33f+H/j997/s98V/O95Hvu/5UHI/5wzLv+dtuv+3/Vy23+cmuvvRvw/9H/h/6U/qv53fgj93/DM93/VA/f/Jbcd/xdctfmffcH8v+kaqf/bnqMZAnJ22RW///v+zP0/52f+/76vUXADah97gQ3IvWz1P/OcKRgrLZNG9c6K+1P+z3lZDrcZhFtfB4E1V9xz/V980wFsQNoZ4/9xJ/T/W62T+d9+oh1hQOm5iP8bPvX9b30ZCMfozgfZnAjUHHlM+7/HdpX/NkusEQYknHACuwCkAWQrzGYQBmkGYZjtQYc0/1tvlv0vN1kSYUDkYdv/Ex6lgG1DxjDNyLb77FcDB6T0BAmEATGHvP933ouCa4BhUKiDMLJmj70q/7Xmi/63mm2JMKD7Yis4nmGhjIzjoJoDD2qANYNsl53C+79lbwvCAFCUGG5QBfsTZBsMgzSC/AyzGeR3ldlC/yXbRDFT5bon68DxC9OErNFpjyLYZpBmgWbO/+uurcOeGtc8WPNfbr44OJ6dtyuCMUij1kJRsLPFW0Vwa4aBF19f/K/eW/3faLbBf9FuATA2mqz/v2ZrzX+QHNVzLwAjy7LsIPIjLQAAAABJRU5ErkJggg=='>";
								}elseif($cantidad_asignados_periodo_anterior<$cantidad_ordenes_periodo_anterior){
									echo "<img src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAABrElEQVR42q2TyytEURzH/QFW1JRHIxsTUiNWHgtCFOWRPEpZyAJZkKSZhccgsVBIFqRkZdKUZCFFNLEQJUnJQk0eGcQCk+bj/O6dyZ3rsXLr272dcz7f3/d3zrlRUf/98PIIK1Mw0gzOKnCU6+/FUYJPD/wNH23DcB14F+DcA7sz4B5ScK8+XmuDvXV+h6da4GkfzpZhywWePljqhLlWmGiEwWoCFdbvJlpsNcm1MjmZVwnGfzaQdpT8RdbIdrSe16fhcFEHRWsOtRfdsNCuJ5MWxKCrgMeadPzTowYDp5o8dutQWAKb4tNTqhm8NmdxWZVnMFCDbM3ogFECTzZFVKctl7eGDE6zEwwGalDbbQHCkthSOQyHqstaaeHYHmcw6CjSj0oAoyS2CaY1i6vCJI7Kcr4MgrMD+mK5MEYJaIKlusS/GHMZDNSRvFWmfFUzgyE4UJ/GZW48ezYLAf+96S5srvJckhgJhUCRFBDYmxyNf83z820Mbrjx5cVpMeWoZLfl26cujsTeSbH8Doefj7sbfCMOToozOUyN5cAWw0G+nbN+J+93t/z73/sJB/zttLkU5gUAAAAASUVORK5CYII='>";
								}else{
									echo "<img src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAABpUlEQVR42q2TyytEURzH/QFWRCFTVhJF2WFla0XJwkKxlJ2VRxI1lA2RlRRWnqVGiRqPmJJXYqKGPEOGYdIwg/Nxzpm55ozByq1v3c6538fve85NSPjvhzcfnPTBYQ3slyP2ymC3Ajx21N7fZN8S4qgKAkO8M4N4HQBvB9w26nXhzIabOX4lc1qHn3WCjMFHJ7w0S4EGvc5JtRYJztviRXRsGTeAU5KH+KALQh0xAsJTiXCXadyN22S6h6iImlk89krX4bCz5f7YKN3qowK6j1IeHLncu+yGgCopNBEmWVBkI75KqMgKgeVCPCMlpkBppLCGWFzXaugC3WF3tovxL+aw05NmCMhF3XaE8AVVXGR2y53NQj1CrIDakEelo5pQsY3o2kgKnI3a2OovigqIg/boxyYsokH2ObJw96fgcXQaAvJI1FxWy3HECPllKY/j4XRW2lIJPXu/3YWLaZ5mM+IcLShnRV5rTcS7O/3zbRSnk5yPpOk0odV87ajeL8czdWxnU+rvZOt5f77haqGZvcECNuzJuNqTcHUX4J5qIei/5d//3k/ScyAkq+CgnAAAAABJRU5ErkJggg=='>";
								}
								?>
								</td>
								</tr>
								<tr>
									<td><i class='fa fa-mobile'></i> Descargadas</td>
									<td class='text-right'><?php echo number_format($cantidad_descargados_dia_actual);?></td>
									<td class='text-right'><?php echo number_format($cantidad_descargados_periodo_actual);?></td>
									<td class='text-right'>
								<?php 
								if($cantidad_ordenes_periodo_actual==$cantidad_descargados_periodo_actual){
									echo "<img src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAB8ElEQVR42mNgoDb4/OfD/9UPpv6vv5z4v+C83/+sM17/c0+F/595o/f/u+/v/uPVfOHrgf8114L+L33f+H/j997/s98V/O95Hvu/5UHI/5wzLv+dtuv+3/Vy23+cmuvvRvw/9H/h/6U/qv53fgj93/DM93/VA/f/Jbcd/xdctfmffcH8v+kaqf/bnqMZAnJ22RW///v+zP0/52f+/76vUXADah97gQ3IvWz1P/OcKRgrLZNG9c6K+1P+z3lZDrcZhFtfB4E1V9xz/V980wFsQNoZ4/9xJ/T/W62T+d9+oh1hQOm5iP8bPvX9b30ZCMfozgfZnAjUHHlM+7/HdpX/NkusEQYknHACuwCkAWQrzGYQBmkGYZjtQYc0/1tvlv0vN1kSYUDkYdv/Ex6lgG1DxjDNyLb77FcDB6T0BAmEATGHvP933ouCa4BhUKiDMLJmj70q/7Xmi/63mm2JMKD7Yis4nmGhjIzjoJoDD2qANYNsl53C+79lbwvCAFCUGG5QBfsTZBsMgzSC/AyzGeR3ldlC/yXbRDFT5bon68DxC9OErNFpjyLYZpBmgWbO/+uurcOeGtc8WPNfbr44OJ6dtyuCMUij1kJRsLPFW0Vwa4aBF19f/K/eW/3faLbBf9FuATA2mqz/v2ZrzX+QHNVzLwAjy7LsIPIjLQAAAABJRU5ErkJggg=='>";
								}elseif($cantidad_descargados_periodo_actual<$cantidad_ordenes_periodo_actual){
									echo "<img src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAABrElEQVR42q2TyytEURzH/QFW1JRHIxsTUiNWHgtCFOWRPEpZyAJZkKSZhccgsVBIFqRkZdKUZCFFNLEQJUnJQk0eGcQCk+bj/O6dyZ3rsXLr272dcz7f3/d3zrlRUf/98PIIK1Mw0gzOKnCU6+/FUYJPD/wNH23DcB14F+DcA7sz4B5ScK8+XmuDvXV+h6da4GkfzpZhywWePljqhLlWmGiEwWoCFdbvJlpsNcm1MjmZVwnGfzaQdpT8RdbIdrSe16fhcFEHRWsOtRfdsNCuJ5MWxKCrgMeadPzTowYDp5o8dutQWAKb4tNTqhm8NmdxWZVnMFCDbM3ogFECTzZFVKctl7eGDE6zEwwGalDbbQHCkthSOQyHqstaaeHYHmcw6CjSj0oAoyS2CaY1i6vCJI7Kcr4MgrMD+mK5MEYJaIKlusS/GHMZDNSRvFWmfFUzgyE4UJ/GZW48ezYLAf+96S5srvJckhgJhUCRFBDYmxyNf83z820Mbrjx5cVpMeWoZLfl26cujsTeSbH8Doefj7sbfCMOToozOUyN5cAWw0G+nbN+J+93t/z73/sJB/zttLkU5gUAAAAASUVORK5CYII='>";
								}else{
									echo "<img src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAABpUlEQVR42q2TyytEURzH/QFWRCFTVhJF2WFla0XJwkKxlJ2VRxI1lA2RlRRWnqVGiRqPmJJXYqKGPEOGYdIwg/Nxzpm55ozByq1v3c6538fve85NSPjvhzcfnPTBYQ3slyP2ymC3Ajx21N7fZN8S4qgKAkO8M4N4HQBvB9w26nXhzIabOX4lc1qHn3WCjMFHJ7w0S4EGvc5JtRYJztviRXRsGTeAU5KH+KALQh0xAsJTiXCXadyN22S6h6iImlk89krX4bCz5f7YKN3qowK6j1IeHLncu+yGgCopNBEmWVBkI75KqMgKgeVCPCMlpkBppLCGWFzXaugC3WF3tovxL+aw05NmCMhF3XaE8AVVXGR2y53NQj1CrIDakEelo5pQsY3o2kgKnI3a2OovigqIg/boxyYsokH2ObJw96fgcXQaAvJI1FxWy3HECPllKY/j4XRW2lIJPXu/3YWLaZ5mM+IcLShnRV5rTcS7O/3zbRSnk5yPpOk0odV87ajeL8czdWxnU+rvZOt5f77haqGZvcECNuzJuNqTcHUX4J5qIei/5d//3k/ScyAkq+CgnAAAAABJRU5ErkJggg=='>";
								}
								?>
								</td>
									<td class='text-right'><?php echo number_format($cantidad_descargados_periodo_anterior);?></td>
									<td class='text-right'>
								<?php 
								if($cantidad_ordenes_periodo_anterior==$cantidad_descargados_periodo_anterior){
									echo "<img src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAB8ElEQVR42mNgoDb4/OfD/9UPpv6vv5z4v+C83/+sM17/c0+F/595o/f/u+/v/uPVfOHrgf8114L+L33f+H/j997/s98V/O95Hvu/5UHI/5wzLv+dtuv+3/Vy23+cmuvvRvw/9H/h/6U/qv53fgj93/DM93/VA/f/Jbcd/xdctfmffcH8v+kaqf/bnqMZAnJ22RW///v+zP0/52f+/76vUXADah97gQ3IvWz1P/OcKRgrLZNG9c6K+1P+z3lZDrcZhFtfB4E1V9xz/V980wFsQNoZ4/9xJ/T/W62T+d9+oh1hQOm5iP8bPvX9b30ZCMfozgfZnAjUHHlM+7/HdpX/NkusEQYknHACuwCkAWQrzGYQBmkGYZjtQYc0/1tvlv0vN1kSYUDkYdv/Ex6lgG1DxjDNyLb77FcDB6T0BAmEATGHvP933ouCa4BhUKiDMLJmj70q/7Xmi/63mm2JMKD7Yis4nmGhjIzjoJoDD2qANYNsl53C+79lbwvCAFCUGG5QBfsTZBsMgzSC/AyzGeR3ldlC/yXbRDFT5bon68DxC9OErNFpjyLYZpBmgWbO/+uurcOeGtc8WPNfbr44OJ6dtyuCMUij1kJRsLPFW0Vwa4aBF19f/K/eW/3faLbBf9FuATA2mqz/v2ZrzX+QHNVzLwAjy7LsIPIjLQAAAABJRU5ErkJggg=='>";
								}elseif($cantidad_descargados_periodo_anterior<$cantidad_ordenes_periodo_anterior){
									echo "<img src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAABrElEQVR42q2TyytEURzH/QFW1JRHIxsTUiNWHgtCFOWRPEpZyAJZkKSZhccgsVBIFqRkZdKUZCFFNLEQJUnJQk0eGcQCk+bj/O6dyZ3rsXLr272dcz7f3/d3zrlRUf/98PIIK1Mw0gzOKnCU6+/FUYJPD/wNH23DcB14F+DcA7sz4B5ScK8+XmuDvXV+h6da4GkfzpZhywWePljqhLlWmGiEwWoCFdbvJlpsNcm1MjmZVwnGfzaQdpT8RdbIdrSe16fhcFEHRWsOtRfdsNCuJ5MWxKCrgMeadPzTowYDp5o8dutQWAKb4tNTqhm8NmdxWZVnMFCDbM3ogFECTzZFVKctl7eGDE6zEwwGalDbbQHCkthSOQyHqstaaeHYHmcw6CjSj0oAoyS2CaY1i6vCJI7Kcr4MgrMD+mK5MEYJaIKlusS/GHMZDNSRvFWmfFUzgyE4UJ/GZW48ezYLAf+96S5srvJckhgJhUCRFBDYmxyNf83z820Mbrjx5cVpMeWoZLfl26cujsTeSbH8Doefj7sbfCMOToozOUyN5cAWw0G+nbN+J+93t/z73/sJB/zttLkU5gUAAAAASUVORK5CYII='>";
								}else{
									echo "<img src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAABpUlEQVR42q2TyytEURzH/QFWRCFTVhJF2WFla0XJwkKxlJ2VRxI1lA2RlRRWnqVGiRqPmJJXYqKGPEOGYdIwg/Nxzpm55ozByq1v3c6538fve85NSPjvhzcfnPTBYQ3slyP2ymC3Ajx21N7fZN8S4qgKAkO8M4N4HQBvB9w26nXhzIabOX4lc1qHn3WCjMFHJ7w0S4EGvc5JtRYJztviRXRsGTeAU5KH+KALQh0xAsJTiXCXadyN22S6h6iImlk89krX4bCz5f7YKN3qowK6j1IeHLncu+yGgCopNBEmWVBkI75KqMgKgeVCPCMlpkBppLCGWFzXaugC3WF3tovxL+aw05NmCMhF3XaE8AVVXGR2y53NQj1CrIDakEelo5pQsY3o2kgKnI3a2OovigqIg/boxyYsokH2ObJw96fgcXQaAvJI1FxWy3HECPllKY/j4XRW2lIJPXu/3YWLaZ5mM+IcLShnRV5rTcS7O/3zbRSnk5yPpOk0odV87ajeL8czdWxnU+rvZOt5f77haqGZvcECNuzJuNqTcHUX4J5qIei/5d//3k/ScyAkq+CgnAAAAABJRU5ErkJggg=='>";
								}
								?>
								</td>
								</tr>
								<tr>
									<td><i class='fa fa-edit'></i> Finalizadas</td>
									<td class='text-right'><?php echo number_format($cantidad_avanzados_dia_actual);?></td>
									<td class='text-right'><?php echo number_format($cantidad_avanzados_periodo_actual);?></td>
									<td class='text-right'>
								<?php 
								if($cantidad_ordenes_periodo_actual==$cantidad_avanzados_periodo_actual){
									echo "<img src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAB8ElEQVR42mNgoDb4/OfD/9UPpv6vv5z4v+C83/+sM17/c0+F/595o/f/u+/v/uPVfOHrgf8114L+L33f+H/j997/s98V/O95Hvu/5UHI/5wzLv+dtuv+3/Vy23+cmuvvRvw/9H/h/6U/qv53fgj93/DM93/VA/f/Jbcd/xdctfmffcH8v+kaqf/bnqMZAnJ22RW///v+zP0/52f+/76vUXADah97gQ3IvWz1P/OcKRgrLZNG9c6K+1P+z3lZDrcZhFtfB4E1V9xz/V980wFsQNoZ4/9xJ/T/W62T+d9+oh1hQOm5iP8bPvX9b30ZCMfozgfZnAjUHHlM+7/HdpX/NkusEQYknHACuwCkAWQrzGYQBmkGYZjtQYc0/1tvlv0vN1kSYUDkYdv/Ex6lgG1DxjDNyLb77FcDB6T0BAmEATGHvP933ouCa4BhUKiDMLJmj70q/7Xmi/63mm2JMKD7Yis4nmGhjIzjoJoDD2qANYNsl53C+79lbwvCAFCUGG5QBfsTZBsMgzSC/AyzGeR3ldlC/yXbRDFT5bon68DxC9OErNFpjyLYZpBmgWbO/+uurcOeGtc8WPNfbr44OJ6dtyuCMUij1kJRsLPFW0Vwa4aBF19f/K/eW/3faLbBf9FuATA2mqz/v2ZrzX+QHNVzLwAjy7LsIPIjLQAAAABJRU5ErkJggg=='>";
								}elseif($cantidad_avanzados_periodo_actual<$cantidad_ordenes_periodo_actual){
									echo "<img src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAABrElEQVR42q2TyytEURzH/QFW1JRHIxsTUiNWHgtCFOWRPEpZyAJZkKSZhccgsVBIFqRkZdKUZCFFNLEQJUnJQk0eGcQCk+bj/O6dyZ3rsXLr272dcz7f3/d3zrlRUf/98PIIK1Mw0gzOKnCU6+/FUYJPD/wNH23DcB14F+DcA7sz4B5ScK8+XmuDvXV+h6da4GkfzpZhywWePljqhLlWmGiEwWoCFdbvJlpsNcm1MjmZVwnGfzaQdpT8RdbIdrSe16fhcFEHRWsOtRfdsNCuJ5MWxKCrgMeadPzTowYDp5o8dutQWAKb4tNTqhm8NmdxWZVnMFCDbM3ogFECTzZFVKctl7eGDE6zEwwGalDbbQHCkthSOQyHqstaaeHYHmcw6CjSj0oAoyS2CaY1i6vCJI7Kcr4MgrMD+mK5MEYJaIKlusS/GHMZDNSRvFWmfFUzgyE4UJ/GZW48ezYLAf+96S5srvJckhgJhUCRFBDYmxyNf83z820Mbrjx5cVpMeWoZLfl26cujsTeSbH8Doefj7sbfCMOToozOUyN5cAWw0G+nbN+J+93t/z73/sJB/zttLkU5gUAAAAASUVORK5CYII='>";
								}else{
									echo "<img src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAABpUlEQVR42q2TyytEURzH/QFWRCFTVhJF2WFla0XJwkKxlJ2VRxI1lA2RlRRWnqVGiRqPmJJXYqKGPEOGYdIwg/Nxzpm55ozByq1v3c6538fve85NSPjvhzcfnPTBYQ3slyP2ymC3Ajx21N7fZN8S4qgKAkO8M4N4HQBvB9w26nXhzIabOX4lc1qHn3WCjMFHJ7w0S4EGvc5JtRYJztviRXRsGTeAU5KH+KALQh0xAsJTiXCXadyN22S6h6iImlk89krX4bCz5f7YKN3qowK6j1IeHLncu+yGgCopNBEmWVBkI75KqMgKgeVCPCMlpkBppLCGWFzXaugC3WF3tovxL+aw05NmCMhF3XaE8AVVXGR2y53NQj1CrIDakEelo5pQsY3o2kgKnI3a2OovigqIg/boxyYsokH2ObJw96fgcXQaAvJI1FxWy3HECPllKY/j4XRW2lIJPXu/3YWLaZ5mM+IcLShnRV5rTcS7O/3zbRSnk5yPpOk0odV87ajeL8czdWxnU+rvZOt5f77haqGZvcECNuzJuNqTcHUX4J5qIei/5d//3k/ScyAkq+CgnAAAAABJRU5ErkJggg=='>";
								}
								?>
								</td>
									<td class='text-right'><?php echo number_format($cantidad_avanzados_periodo_anterior);?></td>
									<td class='text-right'>
								<?php 
								if($cantidad_ordenes_periodo_anterior==$cantidad_avanzados_periodo_anterior){
									echo "<img src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAB8ElEQVR42mNgoDb4/OfD/9UPpv6vv5z4v+C83/+sM17/c0+F/595o/f/u+/v/uPVfOHrgf8114L+L33f+H/j997/s98V/O95Hvu/5UHI/5wzLv+dtuv+3/Vy23+cmuvvRvw/9H/h/6U/qv53fgj93/DM93/VA/f/Jbcd/xdctfmffcH8v+kaqf/bnqMZAnJ22RW///v+zP0/52f+/76vUXADah97gQ3IvWz1P/OcKRgrLZNG9c6K+1P+z3lZDrcZhFtfB4E1V9xz/V980wFsQNoZ4/9xJ/T/W62T+d9+oh1hQOm5iP8bPvX9b30ZCMfozgfZnAjUHHlM+7/HdpX/NkusEQYknHACuwCkAWQrzGYQBmkGYZjtQYc0/1tvlv0vN1kSYUDkYdv/Ex6lgG1DxjDNyLb77FcDB6T0BAmEATGHvP933ouCa4BhUKiDMLJmj70q/7Xmi/63mm2JMKD7Yis4nmGhjIzjoJoDD2qANYNsl53C+79lbwvCAFCUGG5QBfsTZBsMgzSC/AyzGeR3ldlC/yXbRDFT5bon68DxC9OErNFpjyLYZpBmgWbO/+uurcOeGtc8WPNfbr44OJ6dtyuCMUij1kJRsLPFW0Vwa4aBF19f/K/eW/3faLbBf9FuATA2mqz/v2ZrzX+QHNVzLwAjy7LsIPIjLQAAAABJRU5ErkJggg=='>";
								}elseif($cantidad_avanzados_periodo_anterior<$cantidad_ordenes_periodo_anterior){
									echo "<img src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAABrElEQVR42q2TyytEURzH/QFW1JRHIxsTUiNWHgtCFOWRPEpZyAJZkKSZhccgsVBIFqRkZdKUZCFFNLEQJUnJQk0eGcQCk+bj/O6dyZ3rsXLr272dcz7f3/d3zrlRUf/98PIIK1Mw0gzOKnCU6+/FUYJPD/wNH23DcB14F+DcA7sz4B5ScK8+XmuDvXV+h6da4GkfzpZhywWePljqhLlWmGiEwWoCFdbvJlpsNcm1MjmZVwnGfzaQdpT8RdbIdrSe16fhcFEHRWsOtRfdsNCuJ5MWxKCrgMeadPzTowYDp5o8dutQWAKb4tNTqhm8NmdxWZVnMFCDbM3ogFECTzZFVKctl7eGDE6zEwwGalDbbQHCkthSOQyHqstaaeHYHmcw6CjSj0oAoyS2CaY1i6vCJI7Kcr4MgrMD+mK5MEYJaIKlusS/GHMZDNSRvFWmfFUzgyE4UJ/GZW48ezYLAf+96S5srvJckhgJhUCRFBDYmxyNf83z820Mbrjx5cVpMeWoZLfl26cujsTeSbH8Doefj7sbfCMOToozOUyN5cAWw0G+nbN+J+93t/z73/sJB/zttLkU5gUAAAAASUVORK5CYII='>";
								}else{
									echo "<img src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAABpUlEQVR42q2TyytEURzH/QFWRCFTVhJF2WFla0XJwkKxlJ2VRxI1lA2RlRRWnqVGiRqPmJJXYqKGPEOGYdIwg/Nxzpm55ozByq1v3c6538fve85NSPjvhzcfnPTBYQ3slyP2ymC3Ajx21N7fZN8S4qgKAkO8M4N4HQBvB9w26nXhzIabOX4lc1qHn3WCjMFHJ7w0S4EGvc5JtRYJztviRXRsGTeAU5KH+KALQh0xAsJTiXCXadyN22S6h6iImlk89krX4bCz5f7YKN3qowK6j1IeHLncu+yGgCopNBEmWVBkI75KqMgKgeVCPCMlpkBppLCGWFzXaugC3WF3tovxL+aw05NmCMhF3XaE8AVVXGR2y53NQj1CrIDakEelo5pQsY3o2kgKnI3a2OovigqIg/boxyYsokH2ObJw96fgcXQaAvJI1FxWy3HECPllKY/j4XRW2lIJPXu/3YWLaZ5mM+IcLShnRV5rTcS7O/3zbRSnk5yPpOk0odV87ajeL8czdWxnU+rvZOt5f77haqGZvcECNuzJuNqTcHUX4J5qIei/5d//3k/ScyAkq+CgnAAAAABJRU5ErkJggg=='>";
								}
								?>
								</td>
								</tr>
								<tr>
									<td><i class='fa fa-ban'></i> Inconsistentes</td>
									<td class='text-right'><a
										href="<?php echo ENV_WEBROOT_FULL_URL;?>EvaluarInconsistencias/evaluar_inconsistencia<?php echo ($url_completa!=''?'/U/'.date('Y-m-d').'/'.date('Y-m-d').$url_completa:'');?>"
										target="_blank"><?php echo number_format($cantidad_inconsistentes_dia_actual);?></a> de <?php echo number_format($cantidad_inconsistentes_dia_actual+$cantidad_inconsistentes_evaluadas_dia_actual);?></td>
									<td class='text-right'><a
										href="<?php echo ENV_WEBROOT_FULL_URL;?>EvaluarInconsistencias/evaluar_inconsistencia<?php echo ($url_completa!=''?'/U/'.date('Y-m-d').'/'.date('Y-m-d').$url_completa:'');?>"
										target="_blank"><?php echo number_format($cantidad_inconsistentes_periodo_actual);?></a> de <?php echo number_format($cantidad_inconsistentes_periodo_actual+$cantidad_inconsistentes_evaluadas_periodo_actual);?></td>
									<td class='text-right'>
								<?php 
								if($cantidad_inconsistentes_periodo_actual==0){
									echo "<img src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAB8ElEQVR42mNgoDb4/OfD/9UPpv6vv5z4v+C83/+sM17/c0+F/595o/f/u+/v/uPVfOHrgf8114L+L33f+H/j997/s98V/O95Hvu/5UHI/5wzLv+dtuv+3/Vy23+cmuvvRvw/9H/h/6U/qv53fgj93/DM93/VA/f/Jbcd/xdctfmffcH8v+kaqf/bnqMZAnJ22RW///v+zP0/52f+/76vUXADah97gQ3IvWz1P/OcKRgrLZNG9c6K+1P+z3lZDrcZhFtfB4E1V9xz/V980wFsQNoZ4/9xJ/T/W62T+d9+oh1hQOm5iP8bPvX9b30ZCMfozgfZnAjUHHlM+7/HdpX/NkusEQYknHACuwCkAWQrzGYQBmkGYZjtQYc0/1tvlv0vN1kSYUDkYdv/Ex6lgG1DxjDNyLb77FcDB6T0BAmEATGHvP933ouCa4BhUKiDMLJmj70q/7Xmi/63mm2JMKD7Yis4nmGhjIzjoJoDD2qANYNsl53C+79lbwvCAFCUGG5QBfsTZBsMgzSC/AyzGeR3ldlC/yXbRDFT5bon68DxC9OErNFpjyLYZpBmgWbO/+uurcOeGtc8WPNfbr44OJ6dtyuCMUij1kJRsLPFW0Vwa4aBF19f/K/eW/3faLbBf9FuATA2mqz/v2ZrzX+QHNVzLwAjy7LsIPIjLQAAAABJRU5ErkJggg=='>";
								}elseif($cantidad_inconsistentes_periodo_actual>0){
									echo "<img src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAABrElEQVR42q2TyytEURzH/QFW1JRHIxsTUiNWHgtCFOWRPEpZyAJZkKSZhccgsVBIFqRkZdKUZCFFNLEQJUnJQk0eGcQCk+bj/O6dyZ3rsXLr272dcz7f3/d3zrlRUf/98PIIK1Mw0gzOKnCU6+/FUYJPD/wNH23DcB14F+DcA7sz4B5ScK8+XmuDvXV+h6da4GkfzpZhywWePljqhLlWmGiEwWoCFdbvJlpsNcm1MjmZVwnGfzaQdpT8RdbIdrSe16fhcFEHRWsOtRfdsNCuJ5MWxKCrgMeadPzTowYDp5o8dutQWAKb4tNTqhm8NmdxWZVnMFCDbM3ogFECTzZFVKctl7eGDE6zEwwGalDbbQHCkthSOQyHqstaaeHYHmcw6CjSj0oAoyS2CaY1i6vCJI7Kcr4MgrMD+mK5MEYJaIKlusS/GHMZDNSRvFWmfFUzgyE4UJ/GZW48ezYLAf+96S5srvJckhgJhUCRFBDYmxyNf83z820Mbrjx5cVpMeWoZLfl26cujsTeSbH8Doefj7sbfCMOToozOUyN5cAWw0G+nbN+J+93t/z73/sJB/zttLkU5gUAAAAASUVORK5CYII='>";
								}else{
									echo "<img src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAABpUlEQVR42q2TyytEURzH/QFWRCFTVhJF2WFla0XJwkKxlJ2VRxI1lA2RlRRWnqVGiRqPmJJXYqKGPEOGYdIwg/Nxzpm55ozByq1v3c6538fve85NSPjvhzcfnPTBYQ3slyP2ymC3Ajx21N7fZN8S4qgKAkO8M4N4HQBvB9w26nXhzIabOX4lc1qHn3WCjMFHJ7w0S4EGvc5JtRYJztviRXRsGTeAU5KH+KALQh0xAsJTiXCXadyN22S6h6iImlk89krX4bCz5f7YKN3qowK6j1IeHLncu+yGgCopNBEmWVBkI75KqMgKgeVCPCMlpkBppLCGWFzXaugC3WF3tovxL+aw05NmCMhF3XaE8AVVXGR2y53NQj1CrIDakEelo5pQsY3o2kgKnI3a2OovigqIg/boxyYsokH2ObJw96fgcXQaAvJI1FxWy3HECPllKY/j4XRW2lIJPXu/3YWLaZ5mM+IcLShnRV5rTcS7O/3zbRSnk5yPpOk0odV87ajeL8czdWxnU+rvZOt5f77haqGZvcECNuzJuNqTcHUX4J5qIei/5d//3k/ScyAkq+CgnAAAAABJRU5ErkJggg=='>";
								}
								?>
								</td>
									<td class='text-right'><?php echo number_format($cantidad_inconsistentes_periodo_anterior);?> de <?php echo number_format($cantidad_inconsistentes_periodo_anterior+$cantidad_inconsistentes_evaluadas_periodo_anterior);?></td>
									<td class='text-right'>
								<?php 
								if($cantidad_inconsistentes_periodo_anterior==0){
									echo "<img src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAB8ElEQVR42mNgoDb4/OfD/9UPpv6vv5z4v+C83/+sM17/c0+F/595o/f/u+/v/uPVfOHrgf8114L+L33f+H/j997/s98V/O95Hvu/5UHI/5wzLv+dtuv+3/Vy23+cmuvvRvw/9H/h/6U/qv53fgj93/DM93/VA/f/Jbcd/xdctfmffcH8v+kaqf/bnqMZAnJ22RW///v+zP0/52f+/76vUXADah97gQ3IvWz1P/OcKRgrLZNG9c6K+1P+z3lZDrcZhFtfB4E1V9xz/V980wFsQNoZ4/9xJ/T/W62T+d9+oh1hQOm5iP8bPvX9b30ZCMfozgfZnAjUHHlM+7/HdpX/NkusEQYknHACuwCkAWQrzGYQBmkGYZjtQYc0/1tvlv0vN1kSYUDkYdv/Ex6lgG1DxjDNyLb77FcDB6T0BAmEATGHvP933ouCa4BhUKiDMLJmj70q/7Xmi/63mm2JMKD7Yis4nmGhjIzjoJoDD2qANYNsl53C+79lbwvCAFCUGG5QBfsTZBsMgzSC/AyzGeR3ldlC/yXbRDFT5bon68DxC9OErNFpjyLYZpBmgWbO/+uurcOeGtc8WPNfbr44OJ6dtyuCMUij1kJRsLPFW0Vwa4aBF19f/K/eW/3faLbBf9FuATA2mqz/v2ZrzX+QHNVzLwAjy7LsIPIjLQAAAABJRU5ErkJggg=='>";
								}elseif($cantidad_inconsistentes_periodo_anterior>0){
									echo "<img src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAABrElEQVR42q2TyytEURzH/QFW1JRHIxsTUiNWHgtCFOWRPEpZyAJZkKSZhccgsVBIFqRkZdKUZCFFNLEQJUnJQk0eGcQCk+bj/O6dyZ3rsXLr272dcz7f3/d3zrlRUf/98PIIK1Mw0gzOKnCU6+/FUYJPD/wNH23DcB14F+DcA7sz4B5ScK8+XmuDvXV+h6da4GkfzpZhywWePljqhLlWmGiEwWoCFdbvJlpsNcm1MjmZVwnGfzaQdpT8RdbIdrSe16fhcFEHRWsOtRfdsNCuJ5MWxKCrgMeadPzTowYDp5o8dutQWAKb4tNTqhm8NmdxWZVnMFCDbM3ogFECTzZFVKctl7eGDE6zEwwGalDbbQHCkthSOQyHqstaaeHYHmcw6CjSj0oAoyS2CaY1i6vCJI7Kcr4MgrMD+mK5MEYJaIKlusS/GHMZDNSRvFWmfFUzgyE4UJ/GZW48ezYLAf+96S5srvJckhgJhUCRFBDYmxyNf83z820Mbrjx5cVpMeWoZLfl26cujsTeSbH8Doefj7sbfCMOToozOUyN5cAWw0G+nbN+J+93t/z73/sJB/zttLkU5gUAAAAASUVORK5CYII='>";
								}else{
									echo "<img src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAABpUlEQVR42q2TyytEURzH/QFWRCFTVhJF2WFla0XJwkKxlJ2VRxI1lA2RlRRWnqVGiRqPmJJXYqKGPEOGYdIwg/Nxzpm55ozByq1v3c6538fve85NSPjvhzcfnPTBYQ3slyP2ymC3Ajx21N7fZN8S4qgKAkO8M4N4HQBvB9w26nXhzIabOX4lc1qHn3WCjMFHJ7w0S4EGvc5JtRYJztviRXRsGTeAU5KH+KALQh0xAsJTiXCXadyN22S6h6iImlk89krX4bCz5f7YKN3qowK6j1IeHLncu+yGgCopNBEmWVBkI75KqMgKgeVCPCMlpkBppLCGWFzXaugC3WF3tovxL+aw05NmCMhF3XaE8AVVXGR2y53NQj1CrIDakEelo5pQsY3o2kgKnI3a2OovigqIg/boxyYsokH2ObJw96fgcXQaAvJI1FxWy3HECPllKY/j4XRW2lIJPXu/3YWLaZ5mM+IcLShnRV5rTcS7O/3zbRSnk5yPpOk0odV87ajeL8czdWxnU+rvZOt5f77haqGZvcECNuzJuNqTcHUX4J5qIei/5d//3k/ScyAkq+CgnAAAAABJRU5ErkJggg=='>";
								}
								?>
								</td>
								</tr>
								<tr>
									<td><i class='fa fa-check'></i> Consistentes</td>
									<td class='text-right'><?php echo number_format($cantidad_consistentes_dia_actual);?></td>
									<td class='text-right'><?php echo number_format($cantidad_consistentes_periodo_actual);?></td>
									<td class='text-right'>
								<?php 
								if($cantidad_ordenes_periodo_actual==$cantidad_consistentes_periodo_actual){
									echo "<img src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAB8ElEQVR42mNgoDb4/OfD/9UPpv6vv5z4v+C83/+sM17/c0+F/595o/f/u+/v/uPVfOHrgf8114L+L33f+H/j997/s98V/O95Hvu/5UHI/5wzLv+dtuv+3/Vy23+cmuvvRvw/9H/h/6U/qv53fgj93/DM93/VA/f/Jbcd/xdctfmffcH8v+kaqf/bnqMZAnJ22RW///v+zP0/52f+/76vUXADah97gQ3IvWz1P/OcKRgrLZNG9c6K+1P+z3lZDrcZhFtfB4E1V9xz/V980wFsQNoZ4/9xJ/T/W62T+d9+oh1hQOm5iP8bPvX9b30ZCMfozgfZnAjUHHlM+7/HdpX/NkusEQYknHACuwCkAWQrzGYQBmkGYZjtQYc0/1tvlv0vN1kSYUDkYdv/Ex6lgG1DxjDNyLb77FcDB6T0BAmEATGHvP933ouCa4BhUKiDMLJmj70q/7Xmi/63mm2JMKD7Yis4nmGhjIzjoJoDD2qANYNsl53C+79lbwvCAFCUGG5QBfsTZBsMgzSC/AyzGeR3ldlC/yXbRDFT5bon68DxC9OErNFpjyLYZpBmgWbO/+uurcOeGtc8WPNfbr44OJ6dtyuCMUij1kJRsLPFW0Vwa4aBF19f/K/eW/3faLbBf9FuATA2mqz/v2ZrzX+QHNVzLwAjy7LsIPIjLQAAAABJRU5ErkJggg=='>";
								}elseif($cantidad_consistentes_periodo_actual<$cantidad_ordenes_periodo_actual){
									echo "<img src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAABrElEQVR42q2TyytEURzH/QFW1JRHIxsTUiNWHgtCFOWRPEpZyAJZkKSZhccgsVBIFqRkZdKUZCFFNLEQJUnJQk0eGcQCk+bj/O6dyZ3rsXLr272dcz7f3/d3zrlRUf/98PIIK1Mw0gzOKnCU6+/FUYJPD/wNH23DcB14F+DcA7sz4B5ScK8+XmuDvXV+h6da4GkfzpZhywWePljqhLlWmGiEwWoCFdbvJlpsNcm1MjmZVwnGfzaQdpT8RdbIdrSe16fhcFEHRWsOtRfdsNCuJ5MWxKCrgMeadPzTowYDp5o8dutQWAKb4tNTqhm8NmdxWZVnMFCDbM3ogFECTzZFVKctl7eGDE6zEwwGalDbbQHCkthSOQyHqstaaeHYHmcw6CjSj0oAoyS2CaY1i6vCJI7Kcr4MgrMD+mK5MEYJaIKlusS/GHMZDNSRvFWmfFUzgyE4UJ/GZW48ezYLAf+96S5srvJckhgJhUCRFBDYmxyNf83z820Mbrjx5cVpMeWoZLfl26cujsTeSbH8Doefj7sbfCMOToozOUyN5cAWw0G+nbN+J+93t/z73/sJB/zttLkU5gUAAAAASUVORK5CYII='>";
								}else{
									echo "<img src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAABpUlEQVR42q2TyytEURzH/QFWRCFTVhJF2WFla0XJwkKxlJ2VRxI1lA2RlRRWnqVGiRqPmJJXYqKGPEOGYdIwg/Nxzpm55ozByq1v3c6538fve85NSPjvhzcfnPTBYQ3slyP2ymC3Ajx21N7fZN8S4qgKAkO8M4N4HQBvB9w26nXhzIabOX4lc1qHn3WCjMFHJ7w0S4EGvc5JtRYJztviRXRsGTeAU5KH+KALQh0xAsJTiXCXadyN22S6h6iImlk89krX4bCz5f7YKN3qowK6j1IeHLncu+yGgCopNBEmWVBkI75KqMgKgeVCPCMlpkBppLCGWFzXaugC3WF3tovxL+aw05NmCMhF3XaE8AVVXGR2y53NQj1CrIDakEelo5pQsY3o2kgKnI3a2OovigqIg/boxyYsokH2ObJw96fgcXQaAvJI1FxWy3HECPllKY/j4XRW2lIJPXu/3YWLaZ5mM+IcLShnRV5rTcS7O/3zbRSnk5yPpOk0odV87ajeL8czdWxnU+rvZOt5f77haqGZvcECNuzJuNqTcHUX4J5qIei/5d//3k/ScyAkq+CgnAAAAABJRU5ErkJggg=='>";
								}
								?>
								</td>
									<td class='text-right'><?php echo number_format($cantidad_consistentes_periodo_anterior);?></td>
									<td class='text-right'>
								<?php 
								if($cantidad_ordenes_periodo_anterior==$cantidad_consistentes_periodo_anterior){
									echo "<img src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAB8ElEQVR42mNgoDb4/OfD/9UPpv6vv5z4v+C83/+sM17/c0+F/595o/f/u+/v/uPVfOHrgf8114L+L33f+H/j997/s98V/O95Hvu/5UHI/5wzLv+dtuv+3/Vy23+cmuvvRvw/9H/h/6U/qv53fgj93/DM93/VA/f/Jbcd/xdctfmffcH8v+kaqf/bnqMZAnJ22RW///v+zP0/52f+/76vUXADah97gQ3IvWz1P/OcKRgrLZNG9c6K+1P+z3lZDrcZhFtfB4E1V9xz/V980wFsQNoZ4/9xJ/T/W62T+d9+oh1hQOm5iP8bPvX9b30ZCMfozgfZnAjUHHlM+7/HdpX/NkusEQYknHACuwCkAWQrzGYQBmkGYZjtQYc0/1tvlv0vN1kSYUDkYdv/Ex6lgG1DxjDNyLb77FcDB6T0BAmEATGHvP933ouCa4BhUKiDMLJmj70q/7Xmi/63mm2JMKD7Yis4nmGhjIzjoJoDD2qANYNsl53C+79lbwvCAFCUGG5QBfsTZBsMgzSC/AyzGeR3ldlC/yXbRDFT5bon68DxC9OErNFpjyLYZpBmgWbO/+uurcOeGtc8WPNfbr44OJ6dtyuCMUij1kJRsLPFW0Vwa4aBF19f/K/eW/3faLbBf9FuATA2mqz/v2ZrzX+QHNVzLwAjy7LsIPIjLQAAAABJRU5ErkJggg=='>";
								}elseif($cantidad_consistentes_periodo_anterior<$cantidad_ordenes_periodo_anterior){
									echo "<img src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAABrElEQVR42q2TyytEURzH/QFW1JRHIxsTUiNWHgtCFOWRPEpZyAJZkKSZhccgsVBIFqRkZdKUZCFFNLEQJUnJQk0eGcQCk+bj/O6dyZ3rsXLr272dcz7f3/d3zrlRUf/98PIIK1Mw0gzOKnCU6+/FUYJPD/wNH23DcB14F+DcA7sz4B5ScK8+XmuDvXV+h6da4GkfzpZhywWePljqhLlWmGiEwWoCFdbvJlpsNcm1MjmZVwnGfzaQdpT8RdbIdrSe16fhcFEHRWsOtRfdsNCuJ5MWxKCrgMeadPzTowYDp5o8dutQWAKb4tNTqhm8NmdxWZVnMFCDbM3ogFECTzZFVKctl7eGDE6zEwwGalDbbQHCkthSOQyHqstaaeHYHmcw6CjSj0oAoyS2CaY1i6vCJI7Kcr4MgrMD+mK5MEYJaIKlusS/GHMZDNSRvFWmfFUzgyE4UJ/GZW48ezYLAf+96S5srvJckhgJhUCRFBDYmxyNf83z820Mbrjx5cVpMeWoZLfl26cujsTeSbH8Doefj7sbfCMOToozOUyN5cAWw0G+nbN+J+93t/z73/sJB/zttLkU5gUAAAAASUVORK5CYII='>";
								}else{
									echo "<img src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAABpUlEQVR42q2TyytEURzH/QFWRCFTVhJF2WFla0XJwkKxlJ2VRxI1lA2RlRRWnqVGiRqPmJJXYqKGPEOGYdIwg/Nxzpm55ozByq1v3c6538fve85NSPjvhzcfnPTBYQ3slyP2ymC3Ajx21N7fZN8S4qgKAkO8M4N4HQBvB9w26nXhzIabOX4lc1qHn3WCjMFHJ7w0S4EGvc5JtRYJztviRXRsGTeAU5KH+KALQh0xAsJTiXCXadyN22S6h6iImlk89krX4bCz5f7YKN3qowK6j1IeHLncu+yGgCopNBEmWVBkI75KqMgKgeVCPCMlpkBppLCGWFzXaugC3WF3tovxL+aw05NmCMhF3XaE8AVVXGR2y53NQj1CrIDakEelo5pQsY3o2kgKnI3a2OovigqIg/boxyYsokH2ObJw96fgcXQaAvJI1FxWy3HECPllKY/j4XRW2lIJPXu/3YWLaZ5mM+IcLShnRV5rTcS7O/3zbRSnk5yPpOk0odV87ajeL8czdWxnU+rvZOt5f77haqGZvcECNuzJuNqTcHUX4J5qIei/5d//3k/ScyAkq+CgnAAAAABJRU5ErkJggg=='>";
								}
								?>
								</td>
								</tr>
								<tr>
									<td><i class='fa fa-question'></i> Pendientes</td>
									<td class='text-right'><?php echo number_format($cantidad_ordenes_dia_actual-$cantidad_avanzados_dia_actual);?></td>
									<td class='text-right'><?php echo number_format($cantidad_ordenes_periodo_actual-$cantidad_avanzados_periodo_actual);?></td>
									<td class='text-right'>
								<?php 
								if(($cantidad_ordenes_periodo_actual-$cantidad_avanzados_periodo_actual)==0){
									echo "<img src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAB8ElEQVR42mNgoDb4/OfD/9UPpv6vv5z4v+C83/+sM17/c0+F/595o/f/u+/v/uPVfOHrgf8114L+L33f+H/j997/s98V/O95Hvu/5UHI/5wzLv+dtuv+3/Vy23+cmuvvRvw/9H/h/6U/qv53fgj93/DM93/VA/f/Jbcd/xdctfmffcH8v+kaqf/bnqMZAnJ22RW///v+zP0/52f+/76vUXADah97gQ3IvWz1P/OcKRgrLZNG9c6K+1P+z3lZDrcZhFtfB4E1V9xz/V980wFsQNoZ4/9xJ/T/W62T+d9+oh1hQOm5iP8bPvX9b30ZCMfozgfZnAjUHHlM+7/HdpX/NkusEQYknHACuwCkAWQrzGYQBmkGYZjtQYc0/1tvlv0vN1kSYUDkYdv/Ex6lgG1DxjDNyLb77FcDB6T0BAmEATGHvP933ouCa4BhUKiDMLJmj70q/7Xmi/63mm2JMKD7Yis4nmGhjIzjoJoDD2qANYNsl53C+79lbwvCAFCUGG5QBfsTZBsMgzSC/AyzGeR3ldlC/yXbRDFT5bon68DxC9OErNFpjyLYZpBmgWbO/+uurcOeGtc8WPNfbr44OJ6dtyuCMUij1kJRsLPFW0Vwa4aBF19f/K/eW/3faLbBf9FuATA2mqz/v2ZrzX+QHNVzLwAjy7LsIPIjLQAAAABJRU5ErkJggg=='>";
								}elseif(($cantidad_ordenes_periodo_actual-$cantidad_avanzados_periodo_actual)>0){
									echo "<img src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAABrElEQVR42q2TyytEURzH/QFW1JRHIxsTUiNWHgtCFOWRPEpZyAJZkKSZhccgsVBIFqRkZdKUZCFFNLEQJUnJQk0eGcQCk+bj/O6dyZ3rsXLr272dcz7f3/d3zrlRUf/98PIIK1Mw0gzOKnCU6+/FUYJPD/wNH23DcB14F+DcA7sz4B5ScK8+XmuDvXV+h6da4GkfzpZhywWePljqhLlWmGiEwWoCFdbvJlpsNcm1MjmZVwnGfzaQdpT8RdbIdrSe16fhcFEHRWsOtRfdsNCuJ5MWxKCrgMeadPzTowYDp5o8dutQWAKb4tNTqhm8NmdxWZVnMFCDbM3ogFECTzZFVKctl7eGDE6zEwwGalDbbQHCkthSOQyHqstaaeHYHmcw6CjSj0oAoyS2CaY1i6vCJI7Kcr4MgrMD+mK5MEYJaIKlusS/GHMZDNSRvFWmfFUzgyE4UJ/GZW48ezYLAf+96S5srvJckhgJhUCRFBDYmxyNf83z820Mbrjx5cVpMeWoZLfl26cujsTeSbH8Doefj7sbfCMOToozOUyN5cAWw0G+nbN+J+93t/z73/sJB/zttLkU5gUAAAAASUVORK5CYII='>";
								}else{
									echo "<img src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAABpUlEQVR42q2TyytEURzH/QFWRCFTVhJF2WFla0XJwkKxlJ2VRxI1lA2RlRRWnqVGiRqPmJJXYqKGPEOGYdIwg/Nxzpm55ozByq1v3c6538fve85NSPjvhzcfnPTBYQ3slyP2ymC3Ajx21N7fZN8S4qgKAkO8M4N4HQBvB9w26nXhzIabOX4lc1qHn3WCjMFHJ7w0S4EGvc5JtRYJztviRXRsGTeAU5KH+KALQh0xAsJTiXCXadyN22S6h6iImlk89krX4bCz5f7YKN3qowK6j1IeHLncu+yGgCopNBEmWVBkI75KqMgKgeVCPCMlpkBppLCGWFzXaugC3WF3tovxL+aw05NmCMhF3XaE8AVVXGR2y53NQj1CrIDakEelo5pQsY3o2kgKnI3a2OovigqIg/boxyYsokH2ObJw96fgcXQaAvJI1FxWy3HECPllKY/j4XRW2lIJPXu/3YWLaZ5mM+IcLShnRV5rTcS7O/3zbRSnk5yPpOk0odV87ajeL8czdWxnU+rvZOt5f77haqGZvcECNuzJuNqTcHUX4J5qIei/5d//3k/ScyAkq+CgnAAAAABJRU5ErkJggg=='>";
								}
								?>
								</td>
									<td class='text-right'><?php echo number_format($cantidad_ordenes_periodo_anterior-$cantidad_avanzados_periodo_anterior);?></td>
									<td class='text-right'>
								<?php 
								if(($cantidad_ordenes_periodo_anterior-$cantidad_avanzados_periodo_anterior)==0){
									echo "<img src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAB8ElEQVR42mNgoDb4/OfD/9UPpv6vv5z4v+C83/+sM17/c0+F/595o/f/u+/v/uPVfOHrgf8114L+L33f+H/j997/s98V/O95Hvu/5UHI/5wzLv+dtuv+3/Vy23+cmuvvRvw/9H/h/6U/qv53fgj93/DM93/VA/f/Jbcd/xdctfmffcH8v+kaqf/bnqMZAnJ22RW///v+zP0/52f+/76vUXADah97gQ3IvWz1P/OcKRgrLZNG9c6K+1P+z3lZDrcZhFtfB4E1V9xz/V980wFsQNoZ4/9xJ/T/W62T+d9+oh1hQOm5iP8bPvX9b30ZCMfozgfZnAjUHHlM+7/HdpX/NkusEQYknHACuwCkAWQrzGYQBmkGYZjtQYc0/1tvlv0vN1kSYUDkYdv/Ex6lgG1DxjDNyLb77FcDB6T0BAmEATGHvP933ouCa4BhUKiDMLJmj70q/7Xmi/63mm2JMKD7Yis4nmGhjIzjoJoDD2qANYNsl53C+79lbwvCAFCUGG5QBfsTZBsMgzSC/AyzGeR3ldlC/yXbRDFT5bon68DxC9OErNFpjyLYZpBmgWbO/+uurcOeGtc8WPNfbr44OJ6dtyuCMUij1kJRsLPFW0Vwa4aBF19f/K/eW/3faLbBf9FuATA2mqz/v2ZrzX+QHNVzLwAjy7LsIPIjLQAAAABJRU5ErkJggg=='>";
								}elseif(($cantidad_ordenes_periodo_anterior-$cantidad_avanzados_periodo_anterior)>0){
									echo "<img src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAABrElEQVR42q2TyytEURzH/QFW1JRHIxsTUiNWHgtCFOWRPEpZyAJZkKSZhccgsVBIFqRkZdKUZCFFNLEQJUnJQk0eGcQCk+bj/O6dyZ3rsXLr272dcz7f3/d3zrlRUf/98PIIK1Mw0gzOKnCU6+/FUYJPD/wNH23DcB14F+DcA7sz4B5ScK8+XmuDvXV+h6da4GkfzpZhywWePljqhLlWmGiEwWoCFdbvJlpsNcm1MjmZVwnGfzaQdpT8RdbIdrSe16fhcFEHRWsOtRfdsNCuJ5MWxKCrgMeadPzTowYDp5o8dutQWAKb4tNTqhm8NmdxWZVnMFCDbM3ogFECTzZFVKctl7eGDE6zEwwGalDbbQHCkthSOQyHqstaaeHYHmcw6CjSj0oAoyS2CaY1i6vCJI7Kcr4MgrMD+mK5MEYJaIKlusS/GHMZDNSRvFWmfFUzgyE4UJ/GZW48ezYLAf+96S5srvJckhgJhUCRFBDYmxyNf83z820Mbrjx5cVpMeWoZLfl26cujsTeSbH8Doefj7sbfCMOToozOUyN5cAWw0G+nbN+J+93t/z73/sJB/zttLkU5gUAAAAASUVORK5CYII='>";
								}else{
									echo "<img src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAABpUlEQVR42q2TyytEURzH/QFWRCFTVhJF2WFla0XJwkKxlJ2VRxI1lA2RlRRWnqVGiRqPmJJXYqKGPEOGYdIwg/Nxzpm55ozByq1v3c6538fve85NSPjvhzcfnPTBYQ3slyP2ymC3Ajx21N7fZN8S4qgKAkO8M4N4HQBvB9w26nXhzIabOX4lc1qHn3WCjMFHJ7w0S4EGvc5JtRYJztviRXRsGTeAU5KH+KALQh0xAsJTiXCXadyN22S6h6iImlk89krX4bCz5f7YKN3qowK6j1IeHLncu+yGgCopNBEmWVBkI75KqMgKgeVCPCMlpkBppLCGWFzXaugC3WF3tovxL+aw05NmCMhF3XaE8AVVXGR2y53NQj1CrIDakEelo5pQsY3o2kgKnI3a2OovigqIg/boxyYsokH2ObJw96fgcXQaAvJI1FxWy3HECPllKY/j4XRW2lIJPXu/3YWLaZ5mM+IcLShnRV5rTcS7O/3zbRSnk5yPpOk0odV87ajeL8czdWxnU+rvZOt5f77haqGZvcECNuzJuNqTcHUX4J5qIei/5d//3k/ScyAkq+CgnAAAAABJRU5ErkJggg=='>";
								}
								?>
								</td>
								</tr>
								<tr>
									<td><i class='fa fa-bolt'></i> Enviados a Distriluz</td>
									<td class='text-right'><?php echo number_format($cantidad_enviados_distriluz_dia_actual);?></td>
									<td class='text-right'><?php echo number_format($cantidad_enviados_distriluz_periodo_actual);?></td>
									<td class='text-right'>
								<?php 
								if($cantidad_consistentes_periodo_actual==$cantidad_enviados_distriluz_periodo_actual){
									echo "<img src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAB8ElEQVR42mNgoDb4/OfD/9UPpv6vv5z4v+C83/+sM17/c0+F/595o/f/u+/v/uPVfOHrgf8114L+L33f+H/j997/s98V/O95Hvu/5UHI/5wzLv+dtuv+3/Vy23+cmuvvRvw/9H/h/6U/qv53fgj93/DM93/VA/f/Jbcd/xdctfmffcH8v+kaqf/bnqMZAnJ22RW///v+zP0/52f+/76vUXADah97gQ3IvWz1P/OcKRgrLZNG9c6K+1P+z3lZDrcZhFtfB4E1V9xz/V980wFsQNoZ4/9xJ/T/W62T+d9+oh1hQOm5iP8bPvX9b30ZCMfozgfZnAjUHHlM+7/HdpX/NkusEQYknHACuwCkAWQrzGYQBmkGYZjtQYc0/1tvlv0vN1kSYUDkYdv/Ex6lgG1DxjDNyLb77FcDB6T0BAmEATGHvP933ouCa4BhUKiDMLJmj70q/7Xmi/63mm2JMKD7Yis4nmGhjIzjoJoDD2qANYNsl53C+79lbwvCAFCUGG5QBfsTZBsMgzSC/AyzGeR3ldlC/yXbRDFT5bon68DxC9OErNFpjyLYZpBmgWbO/+uurcOeGtc8WPNfbr44OJ6dtyuCMUij1kJRsLPFW0Vwa4aBF19f/K/eW/3faLbBf9FuATA2mqz/v2ZrzX+QHNVzLwAjy7LsIPIjLQAAAABJRU5ErkJggg=='>";
								}elseif($cantidad_enviados_distriluz_periodo_actual<$cantidad_consistentes_periodo_actual){
									echo "<img src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAABrElEQVR42q2TyytEURzH/QFW1JRHIxsTUiNWHgtCFOWRPEpZyAJZkKSZhccgsVBIFqRkZdKUZCFFNLEQJUnJQk0eGcQCk+bj/O6dyZ3rsXLr272dcz7f3/d3zrlRUf/98PIIK1Mw0gzOKnCU6+/FUYJPD/wNH23DcB14F+DcA7sz4B5ScK8+XmuDvXV+h6da4GkfzpZhywWePljqhLlWmGiEwWoCFdbvJlpsNcm1MjmZVwnGfzaQdpT8RdbIdrSe16fhcFEHRWsOtRfdsNCuJ5MWxKCrgMeadPzTowYDp5o8dutQWAKb4tNTqhm8NmdxWZVnMFCDbM3ogFECTzZFVKctl7eGDE6zEwwGalDbbQHCkthSOQyHqstaaeHYHmcw6CjSj0oAoyS2CaY1i6vCJI7Kcr4MgrMD+mK5MEYJaIKlusS/GHMZDNSRvFWmfFUzgyE4UJ/GZW48ezYLAf+96S5srvJckhgJhUCRFBDYmxyNf83z820Mbrjx5cVpMeWoZLfl26cujsTeSbH8Doefj7sbfCMOToozOUyN5cAWw0G+nbN+J+93t/z73/sJB/zttLkU5gUAAAAASUVORK5CYII='>";
								}else{
									echo "<img src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAABpUlEQVR42q2TyytEURzH/QFWRCFTVhJF2WFla0XJwkKxlJ2VRxI1lA2RlRRWnqVGiRqPmJJXYqKGPEOGYdIwg/Nxzpm55ozByq1v3c6538fve85NSPjvhzcfnPTBYQ3slyP2ymC3Ajx21N7fZN8S4qgKAkO8M4N4HQBvB9w26nXhzIabOX4lc1qHn3WCjMFHJ7w0S4EGvc5JtRYJztviRXRsGTeAU5KH+KALQh0xAsJTiXCXadyN22S6h6iImlk89krX4bCz5f7YKN3qowK6j1IeHLncu+yGgCopNBEmWVBkI75KqMgKgeVCPCMlpkBppLCGWFzXaugC3WF3tovxL+aw05NmCMhF3XaE8AVVXGR2y53NQj1CrIDakEelo5pQsY3o2kgKnI3a2OovigqIg/boxyYsokH2ObJw96fgcXQaAvJI1FxWy3HECPllKY/j4XRW2lIJPXu/3YWLaZ5mM+IcLShnRV5rTcS7O/3zbRSnk5yPpOk0odV87ajeL8czdWxnU+rvZOt5f77haqGZvcECNuzJuNqTcHUX4J5qIei/5d//3k/ScyAkq+CgnAAAAABJRU5ErkJggg=='>";
								}
								?>
								</td>
									<td class='text-right'><?php echo number_format($cantidad_enviados_distriluz_periodo_anterior);?></td>
									<td class='text-right'>
								<?php 
								if($cantidad_consistentes_periodo_anterior==$cantidad_enviados_distriluz_periodo_anterior){
									echo "<img src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAB8ElEQVR42mNgoDb4/OfD/9UPpv6vv5z4v+C83/+sM17/c0+F/595o/f/u+/v/uPVfOHrgf8114L+L33f+H/j997/s98V/O95Hvu/5UHI/5wzLv+dtuv+3/Vy23+cmuvvRvw/9H/h/6U/qv53fgj93/DM93/VA/f/Jbcd/xdctfmffcH8v+kaqf/bnqMZAnJ22RW///v+zP0/52f+/76vUXADah97gQ3IvWz1P/OcKRgrLZNG9c6K+1P+z3lZDrcZhFtfB4E1V9xz/V980wFsQNoZ4/9xJ/T/W62T+d9+oh1hQOm5iP8bPvX9b30ZCMfozgfZnAjUHHlM+7/HdpX/NkusEQYknHACuwCkAWQrzGYQBmkGYZjtQYc0/1tvlv0vN1kSYUDkYdv/Ex6lgG1DxjDNyLb77FcDB6T0BAmEATGHvP933ouCa4BhUKiDMLJmj70q/7Xmi/63mm2JMKD7Yis4nmGhjIzjoJoDD2qANYNsl53C+79lbwvCAFCUGG5QBfsTZBsMgzSC/AyzGeR3ldlC/yXbRDFT5bon68DxC9OErNFpjyLYZpBmgWbO/+uurcOeGtc8WPNfbr44OJ6dtyuCMUij1kJRsLPFW0Vwa4aBF19f/K/eW/3faLbBf9FuATA2mqz/v2ZrzX+QHNVzLwAjy7LsIPIjLQAAAABJRU5ErkJggg=='>";
								}elseif($cantidad_enviados_distriluz_periodo_anterior<$cantidad_consistentes_periodo_anterior){
									echo "<img src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAABrElEQVR42q2TyytEURzH/QFW1JRHIxsTUiNWHgtCFOWRPEpZyAJZkKSZhccgsVBIFqRkZdKUZCFFNLEQJUnJQk0eGcQCk+bj/O6dyZ3rsXLr272dcz7f3/d3zrlRUf/98PIIK1Mw0gzOKnCU6+/FUYJPD/wNH23DcB14F+DcA7sz4B5ScK8+XmuDvXV+h6da4GkfzpZhywWePljqhLlWmGiEwWoCFdbvJlpsNcm1MjmZVwnGfzaQdpT8RdbIdrSe16fhcFEHRWsOtRfdsNCuJ5MWxKCrgMeadPzTowYDp5o8dutQWAKb4tNTqhm8NmdxWZVnMFCDbM3ogFECTzZFVKctl7eGDE6zEwwGalDbbQHCkthSOQyHqstaaeHYHmcw6CjSj0oAoyS2CaY1i6vCJI7Kcr4MgrMD+mK5MEYJaIKlusS/GHMZDNSRvFWmfFUzgyE4UJ/GZW48ezYLAf+96S5srvJckhgJhUCRFBDYmxyNf83z820Mbrjx5cVpMeWoZLfl26cujsTeSbH8Doefj7sbfCMOToozOUyN5cAWw0G+nbN+J+93t/z73/sJB/zttLkU5gUAAAAASUVORK5CYII='>";
								}else{
									echo "<img src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAABpUlEQVR42q2TyytEURzH/QFWRCFTVhJF2WFla0XJwkKxlJ2VRxI1lA2RlRRWnqVGiRqPmJJXYqKGPEOGYdIwg/Nxzpm55ozByq1v3c6538fve85NSPjvhzcfnPTBYQ3slyP2ymC3Ajx21N7fZN8S4qgKAkO8M4N4HQBvB9w26nXhzIabOX4lc1qHn3WCjMFHJ7w0S4EGvc5JtRYJztviRXRsGTeAU5KH+KALQh0xAsJTiXCXadyN22S6h6iImlk89krX4bCz5f7YKN3qowK6j1IeHLncu+yGgCopNBEmWVBkI75KqMgKgeVCPCMlpkBppLCGWFzXaugC3WF3tovxL+aw05NmCMhF3XaE8AVVXGR2y53NQj1CrIDakEelo5pQsY3o2kgKnI3a2OovigqIg/boxyYsokH2ObJw96fgcXQaAvJI1FxWy3HECPllKY/j4XRW2lIJPXu/3YWLaZ5mM+IcLShnRV5rTcS7O/3zbRSnk5yPpOk0odV87ajeL8czdWxnU+rvZOt5f77haqGZvcECNuzJuNqTcHUX4J5qIei/5d//3k/ScyAkq+CgnAAAAABJRU5ErkJggg=='>";
								}
								?>
								</td>
								</tr>
								<tr>
									<td><i class='fa fa-print'></i> Cartas Generadas</td>
									<td class='text-right'><?php echo number_format($cantidad_cartas_generadas_dia_actual);?></td>
									<td class='text-right'><?php echo number_format($cantidad_cartas_generadas_periodo_actual);?></td>
									<td class='text-right'>
								<?php 
								if($cantidad_consistentes_periodo_actual>$cantidad_cartas_generadas_periodo_actual){
									echo "<img src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAB8ElEQVR42mNgoDb4/OfD/9UPpv6vv5z4v+C83/+sM17/c0+F/595o/f/u+/v/uPVfOHrgf8114L+L33f+H/j997/s98V/O95Hvu/5UHI/5wzLv+dtuv+3/Vy23+cmuvvRvw/9H/h/6U/qv53fgj93/DM93/VA/f/Jbcd/xdctfmffcH8v+kaqf/bnqMZAnJ22RW///v+zP0/52f+/76vUXADah97gQ3IvWz1P/OcKRgrLZNG9c6K+1P+z3lZDrcZhFtfB4E1V9xz/V980wFsQNoZ4/9xJ/T/W62T+d9+oh1hQOm5iP8bPvX9b30ZCMfozgfZnAjUHHlM+7/HdpX/NkusEQYknHACuwCkAWQrzGYQBmkGYZjtQYc0/1tvlv0vN1kSYUDkYdv/Ex6lgG1DxjDNyLb77FcDB6T0BAmEATGHvP933ouCa4BhUKiDMLJmj70q/7Xmi/63mm2JMKD7Yis4nmGhjIzjoJoDD2qANYNsl53C+79lbwvCAFCUGG5QBfsTZBsMgzSC/AyzGeR3ldlC/yXbRDFT5bon68DxC9OErNFpjyLYZpBmgWbO/+uurcOeGtc8WPNfbr44OJ6dtyuCMUij1kJRsLPFW0Vwa4aBF19f/K/eW/3faLbBf9FuATA2mqz/v2ZrzX+QHNVzLwAjy7LsIPIjLQAAAABJRU5ErkJggg=='>";
								}elseif($cantidad_cartas_generadas_periodo_actual<$cantidad_consistentes_periodo_actual){
									echo "<img src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAABrElEQVR42q2TyytEURzH/QFW1JRHIxsTUiNWHgtCFOWRPEpZyAJZkKSZhccgsVBIFqRkZdKUZCFFNLEQJUnJQk0eGcQCk+bj/O6dyZ3rsXLr272dcz7f3/d3zrlRUf/98PIIK1Mw0gzOKnCU6+/FUYJPD/wNH23DcB14F+DcA7sz4B5ScK8+XmuDvXV+h6da4GkfzpZhywWePljqhLlWmGiEwWoCFdbvJlpsNcm1MjmZVwnGfzaQdpT8RdbIdrSe16fhcFEHRWsOtRfdsNCuJ5MWxKCrgMeadPzTowYDp5o8dutQWAKb4tNTqhm8NmdxWZVnMFCDbM3ogFECTzZFVKctl7eGDE6zEwwGalDbbQHCkthSOQyHqstaaeHYHmcw6CjSj0oAoyS2CaY1i6vCJI7Kcr4MgrMD+mK5MEYJaIKlusS/GHMZDNSRvFWmfFUzgyE4UJ/GZW48ezYLAf+96S5srvJckhgJhUCRFBDYmxyNf83z820Mbrjx5cVpMeWoZLfl26cujsTeSbH8Doefj7sbfCMOToozOUyN5cAWw0G+nbN+J+93t/z73/sJB/zttLkU5gUAAAAASUVORK5CYII='>";
								}else{
									echo "<img src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAABpUlEQVR42q2TyytEURzH/QFWRCFTVhJF2WFla0XJwkKxlJ2VRxI1lA2RlRRWnqVGiRqPmJJXYqKGPEOGYdIwg/Nxzpm55ozByq1v3c6538fve85NSPjvhzcfnPTBYQ3slyP2ymC3Ajx21N7fZN8S4qgKAkO8M4N4HQBvB9w26nXhzIabOX4lc1qHn3WCjMFHJ7w0S4EGvc5JtRYJztviRXRsGTeAU5KH+KALQh0xAsJTiXCXadyN22S6h6iImlk89krX4bCz5f7YKN3qowK6j1IeHLncu+yGgCopNBEmWVBkI75KqMgKgeVCPCMlpkBppLCGWFzXaugC3WF3tovxL+aw05NmCMhF3XaE8AVVXGR2y53NQj1CrIDakEelo5pQsY3o2kgKnI3a2OovigqIg/boxyYsokH2ObJw96fgcXQaAvJI1FxWy3HECPllKY/j4XRW2lIJPXu/3YWLaZ5mM+IcLShnRV5rTcS7O/3zbRSnk5yPpOk0odV87ajeL8czdWxnU+rvZOt5f77haqGZvcECNuzJuNqTcHUX4J5qIei/5d//3k/ScyAkq+CgnAAAAABJRU5ErkJggg=='>";
								}
								?>
								</td>
									<td class='text-right'><?php echo number_format($cantidad_cartas_generadas_periodo_anterior);?></td>
									<td class='text-right'>
								<?php 
								if($cantidad_consistentes_periodo_anterior>$cantidad_cartas_generadas_periodo_anterior){
									echo "<img src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAB8ElEQVR42mNgoDb4/OfD/9UPpv6vv5z4v+C83/+sM17/c0+F/595o/f/u+/v/uPVfOHrgf8114L+L33f+H/j997/s98V/O95Hvu/5UHI/5wzLv+dtuv+3/Vy23+cmuvvRvw/9H/h/6U/qv53fgj93/DM93/VA/f/Jbcd/xdctfmffcH8v+kaqf/bnqMZAnJ22RW///v+zP0/52f+/76vUXADah97gQ3IvWz1P/OcKRgrLZNG9c6K+1P+z3lZDrcZhFtfB4E1V9xz/V980wFsQNoZ4/9xJ/T/W62T+d9+oh1hQOm5iP8bPvX9b30ZCMfozgfZnAjUHHlM+7/HdpX/NkusEQYknHACuwCkAWQrzGYQBmkGYZjtQYc0/1tvlv0vN1kSYUDkYdv/Ex6lgG1DxjDNyLb77FcDB6T0BAmEATGHvP933ouCa4BhUKiDMLJmj70q/7Xmi/63mm2JMKD7Yis4nmGhjIzjoJoDD2qANYNsl53C+79lbwvCAFCUGG5QBfsTZBsMgzSC/AyzGeR3ldlC/yXbRDFT5bon68DxC9OErNFpjyLYZpBmgWbO/+uurcOeGtc8WPNfbr44OJ6dtyuCMUij1kJRsLPFW0Vwa4aBF19f/K/eW/3faLbBf9FuATA2mqz/v2ZrzX+QHNVzLwAjy7LsIPIjLQAAAABJRU5ErkJggg=='>";
								}elseif($cantidad_cartas_generadas_periodo_anterior<$cantidad_consistentes_periodo_anterior){
									echo "<img src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAABrElEQVR42q2TyytEURzH/QFW1JRHIxsTUiNWHgtCFOWRPEpZyAJZkKSZhccgsVBIFqRkZdKUZCFFNLEQJUnJQk0eGcQCk+bj/O6dyZ3rsXLr272dcz7f3/d3zrlRUf/98PIIK1Mw0gzOKnCU6+/FUYJPD/wNH23DcB14F+DcA7sz4B5ScK8+XmuDvXV+h6da4GkfzpZhywWePljqhLlWmGiEwWoCFdbvJlpsNcm1MjmZVwnGfzaQdpT8RdbIdrSe16fhcFEHRWsOtRfdsNCuJ5MWxKCrgMeadPzTowYDp5o8dutQWAKb4tNTqhm8NmdxWZVnMFCDbM3ogFECTzZFVKctl7eGDE6zEwwGalDbbQHCkthSOQyHqstaaeHYHmcw6CjSj0oAoyS2CaY1i6vCJI7Kcr4MgrMD+mK5MEYJaIKlusS/GHMZDNSRvFWmfFUzgyE4UJ/GZW48ezYLAf+96S5srvJckhgJhUCRFBDYmxyNf83z820Mbrjx5cVpMeWoZLfl26cujsTeSbH8Doefj7sbfCMOToozOUyN5cAWw0G+nbN+J+93t/z73/sJB/zttLkU5gUAAAAASUVORK5CYII='>";
								}else{
									echo "<img src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAABpUlEQVR42q2TyytEURzH/QFWRCFTVhJF2WFla0XJwkKxlJ2VRxI1lA2RlRRWnqVGiRqPmJJXYqKGPEOGYdIwg/Nxzpm55ozByq1v3c6538fve85NSPjvhzcfnPTBYQ3slyP2ymC3Ajx21N7fZN8S4qgKAkO8M4N4HQBvB9w26nXhzIabOX4lc1qHn3WCjMFHJ7w0S4EGvc5JtRYJztviRXRsGTeAU5KH+KALQh0xAsJTiXCXadyN22S6h6iImlk89krX4bCz5f7YKN3qowK6j1IeHLncu+yGgCopNBEmWVBkI75KqMgKgeVCPCMlpkBppLCGWFzXaugC3WF3tovxL+aw05NmCMhF3XaE8AVVXGR2y53NQj1CrIDakEelo5pQsY3o2kgKnI3a2OovigqIg/boxyYsokH2ObJw96fgcXQaAvJI1FxWy3HECPllKY/j4XRW2lIJPXu/3YWLaZ5mM+IcLShnRV5rTcS7O/3zbRSnk5yPpOk0odV87ajeL8czdWxnU+rvZOt5f77haqGZvcECNuzJuNqTcHUX4J5qIei/5d//3k/ScyAkq+CgnAAAAABJRU5ErkJggg=='>";
								}
								?>
								</td>
								</tr>
							</tbody>
						</table>
						<div style="text-align: right;">
						<br>
						<span class="lbl_refresh_kpi_dashboard">
						&Uacute;ltima Actualizaci&oacute;n: hace <?php echo $this->Tiempo->getTiempoString($fecha_last_update, date("Y-m-d H:i:s"), "x");?>
						<a href="javascript: void(0);" class="btn_refresh_kpi_dashboard" title="Actualizar datos de KPI"><i class="fa fa-refresh"></i></a>
						</span>
						</div>
					</div>

				</div>
			</div>

		</div>
		
		<div class='col-sm-6 col-md-6'>
			<div class='block-flat'>
				<div class='header'>
					<h3>KPI 2 - Indicadores Clave de Proceso</h3>
				</div>
				<div class='content'>
					<?php 
					if(isset($unidad_neg) && isset($idciclo)){
						$url_unidad_ciclo = '/'.$unidad_neg.'/'.$idciclo;
					}else{
						$url_unidad_ciclo = '/0/0';
					}
					?>
					<table class='red'>
						<thead>
							<tr>
								<th></th>
								<th class='right'><span></span>D&iacute;a Actual</th>
								<th class='right'><span></span>Periodo Actual</th>
								<th class='right'><span></span>Periodo Anterior</th>
								<th class='right'><span></span>KPI</th>
							</tr>
						</thead>
						<tbody class='no-border-x'>
							<tr>
								<td style='width: 40%;'>Lecturas con Observaciones</td>
								<td class='text-right'><a
									href="<?php echo ENV_WEBROOT_FULL_URL;?>EvaluarInconsistencias/evaluar_inconsistencia/U,F/<?php echo date('Y-m-d');?>/<?php echo date('Y-m-d');?><?php echo $url_unidad_ciclo;?>/0/0/0/0/0/OBST"
									target="_blank"><?php echo number_format($cantidad_observaciones_dia_actual);?></a> (<?php echo number_format(($cantidad_ordenes_dia_actual>0?(($cantidad_observaciones_dia_actual*100)/$cantidad_ordenes_dia_actual):0),2);?>%)</td>
								<td class='text-right'><a
									href="<?php echo ENV_WEBROOT_FULL_URL;?>EvaluarInconsistencias/evaluar_inconsistencia/U/<?php echo date('Y-m-d');?>/<?php echo date('Y-m-d');?><?php echo $url_unidad_ciclo;?>/0/0/0/0/0/OBST"
									target="_blank"><?php echo number_format($cantidad_observaciones_periodo_actual);?></a> (<?php echo number_format(($cantidad_ordenes_periodo_actual>0?(($cantidad_observaciones_periodo_actual*100)/$cantidad_ordenes_periodo_actual):0),2);?>%)</td>
								<td class='text-right'><?php echo number_format($cantidad_observaciones_periodo_anterior);?> (<?php echo number_format(($cantidad_ordenes_periodo_anterior>0?(($cantidad_observaciones_periodo_anterior*100)/$cantidad_ordenes_periodo_anterior):0),2);?>%)</td>
								<td class='text-right'>
													<?php
													if ($cantidad_observaciones_periodo_actual < $cantidad_observaciones_periodo_anterior) {
														echo "<img src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAB8ElEQVR42mNgoDb4/OfD/9UPpv6vv5z4v+C83/+sM17/c0+F/595o/f/u+/v/uPVfOHrgf8114L+L33f+H/j997/s98V/O95Hvu/5UHI/5wzLv+dtuv+3/Vy23+cmuvvRvw/9H/h/6U/qv53fgj93/DM93/VA/f/Jbcd/xdctfmffcH8v+kaqf/bnqMZAnJ22RW///v+zP0/52f+/76vUXADah97gQ3IvWz1P/OcKRgrLZNG9c6K+1P+z3lZDrcZhFtfB4E1V9xz/V980wFsQNoZ4/9xJ/T/W62T+d9+oh1hQOm5iP8bPvX9b30ZCMfozgfZnAjUHHlM+7/HdpX/NkusEQYknHACuwCkAWQrzGYQBmkGYZjtQYc0/1tvlv0vN1kSYUDkYdv/Ex6lgG1DxjDNyLb77FcDB6T0BAmEATGHvP933ouCa4BhUKiDMLJmj70q/7Xmi/63mm2JMKD7Yis4nmGhjIzjoJoDD2qANYNsl53C+79lbwvCAFCUGG5QBfsTZBsMgzSC/AyzGeR3ldlC/yXbRDFT5bon68DxC9OErNFpjyLYZpBmgWbO/+uurcOeGtc8WPNfbr44OJ6dtyuCMUij1kJRsLPFW0Vwa4aBF19f/K/eW/3faLbBf9FuATA2mqz/v2ZrzX+QHNVzLwAjy7LsIPIjLQAAAABJRU5ErkJggg=='>";
													} elseif ($cantidad_observaciones_periodo_actual > $cantidad_observaciones_periodo_anterior) {
														echo "<img src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAABrElEQVR42q2TyytEURzH/QFW1JRHIxsTUiNWHgtCFOWRPEpZyAJZkKSZhccgsVBIFqRkZdKUZCFFNLEQJUnJQk0eGcQCk+bj/O6dyZ3rsXLr272dcz7f3/d3zrlRUf/98PIIK1Mw0gzOKnCU6+/FUYJPD/wNH23DcB14F+DcA7sz4B5ScK8+XmuDvXV+h6da4GkfzpZhywWePljqhLlWmGiEwWoCFdbvJlpsNcm1MjmZVwnGfzaQdpT8RdbIdrSe16fhcFEHRWsOtRfdsNCuJ5MWxKCrgMeadPzTowYDp5o8dutQWAKb4tNTqhm8NmdxWZVnMFCDbM3ogFECTzZFVKctl7eGDE6zEwwGalDbbQHCkthSOQyHqstaaeHYHmcw6CjSj0oAoyS2CaY1i6vCJI7Kcr4MgrMD+mK5MEYJaIKlusS/GHMZDNSRvFWmfFUzgyE4UJ/GZW48ezYLAf+96S5srvJckhgJhUCRFBDYmxyNf83z820Mbrjx5cVpMeWoZLfl26cujsTeSbH8Doefj7sbfCMOToozOUyN5cAWw0G+nbN+J+93t/z73/sJB/zttLkU5gUAAAAASUVORK5CYII='>";
													} else {
														echo "<img src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAABpUlEQVR42q2TyytEURzH/QFWRCFTVhJF2WFla0XJwkKxlJ2VRxI1lA2RlRRWnqVGiRqPmJJXYqKGPEOGYdIwg/Nxzpm55ozByq1v3c6538fve85NSPjvhzcfnPTBYQ3slyP2ymC3Ajx21N7fZN8S4qgKAkO8M4N4HQBvB9w26nXhzIabOX4lc1qHn3WCjMFHJ7w0S4EGvc5JtRYJztviRXRsGTeAU5KH+KALQh0xAsJTiXCXadyN22S6h6iImlk89krX4bCz5f7YKN3qowK6j1IeHLncu+yGgCopNBEmWVBkI75KqMgKgeVCPCMlpkBppLCGWFzXaugC3WF3tovxL+aw05NmCMhF3XaE8AVVXGR2y53NQj1CrIDakEelo5pQsY3o2kgKnI3a2OovigqIg/boxyYsokH2ObJw96fgcXQaAvJI1FxWy3HECPllKY/j4XRW2lIJPXu/3YWLaZ5mM+IcLShnRV5rTcS7O/3zbRSnk5yPpOk0odV87ajeL8czdWxnU+rvZOt5f77haqGZvcECNuzJuNqTcHUX4J5qIei/5d//3k/ScyAkq+CgnAAAAABJRU5ErkJggg=='>";
													}
													?>
													</td>
							</tr>
							<tr>
								<td style='width: 40%;'>&nbsp;&nbsp;&nbsp;Observaciones sin
									lectura</td>
								<td class='text-right'><a
									href="<?php echo ENV_WEBROOT_FULL_URL;?>EvaluarInconsistencias/evaluar_inconsistencia/U,F/<?php echo date('Y-m-d');?>/<?php echo date('Y-m-d');?><?php echo $url_unidad_ciclo;?>/0/0/0/0/0/OBSSL"
									target="_blank"><?php echo number_format($cantidad_observaciones_sin_lectura_dia_actual);?></a> (<?php echo number_format(($cantidad_ordenes_dia_actual>0?(($cantidad_observaciones_sin_lectura_dia_actual*100)/$cantidad_ordenes_dia_actual):0),2);?>%)</td>
								<td class='text-right'><a
									href="<?php echo ENV_WEBROOT_FULL_URL;?>EvaluarInconsistencias/evaluar_inconsistencia/U/<?php echo date('Y-m-d');?>/<?php echo date('Y-m-d');?><?php echo $url_unidad_ciclo;?>/0/0/0/0/0/OBSSL"
									target="_blank"><?php echo number_format($cantidad_observaciones_sin_lectura_periodo_actual);?></a> (<?php echo number_format(($cantidad_ordenes_periodo_actual>0?(($cantidad_observaciones_sin_lectura_periodo_actual*100)/$cantidad_ordenes_periodo_actual):0),2);?>%)</td>
								<td class='text-right'><?php echo number_format($cantidad_observaciones_sin_lectura_periodo_anterior);?> (<?php echo number_format(($cantidad_ordenes_periodo_anterior>0?(($cantidad_observaciones_sin_lectura_periodo_anterior*100)/$cantidad_ordenes_periodo_anterior):0),2);?>%)</td>
								<td class='text-right'>
													<?php
													if ($cantidad_observaciones_sin_lectura_periodo_actual < $cantidad_observaciones_sin_lectura_periodo_anterior) {
														echo "<img src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAB8ElEQVR42mNgoDb4/OfD/9UPpv6vv5z4v+C83/+sM17/c0+F/595o/f/u+/v/uPVfOHrgf8114L+L33f+H/j997/s98V/O95Hvu/5UHI/5wzLv+dtuv+3/Vy23+cmuvvRvw/9H/h/6U/qv53fgj93/DM93/VA/f/Jbcd/xdctfmffcH8v+kaqf/bnqMZAnJ22RW///v+zP0/52f+/76vUXADah97gQ3IvWz1P/OcKRgrLZNG9c6K+1P+z3lZDrcZhFtfB4E1V9xz/V980wFsQNoZ4/9xJ/T/W62T+d9+oh1hQOm5iP8bPvX9b30ZCMfozgfZnAjUHHlM+7/HdpX/NkusEQYknHACuwCkAWQrzGYQBmkGYZjtQYc0/1tvlv0vN1kSYUDkYdv/Ex6lgG1DxjDNyLb77FcDB6T0BAmEATGHvP933ouCa4BhUKiDMLJmj70q/7Xmi/63mm2JMKD7Yis4nmGhjIzjoJoDD2qANYNsl53C+79lbwvCAFCUGG5QBfsTZBsMgzSC/AyzGeR3ldlC/yXbRDFT5bon68DxC9OErNFpjyLYZpBmgWbO/+uurcOeGtc8WPNfbr44OJ6dtyuCMUij1kJRsLPFW0Vwa4aBF19f/K/eW/3faLbBf9FuATA2mqz/v2ZrzX+QHNVzLwAjy7LsIPIjLQAAAABJRU5ErkJggg=='>";
													} elseif ($cantidad_observaciones_sin_lectura_periodo_actual > $cantidad_observaciones_sin_lectura_periodo_anterior) {
														echo "<img src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAABrElEQVR42q2TyytEURzH/QFW1JRHIxsTUiNWHgtCFOWRPEpZyAJZkKSZhccgsVBIFqRkZdKUZCFFNLEQJUnJQk0eGcQCk+bj/O6dyZ3rsXLr272dcz7f3/d3zrlRUf/98PIIK1Mw0gzOKnCU6+/FUYJPD/wNH23DcB14F+DcA7sz4B5ScK8+XmuDvXV+h6da4GkfzpZhywWePljqhLlWmGiEwWoCFdbvJlpsNcm1MjmZVwnGfzaQdpT8RdbIdrSe16fhcFEHRWsOtRfdsNCuJ5MWxKCrgMeadPzTowYDp5o8dutQWAKb4tNTqhm8NmdxWZVnMFCDbM3ogFECTzZFVKctl7eGDE6zEwwGalDbbQHCkthSOQyHqstaaeHYHmcw6CjSj0oAoyS2CaY1i6vCJI7Kcr4MgrMD+mK5MEYJaIKlusS/GHMZDNSRvFWmfFUzgyE4UJ/GZW48ezYLAf+96S5srvJckhgJhUCRFBDYmxyNf83z820Mbrjx5cVpMeWoZLfl26cujsTeSbH8Doefj7sbfCMOToozOUyN5cAWw0G+nbN+J+93t/z73/sJB/zttLkU5gUAAAAASUVORK5CYII='>";
													} else {
														echo "<img src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAABpUlEQVR42q2TyytEURzH/QFWRCFTVhJF2WFla0XJwkKxlJ2VRxI1lA2RlRRWnqVGiRqPmJJXYqKGPEOGYdIwg/Nxzpm55ozByq1v3c6538fve85NSPjvhzcfnPTBYQ3slyP2ymC3Ajx21N7fZN8S4qgKAkO8M4N4HQBvB9w26nXhzIabOX4lc1qHn3WCjMFHJ7w0S4EGvc5JtRYJztviRXRsGTeAU5KH+KALQh0xAsJTiXCXadyN22S6h6iImlk89krX4bCz5f7YKN3qowK6j1IeHLncu+yGgCopNBEmWVBkI75KqMgKgeVCPCMlpkBppLCGWFzXaugC3WF3tovxL+aw05NmCMhF3XaE8AVVXGR2y53NQj1CrIDakEelo5pQsY3o2kgKnI3a2OovigqIg/boxyYsokH2ObJw96fgcXQaAvJI1FxWy3HECPllKY/j4XRW2lIJPXu/3YWLaZ5mM+IcLShnRV5rTcS7O/3zbRSnk5yPpOk0odV87ajeL8czdWxnU+rvZOt5f77haqGZvcECNuzJuNqTcHUX4J5qIei/5d//3k/ScyAkq+CgnAAAAABJRU5ErkJggg=='>";
													}
													?>
													</td>
							</tr>
							<tr>
								<td style='width: 40%;'>&nbsp;&nbsp;&nbsp;Observaciones con
									lectura</td>
								<td class='text-right'><a
									href="<?php echo ENV_WEBROOT_FULL_URL;?>EvaluarInconsistencias/evaluar_inconsistencia/U,F/<?php echo date('Y-m-d');?>/<?php echo date('Y-m-d');?><?php echo $url_unidad_ciclo;?>/0/0/0/0/0/OBSCL"
									target="_blank"><?php echo number_format($cantidad_observaciones_con_lectura_dia_actual);?></a> (<?php echo number_format(($cantidad_ordenes_dia_actual>0?(($cantidad_observaciones_con_lectura_dia_actual*100)/$cantidad_ordenes_dia_actual):0),2);?>%)</td>
								<td class='text-right'><a
									href="<?php echo ENV_WEBROOT_FULL_URL;?>EvaluarInconsistencias/evaluar_inconsistencia/U/<?php echo date('Y-m-d');?>/<?php echo date('Y-m-d');?><?php echo $url_unidad_ciclo;?>/0/0/0/0/0/OBSCL"
									target="_blank"><?php echo number_format($cantidad_observaciones_con_lectura_periodo_actual);?></a> (<?php echo number_format(($cantidad_ordenes_periodo_actual>0?(($cantidad_observaciones_con_lectura_periodo_actual*100)/$cantidad_ordenes_periodo_actual):0),2);?>%)</td>
								<td class='text-right'><?php echo number_format($cantidad_observaciones_con_lectura_periodo_anterior);?> (<?php echo number_format(($cantidad_ordenes_periodo_anterior>0?(($cantidad_observaciones_con_lectura_periodo_anterior*100)/$cantidad_ordenes_periodo_anterior):0),2);?>%)</td>
								<td class='text-right'>
													<?php
													if ($cantidad_observaciones_con_lectura_periodo_actual < $cantidad_observaciones_con_lectura_periodo_anterior) {
														echo "<img src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAB8ElEQVR42mNgoDb4/OfD/9UPpv6vv5z4v+C83/+sM17/c0+F/595o/f/u+/v/uPVfOHrgf8114L+L33f+H/j997/s98V/O95Hvu/5UHI/5wzLv+dtuv+3/Vy23+cmuvvRvw/9H/h/6U/qv53fgj93/DM93/VA/f/Jbcd/xdctfmffcH8v+kaqf/bnqMZAnJ22RW///v+zP0/52f+/76vUXADah97gQ3IvWz1P/OcKRgrLZNG9c6K+1P+z3lZDrcZhFtfB4E1V9xz/V980wFsQNoZ4/9xJ/T/W62T+d9+oh1hQOm5iP8bPvX9b30ZCMfozgfZnAjUHHlM+7/HdpX/NkusEQYknHACuwCkAWQrzGYQBmkGYZjtQYc0/1tvlv0vN1kSYUDkYdv/Ex6lgG1DxjDNyLb77FcDB6T0BAmEATGHvP933ouCa4BhUKiDMLJmj70q/7Xmi/63mm2JMKD7Yis4nmGhjIzjoJoDD2qANYNsl53C+79lbwvCAFCUGG5QBfsTZBsMgzSC/AyzGeR3ldlC/yXbRDFT5bon68DxC9OErNFpjyLYZpBmgWbO/+uurcOeGtc8WPNfbr44OJ6dtyuCMUij1kJRsLPFW0Vwa4aBF19f/K/eW/3faLbBf9FuATA2mqz/v2ZrzX+QHNVzLwAjy7LsIPIjLQAAAABJRU5ErkJggg=='>";
													} elseif ($cantidad_observaciones_con_lectura_periodo_actual > $cantidad_observaciones_con_lectura_periodo_anterior) {
														echo "<img src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAABrElEQVR42q2TyytEURzH/QFW1JRHIxsTUiNWHgtCFOWRPEpZyAJZkKSZhccgsVBIFqRkZdKUZCFFNLEQJUnJQk0eGcQCk+bj/O6dyZ3rsXLr272dcz7f3/d3zrlRUf/98PIIK1Mw0gzOKnCU6+/FUYJPD/wNH23DcB14F+DcA7sz4B5ScK8+XmuDvXV+h6da4GkfzpZhywWePljqhLlWmGiEwWoCFdbvJlpsNcm1MjmZVwnGfzaQdpT8RdbIdrSe16fhcFEHRWsOtRfdsNCuJ5MWxKCrgMeadPzTowYDp5o8dutQWAKb4tNTqhm8NmdxWZVnMFCDbM3ogFECTzZFVKctl7eGDE6zEwwGalDbbQHCkthSOQyHqstaaeHYHmcw6CjSj0oAoyS2CaY1i6vCJI7Kcr4MgrMD+mK5MEYJaIKlusS/GHMZDNSRvFWmfFUzgyE4UJ/GZW48ezYLAf+96S5srvJckhgJhUCRFBDYmxyNf83z820Mbrjx5cVpMeWoZLfl26cujsTeSbH8Doefj7sbfCMOToozOUyN5cAWw0G+nbN+J+93t/z73/sJB/zttLkU5gUAAAAASUVORK5CYII='>";
													} else {
														echo "<img src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAABpUlEQVR42q2TyytEURzH/QFWRCFTVhJF2WFla0XJwkKxlJ2VRxI1lA2RlRRWnqVGiRqPmJJXYqKGPEOGYdIwg/Nxzpm55ozByq1v3c6538fve85NSPjvhzcfnPTBYQ3slyP2ymC3Ajx21N7fZN8S4qgKAkO8M4N4HQBvB9w26nXhzIabOX4lc1qHn3WCjMFHJ7w0S4EGvc5JtRYJztviRXRsGTeAU5KH+KALQh0xAsJTiXCXadyN22S6h6iImlk89krX4bCz5f7YKN3qowK6j1IeHLncu+yGgCopNBEmWVBkI75KqMgKgeVCPCMlpkBppLCGWFzXaugC3WF3tovxL+aw05NmCMhF3XaE8AVVXGR2y53NQj1CrIDakEelo5pQsY3o2kgKnI3a2OovigqIg/boxyYsokH2ObJw96fgcXQaAvJI1FxWy3HECPllKY/j4XRW2lIJPXu/3YWLaZ5mM+IcLShnRV5rTcS7O/3zbRSnk5yPpOk0odV87ajeL8czdWxnU+rvZOt5f77haqGZvcECNuzJuNqTcHUX4J5qIei/5d//3k/ScyAkq+CgnAAAAABJRU5ErkJggg=='>";
													}
													?>
													</td>
							</tr>
							<tr>
								<td style='width: 40%;'>&nbsp;&nbsp;&nbsp;Observaciones 99</td>
								<td class='text-right'><a
									href="<?php echo ENV_WEBROOT_FULL_URL;?>EvaluarInconsistencias/evaluar_inconsistencia/U,F/<?php echo date('Y-m-d');?>/<?php echo date('Y-m-d');?><?php echo $url_unidad_ciclo;?>/0/0/0/0/0/OBS99"
									target="_blank"><?php echo number_format($cantidad_obs_99_dia_actual);?></a> (<?php echo number_format(($cantidad_ordenes_dia_actual>0?(($cantidad_obs_99_dia_actual*100)/$cantidad_ordenes_dia_actual):0),2);?>%)</td>
								<td class='text-right'><a
									href="<?php echo ENV_WEBROOT_FULL_URL;?>EvaluarInconsistencias/evaluar_inconsistencia/U/<?php echo date('Y-m-d');?>/<?php echo date('Y-m-d');?><?php echo $url_unidad_ciclo;?>/0/0/0/0/0/OBS99"
									target="_blank"><?php echo number_format($cantidad_obs_99_periodo_actual);?></a> (<?php echo number_format(($cantidad_ordenes_periodo_actual>0?(($cantidad_obs_99_periodo_actual*100)/$cantidad_ordenes_periodo_actual):0),2);?>%)</td>
								<td class='text-right'><?php echo number_format($cantidad_obs_99_periodo_anterior);?> (<?php echo number_format(($cantidad_ordenes_periodo_anterior>0?(($cantidad_obs_99_periodo_anterior*100)/$cantidad_ordenes_periodo_anterior):0),2);?>%)</td>
								<td class='text-right'>
													<?php
													if ($cantidad_obs_99_periodo_actual < $cantidad_obs_99_periodo_anterior) {
														echo "<img src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAB8ElEQVR42mNgoDb4/OfD/9UPpv6vv5z4v+C83/+sM17/c0+F/595o/f/u+/v/uPVfOHrgf8114L+L33f+H/j997/s98V/O95Hvu/5UHI/5wzLv+dtuv+3/Vy23+cmuvvRvw/9H/h/6U/qv53fgj93/DM93/VA/f/Jbcd/xdctfmffcH8v+kaqf/bnqMZAnJ22RW///v+zP0/52f+/76vUXADah97gQ3IvWz1P/OcKRgrLZNG9c6K+1P+z3lZDrcZhFtfB4E1V9xz/V980wFsQNoZ4/9xJ/T/W62T+d9+oh1hQOm5iP8bPvX9b30ZCMfozgfZnAjUHHlM+7/HdpX/NkusEQYknHACuwCkAWQrzGYQBmkGYZjtQYc0/1tvlv0vN1kSYUDkYdv/Ex6lgG1DxjDNyLb77FcDB6T0BAmEATGHvP933ouCa4BhUKiDMLJmj70q/7Xmi/63mm2JMKD7Yis4nmGhjIzjoJoDD2qANYNsl53C+79lbwvCAFCUGG5QBfsTZBsMgzSC/AyzGeR3ldlC/yXbRDFT5bon68DxC9OErNFpjyLYZpBmgWbO/+uurcOeGtc8WPNfbr44OJ6dtyuCMUij1kJRsLPFW0Vwa4aBF19f/K/eW/3faLbBf9FuATA2mqz/v2ZrzX+QHNVzLwAjy7LsIPIjLQAAAABJRU5ErkJggg=='>";
													} elseif ($cantidad_obs_99_periodo_actual > $cantidad_obs_99_periodo_anterior) {
														echo "<img src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAABrElEQVR42q2TyytEURzH/QFW1JRHIxsTUiNWHgtCFOWRPEpZyAJZkKSZhccgsVBIFqRkZdKUZCFFNLEQJUnJQk0eGcQCk+bj/O6dyZ3rsXLr272dcz7f3/d3zrlRUf/98PIIK1Mw0gzOKnCU6+/FUYJPD/wNH23DcB14F+DcA7sz4B5ScK8+XmuDvXV+h6da4GkfzpZhywWePljqhLlWmGiEwWoCFdbvJlpsNcm1MjmZVwnGfzaQdpT8RdbIdrSe16fhcFEHRWsOtRfdsNCuJ5MWxKCrgMeadPzTowYDp5o8dutQWAKb4tNTqhm8NmdxWZVnMFCDbM3ogFECTzZFVKctl7eGDE6zEwwGalDbbQHCkthSOQyHqstaaeHYHmcw6CjSj0oAoyS2CaY1i6vCJI7Kcr4MgrMD+mK5MEYJaIKlusS/GHMZDNSRvFWmfFUzgyE4UJ/GZW48ezYLAf+96S5srvJckhgJhUCRFBDYmxyNf83z820Mbrjx5cVpMeWoZLfl26cujsTeSbH8Doefj7sbfCMOToozOUyN5cAWw0G+nbN+J+93t/z73/sJB/zttLkU5gUAAAAASUVORK5CYII='>";
													} else {
														echo "<img src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAABpUlEQVR42q2TyytEURzH/QFWRCFTVhJF2WFla0XJwkKxlJ2VRxI1lA2RlRRWnqVGiRqPmJJXYqKGPEOGYdIwg/Nxzpm55ozByq1v3c6538fve85NSPjvhzcfnPTBYQ3slyP2ymC3Ajx21N7fZN8S4qgKAkO8M4N4HQBvB9w26nXhzIabOX4lc1qHn3WCjMFHJ7w0S4EGvc5JtRYJztviRXRsGTeAU5KH+KALQh0xAsJTiXCXadyN22S6h6iImlk89krX4bCz5f7YKN3qowK6j1IeHLncu+yGgCopNBEmWVBkI75KqMgKgeVCPCMlpkBppLCGWFzXaugC3WF3tovxL+aw05NmCMhF3XaE8AVVXGR2y53NQj1CrIDakEelo5pQsY3o2kgKnI3a2OovigqIg/boxyYsokH2ObJw96fgcXQaAvJI1FxWy3HECPllKY/j4XRW2lIJPXu/3YWLaZ5mM+IcLShnRV5rTcS7O/3zbRSnk5yPpOk0odV87ajeL8czdWxnU+rvZOt5f77haqGZvcECNuzJuNqTcHUX4J5qIei/5d//3k/ScyAkq+CgnAAAAABJRU5ErkJggg=='>";
													}
													?>
													</td>
							</tr>
							<tr>
								<td style='width: 40%;'>Tiempo promedio de ejecuci&oacute;n</td>
								<td class='text-right'><?php echo substr($cantidad_tiempoejecucion_dia_actual,0,8);?></td>
								<td class='text-right'><?php echo substr($cantidad_tiempoejecucion_periodo_actual,0,8);?></td>
								<td class='text-right'><?php echo substr($cantidad_tiempoejecucion_periodo_anterior,0,8);?></td>
								<td class='text-right'>
													<?php
													if ($cantidad_tiempoejecucion_periodo_actual < $cantidad_tiempoejecucion_periodo_anterior) {
														echo "<img src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAB8ElEQVR42mNgoDb4/OfD/9UPpv6vv5z4v+C83/+sM17/c0+F/595o/f/u+/v/uPVfOHrgf8114L+L33f+H/j997/s98V/O95Hvu/5UHI/5wzLv+dtuv+3/Vy23+cmuvvRvw/9H/h/6U/qv53fgj93/DM93/VA/f/Jbcd/xdctfmffcH8v+kaqf/bnqMZAnJ22RW///v+zP0/52f+/76vUXADah97gQ3IvWz1P/OcKRgrLZNG9c6K+1P+z3lZDrcZhFtfB4E1V9xz/V980wFsQNoZ4/9xJ/T/W62T+d9+oh1hQOm5iP8bPvX9b30ZCMfozgfZnAjUHHlM+7/HdpX/NkusEQYknHACuwCkAWQrzGYQBmkGYZjtQYc0/1tvlv0vN1kSYUDkYdv/Ex6lgG1DxjDNyLb77FcDB6T0BAmEATGHvP933ouCa4BhUKiDMLJmj70q/7Xmi/63mm2JMKD7Yis4nmGhjIzjoJoDD2qANYNsl53C+79lbwvCAFCUGG5QBfsTZBsMgzSC/AyzGeR3ldlC/yXbRDFT5bon68DxC9OErNFpjyLYZpBmgWbO/+uurcOeGtc8WPNfbr44OJ6dtyuCMUij1kJRsLPFW0Vwa4aBF19f/K/eW/3faLbBf9FuATA2mqz/v2ZrzX+QHNVzLwAjy7LsIPIjLQAAAABJRU5ErkJggg=='>";
													} elseif ($cantidad_tiempoejecucion_periodo_actual > $cantidad_tiempoejecucion_periodo_anterior) {
														echo "<img src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAABrElEQVR42q2TyytEURzH/QFW1JRHIxsTUiNWHgtCFOWRPEpZyAJZkKSZhccgsVBIFqRkZdKUZCFFNLEQJUnJQk0eGcQCk+bj/O6dyZ3rsXLr272dcz7f3/d3zrlRUf/98PIIK1Mw0gzOKnCU6+/FUYJPD/wNH23DcB14F+DcA7sz4B5ScK8+XmuDvXV+h6da4GkfzpZhywWePljqhLlWmGiEwWoCFdbvJlpsNcm1MjmZVwnGfzaQdpT8RdbIdrSe16fhcFEHRWsOtRfdsNCuJ5MWxKCrgMeadPzTowYDp5o8dutQWAKb4tNTqhm8NmdxWZVnMFCDbM3ogFECTzZFVKctl7eGDE6zEwwGalDbbQHCkthSOQyHqstaaeHYHmcw6CjSj0oAoyS2CaY1i6vCJI7Kcr4MgrMD+mK5MEYJaIKlusS/GHMZDNSRvFWmfFUzgyE4UJ/GZW48ezYLAf+96S5srvJckhgJhUCRFBDYmxyNf83z820Mbrjx5cVpMeWoZLfl26cujsTeSbH8Doefj7sbfCMOToozOUyN5cAWw0G+nbN+J+93t/z73/sJB/zttLkU5gUAAAAASUVORK5CYII='>";
													} else {
														echo "<img src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAABpUlEQVR42q2TyytEURzH/QFWRCFTVhJF2WFla0XJwkKxlJ2VRxI1lA2RlRRWnqVGiRqPmJJXYqKGPEOGYdIwg/Nxzpm55ozByq1v3c6538fve85NSPjvhzcfnPTBYQ3slyP2ymC3Ajx21N7fZN8S4qgKAkO8M4N4HQBvB9w26nXhzIabOX4lc1qHn3WCjMFHJ7w0S4EGvc5JtRYJztviRXRsGTeAU5KH+KALQh0xAsJTiXCXadyN22S6h6iImlk89krX4bCz5f7YKN3qowK6j1IeHLncu+yGgCopNBEmWVBkI75KqMgKgeVCPCMlpkBppLCGWFzXaugC3WF3tovxL+aw05NmCMhF3XaE8AVVXGR2y53NQj1CrIDakEelo5pQsY3o2kgKnI3a2OovigqIg/boxyYsokH2ObJw96fgcXQaAvJI1FxWy3HECPllKY/j4XRW2lIJPXu/3YWLaZ5mM+IcLShnRV5rTcS7O/3zbRSnk5yPpOk0odV87ajeL8czdWxnU+rvZOt5f77haqGZvcECNuzJuNqTcHUX4J5qIei/5d//3k/ScyAkq+CgnAAAAABJRU5ErkJggg=='>";
													}
													?>
													</td>
							</tr>
							<tr>
								<td style='width: 40%;'>Consumo cero</td>
								<td class='text-right'><a
									href="<?php echo ENV_WEBROOT_FULL_URL;?>EvaluarInconsistencias/evaluar_inconsistencia/U,F/<?php echo date('Y-m-d');?>/<?php echo date('Y-m-d');?><?php echo $url_unidad_ciclo;?>/0/0/0/0/0/CC"
									target="_blank"><?php echo number_format($cantidad_consumo_cero_dia_actual);?></a> (<?php echo number_format(($cantidad_ordenes_dia_actual>0?(($cantidad_consumo_cero_dia_actual*100)/$cantidad_ordenes_dia_actual):0),2);?>%)</td>
								<td class='text-right'><a
									href="<?php echo ENV_WEBROOT_FULL_URL;?>EvaluarInconsistencias/evaluar_inconsistencia/U/<?php echo date('Y-m-d');?>/<?php echo date('Y-m-d');?><?php echo $url_unidad_ciclo;?>/0/0/0/0/0/CC"
									target="_blank"><?php echo number_format($cantidad_consumo_cero_periodo_actual);?></a> (<?php echo number_format(($cantidad_ordenes_periodo_actual>0?(($cantidad_consumo_cero_periodo_actual*100)/$cantidad_ordenes_periodo_actual):0),2);?>%)</td>
								<td class='text-right'><?php echo number_format($cantidad_consumo_cero_periodo_anterior);?> (<?php echo number_format(($cantidad_ordenes_periodo_anterior>0?(($cantidad_consumo_cero_periodo_anterior*100)/$cantidad_ordenes_periodo_anterior):0),2);?>%)</td>
								<td class='text-right'>
													<?php
													if ($cantidad_consumo_cero_periodo_actual < $cantidad_consumo_cero_periodo_anterior) {
														echo "<img src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAB8ElEQVR42mNgoDb4/OfD/9UPpv6vv5z4v+C83/+sM17/c0+F/595o/f/u+/v/uPVfOHrgf8114L+L33f+H/j997/s98V/O95Hvu/5UHI/5wzLv+dtuv+3/Vy23+cmuvvRvw/9H/h/6U/qv53fgj93/DM93/VA/f/Jbcd/xdctfmffcH8v+kaqf/bnqMZAnJ22RW///v+zP0/52f+/76vUXADah97gQ3IvWz1P/OcKRgrLZNG9c6K+1P+z3lZDrcZhFtfB4E1V9xz/V980wFsQNoZ4/9xJ/T/W62T+d9+oh1hQOm5iP8bPvX9b30ZCMfozgfZnAjUHHlM+7/HdpX/NkusEQYknHACuwCkAWQrzGYQBmkGYZjtQYc0/1tvlv0vN1kSYUDkYdv/Ex6lgG1DxjDNyLb77FcDB6T0BAmEATGHvP933ouCa4BhUKiDMLJmj70q/7Xmi/63mm2JMKD7Yis4nmGhjIzjoJoDD2qANYNsl53C+79lbwvCAFCUGG5QBfsTZBsMgzSC/AyzGeR3ldlC/yXbRDFT5bon68DxC9OErNFpjyLYZpBmgWbO/+uurcOeGtc8WPNfbr44OJ6dtyuCMUij1kJRsLPFW0Vwa4aBF19f/K/eW/3faLbBf9FuATA2mqz/v2ZrzX+QHNVzLwAjy7LsIPIjLQAAAABJRU5ErkJggg=='>";
													} elseif ($cantidad_consumo_cero_periodo_actual > $cantidad_consumo_cero_periodo_anterior) {
														echo "<img src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAABrElEQVR42q2TyytEURzH/QFW1JRHIxsTUiNWHgtCFOWRPEpZyAJZkKSZhccgsVBIFqRkZdKUZCFFNLEQJUnJQk0eGcQCk+bj/O6dyZ3rsXLr272dcz7f3/d3zrlRUf/98PIIK1Mw0gzOKnCU6+/FUYJPD/wNH23DcB14F+DcA7sz4B5ScK8+XmuDvXV+h6da4GkfzpZhywWePljqhLlWmGiEwWoCFdbvJlpsNcm1MjmZVwnGfzaQdpT8RdbIdrSe16fhcFEHRWsOtRfdsNCuJ5MWxKCrgMeadPzTowYDp5o8dutQWAKb4tNTqhm8NmdxWZVnMFCDbM3ogFECTzZFVKctl7eGDE6zEwwGalDbbQHCkthSOQyHqstaaeHYHmcw6CjSj0oAoyS2CaY1i6vCJI7Kcr4MgrMD+mK5MEYJaIKlusS/GHMZDNSRvFWmfFUzgyE4UJ/GZW48ezYLAf+96S5srvJckhgJhUCRFBDYmxyNf83z820Mbrjx5cVpMeWoZLfl26cujsTeSbH8Doefj7sbfCMOToozOUyN5cAWw0G+nbN+J+93t/z73/sJB/zttLkU5gUAAAAASUVORK5CYII='>";
													} else {
														echo "<img src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAABpUlEQVR42q2TyytEURzH/QFWRCFTVhJF2WFla0XJwkKxlJ2VRxI1lA2RlRRWnqVGiRqPmJJXYqKGPEOGYdIwg/Nxzpm55ozByq1v3c6538fve85NSPjvhzcfnPTBYQ3slyP2ymC3Ajx21N7fZN8S4qgKAkO8M4N4HQBvB9w26nXhzIabOX4lc1qHn3WCjMFHJ7w0S4EGvc5JtRYJztviRXRsGTeAU5KH+KALQh0xAsJTiXCXadyN22S6h6iImlk89krX4bCz5f7YKN3qowK6j1IeHLncu+yGgCopNBEmWVBkI75KqMgKgeVCPCMlpkBppLCGWFzXaugC3WF3tovxL+aw05NmCMhF3XaE8AVVXGR2y53NQj1CrIDakEelo5pQsY3o2kgKnI3a2OovigqIg/boxyYsokH2ObJw96fgcXQaAvJI1FxWy3HECPllKY/j4XRW2lIJPXu/3YWLaZ5mM+IcLShnRV5rTcS7O/3zbRSnk5yPpOk0odV87ajeL8czdWxnU+rvZOt5f77haqGZvcECNuzJuNqTcHUX4J5qIei/5d//3k/ScyAkq+CgnAAAAABJRU5ErkJggg=='>";
													}
													?>
													</td>
							</tr>
							<tr>
								<td style='width: 40%;'>Consumo menor que 35</td>
								<td class='text-right'><a
									href="<?php echo ENV_WEBROOT_FULL_URL;?>EvaluarInconsistencias/evaluar_inconsistencia/U/<?php echo date('Y-m-d');?>/<?php echo date('Y-m-d');?><?php echo $url_unidad_ciclo;?>/0/0/0/0/34/"
									target="_blank"><?php echo number_format($cantidad_consumo_menor35_dia_actual);?></a> (<?php echo number_format(($cantidad_ordenes_dia_actual>0?(($cantidad_consumo_menor35_dia_actual*100)/$cantidad_ordenes_dia_actual):0),2);?>%)</td>
								<td class='text-right'><a
									href="<?php echo ENV_WEBROOT_FULL_URL;?>EvaluarInconsistencias/evaluar_inconsistencia/U/<?php echo date('Y-m-d');?>/<?php echo date('Y-m-d');?><?php echo $url_unidad_ciclo;?>/0/0/0/0/34/"
									target="_blank"><?php echo number_format($cantidad_consumo_menor35_periodo_actual);?></a> (<?php echo number_format(($cantidad_ordenes_periodo_actual>0?(($cantidad_consumo_menor35_periodo_actual*100)/$cantidad_ordenes_periodo_actual):0),2);?>%)</td>
								<td class='text-right'><?php echo number_format($cantidad_consumo_menor35_periodo_anterior);?> (<?php echo number_format(($cantidad_ordenes_periodo_anterior>0?(($cantidad_consumo_menor35_periodo_anterior*100)/$cantidad_ordenes_periodo_anterior):0),2);?>%)</td>
								<td class='text-right'>
													<?php
													if ($cantidad_consumo_menor35_periodo_actual < $cantidad_consumo_menor35_periodo_anterior) {
														echo "<img src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAB8ElEQVR42mNgoDb4/OfD/9UPpv6vv5z4v+C83/+sM17/c0+F/595o/f/u+/v/uPVfOHrgf8114L+L33f+H/j997/s98V/O95Hvu/5UHI/5wzLv+dtuv+3/Vy23+cmuvvRvw/9H/h/6U/qv53fgj93/DM93/VA/f/Jbcd/xdctfmffcH8v+kaqf/bnqMZAnJ22RW///v+zP0/52f+/76vUXADah97gQ3IvWz1P/OcKRgrLZNG9c6K+1P+z3lZDrcZhFtfB4E1V9xz/V980wFsQNoZ4/9xJ/T/W62T+d9+oh1hQOm5iP8bPvX9b30ZCMfozgfZnAjUHHlM+7/HdpX/NkusEQYknHACuwCkAWQrzGYQBmkGYZjtQYc0/1tvlv0vN1kSYUDkYdv/Ex6lgG1DxjDNyLb77FcDB6T0BAmEATGHvP933ouCa4BhUKiDMLJmj70q/7Xmi/63mm2JMKD7Yis4nmGhjIzjoJoDD2qANYNsl53C+79lbwvCAFCUGG5QBfsTZBsMgzSC/AyzGeR3ldlC/yXbRDFT5bon68DxC9OErNFpjyLYZpBmgWbO/+uurcOeGtc8WPNfbr44OJ6dtyuCMUij1kJRsLPFW0Vwa4aBF19f/K/eW/3faLbBf9FuATA2mqz/v2ZrzX+QHNVzLwAjy7LsIPIjLQAAAABJRU5ErkJggg=='>";
													} elseif ($cantidad_consumo_menor35_periodo_actual > $cantidad_consumo_menor35_periodo_anterior) {
														echo "<img src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAABrElEQVR42q2TyytEURzH/QFW1JRHIxsTUiNWHgtCFOWRPEpZyAJZkKSZhccgsVBIFqRkZdKUZCFFNLEQJUnJQk0eGcQCk+bj/O6dyZ3rsXLr272dcz7f3/d3zrlRUf/98PIIK1Mw0gzOKnCU6+/FUYJPD/wNH23DcB14F+DcA7sz4B5ScK8+XmuDvXV+h6da4GkfzpZhywWePljqhLlWmGiEwWoCFdbvJlpsNcm1MjmZVwnGfzaQdpT8RdbIdrSe16fhcFEHRWsOtRfdsNCuJ5MWxKCrgMeadPzTowYDp5o8dutQWAKb4tNTqhm8NmdxWZVnMFCDbM3ogFECTzZFVKctl7eGDE6zEwwGalDbbQHCkthSOQyHqstaaeHYHmcw6CjSj0oAoyS2CaY1i6vCJI7Kcr4MgrMD+mK5MEYJaIKlusS/GHMZDNSRvFWmfFUzgyE4UJ/GZW48ezYLAf+96S5srvJckhgJhUCRFBDYmxyNf83z820Mbrjx5cVpMeWoZLfl26cujsTeSbH8Doefj7sbfCMOToozOUyN5cAWw0G+nbN+J+93t/z73/sJB/zttLkU5gUAAAAASUVORK5CYII='>";
													} else {
														echo "<img src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAABpUlEQVR42q2TyytEURzH/QFWRCFTVhJF2WFla0XJwkKxlJ2VRxI1lA2RlRRWnqVGiRqPmJJXYqKGPEOGYdIwg/Nxzpm55ozByq1v3c6538fve85NSPjvhzcfnPTBYQ3slyP2ymC3Ajx21N7fZN8S4qgKAkO8M4N4HQBvB9w26nXhzIabOX4lc1qHn3WCjMFHJ7w0S4EGvc5JtRYJztviRXRsGTeAU5KH+KALQh0xAsJTiXCXadyN22S6h6iImlk89krX4bCz5f7YKN3qowK6j1IeHLncu+yGgCopNBEmWVBkI75KqMgKgeVCPCMlpkBppLCGWFzXaugC3WF3tovxL+aw05NmCMhF3XaE8AVVXGR2y53NQj1CrIDakEelo5pQsY3o2kgKnI3a2OovigqIg/boxyYsokH2ObJw96fgcXQaAvJI1FxWy3HECPllKY/j4XRW2lIJPXu/3YWLaZ5mM+IcLShnRV5rTcS7O/3zbRSnk5yPpOk0odV87ajeL8czdWxnU+rvZOt5f77haqGZvcECNuzJuNqTcHUX4J5qIei/5d//3k/ScyAkq+CgnAAAAABJRU5ErkJggg=='>";
													}
													?>
													</td>
							</tr>
							<tr>
								<td style='width: 40%;'>Consumo mayor que 1000</td>
								<td class='text-right'><a
									href="<?php echo ENV_WEBROOT_FULL_URL;?>EvaluarInconsistencias/evaluar_inconsistencia/U,F/<?php echo date('Y-m-d');?>/<?php echo date('Y-m-d');?><?php echo $url_unidad_ciclo;?>/0/0/0/1001/0/"
									target="_blank"><?php echo number_format($cantidad_consumo_mayor1000_dia_actual);?></a> (<?php echo number_format(($cantidad_ordenes_dia_actual>0?(($cantidad_consumo_mayor1000_dia_actual*100)/$cantidad_ordenes_dia_actual):0),2);?>%)</td>
								<td class='text-right'><a
									href="<?php echo ENV_WEBROOT_FULL_URL;?>EvaluarInconsistencias/evaluar_inconsistencia/U/<?php echo date('Y-m-d');?>/<?php echo date('Y-m-d');?><?php echo $url_unidad_ciclo;?>/0/0/0/1001/0/"
									target="_blank"><?php echo number_format($cantidad_consumo_mayor1000_periodo_actual);?></a> (<?php echo number_format(($cantidad_ordenes_periodo_actual>0?(($cantidad_consumo_mayor1000_periodo_actual*100)/$cantidad_ordenes_periodo_actual):0),2);?>%)</td>
								<td class='text-right'><?php echo number_format($cantidad_consumo_mayor1000_periodo_anterior);?> (<?php echo number_format(($cantidad_ordenes_periodo_anterior>0?(($cantidad_consumo_mayor1000_periodo_anterior*100)/$cantidad_ordenes_periodo_anterior):0),2);?>%)</td>
								<td class='text-right'>
													<?php
													if ($cantidad_consumo_mayor1000_periodo_actual < $cantidad_consumo_mayor1000_periodo_anterior) {
														echo "<img src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAB8ElEQVR42mNgoDb4/OfD/9UPpv6vv5z4v+C83/+sM17/c0+F/595o/f/u+/v/uPVfOHrgf8114L+L33f+H/j997/s98V/O95Hvu/5UHI/5wzLv+dtuv+3/Vy23+cmuvvRvw/9H/h/6U/qv53fgj93/DM93/VA/f/Jbcd/xdctfmffcH8v+kaqf/bnqMZAnJ22RW///v+zP0/52f+/76vUXADah97gQ3IvWz1P/OcKRgrLZNG9c6K+1P+z3lZDrcZhFtfB4E1V9xz/V980wFsQNoZ4/9xJ/T/W62T+d9+oh1hQOm5iP8bPvX9b30ZCMfozgfZnAjUHHlM+7/HdpX/NkusEQYknHACuwCkAWQrzGYQBmkGYZjtQYc0/1tvlv0vN1kSYUDkYdv/Ex6lgG1DxjDNyLb77FcDB6T0BAmEATGHvP933ouCa4BhUKiDMLJmj70q/7Xmi/63mm2JMKD7Yis4nmGhjIzjoJoDD2qANYNsl53C+79lbwvCAFCUGG5QBfsTZBsMgzSC/AyzGeR3ldlC/yXbRDFT5bon68DxC9OErNFpjyLYZpBmgWbO/+uurcOeGtc8WPNfbr44OJ6dtyuCMUij1kJRsLPFW0Vwa4aBF19f/K/eW/3faLbBf9FuATA2mqz/v2ZrzX+QHNVzLwAjy7LsIPIjLQAAAABJRU5ErkJggg=='>";
													} elseif ($cantidad_consumo_mayor1000_periodo_actual > $cantidad_consumo_mayor1000_periodo_anterior) {
														echo "<img src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAABrElEQVR42q2TyytEURzH/QFW1JRHIxsTUiNWHgtCFOWRPEpZyAJZkKSZhccgsVBIFqRkZdKUZCFFNLEQJUnJQk0eGcQCk+bj/O6dyZ3rsXLr272dcz7f3/d3zrlRUf/98PIIK1Mw0gzOKnCU6+/FUYJPD/wNH23DcB14F+DcA7sz4B5ScK8+XmuDvXV+h6da4GkfzpZhywWePljqhLlWmGiEwWoCFdbvJlpsNcm1MjmZVwnGfzaQdpT8RdbIdrSe16fhcFEHRWsOtRfdsNCuJ5MWxKCrgMeadPzTowYDp5o8dutQWAKb4tNTqhm8NmdxWZVnMFCDbM3ogFECTzZFVKctl7eGDE6zEwwGalDbbQHCkthSOQyHqstaaeHYHmcw6CjSj0oAoyS2CaY1i6vCJI7Kcr4MgrMD+mK5MEYJaIKlusS/GHMZDNSRvFWmfFUzgyE4UJ/GZW48ezYLAf+96S5srvJckhgJhUCRFBDYmxyNf83z820Mbrjx5cVpMeWoZLfl26cujsTeSbH8Doefj7sbfCMOToozOUyN5cAWw0G+nbN+J+93t/z73/sJB/zttLkU5gUAAAAASUVORK5CYII='>";
													} else {
														echo "<img src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAABpUlEQVR42q2TyytEURzH/QFWRCFTVhJF2WFla0XJwkKxlJ2VRxI1lA2RlRRWnqVGiRqPmJJXYqKGPEOGYdIwg/Nxzpm55ozByq1v3c6538fve85NSPjvhzcfnPTBYQ3slyP2ymC3Ajx21N7fZN8S4qgKAkO8M4N4HQBvB9w26nXhzIabOX4lc1qHn3WCjMFHJ7w0S4EGvc5JtRYJztviRXRsGTeAU5KH+KALQh0xAsJTiXCXadyN22S6h6iImlk89krX4bCz5f7YKN3qowK6j1IeHLncu+yGgCopNBEmWVBkI75KqMgKgeVCPCMlpkBppLCGWFzXaugC3WF3tovxL+aw05NmCMhF3XaE8AVVXGR2y53NQj1CrIDakEelo5pQsY3o2kgKnI3a2OovigqIg/boxyYsokH2ObJw96fgcXQaAvJI1FxWy3HECPllKY/j4XRW2lIJPXu/3YWLaZ5mM+IcLShnRV5rTcS7O/3zbRSnk5yPpOk0odV87ajeL8czdWxnU+rvZOt5f77haqGZvcECNuzJuNqTcHUX4J5qIei/5d//3k/ScyAkq+CgnAAAAABJRU5ErkJggg=='>";
													}
													?>
													</td>
							</tr>
							<tr>
								<td style='width: 40%;'>Consumo mayor 100% que el anterior</td>
								<td class='text-right'><a
									href="<?php echo ENV_WEBROOT_FULL_URL;?>EvaluarInconsistencias/evaluar_inconsistencia/U,F/<?php echo date('Y-m-d');?>/<?php echo date('Y-m-d');?><?php echo $url_unidad_ciclo;?>/0/0/0/0/0/C100MA"
									target="_blank"><?php echo number_format($cantidad_consumo_mayor100porciento_dia_actual);?></a> (<?php echo number_format(($cantidad_ordenes_dia_actual>0?(($cantidad_consumo_mayor100porciento_dia_actual*100)/$cantidad_ordenes_dia_actual):0),2);?>%)</td>
								<td class='text-right'><a
									href="<?php echo ENV_WEBROOT_FULL_URL;?>EvaluarInconsistencias/evaluar_inconsistencia/U/<?php echo date('Y-m-d');?>/<?php echo date('Y-m-d');?><?php echo $url_unidad_ciclo;?>/0/0/0/0/0/C100MA"
									target="_blank"><?php echo number_format($cantidad_consumo_mayor100porciento_periodo_actual);?></a> (<?php echo number_format(($cantidad_ordenes_periodo_actual>0?(($cantidad_consumo_mayor100porciento_periodo_actual*100)/$cantidad_ordenes_periodo_actual):0),2);?>%)</td>
								<td class='text-right'><?php echo number_format($cantidad_consumo_mayor100porciento_periodo_anterior);?> (<?php echo number_format(($cantidad_ordenes_periodo_anterior>0?(($cantidad_consumo_mayor100porciento_periodo_anterior*100)/$cantidad_ordenes_periodo_anterior):0),2);?>%)</td>
								<td class='text-right'>
													<?php
													if ($cantidad_consumo_mayor100porciento_periodo_actual < $cantidad_consumo_mayor100porciento_periodo_anterior) {
														echo "<img src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAB8ElEQVR42mNgoDb4/OfD/9UPpv6vv5z4v+C83/+sM17/c0+F/595o/f/u+/v/uPVfOHrgf8114L+L33f+H/j997/s98V/O95Hvu/5UHI/5wzLv+dtuv+3/Vy23+cmuvvRvw/9H/h/6U/qv53fgj93/DM93/VA/f/Jbcd/xdctfmffcH8v+kaqf/bnqMZAnJ22RW///v+zP0/52f+/76vUXADah97gQ3IvWz1P/OcKRgrLZNG9c6K+1P+z3lZDrcZhFtfB4E1V9xz/V980wFsQNoZ4/9xJ/T/W62T+d9+oh1hQOm5iP8bPvX9b30ZCMfozgfZnAjUHHlM+7/HdpX/NkusEQYknHACuwCkAWQrzGYQBmkGYZjtQYc0/1tvlv0vN1kSYUDkYdv/Ex6lgG1DxjDNyLb77FcDB6T0BAmEATGHvP933ouCa4BhUKiDMLJmj70q/7Xmi/63mm2JMKD7Yis4nmGhjIzjoJoDD2qANYNsl53C+79lbwvCAFCUGG5QBfsTZBsMgzSC/AyzGeR3ldlC/yXbRDFT5bon68DxC9OErNFpjyLYZpBmgWbO/+uurcOeGtc8WPNfbr44OJ6dtyuCMUij1kJRsLPFW0Vwa4aBF19f/K/eW/3faLbBf9FuATA2mqz/v2ZrzX+QHNVzLwAjy7LsIPIjLQAAAABJRU5ErkJggg=='>";
													} elseif ($cantidad_consumo_mayor100porciento_periodo_actual > $cantidad_consumo_mayor100porciento_periodo_anterior) {
														echo "<img src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAABrElEQVR42q2TyytEURzH/QFW1JRHIxsTUiNWHgtCFOWRPEpZyAJZkKSZhccgsVBIFqRkZdKUZCFFNLEQJUnJQk0eGcQCk+bj/O6dyZ3rsXLr272dcz7f3/d3zrlRUf/98PIIK1Mw0gzOKnCU6+/FUYJPD/wNH23DcB14F+DcA7sz4B5ScK8+XmuDvXV+h6da4GkfzpZhywWePljqhLlWmGiEwWoCFdbvJlpsNcm1MjmZVwnGfzaQdpT8RdbIdrSe16fhcFEHRWsOtRfdsNCuJ5MWxKCrgMeadPzTowYDp5o8dutQWAKb4tNTqhm8NmdxWZVnMFCDbM3ogFECTzZFVKctl7eGDE6zEwwGalDbbQHCkthSOQyHqstaaeHYHmcw6CjSj0oAoyS2CaY1i6vCJI7Kcr4MgrMD+mK5MEYJaIKlusS/GHMZDNSRvFWmfFUzgyE4UJ/GZW48ezYLAf+96S5srvJckhgJhUCRFBDYmxyNf83z820Mbrjx5cVpMeWoZLfl26cujsTeSbH8Doefj7sbfCMOToozOUyN5cAWw0G+nbN+J+93t/z73/sJB/zttLkU5gUAAAAASUVORK5CYII='>";
													} else {
														echo "<img src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAABpUlEQVR42q2TyytEURzH/QFWRCFTVhJF2WFla0XJwkKxlJ2VRxI1lA2RlRRWnqVGiRqPmJJXYqKGPEOGYdIwg/Nxzpm55ozByq1v3c6538fve85NSPjvhzcfnPTBYQ3slyP2ymC3Ajx21N7fZN8S4qgKAkO8M4N4HQBvB9w26nXhzIabOX4lc1qHn3WCjMFHJ7w0S4EGvc5JtRYJztviRXRsGTeAU5KH+KALQh0xAsJTiXCXadyN22S6h6iImlk89krX4bCz5f7YKN3qowK6j1IeHLncu+yGgCopNBEmWVBkI75KqMgKgeVCPCMlpkBppLCGWFzXaugC3WF3tovxL+aw05NmCMhF3XaE8AVVXGR2y53NQj1CrIDakEelo5pQsY3o2kgKnI3a2OovigqIg/boxyYsokH2ObJw96fgcXQaAvJI1FxWy3HECPllKY/j4XRW2lIJPXu/3YWLaZ5mM+IcLShnRV5rTcS7O/3zbRSnk5yPpOk0odV87ajeL8czdWxnU+rvZOt5f77haqGZvcECNuzJuNqTcHUX4J5qIei/5d//3k/ScyAkq+CgnAAAAABJRU5ErkJggg=='>";
													}
													?>
													</td>
							</tr>
						</tbody>
					</table>
					<div style="text-align: right;">
					<br>
					<span class="lbl_refresh_kpi_dashboard">
					&Uacute;ltima Actualizaci&oacute;n: hace <?php echo $this->Tiempo->getTiempoString($fecha_last_update, date("Y-m-d H:i:s"), "x");?>
					<a href="javascript: void(0);" class="btn_refresh_kpi_dashboard" title="Actualizar datos de KPI"><i class="fa fa-refresh"></i></a>
					</span>
					</div>
				</div>
			</div>
		</div>

		<div class='col-sm-12 col-md-12'>
			<div class="tab-container">
				<ul class="nav nav-tabs tabs_dashboard">
					<li class="active"><a href="#resumen_detallado_por_ciclo" data-toggle="tab"><img
							src="<?php echo ENV_WEBROOT_FULL_URL;?>images/dashboard/detallado_ciclo.png"
							width="90" height="90"><br>Detallado / Ciclo</a></li>
					<li class=""><a href="#productividad_lecturistas_semanal" data-toggle="tab"><img
							src="<?php echo ENV_WEBROOT_FULL_URL;?>images/dashboard/lecturistas_semana.png"
							width="90" height="90"><br>Lecturistas / Semanal</a></li>
					<li class=""><a href="#productividad_lecturistas_periodo" data-toggle="tab"><img
							src="<?php echo ENV_WEBROOT_FULL_URL;?>images/dashboard/lecturistas_periodo.png"
							width="90" height="90"><br>Lecturistas / Periodo</a></li>
					<li class=""><a href="#productividad_digitadores_periodo" data-toggle="tab"><img
							src="<?php echo ENV_WEBROOT_FULL_URL;?>images/dashboard/digitadores_periodo.png"
							width="90" height="90"><br>Digitadores / Periodo</a></li>
					<li class=""><a href="#distribucion_consumos" data-toggle="tab"><img
							src="<?php echo ENV_WEBROOT_FULL_URL;?>images/dashboard/distribucion_consumo.png"
							width="90" height="90"><br>Distribuci&oacute;n Consumos</a></li>
					<li class=""><a href="#observaciones_ciclo" data-toggle="tab"><img
							src="<?php echo ENV_WEBROOT_FULL_URL;?>images/dashboard/observaciones_ciclo.png"
							width="90" height="90"><br>Observaciones / Ciclo</a></li>
					<li class=""><a href="#acumulacion_consumos" data-toggle="tab"><img
							src="<?php echo ENV_WEBROOT_FULL_URL;?>images/dashboard/acumulacion_consumos.png"
							width="90" height="90"><br>Acumulaci&oacute;n Consumos</a></li>
					<li class=""><a href="#metodos_evaluacion" data-toggle="tab"><img
							src="<?php echo ENV_WEBROOT_FULL_URL;?>images/dashboard/metodos.png"
							width="90" height="90"><br>M&eacute;todos</a></li>
					<li class=""><a href="#monitor_gps" data-toggle="tab"><img
							src="<?php echo ENV_WEBROOT_FULL_URL;?>images/dashboard/monitor_gps.png"
							width="90" height="90"><br>Monitor GPS</a></li>
				</ul>
				<div class="tab-content">
					<div class="tab-pane cont active" id="resumen_detallado_por_ciclo">
						<h2>Resumen Detallado por Ciclo</h2>
						<iframe id="frame_resumen_detallado_por_ciclo" class='preview-pane'
						src='<?php echo ENV_WEBROOT_FULL_URL;?>dashboard/resumen_por_ciclos_v2<?php echo $url_completa;?>'
						width='100%' height='415' frameborder='0' scrolling='yes'></iframe>
						<a
						href="<?php echo ENV_WEBROOT_FULL_URL;?>reportes/resumen_suministros_cronograma"
						target="_blank">ver resumen detallado cronograma</a>
					</div>
					<div class="tab-pane cont" id="productividad_lecturistas_semanal">
						<h2>Control de Productividad Semanal de Lecturistas</h2>
						<iframe id="frame_productividad_lecturistas_semanal" class='preview-pane'
						data-src='<?php echo ENV_WEBROOT_FULL_URL;?>dashboard/grafico_lecturistas_por_semana<?php echo $url_completa;?>'
						width='100%' height='415' frameborder='0' scrolling='no'></iframe>
					</div>
					<div class="tab-pane cont" id="productividad_lecturistas_periodo">
						<h2>Control de Productividad Semanal de Lecturistas</h2>
						<iframe id="frame_productividad_lecturistas_periodo" class='preview-pane'
							data-src='<?php echo ENV_WEBROOT_FULL_URL;?>dashboard/grafico_lecturistas_por_periodo<?php echo $url_completa;?>'
							width='100%' height='415' frameborder='0' scrolling='yes'></iframe>
						<a
							href="<?php echo ENV_WEBROOT_FULL_URL;?>dashboard/grafico_lecturistas_por_periodo<?php echo $url_completa;?>"
							target="_blank">ver gr&aacute;fico completo</a>
					</div>
					<div class="tab-pane cont" id="productividad_digitadores_periodo">
						<h2>Control de Productividad de Digitadores por Periodo Actual</h2>
						<iframe id="frame_productividad_digitadores_periodo" class='preview-pane'
						data-src='<?php echo ENV_WEBROOT_FULL_URL;?>dashboard/grafico_digitadores_por_periodo<?php echo $url_completa;?>'
						width='100%' height='305' frameborder='0' scrolling='yes'></iframe>
						<a
						href="<?php echo ENV_WEBROOT_FULL_URL;?>dashboard/grafico_digitadores_por_periodo<?php echo $url_completa;?>"
						target="_blank">ver gr&aacute;fico completo</a>
					</div>
					<div class="tab-pane cont" id="distribucion_consumos">
						<h2>Control de Distribuci&oacute;n de Consumos</h2>
						<iframe id="frame_distribucion_consumos" class='preview-pane'
						data-src='<?php echo ENV_WEBROOT_FULL_URL;?>dashboard/grafico_rendimiento_consumo_v2<?php echo $url_completa;?>'
						width='100%' height='700' frameborder='0' scrolling='yes'></iframe>
						<a
						href="<?php echo ENV_WEBROOT_FULL_URL;?>dashboard/grafico_rendimiento_consumo_v2<?php echo $url_completa;?>"
						target="_blank">ver gr&aacute;fico completo</a>
					</div>
					<div class="tab-pane cont" id="observaciones_ciclo">
						<h2>Resumen Observaciones por Ciclo</h2>
						<iframe id="frame_observaciones_ciclo" class='preview-pane'
						data-src='<?php echo ENV_WEBROOT_FULL_URL;?>dashboard/grafico_observaciones_por_ciclo<?php echo $url_completa;?>'
						width='100%' height='445' frameborder='0' scrolling='no'></iframe>
						<a
						href="<?php echo ENV_WEBROOT_FULL_URL ?>dashboard/grafico_observaciones_por_ciclo<?php echo $url_completa;?>"
						target="_blank">ver gr&aacute;fico completo</a> | <a
						href="<?php echo ENV_WEBROOT_FULL_URL ?>dashboard/reporte_observaciones"
						target="_blank">ver tabla</a>
					</div>
					<div class="tab-pane cont" id="acumulacion_consumos">
						<h2>Control de Acumulaci&oacute;n de Consumos por D&iacute;a</h2>
						<iframe id="frame_acumulacion_consumos" class='preview-pane'
						data-src='<?php echo ENV_WEBROOT_FULL_URL;?>dashboard/grafico_consumo_por_dia_v2<?php echo $url_completa;?>'
						width='100%' height='305' frameborder='0' scrolling='yes'></iframe>
						<a
						href="<?php echo ENV_WEBROOT_FULL_URL;?>dashboard/grafico_consumo_por_dia_v2<?php echo $url_completa;?>"
						target="_blank">ver gr&aacute;fico completo</a> | <a
						href="<?php echo ENV_WEBROOT_FULL_URL;?>dashboard/resumen_consumo_por_dia_y_ubicacion<?php echo $url_completa;?>"
						target="_blank">ver resumen</a> | <a
						href="<?php echo ENV_WEBROOT_FULL_URL;?>reportes/resumen_suministros_cronograma<?php echo $url_completa;?>"
						target="_blank">ver resumen detallado</a>
					</div>
					<div class="tab-pane cont" id="metodos_evaluacion">
						<h2>Suministros por M&eacute;todo de Evaluaci&oacute;n</h2>
						<iframe id="frame_metodos_evaluacion" class='preview-pane'
						data-src='<?php echo ENV_WEBROOT_FULL_URL;?>dashboard/grafico_suministros_por_metodo<?php echo $url_completa;?>'
						width='100%' height='250' frameborder='0' scrolling='yes'></iframe>
						<a
						href="<?php echo ENV_WEBROOT_FULL_URL;?>dashboard/grafico_suministros_por_metodo<?php echo $url_completa;?>"
						target="_blank">ver gr&aacute;fico completo</a>
					</div>
					<div class="tab-pane cont" id="monitor_gps">
						<h2>Monitor GPS - Ubicacion en Tiempo Real</h2>
						<iframe id="frame_monitor_gps" class='preview-pane'
						data-src='<?php echo ENV_WEBROOT_FULL_URL;?>MonitorearLecturas/mapa_monitoreo_free/F/<?php echo date('Y-m-d');?>/<?php echo date('Y-m-d');?><?php echo $url_completa;?>'
						width='100%' height='320' frameborder='0' scrolling='no'></iframe>
						<a
						href="<?php echo ENV_WEBROOT_FULL_URL;?>MonitorearLecturas/mapa_monitoreo/F/<?php echo date('Y-m-d');?>/<?php echo date('Y-m-d');?><?php echo $url_completa;?>"
						target="_blank">ver mapa completo</a>
					</div>
				</div>
			</div>
		</div>

	</div>
</div>
<script>
$('.tabs_dashboard li a').click(function(){
	var_href = $(this).attr('href');
	console.log(var_href);
	if(var_href=='#productividad_lecturistas_semanal'){
		if(!$('#frame_productividad_lecturistas_semanal').attr('src')){
			$('#frame_productividad_lecturistas_semanal').attr('src',$('#frame_productividad_lecturistas_semanal').data('src'));
		}
	}
	if(var_href=='#productividad_lecturistas_periodo'){
		if(!$('#frame_productividad_lecturistas_periodo').attr('src')){
			$('#frame_productividad_lecturistas_periodo').attr('src',$('#frame_productividad_lecturistas_periodo').data('src'));
		}
	}
	if(var_href=='#productividad_digitadores_periodo'){
		if(!$('#frame_productividad_digitadores_periodo').attr('src')){
			$('#frame_productividad_digitadores_periodo').attr('src',$('#frame_productividad_digitadores_periodo').data('src'));
		}
	}
	if(var_href=='#distribucion_consumos'){
		if(!$('#frame_distribucion_consumos').attr('src')){
			setTimeout(function(){
			$('#frame_distribucion_consumos').attr('src',$('#frame_distribucion_consumos').data('src'));
			}, 300);
		}
	}
	if(var_href=='#observaciones_ciclo'){
		if(!$('#frame_observaciones_ciclo').attr('src')){
			$('#frame_observaciones_ciclo').attr('src',$('#frame_observaciones_ciclo').data('src'));
		}
	}
	if(var_href=='#acumulacion_consumos'){
		if(!$('#frame_acumulacion_consumos').attr('src')){
			$('#frame_acumulacion_consumos').attr('src',$('#frame_acumulacion_consumos').data('src'));
		}
	}
	if(var_href=='#metodos_evaluacion'){
		if(!$('#frame_metodos_evaluacion').attr('src')){
			$('#frame_metodos_evaluacion').attr('src',$('#frame_metodos_evaluacion').data('src'));
		}
	}
	if(var_href=='#monitor_gps'){
		if(!$('#frame_monitor_gps').attr('src')){
			setTimeout(function(){
			$('#frame_monitor_gps').attr('src',$('#frame_monitor_gps').data('src'));
			}, 300);
		}
	}
});

$('.btn_refresh_kpi_dashboard').on('click',function(){
	$(this).attr('disabled',true);
	$('.lbl_refresh_kpi_dashboard').html('Actualizando, esto puede tardar unos minutos, por favor espere. <img src="<?php echo ENV_WEBROOT_FULL_URL ?>/js/jquery.select2/select2-spinner.gif">');
	$.ajax({
        url : ENV_WEBROOT_FULL_URL+"Dashboard/ajax_refresh_kpi",
		type : "POST",
		dataType : 'json',
		success : function(data) {
			console.log(data);
			if(data.success==true){
				$('.lbl_refresh_kpi_dashboard').html('Actualizado correctamente! <i class="fa fa-check" style="color: green;"></i>, estamos redireccionando, por favor espere.');
				window.open(ENV_WEBROOT_FULL_URL+'dashboard/index_new/'+$('#unidadneg').val()+'/'+$('#id_ciclo').val(),'_top');
			}
		}
	});
});
</script>
