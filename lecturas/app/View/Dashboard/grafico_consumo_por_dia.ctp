<?php
$rs=count($arr);

$categorias_x='';
$serie_manual='';
if ($rs > 0) {
	foreach ($arr as $k => $obj){
		$categorias_x .= "'".substr($obj[0]['fecha'],0,10)."',";
		$serie_manual .= $obj[0]['montoconsumo'].",";
	}
	$categorias_x = substr($categorias_x,0,-1);
	$serie_manual = substr($serie_manual,0,-1);
}else{
	echo 'No hay datos';
	exit();
}
?>
<!DOCTYPE html>
<html lang="es" >
<head>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta http-equiv="content-type" content="text/html; charset=UTF-8">
<title>Grafico Pexport</title>

<script type="text/javascript" src="//code.jquery.com/jquery-1.9.1.js"></script>



<script type="text/javascript">//<![CDATA[ 

$(function () {
    $('#container').highcharts({
        title: {
            text: 'Consumo por fecha de lectura',
            x: -20 //center
        },
        subtitle: {
            text: 'Source: www.pexport.com.pe',
            x: -20
        },
        xAxis: {
            categories: [<?php echo $categorias_x;?>],
            labels: {
                rotation: -45,
                style: {
                    fontSize: '13px',
                    fontFamily: 'Verdana, sans-serif'
                }
            }
        },
        yAxis: {
            title: {
                text: 'Consumo (Kwh)'
            },
            plotLines: [{
                value: 0,
                width: 1,
                color: '#808080'
            }]
        },
        tooltip: {
            valueSuffix: ' Kwh'
        },
        legend: {
        	enabled: false
        },
        credits: {
            enabled: false
        },
        series: [{
            name: 'Consumo (Kwh)',
            data: [<?php echo $serie_manual;?>]
        }]
    });
});
//]]>  

</script>


</head>
<body>
	<!-- 
	<script src="http://code.highcharts.com/highcharts.js"></script>
	<script src="http://code.highcharts.com/modules/exporting.js"></script>
	 -->
	
	<script src="http://code.highcharts.com/highcharts.js"></script>
	<script src="http://code.highcharts.com/modules/exporting.js"></script>

	<div id="container" style="width: 100%; height: 100%; margin: 0 auto">
	</div>
	
</body>
</html>