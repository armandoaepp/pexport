<?php 
if (!isset($NEGOCIO)) {
	header("Location:".ENV_WEBROOT_FULL_URL."/Usuarios/logout");
	die();
}
?>
<?php 
if (isset($id_usuario) && $id_usuario == 25){
	$permiso_lectura = 'true';
}else {
	$permiso_lectura = 'true';
}
echo $this->element('menu',array('active'=>'monitorearlectura', 'open'=>'false', 'permiso_lectura' =>$permiso_lectura, 'id_usuario ' =>isset($id_usuario)?$id_usuario:null));

if ((isset($id_usuario) && $id_usuario == 25) || (isset($id_usuario) && $id_usuario == 131)){
	$modulo_permiso = array("dashboard","monitorearlectura",'monitoreargps','extranet','rep_historico','rep_comparar_periodo','rep_consumo_por_dia_y_ubicacion','rep_detallado_ciclo','cronograma');
}elseif (isset($id_usuario) && $id_usuario == 69){//jsanchez
	$modulo_permiso = array("dashboard","evaluarinconsistencia", "extranet", "importar", "listarciclosector", "monitorearlectura",'monitoreargps', "digitarlecturas",'usuarios','rep_observaciones','rep_personal_para_lectura','rep_historico','rep_ordenes_por_observacion','rep_mineria','rep_mineria2','rep_comparar_periodo','rep_consumo_por_dia_y_ubicacion','rep_suministros_cronograma','rep_detallado_ciclo','reporte_ordenes_historico','cronograma');
}elseif (isset($id_usuario) && $id_usuario >= 132 && $id_usuario <= 141){//digitadores trux
	$modulo_permiso = array("digitarlecturas");
}else {
	$modulo_permiso = array("dashboard","evaluarinconsistencia", "extranet", "importar", "listarciclosector", "monitorearlectura",'monitoreargps', "digitarlecturas",'usuarios','rep_observaciones','rep_personal_para_lectura','rep_historico','rep_ordenes_por_observacion','rep_comparar_periodo','rep_consumo_por_dia_y_ubicacion','rep_suministros_cronograma','rep_detallado_ciclo','reporte_ordenes_historico','cronograma');
}
?>



<div class="container-fluid" id="pcont">
	<div class="cl-mcont">
		<h3 class='text-center'>sistema de lectura - Sistema Integrado para la
			Gesti&oacute;n Operativa de Facturaci&oacute;n</h3>
		</br>


		<div class="gallery-cont">
			

			<?php if(in_array("dashboard", $modulo_permiso)){ ?>
			<div class="item">
				<div class="photo">
					<div class="head">
						<h4 style="color: #444; font-weight: bold;">Dashboard</h4>
						<span class="desc" style="color: #444;">Indicadores Clave de
							Proceso</span>
					</div>
					<div class="img">
						<img
							src="<?php echo ENV_WEBROOT_FULL_URL;?>images/modulos/dashboard2.png" />
						<div class="over">
							<div class="func">
								<a href="<?php echo ENV_WEBROOT_FULL_URL;?>dashboard"><i
									class="fa fa-link"></i> </a><a class="image-zoom"
									href="<?php echo ENV_WEBROOT_FULL_URL;?>images/modulos/dashboard2.png"><i
									class="fa fa-search"></i> </a>
							</div>
						</div>
					</div>
				</div>
			</div>
			<?php }?>
			<?php if(in_array("cronograma", $modulo_permiso)){ ?>
			<div class="item">
				<div class="photo">
					<div class="head">
						<span class="pull-right active"> <span
							class="label label-success pull-right" style="color: white;"><i
								class="fa fa-star-o"></i> Nuevo</span>
						</span>
						<h4 style="color: #444; font-weight: bold;">Cronograma</h4>
						<span class="desc" style="color: #444;">Cronograma del proceso
							actual de Lecturas</span>
					</div>
					<div class="img">
						<img
							src="<?php echo ENV_WEBROOT_FULL_URL;?>images/modulos/cronograma.png" />
						<div class="over">
							<div class="func">
								<a
									href="<?php echo ENV_WEBROOT_FULL_URL;?>comlec_ordenlecturas/cronograma"><i
									class="fa fa-link"></i> </a><a class="image-zoom"
									href="<?php echo ENV_WEBROOT_FULL_URL;?>images/modulos/cronograma.png"><i
									class="fa fa-search"></i> </a>
							</div>
						</div>
					</div>
				</div>
			</div>
			<?php }?>
			<?php if(in_array("monitorearlectura", $modulo_permiso)){ ?>
			<div class="item">
				<div class="photo">
					<div class="head">
						<span class="pull-right active"> <span
							class="label label-success pull-right" style="color: white;">Actualizado</span>
						</span>
						<h4 style="color: #444; font-weight: bold;">Monitoreo</h4>
						<span class="desc" style="color: #444;">Lecturas</span>
					</div>
					<div class="img">
						<img
							src="<?php echo ENV_WEBROOT_FULL_URL;?>images/modulos/monitoreo2.png" />
						<div class="over">
							<div class="func">
								<a
									href="<?php echo ENV_WEBROOT_FULL_URL ?>MonitorearLecturas/monitorear_lectura"><i
									class="fa fa-link"></i> </a><a class="image-zoom"
									href="<?php echo ENV_WEBROOT_FULL_URL;?>images/modulos/monitoreo2.png"><i
									class="fa fa-search"></i> </a>
							</div>
						</div>
					</div>
				</div>
			</div>
			<?php }?>
			<?php if(in_array("monitoreargps", $modulo_permiso)){ ?>
			<div class="item">
				<div class="photo">
					<div class="head">
						<h4 style="color: #444; font-weight: bold;">Monitor GPS</h4>
						<span class="desc" style="color: #444;">Ubicacion en Tiempo Real</span>
					</div>
					<div class="img">
						<img
							src="<?php echo ENV_WEBROOT_FULL_URL;?>images/modulos/monitoreogps.png" />
						<div class="over">
							<div class="func">
								<a
									href="<?php echo ENV_WEBROOT_FULL_URL;?>MonitorearLecturas/mapa_monitoreo"><i
									class="fa fa-link"></i> </a><a class="image-zoom"
									href="<?php echo ENV_WEBROOT_FULL_URL;?>images/modulos/monitoreogps.png"><i
									class="fa fa-search"></i> </a>
							</div>
						</div>
					</div>
				</div>
			</div>
			<?php }?>
			<?php if(in_array("evaluarinconsistencia", $modulo_permiso)){ ?>
			<div class="item">
				<div class="photo">
					<div class="head">
						<h4 style="color: #444; font-weight: bold;">Evaluar
							Inconsistencias</h4>
						<span class="desc" style="color: #444;">Lecturas</span>
					</div>
					<div class="img">
						<img
							src="<?php echo ENV_WEBROOT_FULL_URL;?>images/modulos/evaluarinconsistencias.png" />
						<div class="over">
							<div class="func">
								<a
									href="<?php echo ENV_WEBROOT_FULL_URL;?>EvaluarInconsistencias/evaluar_inconsistencia"><i
									class="fa fa-link"></i> </a><a class="image-zoom"
									href="<?php echo ENV_WEBROOT_FULL_URL;?>images/modulos/evaluarinconsistencias.png"><i
									class="fa fa-search"></i> </a>
							</div>
						</div>
					</div>
				</div>
			</div>
			<?php }?>
			<?php if(in_array("digitarlecturas", $modulo_permiso)){ ?>
			<div class="item">
				<div class="photo">
					<div class="head">
						<h4 style="color: #444; font-weight: bold;">Digitaci&oacute;n</h4>
						<span class="desc" style="color: #444;">Digitar Lecturas</span>
					</div>
					<div class="img">
						<img
							src="<?php echo ENV_WEBROOT_FULL_URL;?>images/modulos/digitacion.png" />
						<div class="over">
							<div class="func">
								<a
									href="<?php echo ENV_WEBROOT_FULL_URL;?>ComlecOrdenlecturas/digitarlecturas"><i
									class="fa fa-link"></i> </a><a class="image-zoom"
									href="<?php echo ENV_WEBROOT_FULL_URL;?>images/modulos/digitacion.png"><i
									class="fa fa-search"></i> </a>
							</div>
						</div>
					</div>
				</div>
			</div>
			<?php }?>
			<?php if(in_array("listarciclosector", $modulo_permiso)){ ?>
			<div class="item">
				<div class="photo">
					<div class="head">
						<span class="pull-right active"> <span
							class="label label-success pull-right" style="color: white;">Actualizado</span>
						</span>
						<h4 style="color: #444; font-weight: bold;">Asignaci&oacute;n</h4>
						<span class="desc" style="color: #444;">Asignar Ordenes</span>
					</div>
					<div class="img">
						<img
							src="<?php echo ENV_WEBROOT_FULL_URL;?>images/modulos/Asignacion.png" />
						<div class="over">
							<div class="func">
								<a
									href="<?php echo ENV_WEBROOT_FULL_URL;?>ComlecOrdenlecturas/listar_asignacion"><i
									class="fa fa-link"></i> </a><a class="image-zoom"
									href="<?php echo ENV_WEBROOT_FULL_URL;?>images/modulos/Asignacion.png"><i
									class="fa fa-search"></i> </a>
							</div>
						</div>
					</div>
				</div>
			</div>
			<?php }?>
			<?php if(in_array("importar", $modulo_permiso)){ ?>
			<div class="item">
				<div class="photo">
					<div class="head">
						<h4 style="color: #444; font-weight: bold;">Importaci&oacute;n</h4>
						<span class="desc" style="color: #444;">Importar Ordenes</span>
					</div>
					<div class="img">
						<img
							src="<?php echo ENV_WEBROOT_FULL_URL;?>images/modulos/importacion.png" />
						<div class="over">
							<div class="func">
								<a
									href="<?php echo ENV_WEBROOT_FULL_URL;?>ComlecOrdenlecturas/form"><i
									class="fa fa-link"></i> </a><a class="image-zoom"
									href="<?php echo ENV_WEBROOT_FULL_URL;?>images/modulos/importacion.png"><i
									class="fa fa-search"></i> </a>
							</div>
						</div>
					</div>
				</div>
			</div>
			<?php }?>
			<?php if(in_array("rep_historico", $modulo_permiso)){ ?>
			<div class="item">
				<div class="photo">
					<div class="head">
						<h4 style="color: #444; font-weight: bold;">Hist&oacute;rico por
							Suministro</h4>
						<span class="desc" style="color: #444;">Buscar Suministro en
							Hist&oacute;rico</span>
					</div>
					<div class="img">
						<img
							src="<?php echo ENV_WEBROOT_FULL_URL;?>images/modulos/historicoporsuministro.png" />
						<div class="over">
							<div class="func">
								<a
									href="<?php echo ENV_WEBROOT_FULL_URL;?>reportes/reporte_historico"><i
									class="fa fa-link"></i> </a><a class="image-zoom"
									href="<?php echo ENV_WEBROOT_FULL_URL;?>images/modulos/historicoporsuministro.png"><i
									class="fa fa-search"></i> </a>
							</div>
						</div>
					</div>
				</div>
			</div>
			<?php }?>
			<?php if(in_array("rep_suministros_cronograma", $modulo_permiso)){ ?>
			<div class="item">
				<div class="photo">
					<div class="head">
						<h4 style="color: #444; font-weight: bold;">Reporte Cronograma</h4>
						<span class="desc" style="color: #444;">Reporte lecturas
							seg&uacute;n avance cronograma</span>
					</div>
					<div class="img">
						<img
							src="<?php echo ENV_WEBROOT_FULL_URL;?>images/modulos/cronograma.png" />
						<div class="over">
							<div class="func">
								<a
									href="<?php echo ENV_WEBROOT_FULL_URL;?>reportes/resumen_suministros_cronograma"><i
									class="fa fa-link"></i> </a><a class="image-zoom"
									href="<?php echo ENV_WEBROOT_FULL_URL;?>images/modulos/cronograma.png"><i
									class="fa fa-search"></i> </a>
							</div>
						</div>
					</div>
				</div>
			</div>
			<?php }?>
			<?php if(in_array("rep_detallado_ciclo", $modulo_permiso)){ ?>
			<div class="item">
				<div class="photo">
					<div class="head">
						<h4 style="color: #444; font-weight: bold;">Detallado por Ciclo</h4>
						<span class="desc" style="color: #444;">Reporte detallado de
							avance por ciclo</span>
					</div>
					<div class="img">
						<img
							src="<?php echo ENV_WEBROOT_FULL_URL;?>images/modulos/detalladoporciclo.png" />
						<div class="over">
							<div class="func">
								<a
									href="<?php echo ENV_WEBROOT_FULL_URL;?>dashboard/resumen_por_ciclos?ajax=false"><i
									class="fa fa-link"></i> </a><a class="image-zoom"
									href="<?php echo ENV_WEBROOT_FULL_URL;?>images/modulos/detalladoporciclo.png"><i
									class="fa fa-search"></i> </a>
							</div>
						</div>
					</div>
				</div>
			</div>
			<?php }?>

			<?php if(in_array("dashboard", $modulo_permiso)){ ?>
			<div class="item">
				<div class="photo">
					<div class="head">
						<h4 style="color: #444; font-weight: bold;">Cartas PDF</h4>
						<span class="desc" style="color: #444;">Cartas de Reclamos en
							Consumos</span>
					</div>
					<div class="img">
						<img
							src="<?php echo ENV_WEBROOT_FULL_URL;?>images/modulos/carta-pdf.png" />
						<div class="over">
							<div class="func">
								<a href="<?php echo ENV_WEBROOT_FULL_URL;?>reportes/reclamos"><i
									class="fa fa-link"></i> </a><a class="image-zoom"
									href="<?php echo ENV_WEBROOT_FULL_URL;?>images/modulos/dashboard2.png"><i
									class="fa fa-search"></i> </a>
							</div>
						</div>
					</div>
				</div>
			</div>
			<?php }?>

		</div>
	</div>
</div>
<script type="text/javascript">
    $(document).ready(function(){
      
      //Initialize Mansory
      var $container = $('.gallery-cont');
      // initialize
      setTimeout(function(){
      $container.masonry({
        columnWidth: 0,
        itemSelector: '.item'
      });
      }, 300);
      
      //Resizes gallery items on sidebar collapse
      $("#sidebar-collapse").click(function(){
    	  setTimeout(function(){$container.masonry();}, 500);      
      });

    //MagnificPopup for images zoom
      $('.image-zoom').magnificPopup({ 
        type: 'image',
        mainClass: 'mfp-with-zoom', // this class is for CSS animation below
        zoom: {
        enabled: true, // By default it's false, so don't forget to enable it

        duration: 300, // duration of the effect, in milliseconds
        easing: 'ease-in-out', // CSS transition easing function 

        // The "opener" function should return the element from which popup will be zoomed in
        // and to which popup will be scaled down
        // By defailt it looks for an image tag:
        opener: function(openerElement) {
          // openerElement is the element on which popup was initialized, in this case its <a> tag
          // you don't need to add "opener" option if this code matches your needs, it's defailt one.
          var parent = $(openerElement).parents("div.img");
          return parent;
        }
        }

      });

    });
  </script>
  
<?php 
if(!isset($arr_datos_usuario['email'])){
?>
<button class="btn btn-danger btn-flat md-trigger" data-modal="modal-update-email" style="display: none;">Abrir Modal</button>
<!-- Nifty Modal -->
<div class="md-modal colored-header danger md-effect-10 md-show" id="modal-update-email">
	<div class="md-content">
		<div class="modal-header">
			<h3>Importante!. Actualiza tus datos</h3>
			<button type="button" class="close md-close" data-dismiss="modal" aria-hidden="true">×</button>
		</div>
		<div class="modal-body">
			<div class="text-center">
				<div class="i-circle danger">
					<i class="fa fa-check"></i>
				</div>
				<h4>Importante!</h4>
				<p>Estamos mejorando constantemente, por favor brindanos la siguiente informaci&oacute;n:</p>
				<div class="form-group" style="">
					<label class="control-label">Email</label>              
		        	<input type="text" id="txt_update_email" name="txt_update_email" placeholder="Ingrese tu email. (tuemail@email.com)" class="form-control">
		        </div>
		        <p>sistema de lectura usar&aacute; este email de forma confidencial para enviarte notificaciones, y mantenerte actualizado de todo lo que va ocurriendo de forma r&aacute;pida.</p>
			</div>
		</div>
		<div class="modal-footer">
			<button type="button" class="btn btn-default btn-flat md-close" data-dismiss="modal">Cancelar</button>
			<button id="btn-save-update-email" type="button" class="btn btn-danger btn-flat" data-dismiss="modal">Grabar</button>
		</div>
	</div>
</div>
<div class="md-overlay"></div>
<script>
function validateEmail(email) { 
    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
}

$('.md-trigger').modalEffects();
$('.md-trigger').click();
$('#btn-save-update-email').click(function(){
	if($.trim($('#txt_update_email').val())==''){
		alert('Por favor ingrese un email.');
	}else{

		if(validateEmail($.trim($('#txt_update_email').val()))){

			require(['loadmask'],function(){
			$('.modal-body').mask('Guardando datos, por favor espere...');
			});
			$('#btn-save-update-email').attr('disabled',true);
			$.ajax({
				url : '<?php echo ENV_WEBROOT_FULL_URL;?>Usuarios/save_update_user_email',
				dataType: 'json',
				type: "POST",
				data : { email: $.trim($('#txt_update_email').val())},
				success:function(data, textStatus, jqXHR){
					if(data.success){
						//alert(data.msg);
						$.gritter.add({
					        title: 'Guardado correctamente.',
					        text: data.msg,
					        class_name: 'success'
					      });
					}else{
						//alert(data.msg);
						$.gritter.add({
					        title: 'Error',
					        text: data.msg,
					        class_name: 'danger'
					      });
					}
					require(['loadmask'],function(){
						$('.modal-body').unmask();
					});
					$('#btn-save-update-email').attr('disabled',false);
					$('#modal-update-email').removeClass('md-show');
				}
			});
		}else{
			alert('Por favor ingrese un email valido.');
		}
	}
});
</script>
<?php }?>