<?php 
if(isset($unidad_neg) && isset($idciclo)){
	$url_completa = '/'.$unidad_neg.'/'.$idciclo;
}else{
	$url_completa = '';
}
?>
<!DOCTYPE html>
<html lang="es">
<head>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta http-equiv="content-type" content="text/html; charset=UTF-8">
<title>Grafico Pexport</title>

<script type="text/javascript" src="//code.jquery.com/jquery-1.9.1.js"></script>

<script type="text/javascript">//<![CDATA[

$(function () {
	$(function () {

		$('#container-1').highcharts({
	        chart: {
	            plotBackgroundColor: null,
	            plotBorderWidth: 0,
	            plotShadow: false
	        },
	        title: {
	            text: 'Suministros / Día',
	            align: 'center',
	            verticalAlign: 'middle',
	            y: -100
	        },
	        tooltip: {
	            pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
	        },
	        plotOptions: {
	            pie: {
	                dataLabels: {
	                    enabled: true,
	                    distance: -50,
	                    style: {
	                        fontWeight: 'bold',
	                        color: 'white',
	                        textShadow: '0px 1px 2px black'
	                    }
	                },
	                startAngle: -90,
	                endAngle: 90,
	                center: ['50%', '75%']
	            }
	        },
	        credits: {
	            enabled: false
	        },
	        series: [{
	            type: 'pie',
	            name: 'Suministros / Día',
	            innerSize: '50%',
	            data: [
	                {
	                    name: '<span style="font-size:20px;"><?php echo $cantidad_avanzados_dia_actual;?></span><br>Progreso',
	                    y: <?php echo $porcentaje_progreso_dia_actual;?>
	                },
	                {
	                    name: '<span style="font-size:20px;"><?php echo $pendientes_dia_actual;?></span><br>Pendiente',
	                    y: <?php echo $porcentaje_pendiente_dia_actual;?>,
	                    color: 'red'
	                }
	            ]
	        }]
	    });

		$('#container-2').highcharts({
	        chart: {
	            plotBackgroundColor: null,
	            plotBorderWidth: 0,
	            plotShadow: false
	        },
	        title: {
	            text: 'Suministros / Día',
	            align: 'center',
	            verticalAlign: 'middle',
	            y: -100
	        },
	        tooltip: {
	            pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
	        },
	        plotOptions: {
	            pie: {
	                dataLabels: {
	                    enabled: true,
	                    distance: -50,
	                    style: {
	                        fontWeight: 'bold',
	                        color: 'white',
	                        textShadow: '0px 1px 2px black'
	                    }
	                },
	                startAngle: -90,
	                endAngle: 90,
	                center: ['50%', '75%']
	            }
	        },
	        credits: {
	            enabled: false
	        },
	        series: [{
	            type: 'pie',
	            name: 'Suministros / Día',
	            innerSize: '50%',
	            data: [
	                {
	                    name: '<span style="font-size:20px;"><?php echo $consistencia_dia_actual;?></span><br>Consistentes',
	                    y: <?php echo $porcentaje_consistencia_dia_actual;?>
	                },
	                {
	                    name: '<span style="font-size:20px;"><?php echo $cantidad_inconsistentes_dia_actual;?></span><br>Inconsistentes',
	                    y: <?php echo $porcentaje_inconsistencia_dia_actual;?>,
	                    color: 'red'
	                }
	            ]
	        }]
	    });

		$('#container-3').highcharts({
	        chart: {
	            plotBackgroundColor: null,
	            plotBorderWidth: 0,
	            plotShadow: false
	        },
	        title: {
	            text: 'Suministros / Periodo',
	            align: 'center',
	            verticalAlign: 'middle',
	            y: -100
	        },
	        tooltip: {
	            pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
	        },
	        plotOptions: {
	            pie: {
	                dataLabels: {
	                    enabled: true,
	                    distance: -50,
	                    style: {
	                        fontWeight: 'bold',
	                        color: 'white',
	                        textShadow: '0px 1px 2px black'
	                    }
	                },
	                startAngle: -90,
	                endAngle: 90,
	                center: ['50%', '75%']
	            }
	        },
	        credits: {
	            enabled: false
	        },
	        series: [{
	            type: 'pie',
	            name: 'Suministros / Periodo',
	            innerSize: '50%',
	            data: [
	                {
	                    name: '<span style="font-size:20px;"><?php echo $cantidad_avanzados;?></span><br>Progreso',
	                    y: <?php echo $porcentaje_progreso;?>
	                },
	                {
	                    name: '<span style="font-size:20px;"><?php echo $pendientes;?></span><br>Pendiente',
	                    y: <?php echo $porcentaje_pendiente;?>,
	                    color: 'red'
	                }
	            ]
	        }]
	    });

		$('#container-4').highcharts({
	        chart: {
	            plotBackgroundColor: null,
	            plotBorderWidth: 0,
	            plotShadow: false
	        },
	        title: {
	            text: 'Suministros / Periodo',
	            align: 'center',
	            verticalAlign: 'middle',
	            y: -100
	        },
	        tooltip: {
	            pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
	        },
	        plotOptions: {
	            pie: {
	                dataLabels: {
	                    enabled: true,
	                    distance: -50,
	                    style: {
	                        fontWeight: 'bold',
	                        color: 'white',
	                        textShadow: '0px 1px 2px black'
	                    }
	                },
	                startAngle: -90,
	                endAngle: 90,
	                center: ['50%', '75%']
	            }
	        },
	        credits: {
	            enabled: false
	        },
	        series: [{
	            type: 'pie',
	            name: 'Suministros / Periodo',
	            innerSize: '50%',
	            data: [
	                {
	                    name: '<span style="font-size:20px;"><?php echo $consistencia;?></span><br>Consistentes',
	                    y: <?php echo $porcentaje_consistencia;?>
	                },
	                {
	                    name: '<span style="font-size:20px;"><?php echo $cantidad_inconsistentes;?></span><br>Inconsistentes',
	                    y: <?php echo $porcentaje_inconsistencia;?>,
	                    color: 'red'
	                }
	            ]
	        }]
	    });

	    // Bring life to the dials
	    setInterval(function () {



	    	$.ajax({
          	  url: "../monitor_progreso_semi_pie/true<?php echo $url_completa;?>",
          	  dataType: 'json',
          	  success: function(data) {

          		$('#container-1').highcharts({
        	        chart: {
        	            plotBackgroundColor: null,
        	            plotBorderWidth: 0,
        	            plotShadow: false
        	        },
        	        title: {
        	            text: 'Suministros / Día',
        	            align: 'center',
        	            verticalAlign: 'middle',
        	            y: -100
        	        },
        	        tooltip: {
        	            pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
        	        },
        	        plotOptions: {
        	            pie: {
        	                dataLabels: {
        	                    enabled: true,
        	                    distance: -50,
        	                    style: {
        	                        fontWeight: 'bold',
        	                        color: 'white',
        	                        textShadow: '0px 1px 2px black'
        	                    }
        	                },
        	                startAngle: -90,
        	                endAngle: 90,
        	                center: ['50%', '75%']
        	            }
        	        },
        	        credits: {
        	            enabled: false
        	        },
        	        series: [{
        	            type: 'pie',
        	            name: 'Suministros / Día',
        	            innerSize: '50%',
        	            data: [
        	                {
        	                    name: '<span style="font-size:20px;">' + data.cantidad_avanzados_dia_actual + '</span><br>Progreso',
        	                    y: data.porcentaje_progreso_dia_actual
        	                },
        	                {
        	                    name: '<span style="font-size:20px;">' + data.pendientes_dia_actual + '</span><br>Pendiente',
        	                    y: data.porcentaje_pendiente_dia_actual,
        	                    color: 'red'
        	                }
        	            ]
        	        }]
        	    });

        		$('#container-2').highcharts({
        	        chart: {
        	            plotBackgroundColor: null,
        	            plotBorderWidth: 0,
        	            plotShadow: false
        	        },
        	        title: {
        	            text: 'Suministros / Día',
        	            align: 'center',
        	            verticalAlign: 'middle',
        	            y: -100
        	        },
        	        tooltip: {
        	            pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
        	        },
        	        plotOptions: {
        	            pie: {
        	                dataLabels: {
        	                    enabled: true,
        	                    distance: -50,
        	                    style: {
        	                        fontWeight: 'bold',
        	                        color: 'white',
        	                        textShadow: '0px 1px 2px black'
        	                    }
        	                },
        	                startAngle: -90,
        	                endAngle: 90,
        	                center: ['50%', '75%']
        	            }
        	        },
        	        credits: {
        	            enabled: false
        	        },
        	        series: [{
        	            type: 'pie',
        	            name: 'Suministros / Día',
        	            innerSize: '50%',
        	            data: [
        	                {
        	                    name: '<span style="font-size:20px;">' + data.consistencia_dia_actual + '</span><br>Consistentes',
        	                    y: data.porcentaje_consistencia_dia_actual
        	                },
        	                {
        	                    name: '<span style="font-size:20px;">' + data.cantidad_inconsistentes_dia_actual + '</span><br>Inconsistentes',
        	                    y: data.porcentaje_inconsistencia_dia_actual,
        	                    color: 'red'
        	                }
        	            ]
        	        }]
        	    });

        		$('#container-3').highcharts({
        	        chart: {
        	            plotBackgroundColor: null,
        	            plotBorderWidth: 0,
        	            plotShadow: false
        	        },
        	        title: {
        	            text: 'Suministros / Periodo',
        	            align: 'center',
        	            verticalAlign: 'middle',
        	            y: -100
        	        },
        	        tooltip: {
        	            pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
        	        },
        	        plotOptions: {
        	            pie: {
        	                dataLabels: {
        	                    enabled: true,
        	                    distance: -50,
        	                    style: {
        	                        fontWeight: 'bold',
        	                        color: 'white',
        	                        textShadow: '0px 1px 2px black'
        	                    }
        	                },
        	                startAngle: -90,
        	                endAngle: 90,
        	                center: ['50%', '75%']
        	            }
        	        },
        	        credits: {
        	            enabled: false
        	        },
        	        series: [{
        	            type: 'pie',
        	            name: 'Suministros / Periodo',
        	            innerSize: '50%',
        	            data: [
        	                {
        	                    name: '<span style="font-size:20px;">' + data.cantidad_avanzados + '</span><br>Progreso',
        	                    y: data.porcentaje_progreso
        	                },
        	                {
        	                    name: '<span style="font-size:20px;">' + data.pendientes + '</span><br>Pendiente',
        	                    y: data.porcentaje_pendiente,
        	                    color: 'red'
        	                }
        	            ]
        	        }]
        	    });

        		$('#container-4').highcharts({
        	        chart: {
        	            plotBackgroundColor: null,
        	            plotBorderWidth: 0,
        	            plotShadow: false
        	        },
        	        title: {
        	            text: 'Suministros / Periodo',
        	            align: 'center',
        	            verticalAlign: 'middle',
        	            y: -100
        	        },
        	        tooltip: {
        	            pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
        	        },
        	        plotOptions: {
        	            pie: {
        	                dataLabels: {
        	                    enabled: true,
        	                    distance: -50,
        	                    style: {
        	                        fontWeight: 'bold',
        	                        color: 'white',
        	                        textShadow: '0px 1px 2px black'
        	                    }
        	                },
        	                startAngle: -90,
        	                endAngle: 90,
        	                center: ['50%', '75%']
        	            }
        	        },
        	        credits: {
        	            enabled: false
        	        },
        	        series: [{
        	            type: 'pie',
        	            name: 'Suministros / Periodo',
        	            innerSize: '50%',
        	            data: [
        	                {
        	                    name: '<span style="font-size:20px;">' + data.consistencia + '</span><br>Consistentes',
        	                    y: data.porcentaje_consistencia
        	                },
        	                {
        	                    name: '<span style="font-size:20px;">' + data.cantidad_inconsistentes + '</span><br>Inconsistentes',
        	                    y: data.porcentaje_inconsistencia,
        	                    color: 'red'
        	                }
        	            ]
        	        }]
        	    });

          	  }
	    	});
	    }, 60000);


	});
});
//]]>

</script>

<style>
.outer-div {
	position: relative;
	float: right;
	right: 50%;
}

.inner-div {
	position: relative;
	float: right;
	right: -50%;
}

.floating-div {
	float: left;
	border: 1px solid red;
	margin: 0 1.5em;
}

.clearfix:before, .clearfix:after {
	content: " ";
	display: table;
}

.clearfix:after {
	clear: both;
}

.clearfix {
	*zoom: 1;
}
</style>


</head>
<body style="text-align: center;">
	<!--
	<script src="http://code.highcharts.com/highcharts.js"></script>
	<script src="http://code.highcharts.com/modules/exporting.js"></script>
	 -->

	<script src="http://code.highcharts.com/highcharts.js"></script>
	<script src="http://code.highcharts.com/modules/exporting.js"></script>
	<script src="http://code.highcharts.com/highcharts-more.js"></script>
	<script src="http://code.highcharts.com/modules/solid-gauge.src.js"></script>

	<div class="clearfix">
		<div class="outer-div">
			<div class="inner-div">

				<div id="container-1"
					style="width: 290px; height: 290px; float: left;"></div>
				<div id="container-2"
					style="width: 290px; height: 290px; float: left;"></div>

				<div id="container-3"
					style="width: 290px; height: 290px; float: left;"></div>
				<div id="container-4"
					style="width: 290px; height: 290px; float: left;"></div>
			</div>
		</div>

	</div>

</body>
</html>