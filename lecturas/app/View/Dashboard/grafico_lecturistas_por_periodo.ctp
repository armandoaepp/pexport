<?php
$rs=count($arr_ordenes);

$categorias_x='';
$serie_movil='';
$serie_manual='';
if ($rs > 0) {
	foreach ($arr_ordenes as $k => $obj){
		$categorias_x .= "'".$obj[0]['nombre']."',";
		$serie_movil .= $obj[0]['movil'].",";
		$serie_manual .= $obj[0]['manual'].",";
	}
	$categorias_x = substr($categorias_x,0,-1);
	$serie_movil = substr($serie_movil,0,-1);
	$serie_manual = substr($serie_manual,0,-1);
}else{
	echo 'No hay datos';
	exit();
}
?>
<!DOCTYPE html>
<html lang="es" >
<head>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta http-equiv="content-type" content="text/html; charset=UTF-8">
<title>Grafico Pexport</title>

<script type="text/javascript" src="//code.jquery.com/jquery-1.9.1.js"></script>



<script type="text/javascript">//<![CDATA[ 

$(function () {
    $('#container').highcharts({
        chart: {
            type: 'bar'
        },
        title: {
            text: 'Resumen de Ordenes Finalizadas por Lecturista'
        },
        subtitle: {
            text: 'Source: www.pexport.com.pe'
        },
        xAxis: {
            categories: [<?php echo $categorias_x;?>],
            title: {
                text: null
            }
        },
        yAxis: {
            min: 0,
            title: {
                text: 'Cantidad de suministros',
                align: 'high'
            },
            labels: {
                overflow: 'justify'
            }
        },
        tooltip: {
            valueSuffix: ' suministros'
        },
        plotOptions: {
            bar: {
                dataLabels: {
                    enabled: true
                }
            }
        },
        legend: {
            layout: 'vertical',
            align: 'right',
            verticalAlign: 'top',
            x: -40,
            y: 100,
            floating: true,
            borderWidth: 1,
            backgroundColor: ((Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'),
            shadow: true
        },
        credits: {
            enabled: false
        },
        series: [{
            name: 'Movil',
            data: [<?php echo $serie_movil;?>]
        }, {
            name: 'Manual',
            data: [<?php echo $serie_manual;?>]
        }]
    });
});
//]]>  

</script>


</head>
<body>
	<!-- 
	<script src="http://code.highcharts.com/highcharts.js"></script>
	<script src="http://code.highcharts.com/modules/exporting.js"></script>
	 -->
	
	<script src="http://code.highcharts.com/highcharts.js"></script>
	<script src="http://code.highcharts.com/modules/exporting.js"></script>

	<div id="container" style="width: 100%; height: 100%; margin: 0 auto">
	</div>
	
</body>
</html>