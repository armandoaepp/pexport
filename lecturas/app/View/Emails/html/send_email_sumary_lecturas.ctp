<!-- MODULE ROW // -->
<tr>
	<td align="center" valign="top">
		<!-- CENTERING TABLE // -->
		<table border="0" cellpadding="0" cellspacing="0" width="100%"
			bgcolor="#F8F8F8">
			<tr>
				<td align="center" valign="top">
					<!-- FLEXIBLE CONTAINER // -->
					<table border="0" cellpadding="0" cellspacing="0" width="500"
						class="flexibleContainer">
						<tr>
							<td align="center" valign="top" width="500"
								class="flexibleContainerCell">
								<table border="0" cellpadding="30" cellspacing="0" width="100%">
									<tr>
										<td align="center" valign="top">
											<!-- CONTENT TABLE // -->
											<table border="0" cellpadding="0" cellspacing="0"
												width="100%">
												<tr>
													<td valign="top" class="textContent">
														<h3
															style="color: #5F5F5F; line-height: 125%; font-family: Helvetica, Arial, sans-serif; font-size: 20px; font-weight: normal; margin-top: 0; margin-bottom: 3px; text-align: left;">Resumen del progreso de toma de lectura de medidores.</h3>
														<div
															style="text-align: left; font-family: Helvetica, Arial, sans-serif; font-size: 15px; margin-bottom: 0; color: #5F5F5F; line-height: 135%;">
		<table style="font-size: 10px;">
		<thead>
			<tr>
				<th style="border:none; background-color:rgb(77, 144, 253); font-family:verdana; color:#fff; padding:6px;">Ciclo</th>
				<th style="border:none; background-color:rgb(77, 144, 253); font-family:verdana; color:#fff; padding:6px;"><span></span>Sector</th>
				<th style="border:none; background-color:rgb(77, 144, 253); font-family:verdana; color:#fff; padding:6px;" class='right'><span></span>Total Sumi.</th>
				<th style="border:none; background-color:rgb(77, 144, 253); font-family:verdana; color:#fff; padding:6px;" class='right' title="Asignados"><span></span>Asig.</th>
				<th style="border:none; background-color:rgb(77, 144, 253); font-family:verdana; color:#fff; padding:6px;" class='right' title="Descargados"><span></span>Desc.</th>
				<th style="border:none; background-color:rgb(77, 144, 253); font-family:verdana; color:#fff; padding:6px;" class='right' title="Finalizados"><span></span>Final.</th>
				<th style="border:none; background-color:rgb(77, 144, 253); font-family:verdana; color:#fff; padding:6px;" class='right' title="Pendientes"><span></span>Pend.</th>
				<th style="border:none; background-color:rgb(77, 144, 253); font-family:verdana; color:#fff; padding:6px;" class='right' title="Consistentes"><span></span>Cons.</th>
				<th style="border:none; background-color:rgb(77, 144, 253); font-family:verdana; color:#fff; padding:6px;" class='right' title="Inconsistentes"><span></span>Inc.</th>
				<th style="border:none; background-color:rgb(77, 144, 253); font-family:verdana; color:#fff; padding:6px;" class='right'><span></span>Enviados a Distriluz</th>
				<th style="border:none; background-color:rgb(77, 144, 253); font-family:verdana; color:#fff; padding:6px;" class='right' title="Observaciones con lectura"><span></span>Obs.
					con lectura</th>
				<th style="border:none; background-color:rgb(77, 144, 253); font-family:verdana; color:#fff; padding:6px;" class='right' title="Observaciones sin lectura"><span></span>Obs.
					sin lectura</th>
				<th style="border:none; background-color:rgb(77, 144, 253); font-family:verdana; color:#fff; padding:6px;" class='right' title="Consumo cero"><span></span>Cons. cero</th>
				<th style="border:none; background-color:rgb(77, 144, 253); font-family:verdana; color:#fff; padding:6px;" class='right' title="Consumo menor que 35"><span></span>Cons. >
					35</th>
				<th style="border:none; background-color:rgb(77, 144, 253); font-family:verdana; color:#fff; padding:6px;" class='right' title="Consumo mayor que 1000"><span></span>Cons. >
					1000</th>
				<th style="border:none; background-color:rgb(77, 144, 253); font-family:verdana; color:#fff; padding:6px;" class='right' title="Consumo mayor 100% que el anterior"><span></span>Cons.
					> 100% que el anterior</th>
			</tr>
		</thead>
		<tbody class='no-border-x'>
			<?php
			$sum_suministros = 0;
			$sum_asignados = 0;
			$sum_descargados = 0;
			$sum_finalizados = 0;
			$sum_pendientes = 0;
			$sum_consistentes = 0;
			$sum_inconsistentes_actuales = 0;
			$sum_inconsistentes_evaluadas = 0;
			$sum_observaciones_con_lectura = 0;
			$sum_observaciones_sin_lectura = 0;
			$sum_consumo_cero = 0;
			$sum_consumo_menor_35 = 0;
			$sum_consumo_mayor_1000 = 0;
			$sum_consumo_mayor_100_porciento = 0;
			$sum_enviados_distriluz = 0;
			foreach ( $arr_ciclos as $k => $obj){
				?>
				<tr style="border:none; background-color:#eee; font-family:verdana; color:#222; padding:10px 5px; text-align:center;">
				<td><?php echo $obj[0]['idciclo'].' - '.$obj[0]['nombciclo'];?></td>
				<td><?php echo $obj[0]['sectores'];?></td>
				<td class='right'><?php echo number_format($obj[0]['suministros']);?></td>
				<td class='right'><?php echo number_format($obj[0]['asignados']);?></td>
				<td class='right'><?php echo number_format($obj[0]['descargados']);?></td>
				<td class='right'><?php echo number_format($obj[0]['finalizados']);?></td>
				<td class='right'>
				<?php 
				if(($obj[0]['suministros']-$obj[0]['finalizados'])==0){
				?>
				<span class="badge badge-success" style="background-color: #60C060 !important;display: inline-block;min-width: 10px;line-height: 1;color: #fff;text-align: center;white-space: nowrap;vertical-align: baseline;background-color: #999;border-radius: 10px;font-weight: normal;font-size: 95%;padding: 4px 6px;">0</span>
				<?php }else{?>
				<span class="badge badge-danger" style="background-color: #ED5B56 !important;display: inline-block;min-width: 10px;line-height: 1;color: #fff;text-align: center;white-space: nowrap;vertical-align: baseline;background-color: #999;border-radius: 10px;font-weight: normal;font-size: 95%;padding: 4px 6px;"><?php echo number_format($obj[0]['suministros']-$obj[0]['finalizados']);?></span>
				<?php }?>
				</td>
				<td>
				<?php echo number_format($obj[0]['finalizados']-$obj[0]['inconsistentes_actuales']);?>
				</td>
				<?php 
				$style_badge_inconsistente = '';
				if($obj[0]['inconsistentes_actuales']>0){
					$style_badge_inconsistente = 'style="background-color: #ED5B56 !important;display: inline-block;min-width: 10px;line-height: 1;color: #fff;text-align: center;white-space: nowrap;vertical-align: baseline;background-color: #999;border-radius: 10px;font-weight: normal;font-size: 95%;padding: 4px 6px;"';
				}
				?>
				<td class='right'><a
					href="<?php echo $path;?>EvaluarInconsistencias/evaluar_inconsistencia/U/<?php echo date('Y-m-d');?>/<?php echo date('Y-m-d');?>/0/<?php echo $obj[0]['idciclo'];?>/0/0/0/0/0/"
					target="_blank"> <span
						<?php echo $style_badge_inconsistente;?>><?php echo number_format($obj[0]['inconsistentes_actuales']);?></span></a> de <?php echo number_format($obj[0]['inconsistentes_evaluadas']+$obj[0]['inconsistentes_actuales']);?></td>
				<td class='right'><span
					title="Consistencias pendientes de envio: <?php echo number_format(($obj[0]['finalizados']-$obj[0]['inconsistentes_actuales'])-$obj[0]['enviados_distriluz']);?>"><?php echo number_format($obj[0]['enviados_distriluz']);?></span> de <span><?php echo number_format($obj[0]['suministros']);?></span>
					<br>
					<?php if($obj[0]['suministros']==$obj[0]['enviados_distriluz']){?>
					<span class="badge badge-success" style="background-color: #60C060 !important;display: inline-block;min-width: 10px;line-height: 1;color: #fff;text-align: center;white-space: nowrap;vertical-align: baseline;background-color: #999;border-radius: 10px;font-weight: normal;font-size: 95%;padding: 4px 6px;">OK</span>
					<?php }?>
				</td>
				<td class='right'><a
					href="<?php echo $path;?>EvaluarInconsistencias/evaluar_inconsistencia/U/<?php echo date('Y-m-d');?>/<?php echo date('Y-m-d');?>/0/<?php echo $obj[0]['idciclo'];?>/0/0/0/0/0/OBSCL"
					target="_blank">
				<?php echo number_format($obj[0]['observaciones_con_lectura']);?>
				</a></td>
				<td class='right'><a
					href="<?php echo $path;?>EvaluarInconsistencias/evaluar_inconsistencia/U/<?php echo date('Y-m-d');?>/<?php echo date('Y-m-d');?>/0/<?php echo $obj[0]['idciclo'];?>/0/0/0/0/0/OBSSL"
					target="_blank">
				<?php echo number_format($obj[0]['observaciones_sin_lectura']);?>
				</a></td>
				<td class='right'><a
					href="<?php echo $path;?>EvaluarInconsistencias/evaluar_inconsistencia/U/<?php echo date('Y-m-d');?>/<?php echo date('Y-m-d');?>/0/<?php echo $obj[0]['idciclo'];?>/0/0/0/0/0/CC"
					target="_blank">
				<?php echo number_format($obj[0]['consumo_cero']);?>
				</a></td>
				<td class='right'><a
					href="<?php echo $path;?>EvaluarInconsistencias/evaluar_inconsistencia/U/<?php echo date('Y-m-d');?>/<?php echo date('Y-m-d');?>/0/<?php echo $obj[0]['idciclo'];?>/0/0/0/0/34/"
					target="_blank">
				<?php echo number_format($obj[0]['consumo_menor_35']);?>
				</a></td>
				<td class='right'><a
					href="<?php echo $path;?>EvaluarInconsistencias/evaluar_inconsistencia/U/<?php echo date('Y-m-d');?>/<?php echo date('Y-m-d');?>/0/<?php echo $obj[0]['idciclo'];?>/0/0/0/1001/0/"
					target="_blank">
				<?php echo number_format($obj[0]['consumo_mayor_1000']);?>
				</a></td>
				<td class='right'><a
					href="<?php echo $path;?>EvaluarInconsistencias/evaluar_inconsistencia/U/<?php echo date('Y-m-d');?>/<?php echo date('Y-m-d');?>/0/<?php echo $obj[0]['idciclo'];?>/0/0/0/0/0/C100MA"
					target="_blank">
				<?php echo number_format($obj[0]['consumo_mayor_100_porciento']);?>
				</a></td>
			</tr>
				<?php
				$sum_suministros += $obj[0]['suministros'];
				$sum_asignados += $obj[0]['asignados'];
				$sum_descargados += $obj[0]['descargados'];
				$sum_finalizados += $obj[0]['finalizados'];
				$sum_pendientes += ($obj[0]['suministros']-$obj[0]['finalizados']);
				$sum_consistentes += ($obj[0]['finalizados']-$obj[0]['inconsistentes_actuales']);
				$sum_inconsistentes_actuales += $obj[0]['inconsistentes_actuales'];
				$sum_inconsistentes_evaluadas += $obj[0]['inconsistentes_evaluadas'];
				$sum_observaciones_con_lectura += $obj[0]['observaciones_con_lectura'];
				$sum_observaciones_sin_lectura += $obj[0]['observaciones_sin_lectura'];
				$sum_consumo_cero += $obj[0]['consumo_cero'];
				$sum_consumo_menor_35 += $obj[0]['consumo_menor_35'];
				$sum_consumo_mayor_1000 += $obj[0]['consumo_mayor_1000'];
				$sum_consumo_mayor_100_porciento += $obj[0]['consumo_mayor_100_porciento'];
				$sum_enviados_distriluz += $obj[0]['enviados_distriluz'];
			} 
			?>		
		</tbody>
		<tfoot>
			<tr>
				<th colspan="2">Total</th>
				<th class='right'><?php echo number_format($sum_suministros);?></th>
				<th class='right'><?php echo number_format($sum_asignados);?></th>
				<th class='right'><?php echo number_format($sum_descargados);?></th>
				<th class='right'><?php echo number_format($sum_finalizados);?></th>
				<th class='right'>
				<?php 
				if($sum_pendientes==0){
				?>
				<span class="badge badge-success" style="background-color: #60C060 !important;display: inline-block;min-width: 10px;line-height: 1;color: #fff;text-align: center;white-space: nowrap;vertical-align: baseline;background-color: #999;border-radius: 10px;font-weight: normal;font-size: 95%;padding: 4px 6px;">0</span>
				<?php }else{?>
				<span class="badge badge-danger" style="background-color: #ED5B56 !important;display: inline-block;min-width: 10px;line-height: 1;color: #fff;text-align: center;white-space: nowrap;vertical-align: baseline;background-color: #999;border-radius: 10px;font-weight: normal;font-size: 95%;padding: 4px 6px;"><?php echo number_format($sum_pendientes);?></span>
				<?php }?>
				</th>
				<?php 
				$style_badge_inconsistente = '';
				if($sum_inconsistentes_actuales>0){
					$style_badge_inconsistente = 'style="background-color: #ED5B56 !important;display: inline-block;min-width: 10px;line-height: 1;color: #fff;text-align: center;white-space: nowrap;vertical-align: baseline;background-color: #999;border-radius: 10px;font-weight: normal;font-size: 95%;padding: 4px 6px;"';
				}
				?>
				<th class='right'><?php echo number_format($sum_consistentes);?></th>
				<th class='right'><span
					<?php echo $style_badge_inconsistente;?>><?php echo number_format($sum_inconsistentes_actuales);?></span> de <?php echo number_format($sum_inconsistentes_evaluadas+$sum_inconsistentes_actuales);?></th>
				<th class='right'>
					<span
					title="Consistencias pendientes de envio: <?php echo number_format($sum_consistentes-$sum_enviados_distriluz);?>"><?php echo number_format($sum_enviados_distriluz);?></span> de <span><?php echo number_format($sum_suministros);?></span>
					<br>
					<?php if($sum_suministros==$sum_enviados_distriluz){?>
					<span class="badge badge-success" style="background-color: #60C060 !important;display: inline-block;min-width: 10px;line-height: 1;color: #fff;text-align: center;white-space: nowrap;vertical-align: baseline;background-color: #999;border-radius: 10px;font-weight: normal;font-size: 95%;padding: 4px 6px;">OK</span>
					<?php }?>
				</th>
				<th class='right'><?php echo number_format($sum_observaciones_con_lectura);?></th>
				<th class='right'><?php echo number_format($sum_observaciones_sin_lectura);?></th>
				<th class='right'><?php echo number_format($sum_consumo_cero);?></th>
				<th class='right'><?php echo number_format($sum_consumo_menor_35);?></th>
				<th class='right'><?php echo number_format($sum_consumo_mayor_1000);?></th>
				<th class='right'><?php echo number_format($sum_consumo_mayor_100_porciento);?></th>
			</tr>
		</tfoot>
	</table>

	<br>
	<br>
	<br>
	Leyenda:
	<table style="font-size: 10px;">
	<tr style="border:none; background-color:#eee; font-family:verdana; color:#222; padding:10px 5px; text-align:center;">
	<td>Total Sumi.</td><td>Total de Suministros</td>
	</tr>
	<tr style="border:none; background-color:#eee; font-family:verdana; color:#222; padding:10px 5px; text-align:center;">
	<td>Asig.</td><td>Asignados</td>
	</tr>
	<tr style="border:none; background-color:#eee; font-family:verdana; color:#222; padding:10px 5px; text-align:center;">
	<td>Desc.</td><td>Descargados</td>
	</tr>
	<tr style="border:none; background-color:#eee; font-family:verdana; color:#222; padding:10px 5px; text-align:center;">
	<td>Final.</td><td>Finalizados</td>
	</tr>
	<tr style="border:none; background-color:#eee; font-family:verdana; color:#222; padding:10px 5px; text-align:center;">
	<td>Pend.</td><td>Pendientes</td>
	</tr>
	<tr style="border:none; background-color:#eee; font-family:verdana; color:#222; padding:10px 5px; text-align:center;">
	<td>Cons.</td><td>Consistentes</td>
	</tr>
	<tr style="border:none; background-color:#eee; font-family:verdana; color:#222; padding:10px 5px; text-align:center;">
	<td>Inc.</td><td>Inconsistentes<br><b>x</b> de <b>y</b><br>donde:<br><b>x</b> es el n&uacute;mero de inconsistencias actuales.<br><b>y</b> es el n&uacute;mero de inconsistencias encontradas.</td>
	</tr>
	<tr style="border:none; background-color:#eee; font-family:verdana; color:#222; padding:10px 5px; text-align:center;">
	<td>Enviados a Distriluz</td><td>Enviados a Distriluz<br><b>x</b> de <b>y</b><br>donde:<br><b>x</b> es el n&uacute;mero de suministros enviados a Distriluz.<br><b>y</b> es el n&uacute;mero de suministros total a enviar.</td>
	</tr>
	<tr style="border:none; background-color:#eee; font-family:verdana; color:#222; padding:10px 5px; text-align:center;">
	<td>Obs. con lectura</td><td>Observaciones con lectura</td>
	</tr>
	<tr style="border:none; background-color:#eee; font-family:verdana; color:#222; padding:10px 5px; text-align:center;">
	<td>Obs. sin lectura</td><td>Observaciones sin lectura</td>
	</tr>
	<tr style="border:none; background-color:#eee; font-family:verdana; color:#222; padding:10px 5px; text-align:center;">
	<td>Cons. cero</td><td>Consumo cero</td>
	</tr>
	<tr style="border:none; background-color:#eee; font-family:verdana; color:#222; padding:10px 5px; text-align:center;">
	<td>Cons. >	35</td><td>Consumo menor que 35</td>
	</tr>
	<tr style="border:none; background-color:#eee; font-family:verdana; color:#222; padding:10px 5px; text-align:center;">
	<td>Cons. >	1000</td><td>Consumo mayor que 1000</td>
	</tr>
	<tr style="border:none; background-color:#eee; font-family:verdana; color:#222; padding:10px 5px; text-align:center;">
	<td>Cons. > 100% que el anterior</td><td>Consumo mayor 100% que el anterior</td>
	</tr>
	</table>

					
														</div>
													</td>
												</tr>
											</table> <!-- // CONTENT TABLE -->

										</td>
									</tr>
								</table>
							</td>
						</tr>
					</table> <!-- // FLEXIBLE CONTAINER -->
				</td>
			</tr>
		</table> <!-- // CENTERING TABLE -->
	</td>
</tr>
<!-- // MODULE ROW -->