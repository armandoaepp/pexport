<!-- MODULE ROW // -->
<tr>
	<td align="center" valign="top">
		<!-- CENTERING TABLE // -->
		<table border="0" cellpadding="0" cellspacing="0" width="100%"
			bgcolor="#F8F8F8">
			<tr>
				<td align="center" valign="top">
					<!-- FLEXIBLE CONTAINER // -->
					<table border="0" cellpadding="0" cellspacing="0" width="500"
						class="flexibleContainer">
						<tr>
							<td align="center" valign="top" width="500"
								class="flexibleContainerCell">
								<table border="0" cellpadding="30" cellspacing="0" width="100%">
									<tr>
										<td align="center" valign="top">
											<!-- CONTENT TABLE // -->
											<table border="0" cellpadding="0" cellspacing="0"
												width="100%">
												<tr>
													<td valign="top" class="textContent">
														<h3
															style="color: #5F5F5F; line-height: 125%; font-family: Helvetica, Arial, sans-serif; font-size: 20px; font-weight: normal; margin-top: 0; margin-bottom: 3px; text-align: left;">Reporte de env&iacute;o de lecturas a Distriluz.</h3>
														<div
															style="text-align: left; font-family: Helvetica, Arial, sans-serif; font-size: 15px; margin-bottom: 0; color: #5F5F5F; line-height: 135%;">
		<table style="font-size: 14px;">
		<thead>
			<tr>
				<th style="border:none; background-color:rgb(96, 192, 96); font-family:verdana; color:#fff; padding:6px;">Ciclo</th>
				<th style="border:none; background-color:rgb(96, 192, 96); font-family:verdana; color:#fff; padding:6px;"><span></span>Sector</th>
				<th style="border:none; background-color:rgb(96, 192, 96); font-family:verdana; color:#fff; padding:6px;" class='right'><span></span>Total Suministros</th>
				<th style="border:none; background-color:rgb(96, 192, 96); font-family:verdana; color:#fff; padding:6px;" class='right'><span></span>Enviados a Distriluz</th>
				<th style="border:none; background-color:rgb(96, 192, 96); font-family:verdana; color:#fff; padding:6px;" class='right'><span></span>Estado</th>
			</tr>
		</thead>
		<tbody class='no-border-x'>
			<?php
			$sum_suministros = 0;
			$sum_enviados_distriluz = 0;
			foreach ( $arr_ciclos as $k => $obj){
				?>
				<tr style="border:none; background-color:#eee; font-family:verdana; color:#222; padding:10px 5px; text-align:center;">
				<td><?php echo $obj[0]['idciclo'].' - '.$obj[0]['nombciclo'];?></td>
				<td><?php echo $obj[0]['sectores'];?></td>
				<td class='right'><?php echo number_format($obj[0]['suministros']);?></td>
				<td class='right'><span><?php echo number_format($obj[0]['enviados_distriluz']);?></span></td>
				<td>
				<?php if($obj[0]['suministros']==$obj[0]['enviados_distriluz']){?>
					<span class="badge badge-success" style="background-color: #60C060 !important;display: inline-block;min-width: 10px;line-height: 1;color: #fff;text-align: center;white-space: nowrap;vertical-align: baseline;background-color: #999;border-radius: 10px;font-weight: normal;font-size: 95%;padding: 4px 6px;">OK</span>
				<?php }else{?>
					<span class="badge badge-danger" style="background-color: #ED5B56 !important;display: inline-block;min-width: 10px;line-height: 1;color: #fff;text-align: center;white-space: nowrap;vertical-align: baseline;background-color: #999;border-radius: 10px;font-weight: normal;font-size: 95%;padding: 4px 6px;">PENDIENTE</span>
				<?php }?>
				</td>
			</tr>
				<?php
				$sum_suministros += $obj[0]['suministros'];
				$sum_enviados_distriluz += $obj[0]['enviados_distriluz'];
			} 
			?>		
		</tbody>
		<tfoot>
			<tr style="border:none; background-color:#eee; font-family:verdana; color:#222; padding:10px 5px; text-align:center;font-size: 16px;">
				<th colspan="2">Total</th>
				<th class='right'><?php echo number_format($sum_suministros);?></th>
				<th class='right'><span><?php echo number_format($sum_enviados_distriluz);?></span></th>
				<th>
				<?php if($sum_suministros==$sum_enviados_distriluz){?>
					<span class="badge badge-success" style="background-color: #60C060 !important;display: inline-block;min-width: 10px;line-height: 1;color: #fff;text-align: center;white-space: nowrap;vertical-align: baseline;background-color: #999;border-radius: 10px;font-weight: normal;font-size: 95%;padding: 4px 6px;">OK</span>
				<?php }else{?>
					<span class="badge badge-danger" style="background-color: #ED5B56 !important;display: inline-block;min-width: 10px;line-height: 1;color: #fff;text-align: center;white-space: nowrap;vertical-align: baseline;background-color: #999;border-radius: 10px;font-weight: normal;font-size: 95%;padding: 4px 6px;">PENDIENTE</span>
				<?php }?>
				</th>
			</tr>
		</tfoot>
	</table>
	<br>
	Lecturas programadas para: <b><?php echo date('Y-m-d');?></b> Env&iacute;o a Distriluz: <span class="badge badge-success" style="background-color: #60C060 !important;display: inline-block;min-width: 10px;line-height: 1;color: #fff;text-align: center;white-space: nowrap;vertical-align: baseline;background-color: #999;border-radius: 10px;font-weight: normal;font-size: 95%;padding: 4px 6px;">100% Completado</span>

					
														</div>
													</td>
												</tr>
											</table> <!-- // CONTENT TABLE -->

										</td>
									</tr>
								</table>
							</td>
						</tr>
					</table> <!-- // FLEXIBLE CONTAINER -->
				</td>
			</tr>
		</table> <!-- // CENTERING TABLE -->
	</td>
</tr>
<!-- // MODULE ROW -->