<?php
App::uses('AppHelper', 'View/Helper');

class ImageHelper extends AppHelper {
	public $helpers = array('Html');

	public function applyWatermark($image_base64, $fecha) {

		$data = base64_decode($image_base64);
		$str_marca = explode(' ',$fecha);

		$im = imagecreatefromstring($data);
		if ($im !== false) {
	    
			$color_texto = imagecolorallocate($im, 246, 246, 85);
			
			$alto_imagen = imagesy($im)-10;
			$ancho_imagen = imagesx($im)-40;
					    
		    // Escribir la cadena en la parte superior izquierda
			imagestring($im, 0, 3, $alto_imagen, $str_marca[0], $color_texto);
			imagestring($im, 0, $ancho_imagen, $alto_imagen, $str_marca[1], $color_texto);
		    
			ob_start ();
			
			//imagejpeg ($im);
			imagepng ($im);
			$image_data = ob_get_contents ();
			
			ob_end_clean ();
			
			$image_data_base64 = base64_encode ($image_data);
			echo $image_data_base64;
		}
		else {
		    echo 'Ocurrió un error.';
		}
	}
}