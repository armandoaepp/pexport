<?php
App::uses('AppHelper', 'View/Helper');

class TiempoHelper extends AppHelper {
	public $helpers = array('Html');

	function getTiempoString($oldTime, $newTime, $timeType){
		
		$tiempo_segundos = strtotime($newTime) - strtotime($oldTime);
		
		$str_dias = 0;
		$str_horas = 0;
		$str_minutos = 0;
		if($tiempo_segundos>36400){
			$str_dias = floor ($tiempo_segundos/36400);
			$tiempo_segundos = $tiempo_segundos%36400;
		}
		if($tiempo_segundos>3600){
			$str_horas = floor ($tiempo_segundos/3600);
			$tiempo_segundos = $tiempo_segundos%3600;
		}
		if($tiempo_segundos>60){
			$str_minutos = floor ($tiempo_segundos/60);
			$tiempo_segundos = floor ($tiempo_segundos%60);
		}
		
		$str_horas      = str_pad( $str_horas, 2, "0", STR_PAD_LEFT );
		$str_minutos    = str_pad( $str_minutos, 2, "0", STR_PAD_LEFT );
		$tiempo_segundos= str_pad( $tiempo_segundos, 2, "0", STR_PAD_LEFT );
		
		return ($str_dias>0?$str_dias.' días ':' ').($str_horas>0?$str_horas.' horas ':' ').($str_minutos>0?$str_minutos.' minutos ':' ').($tiempo_segundos>0?$tiempo_segundos.' segundos':' ');
	}
	
}