define(['ajax/inconsistencia','jquery.datatables','fancyapps','loadmask'],function(Inconsistencia){
	if(window.monitorear){ //Prevents defining the module twice
		return false;
	}
	console.log('INIT: Monitorear.js');
	var Monitorear = {

		init: function(){			
			this.initViewController();
			this.bindEvents();
			},
		
		bindEvents: function(){
			$body = $('body');
		},
		
		initViewController: function(){        
			Inconsistencia.ModalImagenInconsistencia();
			Monitorear.listMonitorearLecturistas();
			
		},
		
		listMonitorearLecturistas: function(){
			
			$("#loading-monitoreo").mask("Estamos buscando datos del Monitoreo de Lecturistas, cargando...");

			var oTable = $('#list-monitorear-lecturistas').dataTable({
				"paging": true,
				"bSort": false,
				"iDisplayLength" : 30,
				"aLengthMenu": [
								[30,50,100],
								[30,50,100]
								],
				"fnFooterCallback": function( nFoot, aData, iStart, iEnd, aiDisplay ){
					Monitorear.mi_fnFooterCallback( nFoot, aData, iStart, iEnd, aiDisplay );
				}
			});
			
			var preload_data = [
	            { id: 'F', text: 'Fecha de Asignación'}
	            , { id: 'U', text: 'Ubicación Geográfica'}
	          ];
           
            $('#cbo_criterio').select2({
                multiple: true
                ,query: function (query){
                    var data = {results: []};
           
                    $.each(preload_data, function(){
                        if(query.term.length == 0 || this.text.toUpperCase().indexOf(query.term.toUpperCase()) >= 0 ){
                            data.results.push({id: this.id, text: this.text });
                        }
                    });
           
                    query.callback(data);
                }
            });
            $('#cbo_criterio').select2('data', [{ id: 'F', text: 'Fecha de Asignación'}] )
            
            $('#cbo_criterio').on("change",function (e) { 
				console.log("change "+e.val);
				if(e.val.indexOf("F")>-1){
					$('.por_fecha_asignacion').show();					
				}else{
					$('.por_fecha_asignacion').hide();
				}
				if(e.val.indexOf("U")>-1){
					$('.por_ubicacion').show();
				}else{
					$('.por_ubicacion').hide();
				}
			});
           
            $('#unidadneg').select2({
            	width: '100%'
 	       	});
        	$('#unidadneg').on("change",function (e) { 
				console.log("change "+e.val);
				$('.ciclo_loading').show();
				$('#id_ciclo').select2("enable", false);
				unidad_neg = e.val || 0;
				$.ajax({
                                        url : ENV_WEBROOT_FULL_URL+"/ComlecOrdenlecturas/listarCicloPorUnidadneg/"+unidad_neg,
					type : "POST",
					dataType : 'html',
					success : function(data) {
						console.log(data);
						$('#id_ciclo').html(data);
						$("#id_ciclo").select2("val", "0");
						$('#id_ciclo').select2("enable", true);
						$('.ciclo_loading').hide();
					}
				});
			});  
            $('#id_ciclo').select2({
            	width: '100%'
 	       	});
        	$('#id_ciclo').on("change",function (e) { 
				console.log("change "+e.val);
				$('.sector_loading').show();
				$('#monitoreo_id_sector').select2("enable", false);
				id_ciclo = e.val || 0;
				$.ajax({
					url : ENV_WEBROOT_FULL_URL+"/ComlecOrdenlecturas/listarSectorPorCiclo/"+id_ciclo,
					type : "POST",
					dataType : 'html',
					success : function(data) {
						console.log(data);
						$('#monitoreo_id_sector').html(data);
						$("#monitoreo_id_sector").select2("val", "0");
						$('#monitoreo_id_sector').select2("enable", true);
						$('.sector_loading').hide();
					}
				});
			});
                        
                                   
                        
			$('#monitoreo_id_sector').select2({
            	width: '100%'
 	       	});

			$('body').on('click','#buscar-monitoreo',function(e){
				
				$(".div_content_monitor_reload").mask("Estamos buscando datos del Monitoreo de Lecturistas, cargando...");
				
				criterio = $('#cbo_criterio').val();
    	 		date_start = $('#dpt_date_start').val();
    	 		date_end = $('#dpt_date_end').val();
    	 		unidadneg = $('#unidadneg').val();
    	 		id_ciclo = $('#id_ciclo').val();
    	 		id_sector = $('#monitoreo_id_sector').val();
    	 		
				$('.div_content_monitor_reload').load(ENV_WEBROOT_FULL_URL+"MonitorearLecturas/ajax_monitorear_lectura_reload/"+criterio+"/"+date_start+"/"+date_end+"/"+unidadneg+"/"+id_ciclo+"/"+id_sector,function(){
					
					/*Jquery Easy Pie Chart*/
				      $('.epie-chart').easyPieChart({
				        barColor: '#FD9C35',
				        trackColor: '#EFEFEF',
				        lineWidth: 7,
				        animate: 600,
				        size: 55,
				        onStep: function(val){//Update current value while animation
				          $("span", this.$el).html(parseInt(val) + "%");
				        }
				      });
				      
				      var oTable = $('#list-monitorear-lecturistas').dataTable({
							"paging": true,
							"bSort": false,
							"iDisplayLength" : 30,
							"aLengthMenu": [
											[30,50,100],
											[30,50,100]
											],
							"fnFooterCallback": function( nFoot, aData, iStart, iEnd, aiDisplay ){
								Monitorear.mi_fnFooterCallback( nFoot, aData, iStart, iEnd, aiDisplay );
							}
						});


								   $(".arbol-head-lecturas").click(function(e){
							            if ( $('.widget-monitoreo').hasClass('visible')) {

							                $('.floatHeader').css('width','100%');
							                $('.floatHeader').css('margin-top','0px');
							                $('.cl-mcont').css('margin-top','120px');
							                $('.widget-monitoreo').css("visibility", "visible");
							                $('.widget-monitoreo').removeClass('visible');
							                $('.dato').css("visibility", "visible");
							                $('.widget-monitoreo').addClass('invisible');

							            }else{

							                $('.floatHeader').css('width','100%');
							                $('.floatHeader').css('margin-top','-80px');
							                $('.cl-mcont').css('margin-top','40px');
							                $('.dato').css("visibility", "visible");
							                $('.widget-monitoreo').removeClass('invisible');
							                $('.widget-monitoreo').addClass('visible');
							            } 
							            
							        });

								   if(navigator.appName.indexOf("MSIE")!=-1){
							        //console.log("ie11");
							      }else{ 
							        function UpdateTableHeaders3() {
							            //yeah, he's using IE
							                  
							            $("div.divTableWithFloatingHeader3").each(function() {
							                var originalHeaderRow = $(".tableFloatingHeaderOriginal", this);
							                var floatingHeaderRow = $(".tableFloatingHeader3", this);
							                var offset = $(this).offset();
							                var scrollTop = $(window).scrollTop();

							                var navegador = navigator.userAgent;
							                var inicioScroll = scrollTop+ 110;
							                var marginheader = 95;
							                if (navigator.userAgent.indexOf('MSIE') !=-1) {
							                 
							                } else if (navigator.userAgent.indexOf('Firefox') !=-1) {

							                  if (navigator.platform === "Android"){  
							                    inicioScroll = scrollTop + 110;
							                    marginheader = 95; 
							                  }
							                  if( $('.widget-monitoreo').hasClass('visible') ){
								                inicioScroll = scrollTop + 70 //30  --->-30 + 140
								                marginheader = 65; //55  -> 95
								              }
								              else { 
								                inicioScroll = scrollTop + 70; //30  --->-30 + 140
								                marginheader = 148; //55  -> 95
								              }


							                } else if (navigator.userAgent.indexOf('Chrome') !=-1) {

							                  if (navigator.platform === "Android"){  
							                    inicioScroll = scrollTop + 110;
							                     marginheader = 95; 
							                  }

							                  if( $('.widget-monitoreo').hasClass('visible') ){
							                    inicioScroll = scrollTop + 60 //30  --->-30 + 140
							                    marginheader = 100; //55  -> 95
							                  }
							                  else { 
							                    inicioScroll = scrollTop + 160; //30  --->-30 + 140
							                    marginheader = 200; //55  -> 95
							                  }


							                } else if (navigator.userAgent.indexOf('Opera') !=-1) {

							                  if (navigator.platform === "Android"){  
							                    inicioScroll = scrollTop + 110;
							                    marginheader = 95; 
							                  }else { 
							                    inicioScroll = scrollTop + 110;
							                    marginheader = 125;
							                  }

							                } else {
							                
							                }

							                if (( inicioScroll > offset.top ) && (scrollTop < offset.top + $(this).height())) {
							                    floatingHeaderRow.css("visibility", "visible");
							                    floatingHeaderRow.css("top", (Math.min(scrollTop - offset.top, $(this).height() - floatingHeaderRow.height()))+ marginheader  + "px");

							                    // Copy cell widths from original header
							                    $("th", floatingHeaderRow).each(function(index) {
							                        var cellWidth = $("th", originalHeaderRow).eq(index).css('width');
							                        $(this).css('width', cellWidth);
							                    });

							                    // Copy row width from whole table
							                    floatingHeaderRow.css("width", $(this).css("width"));
							                }
							                else {
							                    floatingHeaderRow.css("visibility", "hidden");
							                    floatingHeaderRow.css("top", "0px");
							                    floatingHeaderRow.css("background-color", "#F8F8F8");
							                }
							            });
							        }

							        $(document).ready(function() {

							                $("table.tableWithFloatingHeader3").each(function() {
							                  $(this).wrap("<div class=\"divTableWithFloatingHeader3\" style=\"position:relative\"></div>");

							                  var originalHeaderRow = $("tr:first", this)
							                  originalHeaderRow.before(originalHeaderRow.clone());
							                  var clonedHeaderRow = $("tr:first", this)

							                  clonedHeaderRow.addClass("tableFloatingHeader3");
							                  clonedHeaderRow.css("position", "absolute");
							                  clonedHeaderRow.css("top", "0px");
							                  clonedHeaderRow.css("left", $(this).css("margin-left"));
							                  clonedHeaderRow.css("visibility", "hidden");
							                  clonedHeaderRow.css("background-color", "#F8F8F8");

							                  originalHeaderRow.addClass("tableFloatingHeaderOriginal");
							                });

							              UpdateTableHeaders3();
							              $(window).scroll(UpdateTableHeaders3);
							              $(window).resize(UpdateTableHeaders3);
							            
							        });
							      }
				      
      			});
					/*
					criterio = $('#cbo_criterio').val();
	    	 		date_start = $('#dpt_date_start').val();
	    	 		date_end = $('#dpt_date_end').val();
	    	 		unidadneg = $('#unidadneg').val();
	    	 		id_ciclo = $('#id_ciclo').val();
	    	 		id_sector = $('#monitoreo_id_sector').val();
	        		
	        		if(oTable != null || typeof oTable != 'undefined') {
              			$('#list-monitorear-lecturistas').dataTable().fnDestroy();
	        		}

	        		$('#list-monitorear-lecturistas').dataTable({
						"bProcessing": true,
						"bDeferRender": true,
						"bServerSide": false,
						"paging": false,
						"bSort": false,
						"iDisplayLength" : 30,
						"sAjaxSource": ENV_WEBROOT_FULL_URL+"MonitorearLecturas/ajax_monitorear_lectura/"+criterio+"/"+date_start+"/"+date_end+"/"+unidadneg+"/"+id_ciclo+"/"+id_sector,
						"aLengthMenu": [
										[30,50,100],
										[30,50,100]
										],
						"fnDrawCallback" : fnEachTd,
						"fnFooterCallback": function( nFoot, aData, iStart, iEnd, aiDisplay ){
							Monitorear.mi_fnFooterCallback( nFoot, aData, iStart, iEnd, aiDisplay );
						}
					});
	        		
	        		$("#loading-monitoreo").mask("Estamos buscando datos del Monitoreo de Lecturistas, cargando...");

	        		function fnEachTd(oSettings){
		        		$('#list-monitorear-lecturistas tbody').find('tr').each(function(){     						
		        			$(this).children('td').eq(0).addClass('hidden');
		        			$(this).children('td').eq(1).addClass('hidden');
		        			$(this).children('td').eq(2).addClass('hidden');
		        			$(this).children('td').eq(3).addClass('hidden');
		        			$(this).children('td').eq(4).addClass('hidden');
		        			$(this).children('td').eq(5).addClass('hidden');
		        			$(this).children('td').eq(6).addClass('hidden');
	              		});
	        		}
	        		$('#list-monitorear-lecturistas').css( "width","100%" );*/
	        	}); 

			$('body').on('click','#btn_mapa_monitoreo',function(e){
				criterio = $('#cbo_criterio').val();
    	 		date_start = $('#dpt_date_start').val();
    	 		date_end = $('#dpt_date_end').val();
    	 		unidadneg = $('#unidadneg').val();
    	 		id_ciclo = $('#id_ciclo').val();
    	 		id_sector = $('#monitoreo_id_sector').val();
    	 		
    	 		window.open(ENV_WEBROOT_FULL_URL+"MonitorearLecturas/mapa_monitoreo/"+criterio+"/"+date_start+"/"+date_end+"/"+unidadneg+"/"+id_ciclo+"/"+id_sector,"_blank");
			});
			
			Monitorear.loadingBeforeSearchPaginationTable();
			
			Monitorear.onClickButtonPlayStopUpdate();
			
			setTimeout(function(){
				Monitorear.refreshListadoMonitoreo();					
			}, 60000);
		},
		
		mi_fnFooterCallback: function( nFoot, aData, iStart, iEnd, aiDisplay ){
			$(nFoot).html('');
			$(nFoot).append('<td colspan="2" style="text-align:right"><strong>Total:</strong></td>');
			
			var col = 9;
			var tot = 0;
			var tot_filter = 0;
			for ( var i = 0; i < aData.length; i++) {
				tot += aData[i][col] * 1;
			}
			for ( var i = iStart; i < iEnd; i++) {
				tot_filter += aData[aiDisplay[i]][col] * 1;
			}
			$(nFoot).append('<td><strong>' + tot_filter + ' (' + tot + '  total)</strong></td>');
			
			var col = 10;
			var tot = 0;
			var tot_filter = 0;
			for ( var i = 0; i < aData.length; i++) {
				tot += aData[i][col] * 1;
			}
			for ( var i = iStart; i < iEnd; i++) {
				tot_filter += aData[aiDisplay[i]][col] * 1;
			}
			$(nFoot).append('<td><strong>' + tot_filter + ' (' + tot + '  total)</strong></td>');
			
			$(nFoot).append('<td></td>');
			
			var col = 12;
			var tot = 0;
			var tot_filter = 0;
			for ( var i = 0; i < aData.length; i++) {
				tot += aData[i][col] * 1;
			}
			for ( var i = iStart; i < iEnd; i++) {
				tot_filter += aData[aiDisplay[i]][col] * 1;
			}
			$(nFoot).append('<td><strong>' + tot_filter + ' (' + tot + '  total)</strong></td>');
			
			var col = 13;
			var tot = 0;
			var tot_filter = 0;
			for ( var i = 0; i < aData.length; i++) {
				tot += aData[i][col] * 1;
			}
			for ( var i = iStart; i < iEnd; i++) {
				tot_filter += aData[aiDisplay[i]][col] * 1;
			}
			$(nFoot).append('<td><strong>' + tot_filter + ' (' + tot + '  total)</strong></td>');
			
			//$(nFoot).append('<td></td>');
			
			var col = 5;
			var tot = 0;
			var tot_filter = 0;
			for ( var i = 0; i < aData.length; i++) {
				tot += aData[i][col] * 1;
			}
			for ( var i = iStart; i < iEnd; i++) {
				tot_filter += aData[aiDisplay[i]][col] * 1;
			}
			str_total_terminadas_page = tot_filter;
			str_total_terminadas = tot;
			
			var col = 6;
			var tot = 0;
			var tot_filter = 0;
			for ( var i = 0; i < aData.length; i++) {
				tot += aData[i][col] * 1;
			}
			for ( var i = iStart; i < iEnd; i++) {
				tot_filter += aData[aiDisplay[i]][col] * 1;
			}
			str_total_asignadas_page = tot_filter;
			str_total_asignadas = tot;
			str_porcentaje_page = ((str_total_terminadas_page * 100) / str_total_asignadas_page).toFixed(2);
			str_porcentaje = ((str_total_terminadas * 100) / str_total_asignadas).toFixed(2);
			if(str_porcentaje_page>=50){
				str_style_porcentaje_page = 'progress-bar progress-bar-success';
			}else{
				str_style_porcentaje_page = 'progress-bar progress-bar-success';
			}
			str_progress_page = '<div class="progress progress-striped" title ="' + str_porcentaje_page + '% - ' + str_total_terminadas_page + ' Lecturas Terminadas.\"><div class="'+ str_style_porcentaje_page +'" style="width: '+ str_porcentaje_page +'% ; color: black; font-weight: bold;\">'+ str_porcentaje_page +'% ('+ str_total_terminadas_page +')</div></div>';
			
			if(str_porcentaje>=50){
				str_style_porcentaje = 'progress-bar progress-bar-success';
			}else{
				str_style_porcentaje = 'progress-bar progress-bar-success';
			}
			str_progress = '<div class="progress progress-striped" title ="' + str_porcentaje + '% - ' + str_total_terminadas + ' Lecturas Terminadas.\"><div class="'+ str_style_porcentaje +'" style="width: '+ str_porcentaje +'% ; color: black; font-weight: bold;\">'+ str_porcentaje +'% ('+ str_total_terminadas +')</div></div>';
			//$(nFoot).append('<td><div class="progress progress-striped" title ="' + str_porcentaje + '% - ' + str_total_terminadas + ' Lecturas Terminadas.\"><div class="'+ str_style_porcentaje +'" style="width: '+ str_porcentaje +'% ; color: black; font-weight: bold;\">'+ str_porcentaje +'% ('+ str_total_terminadas +')</div></div></td>');
			$(nFoot).append('<td>' + str_progress_page + '<br>' + str_progress + '</td>');
					
			var col = 3;
			var tot = 0;
			var tot_filter = 0;
			for ( var i = 0; i < aData.length; i++) {
				tot += aData[i][col] * 1;
			}
			for ( var i = iStart; i < iEnd; i++) {
				tot_filter += aData[aiDisplay[i]][col] * 1;
			}
			tot_str1 = '<strong> Originales: ' + tot_filter + ' (' + tot + '  total)</strong>';
			
			var col = 4;
			var tot = 0;
			var tot_filter = 0;
			for ( var i = 0; i < aData.length; i++) {
				tot += aData[i][col] * 1;
			}
			for ( var i = iStart; i < iEnd; i++) {
				tot_filter += aData[aiDisplay[i]][col] * 1;
			}
			tot_str2 = '<strong> Actuales: ' + tot_filter + ' (' + tot + '  total)</strong>';
			$(nFoot).append('<td>' + tot_str1 + '<br>' + tot_str2 + '</td>');
			
			var col = 16;
			var tot = 0;
			var tot_filter = 0;
			for ( var i = 0; i < aData.length; i++) {
				tot += aData[i][col] * 1;
			}
			for ( var i = iStart; i < iEnd; i++) {
				tot_filter += aData[aiDisplay[i]][col] * 1;
			}
			$(nFoot).append('<td><strong>' + tot_filter + ' (' + tot + '  total)</strong></td>');
			
			var col = 17;
			var tot = 0;
			var tot_filter = 0;
			for ( var i = 0; i < aData.length; i++) {
				tot += aData[i][col] * 1;
			}
			for ( var i = iStart; i < iEnd; i++) {
				tot_filter += aData[aiDisplay[i]][col] * 1;
			}
			$(nFoot).append('<td><strong>' + tot_filter + ' (' + tot + '  total)</strong></td>');
			
			var col = 0;
			var tot = 0;
			var tot_filter = 0;
			for ( var i = 0; i < aData.length; i++) {
				tot += aData[i][col] * 1;
			}
			for ( var i = iStart; i < iEnd; i++) {
				tot_filter += aData[aiDisplay[i]][col] * 1;
			}
			tot_str1 = '<strong> T: ' + tot_filter + ' (' + tot + '  total)</strong>';
			
			var col = 1;
			var tot = 0;
			var tot_filter = 0;
			for ( var i = 0; i < aData.length; i++) {
				tot += aData[i][col] * 1;
			}
			for ( var i = iStart; i < iEnd; i++) {
				tot_filter += aData[aiDisplay[i]][col] * 1;
			}
			tot_str2 = '<strong> SL: ' + tot_filter + ' (' + tot + '  total)</strong>';
			
			var col = 2;
			var tot = 0;
			var tot_filter = 0;
			for ( var i = 0; i < aData.length; i++) {
				tot += aData[i][col] * 1;
			}
			for ( var i = iStart; i < iEnd; i++) {
				tot_filter += aData[aiDisplay[i]][col] * 1;
			}
			tot_str3 = '<strong> CL: ' + tot_filter + ' (' + tot + '  total)</strong>';
			$(nFoot).append('<td colspan="2">' + tot_str1 + '<br>' + tot_str2 + '<br>' + tot_str3 + '</td>');
			
			//$(nFoot).append('<td></td>');
			
			$("#loading-monitoreo").unmask();
		},
		
		loadingBeforeSearchPaginationTable: function(){
			$body = $('body');
			$body.off('click','.pagination a');
			$body.on('click','.pagination a',function(){	
				$("#loading-monitoreo").mask("Estamos buscando datos del Monitoreo de Lecturistas, cargando...");
			});
		},
		
		refreshListadoMonitoreo: function(){
			setInterval(function(){
				if($('.btn_play_stop_update_monitoreo').data('action')=='play'){
				$.ajax({
					url : ENV_WEBROOT_FULL_URL+'ComlecOrdenlecturas/get_last_event_update_lectura',
					type: "POST",
					dataType: 'json',
					success:function(data, textStatus, jqXHR){
						console.log(data);
						if(data.modified==true){
							console.log('ok');
							$('.label_last_update').html('Actualizando...');
							//$('#buscar-monitoreo').click();
							//div_content_monitor_reload
							criterio = $('#cbo_criterio').val();
			    	 		date_start = $('#dpt_date_start').val();
			    	 		date_end = $('#dpt_date_end').val();
			    	 		unidadneg = $('#unidadneg').val();
			    	 		id_ciclo = $('#id_ciclo').val();
			    	 		id_sector = $('#monitoreo_id_sector').val();
			    	 		
							$('.div_content_monitor_reload').load(ENV_WEBROOT_FULL_URL+"MonitorearLecturas/ajax_monitorear_lectura_reload/"+criterio+"/"+date_start+"/"+date_end+"/"+unidadneg+"/"+id_ciclo+"/"+id_sector,function(){
								if(data.str_tiempo!=''){
									$('.label_last_update').html('hace '+data.str_tiempo);	
								}else{
									$('.label_last_update').html('hace instates.');
								}
								

								/*Jquery Easy Pie Chart*/
							      $('.epie-chart').easyPieChart({
							        barColor: '#FD9C35',
							        trackColor: '#EFEFEF',
							        lineWidth: 7,
							        animate: 600,
							        size: 55,
							        onStep: function(val){//Update current value while animation
							          $("span", this.$el).html(parseInt(val) + "%");
							        }
							      });
							      
							      Monitorear.onClickButtonPlayStopUpdate();
							      
							      var oTable = $('#list-monitorear-lecturistas').dataTable({
										"paging": true,
										"bSort": false,
										"iDisplayLength" : 30,
										"aLengthMenu": [
														[30,50,100],
														[30,50,100]
														],
										"fnFooterCallback": function( nFoot, aData, iStart, iEnd, aiDisplay ){
											Monitorear.mi_fnFooterCallback( nFoot, aData, iStart, iEnd, aiDisplay );
										}
									});


								   $(".arbol-head-lecturas").click(function(e){
							            if ( $('.widget-monitoreo').hasClass('visible')) {

							                $('.floatHeader').css('width','100%');
							                $('.floatHeader').css('margin-top','0px');
							                $('.cl-mcont').css('margin-top','120px');
							                $('.dato').css("visibility", "visible");
							                $('.widget-monitoreo').css("visibility", "visible");
							                $('.widget-monitoreo').removeClass('visible');
							                $('.widget-monitoreo').addClass('invisible');

							            }else{

							                $('.floatHeader').css('width','100%');
							                $('.floatHeader').css('margin-top','-80px');
							                $('.cl-mcont').css('margin-top','40px');
							                $('.dato').css("visibility", "visible");
							                $('.widget-monitoreo').removeClass('invisible');
							                $('.widget-monitoreo').addClass('visible');
							            } 
							            
							        });

								   if(navigator.appName.indexOf("MSIE")!=-1){
							        //console.log("ie11");
							      }else{ 
							        function UpdateTableHeaders3() {
							            //yeah, he's using IE
							                  
							            $("div.divTableWithFloatingHeader3").each(function() {
							                var originalHeaderRow = $(".tableFloatingHeaderOriginal", this);
							                var floatingHeaderRow = $(".tableFloatingHeader3", this);
							                var offset = $(this).offset();
							                var scrollTop = $(window).scrollTop();

							                var navegador = navigator.userAgent;
							                var inicioScroll = scrollTop+ 110;
							                var marginheader = 95;
							                if (navigator.userAgent.indexOf('MSIE') !=-1) {
							                 
							                } else if (navigator.userAgent.indexOf('Firefox') !=-1) {

							                  if (navigator.platform === "Android"){  
							                    inicioScroll = scrollTop + 110;
							                    marginheader = 95; 
							                  }
							                  if( $('.widget-monitoreo').hasClass('visible') ){
							                    inicioScroll = scrollTop + 70 //30  --->-30 + 140
							                    marginheader = 65; //55  -> 95
							                  }
							                  else { 
							                    inicioScroll = scrollTop + 70; //30  --->-30 + 140
							                    marginheader = 148; //55  -> 95
							                  }


							                } else if (navigator.userAgent.indexOf('Chrome') !=-1) {

							                  if (navigator.platform === "Android"){  
							                    inicioScroll = scrollTop + 110;
							                     marginheader = 95; 
							                  }

							                  if( $('.widget-monitoreo').hasClass('visible') ){
							                    inicioScroll = scrollTop + 60 //30  --->-30 + 140
							                    marginheader = 100; //55  -> 95
							                  }
							                  else { 
							                    inicioScroll = scrollTop + 160; //30  --->-30 + 140
							                    marginheader = 200; //55  -> 95
							                  }


							                } else if (navigator.userAgent.indexOf('Opera') !=-1) {

							                  if (navigator.platform === "Android"){  
							                    inicioScroll = scrollTop + 110;
							                    marginheader = 95; 
							                  }else { 
							                    inicioScroll = scrollTop + 110;
							                    marginheader = 125;
							                  }

							                } else {
							                
							                }

							                if (( inicioScroll > offset.top ) && (scrollTop < offset.top + $(this).height())) {
							                    floatingHeaderRow.css("visibility", "visible");
							                    floatingHeaderRow.css("top", (Math.min(scrollTop - offset.top, $(this).height() - floatingHeaderRow.height()))+ marginheader  + "px");

							                    // Copy cell widths from original header
							                    $("th", floatingHeaderRow).each(function(index) {
							                        var cellWidth = $("th", originalHeaderRow).eq(index).css('width');
							                        $(this).css('width', cellWidth);
							                    });

							                    // Copy row width from whole table
							                    floatingHeaderRow.css("width", $(this).css("width"));
							                }
							                else {
							                    floatingHeaderRow.css("visibility", "hidden");
							                    floatingHeaderRow.css("top", "0px");
							                    floatingHeaderRow.css("background-color", "#F8F8F8");
							                }
							            });
							        }

							        $(document).ready(function() {

							                $("table.tableWithFloatingHeader3").each(function() {
							                  $(this).wrap("<div class=\"divTableWithFloatingHeader3\" style=\"position:relative\"></div>");

							                  var originalHeaderRow = $("tr:first", this)
							                  originalHeaderRow.before(originalHeaderRow.clone());
							                  var clonedHeaderRow = $("tr:first", this)

							                  clonedHeaderRow.addClass("tableFloatingHeader3");
							                  clonedHeaderRow.css("position", "absolute");
							                  clonedHeaderRow.css("top", "0px");
							                  clonedHeaderRow.css("left", $(this).css("margin-left"));
							                  clonedHeaderRow.css("visibility", "hidden");
							                  clonedHeaderRow.css("background-color", "#F8F8F8");

							                  originalHeaderRow.addClass("tableFloatingHeaderOriginal");
							                });

							              UpdateTableHeaders3();
							              $(window).scroll(UpdateTableHeaders3);
							              $(window).resize(UpdateTableHeaders3);
							            
							        });
							      }
							      
			      			});
						}else{
							if(data.str_tiempo==''){
								$('.label_last_update').html('hace instantes.');
							}else{
								$('.label_last_update').html('hace '+data.str_tiempo);
							}
						}
					}
				});
				}
			}, 45000);
		},
		
		onClickButtonPlayStopUpdate: function (){
			$('.btn_play_stop_update_monitoreo').off('click');
			$('.btn_play_stop_update_monitoreo').on('click',function(){
				if($(this).data('action')=='play'){
					$('i', $(this)).removeClass('fa-stop');
					$('i', $(this)).addClass('fa-play');
					$(this).data('action','stop');
					$(this).attr('title','Iniciar actualizacion automatica.');
				}else{
					$('i', $(this)).removeClass('fa-play');
					$('i', $(this)).addClass('fa-stop');
					$(this).data('action','play');
					$(this).attr('title','Detener actualizacion automatica.');
				}
			});
		}
	};
	
	Monitorear.init();

return Monitorear;

});  