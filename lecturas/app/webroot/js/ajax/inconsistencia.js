define(['jquery.datatables','fancyapps','loadmask'],function(){
	if(window.Inconsistencia){ //Prevents defining the module twice
		return false;
	}
	console.log('INIT: Inconsistencia.js');
	var Inconsistencia = {

		init: function(){			
			this.initViewController();
			this.bindEvents();
			},
		
		bindEvents: function(){
			$body = $('body');
		},
		
		initViewController: function(){
        
			Inconsistencia.listarInconsistencias();
			Inconsistencia.historicoSuministro();
			Inconsistencia.auditoriaSuministro();
			Inconsistencia.verFormula();
			Inconsistencia.generarReLectura();
			Inconsistencia.finalizarReLectura();
			Inconsistencia.editarLecturaEnviadaDistriluz();
			Inconsistencia.tabIndex2(); // En Porceso de evaluacion
			Inconsistencia.clicpass();
			Inconsistencia.ModalImagenInconsistencia();
			Inconsistencia.colorSelectInconsistencia();
			Inconsistencia.exportarPdfInconsistencia();
			Inconsistencia.cambiarCodigoLectura();
			Inconsistencia.BuscarInconsistenciaIntranet();
			Inconsistencia.analizarInconsistenciaLectura();
			Inconsistencia.loadingBeforeSearchPaginationTable();
		},
		
		analizarInconsistenciaLectura : function(){
			$('body').off('click','#analizar-inconsistencia-lectura');
          	$('body').on('click','#analizar-inconsistencia-lectura',function(e){
				e.stopImmediatePropagation();
				e.preventDefault();
				sector = $("#monitoreo_id_sector").val();
				
				$.ajax({
    				url: ENV_WEBROOT_FULL_URL+"EvaluarInconsistencias/analizar_inconsistencia_lectura/",
    				type: 'post',
    				data: {'analizar':sector},
    				dataType: 'html'
    			}).done(function(data){
    				data;
    			});
								
          	});
		},
		
		BuscarInconsistenciaIntranet : function() {
			

			$('body').off('click','#intranet-inconsistencia-lectura');
          	$('body').on('click','#intranet-inconsistencia-lectura',function(e){
				e.stopImmediatePropagation();
				e.preventDefault();
          		
          		sector = $("#id_sector").val();                     		
          		lecturista = $("#id_lecturista").val();
          		search_suministro = $( "#txt_search_by_suministro" ).val();
				
				if(search_suministro == ''){
				search_suministro = '0';
				}else{
				search_suministro = $( "#txt_search_by_suministro" ).val()
				}
          		inconsistenciaforzada = $("#inconsistenciaforzada").val();
          		inconsistenciacodigo = $("#inconsistenciacodigo").val();
          		
          		if(inconsistenciacodigo == true){
          			inconsistenciaconcodigo = true;
          			inconsistenciasincodigo = false;
          		}else{
          			inconsistenciasincodigo = true;
          			inconsistenciaconcodigo = false;
          		}
          		inconsistenciadesde = $("#inconsistenciadesde").val();
          		inconsistenciahasta = $("#inconsistenciahasta").val();
          		
				$('#tabla-inconsistencias-extranet').dataTable({
					"bProcessing": true,
					"bDeferRender": true,
					"bDestroy": true,
					"bServerSide": true,
					"paging": false,
					"bSort": false,
					"iDisplayLength" : 10,							
					"sAjaxSource": ENV_WEBROOT_FULL_URL+"EvaluarInconsistencias/buscar_inconsistencias_extranet/"+sector+"/"+search_suministro+"/"+lecturista,
					"aLengthMenu": [
									[10,25, 40],
									[10,25, 40]
									]
					//"fnDrawCallback" : fnEachTd
				});
		        $('#tabla-inconsistencias-extranet').css( "width","20%" );                 		 
		        $('#tabla-inconsistencias-extranet').css( "font-size","2px" );                 		 
          	/*	function fnEachTd ( oSettings ){                      			
              		$('#tabla-inconsistencias tbody').find('tr').each(function(){     						
              		    var td1 = $(this).children('td').eq(1);
              		    td1.addClass('hidden');
						
						 var td10 = $(this).children('td').eq(10);
              		    td10.addClass('hidden');
						
						var td11 = $(this).children('td').eq(11);
              		    td11.addClass('hidden');
              		});

              		$('#tabla-inconsistencias').css( "width","120%" );
              		$('.dataTable thead th').eq(0).css( "width","2%" );
				 	
          		}*/
          	});
                      	

			$('.md-trigger').modalEffects();

			$('.dataTables_filter input').addClass('form-control').attr('placeholder', 'Algo que buscar?');
			$('.dataTables_length select').addClass('form-control');
			
		},
		
		listarInconsistencias : function() {
			
			var preload_data_inconsistencia = [
			    	            { id: 'F', text: 'Fecha de Asignación'}
			    	            , { id: 'U', text: 'Ubicación Geográfica'}
			    	            , { id: 'L', text: 'Lecturista'}
			    	            , { id: 'S', text: 'Suministro'}
			    	          ];
			               
			                $('#cbo_criterio_evaluacion').select2({
			                    multiple: true
			                    ,query: function (query){
			                        var data = {results: []};
			               
			                        $.each(preload_data_inconsistencia, function(){
			                            if(query.term.length == 0 || this.text.toUpperCase().indexOf(query.term.toUpperCase()) >= 0 ){
			                                data.results.push({id: this.id, text: this.text });
			                            }
			                        });
			               
			                        query.callback(data);
			                    }
			                });
			                $('#cbo_criterio_evaluacion').select2('data', [{ id: 'L', text: 'Lecturista'}] )
			                
			                $('#cbo_criterio_evaluacion').on("change",function (e) { 
			    				console.log("change "+e.val);
			    				if(e.val.indexOf("F")>-1){
			    					$('.por_fecha_asignacion').show();					
			    				}else{
			    					$('.por_fecha_asignacion').hide();
			    				}
			    				if(e.val.indexOf("U")>-1){
			    					$('.por_ubicacion').show();
			    				}else{
			    					$('.por_ubicacion').hide();
			    				}
			    				if(e.val.indexOf("L")>-1){
			    					$('.por_lecturista').show();
			    				}else{
			    					$('.por_lecturista').hide();
			    				}
			    				if(e.val.indexOf("S")>-1){
			    					$('.por_suministro').show();
			    				}else{
			    					$('.por_suministro').hide();
			    				}
			    			});
			               
			                $('#unidadneg').select2({
			                	width: '100%'
			     	       	});
			            	$('#unidadneg').on("change",function (e) { 
			    				console.log("change "+e.val);
			    				$('.ciclo_loading').show();
			    				$('#id_ciclo').select2("enable", false);
			    				unidad_neg = e.val || 0;
			    				$.ajax({
			                                            url : ENV_WEBROOT_FULL_URL+"/ComlecOrdenlecturas/listarCicloPorUnidadneg/"+unidad_neg,
			    					type : "POST",
			    					dataType : 'html',
			    					success : function(data) {
			    						console.log(data);
			    						$('#id_ciclo').html(data);
			    						$("#id_ciclo").select2("val", "0");
			    						$('#id_ciclo').select2("enable", true);
			    						$('.ciclo_loading').hide();
			    					}
			    				});
			    			});  
			                $('#id_ciclo').select2({
			                	width: '100%'
			     	       	});
			            	$('#id_ciclo').on("change",function (e) { 
			    				console.log("change "+e.val);
			    				$('.sector_loading').show();
			    				$('#monitoreo_id_sector').select2("enable", false);
			    				id_ciclo = e.val || 0;
			    				$.ajax({
			    					url : ENV_WEBROOT_FULL_URL+"/ComlecOrdenlecturas/listarSectorPorCiclo/"+id_ciclo,
			    					type : "POST",
			    					dataType : 'html',
			    					success : function(data) {
			    						console.log(data);
			    						$('#monitoreo_id_sector').html(data);
			    						$("#monitoreo_id_sector").select2("val", "0");
			    						$("#monitoreo_id_sector").change();
			    						$('#monitoreo_id_sector').select2("enable", true);
			    						$('.sector_loading').hide();
			    					}
			    				});
			    			});
			                            
			                                       
			    			$('#monitoreo_id_sector').select2({
			                	width: '100%'
			     	       	});
			    			
			            	$('#monitoreo_id_sector').on("change",function (e) { 
			    				console.log("change "+e.val);
			    				if ( $( "#id_ruta" ).length ) {
				    				$('.ruta_loading').show();
				    				$('#id_ruta').select2("enable", false);
				    				id_sector = e.val || 0;
				    				$.ajax({
				    					url : ENV_WEBROOT_FULL_URL+"/ComlecOrdenlecturas/listarRutasPorSector/"+id_sector,
				    					type : "POST",
				    					dataType : 'html',
				    					success : function(data) {
				    						console.log(data);
				    						$('#id_ruta').html(data);
				    						$("#id_ruta").select2("val", "0");
				    						$('#id_ruta').select2("enable", true);
				    						$('.ruta_loading').hide();
				    					}
				    				});
			    				}
			    			});

			    			$('#id_ruta').select2({
			                	width: '100%'
			     	       	});
			
			var preload_data = [
	            { id: 'LSC', text: 'Pendiente de Lectura'}
	            , { id: 'OBST', text: 'Lecturas con Observaciones'}
	            , { id: 'OBSSL', text: 'Observaciones Sin Lectura'}
	            , { id: 'OBSCL', text: 'Observaciones Con Lectura'}
	            , { id: 'OBS99', text: 'Observaciones 99'}
	            , { id: 'LAMA', text: 'Lectura Actual Menor que la Anterior'}
	            , { id: 'CFR', text: 'Consumo Fuera de Rango'}
	            //, { id: 'NS', text: 'Nuevo Suministro'}
	            , { id: 'OBSSC', text: 'Con Observacion sin corregir' }
	            , { id: 'CORR', text: 'Corregidos' }
	            , { id: 'I', text: 'Inconsistentes' }
	            , { id: 'CONS', text: 'Consistentes' }
	            , { id: 'CC', text: 'Consumo cero' }
	            , { id: 'C50MA', text: 'Consumo 50% menor que el anterior' }
	            , { id: 'C100MA', text: 'Consumo 100% mayor que el anterior' }
	            , { id: 'CN', text: 'Clientes Nuevos' }
	            , { id: 'PED', text: 'Pendientes de Envio a Distriluz' }
	          ];
           
            $('#filter_advance').select2({
                multiple: true
                ,query: function (query){
                    var data = {results: []};
           
                    $.each(preload_data, function(){
                        if(query.term.length == 0 || this.text.toUpperCase().indexOf(query.term.toUpperCase()) >= 0 ){
                            data.results.push({id: this.id, text: this.text });
                        }
                    });
           
                    query.callback(data);
                }
            });
            //$('#filter_advance').select2('data', preload_data )
            
          	var oTable = $('#tabla-inconsistencias').dataTable();
			$('body').off('click','#buscar-inconsistencia-lectura');
          	$('body').on('click','#buscar-inconsistencia-lectura',function(e){

				$("#loading-inconsistencias").mask("Estamos buscando Suministros con Lecturas Inconsistentes, cargando...");

				e.stopImmediatePropagation();
				e.preventDefault();
				
				criterio = $('#cbo_criterio_evaluacion').val();
				if($.trim(criterio) == ''){
					criterio = '0';
				}
    	 		date_start = $('#dpt_date_start').val();
    	 		date_end = $('#dpt_date_end').val();
    	 		unidadneg = $('#unidadneg').val();
    	 		id_ciclo = $('#id_ciclo').val();
    	 		id_sector = $('#monitoreo_id_sector').val();
                   		
          		lecturista = $("#id_lecturista").val();
          		search_suministro = $( "#txt_search_by_suministro" ).val();
				
				if(search_suministro == ''){
				search_suministro = '0';
				}else{
				search_suministro = $( "#txt_search_by_suministro" ).val()
				}
				
				consumo_min = $( "#txt_consumo_min" ).val();
				if($.trim(consumo_min) == ''){
					consumo_min = '0';
				}
				consumo_max = $( "#txt_consumo_max" ).val();
				if($.trim(consumo_max) == ''){
					consumo_max = '0';
				}
				id_xml = $( "#txt_id_xml" ).val();
				if($.trim(id_xml) == ''){
					id_xml = '0';
				}
				
				filter_advance = $('#filter_advance').val();
				if($.trim(filter_advance) == ''){
					filter_advance = '0';
				}
          		
          		if(oTable != null || typeof oTable != 'undefined') {
					$('#tabla-inconsistencias').dataTable().fnDestroy();
				}
          				     
				$('#tabla-inconsistencias').dataTable({
					"bProcessing": true,
					"bDeferRender": true,
					"bServerSide": true,
					"paging": false,
					"bSort": false,
					"iDisplayLength" : 50,							
					"sAjaxSource": ENV_WEBROOT_FULL_URL+"EvaluarInconsistencias/evaluar_inconsistenciaintro/"+criterio+"/"+date_start+"/"+date_end+"/"+unidadneg+"/"+id_ciclo+"/"+id_sector+"/"+search_suministro+"/"+lecturista+"/"+consumo_min+"/"+consumo_max+"/"+filter_advance+"/"+id_xml,
					"aLengthMenu": [
									[50,100, 200],
									[50,100, 200]
									],
					"fnDrawCallback" : fnEachTd,
					"sDom": '<"top"p>T<"clear"><"datatables_filter"lrf><"bottom">ti',
		         	"oTableTools": {
		         		"sSwfPath":  ENV_WEBROOT_FULL_URL+"js/jquery.datatables/extras/TableTools/media/swf/copy_csv_xls_pdf.swf"
					}
				});
				function fnEachTd(oSettings){
	        		$('#tabla-inconsistencias tbody').find('tr').each(function(){     						
              		    var td1 = $(this).children('td').eq(0);
              		    td1.addClass('hidden');

              		});	
	        		$('.btn-tooltip').tooltip();
	        		$('[data-popover="popover"]').popover({
	        	        html : true});
	        		
	        		$('.fancybox-inconsistencia').fancybox({
	    			    helpers:  {
	    			        overlay : null
	    			    }
	    			});
	        		
	        	$("#loading-inconsistencias").unmask();
	        	$("#tabla-inconsistencias").unmask();
        		}
		        $('#tabla-inconsistencias').css( "width","100%" ); 
          	});
                      	

			$('.md-trigger').modalEffects();

			$('.dataTables_filter input').addClass('form-control').attr('placeholder', 'Algo que buscar?');
			$('.dataTables_length select').addClass('form-control');
			
		},

		graficoHistoricoInconsistencias : function(){
			
		    var pageviews = [
				             [1, 1],
				             [2, 2],
				             [3, 3],
				             [4, 4],
				             [5,5],
				             [23, 6]
				             ];
             var visitors = [
				             [1,7],
				             [2, 8],
				             [3, 9],
				             [4, 10],
				             [5, 11],
				             [30, 12]
				             ];

             if ($('#site_statistics').size() != 0) {
               $('#site_statistics_loading').hide();
               $('#site_statistics_content').show();

               var plot_statistics2 = $.plot($("#site_statistics2"), [{
                 data: pageviews,
                 label: "Unique Visits"
               }, {
                 data: visitors,
                 label: "Page Views"
               }
               ], {
                 series: {
                   bars: {
                     show: true,
                     barWidth: 0.7,
                     lineWidth: 1,
                     fill: true,
                     hoverable: true,
                     fillColor: {
                       colors: [{
                         opacity: 0.85
                       }, {
                         opacity: 0.85
                       }
                       ]
                     } 
                   },
                   shadowSize: 2
                 },
                 legend:{
                   show: false
                 },
                 grid: {
                 labelMargin: 10,
                    axisMargin: 500,
                   hoverable: true,
                   clickable: true,
                   tickColor: "rgba(255,255,255,0.22)",
                   borderWidth: 0
                 },
                 colors: ["#e67653", "#FFFFFF", "#52e136"],
                 xaxis: {
                   ticks: 11,
                   tickDecimals: 0
                 },
                 yaxis: {
                   ticks: 6,
                   tickDecimals: 0
                 }
               });
               
             }
		},
		
		 
		colorSelectInconsistencia : function(){
				$body = $('body');
				$body.off('click','.fancybox-inconsistencia');
				$body.on('click','.fancybox-inconsistencia',function(){
					var actn = $(this);
					actn.parents('tr:first').css({background:'#d6eaf3'});
					actn.parents('tr:first').css({color:'#000000'});

				});

		},
		
		historicoSuministro : function(){
				$body = $('body');
				$body.off('click','#btn-lectura-inconsistencia');
				$body.on('click','#btn-lectura-inconsistencia',function(){
					var actn = $(this);
					$('#tabla-inconsistencias tr').css({background:''});
					actn.parents('tr:first').css({background:'#d6eaf3'});
					
					var suministro = $(this).html();
					//$(this).addClass("active");
					var modulo = 'inconsistencia';
					$('.dialog_loading').show();
					$("#dialog-lecturas-inconsistencias" ).dialog({ minWidth: 1000, title: "Historico por Suministro" });
					$('#inconsistencia-suministro').html('');
					$('#inconsistencia-suministro').load(ENV_WEBROOT_FULL_URL+"EvaluarInconsistencias/inconsistencia_historico_por_lectura/"+suministro+"/"+modulo, function(){
					$('.dialog_loading').hide();	
					});
					
				});
		},
		
		auditoriaSuministro : function(){
			$body = $('body');
			$body.off('click','#btn-auditoria-suministro');
			$body.on('click','#btn-auditoria-suministro',function(){
				$('#tabla-inconsistencias tr').css({background:''});
				$(this).parents('tr:first').css({background:'#d6eaf3'});
				
				var suministro = $(this).attr('val_suministro');
				var modulo = 'inconsistencia';
				
				$('.dialog_loading').show();
				$("#dialog-lecturas-inconsistencias" ).dialog({ minWidth: 1000, title: "Auditoria por Suministro" });
				$('#inconsistencia-suministro').html('');
				$('#inconsistencia-suministro').load(ENV_WEBROOT_FULL_URL+"EvaluarInconsistencias/inconsistencia_auditoria_por_lectura/"+suministro+"/"+modulo, function(){
				$('.dialog_loading').hide();	
				});
			});
		},
		
		verFormula : function(){
			$body = $('body');
			$body.off('click','#btn-formula-suministro');
			$body.on('click','#btn-formula-suministro',function(){
				$('#tabla-inconsistencias tr').css({background:''});
				$(this).parents('tr:first').css({background:'#d6eaf3'});
				
				var suministro = $(this).data('suministro');
				
				$('.dialog_loading').show();
				$("#dialog-lecturas-inconsistencias" ).dialog({ minWidth: 1000, title: "Metodos de Consistencia" });
				$('#inconsistencia-suministro').html('');
				$('#inconsistencia-suministro').load(ENV_WEBROOT_FULL_URL+"EvaluarInconsistencias/show_formulas_consistencia/"+suministro, function(){
				$('.dialog_loading').hide();	
				});
			});
		},
		
		generarReLectura : function(){
			setTimeout(function(){					
				$body = $('body');
				$body.off('click','.btn-re-lectura-inconsistencia');
				$body.on('click','.btn-re-lectura-inconsistencia',function(){

					if(confirm('¿Esta seguro de marcar como relectura?')){
					
						var suministro = $(this).attr('val_suministro');
	                    var actn = $(this);
						//$(this).addClass("active");
	                    $.ajax({
	        				url: ENV_WEBROOT_FULL_URL+"EvaluarInconsistencias/evalar_relectura/",
	        				type: 'post',
	        				data: {'suministro':suministro},
	        				dataType: 'html'
	        			}).done(function(data){
	        				actn.parents('tr:first').css({background:'#FF8D8D'});
	    					actn.attr("disabled", true);
	    					$('.btn-finalizar-lectura-inconsistencia',actn.parents('tr:first')).attr("disabled", true);
	    					$('input',actn.parents('tr:first')).attr("disabled", true);
	                        var imagen = "<img title= 'INCONSISTENTE: Relectura, por favor ir al modulo de digitaci&oacute;n' src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAABpUlEQVR42q2TyytEURzH/QFWRCFTVhJF2WFla0XJwkKxlJ2VRxI1lA2RlRRWnqVGiRqPmJJXYqKGPEOGYdIwg/Nxzpm55ozByq1v3c6538fve85NSPjvhzcfnPTBYQ3slyP2ymC3Ajx21N7fZN8S4qgKAkO8M4N4HQBvB9w26nXhzIabOX4lc1qHn3WCjMFHJ7w0S4EGvc5JtRYJztviRXRsGTeAU5KH+KALQh0xAsJTiXCXadyN22S6h6iImlk89krX4bCz5f7YKN3qowK6j1IeHLncu+yGgCopNBEmWVBkI75KqMgKgeVCPCMlpkBppLCGWFzXaugC3WF3tovxL+aw05NmCMhF3XaE8AVVXGR2y53NQj1CrIDakEelo5pQsY3o2kgKnI3a2OovigqIg/boxyYsokH2ObJw96fgcXQaAvJI1FxWy3HECPllKY/j4XRW2lIJPXu/3YWLaZ5mM+IcLShnRV5rTcS7O/3zbRSnk5yPpOk0odV87ajeL8czdWxnU+rvZOt5f77haqGZvcECNuzJuNqTcHUX4J5qIei/5d//3k/ScyAkq+CgnAAAAABJRU5ErkJggg=='>"	                                        

	    					$('.lbl_resultado_evaluacion',actn.parents('tr:first')).html(imagen);
	        			});
					}
                    
				
				});
			}, 1000);	
		},
		
		
		
		finalizarReLectura : function(){
			setTimeout(function(){					
				$body = $('body');
				$body.off('click','.btn-finalizar-lectura-inconsistencia');
				$body.on('click','.btn-finalizar-lectura-inconsistencia',function(){
					var actn = $(this);
					var suministro = $(this).attr('val_suministro');
					if($.trim($('input',actn.parents('tr:first')).eq(0).val())=='' && $.trim($('input',actn.parents('tr:first')).eq(1).val())==''){
						alert('No se puede validar, por favor, ingrese un codigo OBS o una LECTURA');
					}else{					
	                    $.ajax({
	        				url: ENV_WEBROOT_FULL_URL+"EvaluarInconsistencias/finalizar_relectura/",
	        				type: 'post',
	        				data: {'suministro':suministro},
	        				dataType: 'html'
	        			}).done(function(data){
	        					actn.parents('tr:first').css({background:'#31e18d'});
	        					actn.attr("disabled", true);
	        					$('.btn-re-lectura-inconsistencia',actn.parents('tr:first')).attr("disabled", true);
	        					$('input',actn.parents('tr:first')).attr("disabled", true);
	                            var imagen = "<img title= 'CONSISTENTE' src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAB2UlEQVR42mNgoCW48/rufxAmWeOU85P/d59t/D/xVuv/zsv1/8t3F/yfdGwCYYNuP737v+98x/+mWwn/q2/6/w8+IvffdBvDf5ONwv+1lsr8r95Z8f/Kkyu4DZp0vud/9+2E/x13IsGaXXbI/XfcovTfYKXIf4bZDP8Zuhn+l64r+Y/T2XXX4/73Poz573FQ7L/vXmUwttkoDdY87+zM/1ufrvqvM1vif/PmFkxD2k/V/a+8GfDfbifDfwugs4P364E1My1h+D/3woz////9///k79X/lotl/0dNivmPEdqgAANpzlqd+z97ayZYI/9yhv8Lr84Ga37w7dL/qO0O/xkKGf4XzCtEDQuQAW0X6sBOPXbx5P+vb3+AA2zZ1YVgzbe/nv3vvkYXHAYM+Qz/8+YU/r/06hKqKyr2Fv0XANoavSLi/4u3T8AaYZrtlir/Z2hm+K84BRiYmUL/4yYmYoYBKJ5BUQWyJWND2P83fx+Cne29xvQ/Qw1EM1cZUD5a6X/9unrsMQFyNtiZQByyyQ6iGWizdI8QRLM3xPk40wEoYMq3lII1gGwFBRgYA53NE6v6P3tu/v9Tj04RTpGgeE6emwoObZCN8V1J/xtW15OeJ0Auwpt0qQEABPJKSUqk0FwAAAAASUVORK5CYII='>"	                                        
	        					$('.lbl_resultado_evaluacion',actn.parents('tr:first')).html(imagen);
	        					
	        			});                    
					}
				});
			}, 1000);	
		},
                
		clicpass: function(){			 
		    $body = $('body');
		    $body.off('click','.clickpasslectura');
		    $body.on("click", ".clickpasslectura" , function(e){		        
			    var  lectura = $(this).attr('lectura');
			    var  suministro = $(this).attr('suministro');
			    var  item = $(this).data('index');
			    if (confirm("Seguro que deseas reemplazar el valor en LA del suministro: "+suministro)) {
			        /*$.ajax({
			            url : ENV_WEBROOT_FULL_URL+'ComlecOrdenlecturas/passlectura',
			            dataType: 'json',
			            type: "POST",
			            data : { lectura: lectura, suministro: suministro },
			            success:function(data, textStatus, jqXHR){
				            var montoconsumo=data.montoconsumo;
				            var resultadoevaluacion=data.resultadoevaluacion;
				            var lectura=data.lectura;
				            var obs1=data.obs1;
				            $("#"+item+"resultadoevaluacion").html(resultadoevaluacion);
				            $("#"+item+"montoconsumo").html(montoconsumo);
				           	$("#"+item+"lectura").val(lectura);
				           	$("#"+item+"codigoobservacion").val(obs1);	
				           	$('#'+item+'codigo').data('tipo_lectura','L');
			             }
			        });*/
			    	
    				 var codigoobservacion = $('#'+item+'codigoobservacion').val();
    				 codigo = $('#'+item+'codigo').html();
    				 tipolectura = 'L';
    				 tipolecturareal = $('#'+item+'codigo').data('tipo_lectura_real');

                     if(lectura == '' && codigoobservacion == ''){
                     	 alert('Ingrese una Lectura o Codigo de Observacion');
                     	 $('#'+item+'lectura').focus();
                     	 return false;
                     }
			    	
                     Inconsistencia.onSaveRegistrarLectura(item,tipolectura,tipolecturareal,lectura,suministro,codigoobservacion,function(){
                    	 $("#"+item+"lectura").val(lectura);
 			    		 $('#'+item+'codigo').data('tipo_lectura','L');	
 			    	});
			    };
			});
		    
		    $body.off('click','.clickpasslecturafromrelectura');
		    $body.on("click", ".clickpasslecturafromrelectura" , function(e){
			    var  suministro = $(this).attr('suministro');
			    var  item = $(this).data('index');
			    var  lectura = $("#"+item+"lectura").val();
			    var  tabindex = $("#"+item+"lectura").attr('tabindex');
			    if (confirm("Seguro que deseas reemplazar el valor en LA del suministro: "+suministro)) {
			        /*$.ajax({
			            url : ENV_WEBROOT_FULL_URL+'ComlecOrdenlecturas/passlectura',
			            dataType: 'json',
			            type: "POST",
			            data : { lectura: lectura, suministro: suministro },
			            success:function(data, textStatus, jqXHR){
				            var montoconsumo=data.montoconsumo;
				            var resultadoevaluacion=data.resultadoevaluacion;
				            var lectura=data.lectura;
				            var obs1=data.obs1;
				            
				            $("#"+item+"lectura").parents('td').html('<label id="r">'+lectura+'</label><br><a href="javascript:void(0);" suministro="'+suministro+'" lectura="'+lectura+'" data-index="'+item+'" class="clickpasslectura">pasar a lectura</a>');
				            $("#"+item+"etiquetalectura").parents('td').html('<input style="width:60px;" type="text" id="'+item+'lectura" tabindex="'+tabindex+'" suministro="'+suministro+'" value="" class="TabOnEnter2"><label id="'+item+'etiquetalectura"><label></label></label>');
				            
				            var obs2 = $("#"+item+"codigoobservacion").val();
				            $("#"+item+"codigoobservacion").parents('td').html(obs2);
				            $("#"+item+"etiquetacodigo").eq(0).parents('td').html('<input style="width:25px;" type="text" id="'+item+'codigoobservacion" tabindex="'+tabindex+'" data-index="'+item+'" suministro="'+suministro+'" value="" class="TabOnEnter2"><label id="'+item+'etiquetacodigo"><label></label></label>');				            
				            
				            $("#"+item+"resultadoevaluacion").html(resultadoevaluacion);
				            $("#"+item+"montoconsumo").html(montoconsumo);
				           	$("#"+item+"lectura").val(lectura);
				           	$("#"+item+"codigoobservacion").val(obs1);
				           	$('#'+item+'codigo').data('tipo_lectura','L');
				           	
			             }
			        });*/
			    	var codigoobservacion = $('#'+item+'codigoobservacion').val();
   				 	codigo = $('#'+item+'codigo').html();
   				 	tipolectura = 'L';
   				 	tipolecturareal = $('#'+item+'codigo').data('tipo_lectura_real');

                    if(lectura == '' && codigoobservacion == ''){
                    	 alert('Ingrese una Lectura o Codigo de Observacion');
                    	 $('#'+item+'lectura').focus();
                    	 return false;
                    }
			    	
                    Inconsistencia.onSaveRegistrarLectura(item,tipolectura,tipolecturareal,lectura,suministro,codigoobservacion,function(){
			    		$("#"+item+"lectura").parents('td').html('<label id="r">'+lectura+'</label><br><a href="javascript:void(0);" suministro="'+suministro+'" lectura="'+lectura+'" data-index="'+item+'" class="clickpasslectura">pasar a lectura</a>');
			            $("#"+item+"etiquetalectura").parents('td').html('<input style="width:60px;" type="text" id="'+item+'lectura" tabindex="'+tabindex+'" suministro="'+suministro+'" value="" class="TabOnEnter2"><label id="'+item+'etiquetalectura"><label></label></label>');
			            
			            var obs2 = $("#"+item+"codigoobservacion").val();
			            $("#"+item+"codigoobservacion").parents('td').html(obs2);
			            $("#"+item+"etiquetacodigo").eq(0).parents('td').html('<input style="width:25px;" type="text" id="'+item+'codigoobservacion" tabindex="'+tabindex+'" data-index="'+item+'" suministro="'+suministro+'" value="" class="TabOnEnter2"><label id="'+item+'etiquetacodigo"><label></label></label>');				            
			            
			           	$("#"+item+"lectura").val(lectura);
			           	$('#'+item+'codigo').data('tipo_lectura','L');
			    	});
			    	
			    };
			});
		},
		
		onSaveRegistrarLectura: function(item,tipolectura,tipolecturareal,lectura,codigo,codigoobservacion,callback){
			$.ajax({
				url : ENV_WEBROOT_FULL_URL+'EvaluarInconsistencias/saveregistarlectura',
				dataType: 'json',
				type: "POST",
				data : { tipolectura:tipolectura,tipolecturareal:tipolecturareal,lectura: lectura, codigo: codigo, codigoobservacion: codigoobservacion },
				success:function(data, textStatus, jqXHR){

               	var montoconsumo=data.montoconsumo;
               	var resultadoevaluacion=data.resultadoevaluacion;
               	var validacionlectura=data.validacionlectura;
               	var validacioncodigo=data.validacioncodigo;
               	var etiquetacodigo=data.etiquetacodigo;

               	var mensaje=data.mensaje;
               	var save=data.save;
               	
               	callback();

               	$("#"+item+"etiquetacodigo").html('');
               	if (validacionlectura==true){
                     alert(mensaje);  
                     $("#"+item+"lectura").css('background','red');
                     $("#"+item+"lectura").css('color','#fff');
                     $("#"+item+"etiquetalectura").html(mensaje);
               	}
                 if (validacioncodigo==true){
                     alert(mensaje);  
                     $("#"+item+"codigoobservacion").css('background','red');
                     $("#"+item+"codigoobservacion").css('color','#fff');
                     $("#"+item+"etiquetacodigo").html(etiquetacodigo);
                     $('#'+item+'codigoobservacion').val('');
                     $('#'+item+'codigoobservacion').focus();

                	}
               	if (validacionlectura==false && validacioncodigo==false){
                     $("#"+item+"lectura").css('background','#666');  
                     $("#"+item+"lectura").css('color','#fff');
                     $("#"+item+"codigoobservacion").css('background','#666');
                     $("#"+item+"codigoobservacion").css('color','#fff');
                     $("#"+item+"etiquetalectura").html('');
                     $("#"+item+"etiquetacodigo").html(etiquetacodigo);
                     $("#"+item+"montoconsumo").html(montoconsumo);
                     $("#"+item+"montoconsumo").css('font-weight','bold');
                     
                     if(resultadoevaluacion == 'CONSISTENTE'){
                         var imagen = "<img title= 'CONSISTENTE' src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAB8ElEQVR42mNgoDb4/OfD/9UPpv6vv5z4v+C83/+sM17/c0+F/595o/f/u+/v/uPVfOHrgf8114L+L33f+H/j997/s98V/O95Hvu/5UHI/5wzLv+dtuv+3/Vy23+cmuvvRvw/9H/h/6U/qv53fgj93/DM93/VA/f/Jbcd/xdctfmffcH8v+kaqf/bnqMZAnJ22RW///v+zP0/52f+/76vUXADah97gQ3IvWz1P/OcKRgrLZNG9c6K+1P+z3lZDrcZhFtfB4E1V9xz/V980wFsQNoZ4/9xJ/T/W62T+d9+oh1hQOm5iP8bPvX9b30ZCMfozgfZnAjUHHlM+7/HdpX/NkusEQYknHACuwCkAWQrzGYQBmkGYZjtQYc0/1tvlv0vN1kSYUDkYdv/Ex6lgG1DxjDNyLb77FcDB6T0BAmEATGHvP933ouCa4BhUKiDMLJmj70q/7Xmi/63mm2JMKD7Yis4nmGhjIzjoJoDD2qANYNsl53C+79lbwvCAFCUGG5QBfsTZBsMgzSC/AyzGeR3ldlC/yXbRDFT5bon68DxC9OErNFpjyLYZpBmgWbO/+uurcOeGtc8WPNfbr44OJ6dtyuCMUij1kJRsLPFW0Vwa4aBF19f/K/eW/3faLbBf9FuATA2mqz/v2ZrzX+QHNVzLwAjy7LsIPIjLQAAAABJRU5ErkJggg=='>"	                                        
                     }
                     
                     if(resultadoevaluacion == 'INCONSISTENTE'){
                         var imagen = "<img title='INCONSISTENTE' src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAABrElEQVR42q2TyytEURzH/QFW1JRHIxsTUiNWHgtCFOWRPEpZyAJZkKSZhccgsVBIFqRkZdKUZCFFNLEQJUnJQk0eGcQCk+bj/O6dyZ3rsXLr272dcz7f3/d3zrlRUf/98PIIK1Mw0gzOKnCU6+/FUYJPD/wNH23DcB14F+DcA7sz4B5ScK8+XmuDvXV+h6da4GkfzpZhywWePljqhLlWmGiEwWoCFdbvJlpsNcm1MjmZVwnGfzaQdpT8RdbIdrSe16fhcFEHRWsOtRfdsNCuJ5MWxKCrgMeadPzTowYDp5o8dutQWAKb4tNTqhm8NmdxWZVnMFCDbM3ogFECTzZFVKctl7eGDE6zEwwGalDbbQHCkthSOQyHqstaaeHYHmcw6CjSj0oAoyS2CaY1i6vCJI7Kcr4MgrMD+mK5MEYJaIKlusS/GHMZDNSRvFWmfFUzgyE4UJ/GZW48ezYLAf+96S5srvJckhgJhUCRFBDYmxyNf83z820Mbrjx5cVpMeWoZLfl26cujsTeSbH8Doefj7sbfCMOToozOUyN5cAWw0G+nbN+J+93t/z73/sJB/zttLkU5gUAAAAASUVORK5CYII='>"	                                        
                     }
                     
                     $("#"+item+"resultadoevaluacion").html(imagen);

                     if(resultadoevaluacion == 'INCONSISTENTE'){	                                        	

                          $("#"+item+"codigoobservacion").css('background','#F52D2D');
                          $("#"+item+"codigoobservacion").css('color','#fff');
                          $("#"+item+"lectura").css('background','#F52D2D'); 

                     }
               	}
               	$("#"+item+"lectura").parents('td').find('.clickpasslecturafromrelectura').show();
               	
			}
			});
		},
   
		tabIndex2: function(){
            $body = $('body');
            $body.off('keypress','.TabOnEnter2');
            $body.on("keypress", ".TabOnEnter2" , function(e){
                
            	if( e.keyCode ==  13 ){
	            	cb = parseInt($(this).attr('tabindex'));

	                if ($(':input[tabindex=\'' + (cb + 1) + '\']') != null) {
	                    $(':input[tabindex=\'' + (cb + 1) + '\']').focus();
	                    $(':input[tabindex=\'' + (cb + 1) + '\']').select();
	                }

					 if((this.tabIndex)%2==0){	
						 
						 var num = this.tabIndex;
			             var item = $('[tabindex="' +num+ '"]').data('index');

	                     var lectura = $('#'+item+'lectura').val();
	    				 var suministro = $(this).attr('suministro');
	    				 var codigoobservacion = $('#'+item+'codigoobservacion').val();
	    				 codigo = $('#'+item+'codigo').html(); //en la nueva funcion ya no se usa codigo es por suministro, en vez de codigo se enviara suministro
	    				 tipolectura = $('#'+item+'codigo').data('tipo_lectura');
	    				 tipolecturareal = $('#'+item+'codigo').data('tipo_lectura_real');

	                     if(lectura == '' && codigoobservacion == ''){
	                     	 alert('Ingrese una Lectura o Codigo de Observacion');
	                     	 $('#'+item+'lectura').focus();
	                     	 return false;
	                     }

	                     Inconsistencia.onSaveRegistrarLectura(item,tipolectura,tipolecturareal,lectura,suministro,codigoobservacion,function(){});                     
					 }
				}
            	var num = this.tabIndex;
            	if((this.tabIndex)%2!=0){	
					 
					 num = num + 1;
            	}
            	var item = $('[tabindex="' +num+ '"]').data('index');
		             
            	$("#"+item+"lectura").parents('td').find('.clickpasslecturafromrelectura').hide();
			});
		},
		
		graficoHistoricoLineal : function(){
			setTimeout(function(){
				var pageviews = $('#datainconsistencia').val();
				var plot_statistics = $.plot($("#site_statistics"), [JSON.parse(pageviews)], 
					{
			        series: {
			          lines: {
			            show: true,
			            lineWidth: 2, 
			            fill: true,
			            fillColor: {
			              colors: [{
			                opacity: 0.2
			              }, {
			                opacity: 0.01
			              }
			              ]
			            } 
			          },
			          points: {
			            show: true
			          },
			          shadowSize: 2
			        },
			        legend:{
			          show: false
			        },
			        grid: {
			        labelMargin: 10,
			           axisMargin: 500,
			          hoverable: true,
			          clickable: true,
			          tickColor: "rgba(255,255,255,0.22)",
			          borderWidth: 0
			        },
			        colors: ["#FFFFFF", "#4A8CF7", "#52e136"],
			        xaxis: {
			        	mode: "time",
			        	timeformat: "%0y-%0m"
			        },
			        yaxis: {
			          ticks: 5,
			          tickDecimals: 0
			        }
			      });
			}, 3000);
		},
		
		exportarPdfInconsistencia: function(){
						
			$body = $('body');
			$body.off('click','#exportar-inconsistencia-lectura');
			$body.on('click','#exportar-inconsistencia-lectura',function(){	
				sector = $("#id_sector").val();
          		lecturista = $("#id_lecturista").val();
          		
				window.open(ENV_WEBROOT_FULL_URL+'evaluarInconsistencias/pdf_inconsistencia/'+sector+'/'+lecturista,'_blank');
			});	
		},
		
		ModalImagenInconsistencia: function(){
			$('.fancybox-inconsistencia').fancybox({
			    helpers:  {
			        overlay : null
			    }
			});			
		},
		
		cambiarCodigoLectura: function(){
			$body = $('body');
			$body.off('keypress','#cambiar-codigo-lecturas');
			$body.on("keypress", "#cambiar-codigo-lecturas" , function(e){                    
	            if( e.keyCode ==  13 ){
	            	variable = $(this);
	          		suministro = $(this).attr('suministro');
	          		observacion = $(this).val();
	          		if (confirm("Seguro que deseas reemplazar la OBSERVACION del suministro: "+suministro)) {
	                    $.ajax({
	                        url : ENV_WEBROOT_FULL_URL+'evaluarInconsistencias/cambiar_codigo_lecturas',
	                        dataType: 'json',
	                        type: "POST",
	                        data : {suministro: suministro, observacion: observacion },
	                        success:function(data, textStatus, jqXHR){
	                        	if(data.save){
	                        		if(data.mensaje == 'NOT'){
	                        			alert('No se puedo guardar la OBSERVACION.');
	                        			return false;
	                        		}else{
	                        			alert('Nueva Observacion OK!');
	                        			return false;
	                        		}	
	                        	}else{
	                        		alert(data.mensaje);
	                        		$(variable).val('');
	                        	}	                        	
	                        	
	                        }
	                   });
	             	};
	            };
			});			
		},
		
		loadingBeforeSearchPaginationTable: function(){
			$body = $('body');
			$body.off('click','.pagination a');
			$body.on('click','.pagination a',function(){	
				$("#loading-inconsistencias").mask("Estamos buscando Suministros con Lecturas Inconsistentes, cargando...");
			});
			
			$body.off('change','#tabla-inconsistencias_length select');
			$body.on('change','#tabla-inconsistencias_length select',function(){	
				$("#loading-inconsistencias").mask("Estamos buscando Suministros con Lecturas Inconsistentes, cargando...");
			});
			
			$body.off('keyup','#tabla-inconsistencias_filter input');
			$body.on('keyup','#tabla-inconsistencias_filter input',function(){	
				$("#tabla-inconsistencias").mask("Estamos buscando Suministros con Lecturas Inconsistentes, cargando...");
			});
		},
		
		editarLecturaEnviadaDistriluz: function(){
			$body = $('body');
			$body.off('click','.btn-icon-enviado-a-distriluz');
			$body.on('click','.btn-icon-enviado-a-distriluz',function(){
				$(this).parents('tr:first').css({background:'rgb(49, 225, 141)'});
			  	$('.dialog_loading-editar-lectura').show();
			  	$('#txt_suministro').val($(this).data('suministro'));
			  	$('#txt_index').val($(this).data('index'));
				$("#dialog-editar-lectura").dialog({ minWidth: 300, title: "Editar Lectura" });
				$('#txt_obs').val('');
				$('#form-editar-lectura-contrasena').show();
				$('#form-editar-lectura-add-comentario').hide();
				$('.dialog_loading-editar-lectura').hide();
			});
		}

	};
	
Inconsistencia.init();

return Inconsistencia;

});
