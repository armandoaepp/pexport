<?php
App::uses('AppController', 'Controller');
App::uses('Sanitize', 'Utility');
App::uses('CakeTime', 'Utility');
class UsuariosController extends AppController {

	public $name = 'Usuario';
	public $helpers = array('Html', 'Form');
		
	public function beforeFilter(){	
		$this->Auth->allow('login','logout','index');
		parent::beforeFilter();
	}
	
	//Landing Page
	function index(){
		$this->layout = 'landing_page';
		if($this->Auth->login()){
			$this->redirect(array('controller' => 'dashboard'));
		}		
	}
	
	function login(){
		set_time_limit(0);
		$this->layout = 'login';
		$this->components[] = 'HashPass';

		if($this->Auth->login()){
			$this->redirect(array('controller' => 'ComlecOrdenlecturas', 'action' => 'form'));			
		}
		
		if ($this->request->is('post')){
			if (!empty($this->data) && !($this->Auth->login())){

				$usuario = $this->request->data['Usuario']['usuario'];
				$pass = $this->request->data['Usuario']['pass'];
				
				$this->Session->write('NEGOCIO','CIX');
				$negocio = 'CIX';
				
				/* Vefirico que el negocio sea valido para el enviroment */
				$bool_negocio = false;
				$arr_negocios = Configure::read('lecturas.negocios');
				foreach ($arr_negocios as $k => $v){
					if($negocio==$k){
						$bool_negocio = true;
						$this->Session->write('NEGOCIO_empresa',$v['empresa']);
					}
				}				
				if(!$bool_negocio){
					return $this->redirect($this->Auth->logout());
				}
				/* Fin Verifico negocio */
				
				if($negocio=='TAC'){
					$data_login = $this->Components->load('PostWebservice')->login('http://54.200.245.17/Ws.Lecturas/api/tacna/index.php/loginweb', $params);
				}else{
					$data= array();
					
					$obj_user_login = $this->Usuario->validarUsuarioWeb($usuario,$pass);
					
					if(count($obj_user_login)==1){
						$user_id = $obj_user_login[0][0]['id'];
						$db_password = $obj_user_login[0][0]['passusuario'];
						// check the password the user tried to login with
						$is_valid_pass =  $this->Components->load('HashPass')->check_password($db_password, $pass);

						if($is_valid_pass){
							$data['IdUsuario'] = $user_id;
							$data['NomUsuario'] = $obj_user_login[0][0]['nomusuario'];
							$data['email'] = $obj_user_login[0][0]['email'];
							$data['success'] = true;
							$data['message'] = 'Bienvenido: '.$obj_user_login[0][0]['nomusuario'];
					
							//Inicio de sesion exitosa
							$data_login = json_encode($data);
						}else{
							// deny access
							$data['success'] = false;
							$data['message'] = 'contrasenia incorrecta';
							$data_login = json_encode($data);
						}
						
					
					}else{
						$data['success'] = false;
						$data['message'] = 'Usuario no existe o duplicidad Usuarios';
						$data_login = json_encode($data);
					}
					//FIN
					
					//$data_login = $this->Components->load('PostWebservice')->login('http://54.200.245.17/Ws.Lecturas/api/cix/index.php/loginweb', $params);
				}

				//$data_login = '{"NomUsuario":"3", "IdUsuario":"7"}';
				if($data_login){
					$usuario= json_decode($data_login);
					if($usuario->success != false){
						$this->set('auth', true);
						$this->Session->write('username', $usuario->NomUsuario);
						$this->Session->write('email', $usuario->email);
						//$this->Session->check('username');
						$this->request->data['Usuario']['id'] = $usuario->IdUsuario;
						
						$obj_user = $this->Usuario->findById($this->request->data['Usuario']['id']);
						$obj_user['Usuario']['email'] = $usuario->email;
						
						$this->Auth->login($obj_user);
						$this->set('obj_logged_user', $obj_user);
						
						if($usuario->IdUsuario==25){
							echo "<script type='text/javascript'>window.open('".ENV_WEBROOT_FULL_URL."EvaluarInconsistencias/extranet', '_top');</script>";
							//echo "<script type='text/javascript'>window.open('".ENV_WEBROOT_FULL_URL."home', '_top');</script>";
							//return $this->redirect(array('controller' => 'EvaluarInconsistencias', 'action' => 'extranet'));
						}elseif ($usuario->IdUsuario==64){
							echo "<script type='text/javascript'>window.open('".ENV_WEBROOT_FULL_URL."reportes/reporte_historico', '_top');</script>";
							//return $this->redirect(array('controller' => 'reportes', 'action' => 'reporte_historico'));
						}else{
							echo "<script type='text/javascript'>window.open('".ENV_WEBROOT_FULL_URL."home', '_top');</script>";
							//return $this->redirect($this->Auth->redirect());
						}
					}else{
						return $this->redirect($this->Auth->logout());
					}
				}else{
					return $this->redirect($this->Auth->logout());
				}
			}else{
				return $this->redirect($this->Auth->logout());
			}
		}
	}
	
	public function logout() {
		/* logout and redirect to url set in app controller */
		$this->Session->destroy();
		return $this->redirect($this->Auth->logout());
	}

	public function home(){
		$this->layout = 'home_page';
		//$this->loadModel('GlomasEmpleado');
		//$arr_obj_user = $this->GlomasEmpleado->find('all');
		//pr($arr_obj_user);
		//debug($data);
		//pr($data);
		//print_r($data);
		//exit();
		$hola="hello word";
		$this->set(compact('hola'));		
	}
	
	public function ui_treeview(){
		$this->layout = 'home_tree';
		$hola="hello word";
		$this->set(compact('hola'));
	}
	
	public function listado(){
		$this->layout = 'home_tree';
		
		$this->loadModel("Usuario");
		$arr_usuarios = $this->Usuario->getListado();
		$id_usuario = $this->obj_logged_user['Usuario']['id'];
		
		$this->set(compact('arr_usuarios','id_usuario'));
	}


	
	public function nuevo(){
		$this->layout = 'home_tree';

		$this->loadModel("GlomasEmpleado");
		$listar_empleados = $this->GlomasEmpleado->ListarPersonasbyUsuarios();
		
		$this->set(compact('listar_empleados'));
	}
	
	public function grabar(){
		$this->layout = 'home_tree';
	
		if($this->request->is('post')){
			
			if(!isset($this->request->data['Usuario']['id']) && $this->request->data['Usuario']['id']==''){
				if(isset($this->request->data['Usuario']['usuario']) && isset($this->request->data['Usuario']['pass'])){
					
					$params = array(
							"usuario" => $this->request->data['Usuario']['usuario'],
							"pass" => $this->request->data['Usuario']['pass'],
							"empleado" => $this->request->data['Usuario']['glomas_empleado_id']
					);
					
					$negocio = $this->Session->read('NEGOCIO');				
					if($negocio=='TAC'){
						$data_login = $this->Components->load('PostWebservice')->login('http://54.200.245.17/Ws.Lecturas/api/tacna/index.php/register', $params);
					}elseif($negocio=='TRUX'){
						$data_login = $this->Components->load('PostWebservice')->login('http://54.200.245.17/PEXPORT/Ws.Lecturas/api/cix/index.php/register', $params);
					}elseif($negocio=='TRUX_VALLE'){
						$data_login = $this->Components->load('PostWebservice')->login('http://54.200.245.17/PEXPORT/trux/Ws.Lecturas/api/cix/index.php/register', $params);
					}elseif($negocio=='CAJ'){
						$data_login = $this->Components->load('PostWebservice')->login('http://54.200.245.17/PEXPORT/caj/Ws.Lecturas/api/cix/index.php/register', $params);
					}else{
						$data_login = $this->Components->load('PostWebservice')->login('http://54.200.245.17/Ws.Lecturas/api/cix/index.php/register', $params);
					}
					
					return $this->redirect(array('controller' => 'Usuarios', 'action' => 'listado'));
				}else{
					throw new Exception("Ingrese Usuario y Clave");
				}
			}else{
				if(isset($this->request->data['Usuario']['usuario']) && isset($this->request->data['Usuario']['pass'])){
						
					$params = array(
							"id" => $this->request->data['Usuario']['id'],
							"usuario" => $this->request->data['Usuario']['usuario'],
							"pass" => $this->request->data['Usuario']['pass']
					);
						
					$negocio = $this->Session->read('NEGOCIO');
					if($negocio=='TAC'){
						$data_login = $this->Components->load('PostWebservice')->login('http://54.200.245.17/Ws.Lecturas/api/tacna/index.php/update', $params);
					}elseif($negocio=='TRUX'){
						$data_login = $this->Components->load('PostWebservice')->login('http://54.200.245.17/PEXPORT/Ws.Lecturas/api/cix/index.php/update', $params);
					}elseif($negocio=='TRUX_VALLE'){
						$data_login = $this->Components->load('PostWebservice')->login('http://54.200.245.17/PEXPORT/trux/Ws.Lecturas/api/cix/index.php/update', $params);
					}elseif($negocio=='CAJ'){
						$data_login = $this->Components->load('PostWebservice')->login('http://54.200.245.17/PEXPORT/caj/Ws.Lecturas/api/cix/index.php/update', $params);
					}else{
						$data_login = $this->Components->load('PostWebservice')->login('http://54.200.245.17/Ws.Lecturas/api/cix/index.php/update', $params);
					}
						
					return $this->redirect(array('controller' => 'Usuarios', 'action' => 'listado'));
				}else{
					throw new Exception("Ingrese Usuario y Clave");
				}
			}
		}else{
			throw new Exception("Ingrese Usuario y Clave");
		}
	}
	
	public function editar($id=null){
		$this->layout = 'home_tree';
		
		if(isset($id)){
			$negocio = $this->Session->read('NEGOCIO');
			$this->Usuario->setDataSource (strtolower($negocio));
			$obj_user = $this->Usuario->findById($id);
			
			$this->set(compact('obj_user'));
		}else{
			throw new NotFoundException("Link no valido");
		}
	}
	
	public function eliminar($id=null){
		$this->layout = 'home_tree';
	
		if(isset($id)){			
			if ($this->Usuario->delete($id)) {
				return $this->redirect(array('controller' => 'Usuarios', 'action' => 'listado'));
			}else{
				throw new Exception("No se pudo eliminar");
			}
		}else{
			throw new NotFoundException("Link no valido");
		}
	}
	
	public function verificar_password(){
		set_time_limit(0);
		$this->autorender = false;
		$this->layout = 'ajax';
		$this->components[] = 'HashPass';
		if ($this->request->is('post')){
			
			$usuario = $this->Session->read('username');
			$pass = $this->request->data['clave'];
			
			$obj_user_login = $this->Usuario->validarUsuarioWeb($usuario,$pass);
				
			if(count($obj_user_login)==1){
				$user_id = $obj_user_login[0][0]['id'];
				$db_password = $obj_user_login[0][0]['passusuario'];
				// check the password the user tried to login with
				$is_valid_pass =  $this->Components->load('HashPass')->check_password($db_password, $pass);
			
				if($is_valid_pass){
					$data['success'] = true;
					$data['message'] = 'Bienvenido: '.$obj_user_login[0][0]['nomusuario'];
						
					//Inicio de sesion exitosa
					$data_login = json_encode($data);
				}else{
					// deny access
					$data['success'] = false;
					$data['message'] = 'contrasenia incorrecta';
					$data_login = json_encode($data);
				}
			
					
			}else{
				$data['success'] = false;
				$data['message'] = 'Usuario no existe o duplicidad Usuarios';
				$data_login = json_encode($data);
			}
			echo $data_login;
		}		
		exit;
	}
	
	public function save_update_user_email(){
		set_time_limit(0);
		$this->autorender = false;
		$this->layout = 'ajax';
		$this->components[] = 'HashPass';
		if ($this->request->is('post')){
			$email = $this->request->data['email'];
			
			if(!isset($email)){
				$data['success'] = false;
				$data['message'] = 'Email vacio';
				$data_login = json_encode($data);
			}else{
				$usuario_id = $this->obj_logged_user['Usuario']['id'];
				
				$this->Usuario->updateEmail($usuario_id,$email);
				
				$this->Session->write('email', $email);
				
				$obj_user = $this->Usuario->findById($usuario_id);
				$obj_user['Usuario']['email'] = $email;
				
				$this->Auth->login($obj_user);
				
				$data['success'] = true;
				$data['message'] = 'Email actualizado';
				$data_login = json_encode($data);
			}
			echo $data_login;
		}
		exit;
	}
	
}
