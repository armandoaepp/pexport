<?php
App::uses('AppController', 'Controller');
App::uses('Sanitize', 'Utility');
App::uses('CakeTime', 'Utility');
class MonitorearLecturasController extends AppController {

	public $name = 'MonitorearLecturas';
	

	public function monitorear_lectura(){
		$this->layout = 'home_tree';
		
		$this->loadModel('GlomasEmpleado');
		$this->loadModel('ComlecOrdenlectura');
		
		$date_start = date('Y-m-d');
		$date_end = date('Y-m-d');
		$criterio = 'F';
		$unidadneg = '0';
		$ciclo = '0';
		$sector = '0';
		$listar_empleados = $this->GlomasEmpleado->ListarLecturasMovil($criterio, $date_start, $date_end);
		$listar_unidadneg = $this->ComlecOrdenlectura->listarUnidadneg();
        $listar_ciclo = $this->ComlecOrdenlectura->listarCiclo();
        $arr_ciclos_actuales = $this->ComlecOrdenlectura->getCiclosActuales();
		$listar_sector = $this->ComlecOrdenlectura->listarSector();
		$id_usuario = $this->obj_logged_user['Usuario']['id'];
		
		$options = array('date_start'=>$date_start,'date_end'=>$date_end);
		$get_resumen_kpi = $this->ComlecOrdenlectura->getResumenKpi($options);
		
        $this->set(compact('id_usuario','criterio','date_start','date_end','unidadneg','ciclo','sector','listar_empleados','listar_ciclo','arr_ciclos_actuales','listar_sector','listar_unidadneg','get_resumen_kpi'));
                
	}

	public function ajax_monitorear_lectura($criterio,$date_start,$date_end,$unidadneg,$ciclo,$sector){
		$this->layout = 'home_tree';
		
		$this->loadModel('GlomasEmpleado');
		
		$listar_empleados = $this->GlomasEmpleado->ListarLecturasMovil($criterio, $date_start, $date_end, $unidadneg, $ciclo, $sector);
		
		$tabla='';
        $j=0;
        $i=0;

        foreach ($listar_empleados as $empleados){ 
                             $tabla.='[';
                           //   $i=0;

                            $tabla.= '"'.$empleados['0']['obs_total'].'"';
                            $tabla.= ',"'.$empleados['0']['obs_sin_lectura'].'"';
                            $tabla.= ',"'.$empleados['0']['obs_con_lectura'].'"';
                            $inconsistencias_originales = $empleados['0']['lecturas_inconsistentes']+$empleados['0']['lecturas_evaluadas'];
                            $inconsistencias_actuales = $inconsistencias_originales-$empleados['0']['lecturas_evaluadas'];
                            if($inconsistencias_actuales==0){
                            	$style_inconcistencias_actuales = 'badge badge-success';
                            }else{
                            	$style_inconcistencias_actuales = 'badge badge-danger';
                            }
                            $tabla.= ',"'.$inconsistencias_originales.'"';
                            $tabla.= ',"'.$inconsistencias_actuales.'"';
                            $tabla.= ',"'.$empleados['0']["lecturas_terminadas"].'"';
                            $tabla.= ',"'.$empleados['0']["lecturas_asignadas"].'"';
                            $tabla.= ',"'.$i++.'"';
                            $tabla.= ',"<a target=\"_blank\" href= \"'.ENV_WEBROOT_FULL_URL.'EvaluarInconsistencias/evaluar_inconsistencia/L,'.$criterio.'/'.$date_start.'/'.$date_end.'/'.$unidadneg.'/'.$ciclo.'/'.$sector.'/0/'.$empleados['0']["id"].'\" ><i class=\"fa fa-user\"></i> '.$empleados['0']['lecturista'].'</a>"';
                            $tabla.= ',"'.$empleados['0']["lecturas_asignadas"].'"';
                            $tabla.= ',"'.$empleados['0']["lecturas_descargadas"].'"';
                            
                            $fechainicio = strtotime($empleados['0']["fecha_hora_inicio"]);
                            $fechafin = strtotime($empleados['0']["fecha_hora_fin"]);
                            $tiempo_total = $fechafin - $fechainicio;
                            
                            $horas              = floor ( $tiempo_total / 3600 );
                            $minutes            = ( ( $tiempo_total / 60 ) % 60 );
                            $seconds            = ( $tiempo_total % 60 );
                            
                            $time['horas']      = str_pad( $horas, 2, "0", STR_PAD_LEFT );
                            $time['minutes']    = str_pad( $minutes, 2, "0", STR_PAD_LEFT );
                            $time['seconds']    = str_pad( $seconds, 2, "0", STR_PAD_LEFT );
                            
                            $TT               = implode( ':', $time );
                            
                            $tabla.= ',"<strong>'."HI:&nbsp;".substr($empleados['0']["fecha_hora_inicio"],11,8).'<br>HF:&nbsp;'.substr($empleados['0']["fecha_hora_fin"],11,8).'<br>TT:&nbsp;'.$TT.'<br>TP:&nbsp;'.substr($empleados['0']["tiempo_promedio"],0,8).'</strong>"';
                            $tabla.= ',"'.$empleados['0']["lecturas_terminadas"].'"';
                            $tabla.= ',"'.($empleados['0']["lecturas_asignadas"]-$empleados['0']["lecturas_terminadas"]).'"';
                            $porcentaje = round(($empleados['0']['lecturas_terminadas'] * 100) / $empleados['0']['lecturas_asignadas'],2);
                            if($porcentaje < '50'){
                            	$style_porcentaje = 'progress-bar progress-bar-danger';	
                            }else{
                            	$style_porcentaje = 'progress-bar progress-bar-success';
                            }
                            
                            $tabla.= ',"<div class=\"progress progress-striped\" title =\"'.$porcentaje.'% - '.$empleados['0']['lecturas_terminadas'].' Lecturas Terminadas.\"><div class=\"'.$style_porcentaje.'\" style=\"width: '.$porcentaje.'% ; color: black; font-weight: bold;\">'.$porcentaje.'% ('.$empleados['0']['lecturas_terminadas'].')</div></div>"';

                            $tabla.= ',"'.'Originales: '.$inconsistencias_originales.'<br>Actuales: <span class=\"'.$style_inconcistencias_actuales.'\">'.$inconsistencias_actuales.'</span>'.'"';
                            
                 			$tabla.= ',"'.$empleados['0']['lecturas_evaluadas'].'"'; 
                            $tabla.= ',"'.$empleados['0']['lecturas_consistentes'].'"'; 
                            $obs_total = $empleados['0']['obs_total'];
                            $obs_sin_lectura = $empleados['0']['obs_sin_lectura'];
                            $obs_con_lectura = $empleados['0']['obs_con_lectura'];
                             
                            $tabla.= ',"<strong>'.'T: '.$obs_total.'<br>SL: <span>'.$obs_sin_lectura.'</span><br>CL: <span>'.$obs_con_lectura.'</span> '.'</strong>"';
                            
                            $tabla.= ',"<a class=\"btn btn-info btn-sm btn\" target=\"_blank\" href=\"'.ENV_WEBROOT_FULL_URL.'MonitorearLecturas/ajax_cargar_mapa/'.$empleados['0']["id"].'/'.$criterio.'/'.$date_start.'/'.$date_end.'/'.$ciclo.'/'.$sector.'\"><i class=\"fa fa-globe\"></i></a>"'; 

                            $tabla.=']';  
                                if ($j<count($listar_empleados)-1){
                                        $tabla.=',';
                                    }
                            $j=$j+1;
                        };

		$vari='{"aaData":[';
        $vari.=$tabla;
        $vari.=']}';
        echo $vari;
        exit();              
                
	}
	
	public function ajax_monitorear_lectura_reload($criterio,$date_start,$date_end,$unidadneg,$ciclo,$sector){
		$this->layout = 'ajax';
	
		$this->loadModel('GlomasEmpleado');
		$this->loadModel('ComlecOrdenlectura');
	
		$listar_empleados = $this->GlomasEmpleado->ListarLecturasMovil($criterio, $date_start, $date_end, $unidadneg, $ciclo, $sector);
	
		$options = array();
		if(strpos($criterio, 'F') !== false){
			$options['date_start'] = $date_start;
			$options['date_end'] = $date_end;
		}
		if(strpos($criterio, 'U') !== false){
			$options['unidad_neg'] = $unidadneg;
			$options['idciclo'] = $ciclo;
			$options['idsector'] = $sector;
		}

		$get_resumen_kpi = $this->ComlecOrdenlectura->getResumenKpi($options);
	
		$this->set(compact('id_usuario','criterio','date_start','date_end','unidadneg','ciclo','sector','listar_empleados','get_resumen_kpi'));
	
	}

	/**
	 * Muestra Ruta de Tomas de Lectura por Lecturista
	 * @param int $id_empleado
	 * @param string $criterio
	 * @param string $date_start
	 * @param string $date_end
	 * @param int $unidadneg
	 * @param int $ciclo
	 * @param int $sector
	 * @param string $rango
	 * @author Geynen
	 */
	public function ajax_cargar_mapa($id_empleado,$criterio,$date_start,$date_end,$unidadneg=null,$ciclo=null,$sector=null,$rango=null){
		$this->layout = 'home_tree';
	   	
	   	$this->loadModel('GlomasEmpleado');
	   	$this->loadModel('ComlecOrdenlectura');
	   	
	   	$listar_unidadneg = $this->ComlecOrdenlectura->listarUnidadneg();
	   	$listar_ciclo = $this->ComlecOrdenlectura->listarCiclo();
	   	$listar_sector = $this->ComlecOrdenlectura->listarSector();
	   	
	   	$page = 0;
	   	$limit = 50;
	   	if(isset($rango)){
		   	$arr_rango = explode(',',$rango);
		   	if(count($arr_rango)>0){
		   		$page = $arr_rango[0];
		   		$limit = $arr_rango[1]-$arr_rango[0];
		   	}
	   	}else{
	   		$rango = $page.','.$limit;
	   	}
	   	
	   	$negocio = $this->Session->read('NEGOCIO');
	   	$this->ComlecOrdenlectura->setDataSource (strtolower($negocio));
	   	
	   	$arr_conditions = array('lecturista1_id' => $id_empleado);
	   	if(strpos($criterio, 'F') !== false){
	   		$arr_conditions['fechaejecucion1 >='] = $date_start;
	   		$arr_conditions['fechaejecucion1 <='] = $date_end.' 23:59:59';
	   	}
	   	if(strpos($criterio, 'U') !== false){
		   	if(isset($unidadneg) && $unidadneg!=0 && $unidadneg!=''){
		   		$arr_conditions['glomas_unidadnegocio_id'] = $unidadneg;
		   	}
		   	if(isset($ciclo) && $ciclo!=0 && $ciclo!=''){
		   		$arr_conditions['idciclo'] = $ciclo;
		   	}
		   	if(isset($sector) && $sector!=0 && $sector!=''){
		   		$arr_conditions['sector'] = $sector;
		   	}
	   	}
	   	
	   	$arr_obj_coordenadas = $this->ComlecOrdenlectura->find('all', array(
	   			'fields' => array('suministro','GE.nombre','latitud1','longitud1','fechaejecucion1','COUNT(*) OVER()'),
	   			'joins' => array(
	   					array(
	   							'table' => 'glomas_empleados',
	   							'alias' => 'GE',
	   							'type' => 'INNER',
	   							'conditions' => array(
	   									'ComlecOrdenlectura.lecturista1_id = GE.id'
	   							)
	   					)
	   			),
	   			'conditions' => $arr_conditions,
	   			'order' => 'fechaejecucion1 asc',
	   			'offset' => $page,
	   			'limit' => $limit
	   	));

	   	$this->set(compact('arr_obj_coordenadas','id_empleado','criterio','date_start','date_end','unidadneg','ciclo','sector','rango','page','limit','listar_ciclo','listar_sector','listar_unidadneg'));
	}
	
	public function mapa_monitoreo($criterio=null,$date_start=null,$date_end=null,$unidadneg=null,$ciclo=null,$sector=null){
		$this->layout = 'home_tree';
		ini_set('memory_limit', '-1');
		ini_set('max_execution_time', 300000);
		set_time_limit(0);
		
		$this->loadModel('GlomasEmpleado');
		$this->loadModel('ComlecOrdenlectura');

		if(!isset($date_start) || $date_start=='0'){
			$date_start = date('Y-m-d');
		}
		if(!isset($date_end) || $date_end=='0'){
			$date_end = date('Y-m-d');
		}
		if(!isset($criterio)){
			$criterio = 'F';
		}
		
		$arr_lecturistas_activos = $this->GlomasEmpleado->ListarLecturasMovil($criterio, $date_start, $date_end, $unidadneg, $ciclo, $sector);

		$arr_lecturistas_ids = array();
		foreach ($arr_lecturistas_activos as $k =>$obj_lecturista){
			$arr_lecturistas_ids[] = $obj_lecturista['0']["id"];
		}		
		$arr_ultima_lectura = $this->ComlecOrdenlectura->getUltimaOrdenPorLecturista($arr_lecturistas_ids);
		
		//$ObjComlecOrdenlectura = $this->ComlecOrdenlectura;
		 
		$listar_unidadneg = $this->ComlecOrdenlectura->listarUnidadneg();
		$listar_ciclo = $this->ComlecOrdenlectura->listarCiclo();
		$listar_sector = $this->ComlecOrdenlectura->listarSector();
	
		$this->set(compact('arr_lecturistas_activos','arr_ultima_lectura','criterio','date_start','date_end','unidadneg','ciclo','sector','rango','page','limit','listar_ciclo','listar_sector','listar_unidadneg'));
	}
	
	public function mapa_monitoreo_free($criterio=null,$date_start=null,$date_end=null,$unidadneg=null,$ciclo=null,$sector=null){
		$this->layout = 'ajax';
		ini_set('memory_limit', '-1');
		ini_set('max_execution_time', 300000);
		set_time_limit(0);
	
		$this->loadModel('GlomasEmpleado');
		$this->loadModel('ComlecOrdenlectura');
	
		if(!isset($date_start) || $date_start=='0'){
			$date_start = date('Y-m-d');
		}
		if(!isset($date_end) || $date_end=='0'){
			$date_end = date('Y-m-d');
		}
		if(!isset($criterio)){
			$criterio = 'F';
		}
		$arr_lecturistas_activos = $this->GlomasEmpleado->ListarLecturasMovil($criterio, $date_start, $date_end, $unidadneg, $ciclo, $sector);
		
		$arr_lecturistas_ids = array();
		foreach ($arr_lecturistas_activos as $k =>$obj_lecturista){
			$arr_lecturistas_ids[] = $obj_lecturista['0']["id"];
		}
		$arr_ultima_lectura = $this->ComlecOrdenlectura->getUltimaOrdenPorLecturista($arr_lecturistas_ids);
	
		//$ObjComlecOrdenlectura = $this->ComlecOrdenlectura;
			
		//$listar_unidadneg = $this->ComlecOrdenlectura->listarUnidadneg();
		//$listar_ciclo = $this->ComlecOrdenlectura->listarCiclo();
		//$listar_sector = $this->ComlecOrdenlectura->listarSector();
		
		$listar_unidadneg = null;
		$listar_ciclo = null;
		$listar_sector = null;
	
		$this->set(compact('arr_lecturistas_activos','arr_ultima_lectura','criterio','date_start','date_end','unidadneg','ciclo','sector','rango','page','limit','listar_ciclo','listar_sector','listar_unidadneg'));
	}

}
