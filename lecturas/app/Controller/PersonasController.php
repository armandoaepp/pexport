<?php
App::uses('AppController', 'Controller');
App::uses('Sanitize', 'Utility');
App::uses('CakeTime', 'Utility');
class PersonasController extends AppController {

	public $name = 'Persona';
	public $helpers = array('Html', 'Form');
		
	public function beforeFilter(){	
		$this->Auth->allow('');
		parent::beforeFilter();
	}
	

	
	public function listado(){
		$this->layout = 'home_tree';
		
		$this->loadModel("GlomasEmpleado");
		$arr_personas = $this->GlomasEmpleado->listarempleadosAll();
		$id_usuario = $this->obj_logged_user['Usuario']['id'];
		
		$this->set(compact('arr_personas','id_usuario'));
	}
	
	public function nuevo(){
		$this->layout = 'home_tree';
		
		$id_usuario = $this->obj_logged_user['Usuario']['id'];
		$this->set(compact('id_usuario'));
	}
	
	public function grabar(){
		$this->layout = 'home_tree';
		//echo $this->request->data['GlomasEmpleado']['nombre']; exit();
			
			//echo $this->request->data['GlomasEmpleado']['nombre']; exit();
				
				if(isset($this->request->data['GlomasEmpleado']['nombre']) && isset($this->request->data['GlomasEmpleado']['glomas_tipoempleado_id'])){
					$this->loadModel("GlomasEmpleado");
					
					$this->request->data['GlomasEmpleado']['nombre'];
					$this->request->data['GlomasEmpleado']['glomas_tipoempleado_id'];
					$this->request->data['GlomasEmpleado']['olmax'] = '100';
					$this->request->data['GlomasEmpleado']['olmin']= '100';
					$this->request->data['GlomasEmpleado']['eliminado']= 'f';
					$this->request->data['GlomasEmpleado']['glomas_grupo_id']= '1';

					$this->GlomasEmpleado->create();
					if($this->GlomasEmpleado->save($this->request->data)){
							return $this->redirect(array('controller' => 'Personas', 'action' => 'listado'));
					}else{
						echo json_encode(array('success'=>false,'msg'=>__('This is not saved.')));
					}

				}else{
					throw new Exception("Ingrese Usuario y Clave");
				}
	}
	
	public function editar($id=null){
		$this->layout = 'home_tree';
		
		if(isset($id)){
			$negocio = $this->Session->read('NEGOCIO');
			$this->Usuario->setDataSource (strtolower($negocio));
			$obj_user = $this->Usuario->findById($id);
			
			$this->set(compact('obj_user'));
		}else{
			throw new NotFoundException("Link no valido");
		}
	}
	
	public function eliminar($id=null){
		$this->layout = 'home_tree';
	
		if(isset($id)){			
			if ($this->Usuario->delete($id)) {
				return $this->redirect(array('controller' => 'Usuarios', 'action' => 'listado'));
			}else{
				throw new Exception("No se pudo eliminar");
			}
		}else{
			throw new NotFoundException("Link no valido");
		}
	}
}
