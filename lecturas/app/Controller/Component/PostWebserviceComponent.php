<?php
App::uses('Component', 'Controller');

class PostWebserviceComponent extends Component {
    public $components = array(
        'Session'
    );
    
    /**
     * Servicio que logea usuario
     * @param $url, $params
     * @return Array Pass Hash
     * @author Joel Vasquez Villalobos
     */
	function login($url,$params){
	  $postData = '';
	   foreach($params as $k => $v){ 
	      $postData .= $k . '='.$v.'&'; 
	   }
	   rtrim($postData, '&'); 
	    $ch = curl_init(); 
	    curl_setopt($ch,CURLOPT_URL,$url);
	    curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);
	    curl_setopt($ch,CURLOPT_HEADER, false); 
	    curl_setopt($ch, CURLOPT_POST, count($postData));
	    curl_setopt($ch, CURLOPT_POSTFIELDS, $postData); 
	    $output=curl_exec($ch); 
	    curl_close($ch);
	    return $output; 
	}
	
	function sendJson($url,$params){
		$postData = $params;
		$ch = curl_init();
		curl_setopt($ch,CURLOPT_URL,$url);
		curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);
		curl_setopt($ch,CURLOPT_HEADER, false);
		curl_setopt($ch, CURLOPT_POST, count($postData));
		curl_setopt($ch, CURLOPT_POSTFIELDS, $postData);
		$output=curl_exec($ch);
		curl_close($ch);
		return $output;
	}
}

?>