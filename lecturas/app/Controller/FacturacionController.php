<?php 
App::uses('AppController', 'Controller');
App::uses('Sanitize', 'Utility');
App::uses('CakeTime', 'Utility');
App::import('Vendor', 'php-excel-reader/excel_reader2'); //import library read excell

class FacturacionController extends AppController {

	public $helpers = array('Html', 'Form');
	public $name = 'Facturacion';
	
	public function beforeFilter(){
		parent::beforeFilter();
		$user_type = 'guest';
		$permisos = array("49", "25", "64");
		$this->Auth->allow(array());
		if($this->Auth->login()){
			$user_type = $this->obj_logged_user['Usuario']['id'];
			if (!in_array($user_type, $permisos)) {
				$user_type = 'usuario';
			}
		}

		$rules = array(
			'user_type' => $user_type,
			'redirect' => '/usuarios/logout',
			'message' => 'You dont have permission to access this page',
		    'action' =>  $this->params['action'],
		    'controller' =>  $this->params['controller'],
		    'groups' => array(
		        'guest' => array(),
		       	'usuario' => array('*'),
		    	'49' => array('*'),//Jorge
		        '25' => array('*'),//Jomira
		    	'64' => array('*')//Ensa reportes
		    ),
		    'views' => array(
		        'edit' => 'checkEdit',
		        'delete' => 'checkDelete',
			),
		);
		
		$this->UserPermissions->allow($rules);
	}
	
	public function index(){
		$this->layout = 'home_tree';
		$id_usuario = $this->obj_logged_user['Usuario']['id'];

		$this->loadModel('Factura');
		$uit_value = $this->Factura->getUitActual();
		$alicuota_value = $this->Factura->getAlicuotaActual();
		$count_pliegos = $this->Factura->getCountPliegos();
		$count_facturas = $this->Factura->getCountFacturas();
		
		$this->set(compact('id_usuario','uit_value','alicuota_value','count_pliegos','count_facturas'));
	}
	
	public function simulacion($suministro=null,$consumo=null) {
		//$this->layout = 'ajax';
		$this->layout = 'home_tree';
		$arr_suministros = array();
		$datos_facturacion = array();
		$id_usuario = $this->obj_logged_user['Usuario']['id'];
		
		if ($this->request->is('post')) {
			if($this->request->data['txt_suministro_facturacion']==''){
				$msg = 'Ingrese Suministro';
				$this->Session->write('wizard_status',false);
				$this->Session->write('wizard_msg',$msg);
				$this->redirect(array('action' => 'simulacion'));
			}
			if($this->request->data['txt_consumo_facturacion']==''){
				$msg = 'Ingrese Consumo';
				$this->Session->write('wizard_status',false);
				$this->Session->write('wizard_msg',$msg);
				$this->redirect(array('action' => 'simulacion'));
			}
			$this->loadModel('ComlecOrdenlectura');
			$suministro = $this->request->data['txt_suministro_facturacion'];
			$consumo = $this->request->data['txt_consumo_facturacion'];
			
			$datos_facturacion = $this->ComlecOrdenlectura->datosfacturarrecibo($suministro);

			if(count($datos_facturacion)>0){
				if($datos_facturacion[0][0]['idtarifa'] != ''){
					$tarifa = $datos_facturacion[0][0]['idtarifa'];
				}else{
					$msg = 'Suministro no encontrado.';
					$this->Session->write('wizard_status',false);
					$this->Session->write('wizard_msg',$msg);
					$this->redirect(array('action' => 'simulacion'));
				}
				
				if($datos_facturacion[0][0]['idsector'] != ''){
					$sector = $datos_facturacion[0][0]['idsector'];
				}else{
					$msg = 'Suministro no encontrado.';
					$this->Session->write('wizard_status',false);
					$this->Session->write('wizard_msg',$msg);
					$this->redirect(array('action' => 'simulacion'));
				}
				if($datos_facturacion[0][0]['idplancondicion'] != ''){
					$plancondicion = $datos_facturacion[0][0]['idplancondicion'];
				}else{
					$msg = 'Suministro no encontrado.';
					$this->Session->write('wizard_status',false);
					$this->Session->write('wizard_msg',$msg);
					$this->redirect(array('action' => 'simulacion'));
				}
				
				if($datos_facturacion[0][0]['pfactura'] != ''){
					$periodo = $datos_facturacion[0][0]['pfactura'];
				}else{
					$msg = 'Suministro no encontrado.';
					$this->Session->write('wizard_status',false);
					$this->Session->write('wizard_msg',$msg);
					$this->redirect(array('action' => 'simulacion'));
				}
				
				$arr_suministros = $this->ComlecOrdenlectura->facturar($tarifa, $sector, $plancondicion, $consumo, $periodo, $suministro);
			}else{
				$msg = 'Suministro no encontrado.';
				$this->Session->write('wizard_status',false);
				$this->Session->write('wizard_msg',$msg);
				$this->redirect(array('action' => 'simulacion'));
			}
			
		}else{
			$suministro = "";
			$consumo = "";
		}

		$this->set(compact('suministro','consumo','tarifa', 'sector', 'plancondicion', 'periodo', 'arr_suministros','id_usuario','datos_facturacion'));
	}
	
	public function factura($suministro=null,$consumo=null) {
		$this->layout = 'ajax';
		$this->loadModel('ComlecOrdenlectura');
		$datosfacturarrecibo = $this->ComlecOrdenlectura->datosfacturarrecibo($suministro);
		$tarifa = $datosfacturarrecibo[0][0]['idtarifa'];
		$sector = $datosfacturarrecibo[0][0]['idsector'];
		$plancondicion = $datosfacturarrecibo[0][0]['idplancondicion'];
		$periodo = $datosfacturarrecibo[0][0]['pfactura'];
		
		$arr_suministros = $this->ComlecOrdenlectura->facturar($tarifa, $sector, $plancondicion, $consumo, $periodo, $suministro);
		
		$this->set(compact('datosfacturarrecibo','arr_suministros', 'consumo'));
	}
	
	public function wizard() {
		$this->layout = 'wizard';	
	}
	
	public function wizard_pliego() {
		$this->layout = 'wizard';
	
	}
	
	public function wizard_confirm_pliego() {
		$this->layout = 'wizard';
	    ini_set('memory_limit', '-1');
	    ini_set('max_execution_time', 300000);
	    set_time_limit(0);
        /* Upload archivo XLS al servidor*/

        $allowedExts = array("xls");
        $temp = explode(".", $_FILES["file"]["name"]);
        $extension = end($temp);

        $respuesta_existe=false;
            if ($_FILES["file"]["error"] > 0) {
                echo "ERROR: " . $_FILES["file"]["error"] . "";
            } else {

                    $respuesta_existe=false;
                    move_uploaded_file($_FILES["file"]["tmp_name"], "files/xls_facturacion/" . $_FILES["file"]["name"]);
                    
                    if ($extension=='zip'){
                            $zip = new ZipArchive;
                            $res = $zip->open('files/xls_facturacion/'.$_FILES["file"]["name"]);
                            $filename_in_zip = $zip->getNameIndex(0);
                             //var_dump($res);exit;
                             if ($res === TRUE) {
                             	$temp_zip = explode(".",trim($filename_in_zip));
                             	$extension_in_zip = end($temp_zip);
                             	if($extension_in_zip == 'xls'){
                             		$xml_upload = $temp[0].'.xls';
                             		
                             		$zip->extractTo('files/xls_facturacion/');
                             		$zip->close();
                             	}
                             } else {
                                 echo 'Error descomprimiendo zip';
                                 exit;
                             }
                    }elseif($extension == 'xls'){
                    		$xml_upload = $temp[0].'.xls';
                    }
            }
            
            
            if ($respuesta_existe==true){
                echo "Archivo existe";
                exit;
            }
            
        $this->Session->write('xls_pliego',$temp[0]);
             
		if($extension == 'xls' or (isset($extension_in_zip) && $extension_in_zip == 'xls')){
        	
        	//$data = new Spreadsheet_Excel_Reader('files/xls_facturacion/'.$temp[0].'.xls', true);
        	$xls_view = 0;
        	
        	$this->set('file_name',$temp[0]);
        }else{
        	echo  'error al subir archivo';
        	exit;
        }	
	}
	
	public function wizard_save_pliego(){
		$this->layout = 'wizard';
		
		$id_usuario = $this->obj_logged_user['Usuario']['id'];
		
		$file_name = $this->Session->read('xls_pliego');
		
		if(isset($file_name)){
			$data = new Spreadsheet_Excel_Reader('files/xls_facturacion/'.$file_name.'.xls', true);
			$data_xls = $data->dumptoarray();
			if($data_xls){
				$this->loadModel('Factura');
				$result = $this->Factura->insertarPliego($data_xls,$id_usuario);
				if($result){
					$msg = 'Pliego Importado Correctamente';
					$this->Session->write('wizard_status',true);
					$this->Session->write('wizard_msg',$msg);
					$this->redirect(array('action' => 'wizard_crm'));
				}else{
					$msg = 'Ocurrio un error';
					$this->Session->write('wizard_status',false);
					$this->Session->write('wizard_msg',$msg);
					$this->redirect(array('action' => 'wizard_pliego'));
				}
			}
		}else{
			$msg = 'No existe el archivo.';
			$this->Session->write('wizard_status',false);
			$this->Session->write('wizard_msg',$msg);
		}
		$this->redirect(array('action' => 'wizard_pliego'));
	}
	
	public function wizard_crm() {
		$this->layout = 'wizard';
	
	}
	
	public function wizard_confirm_crm() {
		$this->layout = 'wizard';
		ini_set('memory_limit', '-1');
		ini_set('max_execution_time', 300000);
		set_time_limit(0);
		/* Upload archivo XLS al servidor*/
	
		$allowedExts = array("xls");
		$temp = explode(".", $_FILES["file"]["name"]);
		$extension = end($temp);
	
		$respuesta_existe=false;
		if ($_FILES["file"]["error"] > 0) {
			echo "ERROR: " . $_FILES["file"]["error"] . "";
		} else {
	
			$respuesta_existe=false;
			move_uploaded_file($_FILES["file"]["tmp_name"], "files/xls_facturacion/" . $_FILES["file"]["name"]);
	
			if ($extension=='zip'){
				$zip = new ZipArchive;
				$res = $zip->open('files/xls_facturacion/'.$_FILES["file"]["name"]);
				$filename_in_zip = $zip->getNameIndex(0);
				//var_dump($res);exit;
				if ($res === TRUE) {
					$temp_zip = explode(".",trim($filename_in_zip));
					$extension_in_zip = end($temp_zip);
					if($extension_in_zip == 'xls'){
						$xml_upload = $temp[0].'.xls';
	
						$zip->extractTo('files/xls_facturacion/');
						$zip->close();
					}
				} else {
					echo 'Error descomprimiendo zip';
					exit;
				}
			}elseif($extension == 'xls'){
				$xml_upload = $temp[0].'.xls';
			}
		}
	
	
		if ($respuesta_existe==true){
			echo "Archivo existe";
			exit;
		}
	
		$this->Session->write('xls_crm',$temp[0]);
		 
		if($extension == 'xls' or (isset($extension_in_zip) && $extension_in_zip == 'xls')){
			 
			//$data = new Spreadsheet_Excel_Reader('files/xls_facturacion/'.$temp[0].'.xls', true);
			$xls_view = 0;
			 
			$this->set('file_name',$temp[0]);
		}else{
			echo  'error al subir archivo';
			exit;
		}
	}
	
	public function wizard_save_crm(){
		$this->layout = 'wizard';
	
		$id_usuario = $this->obj_logged_user['Usuario']['id'];
	
		$file_name = $this->Session->read('xls_crm');
	
		if(isset($file_name)){
			$data = new Spreadsheet_Excel_Reader('files/xls_facturacion/'.$file_name.'.xls', true);
			$data_xls = $data->dumptoarray();
			if($data_xls){
				$this->loadModel('Factura');
				$result = $this->Factura->insertarCrm($data_xls,$id_usuario);
				if($result){
					$msg = 'Pliego CRM Importado Correctamente';
					$this->Session->write('wizard_status',true);
					$this->Session->write('wizard_msg',$msg);
					$this->redirect(array('action' => 'wizard_cronograma'));
				}else{
					$msg = 'Ocurrio un error';
					$this->Session->write('wizard_status',false);
					$this->Session->write('wizard_msg',$msg);
					$this->redirect(array('action' => 'wizard_crm'));
				}
			}
		}else{
			$msg = 'No existe el archivo.';
			$this->Session->write('wizard_status',false);
			$this->Session->write('wizard_msg',$msg);
		}
		$this->redirect(array('action' => 'wizard_crm'));
	}
	
	public function wizard_cronograma() {
		$this->layout = 'wizard';
	
	}
	
	public function wizard_confirm_cronograma() {
		$this->layout = 'wizard';
		ini_set('memory_limit', '-1');
		ini_set('max_execution_time', 300000);
		set_time_limit(0);
		/* Upload archivo XLS al servidor*/
	
		$allowedExts = array("xls");
		$temp = explode(".", $_FILES["file"]["name"]);
		$extension = end($temp);
	
		$respuesta_existe=false;
		if ($_FILES["file"]["error"] > 0) {
			echo "ERROR: " . $_FILES["file"]["error"] . "";
		} else {
	
			$respuesta_existe=false;
			move_uploaded_file($_FILES["file"]["tmp_name"], "files/xls_facturacion/" . $_FILES["file"]["name"]);
	
			if ($extension=='zip'){
				$zip = new ZipArchive;
				$res = $zip->open('files/xls_facturacion/'.$_FILES["file"]["name"]);
				$filename_in_zip = $zip->getNameIndex(0);
				//var_dump($res);exit;
				if ($res === TRUE) {
					$temp_zip = explode(".",trim($filename_in_zip));
					$extension_in_zip = end($temp_zip);
					if($extension_in_zip == 'xls'){
						$xml_upload = $temp[0].'.xls';
						
						$zip->extractTo('files/xls_facturacion/');
						$zip->close();
					}
				} else {
					echo 'Error descomprimiendo zip';
					exit;
				}
			}elseif($extension == 'xls'){
				$xml_upload = $temp[0].'.xls';
			}
		}
	
	
		if ($respuesta_existe==true){
			echo "Archivo existe";
			exit;
		}
	
		$this->Session->write('xls_cronograma',$temp[0]);
			
		if($extension == 'xls' or (isset($extension_in_zip) && $extension_in_zip == 'xls')){
	
			//$data = new Spreadsheet_Excel_Reader('files/xls_facturacion/'.$temp[0].'.xls', true);
			$xls_view = 0;
	
			$this->set('file_name',$temp[0]);
		}else{
			echo  'error al subir archivo';
			exit;
		}
	}
	
	public function wizard_save_cronograma(){
		$this->layout = 'wizard';
	
		$id_usuario = $this->obj_logged_user['Usuario']['id'];
	
		$file_name = $this->Session->read('xls_cronograma');
	
		if(isset($file_name)){
			$data = new Spreadsheet_Excel_Reader('files/xls_facturacion/'.$file_name.'.xls', true);
			$data_xls = $data->dumptoarray();
			if($data_xls){
				$this->loadModel('Factura');
				$result = $this->Factura->insertarCronograma($data_xls,$id_usuario);
				if($result){
					$msg = 'Cronograma Importado Correctamente';
					$this->Session->write('wizard_status',true);
					$this->Session->write('wizard_msg',$msg);
					$this->redirect(array('action' => 'wizard_uit'));
				}else{
					$msg = 'Ocurrio un error';
					$this->Session->write('wizard_status',false);
					$this->Session->write('wizard_msg',$msg);
					$this->redirect(array('action' => 'wizard_cronograma'));
				}
			}
		}else{
			$msg = 'No existe el archivo.';
			$this->Session->write('wizard_status',false);
			$this->Session->write('wizard_msg',$msg);
		}
		$this->redirect(array('action' => 'wizard_cronograma'));
	}
	
	public function wizard_uit() {
		$this->layout = 'wizard';
	
		$this->loadModel('Factura');
		$uit_value = $this->Factura->getUitActual();
		$this->set('uit_value',$uit_value);
		
		$alicuota_value = $this->Factura->getAlicuotaActual();
		$this->set('alicuota_value',$alicuota_value);
	}
	
	public function wizard_save_uit(){
		$this->layout = 'wizard';
	
		$id_usuario = $this->obj_logged_user['Usuario']['id'];
		
		$periodo = $this->request->data['txt_periodo'];
		$uit = $this->request->data['txt_uit'];
		$alicuota = $this->request->data['txt_alicuota'];
		
		if($periodo==''){
			$msg = 'Ingrese Periodo';
			$this->Session->write('wizard_status',true);
			$this->Session->write('wizard_msg',$msg);
			$this->redirect(array('action' => 'wizard_uit'));
		}
		
		if($uit==''){
			$msg = 'Ingrese UIT';
			$this->Session->write('wizard_status',true);
			$this->Session->write('wizard_msg',$msg);
			$this->redirect(array('action' => 'wizard_uit'));
		}
		
		if($alicuota==''){
			$msg = 'Ingrese Alicuota';
			$this->Session->write('wizard_status',true);
			$this->Session->write('wizard_msg',$msg);
			$this->redirect(array('action' => 'wizard_uit'));
		}
		$this->loadModel('Factura');
		$result = $this->Factura->insertarUityAlicuota($periodo,$uit,$alicuota,$id_usuario);
		if($result){
			$this->Session->write('periodo_facturacion',$periodo);
			$msg = 'Datos Guardados Correctamente';
			$this->Session->write('wizard_status',true);
			$this->Session->write('wizard_msg',$msg);
			$this->redirect(array('action' => 'wizard_alumbrado_publico'));
		}else{
			$msg = 'Ocurrio un error';
			$this->Session->write('wizard_status',false);
			$this->Session->write('wizard_msg',$msg);
			$this->redirect(array('action' => 'wizard_uit'));
		}
	}
	
	public function wizard_alumbrado_publico() {
		$this->layout = 'wizard';
		
		$periodo = $this->Session->read('periodo_facturacion');
	
		if(!isset($periodo)){
			$msg = 'Ingrese el Periodo';
			$this->Session->write('wizard_status',false);
			$this->Session->write('wizard_msg',$msg);
			$this->redirect(array('action' => 'wizard_uit'));
		}
		
		$this->loadModel('Factura');
		$arr_aps = $this->Factura->getAlumbradoPublico($periodo);
		$this->set('arr_aps',$arr_aps);
	}
	
	public function wizard_save_finish() {
		$this->layout = 'wizard';
		
		$periodo = $this->Session->read('periodo_facturacion');
		
		if(!isset($periodo)){
			$msg = 'Ingrese el Periodo';
			$this->Session->write('wizard_status',false);
			$this->Session->write('wizard_msg',$msg);
			$this->redirect(array('action' => 'wizard_uit'));
		}
	
		$this->loadModel('Factura');		
		$result = $this->Factura->sincronizarAll();
		
		if($result){
			$msg = 'Todo fue importado correctamente.';
			$this->Session->write('wizard_status',true);
			$this->Session->write('wizard_msg',$msg);
			$this->redirect(array('action' => 'wizard_finish'));
		}
		$msg = 'Ocurrio un error.';
		$this->Session->write('wizard_status',false);
		$this->Session->write('wizard_msg',$msg);
		$this->redirect(array('action' => 'wizard_alumbrado_publico'));
		
	}
	
	public function wizard_finish() {
		$this->layout = 'wizard';
	
		$this->loadModel('Factura');
		$uit_value = $this->Factura->getUitActual();		
		$alicuota_value = $this->Factura->getAlicuotaActual();
		$count_pliegos = $this->Factura->getCountPliegos();
		$min_date_pliegos = $this->Factura->getMinDatePliegos();
		$max_date_pliegos = $this->Factura->getMaxDatePliegos();
		
		$count_pliegos_crm = $this->Factura->getCountPliegosCrm();
		$min_date_pliegos_crm = $this->Factura->getMinDatePliegosCrm();
		$max_date_pliegos_crm = $this->Factura->getMaxDatePliegosCrm();
		
		$count_cronograma = $this->Factura->getCountCronograma();
		$min_date_cronograma = $this->Factura->getMinDateCronograma();
		$max_date_cronograma = $this->Factura->getMaxDateCronograma();
		
		$this->set(compact('uit_value','alicuota_value',
				'count_pliegos','min_date_pliegos','max_date_pliegos',
				'count_pliegos_crm','min_date_pliegos_crm','max_date_pliegos_crm',
				'count_cronograma','min_date_cronograma','max_date_cronograma'));
	}
	
	public function pliegos(){
		$this->layout = 'home_tree';
		$this->components[] = 'RequestHandler';
		$id_usuario = $this->obj_logged_user['Usuario']['id'];
		
		$this->set(compact('id_usuario'));
	
	}
	
	public function ajax_listar_pliegos($criterio=null,$date_start=null,$date_end=null){
		 
		if(!isset($criterio)){
			$criterio = 'F';
		}
		if(!isset($date_start)){
			$date_start = date('Y-m-d', strtotime("-1 month"));
		}
		if(!isset($date_end)){
			$date_end = date('Y-m-d');
		}
		
		$this->loadModel('Factura');
		$result = $this->Factura->getAllPliegos($criterio,$date_start,$date_end);		 
		//debug($result);exit;
		$tabla='';
		$j=0;
	
	
		foreach ($result as $obj1){
			$tabla.='[';
			//   $i=0;
	
			$tabla.= '"'.$obj1[0]['fechadesde'].'"';
			$tabla.= ',"<a class=\'btn btn-info btn-sm btn fancybox fancybox.iframe\' href=\"'.ENV_WEBROOT_FULL_URL.'/Facturacion/ajax_detalle_pliego/'.$obj1[0]['fechadesde'].'\">Detallado</a>"';
			
			$tabla.=']';
			if ($j<count($result)-1){
				$tabla.=',';
			}
			$j=$j+1;
		};
	
		$vari='{
	
"aaData":[';
		$vari.=$tabla;
		$vari.=']}';
		echo $vari;exit;
	}
	
	public function save_pliego(){		
		$this->layout = 'ajax';
		ini_set('memory_limit', '-1');
		ini_set('max_execution_time', 300000);
		set_time_limit(0);
		/* Upload archivo XLS al servidor*/
		
		$allowedExts = array("xls");
		$temp = explode(".", $_FILES["file"]["name"]);
		$extension = end($temp);
		
		$respuesta_existe=false;
		if ($_FILES["file"]["error"] > 0) {
			echo "ERROR: " . $_FILES["file"]["error"] . "";
		} else {
		
			$respuesta_existe=false;
			move_uploaded_file($_FILES["file"]["tmp_name"], "files/xls_facturacion/" . $_FILES["file"]["name"]);
		
			if ($extension=='zip'){
				$zip = new ZipArchive;
				$res = $zip->open('files/xls_facturacion/'.$_FILES["file"]["name"]);
				$filename_in_zip = $zip->getNameIndex(0);
				//var_dump($res);exit;
				if ($res === TRUE) {
					$temp_zip = explode(".",trim($filename_in_zip));
					$extension_in_zip = end($temp_zip);
					if($extension_in_zip == 'xls'){
						$xml_upload = $temp[0].'.xls';
						
						$zip->extractTo('files/xls_facturacion/');
						$zip->close();
					}
				} else {
					echo 'Error descomprimiendo zip';
					exit;
				}
			}elseif($extension == 'xls'){
				$xml_upload = $temp[0].'.xls';
			}
		}
		
		
		if ($respuesta_existe==true){
			echo "Archivo existe";
			exit;
		}
		 
		$id_usuario = $this->obj_logged_user['Usuario']['id'];
		
		if($extension == 'xls' or (isset($extension_in_zip) && $extension_in_zip == 'xls')){
			 
			$data = new Spreadsheet_Excel_Reader('files/xls_facturacion/'.$temp[0].'.xls', true);
			$data_xls = $data->dumptoarray();
			if($data_xls){
				$this->loadModel('Factura');
				$result = $this->Factura->insertarPliego($data_xls,$id_usuario);
				if($result){
					$result = $this->Factura->sincronizarPliegos();
					$msg = 'Importado Correctamente';
					$this->Session->write('wizard_status',true);
					$this->Session->write('wizard_msg',$msg);
					$this->redirect(array('action' => 'pliegos'));
				}else{
					$msg = 'Ocurrio un error';
					$this->Session->write('wizard_status',false);
					$this->Session->write('wizard_msg',$msg);
					$this->redirect(array('action' => 'pliegos'));
				}
			}
		}else{
			echo  'error al subir archivo';
			exit;
		}
	}
	
	public function ajax_detalle_pliego($pliego){
		$this->layout = 'home_tree';
		
		$this->loadModel('Factura');
		$arr_detalle_pliego = $this->Factura->getDetallePliego($pliego);
		
		$this->set(compact("pliego","arr_detalle_pliego"));
	}
	
	public function list_alumbrado_publico($periodo='201502'){
		$this->layout = 'home_tree';
		
		if(!isset($periodo)){
			$periodo = date('Ym');
		}
	
		$this->loadModel('Factura');
		$arr_aps = $this->Factura->getAlumbradoPublico($periodo);
	
		$this->set(compact("periodo","arr_aps"));
	}
	
	public function list_facturas(){
		$this->layout = 'home_tree';
	
		$this->loadModel('Factura');
		$arr_fac = $this->Factura->getFacturas();
	
		$this->set(compact("arr_fac"));
	}
	
	public function view_img_factura($suministro){
		$this->layout = 'home_tree';
	
		$this->loadModel('Factura');
		$arr_fac = $this->Factura->getFacturaBySuministro($suministro);
	
		$this->set(compact("arr_fac"));
	}
	
	public function grafico_pliegos(){
		$this->layout = 'home_tree';
		
		$this->loadModel('Factura');
		$result = $this->Factura->getAllPliegos();
	
		$this->set(compact("result"));
	}
	
	public function ajax_pliegos(){
		$this->layout = 'ajax_pliego';
	
		$this->loadModel('Factura');
		$result = $this->Factura->getAllPliegos();
	
		$this->set(compact("result"));
	}
}