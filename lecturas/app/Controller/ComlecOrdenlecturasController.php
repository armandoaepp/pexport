<?php

App::uses('AppController', 'Controller');
App::uses('Sanitize', 'Utility');
App::uses('CakeTime', 'Utility');
App::import('Vendor', 'php-excel-reader/excel_reader2'); //import library read excell
App::uses('CakeEmail', 'Network/Email');

class ComlecOrdenlecturasController extends AppController {

    public $helpers = array('Html', 'Form');
    public $name = 'ComlecOrdenlectura';
    
    public function beforeFilter(){
    	$this->Auth->allow('');
        //$this->Auth->allow(array('index','form'));
    	parent::beforeFilter();
    }
    
    public function index() {
        $this->layout = 'home_tree';

        $arr_suministros = $this->ComlecOrdenlectura->all_conceptos();
        var_dump($arr_suministros);
        exit;
    }

    public function form(){
		$this->layout = 'home_tree';
		$this->components[] = 'RequestHandler';
		$id_usuario = $this->obj_logged_user['Usuario']['id'];
		
		$listar_sector = $this->ComlecOrdenlectura->listarSector();
		
		$this->set(compact('id_usuario','listar_sector'));

    }

    public function liberarLibro(){
    	$this->layout = 'ajax';
    	$libro = $this->request->data['libro'];
    	$libro_c = str_replace('libro','', $libro);
    	$arr_libro = $this->ComlecOrdenlectura->liberar_libro(trim($libro_c));
    	echo  json_encode($arr_libro);
    	exit();    
    }
    
    public function saveForm() {    
    $this->layout = 'ajax';
    ini_set('memory_limit', '-1');
    ini_set('max_execution_time', 300000);
    set_time_limit(0);
        /* Upload archivo xml al servidor*/

        $allowedExts = array("xml");
        $temp = explode(".", $_FILES["file"]["name"]);
        $extension = end($temp);

        $respuesta_existe=false;
       // if ((($_FILES["file"]["type"] == "text/xml")) && in_array($extension, $allowedExts)) {
            if ($_FILES["file"]["error"] > 0) {
                echo "ERROR: " . $_FILES["file"]["error"] . "";
            } else {

               // if (file_exists("XML/" . $_FILES["file"]["name"])) {
               //    $respuesta_existe=true;
                   
               // } else {
                    $respuesta_existe=false;
                    move_uploaded_file($_FILES["file"]["tmp_name"], "files/XML/" . $_FILES["file"]["name"]);
                    
                    if ($extension=='zip'){
                            $zip = new ZipArchive;
                            $res = $zip->open('files/XML/'.$_FILES["file"]["name"]);
                            $filename_in_zip = $zip->getNameIndex(0);
                             //var_dump($res);exit;
                             if ($res === TRUE) {
                             	$temp_zip = explode(".",trim($filename_in_zip));
                             	$extension_in_zip = end($temp_zip);
                             	if($extension_in_zip == 'xml'){
                             		$xml_upload = $temp[0].'.xml';
                             		$comlec_xml = $this->ComlecOrdenlectura->comlecXml($xml_upload);
                             		
                             		if ($comlec_xml == '1') {
                             			$respuesta_existe=true;
                             		}else{
                             			$zip->extractTo('files/XML/');
                             			$zip->close();
                             			$respuesta_existe=false;
                             		}	
                             	}elseif($extension_in_zip == 'xls'){
                             		$xml_upload = $temp[0].'.xls';
                             		$comlec_xml = $this->ComlecOrdenlectura->comlecXml($xml_upload);
                             		 
                             		if ($comlec_xml == '1') {
                             			$respuesta_existe=true;
                             		}else{
                             			$zip->extractTo('files/XML/');
                             			$zip->close();
                             			$respuesta_existe=false;
                             		}
                             	}
                             } else {
                                 echo 'Error descomprimiendo zip';
                                 exit;
                             }
                    }elseif($extension == 'xml'){
                    		$xml_upload = $temp[0].'.xml';
                    		$comlec_xml = $this->ComlecOrdenlectura->comlecXml($xml_upload);
                    		
                    		if ($comlec_xml == '1') {
                    			$respuesta_existe=true;
                    		} else{
                    			$respuesta_existe=false;
                    		}
                    }elseif($extension == 'xls'){
                    		$xml_upload = $temp[0].'.xls';
                    		$comlec_xml = $this->ComlecOrdenlectura->comlecXml($xml_upload);
                    		
                    		if ($comlec_xml == '1') {
                    			$respuesta_existe=true;
                    		} else{
                    			$respuesta_existe=false;
                    		}                        
                    }
               // }
            }
            
            
            if ($respuesta_existe==true){
                echo "Archivo existe";
                exit;
            }
             
        if($extension == 'xml' or (isset($extension_in_zip) && $extension_in_zip == 'xml')){
	        $content = utf8_encode(file_get_contents('files/XML/'.$temp[0].'.xml'));
	        $xml = simplexml_load_string($content);
	        /* Lectura del archivo xml desde su ubicacion en el servidor*/       
	        $this->loadModel('Usuario');        
	        $grupo_id= $this->Usuario->IdGrupoByUsuario($this->obj_logged_user['Usuario']['id']);   
	        /* Lectura de los conceptos que se encuentran relacionados segun la unidad del negocio */
	        /* Se simulara un id del negocio (1) */
	        /* El id del negocio de simulacion en el futuro sera obtenido desde la sesion */
	        $conceptosByGrupo = $this->ComlecOrdenlectura->conceptosByGrupo($grupo_id);
	        
	        /* Crear tabla orden de lectura en funcion a los conceptos obtenidos */
	        /* Nomenclatura de la tabla lecturas comlec_ordenlecturas{id de unidad de negocio} */
	        /* La nomenclatura de la tabla permitira trabajar con varias unidades del negocio al mismo tiempo */
	
	        /*insertar uml en la nueva tabla creada*/
	        if(isset($_POST['txt_tipo']) && $_POST['txt_tipo']=='SUBESTACION'){
	        	// pendiente para xml
	        	//$insertaruml = $this->ComlecOrdenlectura->insertarUmlv2($conceptosByGrupo,$grupo_id,$xml,$_FILES["file"]["name"],$this->obj_logged_user['Usuario']['id'],$this->request->clientIp());
	        }else{
	        	$insertaruml = $this->ComlecOrdenlectura->insertarUmlv2($conceptosByGrupo,$grupo_id,$xml,$_FILES["file"]["name"],$this->obj_logged_user['Usuario']['id'],$this->request->clientIp());
	        }
        }elseif($extension == 'xls' or (isset($extension_in_zip) && $extension_in_zip == 'xls')){
        	
        	$data = new Spreadsheet_Excel_Reader('files/XML/'.$temp[0].'.xls', true);
        	$temp = $data->dumptoarray();
        	if($temp){
        		/* Lectura del archivo xml desde su ubicacion en el servidor*/
        		$this->loadModel('Usuario');
        		$grupo_id= $this->Usuario->IdGrupoByUsuario($this->obj_logged_user['Usuario']['id']);
        		/* Lectura de los conceptos que se encuentran relacionados segun la unidad del negocio */
        		/* Se simulara un id del negocio (1) */
        		/* El id del negocio de simulacion en el futuro sera obtenido desde la sesion */
        		$conceptosByGrupo = $this->ComlecOrdenlectura->conceptosByGrupo($grupo_id);
        		
        		/* Crear tabla orden de lectura en funcion a los conceptos obtenidos */
        		/* Nomenclatura de la tabla lecturas comlec_ordenlecturas{id de unidad de negocio} */
        		/* La nomenclatura de la tabla permitira trabajar con varias unidades del negocio al mismo tiempo */
        		  
        		/*insertar uml en la nueva tabla creada*/
        		if(isset($_POST['txt_tipo']) && $_POST['txt_tipo']=='SUBESTACION'){
        			$insertaruml = $this->ComlecOrdenlectura->insertarSubEstacionXlsv2($conceptosByGrupo,$grupo_id,$temp,$_FILES["file"]["name"],$this->obj_logged_user['Usuario']['id'],$this->request->clientIp());
        		}else{
        			$insertaruml = $this->ComlecOrdenlectura->insertarXlsv2($conceptosByGrupo,$grupo_id,$temp,$_FILES["file"]["name"],$this->obj_logged_user['Usuario']['id'],$this->request->clientIp());
        		}
        	}
        }

    
        if (@$insertaruml){
            
            	$hola="archivo subido correctamente";
				$this->set(compact('hola'));
                $this->redirect(array('action' => 'listar_asignacion'));
    
        }
        else
        {
            $hola="error al subir archivo";
            $this->set(compact('hola'));
           //  echo  'error al subir archivo';
        }
    }

    public function listarordenlecturas(){
//        
		$this->layout = 'home_tree';
//               
//		$listarOrdenLecturas = $this->ComlecOrdenlectura->listarOrdenLecturas($grupo_id);
//      $conceptosByGrupo = $this->ComlecOrdenlectura->conceptosByGrupo( $grupo_id);
//      $this->set(compact('listarOrdenLecturas','conceptosByGrupo'));	
//      
    }
        
 	public function listarciclosector(){
		$this->layout = 'home_tree';
		$this->loadModel('Usuario');
		$grupo_id= $this->Usuario->IdGrupoByUsuario($this->obj_logged_user['Usuario']['id']);
		$id_usuario =  $this->obj_logged_user['Usuario']['id'];
		$listar_orden_lecturas = $this->ComlecOrdenlectura->listarCicloSector();
       //var_dump($listar_orden_lecturas);exit;
	   $this->set(compact('listar_orden_lecturas', 'id_usuario'));
	}
	
	public function relectura(){
		$this->layout = 'home_tree';
		$this->loadModel('Usuario');
		$grupo_id= $this->Usuario->IdGrupoByUsuario($this->obj_logged_user['Usuario']['id']);
		$listar_orden_lecturas = $this->ComlecOrdenlectura->listarCicloSectorReLectura();
		$this->set(compact('listar_orden_lecturas'));
	}
	
	public function buscarciclosector(){
		$this->layout = 'ajax';
		$tipo = $this->request->data['tipo'];
		$ciclo = $this->request->data['ciclo'];
		$sector= $this->request->data['sector'];
		$this->loadModel('Usuario');
		$grupo_id= $this->Usuario->IdGrupoByUsuario($this->obj_logged_user['Usuario']['id']);
		
		if($tipo=='L'){
			$buscar_ciclo_sector = $this->ComlecOrdenlectura->buscarCicloSector($ciclo, $sector, $grupo_id);
		}else{
			$buscar_ciclo_sector = $this->ComlecOrdenlectura->buscarCicloSectorReLectura($ciclo, $sector, $grupo_id);
		}
		
       	$this->loadModel('GlomasEmpleado');
       	$listar_empleados = $this->GlomasEmpleado->ListarEmpleados();
       	$model_ComlecOrdenlectura = $this->ComlecOrdenlectura;
	   	$this->set(compact('buscar_ciclo_sector','listar_empleados','tipo','ciclo','sector','model_ComlecOrdenlectura'));
	}
	
	public function buscarciclosectorrelectura(){
		$this->layout = 'ajax';
		$ciclo = $this->request->data['ciclo'];	
		$sector= $this->request->data['sector'];
		
		$this->loadModel('Usuario');
		$grupo_id= $this->Usuario->IdGrupoByUsuario($this->obj_logged_user['Usuario']['id']);
		$buscar_ciclo_sector = $this->ComlecOrdenlectura->buscarCicloSectorReLectura($ciclo, $sector, $grupo_id);
		$this->loadModel('GlomasEmpleado');
		$listar_empleados = $this->GlomasEmpleado->ListarEmpleados();
		$this->set(compact('buscar_ciclo_sector','listar_empleados'));
	}
	
    
    public function listarordenlecturasintro(){ 
          // var_dump('holaddddddddddddddddddddddddddddddddddddddddd');exit;
       // $this->layout = 'ajax';
    	$this->loadModel('Usuario');
    	$grupo_id= $this->Usuario->IdGrupoByUsuario($this->obj_logged_user['Usuario']['id']);
    	
        $listarOrdenLecturas = $this->ComlecOrdenlectura->listarOrdenLecturas($grupo_id);
       // var_dump($listarOrdenLecturas);exit;
        $conceptosByGrupo = $this->ComlecOrdenlectura->conceptosByGrupo( $grupo_id);
        $j=0;
        $tabla='';
                        foreach ($listarOrdenLecturas as $obj1){ 
                            $tabla.='[';
                         //   var_dump(count($conceptosByGrupo));exit;
                             $i=0;
                             $tabla.='"<img src=\"/images/plus.png\" >",';
                                foreach ($conceptosByGrupo as $obj){
                                
                          
                                    
                                $var=strtolower( $obj[0]['descripcion']);

                                $tabla.= '"'.$obj1[0][$var].'"'; 
                                
                                    if ($i<count($conceptosByGrupo)-1){
                                        $tabla.=',';
                                    }
                                 $i=$i+1;
                                
                                
                               } 
                           $tabla.=']';  
                                if ($j<count($listarOrdenLecturas)-1){
                                        $tabla.=',';
                                    }
                            $j=$j+1;
                        };
                        
                        //var_dump($tabla);exit;
     //   echo json_encode($listarOrdenLecturas); exit;
     
        
     //   var_dump($arr_suministros);exit;
   //    $this->set(compact('listarOrdenLecturas','conceptosByGrupo'));     
     //  $this->set('hola','q tal como te va');       
        $vari='{"sEcho":1,"iTotalRecords":135,"iTotalDisplayRecords":135,
            

"aaData":[';
        
        $vari.=$tabla;

  
        $vari.=']}';
        echo $vari;exit;

    }
    
	public function listacreatelibro(){
		$this->layout = 'ajax';
		$this->loadModel('GlomasEmpleado');
		$this->loadModel('Usuario');
		$grupo_id= $this->Usuario->IdGrupoByUsuario($this->obj_logged_user['Usuario']['id']);
		$ciclo = $this->request->data['ciclo'];
		$sector= $this->request->data['sector'];
		$ruta= $this->request->data['ruta'];
		$listar_create_libro = $this->ComlecOrdenlectura->listaCreateLibro($ciclo, $sector, $ruta, $grupo_id);
		$listar_empleados = $this->GlomasEmpleado->ListarEmpleados();
       //print_r($listar_empleados);exit;
	   $this->set(compact('listar_create_libro','listar_empleados'));
	}
        
        public function listacreatelibro2(){
        	set_time_limit(0);
                $this->layout = 'ajax';
                $this->loadModel('Usuario');
                $grupo_id= $this->Usuario->IdGrupoByUsuario($this->obj_logged_user['Usuario']['id']);
                
                $tipo = $this->request->data['tipo'];
                $ciclo = $this->request->data['ciclo'];
		$sector= $this->request->data['sector'];
		$ruta= $this->request->data['ruta'];

		if($tipo=='L'){
			$listar_create_libro = $this->ComlecOrdenlectura->listaCreateLibro($ciclo, $sector, $ruta, $grupo_id);
		}else{
			$listar_create_libro = $this->ComlecOrdenlectura->listaCreateLibroRelectura($ciclo, $sector, $ruta, $grupo_id);
		}
                		//var_dump($listar_create_libro);exit;
                        $j=0;
                        $tabla='';
                        foreach ($listar_create_libro as $obj1){ 
                            $tabla.='[';
                         //   var_dump(count($conceptosByGrupo));exit;
                             $i=0;
                           // $tabla.='"<img src=\"images/plus.png\" >",';
                             		
                             $m=$j+1;
                             $tabla.= '"'.$obj1[0]['ordenruta'].'"';  
                            $tabla.= ',"'.$obj1[0]['orden'].'"';
                            $tabla.= ',"'.$obj1[0]['suministro'].'"';
                            $cliente = h(trim($obj1[0]['cliente']));
                            
                             $tabla.= ',"'.stripslashes(htmlspecialchars($cliente, ENT_QUOTES)).'"';  
                            $tabla.= ',"'.stripslashes(htmlspecialchars($obj1[0]['direccion'], ENT_QUOTES)) .'"';  
                            $tabla.= ',"'.$obj1[0]['libro'].'"'; 
                             $tabla.= ',"'.stripslashes(htmlspecialchars($obj1[0]['nombre'], ENT_QUOTES)).'"'; 
                             $tabla.= ',"'.$obj1[0]['glomas_estado_id'].'"'; 

                           $tabla.=']';  
                                if ($j<count($listar_create_libro)-1){
                                        $tabla.=',';
                                    }
                            $j=$j+1;
                        };

        $vari='{
            

"aaData":[';
        
        $vari.=$tabla;

  
        $vari.=']}';
        echo $vari;exit;
      

	}
        
        public function listacreatelibrorelectura(){
            
                $this->layout = 'ajax';
                $this->loadModel('Usuario');
                $grupo_id= $this->Usuario->IdGrupoByUsuario($this->obj_logged_user['Usuario']['id']);
                $ciclo = $this->request->data['ciclo'];
		$sector= $this->request->data['sector'];
		$ruta= $this->request->data['ruta'];

		$listar_create_libro = $this->ComlecOrdenlectura->listaCreateLibroRelectura($ciclo, $sector, $ruta, $grupo_id);
                		//var_dump($listar_create_libro);exit;
                        $j=0;
                        $tabla='';
                        foreach ($listar_create_libro as $obj1){ 
                            $tabla.='[';
                         //   var_dump(count($conceptosByGrupo));exit;
                             $i=0;
                           // $tabla.='"<img src=\"images/plus.png\" >",';
                             $m=$j+1;
                             $tabla.= '"'.$obj1[0]['ordenruta'].'"';  
                            $tabla.= ',"'.$obj1[0]['orden'].'"';
                            $tabla.= ',"'.$obj1[0]['suministro'].'"';
                            $cliente = preg_replace('([^A-Za-z0-9])', '', trim($obj1[0]['cliente']));
                            
                             $tabla.= ',"'.$cliente.'"';  
                            $tabla.= ',"'.$obj1[0]['direccion'].'"';  
                            $tabla.= ',"'.$obj1[0]['libro'].'"'; 
                             $tabla.= ',"'.$obj1[0]['nombre'].'"'; 
                             $tabla.= ',"'.$obj1[0]['glomas_estado_id'].'"'; 

                           $tabla.=']';  
                                if ($j<count($listar_create_libro)-1){
                                        $tabla.=',';
                                    }
                            $j=$j+1;
                        };

        $vari='{
            

"aaData":[';
        
        $vari.=$tabla;

  
        $vari.=']}';
        echo $vari;exit;
      

	}
        
	public function listarLibros(){
		$this->loadModel('Usuario');
		$grupo_id= $this->Usuario->IdGrupoByUsuario($this->obj_logged_user['Usuario']['id']);
		$tipo = $this->request->data['tipo'];
		$ciclo = $this->request->data['ciclo'];
		$sector= $this->request->data['sector'];
		$ruta= $this->request->data['ruta'];
		$pfactura= $this->request->data['pfactura'];
		
		if($tipo=='L'){
			$listarLibros= $this->ComlecOrdenlectura->listaLibros($ciclo, $sector,$ruta, $grupo_id,$pfactura);
		}else{
			$listarLibros= $this->ComlecOrdenlectura->listaLibrosRelecturas($ciclo, $sector,$ruta, $grupo_id,$pfactura);
		}
		
		$j=0;
		$tabla='';
		foreach ($listarLibros as $obj1){
			$tabla.='[';
			$i=0;
			$tabla.= '"'.$obj1[0]['libro'].'"';
			$tabla.= ',"'.$obj1[0]['lecturista'].'"';
			$tabla.= ',"'.$obj1[0]['count'].'"';
			$tabla.= ',"'.$obj1[0]['min'].'"';
			$tabla.= ',"'.$obj1[0]['max'].'"';
			$tabla.= ',"'.$obj1[0]['estado'].'"';
			$tabla.=']';
			if ($j<count($listarLibros)-1){
				$tabla.=',';
			}
			$j=$j+1;
		};
		$vari='{"aaData":[';
		$vari.=$tabla;
		$vari.=']}';
		echo $vari;
		exit();

	}

        
                
	public function listarLibrosRelecturas(){
		$this->loadModel('Usuario');
		$grupo_id= $this->Usuario->IdGrupoByUsuario($this->obj_logged_user['Usuario']['id']);
		$ciclo = $this->request->data['ciclo'];
		$sector= $this->request->data['sector'];
		$ruta= $this->request->data['ruta'];
		$pfactura= $this->request->data['pfactura'];
		
		$listarLibros= $this->ComlecOrdenlectura->listaLibrosRelecturas($ciclo, $sector,$ruta, $grupo_id,$pfactura);
		
		$j=0;
		$tabla='';
		foreach ($listarLibros as $obj1){
			$tabla.='[';
			$i=0;
			$tabla.= '"'.$obj1[0]['libro'].'"';
			$tabla.= ',"'.$obj1[0]['lecturista'].'"';
			$tabla.= ',"'.$obj1[0]['count'].'"';
			$tabla.= ',"'.$obj1[0]['min'].'"';
			$tabla.= ',"'.$obj1[0]['max'].'"';
			$tabla.= ',"'.$obj1[0]['estado'].'"';
			$tabla.=']';
			if ($j<count($listarLibros)-1){
				$tabla.=',';
			}
			$j=$j+1;
		};
		$vari='{"aaData":[';
		$vari.=$tabla;
		$vari.=']}';
		echo $vari;
		exit();

	}

	public function guardarlecturistalibro(){
		$this->layout = 'ajax';
		$this->loadModel('Usuario');
		$grupo_id= $this->Usuario->IdGrupoByUsuario($this->obj_logged_user['Usuario']['id']);
		$desde = $this->request->data['desde'];
		$hasta= $this->request->data['hasta'];
		$lecturista= $this->request->data['lecturista'];
		
		$ciclo= $this->request->data['ciclo'];
		$sector= $this->request->data['sector'];
		$ruta= $this->request->data['ruta'];
		$negocio= $this->request->data['negocio'];
		$fecha= $this->request->data['fecha'];
		
		$create_libro = $this->ComlecOrdenlectura->guardarLecturistaLibro($desde, $hasta, $lecturista, $ciclo, $sector, $ruta, $grupo_id, $negocio, $fecha);
		exit;
	}
        public function guardarlecturistalibrorelecturas(){
		$this->layout = 'ajax';
		$this->loadModel('Usuario');
		$grupo_id= $this->Usuario->IdGrupoByUsuario($this->obj_logged_user['Usuario']['id']);
		$desde = $this->request->data['desde'];
		$hasta= $this->request->data['hasta'];
		$lecturista= $this->request->data['lecturista'];
		
		$ciclo= $this->request->data['ciclo'];
		$sector= $this->request->data['sector'];
		$ruta= $this->request->data['ruta'];
		$negocio= $this->request->data['negocio'];
		
		$create_libro = $this->ComlecOrdenlectura->guardarLecturistaLibroRelecturas($desde, $hasta, $lecturista, $ciclo, $sector, $ruta, $grupo_id, $negocio);
		exit;
	}
    public function arbol(){
    	$this->loadModel('Usuario');
    	$grupo_id= $this->Usuario->IdGrupoByUsuario($this->obj_logged_user['Usuario']['id']);
         $listar_tree = $this->ComlecOrdenlectura->listar_tree( $grupo_id);
       var_dump($listar_tree);exit;
        $data     = $em->getRepository('SisgesdepAtletaBundle:Especialidad')
            ->dropDownListCategorias($Id);
//            foreach ($data as $datos) {
//                if ($datos['parent']=='6'){
//                    $datos['parent']=55555555555;
//                }
//           
//   
//            }
//            
            
//            for($i = 0; $i < count($data); ++$i) {
//               if ( $data[$i]['parent'] == null)
//               {
//                   $data[$i]['parent']='#';
//               }
//            }
//          
//       echo json_encode($data); exit;
       $tree = new TreeExtJS();
       for($i=0;$i<count($data);$i++){
               if ( $data[$i]['parent'] == null)
               {
                   $data[$i]['parent']='#';
               }
               $category = $data[$i];
               $tree->addChild($category,$category["parent"]);
       }
       
  
         // echo json_encode(array('save' => false)); exit;
       echo $tree->toJson();exit;
    }
    
    public function digitarlecturas($idsector=null){
    	ini_set('memory_limit', '-1');
    	$this->layout = 'home_tree';
    	
    	$this->loadModel('GlomasEmpleado');
    	$this->loadModel('Usuario');
    	$listar_empleados = $this->GlomasEmpleado->ListarEmpleados();    	
    	$grupo_id= $this->Usuario->IdGrupoByUsuario($this->obj_logged_user['Usuario']['id']);
    	$listar_sector = $this->ComlecOrdenlectura->listarSector();
    	$listar_ruta = $this->ComlecOrdenlectura->listarRuta();
    	$id_usuario = $this->obj_logged_user['Usuario']['id'];
		$this->set(compact('id_usuario','listar_empleados','listar_sector','listar_ruta','idsector')); 
    }
    
    
    public function listarDigitarLecturas($lecturista_id, $ruta_id, $sector_id, $tipolectura_id, $lectespecial){
    	
    	ini_set('memory_limit', '-1');
    	ini_set('max_execution_time', 300000);
    	set_time_limit(0);
    	
    	$this->loadModel('Usuario');
    	$grupo_id= $this->Usuario->IdGrupoByUsuario($this->obj_logged_user['Usuario']['id']);
    	
                $listaDigitarLecturas=$this->ComlecOrdenlectura->listaDigitarLectura($grupo_id,$lecturista_id, $ruta_id, $sector_id, $tipolectura_id,$lectespecial);
                         $tabla='';
                         $j=0;
                         $i=1;
                         $m=1;


                        foreach ($listaDigitarLecturas as $obj1){ 
                             $tabla.='[';
                           //   $i=0;

                            $tabla.= '"'.$obj1[0]['orden'].'"';
                            $tabla.= ',"<label style=\"width:70px;\" readonly type=\"text\"  id=\"'.$m++.'codigo\"    placeholder=\"Codigo\"   >'.$obj1[0]['comlec_ordenlectura_id'].'</label>"';
                           
                            $ele='';
                            if ($obj1[0]['libro']!=''){
                                $ele='L';
                            }
                            
                            $tabla.= ',"'.$ele.$obj1[0]['libro'].'"';
                            /*$tabla.= ',"'.$obj1[0]['idciclo'].'"'; 
                      
                            $tabla.= ',"'.$obj1[0]['sector'].'"';
                            $tabla.= ',"'.$obj1[0]['ruta'].'"';
                            */
                            $tabla.= ',"'.$obj1[0]['lecturista'].'"';
                            $tabla.= ',"'.$obj1[0]['seriefab'].'"';
                 
                            $tabla.= ',"'.$obj1[0]['suministro'].'"'; 
                            /*
                            $tabla.= ',"'.strtoupper($obj1[0]['nombre']).'"';  
                            */
                            $tabla.= ',"'.strtoupper($obj1[0]['direccion']).'"';  
                            $tabla.= ',"'.$obj1[0]['lecaant'].'"';
                            $tabla.= ',"'.$obj1[0]['lecant'].'"';
                            
                            $style='';
                            if ($obj1[0]['lecturao2']){
                                $style='background:#666;color:#fff;';
                            }
                            $icon = '';
                         	if ($obj1[0]['resultadoevaluacion']=='INCONSISTENTE'){
				            	$style='background:red;color:#fff;';
				            	$icon = '<img title= \"INCONSISTENTE\" src=\"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAABrElEQVR42q2TyytEURzH/QFW1JRHIxsTUiNWHgtCFOWRPEpZyAJZkKSZhccgsVBIFqRkZdKUZCFFNLEQJUnJQk0eGcQCk+bj/O6dyZ3rsXLr272dcz7f3/d3zrlRUf/98PIIK1Mw0gzOKnCU6+/FUYJPD/wNH23DcB14F+DcA7sz4B5ScK8+XmuDvXV+h6da4GkfzpZhywWePljqhLlWmGiEwWoCFdbvJlpsNcm1MjmZVwnGfzaQdpT8RdbIdrSe16fhcFEHRWsOtRfdsNCuJ5MWxKCrgMeadPzTowYDp5o8dutQWAKb4tNTqhm8NmdxWZVnMFCDbM3ogFECTzZFVKctl7eGDE6zEwwGalDbbQHCkthSOQyHqstaaeHYHmcw6CjSj0oAoyS2CaY1i6vCJI7Kcr4MgrMD+mK5MEYJaIKlusS/GHMZDNSRvFWmfFUzgyE4UJ/GZW48ezYLAf+96S5srvJckhgJhUCRFBDYmxyNf83z820Mbrjx5cVpMeWoZLfl26cujsTeSbH8Doefj7sbfCMOToozOUyN5cAWw0G+nbN+J+93t/z73/sJB/zttLkU5gUAAAAASUVORK5CYII=">';
				            }
				            	
				            if ($obj1[0]['resultadoevaluacion']=='CONSISTENTE'){
				            	$icon = '<img title= \"CONSISTENTE\" src=\"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAB8ElEQVR42mNgoDb4/OfD/9UPpv6vv5z4v+C83/+sM17/c0+F/595o/f/u+/v/uPVfOHrgf8114L+L33f+H/j997/s98V/O95Hvu/5UHI/5wzLv+dtuv+3/Vy23+cmuvvRvw/9H/h/6U/qv53fgj93/DM93/VA/f/Jbcd/xdctfmffcH8v+kaqf/bnqMZAnJ22RW///v+zP0/52f+/76vUXADah97gQ3IvWz1P/OcKRgrLZNG9c6K+1P+z3lZDrcZhFtfB4E1V9xz/V980wFsQNoZ4/9xJ/T/W62T+d9+oh1hQOm5iP8bPvX9b30ZCMfozgfZnAjUHHlM+7/HdpX/NkusEQYknHACuwCkAWQrzGYQBmkGYZjtQYc0/1tvlv0vN1kSYUDkYdv/Ex6lgG1DxjDNyLb77FcDB6T0BAmEATGHvP933ouCa4BhUKiDMLJmj70q/7Xmi/63mm2JMKD7Yis4nmGhjIzjoJoDD2qANYNsl53C+79lbwvCAFCUGG5QBfsTZBsMgzSC/AyzGeR3ldlC/yXbRDFT5bon68DxC9OErNFpjyLYZpBmgWbO/+uurcOeGtc8WPNfbr44OJ6dtyuCMUij1kJRsLPFW0Vwa4aBF19f/K/eW/3faLbBf9FuATA2mqz/v2ZrzX+QHNVzLwAjy7LsIPIjLQAAAABJRU5ErkJggg==\">';
				            }
                            
                            $tabla.= ',"<input style=\"width:70px;'.$style.'\" type=\"text\" id=\"'.$i++.'lectura\" tabindex=\"'.$i++.'\"   value=\"'.$obj1[0]['lecturao2'].'\" class=\"TabOnEnter\" ><label id=\"'.$i++.'etiquetalectura\"><label>"';
                            $tabla.= ',"<input style=\"width:50px;'.$style.'\" type=\"text\"  id=\"'.$i++.'codigoobservacion\"   tabindex=\"'.$i++.'\"  placeholder=\"Codigo\" value=\"'.$obj1[0]['codigo1'].'\" class=\"TabOnEnter\" ><label id=\"'.$i++.'etiquetacodigo\"><label>"';
                        	/*
                            $tabla.= ',"'.$obj1[0]['montoconsumo'].'"';  
                            */
                            $tabla.= ',"<label  readonly type=\"text\" id=\"'.$i++.'montoconsumo\"      >'.$obj1[0]['montoconsumo'].'</label>"';
                            
                            $tabla.= ',"<label readonly type=\"text\"  id=\"'.$i++.'resultadoevaluacion\"      >'.$icon.'</label>"';
                            $tabla.= ',"'.$obj1[0]['promedio'].'"';

                            $tabla.=']';  
                                if ($j<count($listaDigitarLecturas)-1){
                                        $tabla.=',';
                                    }
                            $j=$j+1;
                        };

$vari='{
    
"aaData":[';
        $vari.=$tabla;
        $vari.=']}';
        echo $vari;exit;
    }
	
	public function saveregistarlecturista(){
        set_time_limit(0);
		$this->layout = 'ajax';
        $tipolectura = $this->request->data['tipolectura'];
		$lectura = $this->request->data['lectura'];
		$codigo= $this->request->data['codigo'];
        $codigoobservacion= $this->request->data['codigoobservacion'];
                
                
        if (empty($lectura) ){
            $lectura='NULL';
        }
        if (empty($codigoobservacion) ){
            $codigoobservacion='NULL';
        }
                
        if ($codigoobservacion != 'NULL'){
            $validarCodigoObservacion = $this->ComlecOrdenlectura->validarCodigoObservacion($codigoobservacion);
          
              
            if ($validarCodigoObservacion){
                 $descripcioncodigo=$validarCodigoObservacion[0][0]['descripcion'];
                 $validarCodigoObservacion=$validarCodigoObservacion[0][0]['condicional'];

                if ($validarCodigoObservacion==1 && $lectura==0 ){
     
                    echo json_encode(array('save' => true,'mensaje'=>'La lectura es obligatoria','validacionlectura'=>true,'validacioncodigo'=>false,'etiquetacodigo'=>$descripcioncodigo,'montoconsumo'=>'','resultadoevaluacion'=>''));
                    exit();
                }
                if ($validarCodigoObservacion==2 && $lectura!= 'NULL' ){
                    echo json_encode(array('save' => true,'mensaje'=>'Este codigo no permite ingresar una lectura','validacionlectura'=>true,'validacioncodigo'=>false,'etiquetacodigo'=>$descripcioncodigo,'montoconsumo'=>'','resultadoevaluacion'=>''));
                    exit();
                }
            }
            else
            {
                    echo json_encode(array('save' => true,'mensaje'=>'Codigo no Existe','validacionlectura'=>false,'validacioncodigo'=>true,'etiquetacodigo'=>'Codigo No Existe','montoconsumo'=>'','resultadoevaluacion'=>''));
                  exit();
            }
        
            
        } else {
            $descripcioncodigo='';
        }
                
        $this->loadModel('Usuario');
                
		$guardar= $this->ComlecOrdenlectura->saveregistarlecturista($tipolectura,$lectura,$codigo,$codigoobservacion,$this->obj_logged_user['Usuario']['id']);
        if($guardar){
            $listaDigitarLecturaIndividual = $this->ComlecOrdenlectura->getLecturaById($tipolectura,$codigo);
            $montoconsumo=$listaDigitarLecturaIndividual[0][0]['montoconsumo1'];
            $resultadoevaluacion=$listaDigitarLecturaIndividual[0][0]['resultadoevaluacion'];
        }
        echo json_encode(array('save' => true,'mensaje'=>'false','validacionlectura'=>false,'validacioncodigo'=>false,'etiquetacodigo'=>$descripcioncodigo,'montoconsumo'=>$montoconsumo,'resultadoevaluacion'=>$resultadoevaluacion));
		exit;
	}
        
        
    public function listarXml($criterio=null,$date_start=null,$date_end=null,$sector=null){ 
    	
    	$this->loadModel('Usuario');
    	$grupo_id= $this->Usuario->IdGrupoByUsuario($this->obj_logged_user['Usuario']['id']);
    	
    	if(!isset($criterio)){
    		$criterio = 'F';
    	}
    	if(!isset($date_start)){
    		$date_start = date('Y-m-d', strtotime("-1 day"));
    	}
    	if(!isset($date_end)){
    		$date_end = date('Y-m-d');
    	}
     
        $listarXml = $this->ComlecOrdenlectura->listarXml($grupo_id,$criterio,$date_start,$date_end,$sector);
     //   var_dump($listarXml);exit;

                         $tabla='';
                        $j=0;


                        foreach ($listarXml as $obj1){ 
                             $tabla.='[';
                           //   $i=0;

                            $tabla.= '"'.$obj1[0]['id'].'"';
                            $tabla.= ',"'.$obj1[0]['pfactura'].'"';
                            $tabla.= ',"'.$obj1[0]['sectores'].'"';
                            $tabla.= ',"'.$obj1[0]['vdescripcion'].'"'; 
                            $tabla.= ',"'.$obj1[0]['fechaimportacion'].'"';
                            $tabla.= ',"'.$obj1[0]['fechaexportacion'].'"'; 
                            $tabla.= ',"Total:'.$obj1[0]['numordenestotal'].'<br>';  
                            $tabla.= 'Validadas:'.$obj1[0]['numordenesvalidadas'].'"';  
                            $tabla.= ',"Total:'.$obj1[0]['exportadastotal'].'<br>';  
                            $tabla.= 'Validadas:'.$obj1[0]['exportadasvalidadas'].'<br>';
                            $tabla.= 'Enviadas a Distriluz:'.$obj1[0]['count_enviadas_distriluz'].'"';
                            
                            
                            //$tabla.= ',"Consistentes:'.$obj1[0]['consistentes'].'<br>';  
                            //$tabla.= 'Forzadas:'.$obj1[0]['forzadas'].'"';  
                            
                            //$tabla.= ',"Lecturas:'.$obj1[0]['l_x_procesar'].'<br>';  
                            //$tabla.= 'Relecturas:'.$obj1[0]['r_x_procesar'].'<br>';  
                            //$tabla.= 'RL Lecturas:'.$obj1[0]['rl_x_procesar'].'<br>"';  
                            
                            //$tabla.= ',"'.$obj1[0]['l_inconsistentes_no_procesadas'].'"'; 

		                    //$tabla.= ',"<a class=\'btn btn-info btn-sm btn fancybox fancybox.iframe\' href=\"'.ENV_WEBROOT_FULL_URL.'/ComlecOrdenlecturas/ajax_resumen_xml/'.$obj1[0]['id'].'\">Exportar</a><a onclick=\"desabilitar('.$obj1[0]['id'].')\" id=\"btn_exportar'.$obj1[0]['id'].'\"   href=\"exportarlecturas?id='.$obj1[0]['id'].'\" disabled=\"true\"  class=\"btn btn-info\">Exportar</a><div class=\"form-group\"><div class=\"col-sm-6\"><div class=\"input-group date datetime col-md-9 col-xs-7\" data-min-view=\"2\" data-date-format=\"yyyy-mm-dd\"><input placeholder=\"Fecha\"  value=\"'.date("d/m/Y").'\" class=\"form-control\" size=\"16\" type=\"text\" style=\"width:90px;\" onkeyup=\"if(event.keyCode == 13) exportar_fecha('.$obj1[0]['id'].')\" id=\"input_exportar'.$obj1[0]['id'].'\"  name=\"input_exportar'.$obj1[0]['id'].'\"  ></div></div></div>"';
                            $tabla.= ',"<a class=\'btn btn-info btn-sm btn fancybox fancybox.iframe\' href=\"'.ENV_WEBROOT_FULL_URL.'/ComlecOrdenlecturas/ajax_resumen_xml/'.$obj1[0]['id'].'\">Exportar</a>"';
                    
                            $tabla.=']';  
                                if ($j<count($listarXml)-1){
                                        $tabla.=',';
                                    }
                            $j=$j+1;
                        };

$vari='{
    
"aaData":[';
        $vari.=$tabla;
        $vari.=']}';
        echo $vari;exit;
    }
    
    public function ajax_resumen_xml($id_xml){
    	$this->layout = 'home_tree';
    	
    	$obj_xml = $this->ComlecOrdenlectura->listarXMLid($id_xml);
    	$cantidad_suministros = $this->ComlecOrdenlectura->getCountSuministrosByXml($id_xml);
    	$cantidad_asignados = $this->ComlecOrdenlectura->getCountAsignadosByXml($id_xml);
    	$cantidad_descargados = $this->ComlecOrdenlectura->getCountDescargadosByXml($id_xml);
    	$cantidad_finalizados = $this->ComlecOrdenlectura->getCountFinalizadosByXml($id_xml);
    	$cantidad_consistentes = $this->ComlecOrdenlectura->getCountConsistentesByXml($id_xml);
    	$cantidad_inconsistentes = $this->ComlecOrdenlectura->getCountInconsistentesByXml($id_xml);
    	$cantidad_enviadas_distriluz = $this->ComlecOrdenlectura->getCountEnviadasDistriluzByXml($id_xml);
    	//$cantidad_sin_lectura_y_obs = $this->ComlecOrdenlectura->getCountSinLecturayObsByXml($id_xml);
    	$cantidad_sin_lectura_y_obs = 0;
    	$cantidad_obs_sin_lectura = $this->ComlecOrdenlectura->getCountObs1SinLecturaByXml($id_xml);
    	$cantidad_obs_con_lectura = $this->ComlecOrdenlectura->getCountObs1ConLecturaByXml($id_xml);
    	$cantidad_consumo_cero = $this->ComlecOrdenlectura->getCountConsumoCeroByXml($id_xml);
    	$cantidad_consumo_menor35 = $this->ComlecOrdenlectura->getCountConsumoMenoresQueByXml($id_xml);
    	$cantidad_consumo_mayor100_porciento = $this->ComlecOrdenlectura->getCountConsumoMayor100PorcientoQueAnteriorByXml($id_xml);
    	$cantidad_consumo_mayor1000 = $this->ComlecOrdenlectura->getCountConsumoMayoresQueByXml($id_xml);
    	$cantidad_obs_99 = $this->ComlecOrdenlectura->getCountObs1IgualAByXml($id_xml,99);
    	
    	$this->set(compact('id_xml','obj_xml','cantidad_suministros','cantidad_asignados','cantidad_descargados','cantidad_finalizados',
        'cantidad_consistentes','cantidad_inconsistentes','cantidad_enviadas_distriluz','cantidad_sin_lectura_y_obs','cantidad_obs_sin_lectura','cantidad_obs_con_lectura',
        'cantidad_consumo_cero','cantidad_consumo_menor35','cantidad_consumo_mayor100_porciento','cantidad_consumo_mayor1000',
        'cantidad_obs_99'));
    }
    
    /**
    * Exporta xml y cierra proceso
    **/
    public function exportarlecturas() {
        $id= $_REQUEST['id'];
        $fechaexportacion= $_REQUEST['fechaexportacion'];
        $listarXMLid = $this->ComlecOrdenlectura->listarXMLid($id);
        $negocio = $this->Session->read('NEGOCIO');
        if($negocio=='TAC'){
        	$exportarlecturas = $this->ComlecOrdenlectura->exportarlecturasTac($id,$fechaexportacion);
        }
        else
        {
        	$exportarlecturas = $this->ComlecOrdenlectura->exportarlecturasCix($id,$fechaexportacion);
        }
            
        
        
        
        $saveExportarXML = $this->ComlecOrdenlectura->saveExportarXML(count($exportarlecturas),$listarXMLid[0][0]['salidavalidada'],$id);
        
        
$exportar='<?xml version="1.0" standalone="yes"?>
<DocumentElement>';

 foreach ($exportarlecturas as $obj1){ 
 	if($negocio=='TAC'){
 		if((int) $obj1[0]['lecturao'] == 0){
 			$lecturao = '<LECTURA/>';
 		}else{
 			$lecturao = '<LECTURA>'.(int) $obj1[0]['lecturao'].'.0000</LECTURA>';
 		}
 	}else{
 		if($obj1[0]['lecturao'] == ''){
 			$lecturao = '<LecturaO/>';
 		}else{
 			$lecturao = '<LecturaO>'.(int) $obj1[0]['lecturao'].'.0000</LecturaO>';
 		}
 	}
    
    if($obj1[0]['codigo1'] == '0' || $obj1[0]['codigo1'] == null){
      $codlectura = '<CodLectura/>';
    }else{
      $obj1[0]['codigo1'] = $obj1[0]['codigo1'];
      if ($obj1[0]['codigo1']=='0'){
          $codlectura = '<CodLectura/>';
      }
      else {
            $codlectura = '<CodLectura>'.$obj1[0]['codigo1'].'</CodLectura>';
      }
    
    }

  // $fecha_lectura = '05/29/2014';

    if($negocio=='TAC'){
    	$exportar.='
  <Datos>
      <LOCALIDAD>'.$obj1[0]['unidad_neg'].'</LOCALIDAD>
      <ZONA>'.$obj1[0]['sector'].'</ZONA>
      <LIBRO>'.trim($obj1[0]['ruta']).'</LIBRO>
      <SUMINISTRO>'.trim($obj1[0]['lcorrelati']).'</SUMINISTRO>
      <CONTRATO>'.trim($obj1[0]['suministro']).'</CONTRATO>
      <NOMBRES>'.trim($obj1[0]['nombre']).'</NOMBRES>
      <DIRECCION>'.trim($obj1[0]['direccion']).'</DIRECCION>
      <TARIFA>'.$obj1[0]['tarifa'].'</TARIFA>
      <MEDIDOR>'.trim($obj1[0]['seriefab']).'</MEDIDOR>
      <DMEDIDOR>'.$obj1[0]['digitos'].'</DMEDIDOR>
      <LECTURAA>'.$obj1[0]['lecant'].'</LECTURAA>
      <PM>'.$obj1[0]['promedio'].'</PM>
      <FMEDIDOR>'.$obj1[0]['factor'].'</FMEDIDOR>
      '.$lecturao.'
      '.$codlectura.'
      <FECHALEC>'.$obj1[0]['flectura'].'</FECHALEC>
      <HORALEC>'.$obj1[0]['hlectura'].'</HORALEC>
      <PFACTURA>'.$obj1[0]['pfactura'].'</PFACTURA>
      <LECAANT>'.$obj1[0]['lecaant'].'</LECAANT>
      <CONSANT>'.$obj1[0]['consant'].'</CONSANT>
      <CONSRET>'.$obj1[0]['consret'].'</CONSRET>
      <ORDTRABAJO>'.$obj1[0]['ordtrabajo'].'</ORDTRABAJO>
      <IDCICLO>'.$obj1[0]['idciclo'].'</IDCICLO>
 </Datos>';
    }else{
    	$exportar.='
  <Datos>
    <PFactura>'.$obj1[0]['pfactura'].'</PFactura>
    <Unidad_Neg>'.$obj1[0]['unidad_neg'].'</Unidad_Neg>
    <Sector>'.$obj1[0]['sector'].'</Sector>
    <Ruta>'.trim($obj1[0]['ruta']).'</Ruta>
    <LCorrelati>'.trim($obj1[0]['lcorrelati']).'</LCorrelati>
    <Suministro>'.trim($obj1[0]['suministro']).'</Suministro>
    <Concepto>'.$obj1[0]['concepto'].'</Concepto>
    <Concepto_D>'.$obj1[0]['concepto_d'].'</Concepto_D>
    <Marca>'.$obj1[0]['marca'].'</Marca>
    <Modelo>'.$obj1[0]['modelo'].'</Modelo>
    <SerieFab>'.trim($obj1[0]['seriefab']).'</SerieFab>
    <Digitos>'.$obj1[0]['digitos'].'</Digitos>
    <Nombre>'.trim($obj1[0]['nombre']).'</Nombre>
    <Direccion>'.trim($obj1[0]['direccion']).'</Direccion>
    <DireccionR>'.trim($obj1[0]['direccionr']).'</DireccionR>
    '.$lecturao.'
    <OrdTrabajo>'.$obj1[0]['ordtrabajo'].'</OrdTrabajo>
    <Cartera>'.$obj1[0]['cartera'].'</Cartera>
    <FLectura>'.$obj1[0]['flectura'].'</FLectura>
          '.$codlectura.'
    <Factor>'.$obj1[0]['factor'].'</Factor>
    <LecAAnt>'.$obj1[0]['lecaant'].'</LecAAnt>
    <LecAnt>'.$obj1[0]['lecant'].'</LecAnt>
    <Promedio>'.$obj1[0]['promedio'].'</Promedio>
    <ConsRet>'.$obj1[0]['consret'].'</ConsRet>
    <ConsAnt>'.$obj1[0]['consant'].'</ConsAnt>
    <DemandaAct>'.$obj1[0]['demandaact'].'</DemandaAct>
    <TConexion>'.$obj1[0]['tconexion'].'</TConexion>
    <IdCiclo>'.$obj1[0]['idciclo'].'</IdCiclo>
    <NombCiclo>'.$obj1[0]['nombciclo'].'</NombCiclo>
    <NombRuta>'.$obj1[0]['nombruta'].'</NombRuta>
    <PInicio>'.$obj1[0]['pinicio'].'</PInicio>
    <Tarifa>'.$obj1[0]['tarifa'].'</Tarifa>
    <ZonaOpt>'.$obj1[0]['zonaopt'].'</ZonaOpt>
    <SectorOpt>'.$obj1[0]['sectoropt'].'</SectorOpt>
  </Datos>';
    }
     
 }
$exportar.='</DocumentElement>';
        $filename=$listarXMLid[0][0]['vdescripcion'];
        header("Content-type: text/xml");
        header('Content-Disposition: attachment; filename="'.$filename);
        // print out XML that describes the schema 
        echo $exportar; 
       // echo $uno;
        
       
        
        exit;
    }

    /**
    * Solo Exporta xml
    **/
    public function exportarlecturas_xml() {
        $id= $_REQUEST['id'];
        $fechaexportacion= date('Y-m-d H:i:s');
        $listarXMLid = $this->ComlecOrdenlectura->listarXMLid($id);
        $negocio = $this->Session->read('NEGOCIO');
        if($negocio=='TAC'){
            $exportarlecturas = $this->ComlecOrdenlectura->exportarlecturasTac($id,$fechaexportacion);
        }else{
            $exportarlecturas = $this->ComlecOrdenlectura->exportarlecturasCix($id,$fechaexportacion);
        }
        
        
$exportar='<?xml version="1.0" standalone="yes"?>
<DocumentElement>';

 foreach ($exportarlecturas as $obj1){ 
    if($negocio=='TAC'){
        if((int) $obj1[0]['lecturao'] == 0){
            $lecturao = '<LECTURA/>';
        }else{
            $lecturao = '<LECTURA>'.(int) $obj1[0]['lecturao'].'.0000</LECTURA>';
        }
    }else{
        if($obj1[0]['lecturao'] == ''){
            $lecturao = '<LecturaO/>';
        }else{
            $lecturao = '<LecturaO>'.(int) $obj1[0]['lecturao'].'.0000</LecturaO>';
        }
    }
    
    if($obj1[0]['codigo1'] == '0' || $obj1[0]['codigo1'] == null){
      $codlectura = '<CodLectura/>';
    }else{
      $obj1[0]['codigo1'] = $obj1[0]['codigo1'];
      if ($obj1[0]['codigo1']=='0'){
          $codlectura = '<CodLectura/>';
      }
      else {
            $codlectura = '<CodLectura>'.$obj1[0]['codigo1'].'</CodLectura>';
      }
    
    }

  // $fecha_lectura = '05/29/2014';

    if($negocio=='TAC'){
        $exportar.='
  <Datos>
      <LOCALIDAD>'.$obj1[0]['unidad_neg'].'</LOCALIDAD>
      <ZONA>'.$obj1[0]['sector'].'</ZONA>
      <LIBRO>'.trim($obj1[0]['ruta']).'</LIBRO>
      <SUMINISTRO>'.trim($obj1[0]['lcorrelati']).'</SUMINISTRO>
      <CONTRATO>'.trim($obj1[0]['suministro']).'</CONTRATO>
      <NOMBRES>'.trim($obj1[0]['nombre']).'</NOMBRES>
      <DIRECCION>'.trim($obj1[0]['direccion']).'</DIRECCION>
      <TARIFA>'.$obj1[0]['tarifa'].'</TARIFA>
      <MEDIDOR>'.trim($obj1[0]['seriefab']).'</MEDIDOR>
      <DMEDIDOR>'.$obj1[0]['digitos'].'</DMEDIDOR>
      <LECTURAA>'.$obj1[0]['lecant'].'</LECTURAA>
      <PM>'.$obj1[0]['promedio'].'</PM>
      <FMEDIDOR>'.$obj1[0]['factor'].'</FMEDIDOR>
      '.$lecturao.'
      '.$codlectura.'
      <FECHALEC>'.$obj1[0]['flectura'].'</FECHALEC>
      <HORALEC>'.$obj1[0]['hlectura'].'</HORALEC>
      <PFACTURA>'.$obj1[0]['pfactura'].'</PFACTURA>
      <LECAANT>'.$obj1[0]['lecaant'].'</LECAANT>
      <CONSANT>'.$obj1[0]['consant'].'</CONSANT>
      <CONSRET>'.$obj1[0]['consret'].'</CONSRET>
      <ORDTRABAJO>'.$obj1[0]['ordtrabajo'].'</ORDTRABAJO>
      <IDCICLO>'.$obj1[0]['idciclo'].'</IDCICLO>
 </Datos>';
    }else{
        $exportar.='
  <Datos>
    <PFactura>'.$obj1[0]['pfactura'].'</PFactura>
    <Unidad_Neg>'.$obj1[0]['unidad_neg'].'</Unidad_Neg>
    <Sector>'.$obj1[0]['sector'].'</Sector>
    <Ruta>'.trim($obj1[0]['ruta']).'</Ruta>
    <LCorrelati>'.trim($obj1[0]['lcorrelati']).'</LCorrelati>
    <Suministro>'.trim($obj1[0]['suministro']).'</Suministro>
    <Concepto>'.$obj1[0]['concepto'].'</Concepto>
    <Concepto_D>'.$obj1[0]['concepto_d'].'</Concepto_D>
    <Marca>'.$obj1[0]['marca'].'</Marca>
    <Modelo>'.$obj1[0]['modelo'].'</Modelo>
    <SerieFab>'.trim($obj1[0]['seriefab']).'</SerieFab>
    <Digitos>'.$obj1[0]['digitos'].'</Digitos>
    <Nombre>'.trim($obj1[0]['nombre']).'</Nombre>
    <Direccion>'.trim($obj1[0]['direccion']).'</Direccion>
    <DireccionR>'.trim($obj1[0]['direccionr']).'</DireccionR>
    '.$lecturao.'
    <OrdTrabajo>'.$obj1[0]['ordtrabajo'].'</OrdTrabajo>
    <Cartera>'.$obj1[0]['cartera'].'</Cartera>
    <FLectura>'.$obj1[0]['flectura'].'</FLectura>
          '.$codlectura.'
    <Factor>'.$obj1[0]['factor'].'</Factor>
    <LecAAnt>'.$obj1[0]['lecaant'].'</LecAAnt>
    <LecAnt>'.$obj1[0]['lecant'].'</LecAnt>
    <Promedio>'.$obj1[0]['promedio'].'</Promedio>
    <ConsRet>'.$obj1[0]['consret'].'</ConsRet>
    <ConsAnt>'.$obj1[0]['consant'].'</ConsAnt>
    <DemandaAct>'.$obj1[0]['demandaact'].'</DemandaAct>
    <TConexion>'.$obj1[0]['tconexion'].'</TConexion>
    <IdCiclo>'.$obj1[0]['idciclo'].'</IdCiclo>
    <NombCiclo>'.$obj1[0]['nombciclo'].'</NombCiclo>
    <NombRuta>'.$obj1[0]['nombruta'].'</NombRuta>
    <PInicio>'.$obj1[0]['pinicio'].'</PInicio>
    <Tarifa>'.$obj1[0]['tarifa'].'</Tarifa>
    <ZonaOpt>'.$obj1[0]['zonaopt'].'</ZonaOpt>
    <SectorOpt>'.$obj1[0]['sectoropt'].'</SectorOpt>
  </Datos>';
    }
     
 }
$exportar.='</DocumentElement>';
        $filename=$listarXMLid[0][0]['vdescripcion'];
        header("Content-type: text/xml");
        header('Content-Disposition: attachment; filename="'.$filename);
        // print out XML that describes the schema 
        echo $exportar; 
       // echo $uno;
        
       
        
        exit;
    }
    
    /**
     * Reiniciar envio de lecturas a distriluz
     **/
    public function reiniciar_envio_distriluz() {
    	$id= $_REQUEST['id'];
    	$listarXMLid = $this->ComlecOrdenlectura->listarXMLid($id);
    	if(isset($listarXMLid[0][0]['fechaexportacion'])){
    		$this->ComlecOrdenlectura->quitarFechaExportacionXML($id);
    	}
    	exit;
    }

    /**
    * Listado de digitacion de lecturas
    * @return view
    * @author Geynen
    * @version 11 Junio 2014
    **/
	public function listar_digitar_lecturas($tipolectura_id,$sector_id,$ruta_id,$lecturista_id,$lectespecial,$search_suministro) {
        $this->autoRender = false;  
        ini_set('memory_limit', '-1');
        ini_set('max_execution_time', 300000);
        set_time_limit(0);
         
        /*
         * Paging
        */
        if ( isset( $_GET['iDisplayStart'] ) && $_GET['iDisplayLength'] != '-1' )
        {
            $page = intval( $_GET['iDisplayStart'] );
            $limit = intval( $_GET['iDisplayLength'] );
        }
    
        $search = "";
        if ( isset($_GET['sSearch']) && $_GET['sSearch'] != "" ){
            $search = $_GET['sSearch'];
        }
        $negocio = $this->Session->read('NEGOCIO');

        $this->loadModel('ComlecOrdenlectura');
        if($search_suministro != '0'){
        	$this->ComlecOrdenlectura->setDataSource (strtolower($negocio));
        	$obj_suministro = $this->ComlecOrdenlectura->findBySuministro($search_suministro);
        	if(isset($obj_suministro['ComlecOrdenlectura']['orden'])){
        		$suministro_orden = $obj_suministro['ComlecOrdenlectura']['orden'];
        		$sector_id = $obj_suministro['ComlecOrdenlectura']['sector'];
        		$ruta_id = $obj_suministro['ComlecOrdenlectura']['ruta'];
        	}else{
        		$suministro_orden = '0';
        	}
        }else{
        	$suministro_orden = '';
        }
        
        if($negocio=='TAC'){
        	$arr_obj_incosistencia = $this->ComlecOrdenlectura->listaDigitarLectura2Tac($tipolectura_id,$sector_id,$ruta_id,$lecturista_id,$lectespecial,$suministro_orden,$page,$limit,$search);
        }else{
        	$arr_obj_incosistencia = $this->ComlecOrdenlectura->listaDigitarLectura2Cix($tipolectura_id,$sector_id,$ruta_id,$lecturista_id,$lectespecial,$suministro_orden,$page,$limit,$search);
        }
        $i=1;
        $j=1;
        $tabla='';
        $total_records=0;
        
        foreach ($arr_obj_incosistencia as $obj1){
            $tabla.='[';
            $tabla.= '"<label class=\"hidden\" readonly type=\"text\" id=\"'.$i.'codigo\" placeholder=\"Codigo\">'.$obj1[0]['comlec_ordenlectura_id'].'</label>"';
            if($negocio=='TAC'){
            	$tabla.= ',"'.$obj1[0]['lcorrelati'].'"';
			}else{
				$tabla.= ',"'.$obj1[0]['orden'].'"';
			}
			$cliente = preg_replace('([^A-Za-z0-9])', '', trim($obj1[0]['cliente']));
            //$tabla.= ',"<a href=\"javascript:void(0);\" class=\"btn-tooltip\" data-toggle=\"tooltip\" data-placement=\"bottom\" title=\"'.$obj1[0]['lecturista'].'\">'.$obj1[0]['libro'].'</a>"';
            //$tabla.= ',"'.$cliente.'"';
            //$tabla.= ',"'.$obj1[0]['seriefab'].'"';
            //$tabla.= ',"'.$obj1[0]['suministro'].'"';
            $tabla.= ',"<a href=\"javascript:void(0);\" data-popover=\"popover\" data-original-title=\"Suministro: '.$obj1[0]['suministro'].'\" data-content=\"Medidor: '.$obj1[0]['seriefab'].'<br>Cliente: '.$cliente.'<br>Lecturista: '.$obj1[0]['lecturista'].'<br>Libro: '.$obj1[0]['libro'].'\" data-placement=\"bottom\" data-trigger=\"hover\">'.$obj1[0]['suministro'].'</a>"';
            $tabla.= ',"'.str_replace('\\','/',trim($obj1[0]['direccion'])).'"';    
            $tabla.= ',"'.number_format($obj1[0]['lecantant']).'"';
            $tabla.= ',"'.number_format($obj1[0]['lecant']).'"';

            $style='';
            if (isset($obj1[0]['lecturao2'])){
                $style='background:#666;color:#fff;';
            }
            
            if($obj1[0]['libro']==''){
            	$input_disabled = 'disabled';
            	$msg_no_asignado = '<a class=\"label label-warning\"><i class=\"fa fa-warning\"></i> No Asignado</a>';
            }else{
            	$input_disabled = '';
            	$msg_no_asignado = '';
            }
            
            $icon = '';
            $icon_estado_ws = '';
               
            if ($obj1[0]['resultadoevaluacion']=='INCONSISTENTE'){
            	$style='background:red;color:#fff;';
            	$icon = '<img title= \"INCONSISTENTE\" src=\"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAABrElEQVR42q2TyytEURzH/QFW1JRHIxsTUiNWHgtCFOWRPEpZyAJZkKSZhccgsVBIFqRkZdKUZCFFNLEQJUnJQk0eGcQCk+bj/O6dyZ3rsXLr272dcz7f3/d3zrlRUf/98PIIK1Mw0gzOKnCU6+/FUYJPD/wNH23DcB14F+DcA7sz4B5ScK8+XmuDvXV+h6da4GkfzpZhywWePljqhLlWmGiEwWoCFdbvJlpsNcm1MjmZVwnGfzaQdpT8RdbIdrSe16fhcFEHRWsOtRfdsNCuJ5MWxKCrgMeadPzTowYDp5o8dutQWAKb4tNTqhm8NmdxWZVnMFCDbM3ogFECTzZFVKctl7eGDE6zEwwGalDbbQHCkthSOQyHqstaaeHYHmcw6CjSj0oAoyS2CaY1i6vCJI7Kcr4MgrMD+mK5MEYJaIKlusS/GHMZDNSRvFWmfFUzgyE4UJ/GZW48ezYLAf+96S5srvJckhgJhUCRFBDYmxyNf83z820Mbrjx5cVpMeWoZLfl26cujsTeSbH8Doefj7sbfCMOToozOUyN5cAWw0G+nbN+J+93t/z73/sJB/zttLkU5gUAAAAASUVORK5CYII=\">';
            }
            	
            if ($obj1[0]['resultadoevaluacion']=='CONSISTENTE'){
            	$icon = '<img title= \"CONSISTENTE\" src=\"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAB8ElEQVR42mNgoDb4/OfD/9UPpv6vv5z4v+C83/+sM17/c0+F/595o/f/u+/v/uPVfOHrgf8114L+L33f+H/j997/s98V/O95Hvu/5UHI/5wzLv+dtuv+3/Vy23+cmuvvRvw/9H/h/6U/qv53fgj93/DM93/VA/f/Jbcd/xdctfmffcH8v+kaqf/bnqMZAnJ22RW///v+zP0/52f+/76vUXADah97gQ3IvWz1P/OcKRgrLZNG9c6K+1P+z3lZDrcZhFtfB4E1V9xz/V980wFsQNoZ4/9xJ/T/W62T+d9+oh1hQOm5iP8bPvX9b30ZCMfozgfZnAjUHHlM+7/HdpX/NkusEQYknHACuwCkAWQrzGYQBmkGYZjtQYc0/1tvlv0vN1kSYUDkYdv/Ex6lgG1DxjDNyLb77FcDB6T0BAmEATGHvP933ouCa4BhUKiDMLJmj70q/7Xmi/63mm2JMKD7Yis4nmGhjIzjoJoDD2qANYNsl53C+79lbwvCAFCUGG5QBfsTZBsMgzSC/AyzGeR3ldlC/yXbRDFT5bon68DxC9OErNFpjyLYZpBmgWbO/+uurcOeGtc8WPNfbr44OJ6dtyuCMUij1kJRsLPFW0Vwa4aBF19f/K/eW/3faLbBf9FuATA2mqz/v2ZrzX+QHNVzLwAjy7LsIPIjLQAAAABJRU5ErkJggg==\">';
            	if($obj1[0]['ws_l_estado']=='1'){
            		$icon_estado_ws = '<img title= \"Enviado a Distriluz\" src=\"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAACpElEQVR42mNgQAP/79dzFG92EJl9JME1d2t6c9iOqtmRu2a0BV3Y46i9qk8o9/Z/dgZc4O05f63OnQELnJZa/Weab/yfYR4QzzH8zzDX4D/DQuv/8uvK/1vvXznd98Z3RQzNz856uCettXnMPgeoaSYOPEP/P8tCl//S2xY89Lr7RRfh7MvemnVb7V8zzsSjGY6NgNjwv9aJA18Ztt1mZ/h/O5d9+8nAJVLTdAhqZpvn+IdztvFXlplAL80O/h/+8N0ihlf3QyVm7LAAOs8EbDLDLKDkDDTN03T/i6wt/R/64GfX96uuMe6LjN4wTFX+L37ywn+GV8/TQ0LmaEE0z/H+r7Wn7WzJeuufDNON/oO9NFXrv8Leaf89b76PA3v3XYpe8Wa7x0zTtf8zLG7/z/DicXq+zlSQAbb/jY9uuB769J3cvXsx+n2b7f4wTNX7r7R3JlDzY0+w5tdpkrN3Oz5iA4eDPtC1yUADXmQk2c0AGjDD8L/a3hV/HS6+0AEpfnEnVmfSka7Lbne+BoCj+Gmi7LJ9zh8ZphpCvaYHjObi/wxv3iSbVa/ShwhO1f2vvnfR/+D7/91REteDUMveHfbvEZqBeLrmf/ZNm/4DYyGab9MR99sMU/SgEgb/JdbV/454+j8bkjLD/dPWWb9gmGGEGpVTjf7bXn32mOH///+MV855RcatNUUKfdP/fMuyvlRfXnk4bLnJUxb0KJ2m/l9o76r/0S8/WUOc+H8R994DTpOM5ur/RSg0BKc8rAlpacF/t5u3G1b9/8+M8Of/I7w7dtu1OC63fcMzB5RQgIpnGUFTngE4fTAtdP0vtqb8reWFKxVAl7NhzVC3z0ZbxKyyyfdZ47fDZKnnXY2lAW+016Q9N91cd0BydX1K9N2PpgzUBADlgqaJmthzQAAAAABJRU5ErkJggg==\">';
            		$input_disabled = 'disabled';
            	}else{
            		$icon_estado_ws .= '';
            	}
            }

            if($tipolectura_id=='L'){
                $tabla.= ',"<input '.$input_disabled.' style=\"width:70px;'.$style.'\" type=\"text\" id=\"'.$i.'lectura\" tabindex=\"'.$j++.'\" placeholder=\"LecturaO\" value=\"'.$obj1[0]['lecturao1'].'\" class=\"TabOnEnter\" ><label id=\"'.$i.'etiquetalectura\"><label>"';
                $tabla.= ',"<input '.$input_disabled.' style=\"width:50px;'.$style.'\" type=\"text\" id=\"'.$i.'codigoobservacion\" tabindex=\"'.$j++.'\" data-index=\"'.$i.'\" placeholder=\"Codigo\" value=\"'.$obj1[0]['obs1'].'\" class=\"TabOnEnter\" ><label id=\"'.$i.'etiquetacodigo\"><label>"';
                $tabla.= ',"<span id=\"'.$i.'montoconsumo\">'.number_format($obj1[0]['montoconsumo1'],2).'</span>"';
            }elseif($tipolectura_id=='R'){
                $tabla.= ',"<input '.$input_disabled.' style=\"width:70px;'.$style.'\" type=\"text\" id=\"'.$i.'lectura\" tabindex=\"'.$j++.'\" placeholder=\"LecturaO\" value=\"'.$obj1[0]['lecturao2'].'\" class=\"TabOnEnter\" ><label id=\"'.$i.'etiquetalectura\"><label>"';
                $tabla.= ',"<input '.$input_disabled.' style=\"width:50px;'.$style.'\" type=\"text\" id=\"'.$i.'codigoobservacion\" tabindex=\"'.$j++.'\" data-index=\"'.$i.'\" placeholder=\"Codigo\" value=\"'.$obj1[0]['obs2'].'\" class=\"TabOnEnter\" ><label id=\"'.$i.'etiquetacodigo\"><label>"';
                $tabla.= ',"<span id=\"'.$i.'montoconsumo\">'.number_format($obj1[0]['montoconsumo2'],2).'</span>"';
            }elseif($tipolectura_id=='RR'){
                $tabla.= ',"<input '.$input_disabled.' style=\"width:70px;'.$style.'\" type=\"text\" id=\"'.$i.'lectura\" tabindex=\"'.$j++.'\" placeholder=\"LecturaO\" value=\"'.$obj1[0]['lecturao3'].'\" class=\"TabOnEnter\" ><label id=\"'.$i.'etiquetalectura\"><label>"';
                $tabla.= ',"<input '.$input_disabled.' style=\"width:50px;'.$style.'\" type=\"text\" id=\"'.$i.'codigoobservacion\" tabindex=\"'.$j++.'\" data-index=\"'.$i.'\" placeholder=\"Codigo\" value=\"'.$obj1[0]['obs3'].'\" class=\"TabOnEnter\" ><label id=\"'.$i.'etiquetacodigo\"><label>"';
                $tabla.= ',"<span id=\"'.$i.'montoconsumo\">'.number_format($obj1[0]['montoconsumo3'],2).'</span>"';
            }
            $tabla.= ',"<span id=\"'.$i.'resultadoevaluacion\">'.$icon.'</span>'.$icon_estado_ws.$msg_no_asignado.'"';
            $i++;
            $tabla.= ',"'.number_format($obj1[0]['promedio'],2).'"';
            $tabla.='],';
            $total_records =$obj1[0]['row_total'];
        };
        
        $tabla = substr($tabla,0,-1);

        $vari='{
            "sEcho": "'.intval($_GET['sEcho']).'",
            "iTotalRecords":"'.$total_records.'",
            "iTotalDisplayRecords":"'.$total_records.'",
            "aaData":[';
        $vari.=$tabla;
        $vari.=']}';
        return $vari;       
    }

    /**
    * Listar Lecturistas por Ruta
    * @author Geynen
    * @version 16 Junio 2014
    **/
    public function listarLecturistaPorRuta($tipolectura,$ruta){
        $arr_obj_lecturistas = $this->ComlecOrdenlectura->listarLecturistaPorRuta($tipolectura,$ruta);
        if(count($arr_obj_lecturistas)>0){
            echo '<option value="0">Todos</option>';
            foreach ($arr_obj_lecturistas as $lecturista){
                echo '<option value='.$lecturista['0']["id"].'>'.$lecturista['0']["nombre"].' - '.$lecturista['0']["nomusuario"].'</option>';
            }
        }else{
            echo '<option value="0">Ninguno</option>';
        }
        $this->autoRender = false;
    }

    /**
    * Listar Ciclo por Unidad de negocio
    * @author jcasiano
    * @version 19 Junio 2014
    **/
    public function listarCicloPorUnidadneg($unidadneg){
        $arr_obj_ciclos = $this->ComlecOrdenlectura->listarCicloPorUnidadneg($unidadneg);
        $arr_ciclos_actuales = $this->ComlecOrdenlectura->getCiclosActuales();
        if(count($arr_obj_ciclos)>0){
            echo '<option value="0">Todos</option>';
            foreach ($arr_obj_ciclos as $ciclo){
            	$str_ciclo_actual = '';
            	foreach ($arr_ciclos_actuales as $ca){
            		if($ciclo['0']['ciclo']==$ca['0']['idciclo']){
            			$str_ciclo_actual = ' (Actual)';
            		}
            	}
                echo '<option value='.$ciclo['0']['ciclo'].'>'.$ciclo['0']['ciclo'].' - '.$ciclo['0']['nombciclo'].$str_ciclo_actual.'</option>';
            }    
        }else{
            echo '<option value="0">Ninguno</option>';
        }
        
        $this->autoRender = false;
    }
    
    
    /**
    * Listar Sector por Ciclo
    * @author Geynen
    * @version 19 Junio 2014
    **/
    public function listarSectorPorCiclo($ciclo){
        $arr_obj_sectores = $this->ComlecOrdenlectura->listarSectorPorCiclo($ciclo);
        if(count($arr_obj_sectores)>0){
            echo '<option value="0">Todos</option>';
            foreach ($arr_obj_sectores as $sector){
                echo '<option value='.$sector['0']['sector'].'>'.$sector['0']['sector'].'</option>';
            }    
        }else{
            echo '<option value="0">Ninguno</option>';
        }
        
        $this->autoRender = false;
    }

    /**
    * Listar Rutas por Sector
    * @author Geynen
    * @version 14 Junio 2014
    **/
    public function listarRutasPorSector($sector){
        $arr_obj_rutas = $this->ComlecOrdenlectura->listarRutasPorSector($sector);
        if(count($arr_obj_rutas)>0){
            echo '<option value="0">Todos</option>';
            foreach ($arr_obj_rutas as $ruta){
                echo '<option value='.$ruta['0']['ruta'].'>'.$ruta['0']['ruta'].' - '.$ruta['0']['nombruta'].'</option>';
            }    
        }else{
            echo '<option value="0">Ninguno</option>';
        }
        
        $this->autoRender = false;
    }
    
    
    
    public function passlectura(){
        set_time_limit(0);
        $this->layout = 'ajax';
        $lectura = $this->request->data['lectura'];
        $suministro = $this->request->data['suministro'];
        if (empty($lectura) ){
            $lectura='NULL';
        }

        $passlectura = $this->ComlecOrdenlectura->passlectura($lectura,$suministro);
    
        if($passlectura){
            $montoconsumo=$passlectura[0][0]['montoconsumo1'];
            $resultadoevaluacion=$passlectura[0][0]['resultadoevaluacion'];
            $lectura=$passlectura[0][0]['lecturao1'];
            $ob1=$passlectura[0][0]['obs1'];
        }
        echo json_encode(array('montoconsumo'=>$montoconsumo,'lectura'=>$lectura,'obs1'=>$ob1,'resultadoevaluacion'=>$resultadoevaluacion));
		exit;
	}
	
	public function pdf_libros($libro, $ruta, $sector){
		$this->layout = 'ajax';
		
		$libro = trim(str_replace('libro','', $libro));
		$ruta= trim($ruta);
		$lecturista = $this->ComlecOrdenlectura->listarLecturista($libro, $ruta);
		$this->set(compact('libro','ruta','lecturista','sector'));
	}
	
	public function pdf_listar_libros($libro, $ruta) {
		$this->layout = 'ajax';
		set_time_limit(0);
	
		$arr_obj_incosistencia = $this->ComlecOrdenlectura->listarlibrosPdf($libro, $ruta);
		 
		$j=0;
		$tabla='';
		 
		foreach ($arr_obj_incosistencia as $obj1){
			$tabla.='[';
			//   var_dump(count($conceptosByGrupo));exit;
			$i=0;
			$m=$j+1;
			$cliente = h(trim($obj1[0]['cliente']));
			$tabla.= '"'.$obj1[0]['orden'].'"';
			$tabla.= ',"'.mb_strtoupper(stripslashes(htmlspecialchars($cliente, ENT_QUOTES)), 'UTF-8').'"';
			$tabla.= ',"'.mb_strtoupper(stripslashes(htmlspecialchars($obj1[0]['direccion'], ENT_QUOTES)), 'UTF-8').'"';			
			$tabla.= ',"'.$obj1[0]['seriefab'].'"';
			$tabla.= ',"'.$obj1[0]['suministro'].'"';
			$tabla.= ',"______________________"';
			$tabla.= ',"[____]"';
			$tabla.=']';
			if ($j<count($arr_obj_incosistencia)-1){
				$tabla.=',';
			}
			$j=$j+1;
		};
	
	
		$vari=trim('{"aaData":[');
		$vari.=trim($tabla);
		$vari.=']}';
		echo trim($vari);
		exit();
	}
	
	/**
	 * Lista Ordenes para Asignacion
	 * @param String $tipo
	 * @author Geynen
	 * @version 29 Jun 2014
	 */
	public function listar_asignacion($tipo='L',$unidadneg=null,$id_ciclo=null,$id_sector=null){
		$this->layout = 'home_tree';
		$this->loadModel('ComlecOrdenlectura');
		
		if($tipo=='L'){
			$listar_orden_lecturas = $this->ComlecOrdenlectura->listarCicloSector($unidadneg,$id_ciclo,$id_sector);
		}else{
			$listar_orden_lecturas = $this->ComlecOrdenlectura->listarCicloSectorReLectura($unidadneg,$id_ciclo,$id_sector);
		}
		
		$listar_unidadneg = $this->ComlecOrdenlectura->listarUnidadneg();
		$listar_ciclo = $this->ComlecOrdenlectura->listarCiclo();
		$listar_sector = $this->ComlecOrdenlectura->listarSector();
		
		$model_ComlecOrdenlectura = $this->ComlecOrdenlectura;
		
		$id_usuario = $this->obj_logged_user['Usuario']['id'];
		$this->set(compact('id_usuario','listar_orden_lecturas','listar_ciclo','listar_sector','listar_unidadneg','tipo','unidadneg','id_ciclo','id_sector','model_ComlecOrdenlectura'));
	}
	
	public function listar_personal_para_lecturas(){
		ini_set('memory_limit', '-1');
		$this->layout = 'home_tree';
		$this->loadModel('ComlecOrdenlectura');
		
		$listar_orden_lecturas = $this->ComlecOrdenlectura->listarPersonalParaLecturas();
				
		$listar_unidadneg = $this->ComlecOrdenlectura->listarUnidadneg();
		$listar_ciclo = $this->ComlecOrdenlectura->listarCiclo();
		$listar_sector = $this->ComlecOrdenlectura->listarSector();
		$id_usuario = $this->obj_logged_user['Usuario']['id'];
		
		$tipo='';$unidadneg=null;$id_ciclo=null;$id_sector=null;
		
		$this->set(compact('id_usuario','listar_orden_lecturas','listar_ciclo','listar_sector','listar_unidadneg','tipo','unidadneg','id_ciclo','id_sector'));
	}
	
	public function ajax_listar_personal_para_lecturas($tipo='L',$unidadneg=null,$id_ciclo=null,$id_sector=null){
		$this->autoRender = false;
		ini_set('memory_limit', '-1');
		$this->loadModel('ComlecOrdenlectura');
		
		$listar_orden_lecturas = $this->ComlecOrdenlectura->listarPersonalParaLecturas($tipo,$unidadneg,$id_ciclo,$id_sector);
		
		$i=1;
		$j=1;
		$tabla='';
		$total_records=0;
		
		foreach ($listar_orden_lecturas as $obj1){
			$tabla.='[';
			$tabla.= '"'.$i.'"';
			$tabla.= ',"'.trim($obj1[0]['lecturista']).'"';
			$tabla.= ',"'.$obj1[0]['idciclo'].'"';
			$tabla.= ',"'.$obj1[0]['sector'].'"';
			$tabla.= ',"'.$obj1[0]['ruta'].'"';
			$tabla.= ',"'.$obj1[0]['cantidad'].'"';
			$arr_dires = explode(',',$obj1[0]['direccion']);
			$tabla.= ',"'.utf8_decode($arr_dires[0]).'"';
		
			$i++;
			$tabla.='],';
			$total_records =$obj1[0]['row_total'];
		};
		
		$tabla = substr($tabla,0,-1);
		
		$vari='{
            "aaData":[';
		$vari.=$tabla;
		$vari.=']}';
		return $vari;
	}
	
	public function set_date_images(){
		//test
		$this->layout = 'ajax';
		
		$data = '/9j/4AAQSkZJRgABAQAAAQABAAD/2wBDAAEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQH/2wBDAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQH/wAARCACiAHkDASIAAhEBAxEB/8QAHwAAAQUBAQEBAQEAAAAAAAAAAAECAwQFBgcICQoL/8QAtRAAAgEDAwIEAwUFBAQAAAF9AQIDAAQRBRIhMUEGE1FhByJxFDKBkaEII0KxwRVS0fAkM2JyggkKFhcYGRolJicoKSo0NTY3ODk6Q0RFRkdISUpTVFVWV1hZWmNkZWZnaGlqc3R1dnd4eXqDhIWGh4iJipKTlJWWl5iZmqKjpKWmp6ipqrKztLW2t7i5usLDxMXGx8jJytLT1NXW19jZ2uHi4+Tl5ufo6erx8vP09fb3+Pn6/8QAHwEAAwEBAQEBAQEBAQAAAAAAAAECAwQFBgcICQoL/8QAtREAAgECBAQDBAcFBAQAAQJ3AAECAxEEBSExBhJBUQdhcRMiMoEIFEKRobHBCSMzUvAVYnLRChYkNOEl8RcYGRomJygpKjU2Nzg5OkNERUZHSElKU1RVVldYWVpjZGVmZ2hpanN0dXZ3eHl6goOEhYaHiImKkpOUlZaXmJmaoqOkpaanqKmqsrO0tba3uLm6wsPExcbHyMnK0tPU1dbX2Nna4uPk5ebn6Onq8vP09fb3+Pn6/9oADAMBAAIRAxEAPwD8sYte0O7ZoLa7sZHUP9phS4jeWF0eFkeRfOwywqHi37EiSFESJkKbF5JfifoLeNJPA0VjKbwfvZ9TElsLSOYQy3YjeV8zyyNAiRsfMwkggiVlmR5R8QeCNF8D/atEs9bt/EfhXXotRjnm1OSVPsk7ecv+gyWjRWd3aJNKrwIUS4ERULLJO5H2f1XTvhnpur/G3VfBh1C9GnyXtzqNzeRy2n9pvGln58qQsq23lD7RJwMShNyLIzmIyBrh/DUHiFisRVjy4OpiFVlh3TcVCUE61OEG6dXnumoJ3d73XK+aY5lia0sPKhRhGVSvSpVIOcJc8nC/s3eP7tOfLzTT+zJ6ydPl+3IprCRogslu8M+1VKzY3J8ixRxnKRquGITygByAzGRnReY8b+MLPwH4abWf7GfWozqFtYm0syjupmYfviHt2jkijZfMCN5UbxLlQqAiL5X+LngQ6X49+H/gHQLx7dlsIYYL283KY5dQ1OVnu7nyZbZ5g7O+2MnPmIsRjD5D9n8WvhrcfC34aXiWniTUdRutd17RjLcuTavEkL3Fy0UPkTOS00jneJCvmEKZNwkRBeDyHCqeVe2xzrLHOE6OH+r1KVSdJVpRdRyXNKmvZw51GT5pRtFPmfu+lXx2JdLGSjQ9m8JCUfbLknCM1CPIuWSSldzlzcr93lc207HpPir4w2Xhjwf4X8Ujw7dIvieXB0+5ntoZbJUbCSrstpoy9wjJhVRCg8wxtA8byH3jT3t7mxs7vyyglhtpWXCL8kiB8SMYkBlXLRNh2JjCm3VUnKN8UfF5Hh+GfwWeeZ5ZrlFYSq4f7RMixn7QmZpWuZTG08nkR3IiUyb1clAU7rxvc31p8VPhtpMF1PZwvp2gJNZwXckMJV5QxR4dqxyBiUgKSedtLCK33u+aurkdKrQw9Wi1Sq1I5rUqO/OpU8LWUVSV/haV7u6cpJuTTNqGJqKc1UjzQh9RUYqMYShPEU1L2nRPmqa8iXLzKNlraP1bDbQqquFAZP3cLTPv3sNxMWSg+eUNlC2EBYKFYJGy2GtoEEfyK0yRvIUUMMqz4QDDuSVCyIyKF8tHAVlZGQfNPxGuNXj+MPgWx07VdVtrKeCyN1AlzLDb3DT6gsLRyW8UirP+5QIokSWNUlEkxmCuoyvjBoXiPwfd6h4gX4h6jbvq97LJpfh23lvETylkV5Ej8m42iGFDKZHeCJWZgivK5RF4qWR1cS8HR+u0YTxeGhXopwq8yu+TknaNlKMXKSlbkSu0o/AuqpjfYLFTeHlUp0q3s6so8kEpOnCfNvFyXO+TkV+eo01dJs+njYWzKzTRRE7VVt0e+Vjx5Z8qRQwlidrdAu3gL5ZOd5P5hePBGfF+tBGjMEOq6gjeasSApJcyodx2q6sZnCqBJG4aPySUeMSN+hHwY0bxwvhaS/8AGOoT3supOJNMt7yYebb2qRbgZX2CNTLuV0i25KruIQu7n8/fHFiG8T65OYi0aanfhfs5hkLMt6Y47hdqxyFMYgAMhZ1j3K0jl4h588PPDYqtRVaFT2U1CpVopuk2o2c+j5UlK3LFuPs3yyXKpHoU41KlKlKMXG6jLlqckakXF6R00fKlzxTfvWTvZ+9ycWpWqwH5UeO4TbEsw2Rl2ExmYsmyMoixsskTL52V2nJiKLpWZW9jRTbw+YFErB5rSOFWw6YVHjhK+Wq7Yrlem2Zmb7ytQk0K6kgg3W4SNVmKx+UwZoz5uPmjUyBjGrMbeNUfy0dA0Ll607G2uopraKB5GmlimifYy7QJliG2ZpW8wyQISI0e2m2tAYnZIQzGYtqU5U3zttSV7yT3bbaSUU7Ws9FZ35Wm42sNUas78sLuMeTX96+VtS95qStZWd2rKNNKU1LstKFvC20wtMwV2l86JDIiyee+zfGioZAkk8MUalC0U8alljeHZtpDbbESZrWFOEIyT5CtuQuHAlVdu5JVJLmOZzGMu4uGzLERPKhiS6ha2mEVw8qFo5ZWiZXWRVXzDtkSWORZFiBTzBFGAkar0DBNgSIwMBDOFi2BZSR+5hb92jqhSFljj8xJR5axeZuWBwzjCSleS5XJSUrTc4qXK0uSy5FZ2UeXVXnqlEJUajhZSpcvZQ5/3bVOUoxdoxv7r1t8U7uKk0V0UNbysiRhAgdbjZHKgMMiRW8YDlZYohvhMRiOxpo1jYo7or3/AO2Zv77/APgFqX/ybUcNpbshjSdoY3KuGlI8uWSF7ldzukUssTxyTiTmNWaSTcJWVZnbpfsk395v/BhJ/wDG6qFOU23zRv7r960Hyt3i3zUp3e60ei5UkrRRlDDVK0punN0feu7Qve72tGUbKE+a0m5c172TV5b2j/s8+ItQ8S6bq/j3xmviKy0c+bBY2UcsbOYXLQRHIj8m2QLHIZNnmb1dV8tRG79l4s+Bev6j41/4TXwj4lGjaxd7Ib1GS7hwzokblJIN77WhWCMxyL+7ALrliiHxuH9sS9e48o+ELMxleHGqSg+YHL+Tk25V1XBhLRyOWyRbvGrEjurT9rqZ4/MHgyxErGPz0utWzLCBtDM0gtmGSqBlh2RO0e5jsJDPs85zFVPauVH+BPDujChTVH2LlTU4SppcurSlJc3NGyWrTR5GFy7AcsKajdKsq1/aVJVPa6O+uvupckJarmV42akd/d/A/wARX3jHwF4ovvEcN3P4Zt7K31YvBcreXjW88k0rxfK+yCQS7IY5SzxeUhP7xYmT0b4zfDnVviJ4Xi0HSJrK0mi1aC/kmvGkRWSGG6URiWCOQrcKJ1YFkw5RllkKFkHj0f7WKBojceDJWJVd3l6l5auzKxRFjmtVWQCNRGZG6HcGRJ3i8zSsf2tdLeMK3g3UZjEqKWS+Rgpd2EgJMW1VTIjE0isAZSz48hgypZnmLr4TEOFPnwEVChD2SULN80ouKtH4qnupvRyUXytNv1fqOFjCrSlzSp4iSnU9+UpNwVL3nePNFNcq9zd+7eV0ld+InwA8TeL/AAv8ONI0m50wHwfbWUN8921xDHOT9hE/2TZGJCn+jvh0EZThWDytlOh+K3wU17xFqvhvxT4QuLGXXPD9jbWi2ly8VtCGg2yxXEMrRhd+FWNoyy/ut7ptXYy5T/tgeHIfKL+DtbCsyqx+0WQyHmgtwxyzA4dRLJvVnCJ8pKcHWh/bC8DxS+XN4b8QphAN7HTyVDOYiqbrskZjZVARsONgV2ztl7qWc5nRqUfdoSWEhiFCnKkrSpY2q54iFVK1+Zy3aTSte0bs1/snBzVduMk6vspStK6hLC6UXD3LLlila7a59YpXZyVj8HPi3rHjjwv4n8VDR9un6jZTzeRcQq0NlbzsTDGkOM+UGLRxyrL5hdyQVaRlyPFnwn+NWq+OZPFNxo+m6xaWWoPNo1lcXkUtolirILSI20pUqs6ANJsbzGl+UyOFVR7DZftafDuYpNc6R4gt4vM8rdLb2UuXWcJ8qpeyttBKH7rP90Lhwc7sH7VXwuupDFjW4sPhYxYKMqGVNzGGV1UBtroVbDRlWXaGRk6aWd4yFTnWDwnu4d4WEVSqQjSp82saTjPnjJ8z5mnF8ttY2Zn/AGPRnFwVetaVSNa7mpuc+WEYOfuqm2uRuKa1lzOKjK0jpvh8/j+5sL9fG2j2OlXFv5MFjDZ7DFLDIuZC6xSzpEbeQKhXcp2D92o3qrfmn4usYv8AhLdceVtu7Vb95JAV+cC5ZUSMEy+fMZBPKQscSbUUGNMu0P6OD9pb4VvG6x3WpfLu3N/Z05JY8RsVCnyw2wtGyqeBvZFVkc/m/rtzFfa3qV9bFzBc311cQyMFMkUcs00iNvcLuaSG4ZriKUMwbYE3F3avKVq1epNUY4dTnTk4QT5HF8yfKneXvWXNzNqPNKSs7QPSjQUaFOkn9YlFwjJytGa5eZuKcVGPvJuC0i1pdNLnSJbxPax/u44mjVzKieXgRvhnDLyhjJJdpTtZ5DMvknagZyaYkCkxwou8pcfJtRjI5MQR8MRuWNtqqqKZGISPy2HlnVtrEP0B3NuRcbZFkhSWXcV/duPmaUM6NtckjBLiMpoRwqpl3LIhUKwbGzA+8UYlmABOIxtUlI8mUk7GGEKSpSvZxUpX928VDlbk0o3tO8ubW3XlSd3zaOFSLvDmjJx0kuWSa5otr4WqbTcfhUlJLmdrpGTHZBUddwljkMcMpjj3lgzbxmBMMplZGYzJG5ZUmUMqt5RsDTljLMw5mt1nf/SJX2RiRihjimU48yR5UYF2gb5XicEuh2Vg8uWNkSNZlYqdqzSPFG5IQMpdH28MAwG3dwuwA7tWGwCrMpZVjdZ54UctJ5Q3eZArL+63x48sRrtlyfNIJD7FpJRk7ycbvbVO1pNxaUbx5UkopJ/zO+icSg5SkppRa5XGUHazdoqMfhSSS5bLWLso8qSS5T7C0SuysIkaEyKqGYIXFzuikMIXzVkmP2hUk2RAKfMlkmR0L6X2xv8An3tf/BuP/lXWk+myQsTEEnSMhW3Rr88UcMVsuNgZZZSZCpYZixJtkDqsaNof2Yv9y4/74uv8Kumq9STlDqoOWu3vSaTtFO8bNa6aXVm2Y00pR95QVnvyU5N3tLefIrJNWUdItOPLG2vxxaSaa0roLOCJUkh3Sws4kktW81kUlAzRJmMhjIIlESyMPs3+pXr7B7Se2BIV3TISPzGWMomSr7VO8YMTDfASOTvCPIyy8svhCxha3MNqPOjCxHEayzCBWjcSxfvIonb5kUTQiUuw2yOYWkMetZ+G7a1DoYQFeR95MiPJCrPGqAPEZZyHdcsjeWnySKZUZy7+NHF0ZOnG7XvxTdWCtdauLUPeipcr5VF8qWsVZya8rA0pL3ZNO8YtNXilzWcXZpKMm/ecXtJyc20232LG1XYY0jEqQrHCAxUGTDMrkBZAJJFDS7AzeXuyJQyxu1+3ihmdFREVcmJEWMoXOMT7URLdleJN+9mCqiuIVjCjbXJf2HaJjYTDF50gmljnMCOGhfazTlkncRf6ryVLTNIoLIoAWKxBpIWaORJbhJS5kWUO+xCjPJKJgqwyt5jbI4FLFw5eYGIKjL2wrUpqMrxcoxTgnC6bk4xld/DzO6a12k1FqyS9L2TjOo7Je89VJXbikop6Q1vHVRT958rTSaO1uNIikUQERpK1uBGS+Jd7s/zR4WEbixLKIw0RDlmAYKBVuvD4jtpJJIZmM7Hy2EiyMjIrrwwdWlYLLlxP8ymJHlwkpSTX0+2EgdXeQRMSIszvM2Q6hyrbE2xlh+7UOXVQxO/YpOjfeHzPJGdK1W+QFkZdsmwtKgS3RwAIwBsb5ljhALcKBlhH2UZxUocrTTjabavdKKk03rdKSdna+jas5Xj3RjSdOUmpOVpNRVlao2lBuO0uWOq3skn7trR5iHT7doRBGspV2dYhEg3KSULATQyYiJJKoXWLMQkHlD5lexBpttO8OGuSWkmSWJBCsYSNpcJIyDbHkyOkUbCPcPvRs4eRultvD83Ntc/aS54ivUnRAWDlAMncrK4jUqDlyp2uC7RhbUHhG7tZQFv71iV2jfJFJFHl9xwjQFF2l9uzCg+b5kao0kuOqFSCvGzi207yg7uTipPo0lFtNrTfo2zPkirOn0kudp+9Jc+zWju7Ru3eMYtd+VUobG0jXaWnBaV0WIW0LOh2mABmZQBGA37xd8pjxuUqSwa3H4f02UrcS3F5G1xIXVWRHUxBlQv8sZLxxxvt2PE0LCV447ctIrNvr4XvZEi8zU5lIIlj/wBGRgCd8g2oyfMV/wBYSyiMMECxALlt6w8I6nLs86+d44UjEayWduhO4KnmSIIQowcMuGXDsjeYoDCs+ejGHuzikppO0VzKy5dGk4+81GSsrp3SSUkzeFOXK3ZRlKSlq9ffs7uMeXTWSaitW7q11y51jp9iHhj+1SxhN7BvIZnO1nwAQU2bEE2xN8y/eEKly0h6K18J2c+DPrCJHI5m2tZP5pkWHyFLZUKDEHQ7xgN8wKl2w3QWvgvUyR5V2FCt5QP2O1DqBuYHYmxcs7bUbAzvwI3Vndeog8J6yFKSXttKWaNlVtKRZJF5eV/MXbuc5DIqtL5ezd5UJZQeWrXpbwnCy96Wyk4y1i0+VJWlrFN6J+7dXS6Y03KLai7fCnaPK9k5cy1leyb83dtcrb5WDwPpe0JJrtvEUVC2+C6TBRlKiAxrJIAsQLDbEF34dkCvhehT4eafFukbX9OaGXzRzbXZ2WxbEabI4UaN4t0UjgrtO1Vk3PhV62HwtrsUtu0baVI0RYPiwmi3jaGlDEXJkQIUWQP5vloPkAdljdtmPQNZIH7vS5m8vP7u3vELs/JhCPdsyqdjZdsCRW2lGw7tg8TTvb26u2uZuKk18MklfRNaJy2k0l1uc1WHLZWTu2vddpNKUEuZxcHzPmbenxOUkm3yrzh/BVqHihi1yxkUo5WQefkx7htIWeABI2UlZYmGZNrKAd6Ftr/hFLL/AKCejf8AfzUv/jFdFceH9ZScMtrp0ccIkEb2zXSsA27ao3bkyVVXwrJiRZSFTDlbP/CK6h/0CNN/77uP/kqsnioJ61uXa3Io2a06Ras00r3XxXslq3yS5E7+ypVea0rTjKfI3a6XuyWtloko2SSvdpfmroelXkukWU1xAq3k6LLEoi8hJdreWjFPKjhhAVmkk8xbgIjhiXSdSd6PSHgbzpWV5bgGN3eFVYM1vFhpVMh2xrM+f9Ym75V3IDGlfTNh4Ggh0iyjaBlMdpGHV2Mcinyk3eZiIxAHrvxGJApMkSPLM9ZNz4TijmfYpVnjZ9qnqsbK4VMKig5lKAjZtCZG05A+cpVU6lRxu7yldJK/K5RUddElBuUuZp62b0s1GHw8eSElzXSpK7aveEVd/Ys23yaXUYq6u1r8seJbm50JQUs5BPdNKYZI4WS3iCRiSHzCzKsrq4V0a3lOWJCuBtdek0gztpZvLlY/N8ieSbcgZd235I2kWXlmA3KFd9vzsHO6QDrPH2gCS80yxeGV42MsbFPnC/N5akSJGzMskaOrpEcMrcl0LpJ1aaDb2nh+RpLV9sFvcKQfNZl3KFBRFSJYwzFkdWw4I3R+UVUD6mjRpxpUOSnJqo1F2951HzWV3orOUFp2lFaK/NNPSc+ZO0bvW6ejg23GLTtdy5bbqbT5bWj4tpCanqVxcjT2lEq+SZEjnxCyEb28vYdoIBinKMrsABIrO75XudF8Oa/BPFHPcNCsLAbXkjdZBKVfDqxaSRpEYcKU3feZt5KV3XgHw5bQQyyInnSGUIkgYhlkQRx/NIch8AA5BUSzOJhHKWYp6eukW8paQCVo8cgbHIaNj2yBuDFZEEcBZlRclRtmXvqTjD3Y04pyilJSX9y3wuMrNpSSk1vqry5iouTV25LXVJpOSlezWqSu5KMnHSV2na8bcT9jNjC1xiVVVCBuO5t4+bDnMWWKAR4Uush2uvllnRsuwPiHW8z6fmcq2PlitXa2U7jbxorwmSWN3idSmQ6bZQJJvNG30jWEgstLmmlUiONULL8/yEKzndkHcS6lwHiCozgzKMknd+GdtDNZXLKu1fOj2IWB889Ffc3zjepBXzPmB3MNolG64Tpxw9Sq4KVqluW61ekn8K3V4q+q0TaTTZhz3lZOUYpLl292N4JJ3avL3dUltzX5ZLmXknmeK9NMf2lRCC2cSQxM08abWKqNmCWCmQ5JWM/vQQ4Xb9IeGNEbU7TT7sW6xi7tIpRGU3JG0gjYqFCqAH3h1lCEKVj2Qsi/NzPi/TLYaxY2bRxjChxmQhQSS4IxhThj9wo2xpQRg8n648L+E4lstNiEGGjtLZflOVVhbxoWBCopwSMLvG0NkCTK7OfFShCFCooQpyqc0rR5VFvRK6Vurur9VO91eR3YWU5TqU4uTgnyPmSuveimo6WXLy3WnL717tXPAbvRfFtnM0VhatcxL8m6DT2ZGQ7SChjiXAz8pPksWdmLOAgVcAax4o029hg1rT7Syj8xZ/Lks5I5XQuUDEK4MZdc4WTesZVnk2qpA+/9G8HLs2xxLnoqZO0EHYEJPQPvVmCIMb1I2uWMfgfxV8NQS+ModNFsZiq2tuyquZAxi3ruVtqE5bIdf4shT5YXfw4avh8TVqUnQgpKm3OrF7Je67csYx1ctndWtay5j0pUa+HgqilKfNPkVOS92F3zJJWb91pNO3VvR6JJ7OHT/D8WuzROkc8MMlvFsw5e9wVy5aMMcTLkgBsN8jszYXznStW8VXz3MttocFxbJK8TPDa3UmwZUNnYzBS3XZJhmVnO59yuftD4nfD86f4H0q2jtcPJNaxsqlMEw2MpB3qACd5HKh/lVSYnO0N3fwH+Etx/wh91cTWZU3GqTASiHKmOCC3RXGSQ2xi6qWfcpBBLZK149LEYajg/rNSHtvaV/ZwTnJRSXu2suvuvmutfdS926OjE4LEVcR7CHPFRgpcyhe0rRcVpa6TkrW5UkrvVRb+AbXxBdvcCHUtGFns8oFT9phby3MrtKIZC8gcFF2I4VXCuZZHjD167/wAItH/zyf8AK3/+JruPHvw+lvPiXNY/YjIj6jFZqAPMdTvt4pUUZ3KTI2NzbS7vmRldnCfSf/CsYP8An1/8dH/xdcuZVcLH6rUp81GVal7SdNT5km/Ztatx6t6pLTVWSi3x4XDYrmrpKMlGoknKCbstFJKzUVJLmtbdNL3XFL8xdT0qCSGJkVVSWCHdHHv8jeYi7fKi7htw2Y/lWN02Llx5Z4+50WAyNtTbkqztsi2bGkxzHkShDHtUFAwlRjiQld8fues6QFAUKSp2lyE2A/u9q5fYdhVuSEDDqdqkEVxE+nxwv5hfq2WBJC/eYMoKgA7WQBdoXBXcQgZgMsFFtp625mvd+T5UkrSbeict7Wbd4tcDcuZwa+KN1bZ8sUtbu6k3J23WyVrtP5i8XSWFtqtrpLWUE8kmyRHmb94gl81m+yojMqqBtBw5BRSqRzNEAel1W1tLXw9JObaBg0Yhe3VFOQziFRGIgFRhvUIA+9ZNjjKBRWX4l0yS/wDGx80q6RSxsiEkJn7N5bu0YHQkF2DbgB8y5LNI3ReLtPlh0GOONFTfJA2CoQvuZ3c8FTHgRCNc/Kh+beoKpX2dKEF9UheXv+zm05WUuZcy+HZ2ejikr3krpM4ak5OU2nbbl5FH3tU27ckYp/E223zaJpN2Xmun+F7zUEmvbO4EMcryeXEm9F4yBlUZUbc4lUk7XDxhY/KkYtVtZr7Q9TtreadpXup0SaLl4yrswMmRKf3qbvlIXHysQFdZAfWvCNtZQ2EEM89skg5ZWlCn948p3gZJDD97t2liqkluG8qPnpNJt9Q8Y2iRzI0S3ECsgCsSVdXePzEIyoZs5yQMyKAV+VfRhONWU4Tj7kacrSULPTkcelp3coJPyb01vzSqONuV32Senvr3Ypcq1SVrct9VHRvl1k8T26WWgs9zbtIjEZjdRGxD5crnIkEkbMZACfnYCRX3vurI8K+FfEupRQ3+lX32e3luGURmaeOQGCV4n3BAIm8sgo9yzbWSAMSMnf6n440of2ZCitsj83LO8e08SRvvAZSGO2LzG2oB5YZRtJCn0/wBounR6JpcAa3gkKGVonkjhBLzSSvkcONoUkPJwyncu1OFwlOFHB80UpOdVpxmk4qKiknZpWdrptfNOyHRUKtWzbhZRbd9W/c1j7t+VdNey1tI8Y0bTJLfxHa6Vr4l1OdJ7eFIEuJPMzPJEqBJpAu47VXMQChmXYVj2si/ohoWiSC0WGBdkzW4ig3QbljIwU3RqAxVSAExt+cZLbUBPyf4c0GPU/iraRuBg6nAYkRS0brFK0hyV4C4QNkYK5+VlAwv6Q+H/D0SmA8EYTaNnA3YKsc7cA8fKT/DtXkSJXk5vUb+r8m6pKSS0jd2tJLRaXSkrvXR6rlfv5PBS9pKVneUVd25kviurabvm5nfVWXc+eLb4efHWW63affLNCWfaIL+wgR4lLsCYZwgQ7yqKhR9qLtOCCz9h8L/AAsNZ+J+n6N48hubvWP7WtbVpbYWryi8juoECXEq/uWhSRMMioZipleOXKpI33R4S0G3aJHSZNoRSuNn3BuB2sOiBsLxklkxzyW84/Z48FnW/jwl7LHHIi+I724cGNmKeRJdXMbsZE+bGERccfKACflNeZQxcq9HMOehToujhm4zo0+Sbvrvdq3u66a8qScVqfQRp0sNWwiVSrPnr3aqzU4+64NtRezXNZf8E+xdU/ZxtvHWjpahjbT2zNLbFIFCmV4jFiTzVby8qTk4DDe6oQhO3Y8G/s9/ETw3Zx6ZaRCOxgOERE08oWdtzv8AvH84sWJfDbtucKFJVB+gXgzwxHCV2xKowqNhFyyuVGdxCjCnJXG7kgggkge4WXhS0mt2c7UjhgkmYBZCx8uJnXy2j5G3bucMG3b8AnILfP4XnUY0Go1KfPGUIyjdKb3atyqzTdlJy3vFJts97EYiMafNGkn7usmmpK3L/Lb+VdHorO+x+XnhH9l/w14r8Vw3muQl9bW9B+0xWtuAJo3aSVrlUjKuTJBEzKjZYhRHtUoyfQ3/AAxt4a/5/Yv/AAGP/wAdr3n4eeE3PiQ3MErrGhnkOEkRgdkiOXDmMFjJcRsCiER8A4UIF99/s6L+/P8A9+T/AIV62Oy6nUqx1+GEUknFJLRWSTSt1X+K2i2+WnjWpN04KKbV7RveWmvwvX/O60ev8JGra5o87FluYXjAPlHKxKI0JUbY3bMZDIdgZ2xsXcwY7F4C/wBU0tY5ALmMeWWj2KYzGDklEVVZgwkI2LwPmAI/1kRXz4aLdwOIkBwGJVS7Mke3a0nDbNizRqCroP3zuwZP3bMvO3+g35uCU3tCqFNyO6GQhsnDP+8kZGLszMsXl+btOAWkX6LC5HBS0qVOaLbelk03eMY2basnva/R+67nxqzCUrScFZ1GpODdk09/spczUtfeta6uuW3V3UehzX1veuI5ZoEzHJG2whXCqdyiQkrkpjG0r0GzKh4NUfR9YtI43mCtvXyZYwJBE588b1lZNp3JhgJECtgk53JXKW3hu8kVF2TNKY9u6QuQVHzBQUbDM+5lDsG3yrldwJ2St4buUQFUlD4ZSoaTbG2xstLtdVUCMsC4l+fzRI+MEL6cMA4uF60pSg4yg9F3slskvsrS6UbfaSbdVSXK6aXvcur1i042d00vdvzKySbfMrWtGvH4S0dPMlGoyM25liIRX3uQ4j4EjM2zcWYRumdwUgAsh6Dwpoeh6Te/apNRF0VV40iwqJCeD/A75LyvLuDBAw2OCS7u2HNoVxDbXKRmZS+7YIvlCqS2WEkCuChBkyJPmGxYy6ghFbB4anVVlR3DtsOFEmQVZnfAdi21JDHIoUMxHMe3DKfT9hUqU3zVNHF6cqi99Fa11bXXbs3e5zNpJyV2780U1ZJtxdrrS7lHrto43d2/d9Qh0LWrNbeW5SKSMN5UiCMtDKob5vLZgkzKM71LIvynbt5JzbD4f6fPOiweIkRwGCN5MgJdlZsiMzgb4mG0AMw+XhMrmvPLTTtQlgkjieQFMOu1dxljMkrYfPk5QbSGMalSq4VlVWFXLO11VHc7Z44yVcMrShVV8KgUh2cCM+a3LqojMinOQtZLB1KcbRqq100pJOzvC7lo7KTlzKNl72tr2JpyU1HRXk1s7PfR6aJ68rasuaOtk0fV/wAMPBOiaBrFvqt7rg1K6ibzLMiPyzGWCxP5i7n+UKoYbhhXO0SFC1fYsttb+JtLk0/R/EsGiXFwio1wqRTfJsKPEqCSJkD52uyElgAA6hjt/MGxh1a2eMJ9p3wbXIR5mXEqfM8JjwCyFtwb+LG0FVQivRdDl8QxTwLDJqSGQocCS6ZXCrvyeWClQdufvNvJfG1AvFiMorVairKtF1IWaTgnBcjTXuWtZe7pKL76KR6uHx1PD0403RnapKSfLOzldR1vq02nZPtGy+0ff/hb9nnWppRPH8S7cO8qzMVk1BAQZFJCstz8o+UKWL4VmyZGOC36Ufsq/A/Q/CXirw/qXiDxJbX7rrNhPctbrMBcWqkC5jvI5psOWjYqWQj5mI+YjB/F3wzceKokiMWo6mih4yQLm4GUc7iFU+YpQx5I2gbACCCqoD9M+APFXje3vrRYNY1u2SMQgGO6udwUMuNpjxtZApG7JIyqnd5qZ5q+X5hO0I4mg1Llk1DDqCaTi0pKKvJO6ury6JNNqR6OHzDAwcJ+wrxd00/ac8t0m0pKybaty27qytr/AEw+IvgnqGj6dnwr4kb+wtXla6s7i48+WS2STaf7MF87vMqKUeWJ3BPlvsXcqHPJaV8M/GFrl18VxSLgkQtqN+Eb5j8jKIxEFwFDL8wxgE8EtkfsF69qnj7w5438FeOPEmu363Wm6fJpNre307zWnlNcrd3OnySs6w3Nu01rIERGSbaXkR0idFqfFPwZ8RfAGuX2mrrmsTWxLSaXfLI/k3lkzERMDsESsiGP7UGZ/LmQxg7Qjnyq2W1qNRyougoyd5XgvcqLldotJe67KUVNPT3bK15evg8zhiFVwNacvb04KrTbj7N4jDT+Gpy/C5wd6dXkvBOKlG3NKEfoPwJoSaFJPNqc9tNfMjwmSGSRlPmmMsXcGMl1kG1/3bZilwSGDNXffbrP/nrD+Uf/AMbr889O8Q+PbVRJNqWoSMuc7vKBdZCCF/1ahCpXmVTtVgyEKxVa3f8AhYXjD/n/ALv/AMCIf/kSs5YatPlc5xva111Sa7rfrtpdLS9jlqVKVOo4xg7aXTatdSWq0a12dtL6I/jL+yM7ArtY7Mxkb41QqWHzyBRny9g2yBETYvJVxtaaXTt5g3wqd0bkuitCrHZ86FTtB3hXi++mThGY7Rj7yvPgb4Z8SWi3ng/Tk06O5aSxt3v9QllSym0zU5LO5udwif7SLyFULCU4tyJmVNp2niv+GbdTE8Cw+IdAuVmEdwJIWnAtLeW1urq2v5wYl82JoLWWJI0UyJtDbGSV2r7RJRlLpZSasla8eVxi0krcu21nayWicfzyMuVx+LXlvHfW6u9fd1abTvr7r0Wj+Lb7TWSUYhEWA2+Qph3Yjc4IbaqB3ldXyi/MCAVVXZZY9JkKiRkZiwDbflkDcuhLIGCkjDRh8fdbjK7zX214f+Adk0ur2eqCHWpbq0tU8M3GmyXEMF/JNp95freROAjN5Yt/LG4ERhVbG4ps0fCnwK8O3a2a37rdG1s9f0/UrixuJfs7a9BPFDpkAdhGzLDNdCN0Uqlw8Qk5+Vi/d0s2rJczW/V6bW6flZpM74qfKk9W+dqLtyrl5draz15usbpO1/dk/hCXS2ZyFRshThH2pgDgk4VgqmUlgzKihdqFlw24g0xmWcSqU2I4RU2hjIWQsjgM2UCySMH3ghom8wIwVq+4/EXwD0yPw1avpE0dp4isgkmpQ3d5MW1K7j0qXVLyxsIlYiOZIWiba6hE2HCgF6+Zx4eIZW+zqxWVd4SPAj3DAcoyglQq7f3nHIlj6hT0UXDlS8tVGKutkne/VLRtbtJ72FLR2lfm6+9eLivhcfS+qtaya0Su+FtdMaNGPKhQCGL5UBz5owFI+VSzHOd0mwBQAQW07eMxpJZgDMzxgTvADJgRn7jKUIJBy5j+6hzEWbbu7j/hH7qVlCxhUiCM2FALHf8AuwwXBMQYrhAAwU7m2rlhqWfhCZZlZtykbZ9ro2CyeUIxgfNhQuxc8/JMVRlBIuKhJ8zS0tJWfRNWVu63ej3V7Xds1OVJSinvJLVeWrSteKvJW78qTuc14f06YERncz4YKQXVZFQGQ5B3AqR82QrcvkbCCV9W0HTWeJFG5ZFwYzgcqoyPmViwUBNq7SQQyhipGa1tP8LMixt5O2SJVkQhFLEbEMhZkWNlIkVMDY5jVyWKsRFXo2h+F3lkW4a2kbaXRl8hVVl3RqMrhcA5VyWDYRQQAMoScowd009Out1pq7WtfS0bPROydkaw5m1FOy96+1l8Mrrl1s7X3ul0WtrmgrJCqpkhMLCAYMgyJJnco3gCRTk/N94MMEvw30t8PLFjdWonVFO8gEk5YrhSWUbRHggMCobCkMyEFnPXeGPgbpLwT3S2mtai1poWianDbWSQpNLeX4c3KN+6G+OI7vLPMm35ndkCofYvAfwv8zUb+W5stZ0bTNFt7e4lgvYbdtRe4kkPlwROqRIyN8mJZQxH7ti7Jh14nUi2nF6aOV1a702XKknt5X23TOymlBJpKO7tvvyrdW0V5KOkWt9Hv9vfs3eIb7wnqVrqVs7RvELcMgRiTyh+YEkEDgMQrIWwAAhC1+sMlx4V+L/htLK4EKXxiS4tTKqk292AGDQ5wZIy6qZITz8oIBeJXH5h/DPwRG/9njS53aC7g814rgIbyzW3MZKyAZRm4bYVKqG45QYr6q8K2GoaFqNr5d8YIpIhJ5e4CQ7AxAAOY2YjG5sqi7tmQ5XHj1XONes1Bzp1OVSg9E3ZLRvaS2fml31+hqUcFi8BgZ/WI4TMsFCpKhiY+9pzuoqNZK/tKTu0k7W5p2XLKafJeMfAWqeGLmS0ubWSExZEUg8yUSxqcLJHOctPG+4nzGYvlWMgRy6jg/s156yfkn/xFfeMPiLQPE/hwad4hMdzdSJOlm/ljz1ljTaJFYY8p93ykg7XU+W/GQfDv7K0r0j/AO+I/wD4uuOph5Ll5KnutXSvaSXuqzUWlprZ9b6W0ZyUcxVTnWKjGlVpy5JOlrRq2s/aUZe63GWvuu7g9N1r/LiPHBsVkgsPC7WWn+WJksRerI1vPPJcT3s80ssduxSWaZY40aEeWkMRaRZOKm8F67ZyXc1lrFnFbWraNbW9w892sZl/snTb63QbguBNeNeKUEe4w72ILuwC+M3F3cvDme+NxOscLyuhdUdWBmMRQRRKJRHtUgfJKMODvV0ahELxW3QysCQjAtLK7gscYG9wYnCs+TsP8IG1CsVfSrmkr2grqKtqmtIvfq1dR19HytJv4RTVOajq3zQd0rpxW/SS1S5uXSS2d/iPeIdWljudLm03TDpsGj2d/ZWEEl7GdiSWUdlZyyOkasZbbbLK/wAgYCUqqYDuUiu3MF7b2thFaLdeIbPxK8aXICRy2yxPcRozRqojvbqK3uXVsiNn3LG8wCV5La3N3C0ZeSRSFdGHmOrlcc5Tcm8EqjZVcr8xDYZCLLXdwyyxqGaTGQVQMgX7ku9TvJ5QliFVNxBUecqh5cKl76LmSSVrqN5JK8lpfTmXxPl2ve79CnV05U31er5U7Rg1J7Ll2S0duuqVvVLu/gvdRmnuoIE82/1XUQsVwSoS/wBNGm26gAbUW3jaN0OAz7Ay4JYt5xafDvw/NtWS5hUN5b7yVKy/OSI+fLUoD5e1W3HhQUJAd6tjHPujeSQMsRYKMHGAxCuSYyQFIUBssd6sDvKoq9dBAWh/dgoyl8ouACAu7DjcF+UKx6FnG1PlG5qiUq0dE18KVrK2nKl9/wAVtUuruPnpSXvc0XeLt/d2u0rfDZ/Ck1vZSZYtPhj4USJRJdW5BZlcxrbEA7X4IZ2cALlS21d656hnZumsfAXg5chprZljBIKfZ9jqAE6Ruu2R0yrhkZfmUgsWKDm7S58uT7OQxKrnaVzwyDdGCsf3dmVA43ctsADhde3uYo5AVRgCygxlcAsrOBsA5G1QCx3IAN2/cyptiNStePNy293oru8U9VZNJd7ei0ZXLCSvFy5n7nLq9LxT1S3Se9ktOuifp+j+G/CMSAfZ4X3KcMV2yMPnyPkiAZozC6qTHsJJYgK25fRdK0nwxayJ5MEbksQ+Y2537WwA0KKFwGKnG7CE/KXUN4/p18chVXfwMKP3exQMxMeSRGdw+aRdibXKqXBevQ9JnEUYU7YfmBVI4yo3LywJ8qMxgS7OOE+7jKPtqKkqvfdp22Sa6dU9b9L2d4pJpmlJQVtHFpXuu6snez3i2trdU2mrH1vpnjTwjbW88Fxps6wT6bplkXtGhjuD9gUkkSFkVdzsuyONWIwTIx3bK7Lw5408LW0+oRQ217JpuoQRCX7TJF9rV428xJYyuVVQvRZNw+XLcsAPF9P8FwGK8nur6+KWVjpV8RZ2PnXUralGzlVjEjs4hD8/J8wXIRgAlWdH0S0e71K4XUNQtNG0e3ikvZ9Qs2ivGluCRFbx2pYFvM3MisR86LkqVGwc7jUc0lJtaddHZx3t1fe9tnG7fKbc0UttU111t7vS60V9Gltdpq919c6N8TLa1u9Pi0i1a1tLa3cFHaP7TcGZcOfNGIlMfmZULgFflbaCjDuYfilLFcWsuxmhtUxslYsZ8hg27JKlQkhw2wlVfA3fKV+SNI09ZNQ01dM1Jry0vLW4vRMsKm4toIMvJHLbxysokXCLCucOzAkEMFHSSCQ6i2npqMkQ+y/aP30UsV1lIjL9mMarhW2q2cthfmKMzfIZlQk7avVp2+5r1tq357XXuluv7qilbRJpr0TvyprmV+j9bXSPqay+Ma2lmUaFhcweabdvNXy4fPiiB84RqCRAS3kqoBbdhiMORy3/AAty5/6Y/wDfyf8A+Kr5isbwalY3c39pSi4gt7u9e3igEkFvFZqfLOoSxuhje52loIyj7wQOWcLXL/2rd/8ATf8A8BX/AMK5qlGouVRipadem1rfDvo0rdnpe7xVdxlLza3fL/K/nq+mnRJXPx2j0k6aiWwZ3eSZp5SCioGJySdsmxC+YxHIxbywo2ktICdmyME5K7AxSY71MbeWoLKG/eL/AK0TAqqsVJjUYbcjbx7/AOI/g54m0/TIt9ppFxM9zFaxzWOsWE5drq4aOINbxzs7JHJJHGk2N0R+aXco2t4u3hbVdL8QTaILIx61FfJYfZEdMJdmd4lgAOIwrMV/fFsGMouMMmz6eLT5t/KzSTS3fkneNna9221ZXPmoLmgpXl9mctNL2he6UYrmVrLRXvZLW4j6dDuO9FHEZEjI+8q+QI3G5iGIJ2MBGoC5O5juVkaJbSSwrhl3EkukBKqN+SAf3g3bmxgNy8g5D4Xv9U+F3j+ytbm6vtJihgtIp5LlE1TTX8pIUlY/6Ol15gcIqlYkR3JDbei4qaJ8M/Gut6emuafpE01rOFa1Z5YLS4vBEu5ntYJ5IZLpTlyiJvxIpVCpZsxOMd009Et1ypqTaTf82/TpdbpLqpyndaWi2m/d3XkrKzv9r3tXazb0xdNjgfzFbcuYlMZx85271UkqCVJTABBLDaFUHcS2stqyyERoCArhRuz1xKJH27WyWRJG3Fsl0YKAoD72gfDnxhrVvPdWeht5btcwvBJc2VtLA9uWilWaC4uA8X2dl3O0sSIjcqwQFj2dl8MvGDSXEFto4MsH2czkXenrFGkufKO4XMkJ3rGuGXa4PylIy5xztbKztL4r6WS5VvFrVtu72TTas3d7XVt2o+67vTbRWTVmtY2d/NaPTysWxiRw4YbN8zqFLIMNsy7LngMww6ou3y9x2BlxAksrTbSrELMCwDfu2bgybmwhOE3OBtB2hVVSSmfRtR8I+I9C1K1sbzSp3n1PAsrZo1nhvELqkgikt3kUPlky7M77+JCjs2NnWvhp4h0qxbUG0o2Fsu1rtjdQ3C2Lje2JY4N8sShiAUkTYG5XBGwZ6RavCWt7SXw+7a61indq8bq/bTl16IWlHScU1L4JJxbi9tdnzWT7dkk7Ll9PnOA6RgSDBVyB90YYgKxU4Ckf3VyFCYUla7nSbp5m2jblflGRguyl8ggDbsGQAM4G8Bl2BGGRH4R1/Txpdrd6dL/xM44pbBhtZLiK58tUVGGVUGR0BJdfKJwyqQu3qrXwtr2mi4uJbCSKCHUzo8wjeGUjUBF5ht40jwx/ukqCmCFB5UKkua8VZJK3d20uvLRPR+qTa1uc+VW1Xv31jZNbcy+HZK9tejVpKz+mNL8c6HLDqFq1zqmmm70/Q7SK7s7ZDcRTWMGyYj54vlY5Zd0jdMspbILbTxPoyrrOlXN1rGpadrMNu0uqTQxLqEVzaszJhDIYpk27FCswONpA5IfgU8DeLLay/tC5stsVvEktxGlzbNeWsbZJkubaJ/OiDDbwFHl/MGcLvdO48J/DfxDrUEF6ltFHBdBntRcXMFpLdgblQ2sLskzhVYDdjbJ94ZJL0cqS31vFO/3xaSSctLdEtr2vch1JSe1lpfZN6Rve2js7taX6Kzul1/h/WdIt7u1i0u2uVsbfT7nT572RETUbkXiKDdvtKqrRMFCLvchFUFl4A6C81KKO/gvNty8Nppj6XEzhDcS5s5YRPNn5QxdwzYZwqthfnDbvsD9mz9mPQ/FegXt/r+nbrq21BrOdp7q8t/s0IitpIhbJbyrG1zv85nMqTxbFTdGhEfm+k/Fb9lbSdDd9W8O2kR0ZhHGLR7i4nuIZhEQd7TYDCVspG25zwATFnBxdVdIyVnbVKzs1tt1TSvo91a5vScHUjBzhzyj8KetrXtdRik1F7Xd7fd+aVve2ek6bdBLa6j1Kewu7WVFljFleNPJL5N1ccSMHthKPJj2rEpX946EuG5T7bB/0B7n/AMCrz/47X2nefAy8jlt7dre2H2iA3CMY9yxQx4eRp5HXy4hHuxyNpU4Kl3VGof8ACj1/6C2if9/G/wDjVcFWprGKhZRSXw62fL6XW/LovRNndDC812paOV1yuHf+9ZLTZLbfRM/PXXPDn2e/02RPhlrnhk23iCx3as13qMljt+3JHvkhkhNu0UydGkdcNt24YMB5J4w8G+ItN+L8+pPoerR6X/wl0E0eo/2fcG1H2m+t9jrdtGIhEWeLYQ3DbTGS7MtQa34o129gkSXWNYeN1y1v9svJl3p8xP2fdtZSeVOY3WSONySiOW4TW/FHii+iNvP4g1pIlWJnhn1S+ljLI/7vcEd8BNoYcqY2Vdq7k3t9VGFrW6tJrd62Ss1q209F2Xazf5/SxjXK7NOKhJWVlKMd72VkoyUmpLljFraykegfEqw0ga/4p3fDfxmLtbq6kOvpc3j6YHRmc3yp9hEf2YRSbpVE3lL5rKJVfalUviFpXiXUvEHg+48Jw6hLpl1oWhL4eu9NR2tYJYokjlja4tw0VtJbXAL3SFlBV2ZlG0lvDb/x549eZrZ/FmvXEcitbywHVr5o5FYFX3xvOsZSRVAdW3q25ssVzt1fD/iLxhpdkmm2WvavZWk6sfstrfXUNuouGJL/AGeGUmU5cAEbEAHljldqqVLlaSsrpX3b8pWezvZW27Wd2/QhiItSi+aK5layjzO1nooNXclJ2bdldppNJnrngM6wvxI8U2Gt3cV9qNlpHiFr68tJkkjurmOxG+TEKR52oPLdxEMP9z51DNu/Dy+0dPh/4wuNeGp3Fkde0NB/ZtwltdKXjcwyRy3IcLHEw8yVNuW/eFMdF8J0+O/sLqa6029uLa6u0lglls5HhkliudyTq5XyjJDMjx+ajjMobDsdoV9DT5b22WTTYbm5tdOuWR7+ySUiB54S6xO9sspWQxlj5bSBXU7jguGUKdJK191COytrFqTvbdtpp8qfLy62VmbLERlywTVpLtdp2SurK8lflXLr8VrO/M/tTSrawj1DwhFpA/4kT6PrEvh2/upZLq6k1a8gbzYbmaRCsVxDIu1LVQpw/mbVUPXO+DNI1ex1PxFPrsF5FpzaVqMWotfLKI5Z5ItsMZ85P307XB3xrGN4HJD5G3xbStYvbPTotOju5/syM8trau8zxRyB8K8KFo1idCCreVv27lDRoofHTN4s1TVFghvNVv7yAOAkM885jg8sqr5jZ2GV3LgYOwkbd5DluRwsvdevflW3MpdtG18tLp7G6qRcVdaKzsraKySXq/hTtfXolr9Et9i1jUtD8KXAhtLmy0/QNS0e4Kt88sYge9sXAB3faIo2aJmOVmKiMAKxW5DNYWV7HJe+UlvL8UbhCZjtjilWzdLdecIMTiMrtwTuAyCSD88y6pM13Ffm+l+2W4URymZ2kiS3RRGsZIEkaR5DRqmxzGpA4CmsO91e6uPMjmuryYzz/aGjEz7BcnBV2QyKgmk2ygylQ7pJhXTcr0owTdu7T2td7X2Xo7XSvo9UWqjbTb5rWS1vdJpu1vi3bUbLfVbn0JpPhjxquteIr+6e40WzhXULi81W7MkVpd2b3DtBBDOoaOVrpWVY442xsfoABj6Es/D2ra7rOjajoUT3Omy6dpaadcWymSC0FvHEsqTSxZW2Mcm926Z5K5xivirSNa1i5ht7W+1HUbmzjUCK2mvZ54lGMIqQvMVUAIxTEW6PaRlQw3fRPwz1C4imW0Sa5SBmYSWyzGKCU4GPkD8nc3OVyR5jBQQStzilqmm/JJJbLyvdqOvyaS1Qpu3KutlLRN+llZLSWy00Ulq3f9xP2dpoT4e1uGEo4XU4JDJH9yRzZxQysnQFTLA+CMjsGIAC++X1lDf2s1rMoZJVxyAcN/CeQeh9uhI718qfswXDJptxbjHl3FpDK207kLrllO4YUvhpc4z95lGNhFfW1cdVJSXZwholb7PK9tNWm/ysYycoVeaLtKMlJNaWas01dJ6PbQ+W/FGmQTXVxZn7NBLe6dPDAzlUVJTdF/IboYzIgHytxtyCwGQ3ln/CCar/AM9YP+/6f/JNe5+N9KH9szv5abZHLZOeCx3bscD+FQNpHoUINcp/Zif89R+X/wBeuKcFJ9bqy9267LZLo/8AgWV7fX058sYypqNqkIVLWejkk9Leqt5rS1z+YVgJL+5RxvXLfK43L8z2u75Txz345/KvN9UJEd+wJDC8ZAw+8E23XyAjkL8q/L0+UYAwMFFfTL4pev8A7bTPyWntT9If+mpHLXCIJ7UBFAYMWAUAEkgEkAAEkKoJPXaAegx2GjIjx5dFci3uACyqxAwh7+5J/E468lFbVv4k/wDr0v8A0o7qf8Oh6w/9M0jalA+zPx9yNwvA+UYxhfTgAcY4GBgAU/UERLW1ZEVWJtcsqhWP7pjyRjPJP55HWiiuSr8T/wCvf6spfx16Uv8A0mBajJKbSTtwRt/hxvbjHTHA4x/CMYwK11AQuEGwC4GAg2gYiiPAXAGCSeB3yME0UVx/8+f+3/8A289CntD/AK9L88OadtzHATyXA3k9X5l+9/e6D72eg9KeFUXRUKAoKYAUADdFMWwBgDcQC3HJGT0FFFV9r/t39TvX8NfL86Z2ujgLD8oC5lIOABkbycHGMjJPB/qa+g/An/IX0s9237j3Pbk9+ABz2GBgDgorSrtH/r3/AO2wCf8AvMf8U/8A02j9nv2XQBaSADAFjJgAYA/fRdAOB/npk19gUUVyYj41/giYy+J/L8jyfxkFOpjIB+e36gf3DXPYHoPyooril8T+X5H1mG/3fC/9g9L80f/Z';
		$data = base64_decode($data);

		$im = imagecreatefromstring($data);
		if ($im !== false) {
		    header('Content-Type: image/png');
		    
			$color_texto = imagecolorallocate($im, 246, 246, 85);
			
			$alto_imagen = imagesy($im)-10;
			$ancho_imagen = imagesx($im)-40;
					    
		    // Escribir la cadena en la parte superior izquierda
			imagestring($im, 0, 3, $alto_imagen, date('Y-m-d'), $color_texto);
			imagestring($im, 0, $ancho_imagen, $alto_imagen, date('H:i:s'), $color_texto);
		    
		    imagepng($im);
		    imagedestroy($im);
		    
			/*ob_start ();
			
			imagejpeg ($im);
			$image_data = ob_get_contents ();
			
			ob_end_clean ();
			
			$image_data_base64 = base64_encode ($image_data);
			echo $image_data_base64;*/
		}
		else {
		    echo 'Ocurrió un error.';
		}
		exit();
	}
	
	public function importar_digitacion(){
		$this->layout = 'home_tree';
		$this->components[] = 'RequestHandler';
		$id_usuario = $this->obj_logged_user['Usuario']['id'];
		
		$this->set(compact('id_usuario'));	
	}
	
	public function importar_file_digitacion(){
		$this->autoRender = false;
		ini_set('memory_limit', '-1');
		$this->layout = 'ajax';
		if ($this->request->is('post')) {

			/* Upload archivo xml al servidor*/
			$temp = explode(".", $_FILES["file"]["name"]);
			$extension = end($temp);
			if ($_FILES["file"]["error"] > 0) {
				echo "ERROR: " . $_FILES["file"]["error"] . "";
			} else {
				if($extension=='zip' or $extension=='xls'){
					move_uploaded_file($_FILES["file"]["tmp_name"], "files/XLS/" . $_FILES["file"]["name"]);
					if ($extension=='zip'){
						$zip = new ZipArchive;
						if ($zip->open('files/XLS/'.$_FILES["file"]["name"], ZipArchive::CREATE)==TRUE) {
							$filename = $zip->getNameIndex(0);
							$temp_zip = explode(".",trim($filename));
							$extension_in_zip = end($temp_zip);
							if($extension_in_zip == 'xls'){
								$xml_upload = $temp[0].'.xls';
								$zip->extractTo('files/XLS/');
								$zip->close();
							}else{
								echo 'Error descomprimiendo zip, no es un archivo XLS';
								exit;
							}
						} else {
							echo 'Error descomprimiendo zip';
							exit;
						}
					}
				}else{
					echo 'No es un archivo Valida, solo esta permitido Xls o Zip de Xls';
					exit;
				}
			}
			
			$negocio = $this->Session->read('NEGOCIO');
			 
			$data = new Spreadsheet_Excel_Reader('files/XLS/'.$temp[0].'.xls', true);
			$temp = $data->dumptoarray();
			if($temp){

				$arr_lecturas = array();
				foreach ($temp as $key=>$valor){
					if($key > 1){
						$i=1;
						foreach ($valor as $value){
							$val[$i] = addslashes($value);
							$i++;
						}
						$suministro = trim($val['1']);
						$lecturao = trim($val['2']);
						$obs = trim($val['3']);
						
						$arr_lecturas_tmp = array();
						$arr_lecturas_tmp['Suministro'] = (int) $suministro;
						$arr_lecturas_tmp['LecturaO'] = $lecturao;
						$arr_lecturas_tmp['CodLectura'] = $obs;
						$arr_lecturas_tmp['tipolectura'] = 'L';
						$arr_lecturas_tmp['Codigo2'] = '';
						$arr_lecturas_tmp['fotografia'] = '';
						$arr_lecturas_tmp['Consumo'] = '';						
						$arr_lecturas_tmp['Metodo'] = '1';
						$arr_lecturas_tmp['fecha'] = date("Y-m-d H:i:s");
						$arr_lecturas_tmp['Direccion'] = '';
						if($lecturao!='' || $obs!=''){
                            $bool_data = true;
                            if($lecturao!='' && !is_numeric($lecturao)){
                                $bool_data = false;
                            }
                            if($obs!='' && !is_numeric($obs)){
                                $bool_data = false;
                            }
                            if($bool_data){
    							$arr_lecturas[] = $arr_lecturas_tmp;
    							CakeLog::write('lecturas_import_from_excel_'.$negocio, json_encode($arr_lecturas_tmp));
                            }else{
                                CakeLog::write('lecturas_import_fail_from_excel_'.$negocio, json_encode($arr_lecturas_tmp));    
                            }
						}else{
							CakeLog::write('lecturas_import_fail_from_excel_'.$negocio, json_encode($arr_lecturas_tmp));
						}
					}
				}
				
				$params = json_encode($arr_lecturas);

				if($negocio=='TAC'){
					$data_login = $this->Components->load('PostWebservice')->sendJson('http://54.200.245.17/Ws.Lecturas/api/tacna/index.php/saveot', $params);
				}elseif($negocio=='TRUX'){
					$data_login = $this->Components->load('PostWebservice')->sendJson('http://54.200.245.17/PEXPORT/Ws.Lecturas/api/cix/index.php/saveot', $params);
				}elseif($negocio=='TRUX_VALLE'){
					$data_login = $this->Components->load('PostWebservice')->sendJson('http://54.200.245.17/PEXPORT/trux/Ws.Lecturas/api/cix/index.php/saveot', $params);
				}elseif($negocio=='CAJ'){
					$data_login = $this->Components->load('PostWebservice')->sendJson('http://54.200.245.17/PEXPORT/caj/Ws.Lecturas/api/cix/index.php/saveot', $params);
				}else{
					$domais = Configure::read('lecturas.domains');
					$ip_server = $domais[$negocio]['ip'];
					$name_server = $domais[$negocio]['name'];
					
					$data_login = $this->Components->load('PostWebservice')->sendJson('http://'.$ip_server.'/Ws.Lecturas/api/v2/'.strtolower($negocio).'/saveot', $params);
				}
				
				$this->loadModel('ComlecOrdenlectura');
				$this->ComlecOrdenlectura->sanearMovil();
				
				echo $data_login;
				$this->redirect(array('controller' => 'MonitorearLecturas','action' => 'monitorear_lectura'));
				exit;
			}
		}
	}
	
	public function cronograma(){
		
		$this->layout = 'home_tree';
		$this->components[] = 'RequestHandler';
		$id_usuario = $this->obj_logged_user['Usuario']['id'];
		
		$listar_pfacrura = $this->ComlecOrdenlectura->listarPFactura();
		if(count($listar_pfacrura)>0){
			$options_periodo_anterior['pfactura'] = $listar_pfacrura[0][0]['pfactura'];
		}else{
			$options_periodo_anterior['pfactura'] = '0';
		}
		
		$arr_ciclos_actuales = $this->ComlecOrdenlectura->getCiclosActuales();
	
		$listar_ciclo = $this->ComlecOrdenlectura->listarCicloCronograma($options_periodo_anterior['pfactura']);
		$listar_sector = $this->ComlecOrdenlectura->listarSectorCronograma($options_periodo_anterior['pfactura']);
		
		$listar_unidadneg = $this->ComlecOrdenlectura->listarUnidadneg($options_periodo_anterior['pfactura']);
	
		$this->set(compact('id_usuario','listar_ciclo','listar_sector','listar_unidadneg','arr_ciclos_actuales'));
	
	}
	
	public function calendario_cronograma($criterio=null,$date_start=null,$date_end=null,$unidadneg=null,$ciclo=null,$sector=null){
		$this->layout = 'ajax';
		if(!isset($criterio)){
			$criterio = '';
		}
		if(!isset($date_start)){
			$date_start = date('Y-m-d', strtotime("-1 day"));
		}
		if(!isset($date_end)){
			$date_end = date('Y-m-d');
		}
		 
		$listarXml = $this->ComlecOrdenlectura->listarCronograma($criterio,$date_start,$date_end,$unidadneg,$ciclo,$sector);
		//   var_dump($listarXml);exit;
		$arr_cronograma_ids = array();
		foreach ($listarXml as $k => $v){
			$arr_cronograma_ids[] = $v[0]['id'];
		}
		$listar_kpi_cronograma = $this->ComlecOrdenlectura->getKpiByCronograma($arr_cronograma_ids);
		$arr_kpi_cronograma = array();
		foreach ($listar_kpi_cronograma as $kk =>$vv){
			$arr_kpi_cronograma[$vv[0]['id']] = $vv;	
		}
		
		$arr_no_programados = $this->ComlecOrdenlectura->noProgramadosCronograma($criterio,$date_start,$date_end,$unidadneg,$ciclo,$sector);
		
		$this->set(compact('listarXml','arr_kpi_cronograma','arr_no_programados'));
		
	}
	
	public function calendario_cronograma_by_ciclos($criterio=null,$date_start=null,$date_end=null,$unidadneg=null,$ciclo=null,$sector=null){
		$this->layout = 'ajax';
		if(!isset($criterio)){
			$criterio = '';
		}
		if(!isset($date_start)){
			$date_start = date('Y-m-d', strtotime("-1 day"));
		}
		if(!isset($date_end)){
			$date_end = date('Y-m-d');
		}
			
		$listarXml = $this->ComlecOrdenlectura->listarCronograma($criterio,$date_start,$date_end,$unidadneg,$ciclo,$sector);
		//   var_dump($listarXml);exit;
		$arr_cronograma_ids = array();
		foreach ($listarXml as $k => $v){
			$arr_cronograma_ids[] = $v[0]['id'];
		}
		$listar_kpi_cronograma = $this->ComlecOrdenlectura->getKpiByCronograma($arr_cronograma_ids);
		$arr_kpi_cronograma = array();
		foreach ($listar_kpi_cronograma as $kk =>$vv){
			$arr_kpi_cronograma[$vv[0]['id']] = $vv;
		}
	
		$arr_no_programados = $this->ComlecOrdenlectura->noProgramadosCronograma($criterio,$date_start,$date_end,$unidadneg,$ciclo,$sector);
	
		$this->set(compact('listarXml','arr_kpi_cronograma','arr_no_programados'));
	
	}
		
	public function listar_cronograma($criterio=null,$date_start=null,$date_end=null,$unidadneg=null,$ciclo=null,$sector=null){
		$this->layout = 'ajax';
		if(!isset($criterio)){
			$criterio = '';
		}
		if(!isset($date_start)){
			$date_start = date('Y-m-d', strtotime("-1 day"));
		}
		if(!isset($date_end)){
			$date_end = date('Y-m-d');
		}
			
		$listarXml = $this->ComlecOrdenlectura->listarCronograma($criterio,$date_start,$date_end,$unidadneg,$ciclo,$sector);
		//   var_dump($listarXml);exit;
	
		$tabla='';
		$j=0;
	
	
		foreach ($listarXml as $obj1){
			$tabla.='[';
			//   $i=0;
	
			$tabla.= '"'.$obj1[0]['id'].'"';
			$tabla.= ',"'.$obj1[0]['pfactura'].'"';
			$tabla.= ',"'.$obj1[0]['idciclo'].'"';
			$tabla.= ',"'.$obj1[0]['idsector'].'"';
			$tabla.= ',"'.substr($obj1[0]['fecha'],0,10).'"';
	
			$tabla.= ',"<a class=\'btn btn-info btn-sm btn fancybox fancybox.iframe\' href=\"'.ENV_WEBROOT_FULL_URL.'ComlecOrdenlecturas/ajax_resumen_cronograma/'.$obj1[0]['id'].'\">Detalle</a>"';
	
			$tabla.=']';
			if ($j<count($listarXml)-1){
				$tabla.=',';
			}
			$j=$j+1;
		};
	
		$vari='{
	
	"aaData":[';
		$vari.=$tabla;
		$vari.=']}';
		echo $vari;exit;
	}
		
	public function save_cronograma() {
		$this->layout = 'ajax';
		ini_set('memory_limit', '-1');
		ini_set('max_execution_time', 300000);
		set_time_limit(0);
		/* Upload archivo xml al servidor*/
	
		$allowedExts = array("xml");
		$temp = explode(".", $_FILES["file"]["name"]);
		$extension = end($temp);
	
		$respuesta_existe=false;
		// if ((($_FILES["file"]["type"] == "text/xml")) && in_array($extension, $allowedExts)) {
		if ($_FILES["file"]["error"] > 0) {
			echo "ERROR: " . $_FILES["file"]["error"] . "";
		} else {
	
			// if (file_exists("XML/" . $_FILES["file"]["name"])) {
			//    $respuesta_existe=true;
			 
			// } else {
			$respuesta_existe=false;
			move_uploaded_file($_FILES["file"]["tmp_name"], "files/XML/" . $_FILES["file"]["name"]);
	
			if ($extension=='zip'){
				$zip = new ZipArchive;
				$res = $zip->open('files/XML/'.$_FILES["file"]["name"]);
				$filename_in_zip = $zip->getNameIndex(0);
				//var_dump($res);exit;
				if ($res === TRUE) {
					$temp_zip = explode(".",trim($filename_in_zip));
					$extension_in_zip = end($temp_zip);
					if($extension_in_zip == 'xml'){
						$xml_upload = $temp[0].'.xml';
						$comlec_xml = $this->ComlecOrdenlectura->comlecXml($xml_upload);
						 
						if ($comlec_xml == '1') {
							$respuesta_existe=true;
						}else{
							$zip->extractTo('files/XML/');
							$zip->close();
							$respuesta_existe=false;
						}
					}elseif($extension_in_zip == 'xls'){
						$xml_upload = $temp[0].'.xls';
						$comlec_xml = $this->ComlecOrdenlectura->comlecXml($xml_upload);
	
						if ($comlec_xml == '1') {
							$respuesta_existe=true;
						}else{
							$zip->extractTo('files/XML/');
							$zip->close();
							$respuesta_existe=false;
						}
					}
				} else {
					echo 'Error descomprimiendo zip';
					exit;
				}
			}elseif($extension == 'xml'){
				$xml_upload = $temp[0].'.xml';
				$comlec_xml = $this->ComlecOrdenlectura->comlecXml($xml_upload);
	
				if ($comlec_xml == '1') {
					$respuesta_existe=true;
				} else{
					$respuesta_existe=false;
				}
			}elseif($extension == 'xls'){
				$xml_upload = $temp[0].'.xls';
				$comlec_xml = $this->ComlecOrdenlectura->comlecXml($xml_upload);
	
				if ($comlec_xml == '1') {
					$respuesta_existe=true;
				} else{
					$respuesta_existe=false;
				}
			}
			// }
		}
	
	
		if ($respuesta_existe==true){
			echo "Archivo existe";
			exit;
		}
		 
		if($extension == 'xml' or (isset($extension_in_zip) && $extension_in_zip == 'xml')){
			$content = utf8_encode(file_get_contents('files/XML/'.$temp[0].'.xml'));
			$xml = simplexml_load_string($content);
			/* Lectura del archivo xml desde su ubicacion en el servidor*/
			$this->loadModel('Usuario');
			$grupo_id= $this->Usuario->IdGrupoByUsuario($this->obj_logged_user['Usuario']['id']);
			/* Lectura de los conceptos que se encuentran relacionados segun la unidad del negocio */
			/* Se simulara un id del negocio (1) */
			/* El id del negocio de simulacion en el futuro sera obtenido desde la sesion */
			$conceptosByGrupo = $this->ComlecOrdenlectura->conceptosByGrupo($grupo_id);
			 
			/* Crear tabla orden de lectura en funcion a los conceptos obtenidos */
			/* Nomenclatura de la tabla lecturas comlec_ordenlecturas{id de unidad de negocio} */
			/* La nomenclatura de la tabla permitira trabajar con varias unidades del negocio al mismo tiempo */
	
			/*insertar uml en la nueva tabla creada*/
			$insertaruml = $this->ComlecOrdenlectura->insertarXmlCronograma($conceptosByGrupo,$grupo_id,$xml,$_FILES["file"]["name"],$this->obj_logged_user['Usuario']['id'],$this->request->clientIp());
		}elseif($extension == 'xls' or (isset($extension_in_zip) && $extension_in_zip == 'xls')){
			 
			$data = new Spreadsheet_Excel_Reader('files/XML/'.$temp[0].'.xls', true);
			$temp = $data->dumptoarray();
			if($temp){
				/* Lectura del archivo xml desde su ubicacion en el servidor*/
				$this->loadModel('Usuario');
				$grupo_id= $this->Usuario->IdGrupoByUsuario($this->obj_logged_user['Usuario']['id']);
				/* Lectura de los conceptos que se encuentran relacionados segun la unidad del negocio */
				/* Se simulara un id del negocio (1) */
				/* El id del negocio de simulacion en el futuro sera obtenido desde la sesion */
				$conceptosByGrupo = $this->ComlecOrdenlectura->conceptosByGrupo($grupo_id);
	
				/* Crear tabla orden de lectura en funcion a los conceptos obtenidos */
				/* Nomenclatura de la tabla lecturas comlec_ordenlecturas{id de unidad de negocio} */
				/* La nomenclatura de la tabla permitira trabajar con varias unidades del negocio al mismo tiempo */
	
				/*insertar uml en la nueva tabla creada*/
				$insertaruml = $this->ComlecOrdenlectura->insertarXlsCronograma($conceptosByGrupo,$grupo_id,$temp,$_FILES["file"]["name"],$this->obj_logged_user['Usuario']['id'],$this->request->clientIp());
			}
		}
	
	
		if (@$insertaruml){
	
			$hola="archivo subido correctamente";
			$this->set(compact('hola'));
			$this->redirect(array('action' => 'cronograma'));
	
		}
		else
		{
			$hola="error al subir archivo";
			$this->set(compact('hola'));
			echo  'Error al subir archivo';
			exit;
		}
	}
    
		
	public function ajax_resumen_cronograma($id_cronograma){
		$this->layout = 'home_tree';
		 
		$obj_cronograma = $this->ComlecOrdenlectura->listarCronogramaById($id_cronograma);
		$cantidad_suministros = $this->ComlecOrdenlectura->getCountSuministrosByCronograma($id_cronograma);
		$cantidad_asignados = $this->ComlecOrdenlectura->getCountAsignadosByCronograma($id_cronograma);
		$cantidad_descargados = $this->ComlecOrdenlectura->getCountDescargadosByCronograma($id_cronograma);
		$cantidad_finalizados = $this->ComlecOrdenlectura->getCountFinalizadosByCronograma($id_cronograma);
		$cantidad_consistentes = $this->ComlecOrdenlectura->getCountConsistentesByCronograma($id_cronograma);
		$cantidad_inconsistentes = $this->ComlecOrdenlectura->getCountInconsistentesByCronograma($id_cronograma);
		$cantidad_enviadas_distriluz = $this->ComlecOrdenlectura->getCountEnviadasDistriluzByCronograma($id_cronograma);
		//$cantidad_sin_lectura_y_obs = $this->ComlecOrdenlectura->getCountSinLecturayObsByCronograma($id_cronograma);
		$cantidad_sin_lectura_y_obs = 0;
		$cantidad_obs_sin_lectura = $this->ComlecOrdenlectura->getCountObs1SinLecturaByCronograma($id_cronograma);
		$cantidad_obs_con_lectura = $this->ComlecOrdenlectura->getCountObs1ConLecturaByCronograma($id_cronograma);
		$cantidad_consumo_cero = $this->ComlecOrdenlectura->getCountConsumoCeroByCronograma($id_cronograma);
		$cantidad_consumo_menor35 = $this->ComlecOrdenlectura->getCountConsumoMenoresQueByCronograma($id_cronograma);
		$cantidad_consumo_mayor100_porciento = $this->ComlecOrdenlectura->getCountConsumoMayor100PorcientoQueAnteriorByCronograma($id_cronograma);
		$cantidad_consumo_mayor1000 = $this->ComlecOrdenlectura->getCountConsumoMayoresQueByCronograma($id_cronograma);
		$cantidad_obs_99 = $this->ComlecOrdenlectura->getCountObs1IgualAByCronograma($id_cronograma,99);
		 
		$this->set(compact('id_cronograma','obj_cronograma','cantidad_suministros','cantidad_asignados','cantidad_descargados','cantidad_finalizados',
				'cantidad_consistentes','cantidad_inconsistentes','cantidad_enviadas_distriluz','cantidad_sin_lectura_y_obs','cantidad_obs_sin_lectura','cantidad_obs_con_lectura',
				'cantidad_consumo_cero','cantidad_consumo_menor35','cantidad_consumo_mayor100_porciento','cantidad_consumo_mayor1000',
				'cantidad_obs_99'));
	}
	
	public function save_programar_evento_cronograma($unidad_negocio,$fecha,$idciclo,$idsector){
		$this->layout = 'ajax';
		
		$obj_evento_cronograma = $this->ComlecOrdenlectura->existeEventoCronograma($unidad_negocio,date('Ym'),$idciclo,$idsector);
		if(count($obj_evento_cronograma)==0){
			
			$existe_sector_para_ciclo = $this->ComlecOrdenlectura->existeSectorParaCiclo($unidad_negocio,null,$idciclo,$idsector);
			if($existe_sector_para_ciclo>0){
				$this->ComlecOrdenlectura->saveProgramarEventoCronograma($unidad_negocio,date('Ym'),$fecha,$idciclo,$idsector,$this->obj_logged_user['Usuario']['id'],$this->request->clientIp());
					
				echo json_encode(array('success'=>true,'message'=>'Guardado Correctamente.'));
			}else{
				echo json_encode(array('success'=>false,'message'=>'El Sector no pertenece al Ciclo.','id_cronograma'=>''));
			}			
		}else{
			echo json_encode(array('success'=>false,'message'=>'Ya existe el evento programado para el día '.substr($obj_evento_cronograma[0][0]['fecha'],0,10).'.','id_cronograma'=>$obj_evento_cronograma[0][0]['id']));
		}
		
		
		exit;
	}
	
	public function save_actualizar_evento_cronograma($id_cronograma,$unidad_negocio,$fecha,$idciclo,$idsector){
		$this->layout = 'ajax';
	
		$this->ComlecOrdenlectura->saveActualizarEventoCronograma($id_cronograma,$unidad_negocio,date('Ym'),$fecha,$idciclo,$idsector,$this->obj_logged_user['Usuario']['id'],$this->request->clientIp());
			
		echo json_encode(array('success'=>true,'message'=>'Guardado Correctamente.'));
	
		exit;
	}
	
	public function eliminar_evento_cronograma($id_cronograma){
		$this->layout = 'ajax';
		
		$this->ComlecOrdenlectura->eliminarEventoCronograma($id_cronograma);
		
		echo json_encode(array('success'=>true,'message'=>'Guardado Correctamente.'));
		exit;
	}
	
	public function save_commet_after_update_lectura_distriluz(){
		$arr_return = array();
		$msg = '';
		$success = true;
		if (!empty($this->data)) {
			$suministro = $this->request->data['suministro'];
			$comentario = $this->request->data['comentario'];
			$lectant = $this->request->data['lectant'];
			$lectact = $this->request->data['lectact'];
			$obsant = $this->request->data['obsant'];
			$obsact = $this->request->data['obsact'];
			$resultadoevaluacionant = $this->request->data['resultadoevaluacionant'];
			$resultadoevaluacion = $this->request->data['resultadoevaluacion'];
			$id_usuario = $this->obj_logged_user['Usuario']['id'];
				
			if($comentario != ''){
				
				$negocio = $this->Session->read('NEGOCIO');
				$this->loadModel('ComlecOrdenlectura');
				$this->ComlecOrdenlectura->setDataSource (strtolower($negocio));
				$obj_suministro = $this->ComlecOrdenlectura->findBySuministro($suministro);
				
				$comentario2 = $comentario.' - Lectura Anterior:'.$lectant.' Lectura Nueva:'.$lectact.' Obs. Anterior: '.$obsant.' Obs. Nueva:'.$obsact.' Resultado Anterior: '.$resultadoevaluacionant.' Resultado Nuevo: '.$resultadoevaluacion;
				
				if($success == true && $this->ComlecOrdenlectura->insertarReporteHistoricoComentario($suministro, $obj_suministro['ComlecOrdenlectura']['pfactura'], $comentario2, '', $id_usuario, $this->request->clientIp())){
					$success = true;
					
					if(isset($this->obj_logged_user['Usuario']['email']) && $this->obj_logged_user['Usuario']['email']!=''){
					
						$email = new CakeEmail('gmail');
							
						$email->to(array($this->obj_logged_user['Usuario']['email'],'tw.6.3991393.830@replies.teamwork.com'));
						//$email->cc('tw.6.3991393.830@replies.teamwork.com');
						$email->subject('sistema de lectura: Cambio de datos de Suministro, después de envío a Distriluz');
						//$this->Email->replyTo = 'support@ejemplo.com';
						//$email->from('ddanalytics05@gmail.com');
						$email->template('send_cambio_datos_suministro','default'); // NOTAR QUE NO HAY '.ctp'
						$email->emailFormat('html');
						//Variables de la vista
						$email->viewVars(array('suministro' => $suministro,
						'comentario' => $comentario,
						'lectant' => $lectant,
						'lectact' => $lectact,
						'obsant' => $obsant,
						'obsact' => $obsact,
						'resultadoevaluacionant' => $resultadoevaluacionant,
						'resultadoevaluacion' => $resultadoevaluacion,
						'fecha' => date('Y-m-d H:i:s'),
						'usuario' => $this->obj_logged_user['Usuario']['nomusuario']));
						
						$email->send();
					}
				}
			}else{
				$success = false;
				$msg = 'Debe ingresar un comentario';
			}
		}else{
			$success = false;
			$msg = 'Usted esta ingresando de forma indebida';
		}
		$arr_return['success'] = $success;
		$arr_return['msg'] = $msg;
		echo json_encode($arr_return);
		exit();
	}
	
	public function get_last_event_update_lectura(){
		$this->layout = 'ajax';
	
		$date_last_update = $this->ComlecOrdenlectura->getLastEventUpdateLectura();
		
		$str_date_last_update = $this->getTiempoString($date_last_update, date("Y-m-d H:i:s"));
		
		if($date_last_update > $this->Session->read('date_last_update_lectura')){
			echo json_encode(array('success'=>true,'modified'=>true,'date_last_update'=>substr($date_last_update,0,19),'str_tiempo'=>trim($str_date_last_update)));
		}else{
			echo json_encode(array('success'=>true,'modified'=>false,'date_last_update'=>substr($date_last_update,0,19),'str_tiempo'=>trim($str_date_last_update)));
		}
		$this->Session->write('date_last_update_lectura',$date_last_update);
		exit;
	}
	
	/**
	 * Formulario de Asignacion de Personal para la Toma de Lecturas (Asignacion basada en Historico)
	 * @author Geynen
	 * @version 30 Enero 2015
	 */
	public function form_asignar_basado_historico($unidad_neg, $idciclo, $idsector, $idruta){
		$this->layout = 'ajax';
		
		$listar_pfacrura = $this->ComlecOrdenlectura->listarPFactura();
		if(count($listar_pfacrura)>0){
			$pfactura_anterior = $listar_pfacrura[0][0]['pfactura'];
			$listarLibros_anterior = $this->ComlecOrdenlectura->listaLibrosV2($idciclo, $idsector, $idruta, $pfactura_anterior);
		}else{
			$listarLibros_anterior = array();
		}
				
		$listarLibros = $this->ComlecOrdenlectura->listaLibrosV2($idciclo, $idsector, $idruta);
		
		$sum_periodo_actual = $this->ComlecOrdenlectura->getCount(array('unidad_neg'=>$unidad_neg,'idciclo'=>$idciclo,'idsector'=>$idsector,'idruta'=>$idruta));
		$max_orden_periodo_actual = $this->ComlecOrdenlectura->getMaxOrdenRuta(array('unidad_neg'=>$unidad_neg,'idciclo'=>$idciclo,'idsector'=>$idsector,'idruta'=>$idruta));
		$sum_asignados_periodo_actual = $this->ComlecOrdenlectura->getCountAsignados(array('unidad_neg'=>$unidad_neg,'idciclo'=>$idciclo,'idsector'=>$idsector,'idruta'=>$idruta));
		
		$model_ComlecOrdenlectura = $this->ComlecOrdenlectura;
		
		$this->loadModel('GlomasEmpleado');
		$listar_empleados = $this->GlomasEmpleado->ListarEmpleados();
		
		$this->set(compact('listarLibros_anterior','listarLibros','listar_empleados',
				'sum_periodo_actual','max_orden_periodo_actual','sum_asignados_periodo_actual',
				'unidad_neg', 'idciclo', 'idsector', 'idruta', 'model_ComlecOrdenlectura'));
	}
	
	/**
	 * Formulario de Asignacion de Personal para la Toma de Lecturas Por Sector
	 * @author Geynen
	 * @version 02 Febrero 2015
	 */
	public function form_asignar_por_sector($unidad_neg, $idciclo, $idsector){
		$this->layout = 'ajax';
	
		$listarLibros = $this->ComlecOrdenlectura->listaRutasParaAsignacion($idciclo, $idsector);
	
		$sum_periodo_actual = $this->ComlecOrdenlectura->getCount(array('unidad_neg'=>$unidad_neg,'idciclo'=>$idciclo,'idsector'=>$idsector));
		$min_orden_periodo_actual = $this->ComlecOrdenlectura->getMinOrdenRuta(array('unidad_neg'=>$unidad_neg,'idciclo'=>$idciclo,'idsector'=>$idsector));
		$max_orden_periodo_actual = $this->ComlecOrdenlectura->getMaxOrdenRuta(array('unidad_neg'=>$unidad_neg,'idciclo'=>$idciclo,'idsector'=>$idsector));
		$sum_asignados_periodo_actual = $this->ComlecOrdenlectura->getCountAsignados(array('unidad_neg'=>$unidad_neg,'idciclo'=>$idciclo,'idsector'=>$idsector));
	
		$model_ComlecOrdenlectura = $this->ComlecOrdenlectura;
	
		$this->loadModel('GlomasEmpleado');
		$listar_empleados = $this->GlomasEmpleado->ListarEmpleados();
	
		$this->set(compact('listarLibros','listar_empleados','sum_periodo_actual',
				'min_orden_periodo_actual','max_orden_periodo_actual','sum_asignados_periodo_actual',
				'unidad_neg', 'idciclo', 'idsector', 'idruta', 'model_ComlecOrdenlectura'));
	}
	
	/**
	 * Guarda la asignacion de lecturas por sector
	 * @author Geynen
	 * @version 03 Febrero 2015
	 */
	public function guardarlecturistalibro_por_sector(){
		$this->layout = 'ajax';
		$this->loadModel('Usuario');
		$grupo_id= $this->Usuario->IdGrupoByUsuario($this->obj_logged_user['Usuario']['id']);
		$lecturista= $this->request->data['lecturista'];
	
		$ciclo= $this->request->data['ciclo'];
		$sector= $this->request->data['sector'];
		$negocio= $this->request->data['negocio'];
		$fecha= $this->request->data['fecha'];
		
		$listarLibros = $this->ComlecOrdenlectura->listaRutasParaAsignacion($ciclo, $sector);
		foreach ($listarLibros as $obj1){
			$desde = $obj1[0]['min'];
			$hasta = $obj1[0]['max'];
			$ruta = $obj1[0]['ruta'];
			$create_libro = $this->ComlecOrdenlectura->guardarLecturistaLibro($desde, $hasta, $lecturista, $ciclo, $sector, $ruta, $grupo_id, $negocio, $fecha);
		}		
		exit;
	}
}
