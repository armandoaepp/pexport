<?php
/**
 * Application level Controller
 *
 * This file is application-wide controller file. You can put all
 * application-wide controller-related methods here.
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

App::uses('Controller', 'Controller');
App::uses('CakeTime', 'Utility');

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @package		app.Controller
 * @link		http://book.cakephp.org/2.0/en/controllers.html#the-app-controller
 */
class AppController extends Controller {
	public $helpers = array('Session');
	
	public $components = array(
		'Session',
		'Auth' => array(
		    'authenticate' => array(
		        'Form' => array( 
		            'userModel' => 'Usuario', 
		            'fields' => array( 
		                'username' => 'nomusuario', 
		                'password' => 'passusuario'
		            ), 
		        ), 
		     ), 
		     'loginRedirect' => array('controller' => 'ComlecOrdenlecturas', 'action' => 'listar_asignacion'), 
		     'logoutRedirect' => array('controller' => 'usuarios', 'action' => 'login'), 
		     'loginAction' => array('controller' => 'usuarios', 'action' => 'index'), 
		),
		'UserPermissions.UserPermissions'
	);
	
	public $obj_logged_user = null;
	
    function beforeFilter(){
        //$this->disableCache();
        if($this->Auth->user()){
        	$this->obj_logged_user =  $this->Auth->user();
        	$this->set('obj_logged_user', $this->obj_logged_user);
        }
                
        $this->set('NEGOCIO',$this->Session->read('NEGOCIO'));
    }
    
    function getTiempoString($oldTime, $newTime){
    
    	$tiempo_segundos = strtotime($newTime) - strtotime($oldTime);
    
    	$str_dias = 0;
    	$str_horas = 0;
    	$str_minutos = 0;
    	if($tiempo_segundos>36400){
    		$str_dias = floor ($tiempo_segundos/36400);
    		$tiempo_segundos = $tiempo_segundos%36400;
    	}
    	if($tiempo_segundos>3600){
    		$str_horas = floor ($tiempo_segundos/3600);
    		$tiempo_segundos = $tiempo_segundos%3600;
    	}
    	if($tiempo_segundos>60){
    		$str_minutos = floor ($tiempo_segundos/60);
    		$tiempo_segundos = floor ($tiempo_segundos%60);
    	}
    
    	$str_horas      = str_pad( $str_horas, 2, "0", STR_PAD_LEFT );
    	$str_minutos    = str_pad( $str_minutos, 2, "0", STR_PAD_LEFT );
    	$tiempo_segundos= str_pad( $tiempo_segundos, 2, "0", STR_PAD_LEFT );
    
    	return ($str_dias>0?$str_dias.' días ':' ').($str_horas>0?$str_horas.' horas ':' ').($str_minutos>0?$str_minutos.' minutos ':' ').($tiempo_segundos>0?$tiempo_segundos.' segundos':' ');
    }
}
