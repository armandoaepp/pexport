<?php 
App::uses('AppController', 'Controller');
App::uses('Sanitize', 'Utility');
App::uses('CakeTime', 'Utility');

class ReportesController extends AppController {

	public $helpers = array('Html', 'Form');
	public $name = 'Reportes';
	public $components = array('RequestHandler');
	
	public function beforeFilter(){
		$this->Auth->allow(array('reporte_historico','mapa_literal','carta_pdf','listar_personal','listar_personal_by_negocio'));
		parent::beforeFilter();
	}

	public function reporte_artefactos() {
		$this->layout = 'home_tree';
		$arr_suministros = array();
		if ($this->request->is('post')){
			if(isset($this->request->data['txt_search_by_suministro_artefacto'])){
				$suministros = $this->request->data['txt_search_by_suministro_artefacto']; 
			}else{
				$suministros = 0;
			}
			

			$porciones = explode(",", $suministros);
			$c=count($porciones);
			
			$porcion='';
			for ($i=0; $i < $c; $i++) {
					$porcion.=",'".$porciones[$i]."'";
			}			
			$suministros = substr($porcion, 1);
			
			$this->loadModel('ComlecOrdenlectura');
			$arr_suministros  = $this->ComlecOrdenlectura->listarArtefactosQuemadosBySuministros($suministros);

			$this->set(compact('arr_suministros'));
		}
		$this->set(compact('arr_suministros'));
	}

	public function reporte_historico($suministro=null){
		$this->layout = 'home_tree';
		date_default_timezone_set('America/Lima');
		setlocale(LC_ALL,"es_ES");
		if(isset($suministro)){
			$modulo = 'historico';
		}elseif(isset($this->request->data['txt_search_by_suministro_historico'])){
			$suministro = $this->request->data['txt_search_by_suministro_historico'];
			$modulo = $this->request->data['modulo'];
		}else{
			$suministro = 0;
			$modulo = 'historico';
		}
		
		$id_usuario = $this->obj_logged_user['Usuario']['id'];
		$this->loadModel('ComlecOrdenlectura');
		
		$arr_suministros = array();
		$obj_suministro = array();
		$arr_comentarios = array();
		$obj_comentarios = array();
		if($suministro!=0){
			$obj_suministro = $this->ComlecOrdenlectura->buscarSuministroHistorico($suministro);
			$arr_suministros = $this->ComlecOrdenlectura->buscarSuministroHistorico($suministro,'all');
			
			if(count($obj_suministro)>0){
				$pfactura = $obj_suministro[0]['0']['pfactura'];				
			}elseif(count($arr_suministros)>0){
				$pfactura = $arr_suministros[0][0]['pfactura'];
			}else{
				$pfactura = date('Ym');
			}
			$obj_comentarios = $this->ComlecOrdenlectura->listarReporteHistoricoComentario($suministro, $pfactura);
			$arr_comentarios = $this->ComlecOrdenlectura->buscarSuministroHistoricoComentario($suministro,'all');
		}
		
		$this->set(compact('obj_comentarios','arr_comentarios','suministro','obj_suministro','arr_suministros','id_usuario'));
		
	}
	
	public function resumen_historico_pdf($suministro = null){
		date_default_timezone_set('America/Lima');
		setlocale(LC_ALL,"es_ES");
	
		$id_usuario = $this->obj_logged_user['Usuario']['id'];
		$this->loadModel('ComlecOrdenlectura');
	
		$arr_suministros = array();
		$obj_suministro = array();
		$arr_comentarios = array();
		$obj_comentarios = array();
		if($suministro!=0){
			$obj_suministro = $this->ComlecOrdenlectura->buscarSuministroHistorico($suministro);
			$arr_suministros = $this->ComlecOrdenlectura->buscarSuministroHistorico($suministro,'all');
				
			if(count($obj_suministro)>0){
				$pfactura = $obj_suministro[0]['0']['pfactura'];				
			}elseif(count($arr_suministros)>0){
				$pfactura = $arr_suministros[0][0]['pfactura'];
			}else{
				$pfactura = date('Ym');
			}
			$obj_comentarios = $this->ComlecOrdenlectura->listarReporteHistoricoComentario($suministro, $pfactura);
		}
		
		$negocio = $this->Session->read('NEGOCIO');
		$logo_negocios = Configure::read('lecturas.logo_negocios');
		$logo_negocio = $logo_negocios[strtoupper($negocio)];
	
		$this->set(compact('obj_comentarios','obj_suministro','arr_suministros','id_usuario','logo_negocio'));
	}
	
	public function insertar_comentario($suministro=null,$pfactura=null,$comentario=null){
		$arr_return = array();
		$msg = '';
		$success = true;
		if (!empty($this->data)) {
			$suministro = $this->request->data['suministro'];
			$pfactura = $this->request->data['pfactura'];
			$comentario = $this->request->data['comentario'];
			$id_usuario = $this->obj_logged_user['Usuario']['id'];
			
			if($comentario != ''){
				if(isset($_FILES['archivo']['name']) && $_FILES['archivo']['name'] != ''){
					$nombre_archivo = $_FILES['archivo']['name'];
					$file = explode(".",$nombre_archivo);
					$type_file = $file[1];
					$imagen_name = null;
					$type_images = array("jpg", "jpeg", "png", "gif", "wmp");
					if (in_array($type_file, $type_images)) {
						$tmp_archivo = $_FILES['archivo']['tmp_name'];
						$imagen_name = mktime() . $nombre_archivo;
						$archivador = WWW_ROOT . 'images/upload/' . $imagen_name;
						move_uploaded_file($tmp_archivo, $archivador);
					}else{						
						$success = false;
						$msg = 'Usted debe ingresar un formato correcto de imagen';
					}
				}
				
				$this->loadModel('ComlecOrdenlectura');
				if($success == true && $this->ComlecOrdenlectura->insertarReporteHistoricoComentario($suministro, $pfactura, $comentario, $imagen_name, $id_usuario, $this->request->clientIp())){
					$success = true;
				}
			}else{
				$success = false;
				$msg = 'Debe ingresar un comentario';
			}
		}else{
			$success = false;
			$msg = 'Usted esta ingresando de forma indebida';
		}
		$arr_return['success'] = $success;
		$arr_return['msg'] = $msg;
		echo json_encode($arr_return);
		exit();
	}
	
	public function ajax_listar_comentarios($suministro=null, $pfactura=null){
		$this->layout = 'ajax';
		date_default_timezone_set('America/Lima');
		setlocale(LC_ALL,"es_ES");
		
		if(isset($suministro) && isset($pfactura)){
			$this->loadModel('ComlecOrdenlectura');
			$obj_comentarios = $this->ComlecOrdenlectura->listarReporteHistoricoComentario($suministro, $pfactura);
		}else {
			$obj_comentarios = array();
		}
		$this->set(compact('obj_comentarios'));
	}
	
	public function comentarios_por_suministro($suministro,$pfactura) {
		$this->layout = 'ajax';
		$this->loadModel('ComlecOrdenlectura');
		if(isset($suministro) && isset($pfactura)){
			$this->loadModel('ComlecOrdenlectura');
			$obj_comentarios = $this->ComlecOrdenlectura->listarReporteHistoricoComentario($suministro, $pfactura);
		}else {
			$obj_comentarios = array();
		}
    	//debug($obj_comentarios);
		$this->set(compact('obj_comentarios'));
	}
	
	public  function  mapa_literal($latitud,$longitud){
		$this->layout = 'home_tree';
		$this->set(compact('latitud','longitud'));
	}
	
	public function mineria(){
		ini_set('memory_limit', '-1');
		ini_set('max_execution_time', 300000);
		$this->layout = 'home_tree';
		$this->loadModel('Mineria');
	
		//$listado = $this->Mineria->listar();
		$listado = array();
		
		$arr_suministros = array();
		foreach ($listado as $k => $obj_orden_lectura){
			$arr_suministros[$obj_orden_lectura[0]['Suministro']] = $this->Mineria->buscarHistoricoPorSuministro($obj_orden_lectura[0]['Suministro']);
		}
		
		$count_total = $this->Mineria->getCountTotal();
		$count_total_metodo2 = $this->Mineria->getCountTotalMetodo2();
		$count_consistentes_80 = $this->Mineria->getCountConsistentes80();
		$count_inconsistentes_80 = $this->Mineria->getCountInconsistentes80();
		$count_consistentes_95 = $this->Mineria->getCountConsistentes95();
		$count_inconsistentes_95 = $this->Mineria->getCountInconsistentes95();
		
		$id_usuario = $this->obj_logged_user['Usuario']['id'];
				
		$this->set(compact('id_usuario','listado','arr_suministros','count_total','count_total_metodo2','count_consistentes_80','count_inconsistentes_80','count_consistentes_95','count_inconsistentes_95'));
	}
	
	public function ajax_mineria(){
		$this->autoRender = false;
		ini_set('memory_limit', '-1');
        ini_set('max_execution_time', 300000);
        set_time_limit(0);
         
        /*
         * Paging
        */
        if ( isset( $_GET['iDisplayStart'] ) && $_GET['iDisplayLength'] != '-1' )
        {
            $page = intval( $_GET['iDisplayStart'] );
            $limit = intval( $_GET['iDisplayLength'] );
        }
    
        $search = "";
        if ( isset($_GET['sSearch']) && $_GET['sSearch'] != "" ){
            $search = $_GET['sSearch'];
        }
	
        $this->loadModel('ComlecOrdenlectura');
        $this->loadModel('Mineria');
		$listado = $this->Mineria->listar2(isset($page)?$page:0,isset($limit)?$limit:20,$search);
		//debug($listado);exit;
		$arr_sumi = array();
		foreach ($listado as $obj1){
			$arr_sumi[] = $obj1[0]['Suministro'];
		}
		$arr_obj_orden = $this->ComlecOrdenlectura->getOrdenBySuministro($arr_sumi);
		//debug($arr_obj_orden);exit;
		
		$i=1;
        $j=1;
        $tabla='';
        $total_records=0;
        
        foreach ($listado as $obj1){
            $tabla.='[';
            $tabla.= '"'.$obj1[0]['Suministro'].'"';
            //$tabla.= ',"'.$obj1[0]['Periodo'].'"';
            $tabla.= ',"201408"';
            /*$obj_orden = $this->ComlecOrdenlectura->getOrdenBySuministro($obj1[0]['Suministro']);
            
            if(count($obj_orden)>0){
            	$tabla.= ',"'.$obj_orden['0']["montoconsumo1"].'"';
            }else{
            	$tabla.= ',""';
            }*/
            
            $montoconsumo= '';
            foreach ($arr_obj_orden as $obj_orden){
            	if($obj_orden['0']["suministro"]==$obj1[0]['Suministro']){
            		$montoconsumo= $obj_orden['0']["montoconsumo1"];
            	}
            }
            if($montoconsumo!=''){
            	$tabla.= ',"'.$montoconsumo.'"';
            }else{
            	$tabla.= ',""';
            }
            $tabla.= ',"'.$obj1[0]['lecturao'].'"';
            $tabla.= ',"'.$obj1[0]['metodofinal1'].'"';
            $tabla.= ',"'.$obj1[0]['validacion'].'"';
            $tabla.= ',"'.$obj1[0]['sanearmovil'].'"';
            $tabla.= ',"'.$obj1[0]['primera'].'"';
            $tabla.= ',"'.$obj1[0]['demandaact'].'"';
            $tabla.= ',"'.$obj1[0]['pronostico'].'"';
            $tabla.= ',"'.$obj1[0]['minima80'].'"';
            $tabla.= ',"'.$obj1[0]['maxima80'].'"';
            $tabla.= ',"'.$obj1[0]['consistencia80'].'"';
            $tabla.= ',"'.$obj1[0]['minima95'].'"';
            $tabla.= ',"'.$obj1[0]['maxima95'].'"';
            $tabla.= ',"'.$obj1[0]['consistencia95'].'"';
            
            $arr_suministros = array();
            $arr_suministros = $this->Mineria->buscarHistoricoPorSuministro2($obj1[0]['Suministro']);
            $str_plot = '';
            foreach ($arr_suministros as $suministro){
            	if($suministro['0']["demandaact"]!=''){
            	$str_plot .= $suministro['0']["demandaact"].',';
            	}
            }
            $tabla.= ',"<div class=\"spk-suministro text-center\" data-plot=\"'.substr($str_plot,0,-1).'\"></div>"';
            $tabla.='],';
            $total_records =$obj1[0]['row_total'];
        };
        
        $tabla = substr($tabla,0,-1);

        $vari='{
            "sEcho": "'.intval($_GET['sEcho']).'",
            "iTotalRecords":"'.$total_records.'",
            "iTotalDisplayRecords":"'.$total_records.'",
            "aaData":[';
        $vari.=$tabla;
        $vari.=']}';
        return $vari;  
	}
	
	public function mineria2($pfactura='201407'){
		ini_set('memory_limit', '-1');
		ini_set('max_execution_time', 300000);
		$this->layout = 'home_tree';
		$this->loadModel('Mineria');
	
		//$listado = $this->Mineria->listar_mineria2_sql($pfactura,isset($page)?$page:0,isset($limit)?$limit:20,'');
		$listado = array();
		/*
		$count_total = $this->Mineria->getCountTotal();
		$count_total_metodo2 = $this->Mineria->getCountTotalMetodo2();
		$count_consistentes_80 = $this->Mineria->getCountConsistentes80();
		$count_inconsistentes_80 = $this->Mineria->getCountInconsistentes80();
		$count_consistentes_95 = $this->Mineria->getCountConsistentes95();
		$count_inconsistentes_95 = $this->Mineria->getCountInconsistentes95();*/
	
		$id_usuario = $this->obj_logged_user['Usuario']['id'];
	
		$this->set(compact('id_usuario','listado','pfactura','count_total','count_total_metodo2','count_consistentes_80','count_inconsistentes_80','count_consistentes_95','count_inconsistentes_95'));
	}
	
	public function ajax_mineria2($pfactura='201407'){
		$this->autoRender = false;
		ini_set('memory_limit', '-1');
		ini_set('max_execution_time', 300000);
		set_time_limit(0);
		 
		/*
		 * Paging
		*/
		if ( isset( $_GET['iDisplayStart'] ) && $_GET['iDisplayLength'] != '-1' )
		{
			$page = intval( $_GET['iDisplayStart'] );
			$limit = intval( $_GET['iDisplayLength'] );
		}
	
		$search = "";
		if ( isset($_GET['sSearch']) && $_GET['sSearch'] != "" ){
			$search = $_GET['sSearch'];
		}
	
		$this->loadModel('ComlecOrdenlectura');
		$this->loadModel('Mineria');
		$listado = $this->Mineria->listar_mineria2_sql($pfactura,isset($page)?$page:0,isset($limit)?$limit:20,$search);

		$i=1;
		$j=1;
		$tabla='';
		$total_records=0;
	
		foreach ($listado as $obj1){
			$tabla.='[';
			$tabla.= '"'.$obj1[0]['suministro'].'"';
			$tabla.= ',"'.$obj1[0]['pfactura'].'"';
			$tabla.= ',"'.number_format($obj1[0]['lecturao'],2).'"';
			$tabla.= ',"'.number_format($obj1[0]['demandaact'],2).'"';
			$tabla.= ',"'.$obj1[0]['metodo1_pex'].'"';
			$tabla.= ',"'.$obj1[0]['metodo2_pex'].'"';
			$tabla.= ',"'.$obj1[0]['metodoENSA'].'"';
			$tabla.= ',"'.$obj1[0]['metodo_mineria_80'].'"';
			$tabla.= ',"'.$obj1[0]['metodo_mineria_95'].'"';
			$tabla.= ',"'.$obj1[0]['fuerzabruta'].'"';
			$tabla.= ',"'.$obj1[0]['reclamo'].'"';
			$tabla.= ',"<a href='.ENV_WEBROOT_FULL_URL.'reportes/reporte_historico/'.$obj1[0]['suministro'].' target=_blank>ver historco</a>"';
			$tabla.='],';
			$total_records =$obj1[0]['row_total'];
		};
	
		$tabla = substr($tabla,0,-1);
	
		$vari='{
            "sEcho": "'.intval($_GET['sEcho']).'",
            "iTotalRecords":"'.$total_records.'",
            "iTotalDisplayRecords":"'.$total_records.'",
            "aaData":[';
		$vari.=$tabla;
		$vari.=']}';
		return $vari;
	}
	
	public function reporte_ordenes_por_observacion(){
		$this->layout = 'home_tree';
		
		if(isset($this->request->data['unidadneg'])){
			$unidadneg = $this->request->data['unidadneg']; 
		}else{
			$unidadneg = 0;
		}
		
		if(isset($this->request->data['pfactura'])){
			$pfactura = $this->request->data['pfactura'];
		}else{
			$pfactura = 0;
		}
		
		$this->loadModel('ComlecOrdenlectura');
		$listar_unidadneg = $this->ComlecOrdenlectura->listarUnidadneg();
		$listar_pfacrura = $this->ComlecOrdenlectura->listarPFactura();
		$arr_codigos_observacion = $this->ComlecOrdenlectura->getCodigosObservaciones($unidadneg,$pfactura);
		
		if(isset($this->request->data['obs'])){
			$obs = $this->request->data['obs'];
		}else{
			if(count($arr_codigos_observacion)>0){
				$obs = $arr_codigos_observacion[0][0]['obs1'];
			}else{
				$obs = 0;
			}
		}
		
		$arr_ordenes_por_observacion = $this->ComlecOrdenlectura->getOrdenesPorObservacion($obs,$unidadneg,$pfactura);
		//debug($arr_ordenes_por_observacion);exit();
		$this->set(compact('listar_unidadneg','unidadneg','obs','listar_pfacrura','pfactura','arr_codigos_observacion','arr_ordenes_por_observacion'));
	}
	
	public function resumen_suministros_cronograma($unidad_neg=null,$idciclo=null) {
		$this->layout = 'home_tree';
	
		$this->loadModel('ComlecOrdenlectura');
	
		if(isset($unidad_neg) || isset($idciclo)){
			$options1 = array('unidad_neg'=>$unidad_neg,'idciclo'=>$idciclo);
		}else{
			$options1 = null;
		}
	
		$arr_data = $this->ComlecOrdenlectura->getConsumoPorDiaYUbicacionYXml($options1);
		$arr_ciclos_actuales = $this->ComlecOrdenlectura->getCiclosActuales($options1);
	
		$this->set(compact('arr_data','arr_ciclos_actuales'));
	}
	

	public function carta_pdf($suministro = null){		
		$this->loadModel('ComlecOrdenlectura');
		
		$obj_lectura = $this->ComlecOrdenlectura->getLecturaRepartoBySuministro(null,$suministro);
		//$arr_suministros = $this->ComlecOrdenlectura->buscarSuministroHistorico($suministro,'all',6);
		
		$this->set(compact('arr_suministros','obj_lectura'));
	}

	public function ordenes_historico($criterio=null,$date_start=null,$date_end=null,$unidadneg=null,$ciclo=null,$sector=null,$ruta=null,$suministro=null,$lecturista=null,$consumo_min=null,$consumo_max=null,$filter_advance=null,$id_xml=null,$pfactura=null){
		ini_set('memory_limit', '-1');
		$this->layout = 'home_tree';
	
		if(!isset($date_start)){
			$date_start = date('Y-m-d');
		}
		if(!isset($date_end)){
			$date_end = date('Y-m-d');
		}
		if(!isset($criterio)){
			$criterio = 'U';
		}
		 
		$id_usuario = $this->obj_logged_user['Usuario']['id'];
		 
		$this->loadModel('GlomasEmpleado');
		$this->loadModel('ComlecOrdenlectura');
		$this->loadModel('Usuario');
		 
		$listar_empleados = $this->GlomasEmpleado->ListarEmpleados();
		$listar_unidadneg = $this->ComlecOrdenlectura->listarUnidadneg();
		$listar_ciclo = $this->ComlecOrdenlectura->listarCiclo();
		$arr_ciclos_actuales = $this->ComlecOrdenlectura->getCiclosActuales();
		$listar_sector = $this->ComlecOrdenlectura->listarSector();
		$listar_ruta = $this->ComlecOrdenlectura->listarRuta();
		$listar_pfacrura = $this->ComlecOrdenlectura->listarPFactura();
		 
		$this->set(compact('id_usuario','listar_empleados','listar_ciclo','arr_ciclos_actuales','listar_unidadneg','listar_sector','listar_ruta','criterio','date_start','date_end','unidadneg','ciclo','sector','ruta','suministro','lecturista','consumo_min','consumo_max','filter_advance','id_xml','listar_pfacrura','pfactura'));
	}
	
	public function ajax_ordenes_historico($criterio=null,$date_start=null,$date_end=null,$unidadneg=null,$ciclo=null,$sector=null,$ruta=null,$suministro=null,$lecturista=null,$consumo_min=null,$consumo_max=null,$filter_advance=null,$id_xml=null,$pfactura=null) {
		$this->autoRender = false;
		ini_set('memory_limit', '-1');
		ini_set('max_execution_time', 300000);
		set_time_limit(0);
	
		/*
		 * Paging
		*/
		if ( isset( $_GET['iDisplayStart'] ) && $_GET['iDisplayLength'] != '-1' )
		{
			$page = intval( $_GET['iDisplayStart'] );
			$limit = intval( $_GET['iDisplayLength'] );
		}
	
		$search = "";
		if ( isset($_GET['sSearch']) && $_GET['sSearch'] != "" ){
			$search = $_GET['sSearch'];
		}
		$negocio = $this->Session->read('NEGOCIO');
	
		$this->loadModel('ComlecOrdenlectura');
		if($suministro != '0'){
			$this->ComlecOrdenlectura->setDataSource (strtolower($negocio));
			$obj_suministro = $this->ComlecOrdenlectura->findBySuministro($suministro);
			if(isset($obj_suministro['ComlecOrdenlectura']['orden'])){
				$suministro_orden = $obj_suministro['ComlecOrdenlectura']['orden']-10;
				$sector = $obj_suministro['ComlecOrdenlectura']['sector'];
				$ruta = $obj_suministro['ComlecOrdenlectura']['ruta'];
			}else{
				$suministro_orden = '0';
			}
		}else{
			$suministro_orden = '';
		}
	
		if($consumo_min == '0'){
			$consumo_min = null;
		}
	
		if($consumo_max == '0'){
			$consumo_max = null;
		}
	
		$filtros = array();
		if(isset($date_start)){
			$filtros['date_start'] = $date_start;
		}
		if(isset($date_end)){
			$filtros['date_end'] = $date_end;
		}
		if(isset($unidadneg)){
			$filtros['unidadneg'] = $unidadneg;
		}
		if(isset($ciclo)){
			$filtros['ciclo'] = $ciclo;
		}
		if(isset($sector)){
			$filtros['sector'] = $sector;
		}
		if(isset($ruta)){
			$filtros['ruta'] = $ruta;
		}
		if(isset($suministro)){
			$filtros['suministro'] = $suministro;
			$filtros['suministro_orden'] = $suministro_orden;
		}
		if(isset($lecturista)){
			$filtros['lecturista'] = $lecturista;
		}
		if(isset($id_xml) && $id_xml!='0' && $id_xml!=''){
			$filtros['id_xml'] = $id_xml;
		}
		if(isset($pfactura) && $pfactura!='0' && $pfactura!=''){
			$filtros['pfactura'] = $pfactura;
		}
	
		$arr_obj_incosistencia = $this->ComlecOrdenlectura->listar_ordenes_historico($criterio,$filtros,$filter_advance,$page,$limit,$search,$consumo_min,$consumo_max);
		 
		$j = 1;
		$i = 1;
		$tabla='';
		$total_records=0;
		foreach ($arr_obj_incosistencia as $obj1){
			$tabla.='[';
			$tabla.= '"'.$obj1[0]['comlec_ordenlectura_id'].'"';
			$tabla.= ',"'.$obj1[0]['pfactura'].'"';
			$tabla.= ',"'.$obj1[0]['suministro'].'"';
			$tabla.= ',"'.$obj1[0]['seriefab'].'"';
			$tabla.= ',"'.$obj1[0]['cliente'].'"';
			$tabla.= ',"'.$obj1[0]['direccion'].'"';
			if(trim($obj1[0]['tipolectura'])=='L'){
				$nombre_lecturista = $obj1[0]['lecturista1'];
			}else{
				$nombre_lecturista = $obj1[0]['lecturista2'];
			}
			$tabla.= ',"'.$nombre_lecturista.'"';
			$tabla.= ',"'.$obj1[0]['idciclo'].' - '.$obj1['0']['nombciclo'].'"';
			$tabla.= ',"'.$obj1[0]['sector'].'"';
			$tabla.= ',"'.$obj1[0]['ruta'].' - '.$obj1['0']['nombruta'].'"';
			$tabla.= ',"'.$obj1[0]['lecturao1'].'"';
			$tabla.= ',"'.$obj1[0]['obs1'].'"';
			$tabla.= ',"'.$obj1[0]['montoconsumo1'].'"';
			$tabla.= ',"'.$obj1[0]['fechaejecucion1'].'"';
			$tabla.= ',"'.$obj1[0]['fechaasignado1'].'"';
			$tabla.= ',"'.$obj1[0]['orden'].'"';
			$tabla.= ',"'.$obj1[0]['ordenruta'].'"';
			$tabla.= ',"'.$obj1[0]['resultadoevaluacion'].'"';
			$tabla.= ',"'.$obj1[0]['lcorrelati'].'"';
			$tabla.= ',"'.$obj1[0]['ordtrabajo'].'"';
			$tabla.='],';
			$i++;
			$total_records =$obj1[0]['row_total'];
		};
		 
		$tabla = substr($tabla,0,-1);
	
		$vari='{
    		"sEcho": "'.intval($_GET['sEcho']).'",
    		"iTotalRecords":"'.$total_records.'",
    		"iTotalDisplayRecords":"'.$total_records.'",
			"aaData":[';
		$vari.=$tabla;
		$vari.=']}';
		return $vari;
	}
	
	public function ordenes_historico_xls($criterio=null,$date_start=null,$date_end=null,$unidadneg=null,$ciclo=null,$sector=null,$ruta=null,$suministro=null,$lecturista=null,$consumo_min=null,$consumo_max=null,$filter_advance=null,$id_xml=null,$pfactura=null) {
		$this->autoRender = false;
		ini_set('memory_limit', '-1');
		ini_set('max_execution_time', 300000);
		set_time_limit(0);
	
		$negocio = $this->Session->read('NEGOCIO');
	
		$this->loadModel('ComlecOrdenlectura');
		if($suministro != '0'){
			$this->ComlecOrdenlectura->setDataSource (strtolower($negocio));
			$obj_suministro = $this->ComlecOrdenlectura->findBySuministro($suministro);
			if(isset($obj_suministro['ComlecOrdenlectura']['orden'])){
				$suministro_orden = $obj_suministro['ComlecOrdenlectura']['orden']-10;
				$sector = $obj_suministro['ComlecOrdenlectura']['sector'];
				$ruta = $obj_suministro['ComlecOrdenlectura']['ruta'];
			}else{
				$suministro_orden = '0';
			}
		}else{
			$suministro_orden = '';
		}
	
		if($consumo_min == '0'){
			$consumo_min = null;
		}
	
		if($consumo_max == '0'){
			$consumo_max = null;
		}
	
		$filtros = array();
		if(isset($date_start)){
			$filtros['date_start'] = $date_start;
		}
		if(isset($date_end)){
			$filtros['date_end'] = $date_end;
		}
		if(isset($unidadneg)){
			$filtros['unidadneg'] = $unidadneg;
		}
		if(isset($ciclo)){
			$filtros['ciclo'] = $ciclo;
		}
		if(isset($sector)){
			$filtros['sector'] = $sector;
		}
		if(isset($ruta)){
			$filtros['ruta'] = $ruta;
		}
		if(isset($suministro)){
			$filtros['suministro'] = $suministro;
			$filtros['suministro_orden'] = $suministro_orden;
		}
		if(isset($lecturista)){
			$filtros['lecturista'] = $lecturista;
		}
		if(isset($id_xml) && $id_xml!='0' && $id_xml!=''){
			$filtros['id_xml'] = $id_xml;
		}
		if(isset($pfactura) && $pfactura!='0' && $pfactura!=''){
			$filtros['pfactura'] = $pfactura;
		}
	
		$arr_obj_incosistencia = $this->ComlecOrdenlectura->listar_ordenes_historico($criterio,$filtros,$filter_advance,1,null,null,$consumo_min,$consumo_max);
			
		$j = 1;
		$i = 1;
		$tabla='<table border=1><tr>
				<th>ID</th>
				<th>pfactura</th>
				<th>Suministro</th>
				<th>Medidor</th>
				<th>Cliente</th>
				<th>Direcci&oacute;n</th>
				<th>Lecturista</th>
				<th>Ciclo</th>
				<th>Sector</th>
				<th>Ruta</th>
				<th>Lectura</th>
				<th>Observaci&oacute;n</th>
				<th>Monto Consumo</th>
				<th>Fecha Ejecuci&oacute;n</th>
				<th>Fecha Asignaci&oacute;n</th>
				<th>Orden</th>
				<th>Orden Ruta</th>
				<th>Resultado</th>
				<th>lcorrelati</th>
				<th>OT</th></tr>';
		$total_records=0;
		foreach ($arr_obj_incosistencia as $obj1){
			$tabla.='<tr>';
			$tabla.= '<td>'.$obj1[0]['comlec_ordenlectura_id'].'</td>';
			$tabla.= '<td>'.$obj1[0]['pfactura'].'</td>';
			$tabla.= '<td>'.$obj1[0]['suministro'].'</td>';
			$tabla.= '<td>'.$obj1[0]['seriefab'].'</td>';
			$tabla.= '<td>'.utf8_decode($obj1[0]['cliente']).'</td>';
			$tabla.= '<td>'.utf8_decode($obj1[0]['direccion']).'</td>';
			if(trim($obj1[0]['tipolectura'])=='L'){
				$nombre_lecturista = $obj1[0]['lecturista1'];
			}else{
				$nombre_lecturista = $obj1[0]['lecturista2'];
			}
			$tabla.= '<td>'.utf8_decode($nombre_lecturista).'</td>';
			$tabla.= '<td>'.$obj1[0]['idciclo'].' - '.$obj1['0']['nombciclo'].'</td>';
			$tabla.= '<td>'.$obj1[0]['sector'].'</td>';
			$tabla.= '<td>'.$obj1[0]['ruta'].' - '.$obj1['0']['nombruta'].'</td>';
			$tabla.= '<td>'.$obj1[0]['lecturao1'].'</td>';
			$tabla.= '<td>'.$obj1[0]['obs1'].'</td>';
			$tabla.= '<td>'.$obj1[0]['montoconsumo1'].'</td>';
			$tabla.= '<td>'.$obj1[0]['fechaejecucion1'].'</td>';
			$tabla.= '<td>'.$obj1[0]['fechaasignado1'].'</td>';
			$tabla.= '<td>'.$obj1[0]['orden'].'</td>';
			$tabla.= '<td>'.$obj1[0]['ordenruta'].'</td>';
			$tabla.= '<td>'.$obj1[0]['resultadoevaluacion'].'</td>';
			$tabla.= '<td>'.$obj1[0]['lcorrelati'].'</td>';
			$tabla.= '<td>'.$obj1[0]['ordtrabajo'].'</td>';
			$tabla.='</tr>';
			$i++;
			$total_records =$obj1[0]['row_total'];
		};
			
		$tabla = substr($tabla,0,-1).'</table>';
	

		header('Content-type: application/vnd.ms-excel');
		header("Content-Disposition: attachment; filename=archivo.xls");
		header("Pragma: no-cache");
		header("Expires: 0");
		echo $tabla;
	}
	
	public function reclamos($idciclo='2049',$fator_correccion=5){
		ini_set('memory_limit', '-1');
		ini_set('max_execution_time', 300000);
		$this->layout = 'home_tree';
		$this->loadModel('ComlecOrdenlectura');
	
		//$listado = $this->ComlecOrdenlectura->listar_reclamos($idciclo,$fator_correccion,isset($page)?$page:0,isset($limit)?$limit:20,'');
		$listado = array();
		$listar_ciclo = $this->ComlecOrdenlectura->listarCiclo();
		
		$count_total = $this->ComlecOrdenlectura->getCount(array('idciclo'=>$idciclo));
		$count_target1 = $this->ComlecOrdenlectura->getCountReclamosTarget1($idciclo);
		$count_target2 = $this->ComlecOrdenlectura->getCountReclamosTarget2($idciclo,$fator_correccion);
	
		$id_usuario = $this->obj_logged_user['Usuario']['id'];
	
		$this->set(compact('id_usuario','listado','listar_ciclo','idciclo','fator_correccion','count_total','count_target1','count_target2'));
	}
	
	public function ajax_reclamos($idciclo='2049',$fator_correccion=null){
		$this->autoRender = false;
		ini_set('memory_limit', '-1');
		ini_set('max_execution_time', 300000);
		set_time_limit(0);
			
		/*
		 * Paging
		*/
		if ( isset( $_GET['iDisplayStart'] ) && $_GET['iDisplayLength'] != '-1' )
		{
			$page = intval( $_GET['iDisplayStart'] );
			$limit = intval( $_GET['iDisplayLength'] );
		}
	
		$search = "";
		if ( isset($_GET['sSearch']) && $_GET['sSearch'] != "" ){
			$search = $_GET['sSearch'];
		}
	
		$this->loadModel('ComlecOrdenlectura');
		$listado = $this->ComlecOrdenlectura->listar_reclamos($idciclo,$fator_correccion,isset($page)?$page:0,isset($limit)?$limit:20,$search);
		
		$arr_suministros = $this->Session->read('no_carta_reclamos');
		if(!isset($arr_suministros)) { $arr_suministros = array();}
	
		$i=1;
		$j=1;
		$tabla='';
		$total_records=0;
		foreach ($listado as $obj1){
			$str_checked = '';
			if(in_array($obj1[0]['suministro'],$arr_suministros)){
				$str_checked = 'checked';
			}
			$tabla.='[';
			$tabla.= '"<input type=\"checkbox\" '.$str_checked.' class=\"chk_suministro\" data-suministro=\"'.$obj1[0]['suministro'].'\">"';
			$tabla.= ',"'.$obj1[0]['pfactura'].'"';
			$tabla.= ',"'.$obj1[0]['suministro'].'"';	
			$tabla.= ',"'.$obj1[0]['promedio'].'"';
			$tabla.= ',"'.number_format($obj1[0]['mes_anterior'],2).'"';
			$tabla.= ',"'.number_format($obj1[0]['mes_actual'],2).'"';
			$tabla.= ',"'.number_format($obj1[0]['lecant'],2).'"';
			$tabla.= ',"'.(int) $obj1[0]['lecturao1'].'"';
			$tabla.= ',"'.$obj1[0]['metodofinal1'].'"';
			$tabla.= ',"'.number_format($obj1[0]['maxima'],3).'"';
			$tabla.= ',"'.$obj1[0]['target'].'"';
			$tabla.= ',"'.number_format($obj1[0]['factor_correccion'],3).'"';
			$tabla.= ',"'.$obj1[0]['target2'].'"';
			$tabla.= ',"<a class=\"preview\" href=\"data:image/png;base64,'.trim(str_replace("\n", "", str_replace("\r", "", $obj1[0]['foto1']))).'\"><img src=\"data:image/png;base64,'.trim(str_replace("\n", "", str_replace("\r", "", $obj1[0]['foto1']))).'\" class=\"img-thumbnail\" style=\"height:64px;\"></a>"';
			$tabla.='],';
			$total_records =$obj1[0]['row_total'];
		};
	
		$tabla = substr($tabla,0,-1);
	
		$vari='{
            "sEcho": "'.intval($_GET['sEcho']).'",
            "iTotalRecords":"'.$total_records.'",
            "iTotalDisplayRecords":"'.$total_records.'",
            "aaData":[';
		$vari.=$tabla;
		$vari.=']}';
		return $vari;
	}
	
	public function generar_carta_pdf_multiple($idciclo='2049',$fator_correccion=5,$page=null,$limit=null){
		ini_set('memory_limit', '-1');
		
		$this->loadModel('ComlecOrdenlectura');
		
		$page = isset($page)?($page-1):0;
		$limit = isset($limit)?($limit-$page):20;
		
		$arr_suministros = $this->Session->read('no_carta_reclamos');
		if(!isset($arr_suministros)){
			$arr_suministros = array();
		}
		
		$listado = $this->ComlecOrdenlectura->listar_reclamos($idciclo,$fator_correccion,isset($page)?$page:0,isset($limit)?$limit:20,'');
		
		$ObjComlecOrdenlectura = $this->ComlecOrdenlectura;
		
		$this->set(compact('listado','ObjComlecOrdenlectura','page','arr_suministros'));
	}
	
	public function no_carta_reclamos($suministro){
		$this->layout = 'ajax';
	
		$arr_suministros = $this->Session->read('no_carta_reclamos');
		
		if(@in_array($suministro,$arr_suministros)){
			unset($arr_suministros[$suministro]);
		}else{
			$arr_suministros[$suministro] = $suministro; 
		}
		$this->Session->write('no_carta_reclamos',$arr_suministros);
		
		echo json_encode(array('success'=>true,'no_carta_reclamos'=>$arr_suministros));
		exit;
	}
	
	public function guardar_carta_reclamos($idciclo='2049',$fator_correccion=5,$page=null,$limit=null){
		ini_set('memory_limit', '-1');
		
		$id_usuario = $this->obj_logged_user['Usuario']['id'];
		
		$this->loadModel('ComlecOrdenlectura');
		
		$page = isset($page)?($page-1):0;
		$limit = isset($limit)?($limit-$page):20;
		
		$arr_suministros = $this->Session->read('no_carta_reclamos');
		if(!isset($arr_suministros)){
			$arr_suministros = array();
		}
		
		$listado = $this->ComlecOrdenlectura->listar_reclamos($idciclo,$fator_correccion,isset($page)?$page:0,isset($limit)?$limit:20,'');
		
		$this->ComlecOrdenlectura->guardarCartasReclamos($listado,$arr_suministros,$fator_correccion,$id_usuario);	
		
		echo json_encode(array('success'=>true,'msg'=>'Guardado Correctamente.'));
		exit;
	}

	public function listar_personal($tipo='L',$unidadneg=null,$id_ciclo=null,$id_sector=null){
		//pdf
		ini_set('memory_limit', '-1');
		
		$this->loadModel('ComlecOrdenlectura');		
		$listar_orden_lecturas = $this->ComlecOrdenlectura->listarPersonalParaLecturas($tipo,$unidadneg,$id_ciclo,$id_sector);
		
		$negocio = $this->Session->read('NEGOCIO');
		$logo_negocios = Configure::read('lecturas.logo_negocios');
		$logo_negocio = $logo_negocios[strtoupper($negocio)];
		
		$all_negocios = Configure::read('lecturas.negocios_all');
		$logo_contratista = $all_negocios[strtoupper($negocio)]['logo_contratista'];
		
		$this->set(compact('listar_orden_lecturas','logo_negocio','logo_contratista'));
	}
	
	public function listar_personal_by_negocio($negocio,$tipo='L',$unidadneg=null,$id_ciclo=null,$id_sector=null){
		//pdf
		ini_set('memory_limit', '-1');
	
		$this->loadModel('ComlecOrdenlectura');
		$listar_orden_lecturas = $this->ComlecOrdenlectura->listarPersonalParaLecturasByNegocio($negocio,$tipo,$unidadneg,$id_ciclo,$id_sector);
		
		$logo_negocios = Configure::read('lecturas.logo_negocios');
		$logo_negocio = $logo_negocios[strtoupper($negocio)];
		
		$all_negocios = Configure::read('lecturas.negocios_all');
		$logo_contratista = $all_negocios[strtoupper($negocio)]['logo_contratista'];
		
		$this->set(compact('listar_orden_lecturas','logo_negocio','logo_contratista'));
		
		$this->render('listar_personal');
	}
	
	/**
	 * Listar suministros con fotos. (Segun solicitud de supervisor)
	 * @author Geynen
	 * @version 25 Febrero 2015
	 */
	public function suministros_fotos(){
		$this->layout = 'ajax';
		$this->helpers[] = 'Image';
		
		$this->loadModel('ComlecOrdenlectura');
		$listar_orden_lecturas = $this->ComlecOrdenlectura->listarSuministrosFotos();
		//$listar_orden_lecturas = array();

		$this->set(compact('listar_orden_lecturas'));
	}
	
}