<?php

App::uses('AppController', 'Controller');
App::uses('Sanitize', 'Utility');
App::uses('CakeTime', 'Utility');
App::import('Vendor', 'php-excel-reader/excel_reader2'); //import library read excell
class EvaluarInconsistenciasController extends AppController {

	public $helpers = array('Html', 'Form', 'Image');
	public $name = 'EvaluarInconsistencias';
	
	public function analizar_inconsistencia_lectura(){
		$this->autoRender = false;
		$analizar = $this->request->data['analizar'];
		$this->loadModel('ComlecOrdenlectura');
		try {
			$arr_obj_incosistencia = $this->ComlecOrdenlectura->analizarInconsistenciaLectura($analizar);
			$i=1;
			foreach ($arr_obj_incosistencia as $obj_inconsistencia){
				$consumo = (int) $obj_inconsistencia[0]['lecturao1'] - (int) $obj_inconsistencia[0]['lecant'];
				if($consumo =='-1' OR $consumo =='-2' OR $consumo =='-3'){
					$i++;
					$newlecturao = $obj_inconsistencia[0]['lecant'];
					$obj_incosistencia_new = $this->ComlecOrdenlectura->updateInconsistenciaLectura($newlecturao,$obj_inconsistencia[0]['lecturao1'], $obj_inconsistencia[0]['suministro']);
				}
			}
			return json_encode(array('count' => $i,'mensaje'=>'OK'));
		} catch (Exception $e) {
			return $e;
		}
	}

    public function evaluar_inconsistencia($criterio=null,$date_start=null,$date_end=null,$unidadneg=null,$ciclo=null,$sector=null,$suministro=null,$lecturista=null,$consumo_min=null,$consumo_max=null,$filter_advance=null,$id_xml=null){
    	ini_set('memory_limit', '-1');
    	$this->layout = 'home_tree';
		
    	if(!isset($date_start)){
    		$date_start = date('Y-m-d');
    	}
    	if(!isset($date_end)){
    		$date_end = date('Y-m-d');
    	}
    	if(!isset($criterio)){
    		$criterio = 'U';
    	}
    	
    	$id_usuario = $this->obj_logged_user['Usuario']['id'];
    	
    	$this->loadModel('GlomasEmpleado');
    	$this->loadModel('ComlecOrdenlectura');
    	$this->loadModel('Usuario');
    	
    	$listar_empleados = $this->GlomasEmpleado->ListarEmpleados();    	
    	$listar_unidadneg = $this->ComlecOrdenlectura->listarUnidadneg();
    	$listar_ciclo = $this->ComlecOrdenlectura->listarCiclo();
    	$arr_ciclos_actuales = $this->ComlecOrdenlectura->getCiclosActuales();
    	$listar_sector = $this->ComlecOrdenlectura->listarSector();
    	
    	$this->set(compact('id_usuario','listar_empleados','listar_ciclo','arr_ciclos_actuales','listar_unidadneg','listar_sector','criterio','date_start','date_end','unidadneg','ciclo','sector','suministro','lecturista','consumo_min','consumo_max','filter_advance','id_xml'));
    }
    
    public function extranet(){
    	ini_set('memory_limit', '-1');
    	$this->layout = 'home_tree';
    
    	$id_usuario = $this->obj_logged_user['Usuario']['id'];
    	 
    	$this->loadModel('GlomasEmpleado');
    	$this->loadModel('ComlecOrdenlectura');
    	$this->loadModel('Usuario');
    	 
    	$listar_empleados = $this->GlomasEmpleado->ListarEmpleados();
    	$listar_sector = $this->ComlecOrdenlectura->listarSector();
    	 
    	$this->set(compact('id_usuario','listar_empleados','listar_sector'));
    }
    
    public function buscar_inconsistencias_extranet($sector,$search_suministro, $lecturista) {
    	$this->autoRender = false;
    	ini_set('memory_limit', '-1');
    	ini_set('max_execution_time', 300000);
    	set_time_limit(0);
    	$negocio = $this->Session->read('NEGOCIO');	
    	/*
    	 * Paging
    	*/
    	if ( isset( $_GET['iDisplayStart'] ) && $_GET['iDisplayLength'] != '-1' )
    	{
    		$page = intval( $_GET['iDisplayStart'] );
    		$limit = intval( $_GET['iDisplayLength'] );
    	}
    
    	$search = "";
    	if ( isset($_GET['sSearch']) && $_GET['sSearch'] != "" ){
    		$search = $_GET['sSearch'];
    	}
    
    	$this->loadModel('ComlecOrdenlectura');
    	if($search_suministro != '0'){
    		$this->ComlecOrdenlectura->setDataSource (strtolower($negocio));
        	$obj_suministro = $this->ComlecOrdenlectura->findBySuministro($search_suministro);
        	if(isset($obj_suministro['ComlecOrdenlectura']['orden'])){
        		$suministro_id = $obj_suministro['ComlecOrdenlectura']['orden'];
        	}else{
        		$suministro_id = '0';
        	}
    	}else{
    		$suministro_id = '';
    	}
    
    
    	
    	if($negocio=='TAC'){
    		$arr_obj_incosistencia = $this->ComlecOrdenlectura->listarInconsistenciasExtranetTac($sector,$suministro_id,$lecturista,$page,$limit,$search);
    	}else{
    		$arr_obj_incosistencia = $this->ComlecOrdenlectura->listarInconsistenciasExtranetCix($sector,$suministro_id,$lecturista,$page,$limit,$search);
    	}
    	
    	$j = 0;
    	$i = 0;
    	$tabla='';
    	$total_records=0;
    	foreach ($arr_obj_incosistencia as $obj1){
    		$tabla.='[';
    		$negocio = $this->Session->read('NEGOCIO');
    		if($negocio=='TAC'){
    			$tabla.= '"'.$obj1[0]['lcorrelati'].'"';
    		}else{
    			$tabla.= '"'.$obj1[0]['orden'].'"';
    		}
    		$tabla.= ',"'.$obj1[0]['seriefab'].'"';
    		$tabla.= ',"'.trim($obj1[0]['cliente']).'"';
    		$tabla.= ',"'.trim($obj1[0]['suministro']).'"';
    		$tabla.= ',"'.trim($obj1[0]['lap']).'"';
    		$tabla.= ',"'.trim($obj1[0]['lecant']).'"';
    		$tabla.= ',"'.trim($obj1[0]['lecturao1']).'"';
    		$tabla.= ',"'.$obj1[0]['lecturao2'].'"';
    		$tabla.= ',"'.$obj1[0]['lecturao3'].'"';
    		$tabla.= ',"'.$obj1[0]['obs_ant_px'].'"';
    		$tabla.= ',"'.trim($obj1[0]['obs1']).'"';
    		$tabla.= ',"'.$obj1[0]['obs2'].'"';
    		$tabla.= ',"'.$obj1[0]['consant'].'"';
    		$tabla.= ',"'.$obj1[0]['promedio'].'"';
    		$icon = '';
    		if ($obj1[0]['resultadoevaluacion']=='INCONSISTENTE'){
    			$icon = '<img title= \"INCONSISTENTE\" src=\"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAABrElEQVR42q2TyytEURzH/QFW1JRHIxsTUiNWHgtCFOWRPEpZyAJZkKSZhccgsVBIFqRkZdKUZCFFNLEQJUnJQk0eGcQCk+bj/O6dyZ3rsXLr272dcz7f3/d3zrlRUf/98PIIK1Mw0gzOKnCU6+/FUYJPD/wNH23DcB14F+DcA7sz4B5ScK8+XmuDvXV+h6da4GkfzpZhywWePljqhLlWmGiEwWoCFdbvJlpsNcm1MjmZVwnGfzaQdpT8RdbIdrSe16fhcFEHRWsOtRfdsNCuJ5MWxKCrgMeadPzTowYDp5o8dutQWAKb4tNTqhm8NmdxWZVnMFCDbM3ogFECTzZFVKctl7eGDE6zEwwGalDbbQHCkthSOQyHqstaaeHYHmcw6CjSj0oAoyS2CaY1i6vCJI7Kcr4MgrMD+mK5MEYJaIKlusS/GHMZDNSRvFWmfFUzgyE4UJ/GZW48ezYLAf+96S5srvJckhgJhUCRFBDYmxyNf83z820Mbrjx5cVpMeWoZLfl26cujsTeSbH8Doefj7sbfCMOToozOUyN5cAWw0G+nbN+J+93t/z73/sJB/zttLkU5gUAAAAASUVORK5CYII=\">';
    		}
    		 
    		if ($obj1[0]['resultadoevaluacion']=='CONSISTENTE'){
    			$icon = '<img title= \"CONSISTENTE\" src=\"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAB8ElEQVR42mNgoDb4/OfD/9UPpv6vv5z4v+C83/+sM17/c0+F/595o/f/u+/v/uPVfOHrgf8114L+L33f+H/j997/s98V/O95Hvu/5UHI/5wzLv+dtuv+3/Vy23+cmuvvRvw/9H/h/6U/qv53fgj93/DM93/VA/f/Jbcd/xdctfmffcH8v+kaqf/bnqMZAnJ22RW///v+zP0/52f+/76vUXADah97gQ3IvWz1P/OcKRgrLZNG9c6K+1P+z3lZDrcZhFtfB4E1V9xz/V980wFsQNoZ4/9xJ/T/W62T+d9+oh1hQOm5iP8bPvX9b30ZCMfozgfZnAjUHHlM+7/HdpX/NkusEQYknHACuwCkAWQrzGYQBmkGYZjtQYc0/1tvlv0vN1kSYUDkYdv/Ex6lgG1DxjDNyLb77FcDB6T0BAmEATGHvP933ouCa4BhUKiDMLJmj70q/7Xmi/63mm2JMKD7Yis4nmGhjIzjoJoDD2qANYNsl53C+79lbwvCAFCUGG5QBfsTZBsMgzSC/AyzGeR3ldlC/yXbRDFT5bon68DxC9OErNFpjyLYZpBmgWbO/+uurcOeGtc8WPNfbr44OJ6dtyuCMUij1kJRsLPFW0Vwa4aBF19f/K/eW/3faLbBf9FuATA2mqz/v2ZrzX+QHNVzLwAjy7LsIPIjLQAAAABJRU5ErkJggg==\">';
    		}
    		
    		$tabla.= ',"<label id=\"'.$obj1[0]['suministro'].'montoconsumo\" >'.$obj1[0]['montoconsumo1'].'</label>"';
    		$tabla.= ',"<label id=\"'.$obj1[0]['suministro'].'resultadoevaluacion\" >'.$icon.'</label>"';
    		$tabla.= ',"<a class=\'btn btn-info btn-sm btn fancybox-inconsistencia fancybox.iframe\' href=\"'.ENV_WEBROOT_FULL_URL.'EvaluarInconsistencias/ajax_imagen_por_inconsistencia/'.$obj1[0]['suministro'].'\"><i class=\"fa fa-camera\"></i></a>"';
    		$tabla.=']';
    		if ($j<count($arr_obj_incosistencia)-1){
    			$tabla.=',';
    		}
    		$j=$j+1;
    		$total_records =$obj1[0]['row_total'];
    	};
    
    	$vari='{
    		"sEcho": "'.intval($_GET['sEcho']).'",
    		"iTotalRecords":"'.$total_records.'",
    		"iTotalDisplayRecords":"'.$total_records.'",
			"aaData":[';
    	$vari.=$tabla;
    	$vari.=']}';
    	return $vari;
    }
  
	public function evaluar_inconsistenciaintro($criterio=null,$date_start=null,$date_end=null,$unidadneg=null,$ciclo=null,$sector=null,$suministro=null,$lecturista=null,$consumo_min=null,$consumo_max=null,$filter_advance=null,$id_xml=null) {
		$this->autoRender = false;	
		ini_set('memory_limit', '-1');
		ini_set('max_execution_time', 300000);
		set_time_limit(0);

		/*
		 * Paging
		*/
		if ( isset( $_GET['iDisplayStart'] ) && $_GET['iDisplayLength'] != '-1' )
		{
			$page = intval( $_GET['iDisplayStart'] );
			$limit = intval( $_GET['iDisplayLength'] );
		}

		$search = "";
		if ( isset($_GET['sSearch']) && $_GET['sSearch'] != "" ){
			$search = $_GET['sSearch'];
		}
		$negocio = $this->Session->read('NEGOCIO');

		$this->loadModel('ComlecOrdenlectura');
		if($suministro != '0'){
        	$this->ComlecOrdenlectura->setDataSource (strtolower($negocio));
        	$obj_suministro = $this->ComlecOrdenlectura->findBySuministro($suministro);
        	if(isset($obj_suministro['ComlecOrdenlectura']['orden'])){
        		$suministro_orden = $obj_suministro['ComlecOrdenlectura']['orden'];
        		$sector = $obj_suministro['ComlecOrdenlectura']['sector'];
        		$ruta = $obj_suministro['ComlecOrdenlectura']['ruta'];
        	}else{
        		$suministro_orden = '0';
        	}
		}else{
			$suministro_orden = '';
		}

		if($consumo_min == '0'){
			$consumo_min = null;			
		}

		if($consumo_max == '0'){
			$consumo_max = null;
		}
		
		$filtros = array();
		if(isset($date_start)){
			$filtros['date_start'] = $date_start;
		}
		if(isset($date_end)){
			$filtros['date_end'] = $date_end;
		}
		if(isset($unidadneg)){
			$filtros['unidadneg'] = $unidadneg;
		}
		if(isset($ciclo)){
			$filtros['ciclo'] = $ciclo;
		}
		if(isset($sector)){
			$filtros['sector'] = $sector;
		}
		if(isset($ruta)){
			$filtros['ruta'] = $ruta;
		}
		if(isset($suministro)){
			$filtros['suministro'] = $suministro;
			$filtros['suministro_orden'] = $suministro_orden;
		}
		if(isset($lecturista)){
			$filtros['lecturista'] = $lecturista;
		}
		if(isset($id_xml) && $id_xml!='0' && $id_xml!=''){
			$filtros['id_xml'] = $id_xml;
		}
		
		if($negocio=='TAC'){
			$arr_obj_incosistencia = $this->ComlecOrdenlectura->listarInconsistenciasTac($sector,$suministro_id,$lecturista,$filter_advance,$page,$limit,$search,$consumo_min,$consumo_max);
		}else{
			$arr_obj_incosistencia = $this->ComlecOrdenlectura->listarInconsistenciasCix($criterio,$filtros,$filter_advance,$page,$limit,$search,$consumo_min,$consumo_max);
		}

		/*
		if($negocio=='CIX'){
			$arr_obj_incosistencia = $this->ComlecOrdenlectura->listarInconsistenciasCix($sector,$suministro_id,$lecturista,$filter_advance,$page,$limit,$search,$consumo_min,$consumo_max);
		}else{
			$arr_obj_incosistencia = $this->ComlecOrdenlectura->listarInconsistenciasTac($sector,$suministro_id,$lecturista,$filter_advance,$page,$limit,$search,$consumo_min,$consumo_max);
		}
		*/
    	
    	$j = 1;
    	$i = 1;
    	$tabla='';
		$total_records=0;
    	foreach ($arr_obj_incosistencia as $obj1){
    		
    		if(trim($obj1[0]['tipolectura'])=='R' && $obj1[0]['lecturao2']=='' && $obj1[0]['obs2']==''){
    			$tipolecturaforsave = 'R';
    		}else{
    			$tipolecturaforsave = 'L';
    		}
    		$tabla.='[';
    		$tabla.= '"<label class=\"hidden\" readonly type=\"text\" id=\"'.$i.'codigo\" placeholder=\"Codigo\" data-tipo_lectura=\"'.$tipolecturaforsave.'\" data-tipo_lectura_real=\"'.$obj1[0]['tipolectura'].'\">'.$obj1[0]['comlec_ordenlectura_id'].'</label>"';
    		$negocio = $this->Session->read('NEGOCIO');
    		if($negocio=='TAC'){
    			$tabla.= ',"<a href=\"javascript:void(0);\" id=\"btn-auditoria-suministro\" val_suministro=\"'.trim($obj1[0]['suministro']).'\" title=\"Ver Auditoria\">'.$obj1[0]['lcorrelati'].'</a>"';
    		}else{
    			$tabla.= ',"<a href=\"javascript:void(0);\" id=\"btn-auditoria-suministro\" val_suministro=\"'.trim($obj1[0]['suministro']).'\" title=\"Ver Auditoria\">'.$obj1[0]['orden'].'</a>"';
    		}
    		$icon = '';
    		$icon_estado_ws = '';
    		if(trim($obj1[0]['tipolectura'])=='R' && trim($obj1[0]['lecturao2'])=='' && trim($obj1[0]['obs2'])==''){
    			$input_disabled = 'disabled';
    			$btn_RL = 'disabled';
    			$msg_relectura = '<img title= \"RELECTURA\" src=\"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAABpUlEQVR42q2TyytEURzH/QFWRCFTVhJF2WFla0XJwkKxlJ2VRxI1lA2RlRRWnqVGiRqPmJJXYqKGPEOGYdIwg/Nxzpm55ozByq1v3c6538fve85NSPjvhzcfnPTBYQ3slyP2ymC3Ajx21N7fZN8S4qgKAkO8M4N4HQBvB9w26nXhzIabOX4lc1qHn3WCjMFHJ7w0S4EGvc5JtRYJztviRXRsGTeAU5KH+KALQh0xAsJTiXCXadyN22S6h6iImlk89krX4bCz5f7YKN3qowK6j1IeHLncu+yGgCopNBEmWVBkI75KqMgKgeVCPCMlpkBppLCGWFzXaugC3WF3tovxL+aw05NmCMhF3XaE8AVVXGR2y53NQj1CrIDakEelo5pQsY3o2kgKnI3a2OovigqIg/boxyYsokH2ObJw96fgcXQaAvJI1FxWy3HECPllKY/j4XRW2lIJPXu/3YWLaZ5mM+IcLShnRV5rTcS7O/3zbRSnk5yPpOk0odV87ajeL8czdWxnU+rvZOt5f77haqGZvcECNuzJuNqTcHUX4J5qIei/5d//3k/ScyAkq+CgnAAAAABJRU5ErkJggg==\">';
    		}elseif(trim($obj1[0]['tipolectura'])=='R' && (trim($obj1[0]['lecturao2'])!='' || trim($obj1[0]['obs2'])!='')){
    			$input_disabled = '';
    			$btn_RL = 'disabled';
    			$msg_relectura = '';
    		}else{
    			$input_disabled = '';
    			$btn_RL = '';
    			$msg_relectura = '';
    		}
    		
    		if($obj1[0]['libro']==''){
    			$input_disabled = 'disabled';
    			$btn_RL = 'disabled';
    			$msg_no_asignado = '<a class=\"label label-warning\"><i class=\"fa fa-warning\"></i> No Asignado</a>';
    		}else{
    			$msg_no_asignado = '';
    		}
    		
    		if ($obj1[0]['resultadoevaluacion']=='INCONSISTENTE'){
    			$icon = '<img title= \"INCONSISTENTE\" src=\"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAABrElEQVR42q2TyytEURzH/QFW1JRHIxsTUiNWHgtCFOWRPEpZyAJZkKSZhccgsVBIFqRkZdKUZCFFNLEQJUnJQk0eGcQCk+bj/O6dyZ3rsXLr272dcz7f3/d3zrlRUf/98PIIK1Mw0gzOKnCU6+/FUYJPD/wNH23DcB14F+DcA7sz4B5ScK8+XmuDvXV+h6da4GkfzpZhywWePljqhLlWmGiEwWoCFdbvJlpsNcm1MjmZVwnGfzaQdpT8RdbIdrSe16fhcFEHRWsOtRfdsNCuJ5MWxKCrgMeadPzTowYDp5o8dutQWAKb4tNTqhm8NmdxWZVnMFCDbM3ogFECTzZFVKctl7eGDE6zEwwGalDbbQHCkthSOQyHqstaaeHYHmcw6CjSj0oAoyS2CaY1i6vCJI7Kcr4MgrMD+mK5MEYJaIKlusS/GHMZDNSRvFWmfFUzgyE4UJ/GZW48ezYLAf+96S5srvJckhgJhUCRFBDYmxyNf83z820Mbrjx5cVpMeWoZLfl26cujsTeSbH8Doefj7sbfCMOToozOUyN5cAWw0G+nbN+J+93t/z73/sJB/zttLkU5gUAAAAASUVORK5CYII=\">';
    		}
    		
    		if ($obj1[0]['resultadoevaluacion']=='CONSISTENTE'){
    			$icon = '<img title= \"CONSISTENTE\" src=\"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAB8ElEQVR42mNgoDb4/OfD/9UPpv6vv5z4v+C83/+sM17/c0+F/595o/f/u+/v/uPVfOHrgf8114L+L33f+H/j997/s98V/O95Hvu/5UHI/5wzLv+dtuv+3/Vy23+cmuvvRvw/9H/h/6U/qv53fgj93/DM93/VA/f/Jbcd/xdctfmffcH8v+kaqf/bnqMZAnJ22RW///v+zP0/52f+/76vUXADah97gQ3IvWz1P/OcKRgrLZNG9c6K+1P+z3lZDrcZhFtfB4E1V9xz/V980wFsQNoZ4/9xJ/T/W62T+d9+oh1hQOm5iP8bPvX9b30ZCMfozgfZnAjUHHlM+7/HdpX/NkusEQYknHACuwCkAWQrzGYQBmkGYZjtQYc0/1tvlv0vN1kSYUDkYdv/Ex6lgG1DxjDNyLb77FcDB6T0BAmEATGHvP933ouCa4BhUKiDMLJmj70q/7Xmi/63mm2JMKD7Yis4nmGhjIzjoJoDD2qANYNsl53C+79lbwvCAFCUGG5QBfsTZBsMgzSC/AyzGeR3ldlC/yXbRDFT5bon68DxC9OErNFpjyLYZpBmgWbO/+uurcOeGtc8WPNfbr44OJ6dtyuCMUij1kJRsLPFW0Vwa4aBF19f/K/eW/3faLbBf9FuATA2mqz/v2ZrzX+QHNVzLwAjy7LsIPIjLQAAAABJRU5ErkJggg==\">';
    			if($obj1[0]['ws_l_estado']=='1'){
    				$icon_estado_ws = '<a href=\"javascript: void(0);\" class=\"btn-icon-enviado-a-distriluz\" data-suministro=\"'.trim($obj1[0]['suministro']).'\" data-index=\"'.$i.'\"><img title= \"Enviado a Distriluz\" src=\"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAACpElEQVR42mNgQAP/79dzFG92EJl9JME1d2t6c9iOqtmRu2a0BV3Y46i9qk8o9/Z/dgZc4O05f63OnQELnJZa/Weab/yfYR4QzzH8zzDX4D/DQuv/8uvK/1vvXznd98Z3RQzNz856uCettXnMPgeoaSYOPEP/P8tCl//S2xY89Lr7RRfh7MvemnVb7V8zzsSjGY6NgNjwv9aJA18Ztt1mZ/h/O5d9+8nAJVLTdAhqZpvn+IdztvFXlplAL80O/h/+8N0ihlf3QyVm7LAAOs8EbDLDLKDkDDTN03T/i6wt/R/64GfX96uuMe6LjN4wTFX+L37ywn+GV8/TQ0LmaEE0z/H+r7Wn7WzJeuufDNON/oO9NFXrv8Leaf89b76PA3v3XYpe8Wa7x0zTtf8zLG7/z/DicXq+zlSQAbb/jY9uuB769J3cvXsx+n2b7f4wTNX7r7R3JlDzY0+w5tdpkrN3Oz5iA4eDPtC1yUADXmQk2c0AGjDD8L/a3hV/HS6+0AEpfnEnVmfSka7Lbne+BoCj+Gmi7LJ9zh8ZphpCvaYHjObi/wxv3iSbVa/ShwhO1f2vvnfR/+D7/91REteDUMveHfbvEZqBeLrmf/ZNm/4DYyGab9MR99sMU/SgEgb/JdbV/454+j8bkjLD/dPWWb9gmGGEGpVTjf7bXn32mOH///+MV855RcatNUUKfdP/fMuyvlRfXnk4bLnJUxb0KJ2m/l9o76r/0S8/WUOc+H8R994DTpOM5ur/RSg0BKc8rAlpacF/t5u3G1b9/8+M8Of/I7w7dtu1OC63fcMzB5RQgIpnGUFTngE4fTAtdP0vtqb8reWFKxVAl7NhzVC3z0ZbxKyyyfdZ47fDZKnnXY2lAW+016Q9N91cd0BydX1K9N2PpgzUBADlgqaJmthzQAAAAABJRU5ErkJggg==\"></a>';
    				$input_disabled = 'disabled';
    				$btn_RL = 'disabled';
    			}else{
    				$icon_estado_ws = '';
    			}
    		}
    		
    		if(trim($obj1[0]['tipolectura'])=='L'){
    			$nombre_lecturista = $obj1[0]['lecturista1'];
    			//$tabla.= ',"<a href=\"javascript:void(0);\" class=\"btn-tooltip\" data-toggle=\"tooltip\" data-placement=\"bottom\" title=\"'.$obj1[0]['lecturista1'].'\">'.$obj1[0]['seriefab'].'</a>"';
    		}else{
    			$nombre_lecturista = $obj1[0]['lecturista2'];
    			//$tabla.= ',"<a href=\"javascript:void(0);\" class=\"btn-tooltip\" data-toggle=\"tooltip\" data-placement=\"bottom\" title=\"'.$obj1[0]['lecturista2'].'\">'.$obj1[0]['seriefab'].'</a>"';
    		}
    		//$tabla.= ',"'.trim($obj1[0]['cliente']).'"';
    		//Suministro
    		$tabla.= ',"<a href=\"javascript:void(0);\" id=\"btn-lectura-inconsistencia\" data-popover=\"popover\" data-original-title=\"Suministro: '.$obj1[0]['suministro'].'\" data-content=\"Medidor: '.$obj1[0]['seriefab'].'<br>Cliente: '.trim($obj1[0]['cliente']).'<br>Direcci&oacute;n: '.str_replace('\\','/',trim($obj1[0]['direccion'])).'<br>Lecturista: '.$nombre_lecturista.'\" data-placement=\"bottom\" data-trigger=\"hover\">'.$obj1[0]['suministro'].'</a>"';
			//$tabla.= ',"'.number_format($obj1[0]['lap']).'"';
    		$tabla.= ',"'.number_format($obj1[0]['lecant']).'"';
    		//lectura
    		if(trim($obj1[0]['tipolectura'])=='L' || (trim($obj1[0]['tipolectura'])=='R' && ($obj1[0]['lecturao2']!='' || $obj1[0]['obs2']!=''))){	
    			$tabla.= ',"<input '.$input_disabled.' style=\"width:60px;\" type=\"text\" id=\"'.$i.'lectura\" tabindex=\"'.$j++.'\" suministro=\"'.$obj1[0]['suministro'].'\" value=\"'.trim($obj1[0]['lecturao1']).'\" class=\"TabOnEnter2\" ><label id=\"'.$i.'etiquetalectura\"><label>"';
    		}else{
    			$tabla.= ',"'.trim($obj1[0]['lecturao1']).'<label id=\"'.$i.'etiquetalectura\"><label>"';
    		}
    		//relectura
    		if(trim($obj1[0]['tipolectura'])=='R' && $obj1[0]['lecturao2']=='' && $obj1[0]['obs2']==''){
    			if($icon_estado_ws==''){
    				$tabla.= ',"<input style=\"width:60px;\" type=\"text\" id=\"'.$i.'lectura\" tabindex=\"'.$j++.'\" suministro=\"'.$obj1[0]['suministro'].'\" value=\"'.trim($obj1[0]['lecturao2']).'\" class=\"TabOnEnter2\" ><label id=\"'.$i.'etiquetalectura\"><label><br><a href=\"javascript:void(0);\" suministro=\"'.$obj1[0]['suministro'].'\" data-index=\"'.$i.'\" class=\"clickpasslecturafromrelectura\" style=\"display:none;\">pasar a lectura</a>"';
    			}else{
    				$tabla.= ',"<input style=\"width:60px;\" type=\"text\" id=\"'.$i.'lectura\" tabindex=\"'.$j++.'\" suministro=\"'.$obj1[0]['suministro'].'\" value=\"'.trim($obj1[0]['lecturao2']).'\" class=\"TabOnEnter2\" ><label id=\"'.$i.'etiquetalectura\"><label>"';    				
    			}    			
    		}elseif(trim($obj1[0]['tipolectura'])=='L' && $obj1[0]['libro']==''){
    			$tabla.= ',"<label id=\"r\" >'.$obj1[0]['lecturao2'].'</label>"';
   			}elseif($obj1[0]['lecturao2']!=''){
   				if($icon_estado_ws==''){
   					$tabla.= ',"<label id=\"r\" >'.$obj1[0]['lecturao2'].'</label><br><a href=\"javascript:void(0);\" suministro=\"'.$obj1[0]['suministro'].'\"  lectura=\"'.$obj1[0]['lecturao2'].'\" data-index=\"'.$i.'\" class=\"clickpasslectura\">pasar a lectura</a>"';   					
   				}else{
	   				$tabla.= ',"<label id=\"r\" >'.$obj1[0]['lecturao2'].'</label>"';
   				}
    		}else{
    			$tabla.= ',"<label id=\"r\" >'.$obj1[0]['lecturao2'].'</label>"';    			
    		}
			//$tabla.= ',"<label id=\"rr\" suministro=\"'.$obj1[0]['suministro'].'\"  lectura=\"'.$obj1[0]['lecturao3'].'\" data-index=\"'.$i.'\" class=\"clickpasslectura\" >'.$obj1[0]['lecturao3'].'</label>"'; 
			$tabla.= ',"'.$obj1[0]['obs_ant_px'].'"'; 
			
			//lectura obs
			if(trim($obj1[0]['tipolectura'])=='L' || (trim($obj1[0]['tipolectura'])=='R' && ($obj1[0]['lecturao2']!='' || $obj1[0]['obs2']!=''))){
				$tabla.= ',"<input '.$input_disabled.' style=\"width:25px;\" type=\"text\" id=\"'.$i.'codigoobservacion\" tabindex=\"'.$j++.'\" data-index=\"'.$i.'\" suministro=\"'.$obj1[0]['suministro'].'\" value=\"'.trim($obj1[0]['obs1']).'\" class=\"TabOnEnter2\" ><label id=\"'.$i.'etiquetacodigo\"><label>"';
			}else{
				$tabla.= ',"'.trim($obj1[0]['obs1']).'<label id=\"'.$i.'etiquetacodigo\"><label>"';
			}
			
			//relectura obs
			if(trim($obj1[0]['tipolectura'])=='R' && $obj1[0]['lecturao2']=='' && $obj1[0]['obs2']==''){
				$tabla.= ',"<input style=\"width:25px;\" type=\"text\" id=\"'.$i.'codigoobservacion\" tabindex=\"'.$j++.'\" data-index=\"'.$i.'\" suministro=\"'.$obj1[0]['suministro'].'\" value=\"'.trim($obj1[0]['obs2']).'\" class=\"TabOnEnter2\" ><label id=\"'.$i.'etiquetacodigo\"><label>"';				
			}else{
				$tabla.= ',"'.$obj1[0]['obs2'].'"';
			}
			$tabla.= ',"'.number_format($obj1[0]['consant'],2).'"';
			$tabla.= ',"'.number_format($obj1[0]['promedio'],2).'"';
			//Monto Consumo
			$tabla.= ',"<a href=\"javascript:void(0);\" id=\"btn-formula-suministro\" data-suministro=\"'.trim($obj1[0]['suministro']).'\" title=\"Ver Formula\"><label id=\"'.$i.'montoconsumo\" style=\"cursor: pointer;\">'.($obj1[0]['montoconsumo1']!=''?number_format($obj1[0]['montoconsumo1'],2):'').'</label></a>"';
			
			if(trim($obj1[0]['validacion'])>='1'){
				$validacion = 'Forzado Manual';
			}else{
				$validacion = 'Normal';
			}
			
			//if(trim($obj1[0]['tipolectura'])=='L'){
			$tabla.= ',"'.$obj1[0]['metodofinal1'].'|<a href=\"javascript:void(0);\" class=\"btn-tooltip\" data-toggle=\"tooltip\" data-placement=\"bottom\" title=\"'.$validacion.'\">'.$obj1[0]['validacion'].'</a>"';
			//}elseif(trim($obj1[0]['tipolectura'])=='R'){
			//	$tabla.= ',"'.$obj1[0]['metodofinal2'].'"';
			//}else{
			//	$tabla.= ',"'.$obj1[0]['metodofinal3'].'"';
			//}
			
			if($msg_relectura != ''){
				$resultado_msg= $msg_relectura;
			}else{
				$resultado_msg= $icon;
			}
			$tabla.= ',"<label id=\"'.$i.'resultadoevaluacion\" class=\"lbl_resultado_evaluacion\">'.$resultado_msg.'</label>'.$icon_estado_ws.$msg_no_asignado.'"'; 
			if($obj1[0]['foto1']!='' or $obj1[0]['foto2']!='' or $obj1[0]['foto3']!=''){
				if($icon_estado_ws==''){
					$btn_foto = '<a class=\"btn btn-info btn-sm btn fancybox-inconsistencia fancybox.iframe\" href=\"'.ENV_WEBROOT_FULL_URL.'/EvaluarInconsistencias/ajax_imagen_por_inconsistencia/'.$obj1[0]['suministro'].'\" onclick=\"javascript: $(\'.btn-forzar-'.trim($obj1[0]['suministro']).'\').attr(\'disabled\',false);\"><i class=\"fa fa-camera\"></i></a>';
				}else{
					$btn_foto = '<a class=\"btn btn-info btn-sm btn fancybox-inconsistencia fancybox.iframe\" href=\"'.ENV_WEBROOT_FULL_URL.'/EvaluarInconsistencias/ajax_imagen_por_inconsistencia/'.$obj1[0]['suministro'].'\"><i class=\"fa fa-camera\"></i></a>';
				}
				$input_disabled = 'disabled';
			}else{
				$btn_foto = '<a class=\"btn btn-sm\" disabled><i class=\"fa fa-camera\"></i></a>';
			}
			$tabla.= ',"<a '.$input_disabled.' href=\"javascript:void(0)\" val_suministro = \"'.trim($obj1[0]['suministro']).'\" class=\"btn btn-default btn-sm btn-finalizar-lectura-inconsistencia btn-forzar-'.trim($obj1[0]['suministro']).'\"><i class=\"fa fa-check\"></i></a> <a '.$btn_RL.' href=\"javascript:void(0)\" val_suministro = \"'.trim($obj1[0]['suministro']).'\" class=\"btn btn-default btn-sm btn-re-lectura-inconsistencia\">RL</a> '.$btn_foto.'"';
    		$tabla.='],';
    		$i++;
			$total_records =$obj1[0]['row_total'];
    	};
    	
    	$tabla = substr($tabla,0,-1);

		$vari='{
    		"sEcho": "'.intval($_GET['sEcho']).'",
    		"iTotalRecords":"'.$total_records.'",
    		"iTotalDisplayRecords":"'.$total_records.'",
			"aaData":[';
		$vari.=$tabla;
		$vari.=']}';
		return $vari;		
    }
    
    public function pdf_inconsistenciaintro($sector,$lecturista) {
    	$this->layout = 'ajax';
    	set_time_limit(0);
    	$this->loadModel('ComlecOrdenlectura');
    
    	//javier verificar
    	$arr_obj_incosistencia = $this->ComlecOrdenlectura->listarInconsistenciasPdf($sector, $lecturista);
     
    	$j=0;
    	$tabla='';
    	
    	foreach ($arr_obj_incosistencia as $obj1){
    		$tabla.='[';
    		//   var_dump(count($conceptosByGrupo));exit;
    		$i=0;
    		$m=$j+1;
			
    		$tabla.= '"'.$obj1[0]['ruta'].'"';
    		$tabla.= ',"'.$obj1[0]['orden'].'"';
    		$tabla.= ',"'.$obj1[0]['cliente'].'"';
    		$tabla.= ',"'.$obj1[0]['direccion'].'"';
    		$tabla.= ',"'.$obj1[0]['suministro'].'"';
    		$tabla.= ',"'.$obj1[0]['seriefab'].'"';
    		$tabla.= ',"______________________"';
    		$tabla.= ',"[____]"';
    		$tabla.=']';
    		if ($j<count($arr_obj_incosistencia)-1){
    			$tabla.=',';
    		}
    		$j=$j+1;
    	};
    
    
    	$vari=trim('{"aaData":[');
    	$vari.=trim($tabla);
    	$vari.=']}';
    	echo trim($vari);
    	exit();
    
    	// $this->set(compact('arr_obj_incosistencia'));
    }
    
    public function  evalar_relectura(){
    	$this->layout = 'ajax';
    	$this->loadModel('ComlecOrdenlectura');
    	$suministro = $this->request->data['suministro'];
    	$tipo_lectura = 'L';
    	$arr_obj_incosistencia_relectura = $this->ComlecOrdenlectura->inconsistenciaRelectura($suministro, $tipo_lectura);
    	echo $arr_obj_incosistencia_relectura[0][0]['funcion_relecturas'];
    	exit();
    		
    }
    
    public function  finalizar_relectura(){
    	$this->layout = 'ajax';
    	$this->loadModel('ComlecOrdenlectura');
    	$suministro = $this->request->data['suministro'];
    	    	 
    	$arr_obj_incosistencia_relectura = $this->ComlecOrdenlectura->inconsistenciaFinalizarlectura($suministro);
    	echo $arr_obj_incosistencia_relectura[0][0]['funcion_forzarrelectura'];
    	exit();
    
    }
    
    public function inconsistencia_historico_por_lectura($suministro, $modulo) {
    	$this->layout = 'ajax';
    	$this->loadModel('ComlecOrdenlectura');
    	$id_usuario = $this->obj_logged_user['Usuario']['id'];
    	$arr_suministros = $this->ComlecOrdenlectura->buscarSuministroHistorico($suministro, 'all', 4);
    	//debug($arr_suministros);
    	$this->set(compact('arr_suministros','id_usuario'));    	
    }

    public function inconsistencia_auditoria_por_lectura($suministro, $modulo) {
    	$this->layout = 'ajax';
    	$this->loadModel('ComlecOrdenlectura');
    	$id_usuario = $this->obj_logged_user['Usuario']['id'];
    	$arr_suministros = $this->ComlecOrdenlectura->buscarSuministroAuditoria($suministro);
    	//debug($arr_suministros);
    	$this->set(compact('arr_suministros','id_usuario'));
    }
    
    public function ajax_imagen_por_inconsistencia($suministro, $pfactura=null){
   	$this->layout = 'ajax';
   	$this->loadModel('ComlecOrdenlectura');
   	
   	if(!isset($pfactura)){
   		$pfactura='';
   	}
   	$arr_obj_incosistencia = $this->ComlecOrdenlectura->buscarSuministroHistorico($suministro, $pfactura);
   	/*$arr_obj_incosistencia = $this->ComlecOrdenlectura->find('all', array(
   			'fields' => array('foto1','foto2','foto3','fechaejecucion1','fechaejecucion2','fechaejecucion3'),
   			'conditions' => array('suministro' => $suministro)
   	));*/
   	
   	$this->set(compact('arr_obj_incosistencia'));   	
   }
   
   public function pdf_inconsistencia($sector,$lecturista){
   	$this->layout = 'ajax';
   	$this->set(compact('sector','lecturista'));
   	
   	
   }
   
   public function cambiar_codigo_lecturas(){
   	set_time_limit(0);
   	$this->layout = 'ajax';
   	$this->loadModel('ComlecOrdenlectura');
   	$suministro = $this->request->data['suministro'];
   	$observacion = $this->request->data['observacion'];
   	
   	if (empty($observacion) ){
   		$observacion='NULL';
   	}
   	
   	if ($observacion != 'NULL'){
   		$validarCodigoObservacion = $this->ComlecOrdenlectura->validarCodigoObservacion($observacion);
   	
   		if (!($validarCodigoObservacion)){
   			echo json_encode(array('save' => false,'mensaje'=>'Codigo No Existe'));
   			exit();
   		}   	
   	}
   	
   	$cambiar_observacion = $this->ComlecOrdenlectura->cambiarCodigoLecturas($observacion, $suministro);
   	$mensaje='OK';
   	if($cambiar_observacion){
   		$mensaje='NOT';
   	}
   	echo json_encode(array('save' => true,'mensaje'=>$mensaje));
   	exit;
   }
   
   public function saveregistarlectura(){
   	$this->layout = 'ajax';
   	$this->loadModel('ComlecOrdenlectura');
   	
   	$tipolectura = $this->request->data['tipolectura'];
   	$tipolecturareal = $this->request->data['tipolecturareal'];
  
   	$lectura = $this->request->data['lectura'];
   	$codigo= $this->request->data['codigo'];
   	$codigoobservacion= $this->request->data['codigoobservacion'];
   
   
   	if (empty($lectura) ){
   		$lectura='NULL';
   	}
   	if (empty($codigoobservacion) ){
   		$codigoobservacion='NULL';
   	}
   
   	if ($codigoobservacion != 'NULL'){
   		$validarCodigoObservacion = $this->ComlecOrdenlectura->validarCodigoObservacion($codigoobservacion);
   
   
   		if ($validarCodigoObservacion){
   			$descripcioncodigo=$validarCodigoObservacion[0][0]['descripcion'];
   			$validarCodigoObservacion=$validarCodigoObservacion[0][0]['condicional'];
   
   			if ($validarCodigoObservacion==1 && $lectura==0 ){
   				 
   				echo json_encode(array('save' => true,'mensaje'=>'La lectura es obligatoria','validacionlectura'=>true,'validacioncodigo'=>false,'etiquetacodigo'=>$descripcioncodigo,'montoconsumo'=>'','resultadoevaluacion'=>''));
   				exit();
   			}
   			if ($validarCodigoObservacion==2 && $lectura!= 'NULL' ){
   				echo json_encode(array('save' => true,'mensaje'=>'Este codigo no permite ingresar una lectura','validacionlectura'=>true,'validacioncodigo'=>false,'etiquetacodigo'=>$descripcioncodigo,'montoconsumo'=>'','resultadoevaluacion'=>''));
   				exit();
   			}
   		}
   		else
   		{
   			echo json_encode(array('save' => true,'mensaje'=>'Codigo no Existe','validacionlectura'=>false,'validacioncodigo'=>true,'etiquetacodigo'=>'Codigo No Existe','montoconsumo'=>'','resultadoevaluacion'=>''));
   			exit();
   		}
   
   
   	} else {
   		$descripcioncodigo='';
   	}
   
   	$this->loadModel('Usuario');
   
   	$guardar= $this->ComlecOrdenlectura->saveModuloInconsistencias($tipolectura,$lectura,$codigo,$codigoobservacion,$this->obj_logged_user['Usuario']['id']);
   	if($guardar){
   		$listaDigitarLecturaIndividual = $this->ComlecOrdenlectura->getLecturaBySuministro($tipolecturareal,$codigo);
   		$montoconsumo=$listaDigitarLecturaIndividual[0][0]['montoconsumo1'];
   		$resultadoevaluacion=$listaDigitarLecturaIndividual[0][0]['resultadoevaluacion'];
   	}
   	echo json_encode(array('save' => true,'mensaje'=>'false','validacionlectura'=>false,'validacioncodigo'=>false,'etiquetacodigo'=>$descripcioncodigo,'montoconsumo'=>$montoconsumo,'resultadoevaluacion'=>$resultadoevaluacion));
   	exit;
   }
   
   public function excel_inconsistencia(){
   	$this->layout = 'home_tree';
   	$this->components[] = 'RequestHandler';
   	
   }
   
   public function show_excel_inconsistencia(){
   	$this->autoRender = false;
   	ini_set('memory_limit', '-1');
   	$this->layout = 'ajax';
   	if ($this->request->is('post')) {
   		$this->loadModel('Excel');
   		$negocio = $this->Session->read('NEGOCIO');
   		$this->Excel->setDataSource (strtolower($negocio));
   		   		
   		/* Upload archivo xml al servidor*/
   		$temp = explode(".", $_FILES["file"]["name"]);
   		$extension = end($temp);
   		if ($_FILES["file"]["error"] > 0) {
   			echo "ERROR: " . $_FILES["file"]["error"] . "";
   		} else {
   			if($extension=='zip' or $extension=='xls'){
   				move_uploaded_file($_FILES["file"]["tmp_name"], "files/XLS/" . $_FILES["file"]["name"]);
   				if ($extension=='zip'){
   					$zip = new ZipArchive;   					
   					if ($zip->open('files/XLS/'.$_FILES["file"]["name"], ZipArchive::CREATE)==TRUE) {
   						$filename = $zip->getNameIndex(0);
   						$temp_zip = explode(".",trim($filename));
   						$extension_in_zip = end($temp_zip);
   						if($extension_in_zip == 'xls'){
   							$xml_upload = $temp[0].'.xls';
   							$zip->extractTo('files/XLS/');
   							$zip->close();
   						}else{
   							echo 'Error descomprimiendo zip, no es un archivo XLS';
   							exit;
   						}   						
   					} else {
   						echo 'Error descomprimiendo zip';
   						exit;
   					}
   				}   				
   			}else{   				
   				echo 'No es un archivo Valida, solo esta permitido Xls o Zip de Xls';
   				exit;
   			}	   			
   		}
   		
   		$data = new Spreadsheet_Excel_Reader('files/XLS/'.$temp[0].'.xls', true);
   		$temp = $data->dumptoarray();
   		if($temp){
   			$data_query = 'INSERT INTO "lecturas"."excel" ("NombreRutaLectura", "SED", "IdNroServicio", "NombreCliente", "DireccionSuministro", "IdSector", "NombreSector", "IdTarifa", "NombreTarifa", "NombreMagnitud", "LecturaActual", "ConsumoActual", "Observacion", "EstadoSuministro", "LecturaAnterior", "ConsumoAnterior", "NumeroMeses", "Promedio", "SerieMedidor", "FactorMedicion", "FactorTransformacion", "ConsumoAntAnterior", "IdCiclo", "NroSecuenciaLectura") VALUES';
   			foreach ($temp as $key=>$valor){
   				if($key > 4){
   					$i=1;
   					foreach ($valor as $value){
   						$val[$i] = "'".addslashes($value)."'";
   						$i++;
   					}
   					$this->request->data['NombreRutaLectura'] = $val['1'];
   					$this->request->data['SED'] = $val['2'];
   					$this->request->data['IdNroServicio'] = $val['3'];
   					$this->request->data['NombreCliente'] = $val['4'];
   					$this->request->data['DireccionSuministro'] = $val['5'];
   					$this->request->data['IdSector'] = $val['6'];
   					$this->request->data['NombreSector'] = $val['7'];
   					$this->request->data['IdTarifa'] = $val['8'];
   					$this->request->data['NombreTarifa'] = $val['9'];
   					$this->request->data['NombreMagnitud'] = $val['10'];
   					$this->request->data['LecturaActual'] = $val['11'];
   					$this->request->data['ConsumoActual'] = $val['12'];
   					$this->request->data['Observacion'] = $val['13'];
   					$this->request->data['EstadoSuministro'] = $val['14'];
   					$this->request->data['LecturaAnterior'] = $val['15'];
   					$this->request->data['ConsumoAnterior'] = $val['16'];
   					$this->request->data['NumeroMeses'] = $val['17'];
   					$this->request->data['Promedio'] = $val['18'];
   					$this->request->data['SerieMedidor'] = $val['19'];
   					$this->request->data['FactorMedicion'] = $val['20'];
   					$this->request->data['FactorTransformacion'] = $val['21'];
   					$this->request->data['ConsumoAntAnterior'] =$val['22'];
   					$this->request->data['IdCiclo'] = $val['23'];
   					$this->request->data['NroSecuenciaLectura'] =$val['24'];
   					$sql ='(';
   					$sql .= implode(',',$this->request->data);
   					$query =  $sql.'),';
   					$data_query .= $query;
   				}
   			}
   			$data_query = substr($data_query,0,-1).';';
   			$statement = $this->Excel->saveExcel($data_query);
   		}
   	}
   }
   
   public function show_formulas_consistencia($suministro=null){
	   $this->layout = 'ajax';
	   
	   $negocio = $this->Session->read('NEGOCIO');
	   $this->loadModel('ComlecOrdenlectura');
	   $this->ComlecOrdenlectura->setDataSource (strtolower($negocio));
	   $obj_suministro = $this->ComlecOrdenlectura->find('first',array(
	   		'fields'=>array('montoconsumo1','consant','promedio','lecturao1','resultadoevaluacion','validacion'),
	   		'conditions'=>array('suministro'=>$suministro)));
	   $ca = number_format($obj_suministro['ComlecOrdenlectura']['montoconsumo1'],2,'.','');
	   $cac = number_format($obj_suministro['ComlecOrdenlectura']['consant'],2,'.','');
	   $pro = number_format($obj_suministro['ComlecOrdenlectura']['promedio'],2,'.','');
	   $lectura = $obj_suministro['ComlecOrdenlectura']['lecturao1'];
	   $resultadoevaluacion = $obj_suministro['ComlecOrdenlectura']['resultadoevaluacion'];
	   $forzado = $obj_suministro['ComlecOrdenlectura']['validacion'];
	   
	   $this->loadModel('Metodos');
	   $getRangos = $this->Metodos->getRangos();
	   $ObjMetodos = $this->Metodos;
	   
	   $this->set(compact('ca','cac','pro','getRangos','ObjMetodos','suministro','lectura','resultadoevaluacion','forzado'));   	
   }   
   
}
