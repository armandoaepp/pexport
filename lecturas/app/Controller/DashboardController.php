<?php 
App::uses('AppController', 'Controller');
App::uses('Sanitize', 'Utility');
App::uses('CakeTime', 'Utility');

class DashboardController extends AppController {

	public $helpers = array('Html', 'Form');
	public $name = 'Dashboard';
	
	public function beforeFilter(){
		parent::beforeFilter();
		$user_type = 'guest';
		$permisos = array("49", "25", "64");
		$this->Auth->allow(array());
		if($this->Auth->login()){
			$user_type = $this->obj_logged_user['Usuario']['id'];
			if (!in_array($user_type, $permisos)) {
				$user_type = 'usuario';
			}
		}

		$rules = array(
			'user_type' => $user_type,
			'redirect' => '/usuarios/logout',
			'message' => 'You dont have permission to access this page',
		    'action' =>  $this->params['action'],
		    'controller' =>  $this->params['controller'],
		    'groups' => array(
		        'guest' => array(),
		       	'usuario' => array('*'),
		    	'49' => array('*'),//Jorge
		        '25' => array('*'),//Jomira
		    	'64' => array('*')//Ensa reportes
		    ),
		    'views' => array(
		        'edit' => 'checkEdit',
		        'delete' => 'checkDelete',
			),
		);
		
		$this->UserPermissions->allow($rules);
	}
	
	public function init() {
		$this->layout = 'dashboard';
		$id_usuario = $this->obj_logged_user['Usuario']['id'];
		$arr_datos_usuario = $this->obj_logged_user['Usuario'];
		$this->set(compact('id_usuario','arr_datos_usuario'));		
	}
	
	public function index($unidad_neg=null,$idciclo=null) {
		$this->layout = 'dashboard';
		ini_set('memory_limit', '-1');
		ini_set('max_execution_time', 300000);
		set_time_limit(0);
		
		$id_usuario = $this->obj_logged_user['Usuario']['id'];
		$this->set(compact('id_usuario'));
		
		$this->loadModel('ComlecOrdenlectura');
		
		if(isset($unidad_neg) || isset($idciclo)){
			$options = array('unidad_neg'=>$unidad_neg,'idciclo'=>$idciclo);
		}else{
			$options = null;
		}
		$this->set(compact('unidad_neg','idciclo'));
		
		$listar_unidadneg = $this->ComlecOrdenlectura->listarUnidadneg();
		$listar_ciclo = $this->ComlecOrdenlectura->listarCiclo();
		$arr_ciclos_actuales = $this->ComlecOrdenlectura->getCiclosActuales();
		
		$this->set(compact('listar_unidadneg','listar_ciclo','arr_ciclos_actuales'));
		
		$listar_pfacrura = $this->ComlecOrdenlectura->listarPFactura();
		
		//Dia Actual
		$cantidad_ordenes_dia_actual = $this->ComlecOrdenlectura->getCountByFecha($options);
		$cantidad_avanzados_dia_actual = $this->ComlecOrdenlectura->getCountAvanzadosByFecha($options);
		$cantidad_consistentes_dia_actual = $this->ComlecOrdenlectura->getCountConsistentesByFecha($options);
		$cantidad_inconsistentes_dia_actual = $this->ComlecOrdenlectura->getCountInconsistentesActualesByFecha($options);
		$cantidad_inconsistentes_evaluadas_dia_actual = $this->ComlecOrdenlectura->getCountInconsistentesEvaluadasByFecha($options);
		$cantidad_asignados_dia_actual = $this->ComlecOrdenlectura->getCountAsignadosByFecha($options);
		$cantidad_descargados_dia_actual = $this->ComlecOrdenlectura->getCountDescargadosByFecha($options);
		
		$this->set(compact('cantidad_ordenes_dia_actual','cantidad_avanzados_dia_actual','cantidad_consistentes_dia_actual',
				'cantidad_inconsistentes_dia_actual','cantidad_inconsistentes_evaluadas_dia_actual','cantidad_asignados_dia_actual','cantidad_descargados_dia_actual'));
		
		
		//Periodo Actual		
		$cantidad_ordenes_periodo_actual = $this->ComlecOrdenlectura->getCount($options);
		$cantidad_avanzados_periodo_actual = $this->ComlecOrdenlectura->getCountAvanzados($options);
		$cantidad_consistentes_periodo_actual = $this->ComlecOrdenlectura->getCountConsistentes($options);
		$cantidad_inconsistentes_periodo_actual = $this->ComlecOrdenlectura->getCountInconsistentesActuales($options);
		$cantidad_inconsistentes_evaluadas_periodo_actual = $this->ComlecOrdenlectura->getCountInconsistentesEvaluadas($options);
		$cantidad_asignados_periodo_actual = $this->ComlecOrdenlectura->getCountAsignados($options);
		$cantidad_descargados_periodo_actual = $this->ComlecOrdenlectura->getCountDescargados($options);
		
		$this->set(compact('cantidad_ordenes_periodo_actual','cantidad_avanzados_periodo_actual','cantidad_consistentes_periodo_actual',
		'cantidad_inconsistentes_periodo_actual','cantidad_inconsistentes_evaluadas_periodo_actual','cantidad_asignados_periodo_actual','cantidad_descargados_periodo_actual'));
		
		//Periodo Anterior
		if(isset($unidad_neg) || isset($idciclo)){
			$options_periodo_anterior = array('unidad_neg'=>$unidad_neg,'idciclo'=>$idciclo);
		}else{
			$options_periodo_anterior = array();
		}
		$options_periodo_anterior['pfactura'] = $listar_pfacrura[0][0]['pfactura'];
		
		$cantidad_ordenes_periodo_anterior = $this->ComlecOrdenlectura->getCount($options_periodo_anterior);
		$cantidad_avanzados_periodo_anterior = $this->ComlecOrdenlectura->getCountAvanzados($options_periodo_anterior);
		$cantidad_consistentes_periodo_anterior = $this->ComlecOrdenlectura->getCountConsistentes($options_periodo_anterior);
		$cantidad_inconsistentes_periodo_anterior = $this->ComlecOrdenlectura->getCountInconsistentesActuales($options_periodo_anterior);
		$cantidad_inconsistentes_evaluadas_periodo_anterior = $this->ComlecOrdenlectura->getCountInconsistentesEvaluadas($options_periodo_anterior);
		$cantidad_asignados_periodo_anterior = $this->ComlecOrdenlectura->getCountAsignados($options_periodo_anterior);
		$cantidad_descargados_periodo_anterior = $this->ComlecOrdenlectura->getCountDescargados($options_periodo_anterior);
		
		$this->set(compact('cantidad_ordenes_periodo_anterior','cantidad_avanzados_periodo_anterior','cantidad_consistentes_periodo_anterior',
		'cantidad_inconsistentes_periodo_anterior','cantidad_inconsistentes_evaluadas_periodo_anterior','cantidad_asignados_periodo_anterior','cantidad_descargados_periodo_anterior'));
	}
	
	public function resumen_lecturas_kpi2($unidad_neg=null,$idciclo=null) {
		$this->layout = 'default_no_header';
		
		$id_usuario = $this->obj_logged_user['Usuario']['id'];
		$this->set(compact('id_usuario'));
		
		$this->loadModel('ComlecOrdenlectura');
		
		if(isset($unidad_neg) || isset($idciclo)){
			$options1 = array('unidad_neg'=>$unidad_neg,'idciclo'=>$idciclo);
			$options2 = array('unidad_neg'=>$unidad_neg,'idciclo'=>$idciclo);
		}else{
			$options1 = null;
			$options2 = null;
		}
		$this->set(compact('unidad_neg','idciclo'));
		
		$listar_pfacrura = $this->ComlecOrdenlectura->listarPFactura();
		
		// Dia Actual
		$cantidad_ordenes_dia_actual = $this->ComlecOrdenlectura->getCountByFecha($options1);
		$cantidad_observaciones_dia_actual = $this->ComlecOrdenlectura->getCountObs1ByFecha($options1);
		$cantidad_observaciones_sin_lectura_dia_actual = $this->ComlecOrdenlectura->getCountObs1SinLecturaByFecha($options1);
		$cantidad_observaciones_con_lectura_dia_actual = $this->ComlecOrdenlectura->getCountObs1ConLecturaByFecha($options1);
		$cantidad_obs_99_dia_actual = $this->ComlecOrdenlectura->getCountObs1IgualAByFecha($options1,99);
		$cantidad_tiempoejecucion_dia_actual = $this->ComlecOrdenlectura->getCountTiempoPromedioEjecucionByFecha($options1);
		$cantidad_consumo_cero_dia_actual = $this->ComlecOrdenlectura->getCountConsumoCeroByFecha($options1);
		$cantidad_consumo_menor35_dia_actual = $this->ComlecOrdenlectura->getCountConsumoMenoresQueByFecha($options1);
		$cantidad_consumo_mayor1000_dia_actual = $this->ComlecOrdenlectura->getCountConsumoMayoresQueByFecha($options1);
		$cantidad_consumo_mayor100porciento_dia_actual = $this->ComlecOrdenlectura->getCountConsumoMayor100PorcientoQueAnteriorByFecha($options1);
		
		$this->set(compact('cantidad_ordenes_dia_actual','cantidad_observaciones_dia_actual','cantidad_observaciones_sin_lectura_dia_actual','cantidad_observaciones_con_lectura_dia_actual','cantidad_obs_99_dia_actual','cantidad_tiempoejecucion_dia_actual', 'cantidad_consumo_cero_dia_actual', 'cantidad_consumo_menor35_dia_actual','cantidad_consumo_mayor1000_dia_actual','cantidad_consumo_mayor100porciento_dia_actual'));
		
		
		// Periodo Actual
		if(isset($pfactura)){
			$options1['pfactura'] = $pfactura;
		}
		
		$cantidad_ordenes = $this->ComlecOrdenlectura->getCount($options1);
		$cantidad_observaciones = $this->ComlecOrdenlectura->getCountObs1($options1);
		$cantidad_observaciones_sin_lectura = $this->ComlecOrdenlectura->getCountObs1SinLectura($options1);
		$cantidad_observaciones_con_lectura = $this->ComlecOrdenlectura->getCountObs1ConLectura($options1);
		$cantidad_obs_99 = $this->ComlecOrdenlectura->getCountObs1IgualA($options1,99);
		$cantidad_tiempoejecucion = $this->ComlecOrdenlectura->getCountTiempoPromedioEjecucion($options1);
		$cantidad_consumo_cero = $this->ComlecOrdenlectura->getCountConsumoCero($options1);
		$cantidad_consumo_menor35 = $this->ComlecOrdenlectura->getCountConsumoMenoresQue($options1);
		$cantidad_consumo_mayor1000 = $this->ComlecOrdenlectura->getCountConsumoMayoresQue($options1);
		$cantidad_consumo_mayor100porciento = $this->ComlecOrdenlectura->getCountConsumoMayor100PorcientoQueAnterior($options1);
		
		$this->set(compact('cantidad_ordenes','cantidad_observaciones','cantidad_observaciones_sin_lectura','cantidad_observaciones_con_lectura','cantidad_obs_99','cantidad_tiempoejecucion', 'cantidad_consumo_cero', 'cantidad_consumo_menor35','cantidad_consumo_mayor1000','cantidad_consumo_mayor100porciento'));
		
		// Periodo Anterior
		if(!isset($pfactura2)){
			$options2['pfactura'] = $listar_pfacrura[0][0]['pfactura'];
		}else{
			$options2['pfactura'] = $pfactura2;
		}
		
		$cantidad_ordenes2 = $this->ComlecOrdenlectura->getCount($options2);
		$cantidad_observaciones2 = $this->ComlecOrdenlectura->getCountObs1($options2);
		$cantidad_observaciones_sin_lectura2 = $this->ComlecOrdenlectura->getCountObs1SinLectura($options2);
		$cantidad_observaciones_con_lectura2 = $this->ComlecOrdenlectura->getCountObs1ConLectura($options2);
		$cantidad_obs_992 = $this->ComlecOrdenlectura->getCountObs1IgualA($options2,99);
		$cantidad_tiempoejecucion2 = $this->ComlecOrdenlectura->getCountTiempoPromedioEjecucion($options2);
		$cantidad_consumo_cero2 = $this->ComlecOrdenlectura->getCountConsumoCero($options2);
		$cantidad_consumo_menor352 = $this->ComlecOrdenlectura->getCountConsumoMenoresQue($options2);
		$cantidad_consumo_mayor10002 = $this->ComlecOrdenlectura->getCountConsumoMayoresQue($options2);
		$cantidad_consumo_mayor100porciento2 = $this->ComlecOrdenlectura->getCountConsumoMayor100PorcientoQueAnterior($options2);
		
		$this->set(compact('cantidad_ordenes2','cantidad_observaciones2','cantidad_observaciones_sin_lectura2','cantidad_observaciones_con_lectura2','cantidad_obs_992','cantidad_tiempoejecucion2', 'cantidad_consumo_cero2', 'cantidad_consumo_menor352','cantidad_consumo_mayor10002','cantidad_consumo_mayor100porciento2'));
	}

	public function comparar_periodo($pfactura=null,$pfactura2=null) {
		$this->layout = 'dashboard';
		
		$id_usuario = $this->obj_logged_user['Usuario']['id'];
		$this->set(compact('id_usuario'));

		$this->loadModel('ComlecOrdenlectura');

		$listar_pfacrura = $this->ComlecOrdenlectura->listarPFactura();
		$listar_unidadneg = $this->ComlecOrdenlectura->listarUnidadneg();
		
		// DATA 1 
		$options1 = array();
		if(isset($pfactura)){
			$options1['pfactura'] = $pfactura;
		}else{
			$options1 = null;
		}
		
		$leyenda_ciclos = $this->ComlecOrdenlectura->getLeyendaGraficoDashboard($pfactura);
		$cantidad_observaciones = $this->ComlecOrdenlectura->getCountObs1($options1);
		$cantidad_observaciones_sin_lectura = $this->ComlecOrdenlectura->getCountObs1SinLectura($options1);
		$cantidad_observaciones_con_lectura = $this->ComlecOrdenlectura->getCountObs1ConLectura($options1);
		
		$cantidad_avanzados = $this->ComlecOrdenlectura->getCountAvanzados($options1);
		$cantidad_consistentes = $this->ComlecOrdenlectura->getCountConsistentes($options1);
		$cantidad_inconsistentes_evaluadas = $this->ComlecOrdenlectura->getCountInconsistentesEvaluadas($options1);
		$cantidad_tiempoejecucion = $this->ComlecOrdenlectura->getCountTiempoPromedioEjecucion($options1);
		$cantidad_inconsistentes_actuales = $this->ComlecOrdenlectura->getCountInconsistentesActuales($options1);
		$cantidad_consumo_cero = $this->ComlecOrdenlectura->getCountConsumoCero($options1);
		$cantidad_consumo_menor35 = $this->ComlecOrdenlectura->getCountConsumoMenoresQue($options1);
		
		$arr_ciclos = $this->ComlecOrdenlectura->listarCiclo($options1);
		$arr_codigos_observacion = $this->ComlecOrdenlectura->getCountByCodigos($options1);
		$cantidad_ordenes = $this->ComlecOrdenlectura->getCount($options1);
		
		$this->set(compact('cantidad_observaciones','cantidad_observaciones_sin_lectura','cantidad_observaciones_con_lectura','cantidad_ordenes','arr_ciclos','arr_codigos_observacion','cantidad_avanzados','cantidad_consistentes','cantidad_inconsistentes_actuales','cantidad_tiempoejecucion', 'cantidad_inconsistentes_evaluadas', 'cantidad_consumo_cero', 'cantidad_consumo_menor35', 'unidadneg','listar_unidadneg','leyenda_ciclos'));
		
		// DATA 2

		$options2 = array();
		if(!isset($pfactura2)){
			if(count($listar_pfacrura)>0){
				$options2['pfactura'] = $listar_pfacrura[0][0]['pfactura'];
			}else{
				$options2['pfactura'] = null;
			}
		}else{
			$options2['pfactura'] = $pfactura2;
		}
		$pfactura2 = $options2['pfactura']; 
		
		$leyenda_ciclos2 = $this->ComlecOrdenlectura->getLeyendaGraficoDashboard($options2['pfactura']);
		$cantidad_observaciones2 = $this->ComlecOrdenlectura->getCountObs1($options2);
		$cantidad_observaciones_sin_lectura2 = $this->ComlecOrdenlectura->getCountObs1SinLectura($options2);
		$cantidad_observaciones_con_lectura2 = $this->ComlecOrdenlectura->getCountObs1ConLectura($options2);
		
		$cantidad_avanzados2 = $this->ComlecOrdenlectura->getCountAvanzados($options2);
		$cantidad_consistentes2 = $this->ComlecOrdenlectura->getCountConsistentes($options2);
		$cantidad_inconsistentes_evaluadas2 = $this->ComlecOrdenlectura->getCountInconsistentesEvaluadas($options2);
		$cantidad_tiempoejecucion2 = $this->ComlecOrdenlectura->getCountTiempoPromedioEjecucion($options2);
		$cantidad_inconsistentes_actuales2 = $this->ComlecOrdenlectura->getCountInconsistentesActuales($options2);
		$cantidad_consumo_cero2 = $this->ComlecOrdenlectura->getCountConsumoCero($options2);
		$cantidad_consumo_menor352 = $this->ComlecOrdenlectura->getCountConsumoMenoresQue($options2);
		
		$arr_ciclos2 = $this->ComlecOrdenlectura->listarCiclo($options2);
		$arr_codigos_observacion2 = $this->ComlecOrdenlectura->getCountByCodigos($options2);
		$cantidad_ordenes2 = $this->ComlecOrdenlectura->getCount($options2);
		
		
		$this->set(compact('cantidad_observaciones2','cantidad_observaciones_sin_lectura2','cantidad_observaciones_con_lectura2','cantidad_ordenes2','arr_ciclos2','arr_codigos_observacion2','cantidad_avanzados2','cantidad_consistentes2','cantidad_inconsistentes_actuales2','cantidad_tiempoejecucion2', 'cantidad_inconsistentes_evaluadas2', 'cantidad_consumo_cero2', 'cantidad_consumo_menor352', 'leyenda_ciclos2' ));
		
		$this->set(compact('listar_pfacrura','pfactura','pfactura2'));
	}
	
	public function reporte_observaciones() {
		$this->layout = 'home_tree';
		ini_set('memory_limit', '-1');
		ini_set('max_execution_time', 300000);
		set_time_limit(0);
		
		$id_usuario = $this->obj_logged_user['Usuario']['id'];
		$this->set(compact('id_usuario'));
		
		if(isset($this->request->data['unidadneg'])){
			$unidadneg = $this->request->data['unidadneg']; 
		}else{
			$unidadneg = 0;
		}
		
		if(isset($this->request->data['pfactura'])){
			$pfactura = $this->request->data['pfactura'];
		}else{
			$pfactura = 0;
		}
		
		$this->loadModel('ComlecOrdenlectura');
		$listar_unidadneg = $this->ComlecOrdenlectura->listarUnidadneg();
		$listar_pfacrura = $this->ComlecOrdenlectura->listarPFactura();
		$arr_codigos_observacion = $this->ComlecOrdenlectura->getCodigosObservaciones($unidadneg,$pfactura);
		$arr_count_codigos_observacion = $this->ComlecOrdenlectura->getCountObservacionesPorCicloYSector($unidadneg,$pfactura);
		$arr_count_codigos_observacion_sector_ruta = $this->ComlecOrdenlectura->getCountObservacionesPorCicloYSectorYRuta($unidadneg,$pfactura);
		
		$this->set(compact('listar_unidadneg','unidadneg','listar_pfacrura','pfactura','arr_codigos_observacion','arr_count_codigos_observacion','arr_count_codigos_observacion_sector_ruta'));
	}
	
	public function monitor_progreso($ajax=null,$unidad_neg=null,$idciclo=null) {
		$this->layout = 'ajax';
		
		$id_usuario = $this->obj_logged_user['Usuario']['id'];
		$this->set(compact('id_usuario'));
		
		if(isset($unidad_neg) || isset($idciclo)){
			$options = array('unidad_neg'=>$unidad_neg,'idciclo'=>$idciclo);
		}else{
			$options = null;
		}
		$this->set(compact('unidad_neg','idciclo'));
	
		$this->loadModel('ComlecOrdenlectura');
		
		//Dia Actual
		$cantidad_ordenes_dia_actual = $this->ComlecOrdenlectura->getCountByFecha($options);
		$cantidad_avanzados_dia_actual = $this->ComlecOrdenlectura->getCountAvanzadosByFecha($options);
		$cantidad_inconsistentes_dia_actual = $this->ComlecOrdenlectura->getCountInconsistentesActualesByFecha($options);
		
		//Periodo Actual
		$cantidad_ordenes = $this->ComlecOrdenlectura->getCount($options);
		$cantidad_avanzados = $this->ComlecOrdenlectura->getCountAvanzados($options);
		$cantidad_inconsistentes = $this->ComlecOrdenlectura->getCountInconsistentesActuales($options);
		
		if(isset($ajax) && $ajax=='true'){
			echo json_encode(array('Total_dia_actual'=>$cantidad_ordenes_dia_actual,'Progreso_dia_actual'=>$cantidad_avanzados_dia_actual,'Inconsistentes_dia_actual'=>$cantidad_inconsistentes_dia_actual,
					'Total'=>$cantidad_ordenes,'Progreso'=>$cantidad_avanzados,'Inconsistentes'=>$cantidad_inconsistentes));
			exit();
		}else{
			$this->set(compact('cantidad_ordenes_dia_actual','cantidad_avanzados_dia_actual','cantidad_inconsistentes_dia_actual',
					'cantidad_ordenes','cantidad_avanzados','cantidad_inconsistentes'));
		}
		
	}
	
	public function monitor_progreso_semi_pie($ajax=null,$unidad_neg=null,$idciclo=null) {
		$this->layout = 'ajax';
		
		$id_usuario = $this->obj_logged_user['Usuario']['id'];
		$this->set(compact('id_usuario'));
	
		if(isset($unidad_neg) || isset($idciclo)){
			$options = array('unidad_neg'=>$unidad_neg,'idciclo'=>$idciclo);
		}else{
			$options = null;
		}
		$this->set(compact('unidad_neg','idciclo'));
	
		$this->loadModel('ComlecOrdenlectura');
	
		//Dia Actual
		$cantidad_ordenes_dia_actual = $this->ComlecOrdenlectura->getCountByFecha($options);
		$cantidad_avanzados_dia_actual = $this->ComlecOrdenlectura->getCountAvanzadosByFecha($options);
		$cantidad_inconsistentes_dia_actual = $this->ComlecOrdenlectura->getCountInconsistentesActualesByFecha($options);
	
		//Periodo Actual
		$cantidad_ordenes = $this->ComlecOrdenlectura->getCount($options);
		$cantidad_avanzados = $this->ComlecOrdenlectura->getCountAvanzados($options);
		$cantidad_inconsistentes = $this->ComlecOrdenlectura->getCountInconsistentesActuales($options);
		
		//calculos
		if($cantidad_ordenes_dia_actual>0){
			$porcentaje_progreso_dia_actual = ($cantidad_avanzados_dia_actual*100)/$cantidad_ordenes_dia_actual;
		}else{
			$porcentaje_progreso_dia_actual = 0;
		}
		$pendientes_dia_actual = ($cantidad_ordenes_dia_actual-$cantidad_avanzados_dia_actual);
		if($cantidad_ordenes_dia_actual>0){
			$porcentaje_pendiente_dia_actual = ($pendientes_dia_actual*100)/$cantidad_ordenes_dia_actual;
		}else{
			$porcentaje_pendiente_dia_actual = 0;
		}
			
		if($cantidad_ordenes_dia_actual>0){
			$porcentaje_inconsistencia_dia_actual = ($cantidad_inconsistentes_dia_actual*100)/$cantidad_ordenes_dia_actual;
		}else{
			$porcentaje_inconsistencia_dia_actual = 0;
		}
		$consistencia_dia_actual = ($cantidad_avanzados_dia_actual-$cantidad_inconsistentes_dia_actual);
		if($cantidad_avanzados_dia_actual>0){
			$porcentaje_consistencia_dia_actual = ($consistencia_dia_actual*100)/$cantidad_avanzados_dia_actual;
		}else{
			$porcentaje_consistencia_dia_actual = 0;
		}

		if($cantidad_ordenes>0){
			$porcentaje_progreso = ($cantidad_avanzados*100)/$cantidad_ordenes;
		}else{
			$porcentaje_progreso = 0;
		}
		$pendientes = ($cantidad_ordenes-$cantidad_avanzados);
		if($cantidad_ordenes>0){
			$porcentaje_pendiente = ($pendientes*100)/$cantidad_ordenes;
		}else{
			$porcentaje_pendiente = 0;
		}
			
		if($cantidad_ordenes>0){
			$porcentaje_inconsistencia = ($cantidad_inconsistentes*100)/$cantidad_ordenes;
		}else{
			$porcentaje_inconsistencia = 0;
		}
		$consistencia = ($cantidad_avanzados-$cantidad_inconsistentes);
		if($cantidad_avanzados>0){
			$porcentaje_consistencia = ($consistencia*100)/$cantidad_avanzados;
		}else{
			$porcentaje_consistencia = 0;
		}
	
		if(isset($ajax) && $ajax=='true'){
			
			echo json_encode(
					array('cantidad_ordenes_dia_actual'=>(int) $cantidad_ordenes_dia_actual,
							'cantidad_avanzados_dia_actual'=>(int) $cantidad_avanzados_dia_actual,
							'cantidad_inconsistentes_dia_actual'=>(int) $cantidad_inconsistentes_dia_actual,
							'cantidad_ordenes'=>(int) $cantidad_ordenes,
							'cantidad_avanzados'=>(int) $cantidad_avanzados,
							'cantidad_inconsistentes'=>(int) $cantidad_inconsistentes,
							
							'porcentaje_progreso_dia_actual'=>$porcentaje_progreso_dia_actual,
							'pendientes_dia_actual'=>$pendientes_dia_actual,
							'porcentaje_pendiente_dia_actual'=>$porcentaje_pendiente_dia_actual,
							'porcentaje_inconsistencia_dia_actual'=>$porcentaje_inconsistencia_dia_actual,
							'consistencia_dia_actual'=>$consistencia_dia_actual,
							'porcentaje_consistencia_dia_actual'=>$porcentaje_consistencia_dia_actual,
							
							'porcentaje_progreso'=>$porcentaje_progreso,
							'pendientes'=>$pendientes,
							'porcentaje_pendiente'=>$porcentaje_pendiente,
							'porcentaje_inconsistencia'=>$porcentaje_inconsistencia,
							'consistencia'=>$consistencia,
							'porcentaje_consistencia'=>$porcentaje_consistencia));
			exit();
		}else{
			$this->set(compact('porcentaje_progreso_dia_actual','pendientes_dia_actual','porcentaje_pendiente_dia_actual'
			,'porcentaje_inconsistencia_dia_actual','consistencia_dia_actual','porcentaje_consistencia_dia_actual'
			,'porcentaje_progreso','pendientes','porcentaje_pendiente'
			,'porcentaje_inconsistencia','consistencia','porcentaje_consistencia'));
			$this->set(compact('cantidad_ordenes_dia_actual','cantidad_avanzados_dia_actual','cantidad_inconsistentes_dia_actual',
					'cantidad_ordenes','cantidad_avanzados','cantidad_inconsistentes'));
		}
	
	}
	
	public function monitor_progreso_epie($unidad_neg=null,$idciclo=null) {
		$this->layout = 'ajax';
	
		$id_usuario = $this->obj_logged_user['Usuario']['id'];
		$this->set(compact('id_usuario'));
	
		if(isset($unidad_neg) || isset($idciclo)){
			$options = array('unidad_neg'=>$unidad_neg,'idciclo'=>$idciclo);
		}else{
			$options = null;
		}
		$this->set(compact('unidad_neg','idciclo'));
	
		$this->loadModel('ComlecOrdenlectura');
	
		//Dia Actual
		$cantidad_ordenes_dia_actual = $this->ComlecOrdenlectura->getCountByFecha($options);
		$cantidad_avanzados_dia_actual = $this->ComlecOrdenlectura->getCountAvanzadosByFecha($options);
		$cantidad_inconsistentes_dia_actual = $this->ComlecOrdenlectura->getCountInconsistentesActualesByFecha($options);
		$cantidad_distriluz_dia_actual = $this->ComlecOrdenlectura->getCountEnviadasDistriluzByFecha($options);
		
	
		//Periodo Actual
		$cantidad_ordenes = $this->ComlecOrdenlectura->getCount($options);
		$cantidad_avanzados = $this->ComlecOrdenlectura->getCountAvanzados($options);
		$cantidad_inconsistentes = $this->ComlecOrdenlectura->getCountInconsistentesActuales($options);
		$cantidad_distriluz = $this->ComlecOrdenlectura->getCountEnviadasDistriluz($options);
	
		//calculos
		if($cantidad_ordenes_dia_actual>0){
			$porcentaje_progreso_dia_actual = floor(($cantidad_avanzados_dia_actual*100)/$cantidad_ordenes_dia_actual);
		}else{
			$porcentaje_progreso_dia_actual = 0;
		}
		$pendientes_dia_actual = ($cantidad_ordenes_dia_actual-$cantidad_avanzados_dia_actual);
		if($cantidad_ordenes_dia_actual>0){
			$porcentaje_pendiente_dia_actual = round(($pendientes_dia_actual*100)/$cantidad_ordenes_dia_actual);
		}else{
			$porcentaje_pendiente_dia_actual = 0;
		}
			
		if($cantidad_avanzados_dia_actual>0){
			$porcentaje_inconsistencia_dia_actual = round(($cantidad_inconsistentes_dia_actual*100)/$cantidad_avanzados_dia_actual);
		}else{
			$porcentaje_inconsistencia_dia_actual = 0;
		}
		$consistencia_dia_actual = ($cantidad_avanzados_dia_actual-$cantidad_inconsistentes_dia_actual);
		if($cantidad_avanzados_dia_actual>0){
			$porcentaje_consistencia_dia_actual = round(($consistencia_dia_actual*100)/$cantidad_avanzados_dia_actual);
		}else{
			$porcentaje_consistencia_dia_actual = 0;
		}
		if($cantidad_avanzados_dia_actual>0){
			$porcentaje_distriluz_dia_actual = floor(($cantidad_distriluz_dia_actual*100)/$cantidad_avanzados_dia_actual);
		}else{
			$porcentaje_distriluz_dia_actual = 0;
		}
	
		if($cantidad_ordenes>0){
			$porcentaje_progreso = floor(($cantidad_avanzados*100)/$cantidad_ordenes);
		}else{
			$porcentaje_progreso = 0;
		}
		$pendientes = ($cantidad_ordenes-$cantidad_avanzados);
		if($cantidad_ordenes>0){
			$porcentaje_pendiente = floor(($pendientes*100)/$cantidad_ordenes);
		}else{
			$porcentaje_pendiente = 0;
		}
			
		if($cantidad_avanzados>0){
			$porcentaje_inconsistencia = round(($cantidad_inconsistentes*100)/$cantidad_avanzados);
		}else{
			$porcentaje_inconsistencia = 0;
		}
		$consistencia = ($cantidad_avanzados-$cantidad_inconsistentes);
		if($cantidad_avanzados>0){
			$porcentaje_consistencia = round(($consistencia*100)/$cantidad_avanzados);
		}else{
			$porcentaje_consistencia = 0;
		}
		
		if($cantidad_avanzados>0){
			$porcentaje_distriluz = floor(($cantidad_distriluz*100)/$cantidad_avanzados);
		}else{
			$porcentaje_distriluz = 0;
		}
	
		
		$this->set(compact('porcentaje_progreso_dia_actual','pendientes_dia_actual','porcentaje_pendiente_dia_actual'
				,'porcentaje_inconsistencia_dia_actual','consistencia_dia_actual','porcentaje_consistencia_dia_actual'
				,'porcentaje_distriluz_dia_actual','porcentaje_progreso','pendientes','porcentaje_pendiente'
				,'porcentaje_inconsistencia','consistencia','porcentaje_consistencia','porcentaje_distriluz'));
		$this->set(compact('cantidad_ordenes_dia_actual','cantidad_avanzados_dia_actual','cantidad_inconsistentes_dia_actual','cantidad_distriluz_dia_actual',
				'cantidad_ordenes','cantidad_avanzados','cantidad_inconsistentes','cantidad_distriluz'));
	
	}
	
	public function grafico_lecturistas_por_semana($unidad_neg=null,$idciclo=null) {
		$this->layout = 'ajax';
		
		$id_usuario = $this->obj_logged_user['Usuario']['id'];
		$this->set(compact('id_usuario'));
		
		if(isset($unidad_neg) || isset($idciclo)){
			$options = array('unidad_neg'=>$unidad_neg,'idciclo'=>$idciclo);
		}else{
			$options = null;
		}
	
		$this->loadModel('ComlecOrdenlectura');
		
		$dia_semmana = date('w');
		$fecha_ultima_semana=date('Y-m-d 23:59:59',strtotime('-1 weeks', strtotime(date("Y-m-d"))));
	
		$arr_ordenes = $this->ComlecOrdenlectura->getResumenOrdenesFinalizadasPorLecturistaPorSemana($options,$fecha_ultima_semana);
		
		$this->set(compact('arr_ordenes','dia_semmana'));
		
	}
	
	public function grafico_rendimiento_consumo($unidad_neg=null,$idciclo=null) {
		$this->layout = 'ajax';
		
		$id_usuario = $this->obj_logged_user['Usuario']['id'];
		$this->set(compact('id_usuario'));
	
		if(isset($unidad_neg) || isset($idciclo)){
			$options = array('unidad_neg'=>$unidad_neg,'idciclo'=>$idciclo);
		}else{
			$options = null;
		}
	
		$this->loadModel('ComlecOrdenlectura');
	
		$arr_ordenes = $this->ComlecOrdenlectura->getRendimientoConsumo($options);
	
		$this->set(compact('arr_ordenes'));
	
	}
	
	public function grafico_observaciones_por_ciclo($unidad_neg=null,$idciclo=null) {
		$this->layout = 'ajax';
		
		$id_usuario = $this->obj_logged_user['Usuario']['id'];
		$this->set(compact('id_usuario'));
	
		if(isset($unidad_neg) || isset($idciclo)){
			$options = array('unidad_neg'=>$unidad_neg,'idciclo'=>$idciclo);
		}else{
			$options = null;
		}
	
		$this->loadModel('ComlecOrdenlectura');
		
		$arr_ciclos = $this->ComlecOrdenlectura->listarCiclo($options);
		$arr_codigos_observacion = $this->ComlecOrdenlectura->getCountByCodigos($options);
		$cantidad_ordenes = $this->ComlecOrdenlectura->getCount($options);
		
		$this->set(compact('arr_ciclos','arr_codigos_observacion','cantidad_ordenes'));
	
	}
	
	public function resumen_por_ciclos($unidad_neg=null,$idciclo=null) {
		
		$id_usuario = $this->obj_logged_user['Usuario']['id'];
		$this->set(compact('id_usuario'));
		
		if(isset($this->request->query['ajax']) && $this->request->query['ajax']=='false'){
			$this->layout = 'home_tree';
		}else{
			$this->layout = 'default_no_header';
		}
	
		$this->loadModel('ComlecOrdenlectura');
	
		if(isset($unidad_neg) || isset($idciclo)){
			$options1 = array('unidad_neg'=>$unidad_neg,'idciclo'=>$idciclo);
		}else{
			$options1 = null;
		}
	
		$arr_ciclos = $this->ComlecOrdenlectura->getResumenPorCiclos($options1);
		$arr_ciclos_actuales = $this->ComlecOrdenlectura->getCiclosActuales($options1);
		
		$this->set(compact('arr_ciclos','arr_ciclos_actuales'));
	}
	
	public function grafico_lecturistas_por_periodo($unidad_neg=null,$idciclo=null) {
		$this->layout = 'ajax';		
		
		$id_usuario = $this->obj_logged_user['Usuario']['id'];
		$this->set(compact('id_usuario'));
	
		if(isset($unidad_neg) || isset($idciclo)){
			$options = array('unidad_neg'=>$unidad_neg,'idciclo'=>$idciclo);
		}else{
			$options = null;
		}
	
		$this->loadModel('ComlecOrdenlectura');
			
		$arr_ordenes = $this->ComlecOrdenlectura->getResumenOrdenesFinalizadasPorLecturistaPorPeriodo($options);
		
		$this->set(compact('arr_ordenes'));
	}
	
	public function grafico_digitadores_por_periodo($unidad_neg=null,$idciclo=null) {
		$this->layout = 'ajax';
		
		$id_usuario = $this->obj_logged_user['Usuario']['id'];
		$this->set(compact('id_usuario'));
	
		if(isset($unidad_neg) || isset($idciclo)){
			$options = array('unidad_neg'=>$unidad_neg,'idciclo'=>$idciclo);
		}else{
			$options = null;
		}
	
		$this->loadModel('ComlecOrdenlectura');
			
		$arr_ordenes = $this->ComlecOrdenlectura->getResumenOrdenesFinalizadasPorDigitadorPorPeriodo($options);
	
		$this->set(compact('arr_ordenes'));
	}
	
	public function grafico_consumo_por_dia($unidad_neg=null,$idciclo=null) {
		$this->layout = 'ajax';
		
		$id_usuario = $this->obj_logged_user['Usuario']['id'];
		$this->set(compact('id_usuario'));
	
		if(isset($unidad_neg) || isset($idciclo)){
			$options = array('unidad_neg'=>$unidad_neg,'idciclo'=>$idciclo);
		}else{
			$options = null;
		}
	
		$this->loadModel('ComlecOrdenlectura');
			
		$arr = $this->ComlecOrdenlectura->getConsumoPorDia($options);
	
		$this->set(compact('arr'));
	}
	
	public function grafico_suministros_por_metodo($unidad_neg=null,$idciclo=null) {
		$this->layout = 'ajax';
		
		$id_usuario = $this->obj_logged_user['Usuario']['id'];
		$this->set(compact('id_usuario'));
	
		if(isset($unidad_neg) || isset($idciclo)){
			$options = array('unidad_neg'=>$unidad_neg,'idciclo'=>$idciclo);
		}else{
			$options = null;
		}
	
		$this->loadModel('ComlecOrdenlectura');
			
		$arr = $this->ComlecOrdenlectura->getCountSuministrosPorMetodo($options);
	
		$this->set(compact('arr'));
	}
	
	public function resumen_consumo_por_dia_y_ubicacion($unidad_neg=null,$idciclo=null) {
		$this->layout = 'home_tree';
		
		$id_usuario = $this->obj_logged_user['Usuario']['id'];
		$this->set(compact('id_usuario'));
	
		$this->loadModel('ComlecOrdenlectura');
	
		if(isset($unidad_neg) || isset($idciclo)){
			$options1 = array('unidad_neg'=>$unidad_neg,'idciclo'=>$idciclo);
		}else{
			$options1 = null;
		}
	
		$arr_data = $this->ComlecOrdenlectura->getConsumoPorDiaYUbicacion($options1);
		$arr_ciclos_actuales = $this->ComlecOrdenlectura->getCiclosActuales($options1);
	
		$this->set(compact('arr_data','arr_ciclos_actuales'));
	}
	
	public function index_new($unidad_neg=null,$idciclo=null) {
		$this->layout = 'dashboard';
		$helpers[] = 'Tiempo';
		ini_set('memory_limit', '-1');
		ini_set('max_execution_time', 300000);
		set_time_limit(0);
		
		$id_usuario = $this->obj_logged_user['Usuario']['id'];
		$this->set(compact('id_usuario'));
		
		$this->loadModel('ComlecOrdenlectura');
	
		if(isset($unidad_neg) || isset($idciclo)){
			$options = array('unidad_neg'=>$unidad_neg,'idciclo'=>$idciclo);
		}else{
			$options = null;
		}
		$this->set(compact('unidad_neg','idciclo'));
	
		$listar_unidadneg = $this->ComlecOrdenlectura->listarUnidadneg();
		$listar_ciclo = $this->ComlecOrdenlectura->listarCiclo();
		$arr_ciclos_actuales = $this->ComlecOrdenlectura->getCiclosActuales();
	
		$this->set(compact('listar_unidadneg','listar_ciclo','arr_ciclos_actuales'));
	
		$listar_pfacrura = $this->ComlecOrdenlectura->listarPFactura();
		
		$this->loadModel('KpiLectura');
		
		$fecha_last_update = $this->KpiLectura->getDateLastUpdate(null);
		$this->set(compact('fecha_last_update'));
	
		//Dia Actual
		$arr_kpi_dia_actual = $this->KpiLectura->getKpiByFecha($options);
		
		$cantidad_ordenes_dia_actual = $arr_kpi_dia_actual[0][0]['count_suministros'];
		$cantidad_avanzados_dia_actual = $arr_kpi_dia_actual[0][0]['count_finalizados'];
		$cantidad_consistentes_dia_actual = $arr_kpi_dia_actual[0][0]['count_consistentes'];
		$cantidad_inconsistentes_dia_actual = $arr_kpi_dia_actual[0][0]['count_inconsistentes_actuales'];
		$cantidad_inconsistentes_evaluadas_dia_actual = $arr_kpi_dia_actual[0][0]['count_inconsistentes_evaluadas'];
		$cantidad_asignados_dia_actual = $arr_kpi_dia_actual[0][0]['count_asignados'];
		$cantidad_descargados_dia_actual = $arr_kpi_dia_actual[0][0]['count_descargados'];
		$cantidad_enviados_distriluz_dia_actual = $arr_kpi_dia_actual[0][0]['count_enviados_distriluz'];
		
		$cantidad_cartas_generadas_dia_actual = $this->ComlecOrdenlectura->getCountCartasReclamosByFecha($options);
	
		$this->set(compact('cantidad_ordenes_dia_actual','cantidad_avanzados_dia_actual','cantidad_consistentes_dia_actual',
				'cantidad_inconsistentes_dia_actual','cantidad_inconsistentes_evaluadas_dia_actual','cantidad_asignados_dia_actual',
				'cantidad_descargados_dia_actual','cantidad_enviados_distriluz_dia_actual','cantidad_cartas_generadas_dia_actual'));
		
		$cantidad_observaciones_dia_actual = $arr_kpi_dia_actual[0][0]['count_observaciones'];
		$cantidad_observaciones_sin_lectura_dia_actual = $arr_kpi_dia_actual[0][0]['count_observaciones_sin_lectura'];
		$cantidad_observaciones_con_lectura_dia_actual =$arr_kpi_dia_actual[0][0]['count_observaciones_con_lectura'];
		$cantidad_obs_99_dia_actual = $arr_kpi_dia_actual[0][0]['count_observaciones_99'];
		$cantidad_tiempoejecucion_dia_actual = $arr_kpi_dia_actual[0][0]['tiempo_promedio_ejecucion'];
		$cantidad_consumo_cero_dia_actual = $arr_kpi_dia_actual[0][0]['count_consumo_cero'];
		$cantidad_consumo_menor35_dia_actual = $arr_kpi_dia_actual[0][0]['count_consumo_menor_35'];
		$cantidad_consumo_mayor1000_dia_actual = $arr_kpi_dia_actual[0][0]['count_consumo_mayor_1000'];
		$cantidad_consumo_mayor100porciento_dia_actual = $arr_kpi_dia_actual[0][0]['count_consumo_mayor_100_porciento'];
		
		$this->set(compact('cantidad_ordenes_dia_actual','cantidad_observaciones_dia_actual','cantidad_observaciones_sin_lectura_dia_actual','cantidad_observaciones_con_lectura_dia_actual','cantidad_obs_99_dia_actual','cantidad_tiempoejecucion_dia_actual', 'cantidad_consumo_cero_dia_actual', 'cantidad_consumo_menor35_dia_actual','cantidad_consumo_mayor1000_dia_actual','cantidad_consumo_mayor100porciento_dia_actual'));
	
	
		//Periodo Actual
		$arr_kpi_periodo_actual = $this->KpiLectura->getKpi($options);
		
		$cantidad_ordenes_periodo_actual = $arr_kpi_periodo_actual[0][0]['count_suministros'];
		$cantidad_avanzados_periodo_actual = $arr_kpi_periodo_actual[0][0]['count_finalizados'];
		$cantidad_consistentes_periodo_actual = $arr_kpi_periodo_actual[0][0]['count_consistentes'];
		$cantidad_inconsistentes_periodo_actual = $arr_kpi_periodo_actual[0][0]['count_inconsistentes_actuales'];
		$cantidad_inconsistentes_evaluadas_periodo_actual = $arr_kpi_periodo_actual[0][0]['count_inconsistentes_evaluadas'];
		$cantidad_asignados_periodo_actual = $arr_kpi_periodo_actual[0][0]['count_asignados'];
		$cantidad_descargados_periodo_actual = $arr_kpi_periodo_actual[0][0]['count_descargados'];
		$cantidad_enviados_distriluz_periodo_actual = $arr_kpi_periodo_actual[0][0]['count_enviados_distriluz'];
		
		$cantidad_cartas_generadas_periodo_actual = $this->ComlecOrdenlectura->getCountCartasReclamos($options);
	
		$this->set(compact('cantidad_ordenes_periodo_actual','cantidad_avanzados_periodo_actual','cantidad_consistentes_periodo_actual',
				'cantidad_inconsistentes_periodo_actual','cantidad_inconsistentes_evaluadas_periodo_actual','cantidad_asignados_periodo_actual',
				'cantidad_descargados_periodo_actual','cantidad_enviados_distriluz_periodo_actual','cantidad_cartas_generadas_periodo_actual'));
		
		$cantidad_observaciones_periodo_actual = $arr_kpi_periodo_actual[0][0]['count_observaciones'];
		$cantidad_observaciones_sin_lectura_periodo_actual = $arr_kpi_periodo_actual[0][0]['count_observaciones_sin_lectura'];
		$cantidad_observaciones_con_lectura_periodo_actual =$arr_kpi_periodo_actual[0][0]['count_observaciones_con_lectura'];
		$cantidad_obs_99_periodo_actual = $arr_kpi_periodo_actual[0][0]['count_observaciones_99'];
		$cantidad_tiempoejecucion_periodo_actual = $arr_kpi_periodo_actual[0][0]['tiempo_promedio_ejecucion'];
		$cantidad_consumo_cero_periodo_actual = $arr_kpi_periodo_actual[0][0]['count_consumo_cero'];
		$cantidad_consumo_menor35_periodo_actual = $arr_kpi_periodo_actual[0][0]['count_consumo_menor_35'];
		$cantidad_consumo_mayor1000_periodo_actual = $arr_kpi_periodo_actual[0][0]['count_consumo_mayor_1000'];
		$cantidad_consumo_mayor100porciento_periodo_actual = $arr_kpi_periodo_actual[0][0]['count_consumo_mayor_100_porciento'];
		
		$this->set(compact('cantidad_ordenes_periodo_actual','cantidad_observaciones_periodo_actual','cantidad_observaciones_sin_lectura_periodo_actual','cantidad_observaciones_con_lectura_periodo_actual','cantidad_obs_99_periodo_actual','cantidad_tiempoejecucion_periodo_actual', 'cantidad_consumo_cero_periodo_actual', 'cantidad_consumo_menor35_periodo_actual','cantidad_consumo_mayor1000_periodo_actual','cantidad_consumo_mayor100porciento_periodo_actual'));
	
		//Periodo Anterior
		if(isset($unidad_neg) || isset($idciclo)){
			$options_periodo_anterior = array('unidad_neg'=>$unidad_neg,'idciclo'=>$idciclo);
		}else{
			$options_periodo_anterior = array();
		}
		if(count($listar_pfacrura)>0){
			$options_periodo_anterior['pfactura'] = $listar_pfacrura[0][0]['pfactura'];
		}else{
			$options_periodo_anterior['pfactura'] = 'NULL';
		}
		
		$arr_kpi_periodo_anterior = $this->KpiLectura->getKpi($options_periodo_anterior);
	
		$cantidad_ordenes_periodo_anterior = $arr_kpi_periodo_anterior[0][0]['count_suministros'];
		$cantidad_avanzados_periodo_anterior = $arr_kpi_periodo_anterior[0][0]['count_finalizados'];
		$cantidad_consistentes_periodo_anterior = $arr_kpi_periodo_anterior[0][0]['count_consistentes'];
		$cantidad_inconsistentes_periodo_anterior = $arr_kpi_periodo_anterior[0][0]['count_inconsistentes_actuales'];
		$cantidad_inconsistentes_evaluadas_periodo_anterior = $arr_kpi_periodo_anterior[0][0]['count_inconsistentes_evaluadas'];
		$cantidad_asignados_periodo_anterior = $arr_kpi_periodo_anterior[0][0]['count_asignados'];
		$cantidad_descargados_periodo_anterior = $arr_kpi_periodo_anterior[0][0]['count_descargados'];
		$cantidad_enviados_distriluz_periodo_anterior = $arr_kpi_periodo_anterior[0][0]['count_enviados_distriluz'];
		
		$cantidad_cartas_generadas_periodo_anterior = $this->ComlecOrdenlectura->getCountCartasReclamos($options_periodo_anterior);
	
		$this->set(compact('cantidad_ordenes_periodo_anterior','cantidad_avanzados_periodo_anterior','cantidad_consistentes_periodo_anterior',
				'cantidad_inconsistentes_periodo_anterior','cantidad_inconsistentes_evaluadas_periodo_anterior','cantidad_asignados_periodo_anterior',
				'cantidad_descargados_periodo_anterior','cantidad_enviados_distriluz_periodo_anterior','cantidad_cartas_generadas_periodo_anterior'));
		
		$cantidad_observaciones_periodo_anterior = $arr_kpi_periodo_anterior[0][0]['count_observaciones'];
		$cantidad_observaciones_sin_lectura_periodo_anterior = $arr_kpi_periodo_anterior[0][0]['count_observaciones_sin_lectura'];
		$cantidad_observaciones_con_lectura_periodo_anterior =$arr_kpi_periodo_anterior[0][0]['count_observaciones_con_lectura'];
		$cantidad_obs_99_periodo_anterior = $arr_kpi_periodo_anterior[0][0]['count_observaciones_99'];
		$cantidad_tiempoejecucion_periodo_anterior = $arr_kpi_periodo_anterior[0][0]['tiempo_promedio_ejecucion'];
		$cantidad_consumo_cero_periodo_anterior = $arr_kpi_periodo_anterior[0][0]['count_consumo_cero'];
		$cantidad_consumo_menor35_periodo_anterior = $arr_kpi_periodo_anterior[0][0]['count_consumo_menor_35'];
		$cantidad_consumo_mayor1000_periodo_anterior = $arr_kpi_periodo_anterior[0][0]['count_consumo_mayor_1000'];
		$cantidad_consumo_mayor100porciento_periodo_anterior = $arr_kpi_periodo_anterior[0][0]['count_consumo_mayor_100_porciento'];
		
		$this->set(compact('cantidad_ordenes_periodo_anterior','cantidad_observaciones_periodo_anterior','cantidad_observaciones_sin_lectura_periodo_anterior','cantidad_observaciones_con_lectura_periodo_anterior','cantidad_obs_99_periodo_anterior','cantidad_tiempoejecucion_periodo_anterior', 'cantidad_consumo_cero_periodo_anterior', 'cantidad_consumo_menor35_periodo_anterior','cantidad_consumo_mayor1000_periodo_anterior','cantidad_consumo_mayor100porciento_periodo_anterior'));
	}
	
	public function monitor_progreso_semi_pie_v2($ajax=null,$unidad_neg=null,$idciclo=null) {
		$this->layout = 'ajax';
		
		$id_usuario = $this->obj_logged_user['Usuario']['id'];
		$this->set(compact('id_usuario'));
	
		if(isset($unidad_neg) || isset($idciclo)){
			$options = array('unidad_neg'=>$unidad_neg,'idciclo'=>$idciclo);
		}else{
			$options = null;
		}
		$this->set(compact('unidad_neg','idciclo'));
	
		$this->loadModel('KpiLectura');
	
		//Dia Actual
		$arr_kpi_dia_actual = $this->KpiLectura->getKpiByFecha($options);
		
		$cantidad_ordenes_dia_actual = $arr_kpi_dia_actual[0][0]['count_suministros'];
		$cantidad_avanzados_dia_actual = $arr_kpi_dia_actual[0][0]['count_finalizados'];
		$cantidad_inconsistentes_dia_actual = $arr_kpi_dia_actual[0][0]['count_inconsistentes_actuales'];
	
		//Periodo Actual
		$arr_kpi_periodo_actual = $this->KpiLectura->getKpi($options);
				
		$cantidad_ordenes = $arr_kpi_periodo_actual[0][0]['count_suministros'];
		$cantidad_avanzados = $arr_kpi_periodo_actual[0][0]['count_finalizados'];
		$cantidad_inconsistentes = $arr_kpi_periodo_actual[0][0]['count_inconsistentes_actuales'];
	
		//calculos
		if($cantidad_ordenes_dia_actual>0){
			$porcentaje_progreso_dia_actual = ($cantidad_avanzados_dia_actual*100)/$cantidad_ordenes_dia_actual;
		}else{
			$porcentaje_progreso_dia_actual = 0;
		}
		$pendientes_dia_actual = ($cantidad_ordenes_dia_actual-$cantidad_avanzados_dia_actual);
		if($cantidad_ordenes_dia_actual>0){
			$porcentaje_pendiente_dia_actual = ($pendientes_dia_actual*100)/$cantidad_ordenes_dia_actual;
		}else{
			$porcentaje_pendiente_dia_actual = 0;
		}
			
		if($cantidad_ordenes_dia_actual>0){
			$porcentaje_inconsistencia_dia_actual = ($cantidad_inconsistentes_dia_actual*100)/$cantidad_ordenes_dia_actual;
		}else{
			$porcentaje_inconsistencia_dia_actual = 0;
		}
		$consistencia_dia_actual = ($cantidad_avanzados_dia_actual-$cantidad_inconsistentes_dia_actual);
		if($cantidad_avanzados_dia_actual>0){
			$porcentaje_consistencia_dia_actual = ($consistencia_dia_actual*100)/$cantidad_avanzados_dia_actual;
		}else{
			$porcentaje_consistencia_dia_actual = 0;
		}
	
		if($cantidad_ordenes>0){
			$porcentaje_progreso = ($cantidad_avanzados*100)/$cantidad_ordenes;
		}else{
			$porcentaje_progreso = 0;
		}
		$pendientes = ($cantidad_ordenes-$cantidad_avanzados);
		if($cantidad_ordenes>0){
			$porcentaje_pendiente = ($pendientes*100)/$cantidad_ordenes;
		}else{
			$porcentaje_pendiente = 0;
		}
			
		if($cantidad_ordenes>0){
			$porcentaje_inconsistencia = ($cantidad_inconsistentes*100)/$cantidad_ordenes;
		}else{
			$porcentaje_inconsistencia = 0;
		}
		$consistencia = ($cantidad_avanzados-$cantidad_inconsistentes);
		if($cantidad_avanzados>0){
			$porcentaje_consistencia = ($consistencia*100)/$cantidad_avanzados;
		}else{
			$porcentaje_consistencia = 0;
		}
	
		if(isset($ajax) && $ajax=='true'){
				
			echo json_encode(
					array('cantidad_ordenes_dia_actual'=>(int) $cantidad_ordenes_dia_actual,
							'cantidad_avanzados_dia_actual'=>(int) $cantidad_avanzados_dia_actual,
							'cantidad_inconsistentes_dia_actual'=>(int) $cantidad_inconsistentes_dia_actual,
							'cantidad_ordenes'=>(int) $cantidad_ordenes,
							'cantidad_avanzados'=>(int) $cantidad_avanzados,
							'cantidad_inconsistentes'=>(int) $cantidad_inconsistentes,
								
							'porcentaje_progreso_dia_actual'=>$porcentaje_progreso_dia_actual,
							'pendientes_dia_actual'=>$pendientes_dia_actual,
							'porcentaje_pendiente_dia_actual'=>$porcentaje_pendiente_dia_actual,
							'porcentaje_inconsistencia_dia_actual'=>$porcentaje_inconsistencia_dia_actual,
							'consistencia_dia_actual'=>$consistencia_dia_actual,
							'porcentaje_consistencia_dia_actual'=>$porcentaje_consistencia_dia_actual,
								
							'porcentaje_progreso'=>$porcentaje_progreso,
							'pendientes'=>$pendientes,
							'porcentaje_pendiente'=>$porcentaje_pendiente,
							'porcentaje_inconsistencia'=>$porcentaje_inconsistencia,
							'consistencia'=>$consistencia,
							'porcentaje_consistencia'=>$porcentaje_consistencia));
			exit();
		}else{
			$this->set(compact('porcentaje_progreso_dia_actual','pendientes_dia_actual','porcentaje_pendiente_dia_actual'
					,'porcentaje_inconsistencia_dia_actual','consistencia_dia_actual','porcentaje_consistencia_dia_actual'
					,'porcentaje_progreso','pendientes','porcentaje_pendiente'
					,'porcentaje_inconsistencia','consistencia','porcentaje_consistencia'));
			$this->set(compact('cantidad_ordenes_dia_actual','cantidad_avanzados_dia_actual','cantidad_inconsistentes_dia_actual',
					'cantidad_ordenes','cantidad_avanzados','cantidad_inconsistentes'));
			
			$this->render('monitor_progreso_semi_pie');
		}
	
	}
	
	public function grafico_consumo_por_dia_v2($unidad_neg=null,$idciclo=null) {
		$this->layout = 'ajax';
		
		$id_usuario = $this->obj_logged_user['Usuario']['id'];
		$this->set(compact('id_usuario'));
	
		if(isset($unidad_neg) || isset($idciclo)){
			$options = array('unidad_neg'=>$unidad_neg,'idciclo'=>$idciclo);
		}else{
			$options = null;
		}
	
		$this->loadModel('KpiLectura');
			
		$arr = $this->KpiLectura->getConsumoPorDia($options);
	
		$this->set(compact('arr'));
		
		$this->render('grafico_consumo_por_dia');
	}
	
	public function resumen_por_ciclos_v2($unidad_neg=null,$idciclo=null) {
		
		$id_usuario = $this->obj_logged_user['Usuario']['id'];
		$this->set(compact('id_usuario'));
	
		if(isset($this->request->query['ajax']) && $this->request->query['ajax']=='false'){
			$this->layout = 'home_tree';
		}else{
			$this->layout = 'default_no_header';
		}
	
		$this->loadModel('ComlecOrdenlectura');
		$this->loadModel('KpiLectura');
	
		if(isset($unidad_neg) || isset($idciclo)){
			$options1 = array('unidad_neg'=>$unidad_neg,'idciclo'=>$idciclo);
		}else{
			$options1 = null;
		}
	
		$arr_ciclos = $this->KpiLectura->getResumenPorCiclos($options1);
		$arr_ciclos_actuales = $this->ComlecOrdenlectura->getCiclosActuales($options1);
	
		$this->set(compact('arr_ciclos','arr_ciclos_actuales'));
	}
	
	public function grafico_rendimiento_consumo_v2($unidad_neg=null,$idciclo=null) {
		$this->layout = 'ajax';
		
		$id_usuario = $this->obj_logged_user['Usuario']['id'];
		$this->set(compact('id_usuario'));
	
		if(isset($unidad_neg) || isset($idciclo)){
			$options = array('unidad_neg'=>$unidad_neg,'idciclo'=>$idciclo);
		}else{
			$options = null;
		}
	
		$this->loadModel('KpiLectura');
	
		$arr_ordenes = $this->KpiLectura->getRendimientoConsumo($options);
	
		$this->set(compact('arr_ordenes'));
	
	}
	
	public function ajax_refresh_kpi() {
		$this->layout = 'ajax';
	
		$this->loadModel('KpiLectura');
	
		$cantidad = $this->KpiLectura->refreshKpi();
	
		if($cantidad>0){
			echo json_encode(array('success'=>true));
		}else{
			echo json_encode(array('success'=>false));
		}
		exit();	
	}
	
}