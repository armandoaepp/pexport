<?php
App::uses('AppShell', 'Console/Command');
App::uses('CakeEmail', 'Network/Email');

class NotificationLecturasShell extends AppShell {
	
	public $uses = array('ComlecOrdenlectura');
	
	
	/**
	 * Envia email de listado de personal asignado para la toma de lecturas
	 * @author Geynen
	 * @version Enero 2015
	 */
	public function send_listado_personal_para_lecturas(){
		
		echo "Starting job ".date("Y-m-d H:i:s")."\n";

		$unidad_negocio = $this->args[0];
		if(isset($unidad_negocio)){
			$this->ComlecOrdenlectura->setDataSource($unidad_negocio);
		}else{
			$this->ComlecOrdenlectura->setDataSource('cix');
		}
		
		$domais = Configure::read('lecturas.domains');
		$ip_server = $domais[strtoupper($unidad_negocio)]['ip'];
		$name_server = $domais[strtoupper($unidad_negocio)]['name'];
		
		$unidades_negocio = Configure::read('lecturas.negocios_all');
		$name_uunn = $unidades_negocio[strtoupper($unidad_negocio)]['name'];
		
		$listarCronograma = $this->ComlecOrdenlectura->listarCronograma('F',date('Y-m-d'),date('Y-m-d'));

		if(count($listarCronograma)>0){
			$arr_files_attach = array();
			foreach ($listarCronograma as $key =>$obj1){
				
				$es_ciclo_sub_estacion = $this->ComlecOrdenlectura->esCicloSubEstacion($obj1[0]['idciclo']);
				
				if($es_ciclo_sub_estacion==0){
					$options = array('unidad_neg'=>$obj1[0]['glomas_unidadnegocio_id'],'idciclo'=>$obj1[0]['idciclo'],'idsector'=>$obj1[0]['idsector']);
					$cantidad_asignados_periodo_actual = $this->ComlecOrdenlectura->getCountAsignados($options);
					if($cantidad_asignados_periodo_actual>0){
						$url_pdf = "http://".$name_server.ENV_WEBROOT."Reportes/listar_personal_by_negocio/".$unidad_negocio."/0/".$obj1[0]['glomas_unidadnegocio_id']."/".$obj1[0]['idciclo']."/".$obj1[0]['idsector'].".pdf";
						//$url_pdf = "http://54.200.245.17/Asignacion/controler/controler-generaacta-monitorear.php?id=4436";
						$file_name = $obj1[0]['glomas_unidadnegocio_id']."-".$obj1[0]['pfactura']."-".$obj1[0]['idciclo']."-".$obj1[0]['idsector'];
						$file_to_save = "/var/www/html".ENV_WEBROOT."app/webroot/pdf/lecturistas_por_sector/".$file_name.".pdf";
						
						copy($url_pdf,$file_to_save);
						
						if (file_exists($file_to_save)) {
							$arr_files_attach[$file_name] = array(
						 		'file' => $file_to_save,
						 		'mimetype' => 'application/pdf'
						 	);
						}
					}
				}
			}
			
			if(count($arr_files_attach)>0){
	
				$email = new CakeEmail('gmail');
				
				//$email->to(array('geynen@gmail.com','vasquezader@gmail.com'));
				$emails_supervisores_distriluz = Configure::read('lecturas.emails_supervisores_distriluz');
				$emails_supervisores_contratistas = Configure::read('lecturas.emails_supervisores_contratistas');
				$email->to($emails_supervisores_distriluz[strtoupper($unidad_negocio)]);
				$email->cc($emails_supervisores_contratistas[strtoupper($unidad_negocio)]);
				$email->bcc(Configure::read('Developers.email_reportes'));
				$email->subject('sistema de lectura ('.$name_uunn.'): Listado del personal para la toma de lectura de medidores');
				$email->template('send_listado_personal_para_lecturas','default'); // NOTAR QUE NO HAY '.ctp'
				$email->emailFormat('html');
				
				$email->attachments($arr_files_attach
						/*array(
						'doc1' => array(
					 		'file' => '/var/www/html/lecturas/app/webroot/images/avatar1_50.jpg',
					 		'mimetype' => 'image/jpg'
					 	),
						'doc2' =>  array(
					 		'file' => '/var/www/html/lecturas/app/webroot/images/avatar1_50.jpg',
					 		'mimetype' => 'application/pdf'
					 	)
				)*/);
		
				$email->send();
			}
		}
		
		echo "Finish job ".date("Y-m-d H:i:s")."\n";
	}
	
	/**
	 * Envia email de resumen del progreso de toma de lectura de medidores
	 * @author Geynen
	 * @version 29 Enero 2015
	 */
	public function send_email_sumary_lecturas(){
	
		echo "Starting job ".date("Y-m-d H:i:s")."\n";
		
		$unidad_negocio = $this->args[0];
		if(isset($unidad_negocio)){
			$this->ComlecOrdenlectura->setDataSource($unidad_negocio);
		}else{
			$this->ComlecOrdenlectura->setDataSource('cix');
		}
		
		$domais = Configure::read('lecturas.domains');
		$ip_server = $domais[strtoupper($unidad_negocio)]['ip'];
		$name_server = $domais[strtoupper($unidad_negocio)]['name'];
		
		$unidades_negocio = Configure::read('lecturas.negocios_all');
		$name_uunn = $unidades_negocio[strtoupper($unidad_negocio)]['name'];
		
		$listarCronograma = $this->ComlecOrdenlectura->listarCronograma('F',date('Y-m-d'),date('Y-m-d'));
		
		if(count($listarCronograma)>0){
			$arr_ciclos = array(); 
			foreach ($listarCronograma as $key =>$obj1){
		
				$es_ciclo_sub_estacion = $this->ComlecOrdenlectura->esCicloSubEstacion($obj1[0]['idciclo']);
		
				if($es_ciclo_sub_estacion==0){
					$options = array('unidad_neg'=>$obj1[0]['glomas_unidadnegocio_id'],'idciclo'=>$obj1[0]['idciclo'],'idsector'=>$obj1[0]['idsector']);
					
					$arr_ciclos_tmp = $this->ComlecOrdenlectura->getResumenPorCiclos($options);
					
					$arr_ciclos = array_merge($arr_ciclos_tmp,$arr_ciclos);
				}
			}
				
			if(count($arr_ciclos)>0){
		
				$email = new CakeEmail('gmail');
		
				//$email->to(array('geynen@gmail.com'));
				//$email->to(array('yyaipenq@distriluz.com.pe','wlopezg@distriluz.com.pe','vinonanp@distriluz.com.pe'));
				$emails_supervisores_contratistas = Configure::read('lecturas.emails_supervisores_contratistas');
				$email->to($emails_supervisores_contratistas[strtoupper($unidad_negocio)]);
				//$email->to(array('walterneyra@pexport.com.pe','santosgutierrez@pexport.com.pe','jenyminchola@pexport.com.pe'));
				$email->bcc(Configure::read('Developers.email_reportes'));
				$email->subject('sistema de lectura ('.$name_uunn.'): Resumen del progreso de toma de lectura de medidores');
				$email->template('send_email_sumary_lecturas','info'); // NOTAR QUE NO HAY '.ctp'
				$email->emailFormat('html');
				
				//Variables de la vista
				$email->viewVars(array('arr_ciclos' => $arr_ciclos,'path'=>'http://'.$name_server.ENV_WEBROOT));
		
				$email->send();
			}
		}
		
		echo "Finish job ".date("Y-m-d H:i:s")."\n";
	}
	
	/**
	 * Envia email de resumen de finalizado de envio de lecturas a Distriluz
	 * @author Geynen
	 * @version 29 Enero 2015
	 */
	public function send_email_finalizado_envio_distriluz(){
	
		echo "Starting job ".date("Y-m-d H:i:s")."\n";
		
		$unidad_negocio = $this->args[0];
		if(isset($unidad_negocio)){
			$this->ComlecOrdenlectura->setDataSource($unidad_negocio);
		}else{
			$this->ComlecOrdenlectura->setDataSource('cix');
		}
		CakeLog::write('send_email_finalizado_envio_distriluz_'.$unidad_negocio, 'init');
		
		$domais = Configure::read('lecturas.domains');
		$ip_server = $domais[strtoupper($unidad_negocio)]['ip'];
		$name_server = $domais[strtoupper($unidad_negocio)]['name'];
		
		$unidades_negocio = Configure::read('lecturas.negocios_all');
		$name_uunn = $unidades_negocio[strtoupper($unidad_negocio)]['name'];
	
		$listarCronograma = $this->ComlecOrdenlectura->listarCronograma('F',date('Y-m-d'),date('Y-m-d'));
	
		$arr_cronograma_ids = array();
		if(count($listarCronograma)>0){
			$arr_ciclos = array();
			foreach ($listarCronograma as $key =>$obj1){
	
				$es_ciclo_sub_estacion = $this->ComlecOrdenlectura->esCicloSubEstacion($obj1[0]['idciclo']);
	
				if($es_ciclo_sub_estacion==0 && $obj1[0]['finalizado']==false){
					$arr_cronograma_ids[] = $obj1[0]['id'];
					
					$options = array('unidad_neg'=>$obj1[0]['glomas_unidadnegocio_id'],'idciclo'=>$obj1[0]['idciclo'],'idsector'=>$obj1[0]['idsector']);
						
					$arr_ciclos_tmp = $this->ComlecOrdenlectura->getResumenPorCiclos($options);
						
					if(count($arr_ciclos_tmp)>0){
						$arr_ciclos = array_merge($arr_ciclos_tmp,$arr_ciclos);
					}
				}
			}
			
			$sum_suministros = 0;
			$sum_enviados_distriluz = 0;
			foreach ($arr_ciclos as $kk => $obj){
				$sum_suministros += $obj[0]['suministros'];
				$sum_enviados_distriluz += $obj[0]['enviados_distriluz'];
			}
			
			CakeLog::write('send_email_finalizado_envio_distriluz_'.$unidad_negocio, 'Sum: '.$sum_suministros.' Env:'.$sum_enviados_distriluz);
			
			if((int) $sum_suministros == (int) $sum_enviados_distriluz && (int) $sum_suministros>0){
				
				$email = new CakeEmail('gmail');
				
				$emails_supervisores_contratistas = Configure::read('lecturas.emails_supervisores_contratistas');
				$email->to($emails_supervisores_contratistas[strtoupper($unidad_negocio)]);
				$email->bcc(Configure::read('Developers.email_reportes'));
				$email->subject('sistema de lectura ('.$name_uunn.'): Reporte de envio de lecturas a Distriluz (100% Completado)');
				$email->template('send_email_finalizado_envio_distriluz','success'); // NOTAR QUE NO HAY '.ctp'
				$email->emailFormat('html');
				
				//Variables de la vista
				$email->viewVars(array('arr_ciclos' => $arr_ciclos,'path'=>'http://'.$name_server.ENV_WEBROOT));
				
				$email->send();
				
				CakeLog::write('send_email_finalizado_envio_distriluz_'.$unidad_negocio, 'Envio Email');
				
				$this->ComlecOrdenlectura->saveActualizarEventoCronogramaToFinalizado($arr_cronograma_ids);
				CakeLog::write('send_email_finalizado_envio_distriluz_'.$unidad_negocio, 'Cambio estado Cronograma');
				
			}
			CakeLog::write('send_email_finalizado_envio_distriluz_'.$unidad_negocio, 'Fin');
		}
	
		echo "Finish job ".date("Y-m-d H:i:s")."\n";
	}
	
}