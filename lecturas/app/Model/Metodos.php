<?php
App::uses('AppModel', 'Model');
/**
 * Activity Model
 *
 * @property Metodos $Metodos
*/ 
class Metodos extends AppModel {
	/**
	 * Display field
	 *
	 * @var string
	 */
	public $name = 'Metodos';
	
	public $useTable = false;
	
	public $esquema='lecturas';

	/**
	 * $belongsTo associations
	 *
	 * @var array
	 */

	public function getRangos(){
		$this->setDateSourceCustom();
		$db = $this->getDataSource();
	
		$sql="select * from lecturas.comlec_rangos order by min desc";
	
		return $db->query($sql);
	}
	
	public function getMetodo($options=null, $varrango){
		$this->setDateSourceCustom();
		$db = $this->getDataSource();
	
		$str_where = '';
		if(isset($options) && $options == 1){
				$str_where .= ' AND comlec_metodo_id = 1 ';		
		}
		if(isset($options) && $options == 2){
			$str_where .= ' AND comlec_metodo_id = 2 ';
		}
	
		$sql="select * from lecturas.comlecuni_metodorango where 1=1 ".$str_where." and comlec_rango_id = $varrango";
	
		return $db->query($sql);
	}
	
	
	

}