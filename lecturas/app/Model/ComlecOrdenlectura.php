<?php
App::uses('AppModel', 'Model');
class ComlecOrdenlectura extends AppModel {
    public $name = 'ComlecOrdenlectura';
    public $esquema='lecturas';
    
    public function comlecXml($xml){
    	$this->setDateSourceCustom ();
    	$db = $this->getDataSource();
    	$sql="SELECT COUNT(*) as cantidad FROM lecturas.comlec_xml WHERE vdescripcion = '$xml'";
    	$result = $db->query($sql);
    	
    	if($result){
    		return $result[0][0]['cantidad'];
    	}
    	return  0;
    	
    }
    
    public function analizarInconsistenciaLectura($analizar){
    	$this->setDateSourceCustom ();
    	$db = $this->getDataSource();
        if(isset($analizar) && $analizar!='0'){
        	$sql=" SELECT lecturao1, lecant, suministro FROM lecturas.comlec_ordenlecturas WHERE sector = '$analizar' AND (obs1 = '' OR obs1 IS NULL) AND (resultadoevaluacion = 'CONSISTENTE' OR resultadoevaluacion = 'INCONSISTENTE') AND validacion = '0'";
        }else{
            $sql=" SELECT lecturao1, lecant, suministro FROM lecturas.comlec_ordenlecturas WHERE (obs1 = '' OR obs1 IS NULL) AND (resultadoevaluacion = 'CONSISTENTE' OR resultadoevaluacion = 'INCONSISTENTE') AND validacion = '0'";
        }
    	return  $db->query($sql);
    
    }
    
    public function updateInconsistenciaLectura($newlecturao,$lecturao, $suministro){
    	$this->setDateSourceCustom ();
    	$db = $this->getDataSource();
    	$sql=" UPDATE lecturas.comlec_ordenlecturas SET lecturao1 = '$newlecturao', lecturaoold = '$lecturao', montoconsumo1=0 WHERE  suministro ='$suministro'";
    	return  $db->query($sql);
    }
    
    public function conceptosByGrupo($idgrupo){
    	$this->setDateSourceCustom ();
    	$db = $this->getDataSource();
    	$sql="select c.id,c.descripcion,c.tipodato  from  lecturas.comlecuni_grupoconceptos  cuc
    			inner join lecturas.glomas_grupos un
    			on cuc.glomas_grupo_id=un.id and un.eliminado=false
    			inner join lecturas.mas_olconceptos c
    			on cuc.mas_olconceptos_id=c.id and c.eliminado=false
    			where un.id=".$idgrupo." and cuc.eliminado=false;";
    	return  $db->query($sql);
    }

    public function listarUnidadneg($pfactura=null){
    	$this->setDateSourceCustom();
        $db = $this->getDataSource();
        
        $str_filtros = '';
        if(isset($pfactura) && $pfactura!='' && $pfactura!='0'){
        	$schema = 'history';
        	$str_filtros .= " AND pfactura='".$pfactura."'";
        }else{
        	$schema = 'lecturas';
        }
        $sql="SELECT distinct o.glomas_unidadnegocio_id as unidadneg, 
                u.descripcion as nombreunidadnegocio FROM
                 ".$schema.".comlec_ordenlecturas o
                left join lecturas.glomas_unidadnegocios u
                on o.glomas_unidadnegocio_id=u.id WHERE 1=1 ".$str_filtros."
                 order by u.descripcion asc";
        
        //echo $sql;exit;
        return  $db->query($sql);
    }
    
    /**
     * Listar Ciclos (Resumen)
     * @param string $options
     * @author Geynen
     * @version 04 Diciembre 2014
     */
    public function listarCiclo($options=NULL){
    	$this->setDateSourceCustom ();
        $db = $this->getDataSource();

        $schema = 'lecturas';
    	$str_where = '';
    	if(isset($options) && is_array($options)){
    		if(isset($options['unidad_neg']) and $options['unidad_neg']!=0){
    			$str_where .= ' AND glomas_unidadnegocio_id = '.$options['unidad_neg'];
    		}
    		if(isset($options['idciclo']) && $options['idciclo']!='0'){
    			$str_where .= " AND ciclo='".$options['idciclo']."'";
    		}
    		if(isset($options['pfactura']) and $options['pfactura']!='0'){
    			$str_where .= " AND pfactura = '".$options['pfactura']."'";
    			$schema = 'history';
    		}else{
    			$str_where .= ' AND P.actual=true';
    			$schema = 'lecturas';
    		}    		
    	}else{
    		$str_where .= ' AND P.actual=true';
    		$schema = 'lecturas';
    	}

        $sql="SELECT distinct ciclo as idciclo, nombreciclo as nombciclo FROM lecturas.glomas_ciclo gc
        		inner join history.pfactura p on p.idpfactura=gc.idpfactura WHERE 1=1 ".$str_where." order by ciclo asc";
        return  $db->query($sql);
    }
    
    /**
     * Listar Ciclos (Transaccional)
     * @param string $options
     * @author Geynen
     */
    public function listarCicloV1($options=NULL){
    	$this->setDateSourceCustom ();
    	$db = $this->getDataSource();
    
    	$schema = 'lecturas';
    	$str_where = '';
    	if(isset($options) && is_array($options)){
    		if(isset($options['unidad_neg']) and $options['unidad_neg']!=0){
    			$str_where .= ' AND glomas_unidadnegocio_id = '.$options['unidad_neg'];
    		}
    		if(isset($options['idciclo']) && $options['idciclo']!='0'){
    			$str_where .= " AND idciclo='".$options['idciclo']."'";
    		}
    		if(isset($options['pfactura']) and $options['pfactura']!='0'){
    			$str_where .= " AND pfactura = '".$options['pfactura']."'";
    			$schema = 'history';
    		}else{
    			$str_where .= '';
    			$schema = 'lecturas';
    		}
    	}
    
    	$sql="SELECT distinct idciclo, nombciclo FROM ".$schema.".comlec_ordenlecturas WHERE 1=1 ".$str_where." order by idciclo asc";
    	return  $db->query($sql);
    }
    
    /**
     * Listar Sector (Resumen)
     * @author Geynen
     * @version 04 Diciembre 2014
     **/
    public function listarSector(){
    	$this->setDateSourceCustom();
    	$db = $this->getDataSource();
    	$sql="SELECT distinct nombresector as sector FROM lecturas.glomas_sector order by nombresector asc";
    	return  $db->query($sql);
    }
    
    /**
     * Listar Sector (Transaccional)
     * @author Geynen
     * @version 19 Junio 2014
     **/
    public function listarSectorV1(){
    	$this->setDateSourceCustom ();
    	$db = $this->getDataSource();
    	$sql="SELECT distinct sector FROM lecturas.comlec_ordenlecturas order by sector asc";
    	return  $db->query($sql);
    }
    
    /**
     * Listar Ruta (Resumen)
     * @author Geynen
     * @version 04 Diciembre 2014
     **/
    public function listarRuta(){
    	$this->setDateSourceCustom ();
    	$db = $this->getDataSource();
    	$sql="SELECT distinct ruta, nombreruta as nombruta FROM lecturas.glomas_ruta order by ruta asc";
    	return  $db->query($sql);
    }
    
    /**
     * Listar Ruta (Transaccional)
     * @author Geynen
     * @version 19 Junio 2014
     **/
    public function listarRutaV1(){
    	$this->setDateSourceCustom ();
    	$db = $this->getDataSource();
    	$sql="SELECT distinct ruta, nombruta FROM lecturas.comlec_ordenlecturas order by ruta asc";
    	return  $db->query($sql);
    }

    
     /**
     * Listar Ciclo por UnidadNegocio (Resumen)
     * @author Geynen
     * @version 04 Diciembre 2014
     **/
    public function listarCicloPorUnidadneg($unidadneg){
    	$this->setDateSourceCustom();
    	$db = $this->getDataSource();
    	if(isset($unidadneg) && $unidadneg!='' && $unidadneg!='0'){
    		$sql="select DISTINCT ciclo, nombreciclo as nombciclo from lecturas.glomas_ciclo gc
    		inner join history.pfactura pf on gc.idpfactura=pf.idpfactura and pf.actual=true
    		where gc.glomas_unidadnegocio_id='$unidadneg' order by ciclo asc";
    	}else{
    		$sql="SELECT distinct ciclo, nombreciclo as nombciclo FROM lecturas.glomas_ciclo order by ciclo asc";
        }
    
    	return  $db->query($sql);
    }
    
    /**
    * Listar Ciclo por UnidadNegocio (Transaccional)
    * @author Geynen
    * @version 19 Junio 2014
    **/
    public function listarCicloPorUnidadnegV1($unidadneg){
    	$this->setDateSourceCustom();
        $db = $this->getDataSource();
        if(isset($unidadneg) && $unidadneg!='' && $unidadneg!='0'){
            $sql="select DISTINCT idciclo as ciclo, nombciclo from lecturas.comlec_ordenlecturas where glomas_unidadnegocio_id='$unidadneg' order by idciclo asc";    
        }else{
            $sql="SELECT distinct idciclo as ciclo, nombciclo FROM lecturas.comlec_ordenlecturas order by idciclo asc";
        }
        
        return  $db->query($sql);
    }
    
    /**
    * Listar Sector por Ciclo (Resumen)
    * @author Geynen
    * @version 04 Diciembre 2014
    **/
    public function listarSectorPorCiclo($ciclo){
    	$this->setDateSourceCustom ();
        $db = $this->getDataSource();
        if(isset($ciclo) && $ciclo!='' && $ciclo!='0'){
            $sql="SELECT distinct nombresector as sector FROM lecturas.glomas_sector gs 
            inner join lecturas.glomas_ciclo gc on gs.idciclo=gc.idciclo
    		inner join history.pfactura pf on gc.idpfactura=pf.idpfactura and pf.actual=true
            where ciclo='$ciclo' order by nombresector asc";    
        }else{
            $sql="SELECT distinct nombresector as sector FROM lecturas.glomas_sector order by nombresector asc";
        }
        
        return  $db->query($sql);
    }
    

    /**
     * Listar Sector por Ciclo (Transaccional)
     * @author Geynen
     * @version 19 Junio 2014
     **/
    public function listarSectorPorCicloV1($ciclo){
    	$this->setDateSourceCustom ();
    	$db = $this->getDataSource();
    	if(isset($ciclo) && $ciclo!='' && $ciclo!='0'){
    		$sql="SELECT distinct sector FROM lecturas.comlec_ordenlecturas where idciclo='$ciclo' order by sector asc";
    	}else{
    		$sql="SELECT distinct sector FROM lecturas.comlec_ordenlecturas order by sector asc";
        }
    
    	return  $db->query($sql);
    }

    /**
    * Listar Rutas por Sector (Resumen)
    * @author Geynen
    * @version 04 Diciembre 2014
    **/
    public function listarRutasPorSector($sector){
    	$this->setDateSourceCustom ();
        $db = $this->getDataSource();
        if(isset($sector) && $sector!='' && $sector!='0'){
            $sql="SELECT distinct ruta, nombreruta as nombruta FROM lecturas.glomas_ruta gr 
            inner join lecturas.glomas_sector gs on gr.idsector=gs.idsector 
            inner join lecturas.glomas_ciclo gc on gs.idciclo=gc.idciclo
    		inner join history.pfactura pf on gc.idpfactura=pf.idpfactura and pf.actual=true
            where nombresector='$sector' order by ruta asc";    
        }else{
            $sql="SELECT distinct ruta, nombreruta as nombruta FROM lecturas.glomas_ruta order by ruta asc";
        }
        
        return  $db->query($sql);
    }
    
    /**
     * Listar Rutas por Sector (Transaccional)
     * @author Geynen
     * @version 14 Junio 2014
     **/
    public function listarRutasPorSectorV1($sector){
    	$this->setDateSourceCustom ();
    	$db = $this->getDataSource();
    	if(isset($sector) && $sector!='' && $sector!='0'){
    		$sql="SELECT distinct ruta, nombruta FROM lecturas.comlec_ordenlecturas where sector='$sector' order by ruta asc";
    	}else{
    		$sql="SELECT distinct ruta, nombruta FROM lecturas.comlec_ordenlecturas order by ruta asc";
        }
    
    	return  $db->query($sql);
    }

    /**
    * Listar Lecturista por Ruta
    * @author Geynen
    * @version 16 Junio 2014
    **/
    public function listarLecturistaPorRuta($tipolectura,$ruta){
    	$this->setDateSourceCustom ();
        $db = $this->getDataSource();

        if($tipolectura=='L'){
            $str_field = 'lecturista1_id';
        }elseif($tipolectura=='R'){
            $str_field = 'lecturista2_id';
        }elseif($tipolectura=='RR'){
            $str_field = 'lecturista3_id';
        }
        if(isset($ruta) && $ruta!='' && $ruta!='0'){
            $sql="SELECT distinct e.id, nombre, nomusuario FROM lecturas.comlec_ordenlecturas ol inner join lecturas.glomas_empleados e on ol.".$str_field."=e.id 
            inner join lecturas.usuarios u on u.glomas_empleado_id = e.id where ruta='$ruta' order by nombre asc";    
        }else{
            $sql="SELECT distinct e.id, nombre, nomusuario FROM lecturas.comlec_ordenlecturas ol inner join lecturas.glomas_empleados e on ol.".$str_field."=e.id 
            inner join lecturas.usuarios u on u.glomas_empleado_id = e.id order by nombre asc";
        }
        
        return  $db->query($sql);
    }
    

    public function inconsistenciaRelectura($suministro, $tipo_lectura){
    	$this->setDateSourceCustom ();
    	$db = $this->getDataSource();
    	$sql="select  lecturas.funcion_relecturas('$suministro','$tipo_lectura')";
    	return  $db->query($sql);
    	
    }
    
    public function inconsistenciaFinalizarlectura($suministro){
    	$this->setDateSourceCustom ();
    	$db = $this->getDataSource();
    	$sql="select lecturas.funcion_forzarrelectura('$suministro')";
    	return  $db->query($sql);
    	 
    }
    
        public function insertarUml($conceptosByGrupo,$idgrupo,$xml,$nombrearchivo){
        	$this->setDateSourceCustom ();
        $db = $this->getDataSource();
        $esquema=$this->esquema;
        $nombretabla='comlec_ordenlecturas'.$idgrupo;
        $campostabla='';
        $campostabla2='';
        foreach ($conceptosByGrupo as $clave => $valor) {
             $campostabla.=$valor[0]['descripcion'].',';
        }
        $db->begin();

        $i=0;
        foreach ($xml->children() as $child) {
                $i++;
                if ($i==1){
                   $facturacorchete =$child->PFactura; 
                    $sql_xml="INSERT INTO ".$esquema.".comlec_xml ( fechaimportacion,vdescripcion,eliminado,glomas_grupo_id,numordenestotal,pfactura,procesando) VALUES ( now(),'".$nombrearchivo."', false,".$idgrupo.",".count($xml->children()).",'".$facturacorchete."',true) returning id ;";
                    $id_xml=$db->query($sql_xml);
                }
                
            $campostabla2='';
            $campo_direccion= htmlspecialchars($child->Direccion, ENT_QUOTES);
            foreach ($conceptosByGrupo as $clave => $valor) {
                  $campo= htmlspecialchars($child->$valor[0]['descripcion'], ENT_QUOTES);
                  $tipodato=$child->$valor[0]['tipodato'];
                   if ($tipodato=='numeric' || $campo==''){
                    $campostabla2.='0'.',';
                  }
                  else
                  {
                    $campostabla2.="'".$campo."'".',';
                  }
            }
            $sql="INSERT INTO ".$esquema.".".$nombretabla." ( ".$campostabla." eliminado,comlec_xml_id) VALUES (".$campostabla2." false,".$id_xml[0][0]['id'].");";
            $db->query($sql);
        }
        unset ($i);
        $sql_xml_update="update  ".$esquema.".comlec_xml set procesando=false where id=".$id_xml[0][0]['id'] ;
         $db->query($sql_xml_update);
        $sql_ejecturar="  select lecturas.cursor_prepare_xml_for_lectura(".$idgrupo.", ".$id_xml[0][0]['id'].")";
       $db->query($sql_ejecturar);
       return $db->commit();

    }
    
    /**
     * Insertar uml from xml v2
     * @param unknown $conceptosByGrupo
     * @param unknown $idgrupo
     * @param unknown $xml
     * @param unknown $nombrearchivo
     * @author Geynen
     * @version 10 Nov 2014
     */
    public function insertarUmlv2($conceptosByGrupo,$idgrupo,$xml,$nombrearchivo,$usuario_id,$client_ip){
    	$this->setDateSourceCustom ();
    	$db = $this->getDataSource();
    	$esquema=$this->esquema;
    	$nombretabla='comlec_ordenlecturas'.$idgrupo;
    	$campostabla='';
    	$campostabla2='';
    	
    	try {

    	foreach ($conceptosByGrupo as $clave => $valor) {
    		$campostabla.=$valor[0]['descripcion'].',';
    	}
    
    	$str_sql_head = '';
    	$str_sql_body = '';
    	$i=0;
    	foreach ($xml->children() as $child) {
    		$i++;
    		if ($i==1){
    			$facturacorchete =$child->PFactura;
    			$sql_xml="INSERT INTO ".$esquema.".comlec_xml ( fechaimportacion,vdescripcion,eliminado,glomas_grupo_id,numordenestotal,pfactura,procesando,usuario_id,client_ip,created) VALUES ( '".date('Y-m-d H:i:s')."','".$nombrearchivo."', false,".$idgrupo.",".count($xml->children()).",'".$facturacorchete."',true,".$usuario_id.",'".$client_ip."','".date('Y-m-d H:i:s')."') returning id ;";
    			$id_xml=$db->query($sql_xml);
    			$db->begin();
    		}
    
    		$campostabla2='';
    		$campo_direccion= htmlspecialchars($child->Direccion, ENT_QUOTES);
    		foreach ($conceptosByGrupo as $clave => $valor) {
    			$campo= htmlspecialchars($child->$valor[0]['descripcion'], ENT_QUOTES);
    			$tipodato=$valor[0]['tipodato'];

    			if(trim($valor[0]['descripcion'])=='LecturaO'){
    				$campostabla2.='null,';
    			}elseif((trim($tipodato)=='numeric' && trim($campo)=='') || (trim($tipodato)=='integer' && trim($campo)=='')){
    				$campostabla2.='0'.',';
    			}else{
    				$campostabla2.="'".$campo."'".',';
    			}
    		}
    		$str_sql_head = "INSERT INTO ".$esquema.".".$nombretabla." ( ".$campostabla." eliminado,comlec_xml_id) VALUES ";
    		$str_sql_body .= "(".$campostabla2." false,".$id_xml[0][0]['id']."),";
    	}
    	unset ($i);
    	$sql_xml_update="update  ".$esquema.".comlec_xml set procesando=false where id=".$id_xml[0][0]['id'].";";

    	$sql_ejecturar="select lecturas.cursor_prepare_xml_for_lectura(".$idgrupo.", ".$id_xml[0][0]['id'].");";
    	
    	$sql_multiple = $str_sql_head.substr($str_sql_body,0,-1).";";
    	$db->query($sql_multiple);    	
    	$db->query($sql_xml_update);
    	$db->query($sql_ejecturar);
    	return $db->commit();
    	} catch (Exception $e) {
    		$db->rollback();
    		$this->log('ERROR:'.$e, 'import_file'.date('Y-m-d'));
    		$sql_xml="DELETE FROM ".$esquema.".comlec_xml WHERE id=".$id_xml[0][0]['id'];
    		$db->query($sql_xml);
    		return NULL;
    	}
    
    }
    
    /**
     * Insertar uml from xls v2
     * @param unknown $conceptosByGrupo
     * @param unknown $idgrupo
     * @param unknown $xml
     * @param unknown $nombrearchivo
     * @author Geynen
     * @version 17 Nov 2014
     */
    public function insertarXlsv2($conceptosByGrupo,$idgrupo,$xls,$nombrearchivo,$usuario_id,$client_ip){
    	$this->setDateSourceCustom ();
    	$db = $this->getDataSource();
    	$esquema=$this->esquema;
    	$nombretabla='comlec_ordenlecturas'.$idgrupo;
    	$campostabla='';
    	$campostabla2='';
    	
    	try {
    		
    	foreach ($conceptosByGrupo as $clave => $valor) {
    		$campostabla.=$valor[0]['descripcion'].',';
    	}
    
    	$str_sql_head = '';
    	$str_sql_body = '';
    	$i=0;
    	foreach ($xls as $key=>$value) {
    		$i++;
    		if ($i==1){
    			$arr_head = $value;
    		}else{
		    //echo 'a'.print_r($arr_head);echo '<br>';
    			if ($i==2){
    				$facturacorchete = $value[array_search('PFactura', $arr_head)];
    				$sql_xml="INSERT INTO ".$esquema.".comlec_xml ( fechaimportacion,vdescripcion,eliminado,glomas_grupo_id,numordenestotal,pfactura,procesando,usuario_id,client_ip,created) VALUES ( now(),'".$nombrearchivo."', false,".$idgrupo.",".(count($xls)-1).",'".$facturacorchete."',true,".$usuario_id.",'".$client_ip."','".date('Y-m-d H:i:s')."') returning id ;";
    				$id_xml=$db->query($sql_xml);
    				$db->begin();
    			}
	    		$campostabla2='';
	    		foreach ($conceptosByGrupo as $clave => $valor) {
	    			//echo 'b'.$valor[0]['descripcion'];echo '<br>';
	    			//echo 'c'.($value[array_search($valor[0]['descripcion'], $arr_head)]);
	    			//echo '<br>';
	    			$campo= htmlspecialchars(@$value[array_search($valor[0]['descripcion'], $arr_head)], ENT_QUOTES);
	    			$tipodato=$valor[0]['tipodato'];
	    			if(trim($valor[0]['descripcion'])=='LecturaO'){
	    				$campostabla2.='null,';
	    			}elseif((trim($tipodato)=='numeric' && trim($campo)=='') || (trim($tipodato)=='integer' && trim($campo)=='')){
	    				$campostabla2.='0'.',';
	    			}else{
	    				$campostabla2.="'".$campo."'".',';
	    			}
	    		}
	    		$str_sql_head = "INSERT INTO ".$esquema.".".$nombretabla." ( ".$campostabla." eliminado,comlec_xml_id) VALUES ";
	    		$str_sql_body .= "(".$campostabla2." false,".$id_xml[0][0]['id']."),";
    		}
    	}
    	unset ($i);
    	$sql_xml_update="update  ".$esquema.".comlec_xml set procesando=false where id=".$id_xml[0][0]['id'].";";
    
    	$sql_ejecturar="select lecturas.cursor_prepare_xml_for_lectura(".$idgrupo.", ".$id_xml[0][0]['id'].");";
    	 
    	$sql_multiple = $str_sql_head.substr($str_sql_body,0,-1).";";
    	//echo $sql_multiple;exit;
    	$db->query($sql_multiple);
    	$db->query($sql_xml_update);
    	$db->query($sql_ejecturar);
    	return $db->commit();
    	} catch (Exception $e) {
    		$db->rollback();
    		$this->log('ERROR:'.$e, 'import_file'.date('Y-m-d'));
    		$sql_xml="DELETE FROM ".$esquema.".comlec_xml WHERE id=".$id_xml[0][0]['id'];
    		$db->query($sql_xml);
    		return NULL;
    	}
    
    }
    
    /**
     * Insertar OTs from xls v2 - Sub Estacion
     * @param unknown $conceptosByGrupo
     * @param unknown $idgrupo
     * @param unknown $xml
     * @param unknown $nombrearchivo
     * @author Geynen
     * @version 20 Dic 2014
     */
    public function insertarSubEstacionXlsv2($conceptosByGrupo,$idgrupo,$xls,$nombrearchivo,$usuario_id,$client_ip){
    	$this->setDateSourceCustom ();
    	$db = $this->getDataSource();
    	$esquema=$this->esquema;
    	$nombretabla='comlec_ordenlecturas'.$idgrupo;
    	$campostabla='';
    	$campostabla2='';
    	 
    	try {
    
    		foreach ($conceptosByGrupo as $clave => $valor) {
    			$campostabla.=$valor[0]['descripcion'].',';
    		}
    
    		$str_sql_head = '';
    		$str_sql_body = '';
    		$i=0;
    		foreach ($xls as $key=>$value) {
    			$i++;
    			if ($i==1){
    				//$arr_head = $value;
    				//Convierto el encabezado a minusculas
    				foreach ($value as $kv=>$vv) {
    					$arr_head[$kv]= strtolower($vv);
    				}
    			}else{
    				//debug($arr_head);
    				//debug($value);
    				//exit;
    				if ($i==2){
    					$facturacorchete = $value[array_search('pfactura', $arr_head)];
    					$sql_xml="INSERT INTO ".$esquema.".comlec_xml ( fechaimportacion,vdescripcion,eliminado,glomas_grupo_id,numordenestotal,pfactura,procesando,usuario_id,client_ip,created,subestacion) VALUES ( now(),'".$nombrearchivo."', false,".$idgrupo.",".(count($xls)-1).",'".$facturacorchete."',true,".$usuario_id.",'".$client_ip."','".date('Y-m-d H:i:s')."',true) returning id ;";
    					$id_xml=$db->query($sql_xml);
    					$db->begin();
    				}
    				$campostabla2='';
    				foreach ($conceptosByGrupo as $clave => $valor) {
    					$valor[0]['descripcion'] = strtolower($valor[0]['descripcion']);
    					//echo 'b'.$valor[0]['descripcion'];echo '<br>';
    					//echo 'c'.($value[array_search($valor[0]['descripcion'], $arr_head)]);
    					//echo '<br>';
    					$campo= htmlspecialchars(@$value[array_search($valor[0]['descripcion'], $arr_head)], ENT_QUOTES);
    					$tipodato=$valor[0]['tipodato'];
    					if(trim($valor[0]['descripcion'])=='unidad_neg' && trim($campo)==''){
    						$campostabla2.='1,';
    					}elseif(trim($valor[0]['descripcion'])=='suministro' && trim($campo)==''){
    						$campo_idorden= htmlspecialchars(@$value[array_search('idorden', $arr_head)], ENT_QUOTES);
    						$campo_codigotec= htmlspecialchars(@$value[array_search('codigotec', $arr_head)], ENT_QUOTES);
    						$campostabla2.="'".$campo_idorden.$campo_codigotec."',";
    					}elseif(trim($valor[0]['descripcion'])=='sector'){
    						$campostabla2.="'SE".$campo."',";
    					}elseif(trim($valor[0]['descripcion'])=='ruta' && trim($campo)==''){
    						$campostabla2.="'1',";
    					}elseif(trim($valor[0]['descripcion'])=='nombruta' && trim($campo)==''){
    						$campostabla2.="'1 - Sub Estación',";
    					}elseif(trim($valor[0]['descripcion'])=='lcorrelati' && trim($campo)==''){
    						$campostabla2.=($i-1).',';
    					}elseif(trim($valor[0]['descripcion'])=='nombre' && trim($campo)==''){
    						$campo_idorden= htmlspecialchars(@$value[array_search('idorden', $arr_head)], ENT_QUOTES);
    						$campo_codigotec= htmlspecialchars(@$value[array_search('codigotec', $arr_head)], ENT_QUOTES);
    						$campo_tipoconsum= htmlspecialchars(@$value[array_search('tipoconsum', $arr_head)], ENT_QUOTES);
    						$campostabla2.="'".$campo_idorden."-".$campo_codigotec." ".$campo_tipoconsum."',";
    					}elseif(trim($valor[0]['descripcion'])=='idciclo' && trim($campo)==''){
    						$campo_grupo= htmlspecialchars(@$value[array_search('grupo', $arr_head)], ENT_QUOTES);
    						$campostabla2.="'".$campo_grupo."',";
    					}elseif(trim($valor[0]['descripcion'])=='nombciclo' && trim($campo)==''){
    						$campo_grupo= htmlspecialchars(@$value[array_search('grupo', $arr_head)], ENT_QUOTES);
    						$campostabla2.="'".$campo_grupo."',";
    					}elseif(trim($valor[0]['descripcion'])=='pinicio' && trim($campo)==''){
    						$campostabla2.="'".date('Ym')."',";
    					}elseif(trim($valor[0]['descripcion'])=='concepto' && trim($campo)==''){
    						$campostabla2.="'1',";
    					}elseif(trim($valor[0]['descripcion'])=='consant' && trim($campo)==''){
    						$campostabla2.="0,";
    					}elseif(trim($valor[0]['descripcion'])=='consret' && trim($campo)==''){
    						$campostabla2.="0,";
    					}elseif(trim($valor[0]['descripcion'])=='demandaact' && trim($campo)==''){
    						$campostabla2.="0,";
    					}elseif(trim($valor[0]['descripcion'])=='promedio' && trim($campo)==''){
    						$campostabla2.="0,";
    					}elseif(trim($valor[0]['descripcion'])=='direccion' && trim($campo)==''){
    						$campo_direccion= htmlspecialchars(@$value[array_search('direcelem', $arr_head)], ENT_QUOTES);
    						$campostabla2.="'".$campo_direccion."',";
    					}elseif(trim($valor[0]['descripcion'])=='lecturao'){
    						$campostabla2.='null,';
    					}elseif((trim($tipodato)=='numeric' && trim($campo)=='') || (trim($tipodato)=='integer' && trim($campo)=='')){
    						$campostabla2.='0,';
    					}else{
    						$campostabla2.="'".$campo."',";
    					}
    				}
    				$str_sql_head = "INSERT INTO ".$esquema.".".$nombretabla." ( ".$campostabla." eliminado,comlec_xml_id) VALUES ";
    				$str_sql_body .= "(".$campostabla2." false,".$id_xml[0][0]['id']."),";
    			}
    		}
    		unset ($i);
    		$sql_xml_update="update  ".$esquema.".comlec_xml set procesando=false where id=".$id_xml[0][0]['id'].";";
    
    		$sql_ejecturar="select lecturas.cursor_prepare_xml_for_lectura(".$idgrupo.",".$id_xml[0][0]['id'].");";
    
    		$sql_multiple = $str_sql_head.substr($str_sql_body,0,-1).";";
    		//echo $sql_multiple;exit;
    		$db->query($sql_multiple);
    		$db->query($sql_xml_update);
    		$db->query($sql_ejecturar);
    		return $db->commit();
    	} catch (Exception $e) {
    		$db->rollback();
    		$this->log('ERROR:'.$e, 'import_file_sub_estaciones'.date('Y-m-d'));
    		$sql_xml="DELETE FROM ".$esquema.".comlec_xml WHERE id=".$id_xml[0][0]['id'];
    		$db->query($sql_xml);
    		return NULL;
    	}
    
    }

        public function ejecutarcalculos($idgrupo,$id_xml){
        	$this->setDateSourceCustom ();
              	$db = $this->getDataSource();
              $sql="  select lecturas.cursor_prepare_xml_for_lectura(".$idgrupo.", ".$id_xml.")";
            return  $db->query($sql);
        }
    
    
    
    public function listarOrdenLecturas($idgrupo){
    	$this->setDateSourceCustom ();
    	$db = $this->getDataSource();
    	$sql="select * from lecturas.comlec_ordenlecturas".$idgrupo." ";
    	return  $db->query($sql);
    }

    public function listarCicloSector($unidadneg=null,$id_ciclo=null,$id_sector=null){
    	$this->setDateSourceCustom ();
    	$db = $this->getDataSource();
    	
    	$str_where = '';
    	if(isset($unidadneg) && $unidadneg!='0'){
    		$str_where .= " AND glomas_unidadnegocio_id='".$unidadneg."'";
    	}
    	if(isset($id_ciclo) && $id_ciclo!='0'){
    		$str_where .= " AND idciclo='".$id_ciclo."'";
    	}
    	if(isset($id_sector) && $id_sector!='0'){
    		$str_where .= " AND lointro.sector='".$id_sector."'";
    	}
    	
       	$sql = " select glomas_unidadnegocio_id as unidad_neg, u.descripcion as nombreunidadnegocio,idciclo, lointro.sector, count(*) as total, count (lecturista1_id) as total_asignados , 
    			round(((cast(count(lecturista1_id) as numeric)/cast(count (*) as numeric))*100),2) as porcentaje
    			from  lecturas.comlec_ordenlecturas lointro left join lecturas.glomas_unidadnegocios u on lointro.glomas_unidadnegocio_id=u.id
    					where lointro.tipolectura = 'L' ".$str_where."
    					group by unidad_neg,idciclo, u.descripcion, lointro.sector";
    	return  $db->query($sql);
		
    }
	
    public function listarCicloSectorReLectura($unidadneg,$id_ciclo=null,$id_sector=null){
    	$this->setDateSourceCustom ();
    	$db = $this->getDataSource();
    	
	    $str_where = '';
	    if(isset($unidadneg) && $unidadneg!='0'){
	    	$str_where .= " AND glomas_unidadnegocio_id='".$unidadneg."'";
	    }
    	if(isset($id_ciclo) && $id_ciclo!='0'){
    		$str_where .= " AND idciclo='".$id_ciclo."'";
    	}
    	if(isset($id_sector) && $id_sector!='0'){
    		$str_where .= " AND lointro.sector='".$id_sector."'";
    	}
    	
        $sql="  select glomas_unidadnegocio_id as unidad_neg, u.descripcion as nombreunidadnegocio,idciclo, lointro.sector, count(*) as total, count (lecturista2_id) as total_asignados , 
  round(((cast(count(lecturista2_id) as numeric)/cast(count (*) as numeric))*100),2) as porcentaje 
  from lecturas.comlec_ordenlecturas lointro left join lecturas.glomas_unidadnegocios u on lointro.glomas_unidadnegocio_id=u.id
  where lointro.tipolectura = 'R' ".$str_where." group by glomas_unidadnegocio_id,lointro.idciclo, u.descripcion, lointro.sector
";
       
        return  $db->query($sql);
    }
    
    public function buscarCicloSector($ciclo, $sector, $idgrupo){
    	$this->setDateSourceCustom ();
    	$db = $this->getDataSource();
    	$esquema=$this->esquema;
    	$sql="select idciclo, sector, nombruta,ruta ,pfactura,glomas_unidadnegocio_id as unidad_neg, (select count(*) from ( 
select lecturista1_id 
from lecturas.comlec_ordenlecturas where tipolectura = 'L' and idciclo='".$ciclo."' and sector ='".$sector."' and nombruta=ol.nombruta and pfactura=ol.pfactura and ruta=ol.ruta and glomas_unidadnegocio_id=ol.glomas_unidadnegocio_id 
group by lecturista1_id) as c) as total_lecturistas , count (lecturista1_id) as total_asignados, count(*) as total 
            from ".$esquema.".comlec_ordenlecturas as ol where ol.tipolectura = 'L' and idciclo='".$ciclo."' and sector ='".$sector."' group by idciclo,ruta, pfactura,sector, nombruta,unidad_neg  order by ruta";
    		//var_dump($sql);exit; 
        return  $db->query($sql);
    }
    
    public function buscarCicloSectorReLectura($ciclo, $sector, $idgrupo){
    	$this->setDateSourceCustom ();
    	$db = $this->getDataSource();
    	$esquema=$this->esquema;
    	$sql="select idciclo, sector, nombruta,ruta ,pfactura,glomas_unidadnegocio_id as unidad_neg, (select count(*) from ( 
select lecturista2_id 
from lecturas.comlec_ordenlecturas where tipolectura = 'R' and idciclo='".$ciclo."' and sector ='".$sector."' and nombruta=ol.nombruta and pfactura=ol.pfactura and ruta=ol.ruta and glomas_unidadnegocio_id=ol.glomas_unidadnegocio_id 
group by lecturista2_id) as c) as total_lecturistas , count (lecturista2_id) as total_asignados, count(*) as total 
            from ".$esquema.".comlec_ordenlecturas as ol where ol.tipolectura = 'L' and idciclo='".$ciclo."' and sector ='".$sector."' group by idciclo,ruta, pfactura,sector, nombruta,unidad_neg  order by ruta";
    	
     //  var_dump($sql);exit;
        return  $db->query($sql);
    }
	
    public function listaLibros($ciclo, $sector,$ruta, $idgrupo,$pfactura){
    	$this->setDateSourceCustom ();
    	$db = $this->getDataSource();
    	$esquema=$this->esquema;                

        $sql="
 select distinct 'libro '||libro as libro ,count(*) ,e.nombre as lecturista ,
 min(ordenruta)as min,max(ordenruta) as max, loext.lecturista1_id , 
 case when e.olmax< ( select count(*) 
 from lecturas.comlec_ordenlecturas lointro left join lecturas.glomas_empleados ee on 
 ee.id=lointro.lecturista1_id
 where  lointro.idciclo='".$ciclo."' and lointro.sector='".$sector."'  and lointro.pfactura='".$pfactura."'  and ee.id=loext.lecturista1_id ) 
 then 'sobrecarga' when e.olmin>
  ( select count(*) from lecturas.comlec_ordenlecturas lointro left join lecturas.glomas_empleados ee on ee.id=lointro.lecturista1_id 
 where lointro.idciclo='".$ciclo."' and lointro.sector='".$sector."'  and lointro.pfactura='".$pfactura."' and ee.id=loext.lecturista1_id ) 
 then 'poca carga' else 'normal' end as estado from lecturas.comlec_ordenlecturas loext 
 left join lecturas.glomas_empleados e on e.id=loext.lecturista1_id where 
 idciclo='".$ciclo."' and loext.sector='".$sector."' and loext.ruta='".$ruta."' 
 and loext.pfactura='".$pfactura."'  and loext.tipolectura='L' group by libro,e.nombre,e.olmax, e.olmin, loext.lecturista1_id


";
      
    	return  $db->query($sql);
    }
        
    public function listaLibrosRelecturas($ciclo, $sector,$ruta, $idgrupo,$pfactura){
    	$this->setDateSourceCustom ();
    	$db = $this->getDataSource();
    	$esquema=$this->esquema;
        
        $sql="select distinct 'libro '||libro as libro ,
                count(*) ,e.nombre as lecturista ,
                min(ordenruta)as min,max(ordenruta) as max,
                loext.lecturista2_id , 
                case when e.olmax< ( select count(*) from
                lecturas.comlec_ordenlecturas lointro 
                left join lecturas.glomas_empleados ee on ee.id=lointro.lecturista2_id 
                where idciclo='".$ciclo."' and sector='".$sector."' and pfactura='".$pfactura."'   and ee.id=loext.lecturista2_id  )
                then 'sobrecarga' when e.olmin> ( select count(*) 
                from lecturas.comlec_ordenlecturas lointro 
                left join lecturas.glomas_empleados ee 
                on ee.id=lointro.lecturista2_id 
                where idciclo='".$ciclo."' and sector='".$sector."' and pfactura='".$pfactura."'   and ee.id=loext.lecturista2_id ) 
                then 'poca carga' else 'normal' end as estado from lecturas.comlec_ordenlecturas loext
                left join lecturas.glomas_empleados e on e.id=loext.lecturista2_id 
                where idciclo='".$ciclo."' and sector='".$sector."' and ruta='".$ruta."' and pfactura='".$pfactura."' 
                    and loext.tipolectura<>'L' 
                     and (loext.validacion is null or loext.validacion='0')
                 group by libro,e.nombre,e.olmax, e.olmin, loext.lecturista2_id 
             ";
       //   var_dump($sql);exit;
         
    	return  $db->query($sql);
    } 
        
        
    public function listaCreateLibro($ciclo, $sector, $ruta, $idgrupo){
    	$this->setDateSourceCustom ();
    	$db = $this->getDataSource();
       
        $sql="   select * from lecturas.comlec_ordenlecturas  lo 
    left join lecturas.glomas_empleados ge on lo.lecturista1_id = ge.id where lo.idciclo ='".$ciclo."'
     and lo.sector = '".$sector."' and lo.ruta='".$ruta."' and lo.tipolectura='L' order by orden";
        //var_dump($sql);exit;
        return  $db->query($sql);
    }
    
    public function listaCreateLibroRelectura($ciclo, $sector, $ruta, $idgrupo){
    	$this->setDateSourceCustom ();
    	$db = $this->getDataSource();
    	
        $sql="
   select * from lecturas.comlec_ordenlecturas lo 
   left join lecturas.glomas_empleados ge 
   on lo.lecturista2_id = ge.id where 
   lo.idciclo = '".$ciclo."' and 
   lo.sector =  '".$sector."'
    and lo.ruta='".$ruta."'
     and lo.tipolectura<>'L'
      and ( lo.validacion is null or lo.validacion='0') order by orden";
        
        //    var_dump($sql);exit;
        return  $db->query($sql);
    }
    
    public function listar_tree($idgrupo){
    	$this->setDateSourceCustom ();
    	$db = $this->getDataSource();
    	$sql="select idciclo,sector,ruta, count(ruta) as numerosuministros
    			from lecturas.comlec_ordenlecturas".$idgrupo." where trim(concepto)='1'
    					group by idciclo,sector,ruta";
    	return  $db->query($sql);
    }
	
    public function guardarLecturistaLibro($desde, $hasta, $lecturista, $ciclo, $sector, $ruta, $grupo,$negocio, $fecha){
    	$this->setDateSourceCustom ();
    	$db = $this->getDataSource();
    	$sql="select lecturas.funcion_libro('$grupo','$negocio','$ciclo','$sector','$ruta','$lecturista','$desde','$hasta', '$fecha')";
    	//echo $sql;exit;
    	return  $db->query($sql);
    		
    }
    public function guardarLecturistaLibroRelecturas($desde, $hasta, $lecturista, $ciclo, $sector, $ruta, $grupo,$negocio){
    	$this->setDateSourceCustom ();
    	$db = $this->getDataSource();
    	$sql="select lecturas.funcion_librorelecturas('$grupo','$negocio','$ciclo','$sector','$ruta','$lecturista','$desde','$hasta')";
    	return  $db->query($sql);
    		
    }	
    public function listaDigitarLectura($idgrupo, $lecturista_id,$ruta,$sector,$tipolec,$lectespecial){
    	ini_set('memory_limit', '-1');
    	$this->setDateSourceCustom ();
    	$db = $this->getDataSource();
    	$esquema=$this->esquema;




        if ($tipolec!='0'&&$sector!='0'&&$ruta!='0'&&$lecturista_id!='0'&&$lectespecial=='0'){
        /* MULTIPLE FILTRO*/
        $sql="select e.nombre as lecturista,lo.lecturao1 as lecturao12,* from ".$esquema.".comlec_ordenlecturas lo join ".$esquema.".comlec_ordenlecturas".$idgrupo." lo1
                on lo1.unidad_neg || '-' || lo1.pfactura || '-'  || lo1.id  =  lo.comlec_ordenlectura_id and lo.tipolectura='$tipolec'
                inner join lecturas.glomas_empleados e on e.id=lo.lecturista_id where lo1.sector= '$sector' and lo1.ruta= '$ruta' AND lo.lecturista_id = '$lecturista_id' order by orden,ordenruta";  

        } 
        /*CASOS EN LOS QUE SOLO SE SELECCIONA UN CRITERIO*/
        /*POR SECTOR*/
        elseif ($tipolec!='0'&&$sector!='0'&&$ruta=='0'&&$lecturista_id=='0'&&$lectespecial=='0') {
            $sql="select e.nombre as lecturista,lo.lecturao1 as lecturao12,* from ".$esquema.".comlec_ordenlecturas lo join ".$esquema.".comlec_ordenlecturas".$idgrupo." lo1
                on lo1.unidad_neg || '-' || lo1.pfactura || '-'  || lo1.id  =  lo.comlec_ordenlectura_id and lo.tipolectura='$tipolec'
                inner join lecturas.glomas_empleados e on e.id=lo.lecturista_id where lo1.sector= '$sector' order by orden,ordenruta";
        }
        /*POR RUTA*/ 
        elseif ($tipolec!='0'&&$sector=='0'&&$ruta!='0'&&$lecturista_id=='0'&&$lectespecial=='0') {
            $sql="select e.nombre as lecturista,lo.lecturao1 as lecturao12,* from ".$esquema.".comlec_ordenlecturas lo join ".$esquema.".comlec_ordenlecturas".$idgrupo." lo1
                on lo1.unidad_neg || '-' || lo1.pfactura || '-'  || lo1.id  =  lo.comlec_ordenlectura_id and lo.tipolectura='$tipolec'
                inner join lecturas.glomas_empleados e on e.id=lo.lecturista_id where lo1.ruta= '$ruta' order by orden,ordenruta";
        } 
        /*POR LECTURISTA*/
        elseif ($tipolec!='0'&&$sector=='0'&&$ruta=='0'&&$lecturista_id!='0'&&$lectespecial=='0') {
            $sql="select e.nombre as lecturista,lo.lecturao1 as lecturao12,* from ".$esquema.".comlec_ordenlecturas lo join ".$esquema.".comlec_ordenlecturas".$idgrupo." lo1
                on lo1.unidad_neg || '-' || lo1.pfactura || '-'  || lo1.id  =  lo.comlec_ordenlectura_id and lo.tipolectura='$tipolec'
                inner join lecturas.glomas_empleados e
                on e.id=lo.lecturista_id where lo.lecturista_id = '$lecturista_id' order by orden,ordenruta";   
        } 

        /*CASOS EN LOS QUE SE SELECCIONA DOS CRITERIOS*/
        /* POR SECTOR Y RUTA*/
        elseif ($tipolec!='0'&&$sector!='0'&&$ruta!='0'&&$lecturista_id=='0'&&$lectespecial=='0') {
            $sql="select e.nombre as lecturista,lo.lecturao1 as lecturao12,* from ".$esquema.".comlec_ordenlecturas lo join ".$esquema.".comlec_ordenlecturas".$idgrupo." lo1
                on lo1.unidad_neg || '-' || lo1.pfactura || '-'  || lo1.id  =  lo.comlec_ordenlectura_id and lo.tipolectura='$tipolec'
                inner join lecturas.glomas_empleados e on e.id=lo.lecturista_id where lo1.sector= '$sector' and lo1.ruta= '$ruta' order by orden,ordenruta";  
        }
        /*POR SECTOR Y LECTURISTA*/
        elseif ($tipolec!='0'&&$sector!='0'&&$ruta=='0'&&$lecturista_id!='0'&&$lectespecial=='0') {
            $sql="select e.nombre as lecturista,lo.lecturao1 as lecturao12,* from ".$esquema.".comlec_ordenlecturas lo join ".$esquema.".comlec_ordenlecturas".$idgrupo." lo1
                on lo1.unidad_neg || '-' || lo1.pfactura || '-'  || lo1.id  =  lo.comlec_ordenlectura_id and lo.tipolectura='$tipolec'
                inner join lecturas.glomas_empleados e on e.id=lo.lecturista_id where lo1.sector= '$sector' AND lo.lecturista_id = '$lecturista_id' order by orden,ordenruta";  
        }
        /*POR RUTA Y LECTURISTA*/
        elseif ($tipolec!='0'&&$sector=='0'&&$ruta!='0'&&$lecturista_id!='0'&&$lectespecial=='0') {
            $sql="select e.nombre as lecturista,lo.lecturao1 as lecturao12,* from ".$esquema.".comlec_ordenlecturas lo join ".$esquema.".comlec_ordenlecturas".$idgrupo." lo1
                on lo1.unidad_neg || '-' || lo1.pfactura || '-'  || lo1.id  =  lo.comlec_ordenlectura_id and lo.tipolectura='$tipolec'
                inner join lecturas.glomas_empleados e on e.id=lo.lecturista_id where lo1.ruta= '$ruta' AND lo.lecturista_id = '$lecturista_id' order by orden,ordenruta";  
        }


        elseif ($tipolec!='0'&&$sector=='0'&&$ruta=='0'&&$lecturista_id=='0'&&$lectespecial=='0') {
            $sql="select e.nombre as lecturista,lo.lecturao1 as lecturao12,* from ".$esquema.".comlec_ordenlecturas lo join ".$esquema.".comlec_ordenlecturas".$idgrupo." lo1
                on lo1.unidad_neg || '-' || lo1.pfactura || '-'  || lo1.id  =  lo.comlec_ordenlectura_id and lo.tipolectura='$tipolec'
                inner join lecturas.glomas_empleados e on e.id=lo.lecturista_id order by orden,ordenruta";  
        }

        /* POR LECTURAS SIN PROCESAR O SIN ASIGNAR FILTROS ESPECIALES*/

        /*SIN RESOLVER TODAS*/

        elseif ($tipolec!='0'&&$sector=='0'&&$ruta=='0'&&$lecturista_id=='0'&&$lectespecial=='SR') {
            $sql="select e.nombre as lecturista,lo.lecturao1 as lecturao12,* from ".$esquema.".comlec_ordenlecturas lo join ".$esquema.".comlec_ordenlecturas".$idgrupo." lo1
                on lo1.unidad_neg || '-' || lo1.pfactura || '-'  || lo1.id  =  lo.comlec_ordenlectura_id and lo.tipolectura='$tipolec'
                inner join lecturas.glomas_empleados e on e.id=lo.lecturista_id  where lo.lecturao1 is NULL and obs1 is NULL order by orden,ordenruta";  
        }

        /*SIN RESOLVER POR SECTOR*/

        elseif ($tipolec!='0'&&$sector!='0'&&$ruta=='0'&&$lecturista_id=='0'&&$lectespecial=='SR') {
            $sql="select e.nombre as lecturista,lo.lecturao1 as lecturao12,* from ".$esquema.".comlec_ordenlecturas lo join ".$esquema.".comlec_ordenlecturas".$idgrupo." lo1
                on lo1.unidad_neg || '-' || lo1.pfactura || '-'  || lo1.id  =  lo.comlec_ordenlectura_id and lo.tipolectura='$tipolec'
                inner join lecturas.glomas_empleados e on e.id=lo.lecturista_id  where lo1.sector= '$sector' and lo.lecturao1 is NULL and obs1 is NULL order by orden,ordenruta";  
        }


        /*SIN RESULTADO POR SECTOR*/

        elseif ($tipolec!='0'&&$sector!='0'&&$ruta=='0'&&$lecturista_id=='0'&&$lectespecial=='SC') {
            $sql="select e.nombre as lecturista,lo.lecturao1 as lecturao12,* from ".$esquema.".comlec_ordenlecturas lo join ".$esquema.".comlec_ordenlecturas".$idgrupo." lo1
                on lo1.unidad_neg || '-' || lo1.pfactura || '-'  || lo1.id  =  lo.comlec_ordenlectura_id and lo.tipolectura='$tipolec'
                inner join lecturas.glomas_empleados e on e.id=lo.lecturista_id  where lo1.sector= '$sector' and lo.resultadoevaluacion is NULL order by orden,ordenruta";  
        }

        /*SIN RESULTADO TODAS*/


        elseif ($tipolec!='0'&&$sector=='0'&&$ruta=='0'&&$lecturista_id=='0'&&$lectespecial=='SC') {
            $sql="select e.nombre as lecturista,lo.lecturao1 as lecturao12,* from ".$esquema.".comlec_ordenlecturas lo join ".$esquema.".comlec_ordenlecturas".$idgrupo." lo1
                on lo1.unidad_neg || '-' || lo1.pfactura || '-'  || lo1.id  =  lo.comlec_ordenlectura_id and lo.tipolectura='$tipolec'
                inner join lecturas.glomas_empleados e on e.id=lo.lecturista_id  where  lo.resultadoevaluacion is NULL order by orden,ordenruta";  
        }



        /*SIN ASIGNAR POR SECTOR*/

        elseif ($tipolec!='0'&&$sector!='0'&&$ruta=='0'&&$lecturista_id=='0'&&$lectespecial=='SA') {
            $sql="select e.nombre as lecturista,lo.lecturao1 as lecturao12,* from ".$esquema.".comlec_ordenlecturas lo join ".$esquema.".comlec_ordenlecturas".$idgrupo." lo1
                on lo1.unidad_neg || '-' || lo1.pfactura || '-'  || lo1.id  =  lo.comlec_ordenlectura_id and lo.tipolectura='$tipolec'
                left outer join lecturas.glomas_empleados e on e.id=lo.lecturista_id  where lo1.sector= '$sector' and lo.lecturista_id is null order by orden,ordenruta";  
        }

        /*SIN ASIGNAR TODAS*/


        elseif ($tipolec!='0'&&$sector=='0'&&$ruta=='0'&&$lecturista_id=='0'&&$lectespecial=='SA') {
            $sql="select e.nombre as lecturista,lo.lecturao1 as lecturao12,* from ".$esquema.".comlec_ordenlecturas lo join ".$esquema.".comlec_ordenlecturas".$idgrupo." lo1
                on lo1.unidad_neg || '-' || lo1.pfactura || '-'  || lo1.id  =  lo.comlec_ordenlectura_id and lo.tipolectura='$tipolec'
                left outer join lecturas.glomas_empleados e on e.id=lo.lecturista_id  where  lo.lecturista_id is null order by orden,ordenruta";  
        }

        /*
    	if($ruta== '0'){
    		if($lecturista_id == '0'){
    			$sql="select e.nombre as lecturista,lo.lecturao1 as lecturao12,* from ".$esquema.".comlec_ordenlecturas lo join ".$esquema.".comlec_ordenlecturas".$idgrupo." lo1
    			on lo1.unidad_neg || '-' || lo1.pfactura || '-'  || lo1.id  =  lo.comlec_ordenlectura_id and lo.tipolectura='L'
    			inner join lecturas.glomas_empleados e on e.id=lo.lecturista_id order by orden,ordenruta";
    			
    		}else{
    			$sql="select e.nombre as lecturista,lo.lecturao1 as lecturao12,* from ".$esquema.".comlec_ordenlecturas lo join ".$esquema.".comlec_ordenlecturas".$idgrupo." lo1
    			on lo1.unidad_neg || '-' || lo1.pfactura || '-'  || lo1.id  =  lo.comlec_ordenlectura_id and lo.tipolectura='L'
    			inner join lecturas.glomas_empleados e
    			on e.id=lo.lecturista_id where lo.lecturista_id = '$lecturista_id' order by orden,ordenruta";    			
    		}
    			
    	}else {
    		if($lecturista_id == '0'){
    			$sql="select e.nombre as lecturista,lo.lecturao1 as lecturao12,* from ".$esquema.".comlec_ordenlecturas lo join ".$esquema.".comlec_ordenlecturas".$idgrupo." lo1
    			on lo1.unidad_neg || '-' || lo1.pfactura || '-'  || lo1.id  =  lo.comlec_ordenlectura_id and lo.tipolectura='L'
    			inner join lecturas.glomas_empleados e on e.id=lo.lecturista_id where lo1.ruta= '$ruta' order by orden,ordenruta";
    			 
    		}else{
    			$sql="select e.nombre as lecturista,lo.lecturao1 as lecturao12,* from ".$esquema.".comlec_ordenlecturas lo join ".$esquema.".comlec_ordenlecturas".$idgrupo." lo1
    			on lo1.unidad_neg || '-' || lo1.pfactura || '-'  || lo1.id  =  lo.comlec_ordenlectura_id and lo.tipolectura='L'
    			inner join lecturas.glomas_empleados e
    			on e.id=lo.lecturista_id where lo.lecturista_id = '$lecturista_id' and lo1.ruta= '$ruta' order by orden,ordenruta";
    		}
    		
    	}
        */
   
    	
    	
    	return  $db->query($sql);
    }
	
    /**
    * Insert Lectura
    * @author Geynen
    * @version 14 Junio 2014
    **/
    public function saveregistarlecturista($tipolectura, $lectura, $codigo,$codigoobservacion,$obj_user_id){
    	$codigo=  trim($codigo);
    	$this->setDateSourceCustom ();$db = $this->getDataSource();
        $esquema=$this->esquema;
    	$sql="SELECT  $esquema.insertar_lecturao($lectura, '$codigo', '$tipolectura', $obj_user_id, $codigoobservacion,'1')";
    	return  $db->query($sql);
    }
    
    /**
     * Guarda relectura y lectura desde el modulo de inconsistencias
     * @author Joel Vasquez
     * @version 13 Agosto 2014
     **/
    public function saveModuloInconsistencias($tipolectura, $lectura, $codigo,$codigoobservacion,$obj_user_id){
    	$codigo=  trim($codigo);
    	$this->setDateSourceCustom ();$db = $this->getDataSource();
    	$esquema=$this->esquema;
    	$sql="SELECT  $esquema.insertar_lecturao_inconsistencia($lectura, '$codigo', '$tipolectura', $obj_user_id, $codigoobservacion,'1')";
    	return  $db->query($sql);
    }
    
    public function validarCodigoObservacion($codigoobservacion){
    	$codigoobservacion=  trim($codigoobservacion);
    	$this->setDateSourceCustom ();$db = $this->getDataSource();
    	$sql="select id,descripcion,codigofac,comlec_condicionobs_id as condicional from lecturas.comlec_observaciones where codigocamp='$codigoobservacion'";
    	return  $db->query($sql);
    }
    
    /**
    * Get Lectura por ordenlectura_id
    * @author Geynen
    * @version 14 Junio 2014
    **/
    public function getLecturaById($tipolectura, $codigo){
    	$this->setDateSourceCustom ();$db = $this->getDataSource();
    	$esquema=$this->esquema;
    	$sql="select * from ".$esquema.".comlec_ordenlecturas lo where lo.comlec_ordenlectura_id='$codigo' and lo.tipolectura='$tipolectura' ";

    	return  $db->query($sql);
    }

    /**
     * Get Lectura por sumimistro
     * @author joel Vasquez
     * @version 13 Agosto 2014
     **/
    public function getLecturaBySuministro($tipolectura, $codigo){
    	$this->setDateSourceCustom();
    	$db = $this->getDataSource();
    	$esquema=$this->esquema;
    	
    	$str_where = " ";
    	if(isset($tipolectura)){
    		$str_where = " and lo.tipolectura='$tipolectura' ";
    	}
    	$sql="select * from ".$esquema.".comlec_ordenlecturas lo where lo.suministro='$codigo' ".$str_where;
    
    	return  $db->query($sql);
    }
    
    /**
     * Get Lectura por sumimistro
     * @param string|array $suministro
     * @author Geynen
     * @version 19 Agosto 2014
     **/
    public function getOrdenBySuministro($suministro){
    	$this->setDateSourceCustom ();$db = $this->getDataSource();
    	$esquema=$this->esquema;
    	
    	if(is_array($suministro)){
    		$sql="select * from ".$esquema.".comlec_ordenlecturas lo where lo.suministro IN ('".implode("','",$suministro)."') ";
    	}else{
    		$sql="select * from ".$esquema.".comlec_ordenlecturas lo where lo.suministro='$suministro' ";
    	}    	
    
    	return  $db->query($sql);
    }
    
    /**
     * Get Lectura por sumimistro
     * @author Geynen
     * @version 27 Enero 2015
     **/
    public function getLecturaRepartoBySuministro($tipolectura, $codigo){
    	$this->setDateSourceCustom();
    	$db = $this->getDataSource();
    	$esquema=$this->esquema;
    	 
    	$str_where = " ";
    	if(isset($tipolectura)){
    		$str_where = " and lo.tipolectura='$tipolectura' ";
    	}
    	$sql="select lo.*, rep.ruta as idrutareparto, rep.lcorrelati as lcorrelatireparto from ".$esquema.".comlec_ordenlecturas lo left join repartos.comrep_ordenrepartos rep on lo.suministro=rep.suministro  
    	where lo.suministro='$codigo' ".$str_where;
    
    	return  $db->query($sql);
    }
    
    public function listarXml($idgrupo,$criterio=null,$date_start=null,$date_end=null,$sector=null){
    	$this->setDateSourceCustom();
    	$db = $this->getDataSource();
    	
    	$filtro_fecha = "";
    	$str_sector = "";
    	if(strpos($criterio, 'F') !== false){
    		$filtro_fecha .= " AND cast(fechaimportacion as date)  >= '$date_start' AND cast(fechaimportacion as date)  <= '$date_end 23:59:59'";
    	}
    	if(strpos($criterio, 'S') !== false){
	    	if(isset($sector) && $sector!=0 && $sector!=''){
	    		$str_sector .= " AND sector = '".$sector."'";
	    		$filtro_fecha .= " AND sector = '".$sector."'";
	    	}
    	}
    	
    	$sql="select lecturas.comlec_xml.id, numordenesvalidadas,numordenestotal,vdescripcion, lecturas.comlec_xml.pfactura,fechaimportacion,fechaexportacion,exportadastotal,exportadasvalidadas,
                
               (select count(*) from lecturas.comlec_ordenlecturas where comlec_xml_id=lecturas.comlec_xml.id and TIPOLECTURA in ('L','R','RR') and resultadoevaluacion='INCONSISTENTE') AS L_INCONSISTENTES_NO_PROCESADAS,
    			(select string_agg(sector, ', ') from (select DISTINCT sector from lecturas.comlec_ordenlecturas where comlec_xml_id=lecturas.comlec_xml.id ".$str_sector.") T) AS SECTORES,
 				(select count(*) from lecturas.comlec_ordenlecturas where comlec_xml_id=lecturas.comlec_xml.id and resultadoevaluacion='CONSISTENTE' and ws_l_estado='1') AS count_enviadas_distriluz
    	
                from lecturas.comlec_xml 
    			".($str_sector!=''?" inner join lecturas.comlec_ordenlecturas on comlec_xml_id=lecturas.comlec_xml.id ":" "). 
    			" where lecturas.comlec_xml.glomas_grupo_id=".$idgrupo." and lecturas.comlec_xml.eliminado=false ".$filtro_fecha." order by fechaimportacion desc";

    	return  $db->query($sql);
    }
    
    public function metodosPorLectura($codigo){
    	$codigo=  trim($codigo);
    	$this->setDateSourceCustom ();$db = $this->getDataSource();
    	$sql="select  lm.minima,lm.maxima,m.nombre as metodo,mr.formula_max,mr.formula_min,
                mr.variable  as variable
                from lecturas.comlec_lecturametodo lm
                inner join  lecturas.comlec_metodos m on lm.comlec_metodo_id=m.id
                inner join lecturas.comlecuni_metodorango mr on mr.comlec_metodo_id=lm.comlec_metodo_id and mr.comlec_rango_id=lm.comlec_rango_id
                inner join lecturas.comlec_ordenlecturas ol on lm.comlec_ordenlectura_id=ol.comlec_ordenlectura_id
                 where lm.comlec_ordenlectura_id='".$codigo."'  and ol.tipolectura='L' ";
    	return  $db->query($sql);
    }
    
    public function inconsistenciaHistorico($suministro){
    	$suministro =  trim($suministro);
    	$this->setDateSourceCustom ();$db = $this->getDataSource();
    	$sql="select pfactura, (lecturao1 - lecant) as montoc from history.comlec_ordenlecturashistory3 where suministro = '$suministro'";
    	return  $db->query($sql);
    }
    
    
    public function listarXMLid($id_xml){

    	$this->setDateSourceCustom ();$db = $this->getDataSource();
    	$sql="select *,              (   select count(*) from lecturas.comlec_ordenlecturas where comlec_xml_id=lecturas.comlec_xml.id AND TIPOLECTURA in ('L','R','RL')
            and resultadoevaluacion='CONSISTENTE' ) as salidavalidada from lecturas.comlec_xml
            where id=".$id_xml."";
    	return  $db->query($sql);
    }
    
    public function quitarFechaExportacionXML($id_xml){
    
    	$this->setDateSourceCustom();
    	$db = $this->getDataSource();
    	$sql="update lecturas.comlec_xml set fechaexportacion=null where id=".$id_xml."";
    	return  $db->query($sql);
    }
    
    public function mostrarCodigoL($codigolectura){
        $this->setDateSourceCustom ();$db = $this->getDataSource();
        $sql="select codigofac from lecturas.comlec_observaciones where codigofac ='$codigolectura'";
        $obj_codigofac = $db->query($sql);
        $codigofac='0';
        if ($obj_codigofac){
               foreach ($obj_codigofac as $clave => $valor) {
                   $codigofac = $valor[0]['codigofac'];
              }
        }
        return $codigofac;
    }
    
    
    public function exportarLecturasCix($id_xml,$fechaexportacion){

    	$this->setDateSourceCustom ();$db = $this->getDataSource();
       
        $sql_update=" update lecturas.comlec_ordenlecturas1 set lecturao=a.lecturao,codlectura=a.obs,flectura='".$fechaexportacion."' from 
(select suministro, lecturao1 as lecturao, 
(select codigofac from lecturas.comlec_observaciones where cast(codigocamp as varchar)=lecturas.comlec_ordenlecturas.obs1 )
as obs, now() as fecha
from lecturas.comlec_ordenlecturas  where COMLEC_XML_ID='".$id_xml."' and resultadoevaluacion='CONSISTENTE'
) a
where lecturas.comlec_ordenlecturas1.suministro=a.suministro

and lecturas.comlec_ordenlecturas1.COMLEC_XML_ID='".$id_xml."' ";
        
        $db->query($sql_update);
        
$sql="select PFactura,Unidad_Neg,Sector,Ruta,LCorrelati,Suministro,Concepto,Concepto_D,Marca, Modelo,SerieFab,Digitos, 
            Nombre,Direccion,DireccionR,LecturaO,OrdTrabajo,Cartera,FLectura,codlectura as codigo1,Factor,LecAAnt,LecAnt,Promedio,
            ConsRet,ConsAnt,DemandaAct,TConexion,IdCiclo,NombCiclo,NombRuta,PInicio,Tarifa,ZonaOpt,SectorOpt
             from  lecturas.comlec_ordenlecturas1 
           
            where comlec_xml_id=".$id_xml."  order by lcorrelati ";
    	return  $db->query($sql);
    }
    
    public function exportarLecturasTac($id_xml,$fechaexportacion){
    
    	$this->setDateSourceCustom ();$db = $this->getDataSource();
    	 
    	$sql_update=" update lecturas.comlec_ordenlecturas1 set lecturao=a.lecturao,codlectura=a.obs,
            flectura='".$fechaexportacion."',hlectura=a.hlectura
            from
(select suministro, lecturao1 as lecturao,
(select codigofac from lecturas.comlec_observaciones where cast(codigocamp as varchar)=lecturas.comlec_ordenlecturas.obs1 )
as obs,
    
case when fechaejecucion1 is null then  to_char(fechaasignado1, 'DD/MM/YYYY')
else
 to_char(fechaejecucion1, 'DD/MM/YYYY')
end
as flectura ,
    
case when fechaejecucion1 is null then  to_char(fechaasignado1, 'HH12:MI:SS am')
else
 to_char(fechaejecucion1, 'HH12:MI:SS am')
end
as hlectura
    
    
from lecturas.comlec_ordenlecturas  where COMLEC_XML_ID='".$id_xml."' and resultadoevaluacion='CONSISTENTE'
) a
where lecturas.comlec_ordenlecturas1.suministro=a.suministro
    
and lecturas.comlec_ordenlecturas1.COMLEC_XML_ID='".$id_xml."' ";
    
    	$db->query($sql_update);
    
    	$sql="select
            codlectura as codigo1,
          *
             from  lecturas.comlec_ordenlecturas1
             where comlec_xml_id=".$id_xml."  order by lcorrelati";
    	return  $db->query($sql);
    }
    
    
    public function saveExportarXML($numeroexportadastotal, $numeroexportadasvalidadas,$id_xml){
    	$this->setDateSourceCustom ();$db = $this->getDataSource();
    	$sql="update lecturas.comlec_xml set exportadastotal=".$numeroexportadastotal.",   exportadasvalidadas=".$numeroexportadasvalidadas.", fechaexportacion=now() where id=".$id_xml."";
    	return  $db->query($sql);
    }
    
    public function liberar_libro($libro){
    	$this->setDateSourceCustom ();$db = $this->getDataSource();
    	$sql="UPDATE lecturas.comlec_ordenlecturas set down1 = '0', fechadown1 = NULL WHERE down1 = '1' and lecturao1 IS NULL and resultadoevaluacion IS NULL and libro = '$libro'";
    	return  $db->query($sql);
    	
    }
    
    public function sanearMovil(){
    	$this->setDateSourceCustom();
    	$db = $this->getDataSource();
    	$sql="SELECT lecturas.sanearmovil('L')";
    	return  $db->query($sql);    	 
    }
   
    
	public function listarInconsistenciasCix($criterio,$filtros=null,$filter_advance,$page,$limit,$search=null,$consumo_min=null,$consumo_max=null){
		ini_set('memory_limit', '-1');
		$this->setDateSourceCustom ();$db = $this->getDataSource();
		$esquema=$this->esquema;
		$bool_inconsistencia = true;
		
		$sqlsanermovil = "SELECT lecturas.sanearmovil('L')";
		$db->query($sqlsanermovil);
		
		$str_join = " left join glomas_empleados e1 on ol.lecturista1_id = e1.id ";
		$str_join .= " left join glomas_empleados e2 on ol.lecturista2_id = e2.id ";
		
		$str_fields = 'validacion, tipolectura, ol.comlec_ordenlectura_id, consant, orden, libro, lcorrelati, seriefab,cliente,direccion,suministro,lecturaant1px as lap, null as lae,lecturao1,lecturao2, lecturao3, obsantpx as obs_ant_px, obs1, obs2, lecant, promedio, montoconsumo1, resultadoevaluacion, foto1, foto2, foto3, e1.nombre as lecturista1, e2.nombre as lecturista2, metodofinal1, metodofinal2, metodofinal3, ws_l_estado, COUNT(*) OVER() as row_total';
		
		if ( isset( $page ) && isset($limit) ){
			$str_order = " ORDER BY orden, ordenruta";
			$str_limit = " LIMIT ".intval( $limit )." OFFSET ".intval( $page );
		}else{
			$str_order = " ORDER BY orden, ordenruta";
			$str_limit = "";
		}
		
		$str_where = " ";
		// Listar por pfactura
		if(isset($filtros['pfactura'])){
			$str_where .= " AND pfactura = '".$filtros['pfactura']."' ";
			$esquema = "history";
		}
		
		if(strpos($criterio, 'F') !== false){
			$str_where .= " AND cast(ol.fechaasignado1 as date)  >= '".$filtros['date_start']."' AND cast(ol.fechaasignado1 as date)  <= '".$filtros['date_end']." 23:59:59'";
		}
		if(strpos($criterio, 'U') !== false){
			if(isset($filtros['unidadneg']) && $filtros['unidadneg']!=0 && $filtros['unidadneg']!=''){
				$str_where .= " AND glomas_unidadnegocio_id = '".$filtros['unidadneg']."'";
			}
			if(isset($filtros['ciclo']) && $filtros['ciclo']!=0 && $filtros['ciclo']!=''){
				$str_where .= " AND idciclo = '".$filtros['ciclo']."'";
			}
			if(isset($filtros['sector']) && $filtros['sector']!=0 && $filtros['sector']!=''){
				$str_where .= " AND sector = '".$filtros['sector']."'";
			}
		}
		if(strpos($criterio, 'L') !== false){
			$str_where .= " AND lecturista1_id = '".$filtros['lecturista']."' ";
		}
		if(strpos($criterio, 'S') !== false){
			//$str_where .= " AND CAST(suministro AS text) ilike '%".$filtros['suministro']."%' ";
			if($filtros['suministro_orden'] != '0'){
				$str_where .= " AND orden>=".$filtros['suministro_orden'];
			}else{
				$str_where .= " AND orden<".$filtros['suministro_orden'];
				// Para que no encuentre resultados en caso que suministro sea cero.
			}
			if(isset($filtros['sector']) && $filtros['sector']!=0 && $filtros['sector']!=''){
				$str_where .= " AND sector = '".$filtros['sector']."'";
			}
			if(isset($filtros['ruta']) && $filtros['ruta']!=0 && $filtros['ruta']!=''){
				$str_where .= " AND ruta = '".$filtros['ruta']."'";
			}
			$bool_inconsistencia = false;
		}
		// Listar por id_xml
		if(isset($filtros['id_xml'])){
			$str_where .= " AND comlec_xml_id = '".$filtros['id_xml']."' ";
		}
		
		if ( isset($search) && $search!=''){
			$str_where .= " AND (";
			$str_where .= " CAST(suministro AS text) ilike '%".$search."%' ";
			$str_where .= " ) ";
		}
		
		if ( isset($filter_advance) && $filter_advance!='0' ){
			$arr_filters = explode(',',$filter_advance);
			
			foreach ($arr_filters as $k => $filter){
				if($filter=='LSC'){//Pendiente
					$str_where .= " AND (lecturao1 IS NULL OR lecturao1='') AND (obs1 IS NULL OR obs1='') ";
				}
				if($filter=='OBST'){//Observaciones total
					$str_where .= " AND (obs1 IS NOT NULL AND obs1<>'') ";
				}
				if($filter=='OBSSL'){//Observaciones sin lectura
					$str_where .= " AND (lecturao1 IS NULL OR lecturao1='') AND (obs1 IS NOT NULL AND obs1<>'') ";
				}
				if($filter=='OBSCL'){//Observaciones con lectura
					$str_where .= " AND (lecturao1 IS NOT NULL AND lecturao1<>'') AND (obs1 IS NOT NULL AND obs1<>'') ";
				}
				if($filter=='OBS99'){//Observaciones 99
					$str_where .= " AND obs1 = '99' ";
				}
				if($filter=='LAMA'){//Lectura Actual Menor que la Anterior
					$str_where .= " AND (lecturao1<>'' AND CAST(lecturao1 AS numeric) < lecant) ";
				}
				if($filter=='CFR'){//Consumo Fuera de Rango
					$str_join .= " left join lecturas.comlec_lecturametodo lm on ol.comlec_ordenlectura_id=lm.comlec_ordenlectura_id and ol.metodofinal1=lm.comlec_metodo_id ";
					$str_fields .= ",minima,maxima";
					$str_where .= " AND (montoconsumo1 < minima OR montoconsumo1 > maxima) ";
				}
				if($filter=='NS'){//Nuevo Suministro
					//$str_where .= " AND pinicio = pfactura ";
				}
				if($filter=='OBSSC'){//Con Observacion sin correccion
					$str_where .= " AND (obs1 IS NOT NULL AND obs1<>'') AND validacion = '0' ";
				}
				if($filter=='CORR'){//Corregidas
					$str_where .= " AND validacion >= '1' ";
				}
				if($filter=='I'){//Inconsistentes
					$str_where .= " AND resultadoevaluacion = 'INCONSISTENTE' ";
				}
				if($filter=='CONS'){//Consistentes
					$str_where .= " AND resultadoevaluacion = 'CONSISTENTE' ";
				}
				if($filter=='CC'){//Consumo cero
					$str_where .= " AND montoconsumo1 = 0 ";
				}
				if($filter=='C50MA'){//Consumo 50% menor que el anterior
					$str_where .= " AND montoconsumo1 < (consant / 2) ";
				}
				if($filter=='C100MA'){//Consumo 100% mayor que el anterior
					$str_where .= " AND montoconsumo1 > (consant * 2) ";
				}
				if($filter=='CN'){//Clientes Nuevos
					$str_where .= " AND pinicio = pfactura ";
				}
				if($filter=='PED'){//Pendientes de Envio a Distriluz
					$str_where .= " AND ws_l_estado='0' AND resultadoevaluacion = 'CONSISTENTE' ";
				}

				$bool_inconsistencia = false;
			}
		}
		
		if ( isset($consumo_min) ){
			$str_where .= " AND montoconsumo1 >= $consumo_min ";
			$bool_inconsistencia = false;
		}
		
		if ( isset($consumo_max) ){
			$str_where .= " AND montoconsumo1 <= $consumo_max ";
			$bool_inconsistencia = false;
		}
		
		if($bool_inconsistencia){
			$str_where .= " AND (resultadoevaluacion IS NULL OR resultadoevaluacion = 'INCONSISTENTE' OR  resultadoevaluacion = '') ";
		}
			
		$sql="select ".$str_fields." from ".$esquema.".comlec_ordenlecturas ol ".$str_join." where 1=1";
		
		//echo $sql.$str_where.$str_order.$str_limit;exit;
		return  $db->query($sql.$str_where.$str_order.$str_limit);
    }
    
    public function listarInconsistenciasTac($criterio,$filtros=null,$filter_advance,$page,$limit,$search=null,$consumo_min=null,$consumo_max=null){
    	ini_set('memory_limit', '-1');
    	$this->setDateSourceCustom ();$db = $this->getDataSource();
    	$esquema=$this->esquema;
    	$bool_inconsistencia = true;
    
    	$str_fields = 'validacion, tipolectura, comlec_ordenlectura_id, consant, orden, libro,lcorrelati, seriefab,cliente,suministro,lecturaant1px as lap, null as lae,lecturao1,lecturao2, lecturao3, obsantpx as obs_ant_px, obs1, obs2, lecant, promedio, montoconsumo1, resultadoevaluacion, foto1, foto2, foto3, e1.nombre as lecturista1, e2.nombre as lecturista2, metodofinal1, metodofinal2, metodofinal3, COUNT(*) OVER() as row_total';
    
    	if ( isset( $page ) && isset($limit) ){
    		$str_order = " ORDER BY orden, ordenruta";
    		$str_limit = " LIMIT ".intval( $limit )." OFFSET ".intval( $page );
    	}else{
    		$str_order = " ORDER BY orden, ordenruta";
    		$str_limit = "";
    	}
    
    	$str_join = " left join glomas_empleados e1 on ol.lecturista1_id = e1.id ";
		$str_join .= " left join glomas_empleados e2 on ol.lecturista2_id = e2.id ";
    	 
    	$sql="select ".$str_fields." from ".$esquema.".comlec_ordenlecturas ol ".$str_join." where 1=1";
    	
    	$str_where = " ";
    	if(strpos($criterio, 'F') !== false){
    		$str_where .= " AND cast(ol.fechaasignado1 as date)  >= '".$filtros['date_start']."' AND cast(ol.fechaasignado1 as date)  <= '".$filtros['date_end']." 23:59:59'";
    	}
    	if(strpos($criterio, 'U') !== false){
    		if(isset($filtros['unidadneg']) && $filtros['unidadneg']!=0 && $filtros['unidadneg']!=''){
    			$str_where .= " AND glomas_unidadnegocio_id = '".$filtros['unidadneg']."'";
    		}
    		if(isset($filtros['ciclo']) && $filtros['ciclo']!=0 && $filtros['ciclo']!=''){
    			$str_where .= " AND idciclo = '".$filtros['ciclo']."'";
    		}
    		if(isset($filtros['sector']) && $filtros['sector']!=0 && $filtros['sector']!=''){
    			$str_where .= " AND sector = '".$filtros['sector']."'";
    		}
    	}
    	if(strpos($criterio, 'L') !== false){
    		$str_where .= " AND lecturista1_id = '".$filtros['lecturista']."' ";
    	}
    	if(strpos($criterio, 'S') !== false){
    		//$str_where .= " AND CAST(suministro AS text) ilike '%".$filtros['suministro']."%' ";
    		if($filtros['suministro_orden'] != '0'){
    			$str_where .= " AND orden>=".$filtros['suministro_orden'];
    		}else{
    			$str_where .= " AND orden<".$filtros['suministro_orden'];
    			// Para que no encuentre resultados en caso que suministro sea cero.
    		}
    	}
    	// Listar por id_xml
    	if(isset($filtros['id_xml'])){
    		$str_where .= " AND comlec_xml_id = '".$filtros['id_xml']."' ";
    	}
    	
    	if ( isset($search) && $search!=''){
    		$str_where .= " AND (";
    		$str_where .= " CAST(suministro AS text) ilike '%".$search."%' ";
    		$str_where .= " ) ";
    	}
    	
    	/*
    	if ($search){
    		$str_where = " AND (";
    		$str_where .= " lcorrelati >=".$search;
    		$str_where .= " ) ";
    	} else {
    		$str_where = "";
    	}
    
    	if($sector == 0){
    		if($suministro_id != ''){
				if($suministro_id != '0'){
					$str_where .= " AND orden>=".$suministro_id;
				}else{
					$str_where .= " AND orden<".$suministro_id;
					// Para que no encuentre resultados en caso que suministro sea cero.
				}
			}else{
				$str_where .= " ";
			}
    	}else{
    		if($lecturista != 0){
    			$str_where .= " AND sector='$sector' AND lecturista1_id=".$lecturista;
    		}else{
    			if($suministro_id != ''){
    				$str_where .= " AND sector='$sector' AND orden>=".$suministro_id;
    			}else{
    				$str_where .= " AND sector='$sector'";
    			}
    		}
    	}
    	*/
    
    if ( isset($filter_advance) && $filter_advance!='0' ){
			$arr_filters = explode(',',$filter_advance);
			
    	foreach ($arr_filters as $k => $filter){
				if($filter=='LSC'){//Pendiente
					$str_where .= " AND (lecturao1 IS NULL OR lecturao1='') AND (obs1 IS NULL OR obs1='') ";
				}
				if($filter=='OBSSL'){//Observaciones sin lectura
					$str_where .= " AND (lecturao1 IS NULL OR lecturao1='') AND (obs1 IS NOT NULL AND obs1<>'') ";
				}
				if($filter=='OBSCL'){//Observaciones con lectura
					$str_where .= " AND (lecturao1 IS NOT NULL AND lecturao1<>'') AND (obs1 IS NOT NULL AND obs1<>'') ";
				}
				if($filter=='OBS99'){//Observaciones 99
					$str_where .= " AND obs1 = '99' ";
				}
				if($filter=='LAMA'){//Lectura Actual Menor que la Anterior
					$str_where .= " AND (lecturao1<>'' AND CAST(lecturao1 AS numeric) < lecant) ";
				}
				if($filter=='CFR'){//Consumo Fuera de Rango
					$str_where .= " AND (montoconsumo1 < minima AND montoconsumo1 > maxima) ";
				}
				if($filter=='NS'){//Nuevo Suministro
					//$str_where .= " AND pinicio = pfactura ";
				}
				if($filter=='OBSSC'){//Con Observacion sin correccion
					$str_where .= " AND (obs1 IS NOT NULL AND obs1<>'') AND validacion = '0' ";
				}
				if($filter=='CORR'){//Corregidas
					$str_where .= " AND validacion = '1' ";
				}
				if($filter=='I'){//Inconsistentes
					$str_where .= " AND resultadoevaluacion = 'INCONSISTENTE' ";
				}
				if($filter=='CC'){//Consumo cero
					$str_where .= " AND montoconsumo1 = 0 ";
				}
				if($filter=='C50MA'){//Consumo 50% menor que el anterior
					$str_where .= " AND montoconsumo1 <= (consant / 2) ";
				}
				if($filter=='C100MA'){//Consumo 100% mayor que el anterior
					$str_where .= " AND montoconsumo1 >= (consant * 2) ";
				}
				if($filter=='CN'){//Clientes Nuevos
					$str_where .= " AND pinicio = pfactura ";
				}

				$bool_inconsistencia = false;
			}
		}
		
		if ( isset($consumo_min) ){
			$str_where .= " AND montoconsumo1 >= $consumo_min ";
			$bool_inconsistencia = false;
		}
		
		if ( isset($consumo_max) ){
			$str_where .= " AND montoconsumo1 <= $consumo_max ";
			$bool_inconsistencia = false;
		}
    
    	if($bool_inconsistencia){
    		$str_where .= " AND (resultadoevaluacion IS NULL OR resultadoevaluacion = 'INCONSISTENTE' OR  resultadoevaluacion = '') ";
    	}
    	//echo $sql.$str_where.$str_order.$str_limit;
    	return  $db->query($sql.$str_where.$str_order.$str_limit);
    }
    
	
    public function listarInconsistenciasExtranetCix($sector,$suministro_id,$lecturista,$page,$limit,$search){
    	ini_set('memory_limit', '-1');
    	$this->setDateSourceCustom ();$db = $this->getDataSource();
    	$esquema=$this->esquema;
    
    	$str_fields = 'consant, orden,seriefab,cliente,suministro,lecturaant1px as lap, null as lae,lecturao1,lecturao2, lecturao3, obsantpx as obs_ant_px, obs1, obs2, lecant, promedio, montoconsumo1, resultadoevaluacion, COUNT(*) OVER() as row_total';
    
    	if ( isset( $page ) && isset($limit) ){
    		$str_order = " ORDER BY orden, ordenruta";
    		$str_limit = " LIMIT ".intval( $limit )." OFFSET ".intval( $page );
    	}else{
    		$str_order = " ORDER BY orden, ordenruta";
    		$str_limit = "";
    	}
    
    	$sql="select ".$str_fields." from ".$esquema.".comlec_ordenlecturas where resultadoevaluacion ='CONSISTENTE'";
    	//$sql="select ruta, orden, cliente, suministro, seriefab, direccion from lecturas.comlec_ordenlecturas where resultadoevaluacion='INCONSISTENTE' and sector='$sector' and lecturista_id = '$lecturista' order by orden, ordenruta";
    	if ( isset($search) ){
    		$str_where = " AND (";
    		//$str_where .= " CAST(orden AS text) ilike '%".$search."%' ";
    		$str_where .= " OR CAST(suministro AS text) ilike '%".$search."%' ";
    		//$str_where .= " OR orden>=".$search;
    		$str_where .= " ) ";
    	} else {
    		$str_where = "";
    	}
    
    	if($sector == 0){
    		if($suministro_id != ''){
    			$str_where = " AND orden>=".$suministro_id;
    		}else{
    			$str_where = " ";
    		}
    	}else{
    		if($lecturista != 0){
    			$str_where = " AND sector='$sector' AND lecturista1_id=".$lecturista;
    		}else{
    			if($suministro_id != ''){
    				$str_where = " AND sector='$sector' AND orden>=".$suministro_id;
    			}else{
    				$str_where = " AND sector='$sector'";
    			}
    		}
    	}

    	return  $db->query($sql.$str_where.$str_order.$str_limit);
    }
    
    public function listarInconsistenciasExtranetTac($sector,$suministro_id,$lecturista,$page,$limit,$search){
    	ini_set('memory_limit', '-1');
    	$this->setDateSourceCustom ();$db = $this->getDataSource();
    	$esquema=$this->esquema;
    
    	$str_fields = 'consant,lcorrelati , orden,seriefab,cliente,suministro,lecturaant1px as lap, null as lae,lecturao1,lecturao2, lecturao3, obsantpx as obs_ant_px, obs1, obs2, lecant, promedio, montoconsumo1, resultadoevaluacion, COUNT(*) OVER() as row_total';
    
    	if ( isset( $page ) && isset($limit) ){
    		$str_order = " ORDER BY orden, ordenruta";
    		$str_limit = " LIMIT ".intval( $limit )." OFFSET ".intval( $page );
    	}else{
    		$str_order = " ORDER BY orden, ordenruta";
    		$str_limit = "";
    	}
    
    	$sql="select ".$str_fields." from ".$esquema.".comlec_ordenlecturas where resultadoevaluacion ='CONSISTENTE'";
    	//$sql="select ruta, orden, cliente, suministro, seriefab, direccion from lecturas.comlec_ordenlecturas where resultadoevaluacion='INCONSISTENTE' and sector='$sector' and lecturista_id = '$lecturista' order by orden, ordenruta";
    	if ( isset($search) and  $search != ''){
    		$str_where = " AND (";
    		$str_where .= " lcorrelati>=".$search;
    		$str_where .= " ) ";
    	} else {
    		$str_where = "";
    	}
    
    	if($sector == 0){
    		if($suministro_id != ''){
    			$str_where .= " AND orden>=".$suministro_id;
    		}else{
    			$str_where .= " ";
    		}
    	}else{
    		if($lecturista != 0){
    			$str_where .= " AND sector='$sector' AND lecturista1_id=".$lecturista;
    		}else{
    			if($suministro_id != ''){
    				$str_where .= " AND sector='$sector' AND orden>=".$suministro_id;
    			}else{
    				$str_where .= " AND sector='$sector'";
    			}
    		}
    	}
    
    	return  $db->query($sql.$str_where.$str_order.$str_limit);
    }
    
	public function listarInconsistenciasPdf($sector, $lecturista){
    	$this->setDateSourceCustom ();$db = $this->getDataSource();
    	
    	if($sector == 0){
    		if($lecturista== 0){
    			$sql="select ruta, orden, cliente, direccion, suministro, seriefab from lecturas.comlec_ordenlecturas where tipolectura = 'R' AND  (resultadoevaluacion IS NULL OR resultadoevaluacion = 'INCONSISTENTE' OR  resultadoevaluacion = '') order by orden, ordenruta";
    		}else{    			
    			$sql="select ruta, orden, cliente, direccion, suministro, seriefab from lecturas.comlec_ordenlecturas where tipolectura = 'R' and lecturista2_id = '$lecturista' AND  (resultadoevaluacion IS NULL OR resultadoevaluacion = 'INCONSISTENTE' OR  resultadoevaluacion = '') order by orden, ordenruta";
    		}    		
    	}else{    		
    		if($lecturista== 0){
    			$sql="select ruta, orden, cliente, direccion, suministro, seriefab from lecturas.comlec_ordenlecturas where tipolectura = 'R' and sector='$sector'  AND  (resultadoevaluacion IS NULL OR resultadoevaluacion = 'INCONSISTENTE' OR  resultadoevaluacion = '') order by orden, ordenruta";
    		}else{
    			$sql="select ruta, orden, cliente, direccion, suministro, seriefab from lecturas.comlec_ordenlecturas where tipolectura = 'R' and sector='$sector'  and lecturista2_id = '$lecturista' AND  (resultadoevaluacion IS NULL OR resultadoevaluacion = 'INCONSISTENTE' OR  resultadoevaluacion = '') order by orden, ordenruta";
    		}    		
    	}

    	return  $db->query($sql);
    	
    }

    /**
    * Get listado de digitacion de lecturas
    * @return Array Data
    * @author Geynen
    * @version 11 Junio 2014
    **/
    public function listaDigitarLectura2Cix($tipolectura_id,$sector_id,$ruta_id,$lecturista_id,$lectespecial,$suministro_orden,$page=1,$limit=20,$search=null){
        ini_set('memory_limit', '-1');
        $this->setDateSourceCustom ();$db = $this->getDataSource();
        $esquema=$this->esquema;
        
        $str_fields = 'comlec_ordenlectura_id,orden,libro,cliente,seriefab,suministro,direccion,null as lecantant,lecant,lecturao1,obs1,montoconsumo1,lecturao2,obs2,montoconsumo2,lecturao3,obs3,montoconsumo3,resultadoevaluacion,promedio, e.nombre as lecturista, ws_l_estado, COUNT(*) OVER() as row_total';
        
        if ( isset( $page ) && isset($limit) ){
            $str_order = " ORDER BY orden, ordenruta";
            $str_limit = " LIMIT ".intval( $limit )." OFFSET ".intval( $page );
        }else{
            $str_order = " ORDER BY orden, ordenruta";
            $str_limit = "";
        }
        $str_where = '';
        
        if($tipolectura_id=='L'){
        	$str_join = " left join lecturas.glomas_empleados e on ol.lecturista1_id = e.id ";
        }else{
        	$str_join = " left join lecturas.glomas_empleados e on ol.lecturista2_id = e.id ";
        }
                
        $sql="select ".$str_fields." from ".$esquema.".comlec_ordenlecturas ol ".$str_join." where 1=1";
        
        if ( isset($search) && $search!=''){
                    $str_where = " AND (";
                    $str_where .= " comlec_ordenlectura_id ilike '%".$search."%' ";
                    $str_where .= " OR CAST(orden AS text) ilike '%".$search."%' ";
                    $str_where .= " OR libro ilike '%".$search."%' ";
                    $str_where .= " OR cliente ilike '%".$search."%' ";
                    $str_where .= " OR seriefab ilike '%".$search."%' ";
                    $str_where .= " OR suministro ilike '%".$search."%' ";
                    $str_where .= " OR direccion ilike '%".$search."%' ";
                    $str_where .= " OR cast(lecant as text) ilike '%".$search."%' ";
                    if (isset($tipolectura_id) && $tipolectura_id=='L'){
                        $str_where .= " OR cast(lecturao1 as text) ilike '%".$search."%' ";
                        $str_where .= " OR obs1 ilike '%".$search."%' ";
                        $str_where .= " OR cast(montoconsumo1 as text) ilike '%".$search."%' ";
                    }elseif(isset($tipolectura_id) && $tipolectura_id=='R'){
                        $str_where .= " OR cast(lecturao2 as text) ilike '%".$search."%' ";
                        $str_where .= " OR obs2 ilike '%".$search."%' ";
                        $str_where .= " OR cast(montoconsumo2 as text) ilike '%".$search."%' ";
                    }elseif(isset($tipolectura_id) && $tipolectura_id=='RR'){
                        $str_where .= " OR cast(lecturao3 as text) ilike '%".$search."%' ";
                        $str_where .= " OR obs3 ilike '%".$search."%' ";
                        $str_where .= " OR cast(montoconsumo3 as text) ilike '%".$search."%' ";
                    }
                    $str_where .= " OR resultadoevaluacion ilike '%".$search."%' ";
                    $str_where .= " OR cast(promedio as text) ilike '%".$search."%' ";
                    $str_where .= " ) ";
        } else {
            $str_where = "";
        }

        if ( isset($tipolectura_id)  ){
                    $str_where .= " AND tipolectura = '".$tipolectura_id."' ";
        }

        if ( isset($sector_id) && $sector_id!=0 ) {
                    $str_where .= " AND sector = '".$sector_id."' ";
        }

        if ( isset($ruta_id) && $ruta_id!=0 ){
                    $str_where .= " AND ruta = '".$ruta_id."' ";
        }
        
        if ( isset($lecturista_id) && $lecturista_id!=0 ){
        	if($tipolectura_id=='L'){
        		$str_where .= " AND lecturista1_id = '".$lecturista_id."' ";
        	}else{
        		$str_where .= " AND lecturista2_id = '".$lecturista_id."' ";
        	}
        }

        if ( isset($lectespecial) && $lectespecial!='0' ){
            if($lectespecial=='SA'){
                if (isset($tipolectura_id) && $tipolectura_id=='L'){
                    $str_where .= " AND lecturista1_id IS NULL ";
                }elseif(isset($tipolectura_id) && $tipolectura_id=='R'){
                    $str_where .= " AND lecturista2_id IS NULL ";
                }elseif(isset($tipolectura_id) && $tipolectura_id=='RR'){
                    $str_where .= " AND lecturista3_id IS NULL ";
                }
            }elseif($lectespecial=='SR'){
                if (isset($tipolectura_id) && $tipolectura_id=='L'){
                    $str_where .= " AND lecturao1 IS NULL ";
                }elseif(isset($tipolectura_id) && $tipolectura_id=='R'){
                    $str_where .= " AND lecturao2 IS NULL ";
                }elseif(isset($tipolectura_id) && $tipolectura_id=='RR'){
                    $str_where .= " AND lecturao3 IS NULL ";
                }
            }elseif($lectespecial=='SC'){
                $str_where .= " AND resultadoevaluacion IS NULL ";
            }
        }
        
        if($suministro_orden != ''){
        	$str_where .= " AND orden>=".$suministro_orden;
        }else{
        	$str_where .= " ";
        }

        //echo $sql.$str_where.$str_order.$str_limit;

        return  $db->query($sql.$str_where.$str_order.$str_limit);

    }
    
    /**
     * Get listado de digitacion de lecturas
     * @return Array Data
     * @author Geynen
     * @version 11 Junio 2014
     **/
    public function listaDigitarLectura2Tac($tipolectura_id,$sector_id,$ruta_id,$lecturista_id,$lectespecial,$suministro_id,$page=1,$limit=20,$search){
    	ini_set('memory_limit', '-1');
    	$this->setDateSourceCustom ();$db = $this->getDataSource();
    	$esquema=$this->esquema;
    
    	$str_fields = 'comlec_ordenlectura_id,lcorrelati,orden,libro,cliente,seriefab,suministro,direccion,null as lecantant,lecant,lecturao1,obs1,montoconsumo1,lecturao2,obs2,montoconsumo2,lecturao3,obs3,montoconsumo3,resultadoevaluacion,promedio, e.nombre as lecturista, COUNT(*) OVER() as row_total';
    
    	if ( isset( $page ) && isset($limit) ){
    		$str_order = " ORDER BY orden, ordenruta";
    		$str_limit = " LIMIT ".intval( $limit )." OFFSET ".intval( $page );
    	}else{
    		$str_order = " ORDER BY orden, ordenruta";
    		$str_limit = "";
    	}
    	$str_where = '';
    
    	if($tipolectura_id=='L'){
    		$str_join = " left join glomas_empleados e on ol.lecturista1_id = e.id ";
    	}else{
    		$str_join = " left join glomas_empleados e on ol.lecturista2_id = e.id ";
    	}
    	
    	$sql="select ".$str_fields." from ".$esquema.".comlec_ordenlecturas ol ".$str_join." where 1=1";
    	if ( isset($search) and $search != ''){
    		$str_where = " AND lcorrelati >= ".$search;
    		/*  $str_where .= " OR (";
    		 $str_where .= " CAST(orden AS text) ilike '%".$search."%' ";
    				$str_where .= " OR libro ilike '%".$search."%' ";
    				$str_where .= " OR cliente ilike '%".$search."%' ";
    				$str_where .= " OR seriefab ilike '%".$search."%' ";
    				$str_where .= " OR suministro ilike '%".$search."%' ";
    				$str_where .= " OR direccion ilike '%".$search."%' ";
    				$str_where .= " OR cast(lecant as text) ilike '%".$search."%' ";
    				if (isset($tipolectura_id) && $tipolectura_id=='L'){
    				$str_where .= " OR cast(lecturao1 as text) ilike '%".$search."%' ";
    				$str_where .= " OR obs1 ilike '%".$search."%' ";
    				$str_where .= " OR cast(montoconsumo1 as text) ilike '%".$search."%' ";
    				}elseif(isset($tipolectura_id) && $tipolectura_id=='R'){
    				$str_where .= " OR cast(lecturao2 as text) ilike '%".$search."%' ";
    				$str_where .= " OR obs2 ilike '%".$search."%' ";
    				$str_where .= " OR cast(montoconsumo2 as text) ilike '%".$search."%' ";
    				}elseif(isset($tipolectura_id) && $tipolectura_id=='RR'){
    				$str_where .= " OR cast(lecturao3 as text) ilike '%".$search."%' ";
    				$str_where .= " OR obs3 ilike '%".$search."%' ";
    				$str_where .= " OR cast(montoconsumo3 as text) ilike '%".$search."%' ";
    				}
    				$str_where .= " OR resultadoevaluacion ilike '%".$search."%' ";
    				$str_where .= " OR cast(promedio as text) ilike '%".$search."%' ";
    				$str_where .= " ) ";*/
    	} else {
    		$str_where = "";
    	}
    
    	if ( isset($tipolectura_id)  ){
    		$str_where .= " AND tipolectura = '".$tipolectura_id."' ";
    	}
    
    	if ( isset($sector_id) && $sector_id!=0 ) {
    		$str_where .= " AND sector = '".$sector_id."' ";
    	}
    
    	if ( isset($ruta_id) && $ruta_id!=0 ){
    		$str_where .= " AND ruta = '".$ruta_id."' ";
    	}
    	
    	if ( isset($lecturista_id) && $lecturista_id!=0 ){
    		if($tipolectura_id=='L'){
    			$str_where .= " AND lecturista1_id = '".$lecturista_id."' ";
    		}else{
    			$str_where .= " AND lecturista2_id = '".$lecturista_id."' ";
    		}
    	}
    
    	if ( isset($lectespecial) && $lectespecial!=0 ){
    		if($lectespecial=='SA'){
    			if (isset($tipolectura_id) && $tipolectura_id=='L'){
    				$str_where .= " AND lecturista1_id IS NULL ";
    			}elseif(isset($tipolectura_id) && $tipolectura_id=='R'){
    				$str_where .= " AND lecturista2_id IS NULL ";
    			}elseif(isset($tipolectura_id) && $tipolectura_id=='RR'){
    				$str_where .= " AND lecturista3_id IS NULL ";
    			}
    		}elseif($lectespecial=='SR'){
    			if (isset($tipolectura_id) && $tipolectura_id=='L'){
    				$str_where .= " AND lecturao1 IS NULL ";
    			}elseif(isset($tipolectura_id) && $tipolectura_id=='R'){
    				$str_where .= " AND lecturao2 IS NULL ";
    			}elseif(isset($tipolectura_id) && $tipolectura_id=='RR'){
    				$str_where .= " AND lecturao3 IS NULL ";
    			}
    		}elseif($lectespecial=='SC'){
    			$str_where .= " AND resultadoevaluacion IS NULL ";
    		}
    	}
    	
    	if($suministro_id != ''){
    		$str_where .= " AND orden>=".$suministro_id;
    	}else{
    		$str_where .= " ";
    	}
    
    	//echo $sql.$str_where.$str_order.$str_limit;
    
    	return  $db->query($sql.$str_where.$str_order.$str_limit);
    
    }
    
    
    public function passlectura($lectura,$suministro){
    	$this->setDateSourceCustom ();$db = $this->getDataSource();
    	$sql="select lecturas.insertar_lecturao_inconsistencia(".$lectura.",'".$suministro."')";
    	$db->query($sql);
        $sqlretorno="select montoconsumo1, lecturao1, obs1, resultadoevaluacion from lecturas.comlec_ordenlecturas where suministro='".$suministro."'";
        return $db->query($sqlretorno);
    }
    
    public function listarlibrosPdf($libro, $ruta){
    	$this->setDateSourceCustom ();$db = $this->getDataSource();
    	$sql="SELECT cliente, ruta, orden, direccion, suministro, seriefab FROM lecturas.comlec_ordenlecturas WHERE libro = '$libro' and ruta = '$ruta' ORDER BY orden, ordenruta";
    	return  $db->query($sql);    	
    }
    
    public function listarLecturista($libro, $ruta){
    	$this->setDateSourceCustom ();$db = $this->getDataSource();
    	$sql="SELECT ruta, nombre, lecturista1_id FROM lecturas.comlec_ordenlecturas  orl INNER JOIN glomas_empleados em  on orl.lecturista1_id = em.id WHERE libro = '$libro' and ruta = '$ruta' ORDER BY orden, ordenruta";
    	return  $db->query($sql);
    }
    
    public function cambiarCodigoLecturas($observacion, $suministro){
    	$this->setDateSourceCustom ();$db = $this->getDataSource();
    	$sql="UPDATE  lecturas.comlec_ordenlecturas SET obs1 = '$observacion' WHERE suministro = '$suministro'";
    	return  $db->query($sql);
    }
    
    public function buscarObservacionBySuministro($suministro){
    	$this->setDateSourceCustom ();$db = $this->getDataSource();
    	$sql="SELECT obs1 FROM lecturas.comlec_ordenlecturas WHERE suministro = '$suministro' LIMIT 1";
    	return  $db->query($sql);
    }
    
    public function getCountByFecha($options=null,$fecha=null){
    	$this->setDateSourceCustom();
    	$db = $this->getDataSource();
    	
    	$str_where = '';
    	if(isset($options) && is_array($options)){
    		if(isset($options['unidad_neg']) and $options['unidad_neg']!=0){
    			$str_where .= ' AND glomas_unidadnegocio_id = '.$options['unidad_neg'];
    		}
	    	if(isset($options['idciclo']) && $options['idciclo']!='0'){
	    		$str_where .= " AND idciclo='".$options['idciclo']."'";
	    	}
    	}
    
    	if(isset($fecha)){
    		$str_where .= " AND fechaasignado1 >= '".$fecha."'";
    	}else{
    		$str_where .= " AND fechaasignado1 >= '".date('Y-m-d')."'";
    	}
    	$schema = 'lecturas';
    
    	$sql="SELECT COUNT(*) as cantidad FROM ".$schema.".comlec_ordenlecturas WHERE 1=1 ".$str_where;

    	$result = $db->query($sql);
    	 
    	if($result){
    		return $result[0][0]['cantidad'];
    	}
    	return  0;
    }
    
    public function getCountAsignadosByFecha($options=null,$fecha=null){
    	$this->setDateSourceCustom();
    	$db = $this->getDataSource();
    	
    	$str_where = '';
    	if(isset($options) && is_array($options)){
    		if(isset($options['unidad_neg']) and $options['unidad_neg']!=0){
    			$str_where .= ' AND glomas_unidadnegocio_id = '.$options['unidad_neg'];
    		}
    		if(isset($options['idciclo']) && $options['idciclo']!='0'){
    			$str_where .= " AND idciclo='".$options['idciclo']."'";
    		}
    	}
    
    	if(isset($fecha)){
    		$str_where .= " AND fechaasignado1 >= '".$fecha."'";
    	}else{
    		$str_where .= " AND fechaasignado1 >= '".date('Y-m-d')."'";
    	}
    	$schema = 'lecturas';
    
    	$sql="SELECT COUNT(*) as cantidad FROM ".$schema.".comlec_ordenlecturas WHERE lecturista1_id is not null ".$str_where;
    
    	$result = $db->query($sql);
    
    	if($result){
    		return $result[0][0]['cantidad'];
    	}
    	return  0;
    }
    
    public function getCountDescargadosByFecha($options=null,$fecha=null){
    	$this->setDateSourceCustom();
    	$db = $this->getDataSource();
    
    	$str_where = '';
    	if(isset($options) && is_array($options)){
    		if(isset($options['unidad_neg']) and $options['unidad_neg']!=0){
    			$str_where .= ' AND glomas_unidadnegocio_id = '.$options['unidad_neg'];
    		}
    		if(isset($options['idciclo']) && $options['idciclo']!='0'){
    			$str_where .= " AND idciclo='".$options['idciclo']."'";
    		}
    	}
    	
    	if(isset($fecha)){
    		$str_where .= " AND fechaasignado1 >= '".$fecha."'";
    	}else{
    		$str_where .= " AND fechaasignado1 >= '".date('Y-m-d')."'";
    	}
    	$schema = 'lecturas';
    
    	$sql="SELECT COUNT(*) as cantidad FROM ".$schema.".comlec_ordenlecturas WHERE lecturista1_id is not null and down1 in ('1','2') ".$str_where;
    
    	$result = $db->query($sql);
    
    	if($result){
    		return $result[0][0]['cantidad'];
    	}
    	return  0;
    }
    
    public function getCountConsistentesByFecha($options=null,$fecha=null){
    	$this->setDateSourceCustom();
    	$db = $this->getDataSource();
    
    	$str_where = '';
    	if(isset($options) && is_array($options)){
    		if(isset($options['unidad_neg']) and $options['unidad_neg']!=0){
    			$str_where .= ' AND glomas_unidadnegocio_id = '.$options['unidad_neg'];
    		}
    		if(isset($options['idciclo']) && $options['idciclo']!='0'){
    			$str_where .= " AND idciclo='".$options['idciclo']."'";
    		}
    	}
    	
    	if(isset($fecha)){
    		$str_where .= " AND fechaejecucion1 >= '".$fecha."'";
    	}else{
    		$str_where .= " AND fechaejecucion1 >= '".date('Y-m-d')."'";
    	}
    	$schema = 'lecturas';
    
    	$sql="SELECT COUNT(*) as cantidad FROM ".$schema.".comlec_ordenlecturas WHERE resultadoevaluacion='CONSISTENTE' ".$str_where;
    
    	$result = $db->query($sql);
    
    	if($result){
    		return $result[0][0]['cantidad'];
    	}
    	return  0;
    }
    
    public function getCountInconsistentesEvaluadasByFecha($options=null,$fecha=null){
    	$this->setDateSourceCustom();
    	$db = $this->getDataSource();
    
    	$str_where = '';
    	if(isset($options) && is_array($options)){
    		if(isset($options['unidad_neg']) and $options['unidad_neg']!=0){
    			$str_where .= ' AND glomas_unidadnegocio_id = '.$options['unidad_neg'];
    		}
    		if(isset($options['idciclo']) && $options['idciclo']!='0'){
    			$str_where .= " AND idciclo='".$options['idciclo']."'";
    		}
    	}
    	
    	if(isset($fecha)){
    		$str_where .= " AND fechaejecucion1 >= '".$fecha."'";
    	}else{
    		$str_where .= " AND fechaejecucion1 >= '".date('Y-m-d')."'";
    	}
    	$schema = 'lecturas';
    
    	$sql="SELECT COUNT(*) as cantidad FROM ".$schema.".comlec_ordenlecturas WHERE validacion>='1' ".$str_where;
    
    	$result = $db->query($sql);
    
    	if($result){
    		return $result[0][0]['cantidad'];
    	}
    	return  0;
    }
    
    public function getCountInconsistentesActualesByFecha($options=null,$fecha=null){
    	$this->setDateSourceCustom ();$db = $this->getDataSource();
    
    	$str_where = '';
    	if(isset($options) && is_array($options)){
    		if(isset($options['unidad_neg']) and $options['unidad_neg']!=0){
    			$str_where .= ' AND glomas_unidadnegocio_id = '.$options['unidad_neg'];
    		}
    		if(isset($options['idciclo']) && $options['idciclo']!='0'){
    			$str_where .= " AND idciclo='".$options['idciclo']."'";
    		}
    	}
    	
    	if(isset($fecha)){
    		$str_where .= " AND fechaejecucion1 >= '".$fecha."'";
    	}else{
    		$str_where .= " AND fechaejecucion1 >= '".date('Y-m-d')."'";
    	}
    	$schema = 'lecturas';
    
    	$sql="SELECT COUNT(*) as cantidad FROM ".$schema.".comlec_ordenlecturas WHERE resultadoevaluacion='INCONSISTENTE' ".$str_where;
    
    	$result = $db->query($sql);
    
    	if($result){
    		return $result[0][0]['cantidad'];
    	}
    	return  0;
    }
    
    public function getCountAvanzadosByFecha($options=null,$fecha=null){
    	$this->setDateSourceCustom();
    	$db = $this->getDataSource();
    
    	$str_where = '';
    	if(isset($options) && is_array($options)){
    		if(isset($options['unidad_neg']) and $options['unidad_neg']!=0){
    			$str_where .= ' AND glomas_unidadnegocio_id = '.$options['unidad_neg'];
    		}
    		if(isset($options['idciclo']) && $options['idciclo']!='0'){
    			$str_where .= " AND idciclo='".$options['idciclo']."'";
    		}
    	}
    	
    	if(isset($fecha)){
    		$str_where .= " AND fechaejecucion1 >= '".$fecha."'";
    	}else{
    		$str_where .= " AND fechaejecucion1 >= '".date('Y-m-d')."'";
    	}
    	$schema = 'lecturas';
    
    	$sql="SELECT COUNT(*) as cantidad FROM ".$schema.".comlec_ordenlecturas WHERE (resultadoevaluacion='CONSISTENTE' OR resultadoevaluacion='INCONSISTENTE') ".$str_where;
    	    
    	$result = $db->query($sql);
    
    	if($result){
    		return $result[0][0]['cantidad'];
    	}
    	return  0;
    }
    
    public function getCountObs1ByFecha($options=null,$fecha=null){
    	$this->setDateSourceCustom();
    	$db = $this->getDataSource();
    
    	$str_where = '';
    	if(isset($options) && is_array($options)){
    		if(isset($options['unidad_neg']) and $options['unidad_neg']!=0){
    			$str_where .= ' AND glomas_unidadnegocio_id = '.$options['unidad_neg'];
    		}
    		if(isset($options['idciclo']) && $options['idciclo']!='0'){
    			$str_where .= " AND idciclo='".$options['idciclo']."'";
    		}
    	}
    	
    	if(isset($fecha)){
    		$str_where .= " AND fechaejecucion1 >= '".$fecha."'";
    	}else{
    		$str_where .= " AND fechaejecucion1 >= '".date('Y-m-d')."'";
    	}
    	$schema = 'lecturas';
    
    	$sql="SELECT COUNT(obs1) as cantidad FROM ".$schema.".comlec_ordenlecturas WHERE obs1 is not null and obs1 <> '' and obs1 <> 'NULL' and obs1<>'0' ".$str_where;
    
    	$result = $db->query($sql);
    
    	if($result){
    		return $result[0][0]['cantidad'];
    	}
    	return  0;
    }
    
    public function getCountObs1SinLecturaByFecha($options=null,$fecha=null){
    	$this->setDateSourceCustom();
    	$db = $this->getDataSource();
    
    	$str_where = '';
    	if(isset($options) && is_array($options)){
    		if(isset($options['unidad_neg']) and $options['unidad_neg']!=0){
    			$str_where .= ' AND glomas_unidadnegocio_id = '.$options['unidad_neg'];
    		}
    		if(isset($options['idciclo']) && $options['idciclo']!='0'){
    			$str_where .= " AND idciclo='".$options['idciclo']."'";
    		}
    	}
    	
    	if(isset($fecha)){
    		$str_where .= " AND fechaejecucion1 >= '".$fecha."'";
    	}else{
    		$str_where .= " AND fechaejecucion1 >= '".date('Y-m-d')."'";
    	}
    	$schema = 'lecturas';
    
    	$sql="SELECT COUNT(obs1) as cantidad FROM ".$schema.".comlec_ordenlecturas WHERE obs1 is not null and obs1 <> '' and obs1 <> 'NULL' and obs1<>'0' and (lecturao1='' or lecturao1 is null) ".$str_where;
    
    	$result = $db->query($sql);
    
    	if($result){
    		return $result[0][0]['cantidad'];
    	}
    	return  0;
    }
    
    public function getCountObs1ConLecturaByFecha($options=null,$fecha=null){
    	$this->setDateSourceCustom();
    	$db = $this->getDataSource();
    
    	$str_where = '';
    	if(isset($options) && is_array($options)){
    		if(isset($options['unidad_neg']) and $options['unidad_neg']!=0){
    			$str_where .= ' AND glomas_unidadnegocio_id = '.$options['unidad_neg'];
    		}
    		if(isset($options['idciclo']) && $options['idciclo']!='0'){
    			$str_where .= " AND idciclo='".$options['idciclo']."'";
    		}
    	}
    	
    	if(isset($fecha)){
    		$str_where .= " AND fechaejecucion1 >= '".$fecha."'";
    	}else{
    		$str_where .= " AND fechaejecucion1 >= '".date('Y-m-d')."'";
    	}
    	$schema = 'lecturas';
    
    	$sql="SELECT COUNT(obs1) as cantidad FROM ".$schema.".comlec_ordenlecturas WHERE obs1 is not null and obs1 <> '' and obs1 <> 'NULL' and obs1<>'0' and lecturao1!='' ".$str_where;
    
    	$result = $db->query($sql);
    
    	if($result){
    		return $result[0][0]['cantidad'];
    	}
    	return  0;
    }
    
    public function getCountObs1IgualAByFecha($options=null,$obs=99,$fecha=null){
    	$this->setDateSourceCustom();
    	$db = $this->getDataSource();
    
    	$str_where = '';
    	if(isset($options) && is_array($options)){
    		if(isset($options['unidad_neg']) and $options['unidad_neg']!=0){
    			$str_where .= ' AND glomas_unidadnegocio_id = '.$options['unidad_neg'];
    		}
    		if(isset($options['idciclo']) && $options['idciclo']!='0'){
    			$str_where .= " AND idciclo='".$options['idciclo']."'";
    		}
    	}
    	
    	if(isset($fecha)){
    		$str_where .= " AND fechaejecucion1 >= '".$fecha."'";
    	}else{
    		$str_where .= " AND fechaejecucion1 >= '".date('Y-m-d')."'";
    	}
    	$schema = 'lecturas';
    
    	$sql="SELECT COUNT(*) as cantidad FROM ".$schema.".comlec_ordenlecturas WHERE obs1 ='".$obs."' ".$str_where;
    
    	$result = $db->query($sql);
    
    	if($result){
    		return $result[0][0]['cantidad'];
    	}
    	return  0;
    }
    
    public function getCountTiempoPromedioEjecucionByFecha($options=null,$fecha=null){
    	$this->setDateSourceCustom();
    	$db = $this->getDataSource();
    
    	$str_where = '';
    	if(isset($options) && is_array($options)){
    		if(isset($options['unidad_neg']) and $options['unidad_neg']!=0){
    			$str_where .= ' AND glomas_unidadnegocio_id = '.$options['unidad_neg'];
    		}
    		if(isset($options['idciclo']) && $options['idciclo']!='0'){
    			$str_where .= " AND idciclo='".$options['idciclo']."'";
    		}
    	}
    	
    	if(isset($fecha)){
    		$str_where .= " AND fechaejecucion1 >= '".$fecha."'";
    	}else{
    		$str_where .= " AND fechaejecucion1 >= '".date('Y-m-d')."'";
    	}
    	$schema = 'lecturas';
    
    	$sql="select avg(tiempo) as cantidad from (
    select *, age(F1, F2) as tiempo from (
    (select *, ROW_NUMBER() OVER() as I1 from (select fechaejecucion1 AS F1 from ".$schema.".comlec_ordenlecturas WHERE fechaejecucion1 is not null ".$str_where." order by fechaejecucion1 asc) as t1) as t11
    LEFT OUTER JOIN
    (select *, ROW_NUMBER() OVER() as I2 from (select fechaejecucion1 AS F2 from ".$schema.".comlec_ordenlecturas WHERE fechaejecucion1 is not null ".$str_where." order by fechaejecucion1 asc) as t2) as t22
    on t11.I1=t22.I2+1) as t3
    ) as t4";
    
    	$result = $db->query($sql);
    
    	if($result){
    		return $result[0][0]['cantidad'];
    	}
    	return  0;
    }
    
    public function getCountConsumoCeroByFecha($options=null,$fecha=null){
    	$this->setDateSourceCustom();
    	$db = $this->getDataSource();
    
    	$str_where = '';
    	if(isset($options) && is_array($options)){
    		if(isset($options['unidad_neg']) and $options['unidad_neg']!=0){
    			$str_where .= ' AND glomas_unidadnegocio_id = '.$options['unidad_neg'];
    		}
    		if(isset($options['idciclo']) && $options['idciclo']!='0'){
    			$str_where .= " AND idciclo='".$options['idciclo']."'";
    		}
    	}
    	
    	if(isset($fecha)){
    		$str_where .= " AND fechaejecucion1 >= '".$fecha."'";
    	}else{
    		$str_where .= " AND fechaejecucion1 >= '".date('Y-m-d')."'";
    	}
    	$schema = 'lecturas';
    
    	$sql="SELECT COUNT(*) as cantidad FROM ".$schema.".comlec_ordenlecturas WHERE montoconsumo1=0 ".$str_where;
    
    	$result = $db->query($sql);
    
    	if($result){
    		return $result[0][0]['cantidad'];
    	}
    	return  0;
    }
    
    public function getCountConsumoMenoresQueByFecha($options=null,$num=35,$fecha=null){
    	$this->setDateSourceCustom();
    	$db = $this->getDataSource();
    
    	$str_where = '';
    	if(isset($options) && is_array($options)){
    		if(isset($options['unidad_neg']) and $options['unidad_neg']!=0){
    			$str_where .= ' AND glomas_unidadnegocio_id = '.$options['unidad_neg'];
    		}
    		if(isset($options['idciclo']) && $options['idciclo']!='0'){
    			$str_where .= " AND idciclo='".$options['idciclo']."'";
    		}
    	}
    	
    	if(isset($fecha)){
    		$str_where .= " AND fechaejecucion1 >= '".$fecha."'";
    	}else{
    		$str_where .= " AND fechaejecucion1 >= '".date('Y-m-d')."'";
    	}
    	$schema = 'lecturas';
    
    	$sql="SELECT COUNT(*) as cantidad FROM ".$schema.".comlec_ordenlecturas WHERE montoconsumo1<".$num." ".$str_where;
    
    	$result = $db->query($sql);
    
    	if($result){
    		return $result[0][0]['cantidad'];
    	}
    	return  0;
    }
    
    public function getCountConsumoMayoresQueByFecha($options=null,$num=1000,$fecha=null){
    	$this->setDateSourceCustom();
    	$db = $this->getDataSource();
    
    	$str_where = '';
    	if(isset($options) && is_array($options)){
    		if(isset($options['unidad_neg']) and $options['unidad_neg']!=0){
    			$str_where .= ' AND glomas_unidadnegocio_id = '.$options['unidad_neg'];
    		}
    		if(isset($options['idciclo']) && $options['idciclo']!='0'){
    			$str_where .= " AND idciclo='".$options['idciclo']."'";
    		}
    	}
    	
    	if(isset($fecha)){
    		$str_where .= " AND fechaejecucion1 >= '".$fecha."'";
    	}else{
    		$str_where .= " AND fechaejecucion1 >= '".date('Y-m-d')."'";
    	}
    	$schema = 'lecturas';
    
    	$sql="SELECT COUNT(*) as cantidad FROM ".$schema.".comlec_ordenlecturas WHERE montoconsumo1>".$num." ".$str_where;
    
    	$result = $db->query($sql);
    
    	if($result){
    		return $result[0][0]['cantidad'];
    	}
    	return  0;
    }
    
    public function getCountConsumoMayor100PorcientoQueAnteriorByFecha($options=null,$fecha=null){
    	$this->setDateSourceCustom();
    	$db = $this->getDataSource();
    
    	$str_where = '';
    	if(isset($options) && is_array($options)){
    		if(isset($options['unidad_neg']) and $options['unidad_neg']!=0){
    			$str_where .= ' AND glomas_unidadnegocio_id = '.$options['unidad_neg'];
    		}
    		if(isset($options['idciclo']) && $options['idciclo']!='0'){
    			$str_where .= " AND idciclo='".$options['idciclo']."'";
    		}
    	}
    	
    	if(isset($fecha)){
    		$str_where .= " AND fechaejecucion1 >= '".$fecha."'";
    	}else{
    		$str_where .= " AND fechaejecucion1 >= '".date('Y-m-d')."'";
    	}
    	$schema = 'lecturas';
    
    	$sql="SELECT COUNT(*) as cantidad FROM ".$schema.".comlec_ordenlecturas WHERE montoconsumo1 > (consant*2) ".$str_where;
    
    	$result = $db->query($sql);
    
    	if($result){
    		return $result[0][0]['cantidad'];
    	}
    	return  0;
    }
    
    public function getCountEnviadasDistriluzByFecha($options=null,$fecha=null){
    	$this->setDateSourceCustom();
    	$db = $this->getDataSource();
    
    	$str_where = '';
    	if(isset($options) && is_array($options)){
    		if(isset($options['unidad_neg']) and $options['unidad_neg']!=0){
    			$str_where .= ' AND glomas_unidadnegocio_id = '.$options['unidad_neg'];
    		}
    		if(isset($options['idciclo']) && $options['idciclo']!='0'){
    			$str_where .= " AND idciclo='".$options['idciclo']."'";
    		}
    	}
    	 
    	if(isset($fecha)){
    		$str_where .= " AND fechaejecucion1 >= '".$fecha."'";
    	}else{
    		$str_where .= " AND fechaejecucion1 >= '".date('Y-m-d')."'";
    	}
    	$schema = 'lecturas';
    
    	$sql="SELECT COUNT(*) as cantidad FROM ".$schema.".comlec_ordenlecturas WHERE resultadoevaluacion='CONSISTENTE' and ws_l_estado='1' ".$str_where;
    
    	$result = $db->query($sql);
    
    	if($result){
    		return $result[0][0]['cantidad'];
    	}
    	return  0;
    }
    
    public function getCountCartasReclamosByFecha($options=null,$fecha=null){
    	$this->setDateSourceCustom();
    	$db = $this->getDataSource();
    
    	$schema = 'lecturas';
    	$str_where = '';
    	if(isset($options) && is_array($options)){
    		if(isset($options['unidad_neg']) and $options['unidad_neg']!=0){
    			$str_where .= ' AND cart.glomas_unidadnegocio_id = '.$options['unidad_neg'];
    		}
    		if(isset($options['idciclo']) && $options['idciclo']!='0'){
    			$str_where .= " AND idciclo='".$options['idciclo']."'";
    		}
    		if(isset($options['pfactura']) and $options['pfactura']!='0'){
    			$str_where .= " AND cart.pfactura = '".$options['pfactura']."'";
    			$schema = 'history';
    		}else{
    			$str_where .= '';
    			$schema = 'lecturas';
    		}
    	}
    	 
    	if(isset($fecha)){
    		$str_where .= " AND cart.created >= '".$fecha."'";
    	}else{
    		$str_where .= " AND cart.created >= '".date('Y-m-d')."'";
    	}
    	$schema = 'lecturas';
    
    	$sql="SELECT COUNT(cantidad) as cantidad from (
    			SELECT COUNT(*) as cantidad FROM ".$schema.".comlec_cartas cart
    			inner join ".$schema.".comlec_ordenlecturas ol on ol.suministro=cart.suministro and ol.pfactura=cart.pfactura
    			WHERE 1=1 ".$str_where."
    			group by cart.suministro) T ";
    
    	$result = $db->query($sql);
    
    	if($result){
    		return $result[0][0]['cantidad'];
    	}
    	return  0;
    }
    
    public function getCountByCodigos($options=null){
    	$this->setDateSourceCustom();
    	$db = $this->getDataSource();

    	$schema = 'lecturas';
    	$str_where = '';
    	if(isset($options) && is_array($options)){
    		if(isset($options['unidad_neg']) and $options['unidad_neg']!=0){
    			$str_where .= ' AND glomas_unidadnegocio_id = '.$options['unidad_neg'];
    		}
    		if(isset($options['idciclo']) && $options['idciclo']!='0'){
    			$str_where .= " AND idciclo='".$options['idciclo']."'";
    		}
    		if(isset($options['pfactura']) and $options['pfactura']!='0'){
    			$str_where .= " AND pfactura = '".$options['pfactura']."'";
    			$schema = 'history';
    		}else{
    			$str_where .= '';
    			$schema = 'lecturas';
    		}    		
    	}

    	$sql="SELECT idciclo, nombciclo, obs1, descripcion, codigofac, COUNT(obs1) as cantidad FROM ".$schema.".comlec_ordenlecturas ol inner join lecturas.comlec_observaciones obs on ol.obs1=cast(obs.codigocamp as varchar) WHERE obs1 is not null and obs1 <> '' and obs1 <> 'NULL' and obs1<>'0' ".$str_where." GROUP BY idciclo, nombciclo, obs1, descripcion, codigofac ORDER BY cast(obs1 as int) ASC";
    	return  $db->query($sql);
    }
    
    public function getCount($options=null){
    	$this->setDateSourceCustom();
    	$db = $this->getDataSource();
    	
    	$schema = 'lecturas';
    	$str_where = '';
    	if(isset($options) && is_array($options)){
    		if(isset($options['unidad_neg']) and $options['unidad_neg']!=0){
    			$str_where .= ' AND glomas_unidadnegocio_id = '.$options['unidad_neg'];
    		}
    		if(isset($options['idciclo']) && $options['idciclo']!='0'){
    			$str_where .= " AND idciclo='".$options['idciclo']."'";
    		}
    		if(isset($options['idsector']) && $options['idsector']!='0'){
    			$str_where .= " AND sector='".$options['idsector']."'";
    		}
    		if(isset($options['idruta']) && $options['idruta']!='0'){
    			$str_where .= " AND ruta='".$options['idruta']."'";
    		}
    		if(isset($options['pfactura']) and $options['pfactura']!='0'){
    			$str_where .= " AND pfactura = '".$options['pfactura']."'";
    			$schema = 'history';
    		}else{
    			$str_where .= '';
    			$schema = 'lecturas';
    		}    		
    	}

    	$sql="SELECT COUNT(*) as cantidad FROM ".$schema.".comlec_ordenlecturas WHERE 1=1 ".$str_where;
    	
    	$result = $db->query($sql);
    	
    	if($result){
    		return $result[0][0]['cantidad'];
    	}
    	return  0;
    }
    
    public function getCountAsignados($options=null){
    	$this->setDateSourceCustom();
    	$db = $this->getDataSource();
    
    	$schema = 'lecturas';
    	$str_where = '';
    	if(isset($options) && is_array($options)){
    		if(isset($options['unidad_neg']) and $options['unidad_neg']!=0){
    			$str_where .= ' AND glomas_unidadnegocio_id = '.$options['unidad_neg'];
    		}
    		if(isset($options['idciclo']) && $options['idciclo']!='0'){
    			$str_where .= " AND idciclo='".$options['idciclo']."'";
    		}
    		if(isset($options['idsector']) && $options['idsector']!='0'){
    			$str_where .= " AND sector='".$options['idsector']."'";
    		}
    		if(isset($options['idruta']) && $options['idruta']!='0'){
    			$str_where .= " AND ruta='".$options['idruta']."'";
    		}
    		if(isset($options['pfactura']) and $options['pfactura']!='0'){
    			$str_where .= " AND pfactura = '".$options['pfactura']."'";
    			$schema = 'history';
    		}else{
    			$str_where .= '';
    			$schema = 'lecturas';
    		}    		
    	}
    
    	$sql="SELECT COUNT(*) as cantidad FROM ".$schema.".comlec_ordenlecturas WHERE lecturista1_id is not null ".$str_where;
    
    	$result = $db->query($sql);
    
    	if($result){
    		return $result[0][0]['cantidad'];
    	}
    	return  0;
    }
    
    public function getCountDescargados($options=null){
    	$this->setDateSourceCustom();
    	$db = $this->getDataSource();
    
    	$schema = 'lecturas';
    	$str_where = '';
    	if(isset($options) && is_array($options)){
    		if(isset($options['unidad_neg']) and $options['unidad_neg']!=0){
    			$str_where .= ' AND glomas_unidadnegocio_id = '.$options['unidad_neg'];
    		}
    		if(isset($options['idciclo']) && $options['idciclo']!='0'){
    			$str_where .= " AND idciclo='".$options['idciclo']."'";
    		}
    		if(isset($options['pfactura']) and $options['pfactura']!='0'){
    			$str_where .= " AND pfactura = '".$options['pfactura']."'";
    			$schema = 'history';
    		}else{
    			$str_where .= '';
    			$schema = 'lecturas';
    		}    		
    	}
    
    	$sql="SELECT COUNT(*) as cantidad FROM ".$schema.".comlec_ordenlecturas WHERE lecturista1_id is not null and down1 in ('1','2') ".$str_where;
    
    	$result = $db->query($sql);
    
    	if($result){
    		return $result[0][0]['cantidad'];
    	}
    	return  0;
    }
    
    public function getCountObs1($options=null){
    	$this->setDateSourceCustom();
    	$db = $this->getDataSource();

    	$schema = 'lecturas';
    	$str_where = '';
    	if(isset($options) && is_array($options)){
    		if(isset($options['unidad_neg']) and $options['unidad_neg']!=0){
    			$str_where .= ' AND glomas_unidadnegocio_id = '.$options['unidad_neg'];
    		}
    		if(isset($options['idciclo']) && $options['idciclo']!='0'){
    			$str_where .= " AND idciclo='".$options['idciclo']."'";
    		}
    		if(isset($options['pfactura']) and $options['pfactura']!='0'){
    			$str_where .= " AND pfactura = '".$options['pfactura']."'";
    			$schema = 'history';
    		}else{
    			$str_where .= '';
    			$schema = 'lecturas';
    		}    		
    	}

    	$sql="SELECT COUNT(obs1) as cantidad FROM ".$schema.".comlec_ordenlecturas WHERE obs1 is not null and obs1 <> '' and obs1 <> 'NULL' and obs1<>'0' ".$str_where;
    	 
    	$result = $db->query($sql);
    	 
    	if($result){
    		return $result[0][0]['cantidad'];
    	}
    	return  0;
    }
    
    public function getCountObs1SinLectura($options=null){
    	$this->setDateSourceCustom();
    	$db = $this->getDataSource();
    
    	$schema = 'lecturas';
    	$str_where = '';
    	if(isset($options) && is_array($options)){
    		if(isset($options['unidad_neg']) and $options['unidad_neg']!=0){
    			$str_where .= ' AND glomas_unidadnegocio_id = '.$options['unidad_neg'];
    		}
    		if(isset($options['idciclo']) && $options['idciclo']!='0'){
    			$str_where .= " AND idciclo='".$options['idciclo']."'";
    		}
    		if(isset($options['pfactura']) and $options['pfactura']!='0'){
    			$str_where .= " AND pfactura = '".$options['pfactura']."'";
    			$schema = 'history';
    		}else{
    			$str_where .= '';
    			$schema = 'lecturas';
    		}    		
    	}
    
    	$sql="SELECT COUNT(obs1) as cantidad FROM ".$schema.".comlec_ordenlecturas WHERE obs1 is not null and obs1 <> '' and obs1 <> 'NULL' and obs1<>'0' and (lecturao1='' or lecturao1 is null) ".$str_where;
    
    	$result = $db->query($sql);
    
    	if($result){
    		return $result[0][0]['cantidad'];
    	}
    	return  0;
    }
    
    public function getCountObs1ConLectura($options=null){
    	$this->setDateSourceCustom();
    	$db = $this->getDataSource();
    
    	$schema = 'lecturas';
    	$str_where = '';
    	if(isset($options) && is_array($options)){
    		if(isset($options['unidad_neg']) and $options['unidad_neg']!=0){
    			$str_where .= ' AND glomas_unidadnegocio_id = '.$options['unidad_neg'];
    		}
    		if(isset($options['idciclo']) && $options['idciclo']!='0'){
    			$str_where .= " AND idciclo='".$options['idciclo']."'";
    		}
    		if(isset($options['pfactura']) and $options['pfactura']!='0'){
    			$str_where .= " AND pfactura = '".$options['pfactura']."'";
    			$schema = 'history';
    		}else{
    			$str_where .= '';
    			$schema = 'lecturas';
    		}    		
    	}
    
    	$sql="SELECT COUNT(obs1) as cantidad FROM ".$schema.".comlec_ordenlecturas WHERE obs1 is not null and obs1 <> '' and obs1 <> 'NULL' and obs1<>'0' and lecturao1!='' ".$str_where;
    
    	$result = $db->query($sql);
    
    	if($result){
    		return $result[0][0]['cantidad'];
    	}
    	return  0;
    }
    
    public function getCountObs1IgualA($options=null,$obs=99){
    	$this->setDateSourceCustom();
    	$db = $this->getDataSource();
    
    	$schema = 'lecturas';
    	$str_where = '';
    	if(isset($options) && is_array($options)){
    		if(isset($options['unidad_neg']) and $options['unidad_neg']!=0){
    			$str_where .= ' AND glomas_unidadnegocio_id = '.$options['unidad_neg'];
    		}
    		if(isset($options['idciclo']) && $options['idciclo']!='0'){
    			$str_where .= " AND idciclo='".$options['idciclo']."'";
    		}
    		if(isset($options['pfactura']) and $options['pfactura']!='0'){
    			$str_where .= " AND pfactura = '".$options['pfactura']."'";
    			$schema = 'history';
    		}else{
    			$str_where .= '';
    			$schema = 'lecturas';
    		}    		
    	}
    
    	$sql="SELECT COUNT(*) as cantidad FROM ".$schema.".comlec_ordenlecturas WHERE obs1 ='".$obs."' ".$str_where;
    
    	$result = $db->query($sql);
    
    	if($result){
    		return $result[0][0]['cantidad'];
    	}
    	return  0;
    }
    
    public function getCountConsistentes($options=null){
    	$this->setDateSourceCustom();
    	$db = $this->getDataSource();

    	$schema = 'lecturas';
    	$str_where = '';
    	if(isset($options) && is_array($options)){
    		if(isset($options['unidad_neg']) and $options['unidad_neg']!=0){
    			$str_where .= ' AND glomas_unidadnegocio_id = '.$options['unidad_neg'];
    		}
    		if(isset($options['idciclo']) && $options['idciclo']!='0'){
    			$str_where .= " AND idciclo='".$options['idciclo']."'";
    		}
    		if(isset($options['pfactura']) and $options['pfactura']!='0'){
    			$str_where .= " AND pfactura = '".$options['pfactura']."'";
    			$schema = 'history';
    		}else{
    			$str_where .= '';
    			$schema = 'lecturas';
    		}    		
    	}

    	$sql="SELECT COUNT(*) as cantidad FROM ".$schema.".comlec_ordenlecturas WHERE resultadoevaluacion='CONSISTENTE' ".$str_where;
    
    	$result = $db->query($sql);
    
    	if($result){
    		return $result[0][0]['cantidad'];
    	}
    	return  0;
    }
    
    public function getCountInconsistentesEvaluadas($options=null){
    	$this->setDateSourceCustom();
    	$db = $this->getDataSource();

    	$schema = 'lecturas';
    	$str_where = '';
    	if(isset($options) && is_array($options)){
    		if(isset($options['unidad_neg']) and $options['unidad_neg']!=0){
    			$str_where .= ' AND glomas_unidadnegocio_id = '.$options['unidad_neg'];
    		}
    		if(isset($options['idciclo']) && $options['idciclo']!='0'){
    			$str_where .= " AND idciclo='".$options['idciclo']."'";
    		}
    		if(isset($options['pfactura']) and $options['pfactura']!='0'){
    			$str_where .= " AND pfactura = '".$options['pfactura']."'";
    			$schema = 'history';
    		}else{
    			$str_where .= '';
    			$schema = 'lecturas';
    		}    		
    	}

    	$sql="SELECT COUNT(*) as cantidad FROM ".$schema.".comlec_ordenlecturas WHERE validacion>='1' ".$str_where;
    
    	$result = $db->query($sql);
    
    	if($result){
    		return $result[0][0]['cantidad'];
    	}
    	return  0;
    }
    
    public function getCountInconsistentesActuales($options=null){
    	$this->setDateSourceCustom();
    	$db = $this->getDataSource();

    	$schema = 'lecturas';
    	$str_where = '';
    	if(isset($options) && is_array($options)){
    		if(isset($options['unidad_neg']) and $options['unidad_neg']!=0){
    			$str_where .= ' AND glomas_unidadnegocio_id = '.$options['unidad_neg'];
    		}
    		if(isset($options['idciclo']) && $options['idciclo']!='0'){
    			$str_where .= " AND idciclo='".$options['idciclo']."'";
    		}
    		if(isset($options['pfactura']) and $options['pfactura']!='0'){
    			$str_where .= " AND pfactura = '".$options['pfactura']."'";
    			$schema = 'history';
    		}else{
    			$str_where .= '';
    			$schema = 'lecturas';
    		}    		
    	}

    	$sql="SELECT COUNT(*) as cantidad FROM ".$schema.".comlec_ordenlecturas WHERE resultadoevaluacion='INCONSISTENTE' ".$str_where;
    
    	$result = $db->query($sql);
    
    	if($result){
    		return $result[0][0]['cantidad'];
    	}
    	return  0;
    }

    public function getCountTiempoPromedioEjecucion($options=null){
    	$this->setDateSourceCustom();
    	$db = $this->getDataSource();

    	$schema = 'lecturas';
    	$str_where = '';
    	if(isset($options) && is_array($options)){
    		if(isset($options['unidad_neg']) and $options['unidad_neg']!=0){
    			$str_where .= ' AND glomas_unidadnegocio_id = '.$options['unidad_neg'];
    		}
    		if(isset($options['idciclo']) && $options['idciclo']!='0'){
    			$str_where .= " AND idciclo='".$options['idciclo']."'";
    		}
    		if(isset($options['pfactura']) and $options['pfactura']!='0'){
    			$str_where .= " AND pfactura = '".$options['pfactura']."'";
    			$schema = 'history';
    		}else{
    			$str_where .= '';
    			$schema = 'lecturas';
    		}    		
    	}

    	$sql="select avg(tiempo) as cantidad from (
    select *, age(F1, F2) as tiempo from (
    (select *, ROW_NUMBER() OVER() as I1 from (select fechaejecucion1 AS F1 from ".$schema.".comlec_ordenlecturas WHERE fechaejecucion1 is not null ".$str_where." order by fechaejecucion1 asc) as t1) as t11
    LEFT OUTER JOIN
    (select *, ROW_NUMBER() OVER() as I2 from (select fechaejecucion1 AS F2 from ".$schema.".comlec_ordenlecturas WHERE fechaejecucion1 is not null ".$str_where." order by fechaejecucion1 asc) as t2) as t22
    on t11.I1=t22.I2+1) as t3
    ) as t4";
    
    	$result = $db->query($sql);
    
    	if($result){
    		return $result[0][0]['cantidad'];
    	}
    	return  0;
    }
    
    public function getCountAvanzados($options=null){
    	$this->setDateSourceCustom();
    	$db = $this->getDataSource();
    
    	$schema = 'lecturas';
    	$str_where = '';
    	if(isset($options) && is_array($options)){
    		if(isset($options['unidad_neg']) and $options['unidad_neg']!=0){
    			$str_where .= ' AND glomas_unidadnegocio_id = '.$options['unidad_neg'];
    		}
    		if(isset($options['idciclo']) && $options['idciclo']!='0'){
    			$str_where .= " AND idciclo='".$options['idciclo']."'";
    		}
    		if(isset($options['pfactura']) and $options['pfactura']!='0'){
    			$str_where .= " AND pfactura = '".$options['pfactura']."'";
    			$schema = 'history';
    		}else{
    			$str_where .= '';
    			$schema = 'lecturas';
    		}    		
    	}
    
    	$sql="SELECT COUNT(*) as cantidad FROM ".$schema.".comlec_ordenlecturas WHERE (resultadoevaluacion='CONSISTENTE' OR resultadoevaluacion='INCONSISTENTE') ".$str_where;
    
    	$result = $db->query($sql);
    
    	if($result){
    		return $result[0][0]['cantidad'];
    	}
    	return  0;
    }
    
    public function getCountConsumoCero($options=null){
    	$this->setDateSourceCustom();
    	$db = $this->getDataSource();
    
    	$schema = 'lecturas';
    	$str_where = '';
    	if(isset($options) && is_array($options)){
    		if(isset($options['unidad_neg']) and $options['unidad_neg']!=0){
    			$str_where .= ' AND glomas_unidadnegocio_id = '.$options['unidad_neg'];
    		}
    		if(isset($options['idciclo']) && $options['idciclo']!='0'){
    			$str_where .= " AND idciclo='".$options['idciclo']."'";
    		}
    		if(isset($options['pfactura']) and $options['pfactura']!='0'){
    			$str_where .= " AND pfactura = '".$options['pfactura']."'";
    			$schema = 'history';
    		}else{
    			$str_where .= '';
    			$schema = 'lecturas';
    		}    		
    	}
    
    	$sql="SELECT COUNT(*) as cantidad FROM ".$schema.".comlec_ordenlecturas WHERE montoconsumo1=0 ".$str_where;
    
    	$result = $db->query($sql);
    
    	if($result){
    		return $result[0][0]['cantidad'];
    	}
    	return  0;
    }
    
    public function getCountConsumoMenoresQue($options=null,$num=35){
    	$this->setDateSourceCustom();
    	$db = $this->getDataSource();
    
    	$schema = 'lecturas';
    	$str_where = '';
    	if(isset($options) && is_array($options)){
    		if(isset($options['unidad_neg']) and $options['unidad_neg']!=0){
    			$str_where .= ' AND glomas_unidadnegocio_id = '.$options['unidad_neg'];
    		}
    		if(isset($options['idciclo']) && $options['idciclo']!='0'){
    			$str_where .= " AND idciclo='".$options['idciclo']."'";
    		}
    		if(isset($options['pfactura']) and $options['pfactura']!='0'){
    			$str_where .= " AND pfactura = '".$options['pfactura']."'";
    			$schema = 'history';
    		}else{
    			$str_where .= '';
    			$schema = 'lecturas';
    		}    		
    	}
    
    	$sql="SELECT COUNT(*) as cantidad FROM ".$schema.".comlec_ordenlecturas WHERE montoconsumo1<".$num." ".$str_where;
    
    	$result = $db->query($sql);
    
    	if($result){
    		return $result[0][0]['cantidad'];
    	}
    	return  0;
    }
    
    public function getCountConsumoMayoresQue($options=null,$num=1000){
    	$this->setDateSourceCustom();
    	$db = $this->getDataSource();
    
    	$schema = 'lecturas';
    	$str_where = '';
    	if(isset($options) && is_array($options)){
    		if(isset($options['unidad_neg']) and $options['unidad_neg']!=0){
    			$str_where .= ' AND glomas_unidadnegocio_id = '.$options['unidad_neg'];
    		}
    		if(isset($options['idciclo']) && $options['idciclo']!='0'){
    			$str_where .= " AND idciclo='".$options['idciclo']."'";
    		}
    		if(isset($options['pfactura']) and $options['pfactura']!='0'){
    			$str_where .= " AND pfactura = '".$options['pfactura']."'";
    			$schema = 'history';
    		}else{
    			$str_where .= '';
    			$schema = 'lecturas';
    		}    		
    	}
    
    	$sql="SELECT COUNT(*) as cantidad FROM ".$schema.".comlec_ordenlecturas WHERE montoconsumo1>".$num." ".$str_where;
    
    	$result = $db->query($sql);
    
    	if($result){
    		return $result[0][0]['cantidad'];
    	}
    	return  0;
    }
    
    public function getCountConsumoMayor100PorcientoQueAnterior($options=null){
    	$this->setDateSourceCustom();
    	$db = $this->getDataSource();
    
    	$schema = 'lecturas';
    	$str_where = '';
    	if(isset($options) && is_array($options)){
    		if(isset($options['unidad_neg']) and $options['unidad_neg']!=0){
    			$str_where .= ' AND glomas_unidadnegocio_id = '.$options['unidad_neg'];
    		}
    		if(isset($options['idciclo']) && $options['idciclo']!='0'){
    			$str_where .= " AND idciclo='".$options['idciclo']."'";
    		}
    		if(isset($options['pfactura']) and $options['pfactura']!='0'){
    			$str_where .= " AND pfactura = '".$options['pfactura']."'";
    			$schema = 'history';
    		}else{
    			$str_where .= '';
    			$schema = 'lecturas';
    		}    		
    	}
    
    	$sql="SELECT COUNT(*) as cantidad FROM ".$schema.".comlec_ordenlecturas WHERE montoconsumo1 > (consant*2) ".$str_where;
    
    	$result = $db->query($sql);
    
    	if($result){
    		return $result[0][0]['cantidad'];
    	}
    	return  0;
    }
    
    public function getCountEnviadasDistriluz($options=null){
    	$this->setDateSourceCustom();
    	$db = $this->getDataSource();
    
    	$schema = 'lecturas';
    	$str_where = '';
    	if(isset($options) && is_array($options)){
    		if(isset($options['unidad_neg']) and $options['unidad_neg']!=0){
    			$str_where .= ' AND glomas_unidadnegocio_id = '.$options['unidad_neg'];
    		}
    		if(isset($options['idciclo']) && $options['idciclo']!='0'){
    			$str_where .= " AND idciclo='".$options['idciclo']."'";
    		}
    		if(isset($options['pfactura']) and $options['pfactura']!='0'){
    			$str_where .= " AND pfactura = '".$options['pfactura']."'";
    			$schema = 'history';
    		}else{
    			$str_where .= '';
    			$schema = 'lecturas';
    		}    		
    	}
    
    	$sql="SELECT COUNT(*) as cantidad FROM ".$schema.".comlec_ordenlecturas WHERE resultadoevaluacion='CONSISTENTE' and ws_l_estado='1' ".$str_where;
    
    	$result = $db->query($sql);
    
    	if($result){
    		return $result[0][0]['cantidad'];
    	}
    	return  0;
    }
    
    public function getMaxOrdenRuta($options=null){
    	$this->setDateSourceCustom();
    	$db = $this->getDataSource();
    	 
    	$schema = 'lecturas';
    	$str_where = '';
    	if(isset($options) && is_array($options)){
    		if(isset($options['unidad_neg']) and $options['unidad_neg']!=0){
    			$str_where .= ' AND glomas_unidadnegocio_id = '.$options['unidad_neg'];
    		}
    		if(isset($options['idciclo']) && $options['idciclo']!='0'){
    			$str_where .= " AND idciclo='".$options['idciclo']."'";
    		}
    		if(isset($options['idsector']) && $options['idsector']!='0'){
    			$str_where .= " AND sector='".$options['idsector']."'";
    		}
    		if(isset($options['idruta']) && $options['idruta']!='0'){
    			$str_where .= " AND ruta='".$options['idruta']."'";
    		}
    		if(isset($options['pfactura']) and $options['pfactura']!='0'){
    			$str_where .= " AND pfactura = '".$options['pfactura']."'";
    			$schema = 'history';
    		}else{
    			$str_where .= '';
    			$schema = 'lecturas';
    		}
    	}
    
    	$sql="SELECT max(ordenruta) as cantidad FROM ".$schema.".comlec_ordenlecturas WHERE 1=1 ".$str_where;
    	 
    	$result = $db->query($sql);
    	 
    	if($result){
    		return $result[0][0]['cantidad'];
    	}
    	return  0;
    }
    
    public function getMinOrdenRuta($options=null){
    	$this->setDateSourceCustom();
    	$db = $this->getDataSource();
    
    	$schema = 'lecturas';
    	$str_where = '';
    	if(isset($options) && is_array($options)){
    		if(isset($options['unidad_neg']) and $options['unidad_neg']!=0){
    			$str_where .= ' AND glomas_unidadnegocio_id = '.$options['unidad_neg'];
    		}
    		if(isset($options['idciclo']) && $options['idciclo']!='0'){
    			$str_where .= " AND idciclo='".$options['idciclo']."'";
    		}
    		if(isset($options['idsector']) && $options['idsector']!='0'){
    			$str_where .= " AND sector='".$options['idsector']."'";
    		}
    		if(isset($options['idruta']) && $options['idruta']!='0'){
    			$str_where .= " AND ruta='".$options['idruta']."'";
    		}
    		if(isset($options['pfactura']) and $options['pfactura']!='0'){
    			$str_where .= " AND pfactura = '".$options['pfactura']."'";
    			$schema = 'history';
    		}else{
    			$str_where .= '';
    			$schema = 'lecturas';
    		}
    	}
    
    	$sql="SELECT min(ordenruta) as cantidad FROM ".$schema.".comlec_ordenlecturas WHERE 1=1 ".$str_where;
    
    	$result = $db->query($sql);
    
    	if($result){
    		return $result[0][0]['cantidad'];
    	}
    	return  0;
    }
    
    
      public function reporteByObs(){
    	$this->setDateSourceCustom ();$db = $this->getDataSource();
               
        $sql_cabecera="select  string_agg(distinct ''''||obs1||'''',' int, ' ORDER BY ''''||obs1||'''') as cabecera from lecturas.comlec_ordenlecturas
where   OBS1<>'' and obs1<>'NULL' and obs1<>'0'";
        $cabecera=$db->query($sql_cabecera);
        
        $sql="select * from crosstab(
                    'select idciclo,sector, obs1,count(suministro) from lecturas.comlec_ordenlecturas
                  where   OBS1<>''
                  group by idciclo,sector,obs1
                  order by idciclo,sector,obs1',
                    'select  distinct obs1 from lecturas.comlec_ordenlecturas
                  where   OBS1<>'' and obs1<>'NULL' and obs1<>'0'
                  group by idciclo,sector,obs1
                  order by obs1'
                  ) as (
                  idciclo text,sector text, ".$cabecera." int
                  );
                  ";
    	/*$sql="select * from crosstab(
                    'select idciclo,sector, obs1,count(suministro) from lecturas.comlec_ordenlecturas
                  where   OBS1<>''
                  group by idciclo,sector,obs1
                  order by idciclo,sector,obs1',
                    'select  distinct obs1 from lecturas.comlec_ordenlecturas
                  where   OBS1<>'' and obs1<>'NULL' and obs1<>'0'
                  group by idciclo,sector,obs1
                  order by obs1'
                  ) as (
                  idciclo text,sector text, '1' int, '10' int, '11' int, '12' int,'13' int, '14' int, '16' int, '2' int, '20' int, '21' int, '30' int, '31' int, '32' int, '33' int, '4' int, '40' int, '41' int, '42' int, '44' int, '50' int, '74' int, '76' int, '8' int, '9' int, '99' int
                  );
                  ";
         * */
         
    	return  $db->query($sql);
    }
    
    public function getCodigosObservaciones($unidadneg=null,$pfactura=null){
    	$this->setDateSourceCustom ();$db = $this->getDataSource();
    	
    	if(isset($unidadneg) and $unidadneg!=0){
    		$str_where = ' AND glomas_unidadnegocio_id = '.$unidadneg;
    	}else{
    		$str_where = '';
    	}
    	if(isset($pfactura) and $pfactura!=0){
    		$str_where .= " AND pfactura = '".$pfactura."'";
    		$schema = 'history';
    	}else{
    		$str_where .= '';
    		$schema = 'lecturas';
    	}
    	$sql="SELECT obs1, descripcion, codigofac FROM ".$schema.".comlec_ordenlecturas ol inner join lecturas.comlec_observaciones obs on ol.obs1=cast(obs.codigocamp as varchar) WHERE obs1 is not null and obs1 <> '' and obs1 <> 'NULL' and obs1<>'0' ".$str_where." GROUP BY obs1, descripcion, codigofac ORDER BY cast(obs1 as int) asc";
    
    	return $db->query($sql);
    }
    
    public function getCountObservacionesPorCicloYSector($unidadneg=null,$pfactura=null){
    	$this->setDateSourceCustom ();$db = $this->getDataSource();
    	
    	if(isset($unidadneg) and $unidadneg!=0){
    		$str_where = ' AND glomas_unidadnegocio_id = '.$unidadneg;
    	}else{
    		$str_where = '';
    	}
    	if(isset($pfactura) and $pfactura!=0){
    		$str_where .= " AND pfactura = '".$pfactura."'";
    		$schema = 'history';
    	}else{
    		$str_where .= '';
    		$schema = 'lecturas';
    	}
    	$sql="SELECT idciclo, sector, obs1, COUNT(obs1) as cantidad FROM ".$schema.".comlec_ordenlecturas WHERE obs1 is not null and obs1 <> '' and obs1 <> 'NULL' and obs1<>'0' ".$str_where." GROUP BY idciclo, sector, obs1 ORDER BY idciclo asc, sector asc, cast(obs1 as int) asc";
    
    	return $db->query($sql);
    }
    
    public function getCountObservacionesPorCicloYSectorYRuta($unidadneg=null,$pfactura=null){
    	$this->setDateSourceCustom ();$db = $this->getDataSource();
    	
    	if(isset($unidadneg) and $unidadneg!=0){
    		$str_where = ' AND glomas_unidadnegocio_id = '.$unidadneg;
    	}else{
    		$str_where = '';
    	}
    	if(isset($pfactura) and $pfactura!=0){
    		$str_where .= " AND pfactura = '".$pfactura."'";
    		$schema = 'history';
    	}else{
    		$str_where .= '';
    		$schema = 'lecturas';
    	}
    	$sql="SELECT idciclo, sector, ruta, obs1, COUNT(obs1) as cantidad FROM ".$schema.".comlec_ordenlecturas WHERE obs1 is not null and obs1 <> '' and obs1 <> 'NULL' and obs1<>'0' ".$str_where." GROUP BY idciclo, sector, ruta, obs1 ORDER BY idciclo asc, sector asc, ruta asc, cast(obs1 as int) asc";
    
    	return $db->query($sql);
    }
    
    public function listarPersonalParaLecturas($tipo='L',$unidadneg=null,$id_ciclo=null,$id_sector=null){
    	$this->setDateSourceCustom();
    	$db = $this->getDataSource();    	
    	
    	$str_field_relation_lecturista = '';
    	if($tipo=='R'){
    		$str_field_relation_lecturista .= 'lecturista2_id';
    	}else{
    		$str_field_relation_lecturista .= 'lecturista1_id';
    	}
    	
    	$str_where = '';
    	if(isset($tipo) && $tipo!='0'){
	    	$str_where .= " AND tipolectura='".$tipo."'";
    	}
    	
    	if(isset($unidadneg) && $unidadneg!='0'){
    		$str_where .= " AND glomas_unidadnegocio_id='".$unidadneg."'";
    	}
    	if(isset($id_ciclo) && $id_ciclo!='0'){
    		$str_where .= " AND idciclo='".$id_ciclo."'";
    	}
    	if(isset($id_sector) && $id_sector!='0'){
    		$str_where .= " AND sector='".$id_sector."'";
    	}
    	
    	$sql="SELECT ".$str_field_relation_lecturista.", lecturista, cro.fecha, T.pfactura, T.glomas_unidadnegocio_id, gun.descripcion as glomas_unidadnegocio, T.idciclo, sector, string_agg(ruta, ', ') AS ruta, string_agg(nombruta, ', ') AS nombruta, string_agg(direccion, ', ') as direccion, sum(cantidad) as cantidad, COUNT(*) OVER() as row_total FROM (
SELECT ".$str_field_relation_lecturista.", e.nombre as lecturista, ol.pfactura, ol.glomas_unidadnegocio_id, idciclo, sector, ruta, nombruta, string_agg(direccion, ',') as direccion, COUNT(*) as cantidad FROM lecturas.comlec_ordenlecturas ol 
INNER JOIN lecturas.glomas_empleados e ON ol.".$str_field_relation_lecturista."=e.id 
WHERE 1=1 ".$str_where." GROUP BY ol.pfactura, ol.glomas_unidadnegocio_id, idciclo, sector, ruta, nombruta, ".$str_field_relation_lecturista.", e.nombre
) T inner join lecturas.glomas_unidadnegocios gun on T.glomas_unidadnegocio_id=gun.id 
left join lecturas.comlec_cronograma cro on T.glomas_unidadnegocio_id=cro.glomas_unidadnegocio_id and T.idciclo=cro.idciclo and T.sector=cro.idsector
GROUP BY ".$str_field_relation_lecturista.", lecturista, cro.fecha, T.pfactura, T.glomas_unidadnegocio_id, gun.descripcion, T.idciclo, sector ORDER BY cro.fecha, T.glomas_unidadnegocio_id, T.idciclo, sector, ruta";
    	
    	//echo $sql;exit;
    	return $db->query($sql);
    }
    
    public function listarPersonalParaLecturasByNegocio($negocio,$tipo='L',$unidadneg=null,$id_ciclo=null,$id_sector=null){
    	$this->setDataSource($negocio);
    	$db = $this->getDataSource();
    	 
    	$str_field_relation_lecturista = '';
    	if($tipo=='R'){
    		$str_field_relation_lecturista .= 'lecturista2_id';
    	}else{
    		$str_field_relation_lecturista .= 'lecturista1_id';
    	}
    	 
    	$str_where = '';
    	if(isset($tipo) && $tipo!='0'){
    		$str_where .= " AND tipolectura='".$tipo."'";
    	}
    	 
    	if(isset($unidadneg) && $unidadneg!='0'){
    		$str_where .= " AND glomas_unidadnegocio_id='".$unidadneg."'";
    	}
    	if(isset($id_ciclo) && $id_ciclo!='0'){
    		$str_where .= " AND idciclo='".$id_ciclo."'";
    	}
    	if(isset($id_sector) && $id_sector!='0'){
    		$str_where .= " AND sector='".$id_sector."'";
    	}
    	 
    	$sql="SELECT ".$str_field_relation_lecturista.", lecturista, cro.fecha, T.pfactura, T.glomas_unidadnegocio_id, gun.descripcion as glomas_unidadnegocio, T.idciclo, sector, string_agg(ruta, ', ') AS ruta, string_agg(nombruta, ', ') AS nombruta, string_agg(direccion, ', ') as direccion, sum(cantidad) as cantidad, COUNT(*) OVER() as row_total FROM (
SELECT ".$str_field_relation_lecturista.", e.nombre as lecturista, ol.pfactura, ol.glomas_unidadnegocio_id, idciclo, sector, ruta, nombruta, string_agg(direccion, ',') as direccion, COUNT(*) as cantidad FROM lecturas.comlec_ordenlecturas ol
INNER JOIN lecturas.glomas_empleados e ON ol.".$str_field_relation_lecturista."=e.id
WHERE 1=1 ".$str_where." GROUP BY ol.pfactura, ol.glomas_unidadnegocio_id, idciclo, sector, ruta, nombruta, ".$str_field_relation_lecturista.", e.nombre
) T inner join lecturas.glomas_unidadnegocios gun on T.glomas_unidadnegocio_id=gun.id
left join lecturas.comlec_cronograma cro on T.glomas_unidadnegocio_id=cro.glomas_unidadnegocio_id and T.idciclo=cro.idciclo and T.sector=cro.idsector
GROUP BY ".$str_field_relation_lecturista.", lecturista, cro.fecha, T.pfactura, T.glomas_unidadnegocio_id, gun.descripcion, T.idciclo, sector ORDER BY cro.fecha, T.glomas_unidadnegocio_id, T.idciclo, sector, ruta";
    	 
    	//echo $sql;exit;
    	return $db->query($sql);
    }
    
    public function  listarArtefactosQuemadosBySuministros($suministros){
    	$this->setDateSourceCustom ();$db = $this->getDataSource();
    	$sql="SELECT * FROM  lecturas.comlec_ordenlecturas WHERE suministro IN (".$suministros.")";
    	
    	return $db->query($sql);
    }
    
    public function  buscarSuministroHistorico($suministro, $pfactura='' ,$limit=''){
    	$this->setDateSourceCustom();
    	$db = $this->getDataSource();
    	
    	if($pfactura == ''){
    		$str_esquema = "lecturas";
    		$str_where = "";
    	}elseif($pfactura == 'all'){
    		$str_esquema = "history";
    		$str_where = "";
    	}else{
    		$str_esquema = "history";
    		$str_where = " AND pfactura='".$pfactura."'";
    	}
    	
    	if($limit != ''){
    		$limit = " LIMIT ".$limit;
    	}else{
    		$limit = "";
    	}
    	
    	$sql="SELECT * FROM ".$str_esquema.".comlec_ordenlecturas ol left join lecturas.comlec_observaciones obs on ol.obs1 =obs.codigocamp_varchar left join lecturas.glomas_empleados e on ol.lecturista1_id=e.id WHERE suministro = '$suministro' ".$str_where." ORDER BY pfactura DESC $limit";
    	 
    	return $db->query($sql);
    }
    
    public function  buscarSuministroHistoricoComentario($suministro, $pfactura=''){
    	$this->setDateSourceCustom();
    	$db = $this->getDataSource();
    	 
    	if($pfactura == ''){
    		$str_esquema = "lecturas";
    		$str_where = "";
    	}elseif($pfactura == 'all'){
    		$str_esquema = "history";
    		$str_where = "";
    	}else{
    		$str_esquema = "history";
    		$str_where = " AND pfactura='".$pfactura."'";
    	}
    	 
    	$sql="SELECT ol.suministro,count(*) as count_comentario FROM ".$str_esquema.".comlec_ordenlecturas ol left join lecturas.comlec_ordenlecturas_comentarios clc on clc.suministro = ol.suministro and clc.pfactura = ol.pfactura WHERE ol.suministro = '$suministro' ".$str_where." GROUP BY ol.suministro";
    
    	return $db->query($sql);
    }
    
    public function  buscarSuministroAuditoria($suministro, $limit=''){
    	$this->setDateSourceCustom();
    	$db = $this->getDataSource();
    	
    	$str_esquema = "auditoria";
    	$str_where = "";
    	if($limit != ''){
    		$limit = " LIMIT ".$limit;
    	}else{
    		$limit = "";
    	}
    	 
    	$sql="SELECT * FROM ".$str_esquema.".comlec_ordenlecturas ol left join lecturas.comlec_observaciones obs on ol.obs1 =obs.codigocamp_varchar left join lecturas.glomas_empleados e on ol.lecturista1_id=e.id WHERE suministro = '$suministro' ".$str_where." ORDER BY fecha_actualizacion DESC $limit";
    
    	return $db->query($sql);
    }
    
    public function listarPFactura(){
    	$this->setDateSourceCustom ();
    	$db = $this->getDataSource();
    	$sql="SELECT distinct pfactura FROM
                 history.comlec_ordenlecturas o
                 order by pfactura desc";
    	return  $db->query($sql);
    }
    
    public function getOrdenesPorObservacion($obs=null,$unidadneg=null,$pfactura=null){
    	$this->setDateSourceCustom ();$db = $this->getDataSource();
    	 
    	if(isset($obs)){
    		$str_where = " AND obs1 = '".$obs."'";
    	}else{
    		$str_where = '';
    	}
    	if(isset($unidadneg) and $unidadneg!=0){
    		$str_where .= ' AND glomas_unidadnegocio_id = '.$unidadneg;
    	}else{
    		$str_where .= '';
    	}
    	if(isset($pfactura) and $pfactura!=0){
    		$str_where .= " AND pfactura = '".$pfactura."'";
    		$schema = 'history';
    	}else{
    		$str_where .= '';
    		$schema = 'lecturas';
    	}
    	$sql="SELECT * FROM ".$schema.".comlec_ordenlecturas WHERE 1=1 ".$str_where." ORDER BY idciclo asc, sector asc";
    
    	return $db->query($sql);
    }
    
    public function getUltimaOrdenPorLecturista($lecturista_id=null){
    	$this->setDateSourceCustom();
    	$db = $this->getDataSource();
    
    	$str_where = '';
    	if(isset($lecturista_id) && is_array($lecturista_id)){
    		if(count($lecturista_id)>0){
    			$str_where = " AND lecturista1_id in (".implode(',',$lecturista_id).")";
    		}
    	}elseif(isset($lecturista_id) && $lecturista_id!=''){
    		$str_where = " AND lecturista1_id = '".$lecturista_id."'";
    	}else{
    		$str_where = '';
    	}
    	
    	if($str_where!=''){
    		$sql="select *, ee.nombre as lecturista from lecturas.comlec_ordenlecturas lo left join lecturas.glomas_empleados ee 
                on ee.id=lo.lecturista1_id where 1=1 ".$str_where." and lo.comlec_ordenlectura_id=
              		(select comlec_ordenlectura_id from lecturas.comlec_ordenlecturas where 1=1 and lecturista1_id = lo.lecturista1_id and resultadoevaluacion is not null 
                	ORDER BY fechaejecucion1 desc LIMIT 1)";
    
    	
    		return $db->query($sql);
    	}else{
    		return null;
    	}
    }
    
    public function getCountSuministrosByXml($id_xml=null){
    	$this->setDateSourceCustom();
    	$db = $this->getDataSource();
    
    	$schema = 'lecturas';
    
    	$sql="SELECT COUNT(*) as cantidad FROM ".$schema.".comlec_xml inner join ".$schema.".comlec_ordenlecturas on comlec_xml_id=lecturas.comlec_xml.id and lecturas.comlec_xml.id=".$id_xml." ";
    
    	$result = $db->query($sql);
    
    	if($result){
    		return $result[0][0]['cantidad'];
    	}
    	return  0;
    }
    
    public function getCountAsignadosByXml($id_xml=null){
    	$this->setDateSourceCustom();
    	$db = $this->getDataSource();
    
    	$schema = 'lecturas';
    
    	$sql="SELECT COUNT(*) as cantidad FROM ".$schema.".comlec_xml inner join ".$schema.".comlec_ordenlecturas on comlec_xml_id=lecturas.comlec_xml.id and lecturas.comlec_xml.id=".$id_xml." WHERE lecturista1_id is not null ";

    	$result = $db->query($sql);
    
    	if($result){
    		return $result[0][0]['cantidad'];
    	}
    	return  0;
    }
    
    public function getCountDescargadosByXml($id_xml=null){
    	$this->setDateSourceCustom();
    	$db = $this->getDataSource();
    
    	$schema = 'lecturas';
    
    	$sql="SELECT COUNT(*) as cantidad FROM ".$schema.".comlec_xml inner join ".$schema.".comlec_ordenlecturas on comlec_xml_id=lecturas.comlec_xml.id and lecturas.comlec_xml.id=".$id_xml." WHERE lecturista1_id is not null and down1 in ('1','2') ";
    
    	$result = $db->query($sql);
    
    	if($result){
    		return $result[0][0]['cantidad'];
    	}
    	return  0;
    }
    
    public function getCountFinalizadosByXml($id_xml=null){
    	$this->setDateSourceCustom();
    	$db = $this->getDataSource();
    
    	$schema = 'lecturas';
    
    	$sql="SELECT COUNT(*) as cantidad FROM ".$schema.".comlec_xml inner join ".$schema.".comlec_ordenlecturas on comlec_xml_id=lecturas.comlec_xml.id and lecturas.comlec_xml.id=".$id_xml." WHERE (resultadoevaluacion='CONSISTENTE' OR resultadoevaluacion='INCONSISTENTE') ";
    
    	$result = $db->query($sql);
    
    	if($result){
    		return $result[0][0]['cantidad'];
    	}
    	return  0;
    }
    
    public function getCountConsistentesByXml($id_xml=null){
    	$this->setDateSourceCustom();
    	$db = $this->getDataSource();
    
    	$schema = 'lecturas';
    
    	$sql="SELECT COUNT(*) as cantidad FROM ".$schema.".comlec_xml inner join ".$schema.".comlec_ordenlecturas on comlec_xml_id=lecturas.comlec_xml.id and lecturas.comlec_xml.id=".$id_xml." WHERE resultadoevaluacion='CONSISTENTE' ";
    
    	$result = $db->query($sql);
    
    	if($result){
    		return $result[0][0]['cantidad'];
    	}
    	return  0;
    }
    
    public function getCountInconsistentesByXml($id_xml=null){
    	$this->setDateSourceCustom();
    	$db = $this->getDataSource();
    
    	$schema = 'lecturas';
    
    	$sql="SELECT COUNT(*) as cantidad FROM ".$schema.".comlec_xml inner join ".$schema.".comlec_ordenlecturas on comlec_xml_id=lecturas.comlec_xml.id and lecturas.comlec_xml.id=".$id_xml." WHERE resultadoevaluacion='INCONSISTENTE' ";
    
    	$result = $db->query($sql);
    
    	if($result){
    		return $result[0][0]['cantidad'];
    	}
    	return  0;
    }
    
    public function getCountObs1SinLecturaByXml($id_xml=null){
    	$this->setDateSourceCustom();
    	$db = $this->getDataSource();
    
    	$schema = 'lecturas';
    
    	$sql="SELECT COUNT(*) as cantidad FROM ".$schema.".comlec_xml inner join ".$schema.".comlec_ordenlecturas on comlec_xml_id=lecturas.comlec_xml.id and lecturas.comlec_xml.id=".$id_xml." WHERE obs1 is not null and obs1 <> '' and obs1 <> 'NULL' and obs1<>'0' and (lecturao1='' or lecturao1 is null) ";
    
    	$result = $db->query($sql);
    
    	if($result){
    		return $result[0][0]['cantidad'];
    	}
    	return  0;
    }
    
    public function getCountObs1ConLecturaByXml($id_xml=null){
    	$this->setDateSourceCustom();
    	$db = $this->getDataSource();
    
    	$schema = 'lecturas';
    
    	$sql="SELECT COUNT(*) as cantidad FROM ".$schema.".comlec_xml inner join ".$schema.".comlec_ordenlecturas on comlec_xml_id=lecturas.comlec_xml.id and lecturas.comlec_xml.id=".$id_xml." WHERE obs1 is not null and obs1 <> '' and obs1 <> 'NULL' and obs1<>'0' and lecturao1!='' ";
    
    	$result = $db->query($sql);
    
    	if($result){
    		return $result[0][0]['cantidad'];
    	}
    	return  0;
    }
    
    public function getCountObs1IgualAByXml($id_xml=null,$obs=99){
    	$this->setDateSourceCustom();
    	$db = $this->getDataSource();
    
    	$schema = 'lecturas';
    
    	$sql="SELECT COUNT(*) as cantidad FROM ".$schema.".comlec_xml inner join ".$schema.".comlec_ordenlecturas on comlec_xml_id=lecturas.comlec_xml.id and lecturas.comlec_xml.id=".$id_xml." WHERE obs1 ='".$obs."' ";
    
    	$result = $db->query($sql);
    
    	if($result){
    		return $result[0][0]['cantidad'];
    	}
    	return  0;
    }
    
    public function getCountConsumoCeroByXml($id_xml=null){
    	$this->setDateSourceCustom();
    	$db = $this->getDataSource();
    
    	$schema = 'lecturas';
    
    	$sql="SELECT COUNT(*) as cantidad FROM ".$schema.".comlec_xml inner join ".$schema.".comlec_ordenlecturas on comlec_xml_id=lecturas.comlec_xml.id and lecturas.comlec_xml.id=".$id_xml." WHERE montoconsumo1=0 ";
    
    	$result = $db->query($sql);
    
    	if($result){
    		return $result[0][0]['cantidad'];
    	}
    	return  0;
    }
    
    public function getCountConsumoMenoresQueByXml($id_xml=null,$num=35){
    	$this->setDateSourceCustom();
    	$db = $this->getDataSource();
    
    	$schema = 'lecturas';
    
    	$sql="SELECT COUNT(*) as cantidad FROM ".$schema.".comlec_xml inner join ".$schema.".comlec_ordenlecturas on comlec_xml_id=lecturas.comlec_xml.id and lecturas.comlec_xml.id=".$id_xml." WHERE montoconsumo1<".$num." ";
    
    	$result = $db->query($sql);
    
    	if($result){
    		return $result[0][0]['cantidad'];
    	}
    	return  0;
    }
    
    public function getCountConsumoMayoresQueByXml($id_xml=null,$num=1000){
    	$this->setDateSourceCustom();
    	$db = $this->getDataSource();
    
    	$schema = 'lecturas';
    
    	$sql="SELECT COUNT(*) as cantidad FROM ".$schema.".comlec_xml inner join ".$schema.".comlec_ordenlecturas on comlec_xml_id=lecturas.comlec_xml.id and lecturas.comlec_xml.id=".$id_xml." WHERE montoconsumo1>".$num." ";
    
    	$result = $db->query($sql);
    
    	if($result){
    		return $result[0][0]['cantidad'];
    	}
    	return  0;
    }
    
    public function getCountSinLecturayObsByXml($id_xml=null){
    	$this->setDateSourceCustom();
    	$db = $this->getDataSource();
    
    	$schema = 'lecturas';
    
    	$sql="SELECT COUNT(*) as cantidad FROM ".$schema.".comlec_xml inner join ".$schema.".comlec_ordenlecturas on comlec_xml_id=lecturas.comlec_xml.id and lecturas.comlec_xml.id=".$id_xml." WHERE (obs1 is null or obs1 = '' or obs1 = 'NULL' or obs1='0') and (lecturao1='' or lecturao1 is null) ";
    
    	$result = $db->query($sql);
    
    	if($result){
    		return $result[0][0]['cantidad'];
    	}
    	return  0;
    }
    
    public function getCountConsumoMayor100PorcientoQueAnteriorByXml($id_xml=null){
    	$this->setDateSourceCustom();
    	$db = $this->getDataSource();
    
    	$schema = 'lecturas';
    
    	$sql="SELECT COUNT(*) as cantidad FROM ".$schema.".comlec_xml inner join ".$schema.".comlec_ordenlecturas on comlec_xml_id=lecturas.comlec_xml.id and lecturas.comlec_xml.id=".$id_xml." WHERE montoconsumo1 > (consant*2) ";
    
    	$result = $db->query($sql);
    
    	if($result){
    		return $result[0][0]['cantidad'];
    	}
    	return  0;
    }
    
    public function getCountEnviadasDistriluzByXml($id_xml=null){
    	$this->setDateSourceCustom();
    	$db = $this->getDataSource();
    
    	$schema = 'lecturas';
    
    	$sql="SELECT COUNT(*) as cantidad FROM ".$schema.".comlec_xml inner join ".$schema.".comlec_ordenlecturas on comlec_xml_id=lecturas.comlec_xml.id and lecturas.comlec_xml.id=".$id_xml." WHERE resultadoevaluacion='CONSISTENTE' and ws_l_estado='1' ";
    
    	$result = $db->query($sql);
    
    	if($result){
    		return $result[0][0]['cantidad'];
    	}
    	return  0;
    }
    
  public function getLeyendaGraficoDashboard($pfactura){
  	$this->setDateSourceCustom();
  	$db = $this->getDataSource();
  	
 	if(isset($pfactura) and $pfactura!=0){
        $str_where = " AND pfactura = '".$pfactura."'";
    	$schema = 'history';
    	$table = 'comlec_ordenlecturas';
    	$campo = ' , pfactura';
    }else{
        $str_where = '';
        $schema = 'lecturas';
        $table = 'comlec_ordenlecturas1';
        $campo = '';
    }
  	  	
  	//$sql="select idciclo,nombciclo,string_agg(sector, ',') AS SECTORES ".$campo." from (select DISTINCT idciclo,nombciclo,sector ".$campo." from ".$schema.'.'.$table." ) T WHERE 1=1 ". $str_where." group by idciclo,nombciclo ".$campo." ORDER BY nombciclo";
    $sql="select idciclo,string_agg(sector, ',') AS SECTORES ".$campo." from (select DISTINCT idciclo,sector ".$campo." from ".$schema.'.'.$table." ) T WHERE 1=1 ". $str_where." group by idciclo ".$campo."";
  	$result = $db->query($sql);

  	return  $result;
  }
  
  public function  getResumenOrdenesFinalizadasPorLecturistaPorSemana($options=null,$fecha){
  	$this->setDateSourceCustom();
  	$db = $this->getDataSource();
  	
  	$str_where = '';
  	if(isset($options) && is_array($options)){
  		if(isset($options['unidad_neg']) and $options['unidad_neg']!=0){
  			$str_where .= ' AND glomas_unidadnegocio_id = '.$options['unidad_neg'];
  		}
  		if(isset($options['idciclo']) && $options['idciclo']!='0'){
  			$str_where .= " AND idciclo='".$options['idciclo']."'";
  		}
  	}
  	
  	$sql="SELECT e.id lecturista_id, nombre, nomusuario, date_part('dow',fechaejecucion1) as dia_semana,  count(*) as Total FROM lecturas.comlec_ordenlecturas ol 
inner join lecturas.glomas_empleados e on ol.lecturista1_id=e.id 
inner join lecturas.usuarios u on u.glomas_empleado_id = e.id
WHERE (resultadoevaluacion='CONSISTENTE' OR resultadoevaluacion='INCONSISTENTE')
  			AND fechaejecucion1>'".$fecha."' and fechaejecucion1 is not null ".$str_where." 
GROUP BY e.id, nombre, nomusuario, date_part('dow',fechaejecucion1)";
  	 
  	return $db->query($sql);
  }
  
  public function  getResumenOrdenesFinalizadasPorLecturistaPorPeriodo($options=null){
  	$this->setDateSourceCustom();
  	$db = $this->getDataSource();
  	 
  	$str_where = '';
  	if(isset($options) && is_array($options)){
  		if(isset($options['unidad_neg']) and $options['unidad_neg']!=0){
  			$str_where .= ' AND glomas_unidadnegocio_id = '.$options['unidad_neg'];
  		}
  		if(isset($options['idciclo']) && $options['idciclo']!='0'){
  			$str_where .= " AND idciclo='".$options['idciclo']."'";
  		}
  	}
  	 
  	$sql="SELECT e.id lecturista_id, nombre, nomusuario, 
(SELECT count(*) as Total FROM lecturas.comlec_ordenlecturas
WHERE (resultadoevaluacion='CONSISTENTE' OR resultadoevaluacion='INCONSISTENTE') 
and fechaejecucion1 is not null  and up1='1' and e.id=lecturista1_id ".$str_where."
) as Movil, 
(SELECT count(*) as Total FROM lecturas.comlec_ordenlecturas
WHERE (resultadoevaluacion='CONSISTENTE' OR resultadoevaluacion='INCONSISTENTE') 
and up1='2' and e.id=lecturista1_id ".$str_where."
) as Manual
FROM lecturas.comlec_ordenlecturas ol 
inner join lecturas.glomas_empleados e on ol.lecturista1_id=e.id 
inner join lecturas.usuarios u on u.glomas_empleado_id = e.id
WHERE (resultadoevaluacion='CONSISTENTE' OR resultadoevaluacion='INCONSISTENTE') 
and up1 in ('1','2') ".$str_where."
GROUP BY e.id, nombre, nomusuario
ORDER BY 2 ASC";
  
  	return $db->query($sql);
  }
  
  public function  getResumenOrdenesFinalizadasPorDigitadorPorPeriodo($options=null){
  	$this->setDateSourceCustom();
  	$db = $this->getDataSource();
  
  	$str_where = '';
  	if(isset($options) && is_array($options)){
  		if(isset($options['unidad_neg']) and $options['unidad_neg']!=0){
  			$str_where .= ' AND glomas_unidadnegocio_id = '.$options['unidad_neg'];
  		}
  		if(isset($options['idciclo']) && $options['idciclo']!='0'){
  			$str_where .= " AND idciclo='".$options['idciclo']."'";
  		}
  	}
  
  	$sql="SELECT e.id lecturista_id, nombre, nomusuario, count(*) as Total FROM lecturas.comlec_ordenlecturas ol 
inner join lecturas.usuarios u on u.id = ol.usuario_id
inner join lecturas.glomas_empleados e on u.glomas_empleado_id = e.id
WHERE (resultadoevaluacion='CONSISTENTE' OR resultadoevaluacion='INCONSISTENTE') 
and up1 = '2' ".$str_where."
GROUP BY e.id, nombre, nomusuario
ORDER BY 2 ASC";
  
  	return $db->query($sql);
  }
  
  public function getRendimientoConsumo($options=null){
  	$this->setDateSourceCustom();
  	$db = $this->getDataSource();
  	 
  	$str_where = '';
  	if(isset($options) && is_array($options)){
  		if(isset($options['unidad_neg']) and $options['unidad_neg']!=0){
  			$str_where .= ' AND glomas_unidadnegocio_id = '.$options['unidad_neg'];
  		}
  		if(isset($options['idciclo']) && $options['idciclo']!='0'){
  			$str_where .= " AND idciclo='".$options['idciclo']."'";
  		}
  	}
  	 
  	$sql="select glomas_unidadnegocio_id, u.descripcion as nombreunidadnegocio,
  			idciclo, nombciclo, sector, sum(montoconsumo1) as suma_consumo 
from lecturas.comlec_ordenlecturas ol 
left join lecturas.glomas_unidadnegocios u on ol.glomas_unidadnegocio_id=u.id where montoconsumo1 is not null ".$str_where."
group by glomas_unidadnegocio_id, u.descripcion, idciclo, nombciclo, sector
order by 1,2,3,4,5 asc;";
  	
  	return $db->query($sql);
  }
  
  public function getConsumoPorDia($options=null){
  	$this->setDateSourceCustom();
  	$db = $this->getDataSource();
  
  	$str_where = '';
  	if(isset($options) && is_array($options)){
  		if(isset($options['unidad_neg']) and $options['unidad_neg']!=0){
  			$str_where .= ' AND glomas_unidadnegocio_id = '.$options['unidad_neg'];
  		}
  		if(isset($options['idciclo']) && $options['idciclo']!='0'){
  			$str_where .= " AND idciclo='".$options['idciclo']."'";
  		}
  	}
  
  	$sql="select date_trunc('day', fechaasignado1) as fecha, sum(montoconsumo1) as montoconsumo 
from lecturas.comlec_ordenlecturas where 1=1 ".$str_where." 
group by date_trunc('day', fechaasignado1)
order by 1 asc";
  	 
  	return $db->query($sql);
  }
  
  public function getConsumoPorDiaYUbicacion($options=null){
  	$this->setDateSourceCustom();
  	$db = $this->getDataSource();
  
  	$str_where = '';
  	if(isset($options) && is_array($options)){
  		if(isset($options['unidad_neg']) and $options['unidad_neg']!=0){
  			$str_where .= ' AND glomas_unidadnegocio_id = '.$options['unidad_neg'];
  		}
  		if(isset($options['idciclo']) && $options['idciclo']!='0'){
  			$str_where .= " AND idciclo='".$options['idciclo']."'";
  		}
  	}
  
  	$sql="select fecha, idciclo, nombciclo, string_agg(sector, ', ') AS sectores, sum(montoconsumo) as montoconsumo  from (
select date_trunc('day', fechaasignado1) as fecha, idciclo, nombciclo, sector, sum(montoconsumo1) as montoconsumo 
from lecturas.comlec_ordenlecturas where 1=1 ".$str_where." 
group by date_trunc('day', fechaasignado1), idciclo, nombciclo, sector
order by 1 asc) T GROUP BY fecha, idciclo, nombciclo order by 1 asc";
  
  	return $db->query($sql);
  }
  
  public function getConsumoPorDiaYUbicacionYXml($options=null){
  	$this->setDateSourceCustom();
  	$db = $this->getDataSource();
  
  	$str_where = '';
  	if(isset($options) && is_array($options)){
  		if(isset($options['unidad_neg']) and $options['unidad_neg']!=0){
  			$str_where .= ' AND glomas_unidadnegocio_id = '.$options['unidad_neg'];
  		}
  		if(isset($options['idciclo']) && $options['idciclo']!='0'){
  			$str_where .= " AND idciclo='".$options['idciclo']."'";
  		}
  	}
  
  	$sql="select glomas_unidadnegocio_id, u.descripcion as unidad_negocio, date_trunc('day', fechaasignado1) as fecha, idciclo, nombciclo, sector, comlec_xml_id, x.vdescripcion as nombre_xml, sum(montoconsumo1) as montoconsumo, count(*)  as total_suministros
from lecturas.comlec_ordenlecturas  ol
inner join lecturas.glomas_unidadnegocios u on ol.glomas_unidadnegocio_id=u.id 
inner join lecturas.comlec_xml x on x.id=ol.comlec_xml_id
where 1=1 ".$str_where." 
group by glomas_unidadnegocio_id, u.descripcion, date_trunc('day', fechaasignado1), idciclo, nombciclo, sector, comlec_xml_id, x.vdescripcion
order by 1, 2, 3 asc";
  
  	return $db->query($sql);
  }
  
  public function getResumenPorCiclos($options=null){
  	$this->setDateSourceCustom();
  	$db = $this->getDataSource();
  	 
  	$str_where = '';
  	if(isset($options) && is_array($options)){
  		if(isset($options['unidad_neg']) and $options['unidad_neg']!=0){
  			$str_where .= ' AND glomas_unidadnegocio_id = '.$options['unidad_neg'];
  		}
  		if(isset($options['idciclo']) && $options['idciclo']!='0'){
  			if(is_array($options['idciclo'])){
  				$str_where .= " AND idciclo in('".implode("','",$options['idciclo'])."')";
  			}else{
  				$str_where .= " AND idciclo='".$options['idciclo']."'";
  			}
  		}
  		if(isset($options['idsector']) && $options['idsector']!='0'){
  			if(is_array($options['idsector'])){
  				$str_where .= " AND sector in('".implode("','",$options['idsector'])."')";
  			}else{
  				$str_where .= " AND sector='".$options['idsector']."'";
  			}
  		}
  	}
  	 
  	$sql="select idciclo, nombciclo, string_agg(sector, ', ') AS sectores, 
(SELECT COUNT(*) as cantidad FROM lecturas.comlec_ordenlecturas where idciclo=T.idciclo ".$str_where.") as suministros ,
(SELECT COUNT(*) as cantidad FROM lecturas.comlec_ordenlecturas WHERE lecturista1_id is not null and idciclo=T.idciclo ".$str_where.") as asignados,
(SELECT COUNT(*) as cantidad FROM lecturas.comlec_ordenlecturas WHERE lecturista1_id is not null and down1='1' and idciclo=T.idciclo ".$str_where.") as descargados,
(SELECT COUNT(*) as cantidad FROM lecturas.comlec_ordenlecturas WHERE (resultadoevaluacion='CONSISTENTE' OR resultadoevaluacion='INCONSISTENTE') and idciclo=T.idciclo ".$str_where.") as finalizados,
(SELECT COUNT(*) as cantidad FROM lecturas.comlec_ordenlecturas WHERE resultadoevaluacion='INCONSISTENTE' and idciclo=T.idciclo ".$str_where.") as inconsistentes_actuales,
(SELECT COUNT(*) as cantidad FROM lecturas.comlec_ordenlecturas WHERE validacion>='1' and idciclo=T.idciclo ".$str_where.") as inconsistentes_evaluadas,
(SELECT COUNT(obs1) as cantidad FROM lecturas.comlec_ordenlecturas WHERE obs1 is not null and obs1 <> '' and obs1 <> 'NULL' and obs1<>'0' and lecturao1!='' and idciclo=T.idciclo ".$str_where.") as observaciones_con_lectura,
(SELECT COUNT(obs1) as cantidad FROM lecturas.comlec_ordenlecturas WHERE obs1 is not null and obs1 <> '' and obs1 <> 'NULL' and obs1<>'0' and (lecturao1='' or lecturao1 is null) and idciclo=T.idciclo ".$str_where.") as observaciones_sin_lectura,
(SELECT COUNT(*) as cantidad FROM lecturas.comlec_ordenlecturas WHERE montoconsumo1=0 and idciclo=T.idciclo ".$str_where.") as consumo_cero,
(SELECT COUNT(*) as cantidad FROM lecturas.comlec_ordenlecturas WHERE montoconsumo1<35 and idciclo=T.idciclo ".$str_where.") as consumo_menor_35,
(SELECT COUNT(*) as cantidad FROM lecturas.comlec_ordenlecturas WHERE montoconsumo1>1000 and idciclo=T.idciclo ".$str_where.") as consumo_mayor_1000,
(SELECT COUNT(*) as cantidad FROM lecturas.comlec_ordenlecturas WHERE montoconsumo1 > (consant*2) and idciclo=T.idciclo ".$str_where.") as consumo_mayor_100_porciento,
(SELECT COUNT(*) as cantidad FROM lecturas.comlec_ordenlecturas WHERE ws_l_estado='1' and idciclo=T.idciclo ".$str_where.") as enviados_distriluz 
from (select DISTINCT idciclo, nombciclo, sector  from lecturas.comlec_ordenlecturas where 1=1 ".$str_where.") T group by idciclo, nombciclo";
  	
  	return $db->query($sql);
  }
  
  public function getCiclosActuales($options=null,$fecha=null){
  	$this->setDateSourceCustom();
  	$db = $this->getDataSource();
  
  	$str_where = '';
  	if(isset($options) && is_array($options)){
  		if(isset($options['unidad_neg']) and $options['unidad_neg']!=0){
  			$str_where .= ' AND glomas_unidadnegocio_id = '.$options['unidad_neg'];
  		}
  		if(isset($options['idciclo']) && $options['idciclo']!='0'){
  			$str_where .= " AND idciclo='".$options['idciclo']."'";
  		}
  	}
  	
  	if(isset($fecha)){
  		$str_where .= " AND fechaasignado1 >= '".$fecha."'";
  	}else{
  		$str_where .= " AND fechaasignado1 >= '".date('Y-m-d')."'";
  	}
  
  	$sql="select DISTINCT idciclo, nombciclo  from lecturas.comlec_ordenlecturas where 1=1 ".$str_where."";
  	 
  	return $db->query($sql);
  }
  
  public function getCountSuministrosPorMetodo($options=null){
  	$this->setDateSourceCustom();
  	$db = $this->getDataSource();
  
  	$str_where = '';
  	if(isset($options) && is_array($options)){
  		if(isset($options['unidad_neg']) and $options['unidad_neg']!=0){
  			$str_where .= ' AND glomas_unidadnegocio_id = '.$options['unidad_neg'];
  		}
  		if(isset($options['idciclo']) && $options['idciclo']!='0'){
  			$str_where .= " AND idciclo='".$options['idciclo']."'";
  		}
  	}
  
  	$sql="select metodofinal1, count(*)  as total from lecturas.comlec_ordenlecturas WHERE 1=1 ".$str_where." GROUP BY metodofinal1 ";
  
  	return $db->query($sql);
  }
  
  public function listar_ordenes_historico($criterio,$filtros=null,$filter_advance,$page,$limit,$search=null,$consumo_min=null,$consumo_max=null){
  	ini_set('memory_limit', '-1');
  	$this->setDateSourceCustom();
  	$db = $this->getDataSource();
  	$esquema=$this->esquema;
  	$bool_inconsistencia = false;
  
  	$str_fields = 'pfactura, tipolectura, comlec_ordenlectura_id, orden, ordenruta, libro, lcorrelati, seriefab,cliente,direccion,suministro,lecturao1,obs1,montoconsumo1, resultadoevaluacion, e1.nombre as lecturista1, e2.nombre as lecturista2, metodofinal1, metodofinal2, metodofinal3,
  			 idciclo, nombciclo, sector, ruta, nombruta, fechaejecucion1, fechaasignado1, ordtrabajo, COUNT(*) OVER() as row_total';
  
  	if ( isset( $page ) && isset($limit) ){
  		$str_order = " ORDER BY orden, ordenruta";
  		$str_limit = " LIMIT ".intval( $limit )." OFFSET ".intval( $page );
  	}else{
  		$str_order = " ORDER BY orden, ordenruta";
  		$str_limit = "";
  	}
  
  	$str_where = " ";
  	// Listar por pfactura
  	if(isset($filtros['pfactura'])){
  		$str_where .= " AND pfactura = '".$filtros['pfactura']."' ";
  		$esquema = "history";
  	}
  
  	$str_join = " left join glomas_empleados e1 on ol.lecturista1_id = e1.id ";
  	$str_join .= " left join glomas_empleados e2 on ol.lecturista2_id = e2.id ";
  		
  	$sql="select ".$str_fields." from ".$esquema.".comlec_ordenlecturas ol ".$str_join." where 1=1";
  
  	if(strpos($criterio, 'F') !== false){
  		$str_where .= " AND cast(ol.fechaasignado1 as date)  >= '".$filtros['date_start']."' AND cast(ol.fechaasignado1 as date)  <= '".$filtros['date_end']." 23:59:59'";
  	}
  	if(strpos($criterio, 'U') !== false){
  		if(isset($filtros['unidadneg']) && $filtros['unidadneg']!=0 && $filtros['unidadneg']!=''){
  			$str_where .= " AND glomas_unidadnegocio_id = '".$filtros['unidadneg']."'";
  		}
  		if(isset($filtros['ciclo']) && $filtros['ciclo']!=0 && $filtros['ciclo']!=''){
  			$str_where .= " AND idciclo = '".$filtros['ciclo']."'";
  		}
  		if(isset($filtros['sector']) && $filtros['sector']!=0 && $filtros['sector']!=''){
  			$str_where .= " AND sector = '".$filtros['sector']."'";
  		}
  		if(isset($filtros['ruta']) && $filtros['ruta']!=0 && $filtros['ruta']!=''){
  			$str_where .= " AND ruta = '".$filtros['ruta']."'";
  		}
  	}
  	if(strpos($criterio, 'L') !== false){
  		$str_where .= " AND lecturista1_id = '".$filtros['lecturista']."' ";
  	}
  	if(strpos($criterio, 'S') !== false){
  		//$str_where .= " AND CAST(suministro AS text) ilike '%".$filtros['suministro']."%' ";
  		if($filtros['suministro_orden'] != '0'){
  			$str_where .= " AND orden>=".$filtros['suministro_orden'];
  		}else{
  			$str_where .= " AND orden<".$filtros['suministro_orden'];
  			// Para que no encuentre resultados en caso que suministro sea cero.
  		}
  		if(isset($filtros['sector']) && $filtros['sector']!=0 && $filtros['sector']!=''){
  			$str_where .= " AND sector = '".$filtros['sector']."'";
  		}
  		if(isset($filtros['ruta']) && $filtros['ruta']!=0 && $filtros['ruta']!=''){
  			$str_where .= " AND ruta = '".$filtros['ruta']."'";
  		}
  	}
  	// Listar por id_xml
  	if(isset($filtros['id_xml'])){
  		$str_where .= " AND comlec_xml_id = '".$filtros['id_xml']."' ";
  	}
  
  	if ( isset($search) && $search!=''){
  		$str_where .= " AND (";
  		$str_where .= " CAST(suministro AS text) ilike '%".$search."%' ";
  		$str_where .= " ) ";
  	}
  
  	if ( isset($filter_advance) && $filter_advance!='0' ){
  		$arr_filters = explode(',',$filter_advance);
  			
  		foreach ($arr_filters as $k => $filter){
  			if($filter=='LSC'){//Pendiente
  				$str_where .= " AND (lecturao1 IS NULL OR lecturao1='') AND (obs1 IS NULL OR obs1='') ";
  			}
  			if($filter=='OBSSL'){//Observaciones sin lectura
  				$str_where .= " AND (lecturao1 IS NULL OR lecturao1='') AND (obs1 IS NOT NULL AND obs1<>'') ";
  			}
  			if($filter=='OBSCL'){//Observaciones con lectura
  				$str_where .= " AND (lecturao1 IS NOT NULL AND lecturao1<>'') AND (obs1 IS NOT NULL AND obs1<>'') ";
  			}
  			if($filter=='OBS99'){//Observaciones 99
  				$str_where .= " AND obs1 = '99' ";
  			}
  			if($filter=='LAMA'){//Lectura Actual Menor que la Anterior
  				$str_where .= " AND (lecturao1<>'' AND CAST(lecturao1 AS numeric) < lecant) ";
  			}
  			if($filter=='CFR'){//Consumo Fuera de Rango
  				$str_where .= " AND (montoconsumo1 < minima AND montoconsumo1 > maxima) ";
  			}
  			if($filter=='NS'){//Nuevo Suministro
  				//$str_where .= " AND pinicio = pfactura ";
  			}
  			if($filter=='OBSSC'){//Con Observacion sin correccion
  				$str_where .= " AND (obs1 IS NOT NULL AND obs1<>'') AND validacion = '0' ";
  			}
  			if($filter=='CORR'){//Corregidas
  				$str_where .= " AND validacion >= '1' ";
  			}
  			if($filter=='I'){//Inconsistentes
  				$str_where .= " AND resultadoevaluacion = 'INCONSISTENTE' ";
  			}
  			if($filter=='CC'){//Consumo cero
  				$str_where .= " AND montoconsumo1 = 0 ";
  			}
  			if($filter=='C50MA'){//Consumo 50% menor que el anterior
  				$str_where .= " AND montoconsumo1 < (consant / 2) ";
  			}
  			if($filter=='C100MA'){//Consumo 100% mayor que el anterior
  				$str_where .= " AND montoconsumo1 > (consant * 2) ";
  			}
  			if($filter=='CN'){//Clientes Nuevos
  				$str_where .= " AND pinicio = pfactura ";
  			}
  
  			$bool_inconsistencia = false;
  		}
  	}
  
  	if ( isset($consumo_min) ){
  		$str_where .= " AND montoconsumo1 >= $consumo_min ";
  		$bool_inconsistencia = false;
  	}
  
  	if ( isset($consumo_max) ){
  		$str_where .= " AND montoconsumo1 <= $consumo_max ";
  		$bool_inconsistencia = false;
  	}
  
  	if($bool_inconsistencia){
  		$str_where .= " AND (resultadoevaluacion IS NULL OR resultadoevaluacion = 'INCONSISTENTE' OR  resultadoevaluacion = '') ";
  	}
  	//echo $sql.$str_where.$str_order.$str_limit;exit;
  	return  $db->query($sql.$str_where.$str_order.$str_limit);
  }
  
  public function listar_reclamos($idciclo,$factor,$page,$limit,$search=''){
  	$this->setDateSourceCustom();
  	$db = $this->getDataSource();
  	$esquema=$this->esquema;
  	
  	if ( isset( $page ) && isset($limit) ){
  		$str_limit = " LIMIT ".intval( $limit )." OFFSET ".intval( $page );
  	}else{
  		$str_limit = "";
  	}
  	
  	$str_order = " ";
  	$str_where = " ";
  	if(isset($idciclo)){
  		$str_where .= " AND idciclo = '".$idciclo."' ";
  	}
  	
  	if(isset($search) && $search!=''){
  		$str_where .= " AND suministro = '".$search."' ";
  	}
  	
  	$str_where .= " AND ( ((consant < 0 or consant >10) and (montoconsumo1 < 0 or montoconsumo1 >30) )
  			OR ((consant < 11 or consant >30) and (montoconsumo1 < 0 or montoconsumo1 >60) ) )";
  	//1.- No generar carta a todos los casos que la lectura actual sea menor a la anterior. (Jeny Sensey)
  	$str_where .= " AND cast(lecturao1 as numeric) > lecant ";
  	//2.- No generar los consumos actuales menores o iguales a 50Kw (Jeny Sensey)
  	$str_where .= " AND montoconsumo1 > 50 ";
  	//3.- No generar a los clientes cuyo pinicio sea el proceso anterior. (Jeny Sensey)
  	if(date('d')<=10){
  		$mes = date('m')-1;
  	}else{
  		$mes = date('m');
  	}
  	if($mes==1){
  		$anio = date('Y')-1;
  		$mes = 12;
  	}else{
  		$anio = date('Y');
  	}
  	$pfactura_anterior = $anio.''.str_pad($mes, 2, "0", STR_PAD_LEFT);  	
  	$str_where .= " AND pinicio < '".$pfactura_anterior."' ";
  	
  	//4.- No generar a los clientes cuyo lectura anterior es cero. (Jeny Sensey)
  	$str_where .= " AND lecant <> 0 ";
  	 
  	//5.- No generar a los clientes cuyo lectura anterior es uno. (Jeny Sensey)
  	$str_where .= " AND lecant <> 1 ";
  	 
  	//6.- No generar a los clientes cuya lectura vacia se envio cero en el mes de diciembre 2014. (Jeny Sensey)
  	$str_where .= " AND suministro not in (select suministro from history.comlec_ordenlecturas
where pfactura='201412' and glomas_unidadnegocio_id in(26,30) and ws_l_cadenaenvio like '%l=\"0\"%' and (lecturao1 is null or lecturao1='')
and suministro not in (SELECT suministro from history.comlec_ordenlecturas where pfactura='201501' and lecturao1<>'' and glomas_unidadnegocio_id in(26,30))) ";
  	
  	if(isset($factor)){
  		$str_factor = " ,(maxima + ((maxima*".$factor.")/100)) as factor_correccion";
  	}else{
  		$str_factor = " ,(maxima) as factor_correccion";
  	}
  	
  	$sql = "select *, case when T.mes_actual > T.maxima then 1 else 0 end as target, 
case when T.mes_actual > T.factor_correccion then 1 else 0 end as target2, COUNT(*) OVER() as row_total from 
(select ol.comlec_ordenlectura_id, pfactura, ol.glomas_unidadnegocio_id, suministro, idciclo, promedio, lecant, lecturao1, consant as mes_anterior, montoconsumo1 as mes_actual, 
ol.metodofinal1, maxima, foto1 ".$str_factor."
from lecturas.comlec_ordenlecturas as ol
inner join lecturas.comlec_lecturametodo le on le.comlec_ordenlectura_id=ol.comlec_ordenlectura_id and le.comlec_metodo_id=ol.metodofinal1
where 1=1 ".$str_where." and montoconsumo1 is not null and foto1<>''
) T where (case when T.mes_actual > T.factor_correccion then 1 else 0 end = 1) 
		order by T.comlec_ordenlectura_id asc";
  	
  	//echo $sql.$str_order.$str_limit;exit();
  	$arr_result = $db->query($sql.$str_order.$str_limit);
  	return $arr_result;
  }
  
  public function getCountReclamosTarget1($idciclo){
  	$this->setDateSourceCustom();
  	$db = $this->getDataSource();
  	$esquema=$this->esquema;
  	
  	$str_where = " ";
  	if(isset($idciclo)){
  		$str_where .= " AND idciclo = '".$idciclo."' ";
  	}
  	
  	$str_where .= " AND ( ((consant < 0 or consant >10) and (montoconsumo1 < 0 or montoconsumo1 >30) )
  			OR ((consant < 11 or consant >30) and (montoconsumo1 < 0 or montoconsumo1 >60) ) )";
  	//1.- No generar carta a todos los casos que la lectura actual sea menor a la anterior. (Jeny Sensey)
  	$str_where .= " AND cast(lecturao1 as numeric) > lecant ";
  	//2.- No generar los consumos actuales menores o iguales a 50Kw (Jeny Sensey)
  	$str_where .= " AND montoconsumo1 > 50 ";
  	//3.- No generar a los clientes cuyo pinicio sea el proceso anterior. (Jeny Sensey)
  	if(date('d')<=10){
  		$mes = date('m')-1;
  	}else{
  		$mes = date('m');
  	}
  	if($mes==1){
  		$anio = date('Y')-1;
  		$mes = 12;
  	}else{
  		$anio = date('Y');
  	}
  	$pfactura_anterior = $anio.''.str_pad($mes, 2, "0", STR_PAD_LEFT);
  	$str_where .= " AND pinicio < '".$pfactura_anterior."' ";
  	
  	//4.- No generar a los clientes cuyo lectura anterior es cero. (Jeny Sensey)
  	$str_where .= " AND lecant <> 0 ";
  	
  	//5.- No generar a los clientes cuyo lectura anterior es uno. (Jeny Sensey)
  	$str_where .= " AND lecant <> 1 ";
  	
  	//6.- No generar a los clientes cuya lectura vacia se envio cero en el mes de diciembre 2014. (Jeny Sensey)
  	$str_where .= " AND suministro not in (select suministro from history.comlec_ordenlecturas 
where pfactura='201412' and glomas_unidadnegocio_id in(26,30) and ws_l_cadenaenvio like '%l=\"0\"%' and (lecturao1 is null or lecturao1='')
and suministro not in (SELECT suministro from history.comlec_ordenlecturas where pfactura='201501' and lecturao1<>'' and glomas_unidadnegocio_id in(26,30))) ";
  	 
  	$sql = "select COUNT(*) as cantidad from
(select pfactura, suministro, idciclo, consant as mes_anterior, montoconsumo1 as mes_actual,
ol.metodofinal1, maxima
from lecturas.comlec_ordenlecturas as ol
inner join lecturas.comlec_lecturametodo le on le.comlec_ordenlectura_id=ol.comlec_ordenlectura_id and le.comlec_metodo_id=ol.metodofinal1
where 1=1 ".$str_where." and montoconsumo1 is not null and foto1<>''
) T where (case when T.mes_actual > T.maxima then 1 else 0 end = 1) ";
  	 
  	$result = $db->query($sql);
  	if($result){
    	return $result[0][0]['cantidad'];
    }
    return  0;
  }
  
  public function getCountReclamosTarget2($idciclo,$factor){
  	$this->setDateSourceCustom();
  	$db = $this->getDataSource();
  	$esquema=$this->esquema;
  	 
  	$str_where = " ";
  	if(isset($idciclo)){
  		$str_where .= " AND idciclo = '".$idciclo."' ";
  	}
  	
  	$str_where .= " AND ( ((consant < 0 or consant >10) and (montoconsumo1 < 0 or montoconsumo1 >30) )
  			OR ((consant < 11 or consant >30) and (montoconsumo1 < 0 or montoconsumo1 >60) ) )";
  	//1.- No generar carta a todos los casos que la lectura actual sea menor a la anterior. (Jeny Sensey)
  	$str_where .= " AND cast(lecturao1 as numeric) > lecant ";
  	//2.- No generar los consumos actuales menores o iguales a 50Kw (Jeny Sensey)
  	$str_where .= " AND montoconsumo1 > 50 ";
  	//3.- No generar a los clientes cuyo pinicio sea el proceso anterior. (Jeny Sensey)
  	if(date('d')<=10){
  		$mes = date('m')-1;
  	}else{
  		$mes = date('m');
  	}
  	if($mes==1){
  		$anio = date('Y')-1;
  		$mes = 12;
  	}else{
  		$anio = date('Y');
  	}
  	$pfactura_anterior = $anio.''.str_pad($mes, 2, "0", STR_PAD_LEFT);
  	$str_where .= " AND pinicio < '".$pfactura_anterior."' ";
  	
  	//4.- No generar a los clientes cuyo lectura anterior es cero. (Jeny Sensey)
  	$str_where .= " AND lecant <> 0 ";
  	
  	//5.- No generar a los clientes cuyo lectura anterior es uno. (Jeny Sensey)
  	$str_where .= " AND lecant <> 1 ";
  	
  	//6.- No generar a los clientes cuya lectura vacia se envio cero en el mes de diciembre 2014. (Jeny Sensey)
  	$str_where .= " AND suministro not in (select suministro from history.comlec_ordenlecturas
where pfactura='201412' and glomas_unidadnegocio_id in(26,30) and ws_l_cadenaenvio like '%l=\"0\"%' and (lecturao1 is null or lecturao1='')
and suministro not in (SELECT suministro from history.comlec_ordenlecturas where pfactura='201501' and lecturao1<>'' and glomas_unidadnegocio_id in(26,30))) ";
  	
  	if(isset($factor)){
  		$str_factor = " ,(maxima + ((maxima*".$factor.")/100)) as factor_correccion";
  	}else{
  		$str_factor = " ,(maxima) as factor_correccion";
  	}  
  
  	$sql = "select COUNT(*) as cantidad from 
(select pfactura, suministro, idciclo, consant as mes_anterior, montoconsumo1 as mes_actual, 
ol.metodofinal1, maxima ".$str_factor."
from lecturas.comlec_ordenlecturas as ol
inner join lecturas.comlec_lecturametodo le on le.comlec_ordenlectura_id=ol.comlec_ordenlectura_id and le.comlec_metodo_id=ol.metodofinal1
where 1=1 ".$str_where." and montoconsumo1 is not null and foto1<>''
) T where (case when T.mes_actual > T.factor_correccion then 1 else 0 end = 1) ";
  
  	$result = $db->query($sql);
  	if($result){
  		return $result[0][0]['cantidad'];
  	}
  	return  0;
  }
  /*
   * Insertar observaciones al historico de observaciones
   * @suministro int
   * @pfactura int
   * @observacion string
   * @autor Alan Hugo
   */
  public function insertarReporteHistoricoComentario($suministro, $pfactura, $comentario, $imagen, $idusuario, $client_ip){
  	$this->setDateSourceCustom ();
  	$db = $this->getDataSource();
  	$fecha = date("Y-m-d H:i:s");
  	$sql = "INSERT INTO lecturas.comlec_ordenlecturas_comentarios (pfactura,suministro,comentario,fecha,imagen,idusuario,client_ip) VALUES ('$pfactura','$suministro','$comentario','$fecha','$imagen','$idusuario','$client_ip')";
  	$db->query($sql);
  	return true;
  }
  
  public function listarReporteHistoricoComentario($suministro, $pfactura){
  	$this->setDateSourceCustom ();
  	$db = $this->getDataSource();
  	$fecha = date("Y-m-d H:i:s");
  	$sql = "SELECT * FROM lecturas.comlec_ordenlecturas_comentarios as lolc INNER JOIN lecturas.usuarios as usu ON usu.id = lolc.idusuario INNER JOIN glomas_empleados as ge ON usu.glomas_empleado_id = ge.id WHERE pfactura='$pfactura' and suministro='$suministro'";
  	return  $db->query($sql);
  	return true;
  }
  
  public function listarCronograma($criterio=null,$date_start=null,$date_end=null,$unidadneg=null,$ciclo=null,$sector=null){
  	$this->setDateSourceCustom();
  	$db = $this->getDataSource();
  	 
  	$filtro_fecha = "";
  	$str_sector = "";
  	if(strpos($criterio, 'F') !== false){
  		$filtro_fecha .= " AND cast(fecha as date) >= '$date_start' AND cast(fecha as date)  <= '$date_end 23:59:59'";
  	}
  	if(strpos($criterio, 'U') !== false){
  		if(isset($unidadneg) && $unidadneg!=0 && $unidadneg!=''){
  			$filtro_fecha .= " AND glomas_unidadnegocio_id = '".$unidadneg."'";
  		}
  		if(isset($ciclo) && $ciclo!=0 && $ciclo!=''){
  			$filtro_fecha .= " AND idciclo = '".$ciclo."'";
  		}
  		if(isset($sector) && $sector!=0 && $sector!=''){
  			$filtro_fecha .= " AND idsector = '".$sector."'";
  		}
  	}
  	 
  	$sql="select * from lecturas.comlec_cronograma where 1=1 ".$filtro_fecha." order by fecha asc, idciclo asc, idsector asc";
  
  	return  $db->query($sql);
  }
  
  /**
   * Insertar cronograma from xls
   * @param unknown $conceptosByGrupo
   * @param unknown $idgrupo
   * @param unknown $xml
   * @param unknown $nombrearchivo
   * @author Geynen
   * @version 29 Dic 2014
   */
  public function insertarXlsCronograma($conceptosByGrupo,$idgrupo,$xls,$nombrearchivo,$usuario_id,$client_ip){
  	$this->setDateSourceCustom ();
  	$db = $this->getDataSource();
  
  	$db->begin();
  	
  	$str_sql_head_insert = 'INSERT INTO lecturas.comlec_cronograma(
            glomas_unidadnegocio_id, pfactura, idciclo, idsector, fecha, usuario_id, client_ip, created)
    		VALUES ';
  	$str_sql_body_insert = '';
  	$i=0;
  	foreach ($xls as $key=>$value) {
  		$i++;
  		if ($i>1){
  			$obj_evento_cronograma = $this->existeEventoCronograma($value[1],$value[2],$value[3],$value[4]);
  			if(count($obj_evento_cronograma)>0){
  				$db->query("DELETE FROM lecturas.comlec_cronograma WHERE id=".$obj_evento_cronograma[0][0]['id']);
  			}  				
  			$str_sql_body_insert .= "('".$value[1]."','".$value[2]."','".$value[3]."','".$value[4]."','".$value[5]."',".$usuario_id.",'".$client_ip."','".date('Y-m-d H:i:s')."'),";
  		}
  	}
  	
  	if($i>0){
  		unset ($i);
  		$sql_multiple = $str_sql_head_insert.substr($str_sql_body_insert,0,-1).";";
  		//echo $sql_multiple;exit;
  		$db->query($sql_multiple);
  		return $db->commit();
  	}else{
  		return false;
  	}
  
  }
  
  public function listarCronogramaById($id_cronograma){
  
  	$this->setDateSourceCustom();
  	$db = $this->getDataSource();
  	$sql="select * from lecturas.comlec_cronograma where id=".$id_cronograma."";
  	return  $db->query($sql);
  }
  
  public function getCountSuministrosByCronograma($id_cronograma=null){
  	$this->setDateSourceCustom();
  	$db = $this->getDataSource();
  
  	$schema = 'lecturas';
  
  	$sql="SELECT COUNT(*) as cantidad FROM ".$schema.".comlec_ordenlecturas where 
  			idciclo in (select idciclo from lecturas.comlec_cronograma cro where id=".$id_cronograma.")
  			and sector in (select idsector from lecturas.comlec_cronograma cro where id=".$id_cronograma.") ";
  
  	$result = $db->query($sql);
  
  	if($result){
  		return $result[0][0]['cantidad'];
  	}
  	return  0;
  }
  
  public function getCountAsignadosByCronograma($id_cronograma=null){
  	$this->setDateSourceCustom();
  	$db = $this->getDataSource();
  
  	$schema = 'lecturas';
  
  	$sql="SELECT COUNT(*) as cantidad FROM ".$schema.".comlec_ordenlecturas 
  			WHERE lecturista1_id is not null and 
  			idciclo in (select idciclo from lecturas.comlec_cronograma cro where id=".$id_cronograma.")
  			and sector in (select idsector from lecturas.comlec_cronograma cro where id=".$id_cronograma.")";
  
  	$result = $db->query($sql);
  
  	if($result){
  		return $result[0][0]['cantidad'];
  	}
  	return  0;
  }
  
  public function getCountDescargadosByCronograma($id_cronograma=null){
  	$this->setDateSourceCustom();
  	$db = $this->getDataSource();
  
  	$schema = 'lecturas';
  
  	$sql="SELECT COUNT(*) as cantidad FROM ".$schema.".comlec_ordenlecturas WHERE lecturista1_id is not null and down1 in ('1','2') and 
  			idciclo in (select idciclo from lecturas.comlec_cronograma cro where id=".$id_cronograma.")
  			and sector in (select idsector from lecturas.comlec_cronograma cro where id=".$id_cronograma.")";
  
  	$result = $db->query($sql);
  
  	if($result){
  		return $result[0][0]['cantidad'];
  	}
  	return  0;
  }
  
  public function getCountFinalizadosByCronograma($id_cronograma=null){
  	$this->setDateSourceCustom();
  	$db = $this->getDataSource();
  
  	$schema = 'lecturas';
  
  	$sql="SELECT COUNT(*) as cantidad FROM ".$schema.".comlec_ordenlecturas WHERE (resultadoevaluacion='CONSISTENTE' OR resultadoevaluacion='INCONSISTENTE') and 
  			idciclo in (select idciclo from lecturas.comlec_cronograma cro where id=".$id_cronograma.")
  			and sector in (select idsector from lecturas.comlec_cronograma cro where id=".$id_cronograma.")";
  
  	$result = $db->query($sql);
  
  	if($result){
  		return $result[0][0]['cantidad'];
  	}
  	return  0;
  }
  
  public function getCountConsistentesByCronograma($id_cronograma=null){
  	$this->setDateSourceCustom();
  	$db = $this->getDataSource();
  
  	$schema = 'lecturas';
  
  	$sql="SELECT COUNT(*) as cantidad FROM ".$schema.".comlec_ordenlecturas WHERE resultadoevaluacion='CONSISTENTE' and 
  			idciclo in (select idciclo from lecturas.comlec_cronograma cro where id=".$id_cronograma.")
  			and sector in (select idsector from lecturas.comlec_cronograma cro where id=".$id_cronograma.")";
  
  	$result = $db->query($sql);
  
  	if($result){
  		return $result[0][0]['cantidad'];
  	}
  	return  0;
  }
  
  public function getCountInconsistentesByCronograma($id_cronograma=null){
  	$this->setDateSourceCustom();
  	$db = $this->getDataSource();
  
  	$schema = 'lecturas';
  
  	$sql="SELECT COUNT(*) as cantidad FROM ".$schema.".comlec_ordenlecturas WHERE resultadoevaluacion='INCONSISTENTE' and 
  			idciclo in (select idciclo from lecturas.comlec_cronograma cro where id=".$id_cronograma.")
  			and sector in (select idsector from lecturas.comlec_cronograma cro where id=".$id_cronograma.")";
  
  	$result = $db->query($sql);
  
  	if($result){
  		return $result[0][0]['cantidad'];
  	}
  	return  0;
  }
  
  public function getCountObs1SinLecturaByCronograma($id_cronograma=null){
  	$this->setDateSourceCustom();
  	$db = $this->getDataSource();
  
  	$schema = 'lecturas';
  
  	$sql="SELECT COUNT(*) as cantidad FROM ".$schema.".comlec_ordenlecturas WHERE obs1 is not null and obs1 <> '' and obs1 <> 'NULL' and obs1<>'0' and (lecturao1='' or lecturao1 is null) and 
  			idciclo in (select idciclo from lecturas.comlec_cronograma cro where id=".$id_cronograma.")
  			and sector in (select idsector from lecturas.comlec_cronograma cro where id=".$id_cronograma.")";
  
  	$result = $db->query($sql);
  
  	if($result){
  		return $result[0][0]['cantidad'];
  	}
  	return  0;
  }
  
  public function getCountObs1ConLecturaByCronograma($id_cronograma=null){
  	$this->setDateSourceCustom();
  	$db = $this->getDataSource();
  
  	$schema = 'lecturas';
  
  	$sql="SELECT COUNT(*) as cantidad FROM ".$schema.".comlec_ordenlecturas WHERE obs1 is not null and obs1 <> '' and obs1 <> 'NULL' and obs1<>'0' and lecturao1!='' and 
  			idciclo in (select idciclo from lecturas.comlec_cronograma cro where id=".$id_cronograma.")
  			and sector in (select idsector from lecturas.comlec_cronograma cro where id=".$id_cronograma.")";
  
  	$result = $db->query($sql);
  
  	if($result){
  		return $result[0][0]['cantidad'];
  	}
  	return  0;
  }
  
  public function getCountObs1IgualAByCronograma($id_cronograma=null,$obs=99){
  	$this->setDateSourceCustom();
  	$db = $this->getDataSource();
  
  	$schema = 'lecturas';
  
  	$sql="SELECT COUNT(*) as cantidad FROM ".$schema.".comlec_ordenlecturas WHERE obs1 ='".$obs."' and 
  			idciclo in (select idciclo from lecturas.comlec_cronograma cro where id=".$id_cronograma.")
  			and sector in (select idsector from lecturas.comlec_cronograma cro where id=".$id_cronograma.")";
  
  	$result = $db->query($sql);
  
  	if($result){
  		return $result[0][0]['cantidad'];
  	}
  	return  0;
  }
  
  public function getCountConsumoCeroByCronograma($id_cronograma=null){
  	$this->setDateSourceCustom();
  	$db = $this->getDataSource();
  
  	$schema = 'lecturas';
  
  	$sql="SELECT COUNT(*) as cantidad FROM ".$schema.".comlec_ordenlecturas WHERE montoconsumo1=0 and 
  			idciclo in (select idciclo from lecturas.comlec_cronograma cro where id=".$id_cronograma.")
  			and sector in (select idsector from lecturas.comlec_cronograma cro where id=".$id_cronograma.")";
  
  	$result = $db->query($sql);
  
  	if($result){
  		return $result[0][0]['cantidad'];
  	}
  	return  0;
  }
  
  public function getCountConsumoMenoresQueByCronograma($id_cronograma=null,$num=35){
  	$this->setDateSourceCustom();
  	$db = $this->getDataSource();
  
  	$schema = 'lecturas';
  
  	$sql="SELECT COUNT(*) as cantidad FROM ".$schema.".comlec_ordenlecturas WHERE montoconsumo1<".$num." and 
  			idciclo in (select idciclo from lecturas.comlec_cronograma cro where id=".$id_cronograma.")
  			and sector in (select idsector from lecturas.comlec_cronograma cro where id=".$id_cronograma.")";
  
  	$result = $db->query($sql);
  
  	if($result){
  		return $result[0][0]['cantidad'];
  	}
  	return  0;
  }
  
  public function getCountConsumoMayoresQueByCronograma($id_cronograma=null,$num=1000){
  	$this->setDateSourceCustom();
  	$db = $this->getDataSource();
  
  	$schema = 'lecturas';
  
  	$sql="SELECT COUNT(*) as cantidad FROM ".$schema.".comlec_ordenlecturas WHERE montoconsumo1>".$num." and 
  			idciclo in (select idciclo from lecturas.comlec_cronograma cro where id=".$id_cronograma.")
  			and sector in (select idsector from lecturas.comlec_cronograma cro where id=".$id_cronograma.")";
  
  	$result = $db->query($sql);
  
  	if($result){
  		return $result[0][0]['cantidad'];
  	}
  	return  0;
  }
  
  public function getCountSinLecturayObsByCronograma($id_cronograma=null){
  	$this->setDateSourceCustom();
  	$db = $this->getDataSource();
  
  	$schema = 'lecturas';
  
  	$sql="SELECT COUNT(*) as cantidad FROM ".$schema.".comlec_ordenlecturas WHERE (obs1 is null or obs1 = '' or obs1 = 'NULL' or obs1='0') and (lecturao1='' or lecturao1 is null) and 
  			idciclo in (select idciclo from lecturas.comlec_cronograma cro where id=".$id_cronograma.")
  			and sector in (select idsector from lecturas.comlec_cronograma cro where id=".$id_cronograma.")";
  
  	$result = $db->query($sql);
  
  	if($result){
  		return $result[0][0]['cantidad'];
  	}
  	return  0;
  }
  
  public function getCountConsumoMayor100PorcientoQueAnteriorByCronograma($id_cronograma=null){
  	$this->setDateSourceCustom();
  	$db = $this->getDataSource();
  
  	$schema = 'lecturas';
  
  	$sql="SELECT COUNT(*) as cantidad FROM ".$schema.".comlec_ordenlecturas WHERE montoconsumo1 > (consant*2) and 
  			idciclo in (select idciclo from lecturas.comlec_cronograma cro where id=".$id_cronograma.")
  			and sector in (select idsector from lecturas.comlec_cronograma cro where id=".$id_cronograma.")";
  
  	$result = $db->query($sql);
  
  	if($result){
  		return $result[0][0]['cantidad'];
  	}
  	return  0;
  }
  
  public function getCountEnviadasDistriluzByCronograma($id_cronograma=null){
  	$this->setDateSourceCustom();
  	$db = $this->getDataSource();
  
  	$schema = 'lecturas';
  
  	$sql="SELECT COUNT(*) as cantidad FROM ".$schema.".comlec_ordenlecturas WHERE resultadoevaluacion='CONSISTENTE' and ws_l_estado='1' and 
  			idciclo in (select idciclo from lecturas.comlec_cronograma cro where id=".$id_cronograma.")
  			and sector in (select idsector from lecturas.comlec_cronograma cro where id=".$id_cronograma.")";
  
  	$result = $db->query($sql);
  
  	if($result){
  		return $result[0][0]['cantidad'];
  	}
  	return  0;
  }
  
  public function saveProgramarEventoCronograma($unidad_negocio,$pfactura,$fecha,$idciclo,$idsector,$usuario_id,$client_id){
  	$this->setDateSourceCustom();
  	$db = $this->getDataSource();
  	
  	$sql="INSERT INTO lecturas.comlec_cronograma(
            glomas_unidadnegocio_id, pfactura, idciclo, idsector, fecha, usuario_id, client_ip, created)
    		VALUES (".$unidad_negocio.",'".$pfactura."','".$idciclo."','".$idsector."','".$fecha."',".$usuario_id.",'".$client_id."','".date('Y-m-d H:i:s')."');";
  	
  	$result = $db->query($sql);
  }
  
  public function saveActualizarEventoCronograma($id_cronograma,$unidad_negocio,$pfactura,$fecha,$idciclo,$idsector,$usuario_id,$client_id){
  	$this->setDateSourceCustom();
  	$db = $this->getDataSource();
  	 
  	$sql="UPDATE lecturas.comlec_cronograma SET
            glomas_unidadnegocio_id=".$unidad_negocio.", 
            pfactura='".$pfactura."', 
            idciclo='".$idciclo."', 
            idsector='".$idsector."', 
            fecha='".$fecha."', 
            usuario_id=".$usuario_id.", 
            client_ip='".$client_id."', 
            created='".date('Y-m-d H:i:s')."'
    		WHERE id=".$id_cronograma.";";
  	 
  	$result = $db->query($sql);
  }
  
  public function eliminarEventoCronograma($id_cronograma){
  	$this->setDateSourceCustom();
  	$db = $this->getDataSource();
  	 
  	$sql="DELETE FROM lecturas.comlec_cronograma WHERE id=".$id_cronograma;
  	 
  	$result = $db->query($sql);
  }
  
  public function saveActualizarEventoCronogramaToFinalizado($id_cronograma){
  	$this->setDateSourceCustom();
  	$db = $this->getDataSource();
  	
  	if(is_array($id_cronograma)){
  		$id_cronograma = implode(',',$id_cronograma);
  	}
  
  	$sql="UPDATE lecturas.comlec_cronograma SET
            finalizado=true
    		WHERE id in (".$id_cronograma.");";
  
  	$result = $db->query($sql);
  }
  
  public function existeEventoCronograma($unidad_negocio,$pfactura,$idciclo,$idsector){
  
  	$this->setDateSourceCustom();
  	$db = $this->getDataSource();
  	
  	$str_where = '';
  	if(isset($unidad_negocio) && $unidad_negocio!=''){
  		$str_where = " and glomas_unidadnegocio_id=".$unidad_negocio;
  	}
  	//pfactura = '".$pfactura."' and
  	$sql="select * from lecturas.comlec_cronograma where 1=1 
  			".$str_where." and  			
  			idciclo = '".$idciclo."' and
  			idsector = '".$idsector."' ";
  	return  $db->query($sql);
  }
  
  public function getKpiByCronograma($id_cronograma=null){
  	$this->setDateSourceCustom();
  	$db = $this->getDataSource();
  	
  	if(is_array($id_cronograma)){
  		if(count($id_cronograma)){
  			$str_filtro = " and cro_master.id in (".implode(',',$id_cronograma).") ";;
  		}
  	}else{
  		$str_filtro = ' and cro_master.id='.$id_cronograma;
  	}
  
  	$schema = 'lecturas';
  
  	$sql="SELECT cro_master.id, (select COUNT(*) as cantidad FROM ".$schema.".comlec_ordenlecturas ol
  			inner join lecturas.comlec_cronograma cro on ol.idciclo=cro.idciclo and ol.sector=cro.idsector
  			where cro.id=cro_master.id) as count_suministros,
  			(select COUNT(*) as cantidad FROM ".$schema.".comlec_ordenlecturas ol
  			inner join lecturas.comlec_cronograma cro on ol.idciclo=cro.idciclo and ol.sector=cro.idsector
  			where cro.id=cro_master.id and lecturista1_id is not null and (up1 in ('1','2') or up2 in ('1','2') or up3 in ('1','2'))) as count_finalizados FROM  
  			lecturas.comlec_cronograma cro_master
  			where 1=1 ";

  	return $db->query($sql);
  }
  
  public function noProgramadosCronograma($criterio=null,$date_start=null,$date_end=null,$unidadneg=null,$ciclo=null,$sector=null){
  	$this->setDateSourceCustom();
  	$db = $this->getDataSource();
  
  	$filtro_fecha = "";
  	$str_sector = "";
  	if(strpos($criterio, 'F') !== false){
  		$filtro_fecha .= " AND cast(fecha as date)  >= '$date_start' AND cast(fecha as date)  <= '$date_end 23:59:59'";
  	}
  	if(strpos($criterio, 'U') !== false){
  		if(isset($unidadneg) && $unidadneg!=0 && $unidadneg!=''){
  			$filtro_fecha .= " AND ol.glomas_unidadnegocio_id = '".$unidadneg."'";
  		}
  		if(isset($ciclo) && $ciclo!=0 && $ciclo!=''){
  			$filtro_fecha .= " AND ol.idciclo = '".$ciclo."'";
  		}
  		if(isset($sector) && $sector!=0 && $sector!=''){
  			$filtro_fecha .= " AND ol.sector = '".$sector."'";
  		}
  	}
  
  	$sql="select distinct ol.glomas_unidadnegocio_id, ol.idciclo, ol.sector as idsector, case when lecturista1_id is not null and fechaejecucion1 is not null then date_trunc('day',fechaejecucion1) when lecturista1_id is not null and fechaasignado1 is not null then date_trunc('day',fechaasignado1) else date_trunc('day',now()) end as fecha from lecturas.comlec_ordenlecturas ol left join lecturas.comlec_cronograma cro on ol.idciclo=cro.idciclo and ol.sector=cro.idsector where cro.idciclo is null and cro.idsector is null
  	 ".$filtro_fecha." order by 4 desc";
  
  	return  $db->query($sql);
  }
  
  public function getLastEventUpdateLectura(){
  	$this->setDateSourceCustom();
  	$db = $this->getDataSource();
  	 
  	$schema = 'lecturas';
  
  	$sql="SELECT modified FROM ".$schema.".comlec_last_events WHERE id = 1";
  	 
  	$result = $db->query($sql);
  	 
  	if($result){
  		return $result[0][0]['modified'];
  	}
  	return  0;
  }
  
  public function getResumenKpi($options=null){
  	$this->setDateSourceCustom();
  	$db = $this->getDataSource();
  	 
  	$str_where = '';
  	if(isset($options) && is_array($options)){
  		if(isset($options['unidad_neg']) and $options['unidad_neg']!=0){
  			$str_where .= ' AND glomas_unidadnegocio_id = '.$options['unidad_neg'];
  		}
  		if(isset($options['idciclo']) && $options['idciclo']!='0'){
  			$str_where .= " AND idciclo='".$options['idciclo']."'";
  		}
  		if(isset($options['idsector']) && $options['idsector']!='0'){
  			$str_where .= " AND sector='".$options['idsector']."'";
  		}
  		if(isset($options['date_start']) && $options['date_start']!='0'){
  			$str_where .= " AND cast(fechaasignado1 as date)  >= '".$options['date_start']."'";
  		}
  		if(isset($options['date_end']) && $options['date_end']!='0'){
  			$str_where .= " AND cast(fechaasignado1 as date)  <= '".$options['date_end']."'";
  		}
  	}
  	 
  	$sql="select 
(SELECT COUNT(*) as cantidad FROM lecturas.comlec_ordenlecturas where 1=1 ".$str_where.") as suministros ,
(SELECT COUNT(*) as cantidad FROM lecturas.comlec_ordenlecturas WHERE lecturista1_id is not null ".$str_where.") as asignados,
(SELECT COUNT(*) as cantidad FROM lecturas.comlec_ordenlecturas WHERE lecturista1_id is not null and down1='1' ".$str_where.") as descargados,
(SELECT COUNT(*) as cantidad FROM lecturas.comlec_ordenlecturas WHERE (resultadoevaluacion='CONSISTENTE' OR resultadoevaluacion='INCONSISTENTE') ".$str_where.") as finalizados,
(SELECT COUNT(*) as cantidad FROM lecturas.comlec_ordenlecturas WHERE resultadoevaluacion='INCONSISTENTE' ".$str_where.") as inconsistentes_actuales,
(SELECT COUNT(*) as cantidad FROM lecturas.comlec_ordenlecturas WHERE validacion>='1' ".$str_where.") as inconsistentes_evaluadas,
(SELECT COUNT(*) as cantidad FROM lecturas.comlec_ordenlecturas WHERE ws_l_estado='1' ".$str_where.") as enviados_distriluz";
  	
  	return $db->query($sql);
  }
  
  /**
   * Listar Ciclos para Cronograma (Combina los ciclos actuales + ciclos de ultimo periodo)
   * @param string $ultimo_pfactura
   * @author Geynen
   * @version 23 Enero 2015
   */
  public function listarCicloCronograma($ultimo_pfactura){
  	$this->setDateSourceCustom ();
  	$db = $this->getDataSource();
  
  	$sql="SELECT distinct ciclo as idciclo, nombreciclo as nombciclo FROM lecturas.glomas_ciclo gc
        		inner join history.pfactura p on p.idpfactura=gc.idpfactura WHERE (p.actual=true OR pfactura = '".$ultimo_pfactura."') order by ciclo asc";
  	return  $db->query($sql);
  }
  
  /**
   * Listar Sector para Cronograma (Combina los ciclos actuales + ciclos de ultimo periodo)
   * @author Geynen
   * @version 23 Enero 2015
   **/
  public function listarSectorCronograma($ultimo_pfactura){
  	$this->setDateSourceCustom();
  	$db = $this->getDataSource();
  	$sql="SELECT distinct nombresector as sector FROM lecturas.glomas_sector gs inner join lecturas.glomas_ciclo gc on gs.idciclo=gc.idciclo 
        		inner join history.pfactura p on p.idpfactura=gc.idpfactura WHERE (p.actual=true OR pfactura = '".$ultimo_pfactura."') order by nombresector asc";
  	return  $db->query($sql);
  }
  
  /**
   * verificar si el ciclo es de sub estacion
   * @param string $idciclo
   * @return number
   * @author Geynen
   * @version 24 Enero 2015
   */
  public function esCicloSubEstacion($idciclo){
  
  	$this->setDateSourceCustom();
  	$db = $this->getDataSource();
  	//pfactura = '".$pfactura."' and
  	$sql="select count(*) as cantidad from lecturas.comlec_ordenlecturas ol inner join lecturas.comlec_xml xm on ol.comlec_xml_id=xm.id and subestacion=true 
  			where idciclo = '".$idciclo."' ";
  	$result = $db->query($sql);
  
  	if($result){
  		return $result[0][0]['cantidad'];
  	}
  	return  0;
  }
  
  /**
   * Listar Libros de Asignacion de Lecturistas
   * @param unknown $ciclo
   * @param unknown $sector
   * @param unknown $ruta
   * @param unknown $pfactura
   * @author Geynen
   * @version 30 Enero 2015
   */
  public function listaLibrosV2($ciclo, $sector, $ruta=null, $pfactura=null) {
		$this->setDateSourceCustom ();
		$db = $this->getDataSource ();
		
		$str_where = "";
		if(isset($ciclo) && $ciclo!='0'){
			$str_where .= " AND idciclo='".$ciclo."'";
		}
		if(isset($sector) && $sector!='0'){
			$str_where .= " AND sector='".$sector."'";
		}
		if(isset($ruta) && $ruta!='0'){
			$str_where .= " AND ruta='".$ruta."'";
		}
		if(isset($pfactura) && $pfactura!='0'){
			$str_where .= " AND pfactura = '".$pfactura."'";
			$schema = 'history';
		}else{
			$str_where .= '';
			$schema = 'lecturas';
		}
		
		$sql = "
		 select distinct 'libro '||libro as libro ,count(*) ,e.nombre as lecturista ,
		 min(ordenruta)as min,max(ordenruta) as max, loext.lecturista1_id, fechaasignado1
		 from ".$schema.".comlec_ordenlecturas loext
		 left join lecturas.glomas_empleados e on e.id=loext.lecturista1_id where
		 1=1 ".$str_where." and loext.tipolectura='L' 
		 group by libro,e.nombre,e.olmax, e.olmin, loext.lecturista1_id, fechaasignado1
		 order by libro, min, max, e.nombre asc";
		
		//echo $sql;exit;
		return $db->query ( $sql );
	}
	
	/**
	 * Listar Rutas para Asignacion de Lecturistas
	 * @param unknown $ciclo
	 * @param unknown $sector
	 * @param unknown $pfactura
	 * @author Geynen
	 * @version 02 Febrero 2015
	 */
	public function listaRutasParaAsignacion($ciclo, $sector, $pfactura=null) {
		$this->setDateSourceCustom ();
		$db = $this->getDataSource ();
	
		$str_where = "";
		if(isset($ciclo) && $ciclo!='0'){
			$str_where .= " AND idciclo='".$ciclo."'";
		}
		if(isset($sector) && $sector!='0'){
			$str_where .= " AND sector='".$sector."'";
		}
		if(isset($pfactura) && $pfactura!='0'){
			$str_where .= " AND pfactura = '".$pfactura."'";
			$schema = 'history';
		}else{
			$str_where .= '';
			$schema = 'lecturas';
		}
	
		$sql = "
		 select ruta, nombruta as nombreruta,count(*) ,e.nombre as lecturista ,
		 min(ordenruta)as min,max(ordenruta) as max, loext.lecturista1_id, fechaasignado1
		 from ".$schema.".comlec_ordenlecturas loext
		 left join lecturas.glomas_empleados e on e.id=loext.lecturista1_id where
		 1=1 ".$str_where." and loext.tipolectura='L'
		 group by ruta,nombruta,e.nombre,e.olmax, e.olmin, loext.lecturista1_id, fechaasignado1
		 order by ruta, min, max, e.nombre asc";
	
		//echo $sql;exit;
		return $db->query ( $sql );
	}
	
	public function existeSectorParaCiclo($unidad_negocio,$pfactura,$idciclo,$idsector){
	
		$this->setDateSourceCustom();
		$db = $this->getDataSource();
		 
		$str_where = '';
		if(isset($unidad_negocio) && $unidad_negocio!=''){
			$str_where .= " and pf.glomas_unidadnegocio_id=".$unidad_negocio;
		}
		if(isset($pfactura) && $pfactura!=''){
			$str_where .= " and pf.pfactura='".$pfactura."'";
		}
		if(isset($idciclo) && $idciclo!=''){
			$str_where .= " and gc.ciclo='".$idciclo."'";
		}
		if(isset($idsector) && $idsector!=''){
			$str_where .= " and gs.nombresector='".$idsector."'";
		}
		
		$sql="SELECT count(*) as cantidad FROM lecturas.glomas_sector gs
            inner join lecturas.glomas_ciclo gc on gs.idciclo=gc.idciclo
    		inner join history.pfactura pf on gc.idpfactura=pf.idpfactura
            where 1=1 ".$str_where;
		
		$result = $db->query($sql);
		
		if($result){
			return $result[0][0]['cantidad'];
		}
		return  0;		
	}
	
	public function guardarCartasReclamos($listado,$arr_suministros,$fator_correccion,$id_usuario){
	
		$this->setDateSourceCustom();
		$db = $this->getDataSource();
			
		if(count($listado)>0){
			$sql_head='INSERT INTO lecturas.comlec_cartas(
	            pfactura, glomas_unidadnegocio_id, suministro, factor, created, 
	            usuario_id) values ';
			$sql='';
			foreach ($listado as $obj_orden_lectura){
				if(!in_array($obj_orden_lectura[0]['suministro'],$arr_suministros)){
					$sql .= "('".$obj_orden_lectura[0]['pfactura']."',".$obj_orden_lectura[0]['glomas_unidadnegocio_id'].",'".$obj_orden_lectura[0]['suministro']."',".$fator_correccion.",'".date('Y-m-d H:i:s')."',".$id_usuario."),";
				}
			}
			$query = $sql_head.substr($sql,0,-1);
			$result = $db->query($query);
			if($result){
				return $result;
			}
		}	
		
		return  0;
	}
	
	public function getCountCartasReclamos($options=null){
		$this->setDateSourceCustom();
		$db = $this->getDataSource();
	
		$schema = 'lecturas';
    	$str_where = '';
    	if(isset($options) && is_array($options)){
    		if(isset($options['unidad_neg']) and $options['unidad_neg']!=0){
    			$str_where .= ' AND cart.glomas_unidadnegocio_id = '.$options['unidad_neg'];
    		}
    		if(isset($options['idciclo']) && $options['idciclo']!='0'){
    			$str_where .= " AND idciclo='".$options['idciclo']."'";
    		}
    		if(isset($options['pfactura']) and $options['pfactura']!='0'){
    			$str_where .= " AND cart.pfactura = '".$options['pfactura']."'";
    			$schema = 'history';
    		}else{
    			$str_where .= '';
    			$schema = 'lecturas';
    		}    		
    	}
    	
    	$schema = 'lecturas';
    
    	$sql="SELECT COUNT(cantidad) as cantidad from (
    			SELECT COUNT(*) as cantidad FROM ".$schema.".comlec_cartas cart 
    			inner join ".$schema.".comlec_ordenlecturas ol on ol.suministro=cart.suministro and ol.pfactura=cart.pfactura 
    			WHERE 1=1 ".$str_where."
    			group by cart.suministro) T ";
    
    	$result = $db->query($sql);
    
    	if($result){
    		return $result[0][0]['cantidad'];
    	}
    	return  0;
	}
	
	/**
	 * Listar Rutas para Asignacion de Lecturistas
	 * @param unknown $ciclo
	 * @param unknown $sector
	 * @param unknown $pfactura
	 * @author Geynen
	 * @version 02 Febrero 2015
	 */
	public function listarSuministrosFotos() {
		$this->setDateSourceCustom ();
		$db = $this->getDataSource ();
	
		$sql = "
		select suministro, direccion, lecturao1, obs1, foto1, fechaejecucion1, seriefab, obs.codigofac
				from lecturas.comlec_ordenlecturas ol 
				left join lecturas.comlec_observaciones obs on ol.obs1 =obs.codigocamp_varchar 
				where foto1!='' and foto1 is not null and 
suministro in (
'50032586',
'50161995',
'50032503',
'50031794',
'50031337',
'50107186',
'50036226',
'50031711',
'60358804',
'50035819',
'50035846',
'52771008',
'58506338',
'58709635',
'58671135',
'50089275',
'50076615',
'50037340',
'50037653',
'50029339',
'50027862',
'50028734',
'58709564',
'50028073',
'50035766',
'50035659',
'50035229',
'50029141',
'50029132',
'50151282',
'50028968',
'50028814',
'52804172',
'50145730',
'50035318',
'59228530',
'59492596',
'50029375',
'50029437',
'52815659',
'50029428',
'50029446',
'60384205',
'50161162',
'52777567',
'50029590',
'50029731',
'50029876',
'50029974',
'50035425',
'50030760',
'50035480',
'50035532',
'50030939',
'59549616',
'50030993',
'50134174',
'50031005',
'50031570',
'50031542',
'50152403',
'50032951',
'50037359',
'50032880',
'50033879',
'50162320',
'50034268',
'50034464',
'50034713',
'50035200',
'50049153',
'50015478',
'50015244',
'50015164',
'50099137',
'50015585',
'59075913',
'50014550',
'52785030',
'58052638',
'50069370',
'50016869',
'50014603',
'50027030',
'52773782',
'50013689',
'50090605',
'50151246',
'50013938',
'50014087',
'50015093',
'50019172',
'50019403',
'58902241',
'50019477',
'50143039',
'50019646',
'59510658',
'50075912',
'50034615',
'50012995',
'50012959',
'50012636',
'50012645',
'50012627',
'50012618',
'50012609',
'50012592',
'50012583',
'50105870',
'50012467',
'52786207',
'50012340',
'50012270',
'50012190',
'50011782',
'59440114',
'50015576',
'50007770',
'50089364',
'50007823',
'50111142',
'50007968',
'50008025',
'50068829',
'50019913',
'60299623',
'50020084',
'50149631',
'50008517',
'50019299',
'52780197',
'50069521',
'50030456',
'50125059',
'50093527',
'50104086',
'50026829',
'50026810',
'50131010',
'50027110',
'50012162',
'50144107',
'50134684',
'52811337',
'60157889',
'50012420',
'50069156',
'52779042',
'50016448',
'50016170',
'50015656',
'50011826',
'52793201',
'52783822',
'49999170',
'52812559',
'50140073',
'59024048',
'50011414',
'50011263',
'50007761',
'59137613',
'50019996',
'59219263',
'59222071',
'59125185',
'50000410',
'50006138',
'52775188',
'52791646',
'50126575',
'50006639',
'50008464',
'50008660',
'52765655',
'58890187',
'50127151',
'50127170',
'50033190',
'50033206',
'50079303',
'58118838',
'50146086',
'50006915',
'52810895',
'50004957',
'59550312',
'58519069',
'50147987',
'50005749',
'50070059',
'50030367',
'50019092',
'60378450',
'50005373',
'50015861',
'50069307',
'49998996',
'50006595',
'59136509',
'50097132',
'59416271',
'59416315',
'50016724',
'50069361',
'49984248',
'58864024',
'60242925',
'50131305',
'50001427',
'50001383',
'60209617',
'50068328',
'50135430',
'50001614',
'50079706',
'50003548',
'50125282',
'52789094',
'52790184',
'58245090',
'59124203',
'50003181',
'50123831',
'50161144',
'60328467',
'50068337',
'50002264',
'50068346',
'50030124',
'50001937',
'50086282',
'52789380',
'59583396',
'50068696',
'50129808',
'50005981',
'50069432',
'58286412',
'50018676',
'50018362',
'50005168',
'52807094',
'50017848',
'50017875',
'50017810',
'50017650',
'50030240',
'50004797',
'50004723',
'50004741',
'52813162',
'50004287',
'50017220',
'59455289',
'50004616',
'50004625',
'50004429',
'50004447',
'60210592',
'50068506',
'50068490',
'50015899',
'60142394',
'49994066',
'52766231',
'52816057',
'49988514',
'50085614',
'49993846',
'60360624',
'52797935',
'49986251',
'49993828',
'58128585',
'49993793',
'59974218',
'59641711',
'50082612',
'49989709',
'49993130',
'49993096',
'49992689',
'49991645',
'49991672',
'49991888',
'49992034',
'50154130',
'49991707',
'49991690',
'60232615',
'50067901',
'49988078',
'50105780',
'49991402',
'49991609',
'49985639',
'50000107',
'49985791',
'50114921',
'52775221',
'49985610',
'49985512',
'50081446',
'49984963',
'49984589',
'59587715',
'49984990',
'49987615',
'49988185',
'49988194',
'49987992',
'49990880',
'60001378',
'58306936',
'49987580',
'49990503',
'52797766',
'59443279',
'49993603',
'58005816',
'49993434',
'58696493',
'50000733',
'49994164',
'49986378',
'58941212',
'49994576',
'49990165',
'52765154',
'49994280',
'50000770',
'50127625',
'52797229',
'49986494',
'52796160',
'49994905',
'50121434',
'49989790',
'52768399',
'52788506',
'50000813',
'52796929',
'49988800',
'49989010',
'52811050',
'59320626',
'49989665',
'49989137',
'49995831',
'50135224',
'49995546',
'49987357',
'50162858',
'58689603',
'49997649',
'49995751',
'52764685',
'49987160',
'60351910',
'49987366',
'49986850',
'52797013',
'49996605',
'52760541',
'50084410',
'49996759',
'49999456',
'50080770',
'50085605',
'52764110',
'49990325',
'50159745',
'49990209',
'49989905',
'49999536',
'50155684',
'60233274',
'50083709',
'52800469',
'49998000',
'59317442',
'50021662',
'50021626',
'58301896',
'50069488',
'50021448',
'50021395',
'50021152',
'50020558',
'58054580',
'50144690',
'50020807',
'50014971',
'50015066',
'60282537',
'50027227',
'50027414',
'50021063',
'50103356',
'50020324',
'50020306',
'50020960',
'50020610',
'60382775',
'50028270',
'50131691',
'60381796',
'50118063',
'52796043',
'50023774',
'50024477',
'57923917',
'50023890',
'50023685',
'60315253',
'60245408',
'50069595',
'50025124',
'58079598',
'50090580',
'58399793',
'59561791',
'52763024',
'60142975',
'50025330',
'52793210',
'58740395',
'52797980',
'50121167',
'50069764',
'59971637',
'59577272',
'52805474',
'50023264',
'52774369',
'52802472',
'49998010',
'49998047',
'50079742',
'60346438',
'60346474',
'50152252',
'59371788',
'50149702',
'52807782',
'50022338',
'50022300',
'50022294',
'50022211',
'50023390',
'60279649',
'60279059',
'60279166',
'50154981',
'58944170',
'50070255',
'50037500',
'50036306',
'50022104',
'50021869',
'50022866',
'50023498',
'50023442',
'49974180',
'49974199',
'49974205',
'49972149',
'49973772',
'58053706',
'50002738',
'49973585',
'49973656',
'49973502',
'49973763',
'49974386',
'49973941',
'49971830',
'49973100',
'49972701',
'49973066',
'49972891',
'52765619',
'49972668',
'49971651',
'49972309',
'50139896',
'59918827',
'60006463',
'49971491',
'49971301',
'49971240',
'52786252',
'52780885',
'59946044',
'52794058',
'50094650',
'50066815',
'49971704',
'49973253',
'49972621',
'49975150',
'49975140',
'49975060',
'49975276',
'49974706',
'49974724',
'49975679',
'59451305',
'52777576',
'50106142',
'58889613',
'49975570',
'59530759',
'49976237',
'49976890',
'58399283',
'49976040',
'49976059',
'49976353',
'49976200',
'49976228',
'49976219',
'49976255',
'49976264',
'60227419',
'60152130',
'49975900',
'49976943',
'50107702',
'50120114',
'50135153',
'50126118',
'49978080',
'52796464',
'52798852',
'50082363',
'59317291',
'59315564',
'59315546',
'59315081',
'59310835',
'59316688',
'50120885',
'52796517',
'49978652',
'52775016',
'49978438',
'50139985',
'58231559',
'49977682',
'49977673',
'58803261',
'58962974',
'49977806',
'49978311',
'60231627',
'49978302',
'52801500',
'49978296',
'59027504',
'49978222',
'52777594',
'49977440',
'59598853',
'49977109',
'59125111',
'52778690',
'60327880',
'60327817',
'49977771',
'50141689',
'49978966',
'49978895',
'59963751',
'52774126',
'49979112',
'50104596',
'60245515',
'60245524',
'49979874',
'50089909',
'60357772') and suministro not in ('50135430',
'50141689',
'49978966',
'49978895',
'59963751',
'52774126',
'49979112',
'50104596',
'60245515',
'60245524',
'49979874',
'50089909',
'60357772',
'50026829',
'50026810',
'49995751',
'49976059',
'50001614',
'50003548',
'50019646',
'50069432',
'50007770',
'50070255',
'50068328',
'49976943',
'50022300',
'60279649',
'58944170',
'50037500',
'50036306',
'50022104',
'50022338');";
	
		//echo $sql;exit;
		return $db->query ( $sql );
	}
  

/**
   * Realiza la facturacion online
   * @param string $datos
   * @return number
   * @author vasquezader
   * @version 21 Marzo 2015
   */
  public function facturar($var_tarifa , $var_sector, $var_plancondicion, $var_consumo, $var_periodo, $suministro){
  	$this->setDateSourceCustom();
  	$db = $this->getDataSource();
	
	$s="SET search_path = public;"; 
	$db->query($s);
	
  	$sql="SELECT * FROM facturacion.calcularmontofacturacion46($var_tarifa , $var_sector, $var_plancondicion, $var_consumo, '$var_periodo', '$suministro') as cantidad";
  	$pa = $db->query($sql);
	
	$result = 0;
	
	if($pa[0][0]['cantidad'] == 1){
  		$sql2="select * from facturacion.detallefacturas df
					inner join facturacion.conceptos c
					on df.idconcepto=c.idconcepto where df.suministro='$suministro' and c.verenrecibo=true order by c.orden asc";
		$result = $db->query($sql2);
  	}
  	return  $result;
  }
  
 /**
   * Realiza la facturacion online
   * @param string $datos
   * @return number
   * @author vasquezader
   * @version 21 Marzo 2015
   */
  public function datosfacturar($var_suministro){
	$this->setDateSourceCustom();
  	$db = $this->getDataSource();

  	$sql="SELECT idtarifa, idsector, idplancondicion, pfactura  FROM lecturas.comlec_ordenlecturas WHERE suministro = '$var_suministro'";
  	return $db->query($sql);
	}

	/**
	 * Campos para remplazar en recibo 
	 * @param string $datos
	 * @return number
	 * @author vasquezader
	 * @version 28 Marzo 2015
	 */
	public function datosfacturarrecibo($var_suministro){
		$this->setDateSourceCustom();
		$db = $this->getDataSource();
		$header_sql = "comlec_ordenlectura_id,suministro,cliente,digitos,direccion,direccionr,seriefab,orden,factor,lecant,consret,lcorrelati,comlec_xml_id,resultadoevaluacion,medidornuevo,glomas_grupo_id, tipolectura, promedio, pinicio";
		$facturacion_element = " ,idplancondicion,idtarifa,tarifa,pfactura,idsector,sector,ruta,nombruta, glomas_unidadnegocio_id as idunidadnegocio,
			tension, subestacion, tipoconexion, opciontarifaria, hilos, tipomedidor, potencia, to_char(inicio_contrato, 'DD/MM/YYYY') as inicio_contrato, to_char(fin_contrato, 'DD/MM/YYYY') as fin_contrato, to_char(fecha_emision, 'DD/MM/YYYY') as fecha_emision,
					fecha_corte , to_char(fecha_vencimiento, 'DD/MM/YYYY') as fecha_vencimiento, to_char(fecha_lecant, 'DD/MM/YYYY') as fecha_lecant, nro_recibo, fise_nro, fise_precio, to_char(fise_vencimiento, 'DD/MM/YYYY') as fise_vencimiento, fise_dni ";
		 
		$sql = "SELECT ".$header_sql.$facturacion_element." FROM lecturas.comlec_ordenlecturas WHERE suministro = '$var_suministro'";
	
		return $db->query($sql);
	}
	
}