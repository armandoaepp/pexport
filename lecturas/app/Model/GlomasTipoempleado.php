<?php
App::uses('AppModel', 'Model');
/**
 * Activity Model
 *
 * @property GlomasTipoempleado $GlomasTipoempleado
*/ 
class GlomasTipoempleado extends AppModel {
	/**
	 * Display field
	 *
	 * @var string
	 */
	public $name = 'GlomasTipoempleado'; 

	
	/**
	 * hasMany associations
	 *
	 * @var array
	 */
	public $hasMany = array(
			'GlomasEmpleado' => array(
					'className' => 'GlomasEmpleado',
					'foreignKey' => 'glomas_tipoempleado_id',
					'dependent' => false,
					'conditions' => '',
					'fields' => '',
					'order' => '',
					'limit' => '',
					'offset' => '',
					'exclusive' => '',
					'finderQuery' => '',
					'counterQuery' => ''
			)
	);

}