<?php
App::uses('AppModel', 'Model');
/**
 * Activity Model
 *
 * @property GlomasEmpleado $GlomasEmpleado
*/ 
class GlomasEmpleado extends AppModel {
	/**
	 * Display field
	 *
	 * @var string
	 */
	public $name = 'GlomasEmpleado'; 

	/**
	 * $belongsTo associations
	 *
	 * @var array
	 */
	
	public function listarempleadosAll(){
		$this->setDateSourceCustom ();
		$db = $this->getDataSource ();
		$sql = "SELECT * FROM lecturas.glomas_empleados e inner join lecturas.glomas_tipoempleados t on e.glomas_grupo_id = t.id where e.eliminado=false order by nombre";
		$arr_empleados = $db->query($sql);
		return $arr_empleados;
	}
	
	public function ListarEmpleados() {
		$this->setDateSourceCustom ();
		$db = $this->getDataSource ();
		$sql = "SELECT * FROM lecturas.usuarios lu inner join lecturas.glomas_empleados ge on lu.glomas_empleado_id = ge.id where ge.eliminado=false and glomas_tipoempleado_id =1 order by nombre";
		$arr_obj_usuario = $db->query($sql);
		
		return $arr_obj_usuario;
	}
	
	public function ListarPersonasbyUsuarios() {
		$this->setDateSourceCustom ();
		$db = $this->getDataSource ();
		$sql = "SELECT * FROM lecturas.usuarios lu right join lecturas.glomas_empleados ge on lu.glomas_empleado_id = ge.id order by nombre";
		$arr_obj_usuario = $db->query($sql);
	
		return $arr_obj_usuario;
	}
	
	public function ListarLecturasMovil($criterio,$date_start,$date_end,$unidadneg=null,$ciclo=null,$sector=null) {
		$this->setDateSourceCustom ();
		$db = $this->getDataSource ();

		$filtro_fecha = " 1=1 ";
		$filtro_fecha2 = " 1=1 ";
		if(strpos($criterio, 'F') !== false){
			$filtro_fecha .= " AND cast(o.fechaasignado1 as date)  >= '$date_start' AND cast(o.fechaasignado1 as date)  <= '$date_end 23:59:59'";
			$filtro_fecha2 .= " AND cast(fechaasignado1 as date)  >= '$date_start' AND cast(fechaasignado1 as date)  <= '$date_end 23:59:59'";			
		}
		if(strpos($criterio, 'U') !== false){
			if(isset($unidadneg) && $unidadneg!=0 && $unidadneg!=''){
				$filtro_fecha .= " AND glomas_unidadnegocio_id = '$unidadneg'";
				$filtro_fecha2 .= " AND glomas_unidadnegocio_id = '$unidadneg'";
			}
			if(isset($ciclo) && $ciclo!=0 && $ciclo!=''){
				$filtro_fecha .= " AND idciclo = '$ciclo'";
				$filtro_fecha2 .= " AND idciclo = '$ciclo'";
			}
			if(isset($sector) && $sector!=0 && $sector!=''){
				$filtro_fecha .= " AND sector = '$sector'";
				$filtro_fecha2 .= " AND sector = '$sector'";
			}
		}
		/*if($criterio == 'F'){
			$filtro_fecha = "cast(o.fechaasignado1 as date)  >= '$date_start' AND cast(o.fechaasignado1 as date)  <= '$date_end 23:59:59'";
			$filtro_fecha2 = "cast(fechaasignado1 as date)  >= '$date_start' AND cast(fechaasignado1 as date)  <= '$date_end 23:59:59'";
		}else{
			if($sector == 0){
				$filtro_fecha = "idciclo = '$ciclo'";
				$filtro_fecha2 = "idciclo = '$ciclo'";
			}else{
				$filtro_fecha = "idciclo = '$ciclo' AND sector = '$sector'";
				$filtro_fecha2 = "idciclo = '$ciclo' AND sector = '$sector'";
			}
			
		}*/

		$sql = "select e.id, e.nombre as lecturista,
			(select count(*) from lecturas.comlec_ordenlecturas where ".$filtro_fecha2." AND lecturista1_id=e.id) as lecturas_asignadas,
			(select count(*) from lecturas.comlec_ordenlecturas where ".$filtro_fecha2." AND lecturista1_id=e.id and (down1 = '1' or down2 = '1' or down3 = '1') ) as lecturas_descargadas,
			(select min(fechaejecucion1) from lecturas.comlec_ordenlecturas where ".$filtro_fecha2." AND lecturista1_id=e.id) as fecha_hora_inicio,
			(select max(fechaejecucion1) from lecturas.comlec_ordenlecturas where ".$filtro_fecha2." AND lecturista1_id=e.id) as fecha_hora_fin,
			(select count(*) from lecturas.comlec_ordenlecturas where ".$filtro_fecha2." AND resultadoevaluacion='CONSISTENTE' ) as lecturas_finalizadasexportar,
			(select count(*) from lecturas.comlec_ordenlecturas where ".$filtro_fecha2." AND lecturista1_id=e.id and (up1 in ('1','2') or up2 in ('1','2') or up3 in ('1','2')) ) as lecturas_terminadas,
			(select count(*) from lecturas.comlec_ordenlecturas where ".$filtro_fecha2." AND lecturista1_id=e.id and resultadoevaluacion='INCONSISTENTE' ) as lecturas_inconsistentes,
			(select count(*) from lecturas.comlec_ordenlecturas where ".$filtro_fecha2." AND lecturista1_id=e.id and validacion>='1' ) as lecturas_evaluadas,
			(select count(*) from lecturas.comlec_ordenlecturas where ".$filtro_fecha2." AND lecturista1_id=e.id and resultadoevaluacion='CONSISTENTE' ) as lecturas_consistentes,
			(select count(*) from lecturas.comlec_ordenlecturas where ".$filtro_fecha2." AND lecturista2_id=e.id and tipolectura='R') as relecturas_asignadas,
			(select count(*) from lecturas.comlec_ordenlecturas where ".$filtro_fecha2." AND lecturista3_id=e.id and tipolectura='RR') as rerelecturas_asignadas,
			(select count(*) from lecturas.comlec_ordenlecturas where ".$filtro_fecha2." AND lecturista1_id=e.id and (obs1 IS NOT NULL AND obs1<>'')) as obs_total,
			(select count(*) from lecturas.comlec_ordenlecturas where ".$filtro_fecha2." AND lecturista1_id=e.id and (lecturao1 IS NULL OR lecturao1='') AND (obs1 IS NOT NULL OR obs1<>'')) as obs_sin_lectura,
			(select count(*) from lecturas.comlec_ordenlecturas where ".$filtro_fecha2." AND lecturista1_id=e.id and (lecturao1 IS NOT NULL AND lecturao1<>'') AND (obs1 IS NOT NULL AND obs1<>'')) as obs_con_lectura,
			(select avg(tiempo) from (
			select *, age(F1, F2) as tiempo from (
			(select *, ROW_NUMBER() OVER() as I1 from (select fechaejecucion1 AS F1 from lecturas.comlec_ordenlecturas WHERE ".$filtro_fecha2." AND lecturista1_id=e.id and fechaejecucion1 is not null order by fechaejecucion1 asc) as t1) as t11
			 LEFT OUTER JOIN 
			(select *, ROW_NUMBER() OVER() as I2 from (select fechaejecucion1 AS F2 from lecturas.comlec_ordenlecturas WHERE ".$filtro_fecha2." AND lecturista1_id=e.id and fechaejecucion1 is not null order by fechaejecucion1 asc) as t2) as t22
			on t11.I1=t22.I2+1) as t3
			) as t4) as tiempo_promedio 
			from lecturas.comlec_ordenlecturas o 
			INNER JOIN lecturas.glomas_empleados e 
			ON o.lecturista1_id=e.id 
			WHERE ".$filtro_fecha." 
			GROUP BY e.id,e.nombre, lecturista1_id";
		
		//echo $sql;exit;
		$arr_obj_lecturasmovil = $db->query($sql);
		
		return $arr_obj_lecturasmovil;
	}
}