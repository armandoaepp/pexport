<?php
App::uses('AppModel', 'Model');
/**
 * Activity Model
 *
 * @property Factura $Factura
*/ 
class Factura extends AppModel {
	/**
	 * Display field
	 *
	 * @var string
	 */
	public $name = 'Factura';
	
	public $useTable = false;
	
	public $esquema='facturacion';

	/**
	 * $belongsTo associations
	 *
	 * @var array
	 */
	
	public function insertarPliego($data,$id_usuario){
		$this->setDateSourceCustom();
		$db = $this->getDataSource();
		
		$esquema='facturacion';
		$nombretabla='tmp_pliego';
		
		$campostabla = 'idsistemaelectrico,fechadesde,idpliegotarifadetalle,idtarifaconceptorango,conceptorango,preciofose,preciosinfose';
		$camposxls = 'idSistemaElectrico,fechadesde,IdPliegoTarifaDetalle,IdTarifaConceptoRango,conceptorango,Precio,PrecioSinFose';
		
		try {
			
			$db->begin();
			
			$str_sql_head = '';
			$str_sql_body = '';
			$i=0;
			foreach ($data as $key => $value){
				$i++;
	    		if ($i==1){
	    			$arr_head = $value;
	    		}elseif ($i>1){
	    			$campostabla2='';
					$arr_campos_xls = explode(',',$camposxls);
					foreach ($arr_campos_xls as $campo){
						$campo = trim($campo);
						$dato_campo= htmlspecialchars(@$value[array_search($campo, $arr_head)], ENT_QUOTES);
						$campostabla2.="'".trim($dato_campo)."',";
					}
					
					$str_sql_head = "INSERT INTO ".$esquema.".".$nombretabla." ( ".$campostabla." ,created, idusuario) VALUES ";
					$str_sql_body .= "(".$campostabla2." '".date('Y-m-d H:i:s')."',".$id_usuario."),";
				}
			}
			if($i>0){
				unset ($i);
				$sql_format_date = 'SET datestyle = "ISO, DMY";';
				$sql_xls_clear="TRUNCATE ".$esquema.".".$nombretabla.";";
								
				$sql_multiple = $str_sql_head.substr($str_sql_body,0,-1).";";
				//echo $sql_multiple;exit;
				
				$db->query($sql_format_date);
				$db->query($sql_xls_clear);
				$db->query($sql_multiple);
				
				return $db->commit();
			}else{
				return false;
			}
		} catch (Exception $e) {
			$db->rollback();
			$this->log('ERROR:'.$e, 'import_file_pliego'.date('Y-m-d'));
			return NULL;
		}
	}
	
	public function insertarCrm($data,$id_usuario){
		$this->setDateSourceCustom();
		$db = $this->getDataSource();
	
		$esquema='facturacion';
		$nombretabla='tmp_pliegocrm';
	
		$campostabla = 'idpliegocrm,fechadesde,fechahasta,idplancondicion,idtarifa,idconcepto,precio,idestado,fechapublicado';
		$camposxls = 'idPliegoCRM,fechadesde,fechahasta,idPlanCondicion,IdTarifa,IdConcepto,Precio,idestado,fechapublicado';
	
		try {
				
			$db->begin();
				
			$str_sql_head = '';
			$str_sql_body = '';
			$i=0;
			foreach ($data as $key => $value){
				$i++;
				if ($i==1){
					$arr_head = $value;
				}elseif ($i>1){
					$campostabla2='';
					$arr_campos_xls = explode(',',$camposxls);
					foreach ($arr_campos_xls as $campo){
						$campo = trim($campo);
						$dato_campo= htmlspecialchars(@$value[array_search($campo, $arr_head)], ENT_QUOTES);
						if($campo=='fechahasta'){
							$campostabla2.="null,";
						}else{
							$campostabla2.="'".trim($dato_campo)."',";
						}
					}
						
					$str_sql_head = "INSERT INTO ".$esquema.".".$nombretabla." ( ".$campostabla." ,created, idusuario) VALUES ";
					$str_sql_body .= "(".$campostabla2." '".date('Y-m-d H:i:s')."',".$id_usuario."),";
				}
			}
			if($i>0){
				unset ($i);
				$sql_format_date = 'SET datestyle = "ISO, DMY";';
				$sql_xls_clear="TRUNCATE ".$esquema.".".$nombretabla.";";
	
				$sql_multiple = $str_sql_head.substr($str_sql_body,0,-1).";";
				//echo $sql_multiple;exit;
	
				$db->query($sql_format_date);
				$db->query($sql_xls_clear);
				$db->query($sql_multiple);
	
				return $db->commit();
			}else{
				return false;
			}
		} catch (Exception $e) {
			$db->rollback();
			$this->log('ERROR:'.$e, 'import_file_crm'.date('Y-m-d'));
			return NULL;
		}
	}
	
	public function insertarCronograma($data,$id_usuario){
		$this->setDateSourceCustom();
		$db = $this->getDataSource();
	
		$esquema='facturacion';
		$nombretabla='tmp_cronogramas';
	
		$campostabla = 'idempresaservicio,idservicioflujo,iduunn,cartera,idactividad,idciclo,
				iditeracion,periodo,estado,fecha,fechaejecucion,idestadoejecucion';
		$camposxls = 'IdEmpresaServicio,IdServicioFlujo,IdUUNN,Cartera,IdActividad,IdCiclo,
				IdIteracion,Periodo,Estado,Fecha,FechaEjecucion,IdEstadoEjecucion';
	
		try {
	
			$db->begin();
	
			$str_sql_head = '';
			$str_sql_body = '';
			$i=0;
			foreach ($data as $key => $value){
				$i++;
				if ($i==1){
					$arr_head = $value;
				}elseif ($i>1){
					$campostabla2='';
					$arr_campos_xls = explode(',',$camposxls);
					foreach ($arr_campos_xls as $campo){
						$campo = trim($campo);
						$dato_campo= htmlspecialchars(@$value[array_search($campo, $arr_head)], ENT_QUOTES);
						$campostabla2.="'".trim($dato_campo)."',";
					}
	
					$str_sql_head = "INSERT INTO ".$esquema.".".$nombretabla." ( ".$campostabla." ,created, idusuario) VALUES ";
					$str_sql_body .= "(".$campostabla2." '".date('Y-m-d H:i:s')."',".$id_usuario."),";
				}
			}
			if($i>0){
				unset ($i);
				$sql_format_date = 'SET datestyle = "ISO, DMY";';
				$sql_xls_clear="TRUNCATE ".$esquema.".".$nombretabla.";";
	
				$sql_multiple = $str_sql_head.substr($str_sql_body,0,-1).";";
				//echo $sql_multiple;exit;
	
				$db->query($sql_format_date);
				$db->query($sql_xls_clear);
				$db->query($sql_multiple);
	
				return $db->commit();
			}else{
				return false;
			}
		} catch (Exception $e) {
			$db->rollback();
			$this->log('ERROR:'.$e, 'import_file_cronograma'.date('Y-m-d'));
			return NULL;
		}
	}
	
	public function insertarUityAlicuota($periodo,$uit,$alicuota,$id_usuario){
		$this->setDateSourceCustom();
		$db = $this->getDataSource();
		
		$esquema='facturacion';
		$nombretabla='tmp_uit_alicuota';
		
		try {
		
			$db->begin();
		
			$sql_xls_clear="TRUNCATE ".$esquema.".".$nombretabla.";";
		
			$sql_multiple = "INSERT INTO ".$esquema.".".$nombretabla."(uit, alicuota, created, idusuario)
	    		VALUES (".$uit.", ".$alicuota.", '".date('Y-m-d H:i:s')."', ".$id_usuario.");";
			//echo $sql_multiple;exit;
			
			$sql_calcular_aps = "select facturacion.calcularaps('".$periodo."', ".$alicuota.", ".$uit.");";
	
			$db->query($sql_xls_clear);
			$db->query($sql_multiple);
			$db->query($sql_calcular_aps);
			
			return $db->commit();
			
		} catch (Exception $e) {
			$db->rollback();
			$this->log('ERROR:'.$e, 'import_uit_y_alicuota'.date('Y-m-d'));
			return NULL;
		}
	}
	
	public function getUitActual(){
		$this->setDateSourceCustom();
		$db = $this->getDataSource();
	
		$sql = "select valor from facturacion.constantes where idconstante=2 and bestado=true;";
	
		$result = $db->query($sql);
    
    	if($result){
    		return $result[0][0]['valor'];
    	}
    	return  null;
	}
	
	public function getAlicuotaActual(){
		$this->setDateSourceCustom();
		$db = $this->getDataSource();
	
		$sql = "select valor from facturacion.constantes where idconstante=1 and bestado=true;";
	
		$result = $db->query($sql);
	
		if($result){
			return $result[0][0]['valor'];
		}
		return  null;
	}
	
	public function getAlumbradoPublico($periodo){
		$this->setDateSourceCustom();
		$db = $this->getDataSource();
		
		$sql = "select * from facturacion.tmp_aps where periodo='".$periodo."' and bestado=true";
		//echo $sql;exit;
		
		return  $db->query($sql);
	}
	
	public function sincronizarAll(){
		$this->setDateSourceCustom();
		$db = $this->getDataSource();
	
		$sql = "select facturacion.sincronizartodo() as valor;";
	
		$result = $db->query($sql);
	
		if($result){
			return $result[0][0]['valor'];
		}
		return  null;
	}
	
	public function sincronizarPliegos(){
		$this->setDateSourceCustom();
		$db = $this->getDataSource();
	
		$sql = "select facturacion.sincronizarpliegos() as valor;";
	
		$result = $db->query($sql);
	
		if($result){
			return $result[0][0]['valor'];
		}
		return  null;
	}
	
	public function getCountPliegos(){
		$this->setDateSourceCustom();
		$db = $this->getDataSource();
	
		$sql = "select count(*) as cantidad from (select fechadesde from facturacion.pliegos group by fechadesde) T;";
	
		$result = $db->query($sql);
	
		if($result){
			return $result[0][0]['cantidad'];
		}
		return  0;
	}
	
	public function getMinDatePliegos(){
		$this->setDateSourceCustom();
		$db = $this->getDataSource();
	
		$sql = "select min(fechadesde) as min from facturacion.pliegos;";
	
		$result = $db->query($sql);
	
		if($result){
			return $result[0][0]['min'];
		}
		return  0;
	}
	
	public function getMaxDatePliegos(){
		$this->setDateSourceCustom();
		$db = $this->getDataSource();
	
		$sql = "select max(fechadesde) as max from facturacion.pliegos;";
	
		$result = $db->query($sql);
	
		if($result){
			return $result[0][0]['max'];
		}
		return  0;
	}
	
	public function getCountPliegosCrm(){
		$this->setDateSourceCustom();
		$db = $this->getDataSource();
	
		$sql = "select count(*) as cantidad from facturacion.pliegocrm;";
	
		$result = $db->query($sql);
	
		if($result){
			return $result[0][0]['cantidad'];
		}
		return  0;
	}
	
	public function getMinDatePliegosCrm(){
		$this->setDateSourceCustom();
		$db = $this->getDataSource();
	
		$sql = "select min(fechadesde) as min from facturacion.pliegocrm;";
	
		$result = $db->query($sql);
	
		if($result){
			return $result[0][0]['min'];
		}
		return  0;
	}
	
	public function getMaxDatePliegosCrm(){
		$this->setDateSourceCustom();
		$db = $this->getDataSource();
	
		$sql = "select max(fechadesde) as max from facturacion.pliegocrm;";
	
		$result = $db->query($sql);
	
		if($result){
			return $result[0][0]['max'];
		}
		return  0;
	}
	
	public function getCountCronograma(){
		$this->setDateSourceCustom();
		$db = $this->getDataSource();
	
		$sql = "select count(*) as cantidad from facturacion.cronogramas;";
	
		$result = $db->query($sql);
	
		if($result){
			return $result[0][0]['cantidad'];
		}
		return  0;
	}
	
	public function getMinDateCronograma(){
		$this->setDateSourceCustom();
		$db = $this->getDataSource();
	
		$sql = "select min(to_date(fecha,'DD/MM/YYYY')) as min from facturacion.cronogramas where fecha<>'';";
	
		$result = $db->query($sql);
	
		if($result){
			return $result[0][0]['min'];
		}
		return  0;
	}
	
	public function getMaxDateCronograma(){
		$this->setDateSourceCustom();
		$db = $this->getDataSource();
	
		$sql = "select max(to_date(fecha,'DD/MM/YYYY')) as max from facturacion.cronogramas where fecha<>'';";
	
		$result = $db->query($sql);
	
		if($result){
			return $result[0][0]['max'];
		}
		return  0;
	}
	
	public function getAllPliegos($criterio=null,$date_start=null,$date_end=null){
		$this->setDateSourceCustom();
		$db = $this->getDataSource();
		
		$str_where = "";
		if(strpos($criterio, 'F') !== false){
			$str_where .= " AND fechadesde >= '".$date_start."' AND fechadesde <= '".$date_end." 23:59:59'";
		}
	
		$sql = "select distinct fechadesde from facturacion.pliegos WHERE 1=1 ".$str_where." order by fechadesde asc";
		//echo $sql;exit;
	
		return  $db->query($sql);
	}
	
	public function getDetallePliego($pliego){
		$this->setDateSourceCustom();
		$db = $this->getDataSource();
		
		$sql = "select s.nombresistemaelectrico,s.abreviaistemaelectrico,t.descripcion as tarifa,c.nombreconcepto,pd.preciofose,pd.preciosinfose,pd.fechadesde from facturacion.pliegosdetalle pd
		inner join facturacion.sistemaselectricos s
		on pd.idsistemaelectrico=s.idsistemaelectrico
		inner join facturacion.tarifaconceptorango tcr
		on tcr.idtarifaconceptorango=pd.idtarifaconceptorango
		inner join facturacion.tarifaconceptos tc
		on tc.idtarifaconcepto=tcr.idtarifaconcepto
		inner join facturacion.conceptos c
		on c.idconcepto=tc.idconcepto
		inner join facturacion.tarifas t
		on t.idtarifa=tc.idtarifa
		where pd.fechadesde='".$pliego."' order by s.idsistemaelectrico asc";
		//echo $sql;exit;
		
		return  $db->query($sql);
	}
	
	public function getCountFacturas(){
		$this->setDateSourceCustom();
		$db = $this->getDataSource();
	
		$sql = "select count(*) as cantidad  
			from lecturas.comlec_ordenlecturas ol
			where factura<>''";
		//echo $sql;exit;
	
		$result = $db->query($sql);
	
		if($result){
			return $result[0][0]['cantidad'];
		}
		return  0;
	}
	
	public function getFacturas(){
		$this->setDateSourceCustom();
		$db = $this->getDataSource();
	
		$sql = "select suministro, cliente, direccion, lecturao1, obs1, montoconsumo1, resultadoevaluacion, fechaejecucion1
			from lecturas.comlec_ordenlecturas ol
			where factura<>'' order by fechaejecucion1 desc";
		//echo $sql;exit;
	
		return $db->query($sql);
	}
	
	public function getFacturaBySuministro($suministro){
		$this->setDateSourceCustom();
		$db = $this->getDataSource();
		
		$sql = "select suministro, cliente, direccion, lecturao1, obs1, montoconsumo1, resultadoevaluacion, fechaejecucion1, factura
			from lecturas.comlec_ordenlecturas ol
			where factura<>'' and suministro='".$suministro."'";
		//echo $sql;exit;
		
		return $db->query($sql);
	}
}