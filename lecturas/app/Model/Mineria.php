<?php
App::uses('AppModel', 'Model');
/**
 * Activity Model
 *
 * @property Mineria $Mineria
*/ 
class Mineria extends AppModel {
	/**
	 * Display field
	 *
	 * @var string
	 */
	public $name = 'Mineria';

	public $useTable = false;

	/**
	 * $belongsTo associations
	 *
	 * @var array
	 */
	
	public function listar($page,$limit,$search=''){
		$this->setDataSource ('mineria');
		$db = $this->getDataSource ();
		
		$str_search = "";
		if($search!=''){
			$str_search = " AND em.Suministro='".$search."'";
		}
		$sql = "select top ".$limit." * from (select em.Suministro,
		em.Periodo,
		ss.lecturao,
		ss.metodofinal1,
		ss.validacion,
		ss.sanearmovil,
		ss.primera,
		ss.demandaact,
		round(em.pronostico,2) as pronostico,
		round(em.Minima80,0) as Minima80,
		round(em.Maxima80,0) as Maxima80,
		case when demandaact between round(em.Minima80,0) and round(em.Maxima80,0) then 1 else 0 end as Consistencia80,
		round(em.Minima95,0) as Minima95,
		round(em.Maxima95,0) as Maxima95,
		case when demandaact between round(em.Minima95,0) and round(em.Maxima95,0) then 1 else 0 end as Consistencia95,
    	COUNT(*) OVER() AS row_total,
		ROW_NUMBER() OVER (ORDER BY em.Suministro) as row 
		from Estimaciones_Mineria em inner join SuperSet ss
		on ss.suministro=em.Suministro where pfactura='201408' and demandaact is not null ".$str_search." 
		) T where row > ".($page*$limit)." order by row";
		$arr_result = $db->query($sql);
		return $arr_result;
	}
	
	public function buscarHistoricoPorSuministro($suministro){
		
		$this->setDataSource ('mineria');
		$db = $this->getDataSource ();
		$sql = "select top 24 pfactura,demandaact from Superset where suministro = '$suministro' order by pfactura";
		$arr_result = $db->query($sql);
		return $arr_result;
	}
	
	public function listar2($page,$limit,$search=''){
		$this->setDateSourceCustom();
    	$db = $this->getDataSource();
	
		$str_search = "";
		if($search!=''){
			$str_search = " AND ss.suministro='".$search."'";
		}
		$sql = 'select em."Suministro",em."Periodo",ss.lecturao1 as lecturao,ss.metodofinal1,ss.validacion,ss.sanearmovil,ss.lecturaoold as primera,ss.montoconsumo1 as demandaact,round(cast(em."Pronostico" as NUMERIC),2) as pronostico,
		round(cast(em."Minima80" as NUMERIC),0) as Minima80,
		round(cast(em."Maxima80" as NUMERIC),0) as Maxima80,
		case when montoconsumo1 between round(cast(em."Minima80" as NUMERIC),0) and round(cast(em."Maxima80" as NUMERIC),0) then 1 else 0 end as Consistencia80,
		round(cast(em."Minima95" as NUMERIC),0) as Minima95,
		round(cast(em."Maxima95" as NUMERIC),0) as Maxima95,
		case when montoconsumo1 between round(cast(em."Minima95" as NUMERIC),0) and round(cast(em."Maxima95" as NUMERIC),0) then 1 else 0 end as Consistencia95, COUNT(*) OVER() as row_total from migracion.mineria201408 em inner join 
temporal.comlec_ordenlecturas ss on ss.suministro=em."Suministro" where pfactura=\'201408\' and montoconsumo1 is not null and metodofinal1=\'2\' and validacion = \'1\'  '.$str_search.'  
		LIMIT '.intval( $limit ).' OFFSET '.intval( $page);

		$arr_result = $db->query($sql);
		return $arr_result;
	}
	
	public function buscarHistoricoPorSuministro2($suministro){
	
		$this->setDateSourceCustom();
    	$db = $this->getDataSource();
		$sql = "select pfactura,montoconsumo1 as demandaact from history.comlec_ordenlecturas where suministro = '$suministro' order by pfactura limit 24";
		$arr_result = $db->query($sql);
		return $arr_result;
	}
	
	public function getCountTotal(){
		$this->setDateSourceCustom();
		$db = $this->getDataSource();
	
		$sql = 'select count(*) as cantidad
from migracion.mineria201408 em inner join
temporal.comlec_ordenlecturas ss on ss.suministro=em."Suministro" where pfactura=\'201408\' and montoconsumo1 is not null  ';
	
		$result = $db->query($sql);
			
		if($result){
			return $result[0][0]['cantidad'];
		}
		return  0;
	}
	
	public function getCountTotalMetodo2(){
		$this->setDateSourceCustom();
		$db = $this->getDataSource();
	
		$sql = 'select count(*) as cantidad
from migracion.mineria201408 em inner join
temporal.comlec_ordenlecturas ss on ss.suministro=em."Suministro" where pfactura=\'201408\' and montoconsumo1 is not null and metodofinal1=\'2\' and validacion = \'1\'';
	
		$result = $db->query($sql);
		 
		if($result){
			return $result[0][0]['cantidad'];
		}
		return  0;
	}
	
	public function getCountConsistentes80(){
		$this->setDateSourceCustom();
		$db = $this->getDataSource();
	
		$sql = 'select count(*) as cantidad
from migracion.mineria201408 em inner join
temporal.comlec_ordenlecturas ss on ss.suministro=em."Suministro" where pfactura=\'201408\' and montoconsumo1 is not null and metodofinal1=\'2\' and validacion = \'1\'  and 
case when montoconsumo1 between round(cast(em."Minima80" as NUMERIC),0) and round(cast(em."Maxima80" as NUMERIC),0) then 1 else 0 end=\'1\'  ';
	
		$result = $db->query($sql);
    	
    	if($result){
    		return $result[0][0]['cantidad'];
    	}
    	return  0;
	}
	
	public function getCountInconsistentes80(){
		$this->setDateSourceCustom();
		$db = $this->getDataSource();
	
		$sql = 'select count(*) as cantidad
from migracion.mineria201408 em inner join
temporal.comlec_ordenlecturas ss on ss.suministro=em."Suministro" where pfactura=\'201408\' and montoconsumo1 is not null and metodofinal1=\'2\' and validacion = \'1\'  and
case when montoconsumo1 between round(cast(em."Minima80" as NUMERIC),0) and round(cast(em."Maxima80" as NUMERIC),0) then 1 else 0 end=\'0\'  ';
	
		$result = $db->query($sql);
		 
		if($result){
			return $result[0][0]['cantidad'];
		}
		return  0;
	}
	
	public function getCountConsistentes95(){
		$this->setDateSourceCustom();
		$db = $this->getDataSource();
	
		$sql = 'select count(*) as cantidad
from migracion.mineria201408 em inner join
temporal.comlec_ordenlecturas ss on ss.suministro=em."Suministro" where pfactura=\'201408\' and montoconsumo1 is not null and metodofinal1=\'2\' and validacion = \'1\'  and
case when montoconsumo1 between round(cast(em."Minima95" as NUMERIC),0) and round(cast(em."Maxima95" as NUMERIC),0) then 1 else 0 end=\'1\'  ';
	
		$result = $db->query($sql);
		 
		if($result){
			return $result[0][0]['cantidad'];
		}
		return  0;
	}
	
	public function getCountInconsistentes95(){
		$this->setDateSourceCustom();
		$db = $this->getDataSource();
	
		$sql = 'select count(*) as cantidad
from migracion.mineria201408 em inner join
temporal.comlec_ordenlecturas ss on ss.suministro=em."Suministro" where pfactura=\'201408\' and montoconsumo1 is not null and metodofinal1=\'2\' and validacion = \'1\'  and
case when montoconsumo1 between round(cast(em."Minima95" as NUMERIC),0) and round(cast(em."Maxima95" as NUMERIC),0) then 1 else 0 end=\'0\'  ';
	
		$result = $db->query($sql);
			
		if($result){
			return $result[0][0]['cantidad'];
		}
		return  0;
	}
	
	public function listar_mineria2_sql($pfactura,$page,$limit,$search=''){
		$this->setDataSource ('mineria');
		$db = $this->getDataSource ();
	
		$str_search = "";
		if($search!=''){
			$str_search = " AND em.Suministro='".$search."'";
		}
		if($pfactura=='201408'){
			$ordentrab = '';
		}elseif($pfactura=='201407'){
			$ordentrab = '25100336005';
			
			$sql = "select top ".$limit." * from (select
ss.pfactura,
ss.suministro,
ss.lecturao,
ss.codlectura,
ss.demandaact,
case when metodofinal1 = 1 then 1 else 0 end as metodo1_pex,
case when metodofinal1 = 2 and validacion = 0 then 1 else 0 end as metodo2_pex,
case when ej.suministro is null then 1 else 0 end as metodoENSA,
case when ss.demandaact between round(em.Minima80,0) and round(em.Maxima80,0) and em.Suministro is not null then 1 else 0 end as metodo_mineria_80,
case when ss.demandaact between round(em.Minima95,0) and round(em.Maxima95,0) and em.Suministro is not null then 1 else 0 end as metodo_mineria_95,
case when validacion = 1 then 1 else 0 end as fuerzabruta,
case when rj.b is not null then 1 else 0 end as reclamo,
rj.infundado as situacion,
rj.f as f,
rj.g as g,
COUNT(*) OVER() AS row_total,
ROW_NUMBER() OVER (ORDER BY ss.Suministro) as row
from SuperSet ss
left outer join Estimaciones_Mineria".$pfactura." em
on ss.suministro=em.Suministro
left outer join InconsistenciaEnsa".$pfactura." ej
on ss.suministro = ej.suministro
left outer join Reclamo".$pfactura." rj
on ss.suministro = rj.b
where ss.pfactura='".$pfactura."' ".$str_search." and ss.ordtrabajo='".$ordentrab."'
		) T where row > ".($page*$limit)." order by row";
		}elseif($pfactura=='201405'){
			$ordentrab = '25100324108';
			
			$sql = "select top ".$limit." * from (
select 
ss.pfactura,
ss.suministro,
ss.lecturao,
ss.codlectura,
ss.demandaact,
case when metodofinal1 = 1 then 1 else 0 end as metodo1_pex,
case when metodofinal1 = 2 and validacion = 0 then 1 else 0 end as metodo2_pex,
case when ej.suministro is null then 1 else 0 end as metodoENSA,
case when ss.demandaact between round(em.Minima80,0) and round(em.Maxima80,0) and em.Suministro is not null then 1 else 0 end as metodo_mineria_80,
case when ss.demandaact between round(em.Minima95,0) and round(em.Maxima95,0) and em.Suministro is not null then 1 else 0 end as metodo_mineria_95,
case when validacion = 1 then 1 else 0 end as fuerzabruta,
case when rj.NroServicio is not null then 1 else 0 end as reclamo,
COUNT(*) OVER() AS row_total,
ROW_NUMBER() OVER (ORDER BY ss.Suministro) as row 
from SuperSet ss 
left outer join Estimaciones_Mineria201405 em 
on ss.suministro=em.Suministro 
left outer join InconsistenciaEnsa201405 ej  
on ss.suministro = ej.suministro 
left outer join Reclamo201405 rj 
on ss.suministro = rj.NroServicio 
where ss.pfactura='".$pfactura."' ".$str_search." and ss.ordtrabajo='".$ordentrab."'
		) T where row > ".($page*$limit)." order by row";
		}
		
		
		
		$arr_result = $db->query($sql);
		return $arr_result;
	}

}