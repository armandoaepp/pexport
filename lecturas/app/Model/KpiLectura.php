<?php
App::uses('AppModel', 'Model');
/**
 * Activity Model
 *
 * @property KpiLectura $KpiLectura
*/ 
class KpiLectura extends AppModel {
	/**
	 * Display field
	 *
	 * @var string
	 */
	public $name = 'KpiLectura';
	
	public $useTable = false;
	
	public $esquema='sumary';

	/**
	 * $belongsTo associations
	 *
	 * @var array
	 */
	
	public function getDateLastUpdate($options=null){
		$this->setDateSourceCustom();
		$db = $this->getDataSource();
	
		$str_where = '';
		if(isset($options) && is_array($options)){
			if(isset($options['unidad_neg']) and $options['unidad_neg']!=0){
				$str_where .= ' AND glomas_unidadnegocio_id = '.$options['unidad_neg'];
			}
			if(isset($options['idciclo']) && $options['idciclo']!='0'){
				$str_where .= " AND idciclo='".$options['idciclo']."'";
			}
		}
	
		$sql="select max(fecha_last_update) as fecha_last_update 
		from sumary.kpi_lecturas where 1=1 ".$str_where."";
	
		$result = $db->query($sql);
		
		if($result){
			return $result[0][0]['fecha_last_update'];
		}
		return  0;
	}
	
	public function getKpiByFecha($options=null,$fecha=null){
		$this->setDateSourceCustom();
    	$db = $this->getDataSource();
    	
    	$str_where = '';
    	if(isset($options) && is_array($options)){
    		if(isset($options['unidad_neg']) and $options['unidad_neg']!=0){
    			$str_where .= ' AND glomas_unidadnegocio_id = '.$options['unidad_neg'];
    		}
	    	if(isset($options['idciclo']) && $options['idciclo']!='0'){
	    		$str_where .= " AND idciclo='".$options['idciclo']."'";
	    	}
    	}
    
    	if(isset($fecha)){
    		$str_where .= " AND fecha >= '".$fecha."'";
    	}else{
    		$str_where .= " AND fecha >= '".date('Y-m-d')."'";
    	}
    	$schema = 'sumary';
    
    	$sql="select
			sum(count_suministros) as count_suministros,
			sum(count_asignados) as count_asignados,
			sum(count_descargados) as count_descargados,
			sum(count_finalizados) as count_finalizados,
			sum(count_pendientes) as count_pendientes,
			sum(count_inconsistentes_actuales) as count_inconsistentes_actuales,
			sum(count_inconsistentes_evaluadas) as count_inconsistentes_evaluadas,
			sum(count_consistentes) as count_consistentes,
			sum(count_observaciones) as count_observaciones,
			sum(count_observaciones_con_lectura) as count_observaciones_con_lectura,
			sum(count_observaciones_sin_lectura) as count_observaciones_sin_lectura,
			sum(count_observaciones_99) as count_observaciones_99,
			sum(count_consumo_cero) as count_consumo_cero,
			sum(count_consumo_menor_35) as count_consumo_menor_35,
			sum(count_consumo_mayor_1000) as count_consumo_mayor_1000,
			sum(count_consumo_mayor_100_porciento) as count_consumo_mayor_100_porciento,
			avg(tiempo_promedio_ejecucion) as tiempo_promedio_ejecucion,
			sum(sum_montoconsumo) as sum_montoconsumo,
    		sum(count_enviados_distriluz) as count_enviados_distriluz
  			from sumary.kpi_lecturas WHERE 1=1 ".$str_where;

    	$arr_result = $db->query($sql);
		return $arr_result;
	}
	
	public function getKpi($options=null){
		$this->setDateSourceCustom();
		$db = $this->getDataSource();
		 
		$schema = 'sumary';
		$str_where = '';
    	if(isset($options) && is_array($options)){
    		if(isset($options['unidad_neg']) and $options['unidad_neg']!=0){
    			$str_where .= ' AND glomas_unidadnegocio_id = '.$options['unidad_neg'];
    		}
    		if(isset($options['idciclo']) && $options['idciclo']!='0'){
    			$str_where .= " AND idciclo='".$options['idciclo']."'";
    		}
    		if(isset($options['pfactura']) and $options['pfactura']!='0'){
    			$str_where .= " AND pfactura = '".$options['pfactura']."'";
    			$schema = 'history';
    		}else{
    			$str_where .= '';
    			$schema = 'sumary';
    		}    		
    	}
	
		$sql="select
			sum(count_suministros) as count_suministros,
			sum(count_asignados) as count_asignados,
			sum(count_descargados) as count_descargados,
			sum(count_finalizados) as count_finalizados,
			sum(count_pendientes) as count_pendientes,
			sum(count_inconsistentes_actuales) as count_inconsistentes_actuales,
			sum(count_inconsistentes_evaluadas) as count_inconsistentes_evaluadas,
			sum(count_consistentes) as count_consistentes,
			sum(count_observaciones) as count_observaciones,
			sum(count_observaciones_con_lectura) as count_observaciones_con_lectura,
			sum(count_observaciones_sin_lectura) as count_observaciones_sin_lectura,
			sum(count_observaciones_99) as count_observaciones_99,
			sum(count_consumo_cero) as count_consumo_cero,
			sum(count_consumo_menor_35) as count_consumo_menor_35,
			sum(count_consumo_mayor_1000) as count_consumo_mayor_1000,
			sum(count_consumo_mayor_100_porciento) as count_consumo_mayor_100_porciento,
			avg(tiempo_promedio_ejecucion) as tiempo_promedio_ejecucion,
			sum(sum_montoconsumo) as sum_montoconsumo,
    		sum(count_enviados_distriluz) as count_enviados_distriluz
  			from ".$schema.".kpi_lecturas WHERE 1=1 ".$str_where;
	
		$arr_result = $db->query($sql);
		return $arr_result;
	}
	
	
	public function getConsumoPorDia($options=null){
		$this->setDateSourceCustom();
		$db = $this->getDataSource();
	
		$str_where = '';
		if(isset($options) && is_array($options)){
			if(isset($options['unidad_neg']) and $options['unidad_neg']!=0){
				$str_where .= ' AND glomas_unidadnegocio_id = '.$options['unidad_neg'];
			}
			if(isset($options['idciclo']) && $options['idciclo']!='0'){
				$str_where .= " AND idciclo='".$options['idciclo']."'";
			}
		}
	
		$sql="select date_trunc('day', fecha) as fecha, sum(sum_montoconsumo) as montoconsumo
from sumary.kpi_lecturas where 1=1 ".$str_where."
group by date_trunc('day', fecha)
order by 1 asc";
	
		return $db->query($sql);
	}
	
	public function getResumenPorCiclos($options=null){
		$this->setDateSourceCustom();
		$db = $this->getDataSource();
	
		$str_where = '';
		if(isset($options) && is_array($options)){
			if(isset($options['unidad_neg']) and $options['unidad_neg']!=0){
				$str_where .= ' AND glomas_unidadnegocio_id = '.$options['unidad_neg'];
			}
			if(isset($options['idciclo']) && $options['idciclo']!='0'){
				$str_where .= " AND idciclo='".$options['idciclo']."'";
			}
		}
	
		$sql="select idciclo, string_agg(sector, ', ') AS sectores, sum(count_suministros) as count_suministros,
sum(count_asignados) as count_asignados,
sum(count_descargados) as count_descargados,
sum(count_finalizados) as count_finalizados,
sum(count_pendientes) as count_pendientes,
sum(count_inconsistentes_actuales) as count_inconsistentes_actuales,
sum(count_inconsistentes_evaluadas) as count_inconsistentes_evaluadas,
sum(count_consistentes) as count_consistentes,
sum(count_observaciones) as count_observaciones,
sum(count_observaciones_con_lectura) as count_observaciones_con_lectura,
sum(count_observaciones_sin_lectura) as count_observaciones_sin_lectura,
sum(count_observaciones_99) as count_observaciones_99,
sum(count_consumo_cero) as count_consumo_cero,
sum(count_consumo_menor_35) as count_consumo_menor_35,
sum(count_consumo_mayor_1000) as count_consumo_mayor_1000,
sum(count_consumo_mayor_100_porciento) as count_consumo_mayor_100_porciento,
avg(tiempo_promedio_ejecucion) as tiempo_promedio_ejecucion,
sum(sum_montoconsumo) as sum_montoconsumo,
sum(count_enviados_distriluz) as count_enviados_distriluz 
  from (select idciclo, sector, sum(count_suministros) as count_suministros,
sum(count_asignados) as count_asignados,
sum(count_descargados) as count_descargados,
sum(count_finalizados) as count_finalizados,
sum(count_pendientes) as count_pendientes,
sum(count_inconsistentes_actuales) as count_inconsistentes_actuales,
sum(count_inconsistentes_evaluadas) as count_inconsistentes_evaluadas,
sum(count_consistentes) as count_consistentes,
sum(count_observaciones) as count_observaciones,
sum(count_observaciones_con_lectura) as count_observaciones_con_lectura,
sum(count_observaciones_sin_lectura) as count_observaciones_sin_lectura,
sum(count_observaciones_99) as count_observaciones_99,
sum(count_consumo_cero) as count_consumo_cero,
sum(count_consumo_menor_35) as count_consumo_menor_35,
sum(count_consumo_mayor_1000) as count_consumo_mayor_1000,
sum(count_consumo_mayor_100_porciento) as count_consumo_mayor_100_porciento,
avg(tiempo_promedio_ejecucion) as tiempo_promedio_ejecucion,
sum(sum_montoconsumo) as sum_montoconsumo,
sum(count_enviados_distriluz) as count_enviados_distriluz 
from sumary.kpi_lecturas where 1=1 ".$str_where." group by idciclo, sector ) T group by idciclo";
		 
		return $db->query($sql);
	}
	
	
	public function getRendimientoConsumo($options=null){
		$this->setDateSourceCustom();
		$db = $this->getDataSource();
	
		$str_where = '';
		if(isset($options) && is_array($options)){
			if(isset($options['unidad_neg']) and $options['unidad_neg']!=0){
				$str_where .= ' AND glomas_unidadnegocio_id = '.$options['unidad_neg'];
			}
			if(isset($options['idciclo']) && $options['idciclo']!='0'){
				$str_where .= " AND idciclo='".$options['idciclo']."'";
			}
		}
	
		$sql="select glomas_unidadnegocio_id, u.descripcion as nombreunidadnegocio,
  			idciclo, '' as nombciclo, sector, sum(sum_montoconsumo) as suma_consumo
from sumary.kpi_lecturas ol
left join lecturas.glomas_unidadnegocios u on ol.glomas_unidadnegocio_id=u.id where sum_montoconsumo is not null ".$str_where."
group by glomas_unidadnegocio_id, u.descripcion, idciclo, sector
order by 1,2,3,4,5 asc;";
		 
		return $db->query($sql);
	}
	

}