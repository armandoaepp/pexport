<?php
App::uses('AppModel', 'Model');
/**
 * Activity Model
 *
 * @property Usuario $Usuario
*/ 
class Usuario extends AppModel {
	/**
	 * Display field
	 *
	 * @var string
	 */
	public $name = 'Usuario'; 

	/**
	 * $belongsTo associations
	 *
	 * @var array
	 */
	
	
	public function IdGrupoByUsuario($usuario_id){
		$this->setDateSourceCustom ();$db = $this->getDataSource();
		$sql = "SELECT glomas_grupo_id FROM lecturas.usuarios lu inner join lecturas.glomas_empleados ge on lu.glomas_empleado_id = ge.id WHERE lu.id = $usuario_id";
		$arr_obj_usuario = $db->query($sql);
		
		if(count($arr_obj_usuario)>0){
			$grupo_id = $arr_obj_usuario[0][0]['glomas_grupo_id'];
			return $grupo_id;
		}		 
	}
	
	public function getListado(){
		$this->setDateSourceCustom ();$db = $this->getDataSource();
		$sql = "SELECT * FROM lecturas.usuarios";
		return $db->query($sql);
	}
	
	public function validarUsuarioWeb($usuario,$pass){
		$this->setDateSourceCustom ();$db = $this->getDataSource();
		$sql = "SELECT lu.id, lu.nomusuario, lu.apikey, lu.created, lu.passusuario, ge.email FROM lecturas.usuarios lu INNER JOIN lecturas.glomas_empleados ge on lu.glomas_empleado_id = ge.id WHERE glomas_tipoempleado_id = 2 AND nomusuario = '$usuario' and estado_id = '3'";
		return $db->query($sql);
	}
	
	public function updateEmail($usuario_id, $email){
		$this->setDateSourceCustom();
		$db = $this->getDataSource();
		$sql = "UPDATE lecturas.glomas_empleados SET email = '".$email."' WHERE id in (select glomas_empleado_id from lecturas.usuarios where id= ".$usuario_id.")";
		return $db->query($sql);
	} 

}