<?php
App::uses('AppModel', 'Model');
/**
 * Activity Model
 *
 * @property Mineria $Mineria
*/ 
class Excel extends AppModel {
	/**
	 * Display field
	 *
	 * @var string
	 */
	public $name = 'Excel';

	public $useTable = 'Excel';

	/**
	 * $belongsTo associations
	 *
	 * @var array
	 */
	

	public function saveExcel($query){
		$this->setDateSourceCustom();
		$db = $this->getDataSource();		
		$arr_result = $db->query($query);
		return $arr_result;
	}
}