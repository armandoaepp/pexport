<?php
/**
 * This file is loaded automatically by the app/webroot/index.php file after core.php
 *
 * This file should load/create any application wide configuration settings, such as
 * Caching, Logging, loading additional configuration files.
 *
 * You should also use this file to include any files that provide global functions/constants
 * that your application uses.
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Config
 * @since         CakePHP(tm) v 0.10.8.2117
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

// Setup a 'default' cache configuration for use in the application.
Cache::config('default', array('engine' => 'File'));

/**
 * The settings below can be used to set additional paths to models, views and controllers.
 *
 * App::build(array(
 *     'Model'                     => array('/path/to/models/', '/next/path/to/models/'),
 *     'Model/Behavior'            => array('/path/to/behaviors/', '/next/path/to/behaviors/'),
 *     'Model/Datasource'          => array('/path/to/datasources/', '/next/path/to/datasources/'),
 *     'Model/Datasource/Database' => array('/path/to/databases/', '/next/path/to/database/'),
 *     'Model/Datasource/Session'  => array('/path/to/sessions/', '/next/path/to/sessions/'),
 *     'Controller'                => array('/path/to/controllers/', '/next/path/to/controllers/'),
 *     'Controller/Component'      => array('/path/to/components/', '/next/path/to/components/'),
 *     'Controller/Component/Auth' => array('/path/to/auths/', '/next/path/to/auths/'),
 *     'Controller/Component/Acl'  => array('/path/to/acls/', '/next/path/to/acls/'),
 *     'View'                      => array('/path/to/views/', '/next/path/to/views/'),
 *     'View/Helper'               => array('/path/to/helpers/', '/next/path/to/helpers/'),
 *     'Console'                   => array('/path/to/consoles/', '/next/path/to/consoles/'),
 *     'Console/Command'           => array('/path/to/commands/', '/next/path/to/commands/'),
 *     'Console/Command/Task'      => array('/path/to/tasks/', '/next/path/to/tasks/'),
 *     'Lib'                       => array('/path/to/libs/', '/next/path/to/libs/'),
 *     'Locale'                    => array('/path/to/locales/', '/next/path/to/locales/'),
 *     'Vendor'                    => array('/path/to/vendors/', '/next/path/to/vendors/'),
 *     'Plugin'                    => array('/path/to/plugins/', '/next/path/to/plugins/'),
 * ));
 *
 */

/**
 * Custom Inflector rules can be set to correctly pluralize or singularize table, model, controller names or whatever other
 * string is passed to the inflection functions
 *
 * Inflector::rules('singular', array('rules' => array(), 'irregular' => array(), 'uninflected' => array()));
 * Inflector::rules('plural', array('rules' => array(), 'irregular' => array(), 'uninflected' => array()));
 *
 */

/**
 * Plugins need to be loaded manually, you can either load them one by one or all of them in a single call
 * Uncomment one of the lines below, as you need. Make sure you read the documentation on CakePlugin to use more
 * advanced ways of loading plugins
 *
 * CakePlugin::loadAll(); // Loads all plugins at once
 * CakePlugin::load('DebugKit'); //Loads a single plugin named DebugKit
 *
 */
	CakePlugin::load('UserPermissions');
/**
 * You can attach event listeners to the request lifecycle as Dispatcher Filter. By default CakePHP bundles two filters:
 *
 * - AssetDispatcher filter will serve your asset files (css, images, js, etc) from your themes and plugins
 * - CacheDispatcher filter will read the Cache.check configure variable and try to serve cached content generated from controllers
 *
 * Feel free to remove or add filters as you see fit for your application. A few examples:
 *
 * Configure::write('Dispatcher.filters', array(
 *		'MyCacheFilter', //  will use MyCacheFilter class from the Routing/Filter package in your app.
 *		'MyPlugin.MyFilter', // will use MyFilter class from the Routing/Filter package in MyPlugin plugin.
 * 		array('callable' => $aFunction, 'on' => 'before', 'priority' => 9), // A valid PHP callback type to be called on beforeDispatch
 *		array('callable' => $anotherMethod, 'on' => 'after'), // A valid PHP callback type to be called on afterDispatch
 *
 * ));
 */

 $port = @$_SERVER["SERVER_PORT"];
 if(isset($port) && (int) $port != 80){
 	$port = ':'.$_SERVER["SERVER_PORT"];
 }else{
 	$port = '';
 }

switch (Configure::read('environment')){
	case 'localhost':
		Configure::write('lecturas.url','http://'.$_SERVER['SERVER_NAME'].$port.'/lecturas/');
		Configure::write('lecturas.company','D&D Analytics S.R.L');
		Configure::write('lecturas.negocios',array(
			'CIX'	=>	array('name'=>'Chiclayo','empresa'=>'ENSA')
			));			
		break;
	case 'production':
		Configure::write('lecturas.url','/lecturas/');
		Configure::write('lecturas.company','PEXPORT S.A.C');
		Configure::write('lecturas.negocios',array(
			'CIX'	=>	array('name'=>'Chiclayo','empresa'=>'ENSA SA')));
		break;
	case 'production_PEXPORT':
		Configure::write('lecturas.url','/lecturas/');
		Configure::write('lecturas.company','PEXPORT S.A.C');
		Configure::write('lecturas.negocios',array(
			//'TRUX'	=>	array('name'=>'Libertad Norte - Chepen','empresa'=>'Hidrandina SA - Chepen'),
			//'TRUX_VALLE'	=>	array('name'=>'Libertad Norte - Valle Chicama','empresa'=>'Hidrandina SA - Valle Chicama'),
			//'CAJ'	=>	array('name'=>'Cajamarca','empresa'=>'Hidrandina SA - Cajamarca')));
			'HUAR'	=>	array('name'=>'Huaraz','empresa'=>'Hidrandina SA - Huaraz'),
			'CHIMB'	=>	array('name'=>'Chimbote','empresa'=>'Hidrandina SA - Chimbote')));
		break;
	default:
		Configure::write('lecturas.url','/lecturas/');
		Configure::write('lecturas.company','PEXPORT S.A.C');
		Configure::write('lecturas.negocios',array(
			'CIX'	=>	array('name'=>'Chiclayo','empresa'=>'ENSA SA')));
}

/* Configuracion usada para trabajar desde los shell, ya que alli no reconoces los environment */
Configure::write('lecturas.negocios_all',array(
'CIX'	=>	array('name'=>'Chiclayo','empresa'=>'Ensa SA','contratista'=>'PEXPORT S.A.C','logo_contratista'=>'/contratista/pexport.png')));

Configure::write('lecturas.logo_negocios',array(
'CIX'	=>	'ensa.png'));

Configure::write('lecturas.emails_supervisores_distriluz',array(
'CIX'	=>	array('yyaipenq@distriluz.com.pe','wlopezg@distriluz.com.pe','vinonanp@distriluz.com.pe')));

Configure::write('lecturas.emails_supervisores_contratistas',array(
'CIX'	=>	array('walterneyra@pexport.com.pe','santosgutierrez@pexport.com.pe','jenyminchola@pexport.com.pe')));

Configure::write('lecturas.domains',array(
'CIX'	=>	array('ip'=>'54.191.110.117','name'=>'pexport.net')));


Configure::write('Developers.email', 'ddanalytics.dev@gmail.com');

Configure::write('Developers.email_reportes', array('jenyminchola@pexport.com.pe'));


Configure::write('Dispatcher.filters', array(
	'AssetDispatcher',
	'CacheDispatcher'
));

/**
 * Configures default file logging options
 */
App::uses('CakeLog', 'Log');
CakeLog::config('debug', array(
	'engine' => 'File',
	'types' => array('notice', 'info', 'debug'),
	'file' => 'debug',
));
CakeLog::config('error', array(
	'engine' => 'File',
	'types' => array('warning', 'error', 'critical', 'alert', 'emergency'),
	'file' => 'error',
));

Configure::write('ENV_WEBROOT','/lecturas/');
Configure::write('ENV_WEBROOT_FULL_URL',Configure::read('lecturas.url'));
Configure::write('NEGOCIO','TAC');//Unidad de Negocio Activo
define('ENV_WEBROOT',Configure::read('ENV_WEBROOT'));
define('ENV_WEBROOT_FULL_URL',Configure::read('ENV_WEBROOT_FULL_URL'));
//define('NEGOCIO',Configure::read('NEGOCIO'));

Configure::write('COUNT_LIMIT_DOCUMENT_UPLOAD',1);
Configure::write('ALLOWED_EXTENSIONS_DOCUMENT_UPLOAD',array('pdf','doc','xls','ppt','odt','docx','xlsx','pptx'));
Configure::write('MAX_SIZE_DOCUMENT_UPLOAD',5);//MB

