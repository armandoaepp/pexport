CREATE OR REPLACE FUNCTION sumary.funcion_kpi_lecturas()
  RETURNS integer AS
$BODY$
	/**
	 * Llena la tabla sumary.kpi_lecturas para dashboard
	 * @version 11 Noviembre 2014
	 * @author Geynen Rossler Montenegro Cochas, David Jesus Marchena Pejerrey 
    */ 
declare vcount integer;
declare vcount2 integer;
BEGIN
	SET TIME ZONE 'America/Lima';	
	
	vcount = (SELECT count(*) as vcount FROM sumary.kpi_lecturas);
	IF (vcount > 0) THEN
		vcount2 = (SELECT count(*) as vcount FROM sumary.kpi_lecturas WHERE NOW() > fecha_last_update + interval '60 seconds' );
	END IF;
	
	IF (vcount = 0 OR vcount2 > 0) THEN
	    DROP TABLE IF EXISTS tmp_kpi_lecturas;
		CREATE TEMPORARY TABLE tmp_kpi_lecturas as
	    SELECT 
		glomas_unidadnegocio_id,
		pfactura,
		idciclo,
		sector,
		ruta,
		fecha,
	    (SELECT count(*) AS cantidad
	          FROM lecturas.comlec_ordenlecturas
	          WHERE (((date_trunc('day', fechaasignado1) = t.fecha) OR fechaasignado1 IS NULL) AND  
	          ((comlec_ordenlecturas.ruta)::TEXT = (t.ruta)::TEXT) AND 
	          ((comlec_ordenlecturas.sector)::TEXT = (t.sector)::TEXT) AND 
	          ((comlec_ordenlecturas.idciclo)::TEXT = (t.idciclo)::TEXT) AND 
	          (comlec_ordenlecturas.pfactura = t.pfactura) AND 
	          (comlec_ordenlecturas.glomas_unidadnegocio_id = t.glomas_unidadnegocio_id))) AS count_suministros,
	    (SELECT count(*) AS cantidad
	          FROM lecturas.comlec_ordenlecturas
	          WHERE ((comlec_ordenlecturas.lecturista1_id IS NOT NULL) AND
	          (date_trunc('day', fechaasignado1) = t.fecha) AND 
	          ((comlec_ordenlecturas.ruta)::TEXT = (t.ruta)::TEXT) AND 
	          ((comlec_ordenlecturas.sector)::TEXT = (t.sector)::TEXT) AND 
	          ((comlec_ordenlecturas.idciclo)::TEXT = (t.idciclo)::TEXT) AND 
	          (comlec_ordenlecturas.pfactura = t.pfactura) AND 
	          (comlec_ordenlecturas.glomas_unidadnegocio_id = t.glomas_unidadnegocio_id))) AS count_asignados,
	    (SELECT count(*) AS cantidad
	          FROM lecturas.comlec_ordenlecturas
	          WHERE ((comlec_ordenlecturas.lecturista1_id IS NOT NULL) AND 
	          ((comlec_ordenlecturas.down1)::TEXT = '1'::TEXT) AND 
	          (date_trunc('day', fechaasignado1) = t.fecha) AND 
	          ((comlec_ordenlecturas.ruta)::TEXT = (t.ruta)::TEXT) AND 
	          ((comlec_ordenlecturas.sector)::TEXT = (t.sector)::TEXT) AND 
	          ((comlec_ordenlecturas.idciclo)::TEXT = (t.idciclo)::TEXT) AND 
	          (comlec_ordenlecturas.pfactura = t.pfactura) AND 
	          (comlec_ordenlecturas.glomas_unidadnegocio_id = t.glomas_unidadnegocio_id))) AS count_descargados,
	    (SELECT count(*) AS cantidad
	          FROM lecturas.comlec_ordenlecturas
	          WHERE ((((comlec_ordenlecturas.resultadoevaluacion)::TEXT = 'CONSISTENTE'::TEXT) OR 
	          ((comlec_ordenlecturas.resultadoevaluacion)::TEXT = 'INCONSISTENTE'::TEXT)) AND 
	          (date_trunc('day', fechaasignado1) = t.fecha) AND 
	          ((comlec_ordenlecturas.ruta)::TEXT = (t.ruta)::TEXT) AND 
	          ((comlec_ordenlecturas.sector)::TEXT = (t.sector)::TEXT) AND 
	          ((comlec_ordenlecturas.idciclo)::TEXT = (t.idciclo)::TEXT) AND 
	          (comlec_ordenlecturas.pfactura = t.pfactura) AND 
	          (comlec_ordenlecturas.glomas_unidadnegocio_id = t.glomas_unidadnegocio_id))) AS count_finalizados,
	    (0) AS count_pendientes,
	    (SELECT count(*) AS cantidad
	          FROM lecturas.comlec_ordenlecturas
	          WHERE (((comlec_ordenlecturas.resultadoevaluacion)::TEXT = 'INCONSISTENTE'::TEXT) AND 
	          (date_trunc('day', fechaasignado1) = t.fecha) AND 
	          ((comlec_ordenlecturas.ruta)::TEXT = (t.ruta)::TEXT) AND 
	          ((comlec_ordenlecturas.sector)::TEXT = (t.sector)::TEXT) AND 
	          ((comlec_ordenlecturas.idciclo)::TEXT = (t.idciclo)::TEXT) AND 
	          (comlec_ordenlecturas.pfactura = t.pfactura) AND 
	          (comlec_ordenlecturas.glomas_unidadnegocio_id = t.glomas_unidadnegocio_id))) AS count_inconsistentes_actuales, 
	    (SELECT count(*) AS cantidad
	          FROM lecturas.comlec_ordenlecturas
	          WHERE ((comlec_ordenlecturas.validacion = '1'::bpchar) AND 
	          (date_trunc('day', fechaasignado1) = t.fecha) AND 
	          ((comlec_ordenlecturas.ruta)::TEXT = (t.ruta)::TEXT) AND 
	          ((comlec_ordenlecturas.sector)::TEXT = (t.sector)::TEXT) AND 
	          ((comlec_ordenlecturas.idciclo)::TEXT = (t.idciclo)::TEXT) AND 
	          (comlec_ordenlecturas.pfactura = t.pfactura) AND 
	          (comlec_ordenlecturas.glomas_unidadnegocio_id = t.glomas_unidadnegocio_id))) AS count_inconsistentes_evaluadas,
	    (0) AS count_consistentes,
	    (0) AS count_observaciones,
	    (SELECT count(comlec_ordenlecturas.obs1) AS cantidad
	          FROM lecturas.comlec_ordenlecturas
	          WHERE ((comlec_ordenlecturas.obs1 IS NOT NULL) AND 
	          ((comlec_ordenlecturas.obs1)::TEXT <> ''::TEXT) AND 
	          ((comlec_ordenlecturas.obs1)::TEXT <> 'NULL'::TEXT) AND 
	          ((comlec_ordenlecturas.obs1)::TEXT <> '0'::TEXT) AND 
	          ((comlec_ordenlecturas.lecturao1)::TEXT <> ''::TEXT) AND 
	          (date_trunc('day', fechaasignado1) = t.fecha) AND 
	          ((comlec_ordenlecturas.ruta)::TEXT = (t.ruta)::TEXT) AND 
	          ((comlec_ordenlecturas.sector)::TEXT = (t.sector)::TEXT) AND 
	          ((comlec_ordenlecturas.idciclo)::TEXT = (t.idciclo)::TEXT) AND 
	          (comlec_ordenlecturas.pfactura = t.pfactura) AND 
	          (comlec_ordenlecturas.glomas_unidadnegocio_id = t.glomas_unidadnegocio_id))) AS count_observaciones_con_lectura,
	    (SELECT count(comlec_ordenlecturas.obs1) AS cantidad
	          FROM lecturas.comlec_ordenlecturas
	          WHERE ((comlec_ordenlecturas.obs1 IS NOT NULL) AND 
	          ((comlec_ordenlecturas.obs1)::TEXT <> ''::TEXT) AND 
	          ((comlec_ordenlecturas.obs1)::TEXT <> 'NULL'::TEXT) AND 
	          ((comlec_ordenlecturas.obs1)::TEXT <> '0'::TEXT) AND 
	          (((comlec_ordenlecturas.lecturao1)::TEXT = ''::TEXT) OR 
	          (comlec_ordenlecturas.lecturao1 IS NULL)) AND 
	          (date_trunc('day', fechaasignado1) = t.fecha) AND 
	          ((comlec_ordenlecturas.ruta)::TEXT = (t.ruta)::TEXT) AND 
	          ((comlec_ordenlecturas.sector)::TEXT = (t.sector)::TEXT) AND 
	          ((comlec_ordenlecturas.idciclo)::TEXT = (t.idciclo)::TEXT) AND 
	          (comlec_ordenlecturas.pfactura = t.pfactura) AND 
	          (comlec_ordenlecturas.glomas_unidadnegocio_id = t.glomas_unidadnegocio_id))) AS count_observaciones_sin_lectura,
	    (SELECT count(comlec_ordenlecturas.obs1) AS cantidad
	          FROM lecturas.comlec_ordenlecturas
	          WHERE ((comlec_ordenlecturas.obs1 = '99') AND 
	          (date_trunc('day', fechaasignado1) = t.fecha) AND 
	          ((comlec_ordenlecturas.ruta)::TEXT = (t.ruta)::TEXT) AND 
	          ((comlec_ordenlecturas.sector)::TEXT = (t.sector)::TEXT) AND 
	          ((comlec_ordenlecturas.idciclo)::TEXT = (t.idciclo)::TEXT) AND 
	          (comlec_ordenlecturas.pfactura = t.pfactura) AND 
	          (comlec_ordenlecturas.glomas_unidadnegocio_id = t.glomas_unidadnegocio_id))) AS count_observaciones_99,
	    (SELECT count(*) AS cantidad
	          FROM lecturas.comlec_ordenlecturas
	          WHERE ((comlec_ordenlecturas.montoconsumo1 = (0)::NUMERIC) AND 
	          (date_trunc('day', fechaasignado1) = t.fecha) AND 
	          ((comlec_ordenlecturas.ruta)::TEXT = (t.ruta)::TEXT) AND 
	          ((comlec_ordenlecturas.sector)::TEXT = (t.sector)::TEXT) AND 
	          ((comlec_ordenlecturas.idciclo)::TEXT = (t.idciclo)::TEXT) AND 
	          (comlec_ordenlecturas.pfactura = t.pfactura) AND 
	          (comlec_ordenlecturas.glomas_unidadnegocio_id = t.glomas_unidadnegocio_id))) AS count_consumo_cero,
	    (SELECT count(*) AS cantidad
	          FROM lecturas.comlec_ordenlecturas
	          WHERE ((comlec_ordenlecturas.montoconsumo1 < (35)::NUMERIC) AND 
	          (date_trunc('day', fechaasignado1) = t.fecha) AND 
	          ((comlec_ordenlecturas.ruta)::TEXT = (t.ruta)::TEXT) AND 
	          ((comlec_ordenlecturas.sector)::TEXT = (t.sector)::TEXT) AND 
	          ((comlec_ordenlecturas.idciclo)::TEXT = (t.idciclo)::TEXT) AND 
	          (comlec_ordenlecturas.pfactura = t.pfactura) AND 
	          (comlec_ordenlecturas.glomas_unidadnegocio_id = t.glomas_unidadnegocio_id))) AS count_consumo_menor_35,
	    (SELECT count(*) AS cantidad
	          FROM lecturas.comlec_ordenlecturas
	          WHERE ((comlec_ordenlecturas.montoconsumo1 > (1000)::NUMERIC) AND 
	          (date_trunc('day', fechaasignado1) = t.fecha) AND 
	          ((comlec_ordenlecturas.ruta)::TEXT = (t.ruta)::TEXT) AND 
	          ((comlec_ordenlecturas.sector)::TEXT = (t.sector)::TEXT) AND 
	          ((comlec_ordenlecturas.idciclo)::TEXT = (t.idciclo)::TEXT) AND 
	          (comlec_ordenlecturas.pfactura = t.pfactura) AND 
	          (comlec_ordenlecturas.glomas_unidadnegocio_id = t.glomas_unidadnegocio_id))) AS count_consumo_mayor_1000,
	    (SELECT count(*) AS cantidad
	          FROM lecturas.comlec_ordenlecturas
	          WHERE ((comlec_ordenlecturas.montoconsumo1 > (comlec_ordenlecturas.consant * (2)::NUMERIC)) AND 
	          (date_trunc('day', fechaasignado1) = t.fecha) AND 
	          ((comlec_ordenlecturas.ruta)::TEXT = (t.ruta)::TEXT) AND 
	          ((comlec_ordenlecturas.sector)::TEXT = (t.sector)::TEXT) AND 
	          ((comlec_ordenlecturas.idciclo)::TEXT = (t.idciclo)::TEXT) AND 
	          (comlec_ordenlecturas.pfactura = t.pfactura) AND 
	          (comlec_ordenlecturas.glomas_unidadnegocio_id = t.glomas_unidadnegocio_id))) AS count_consumo_mayor_100_porciento,
	    (SELECT avg(tiempo) AS cantidad FROM (
	    SELECT *, age(F1, F2) AS tiempo FROM (
	    (SELECT *, ROW_NUMBER() OVER() AS I1 FROM (SELECT fechaejecucion1 AS F1 FROM lecturas.comlec_ordenlecturas WHERE fechaejecucion1 IS NOT NULL AND 
	    	  (date_trunc('day', fechaasignado1) = t.fecha) AND 
	          ((comlec_ordenlecturas.ruta)::TEXT = (t.ruta)::TEXT) AND 
	          ((comlec_ordenlecturas.sector)::TEXT = (t.sector)::TEXT) AND 
	          ((comlec_ordenlecturas.idciclo)::TEXT = (t.idciclo)::TEXT) AND 
	          (comlec_ordenlecturas.pfactura = t.pfactura) AND 
	          (comlec_ordenlecturas.glomas_unidadnegocio_id = t.glomas_unidadnegocio_id) ORDER BY fechaejecucion1 ASC) AS t1) AS t11
	    LEFT OUTER JOIN
	    (SELECT *, ROW_NUMBER() OVER() AS I2 FROM (SELECT fechaejecucion1 AS F2 FROM lecturas.comlec_ordenlecturas WHERE fechaejecucion1 IS NOT NULL AND 
	    	  (date_trunc('day', fechaasignado1) = t.fecha) AND 
	          ((comlec_ordenlecturas.ruta)::TEXT = (t.ruta)::TEXT) AND 
	          ((comlec_ordenlecturas.sector)::TEXT = (t.sector)::TEXT) AND 
	          ((comlec_ordenlecturas.idciclo)::TEXT = (t.idciclo)::TEXT) AND 
	          (comlec_ordenlecturas.pfactura = t.pfactura) AND 
	          (comlec_ordenlecturas.glomas_unidadnegocio_id = t.glomas_unidadnegocio_id) ORDER BY fechaejecucion1 ASC) AS t2) AS t22
	    ON t11.I1=t22.I2+1) AS t3
	    ) AS t4 ) AS tiempo_promedio_ejecucion,
	    ( SELECT SUM(montoconsumo1) AS montoconsumo 
	           FROM lecturas.comlec_ordenlecturas
	          WHERE (
	          (date_trunc('day', fechaasignado1) = t.fecha) AND 
	          ((comlec_ordenlecturas.ruta)::TEXT = (t.ruta)::TEXT) AND 
	          ((comlec_ordenlecturas.sector)::TEXT = (t.sector)::TEXT) AND 
	          ((comlec_ordenlecturas.idciclo)::TEXT = (t.idciclo)::TEXT) AND 
	          (comlec_ordenlecturas.pfactura = t.pfactura) AND 
	          (comlec_ordenlecturas.glomas_unidadnegocio_id = t.glomas_unidadnegocio_id))) AS sum_montoconsumo,
	   NOW() AS fecha_last_update,
	   (SELECT count(*) AS cantidad
	          FROM lecturas.comlec_ordenlecturas
	          WHERE (ws_l_estado='1' AND 
	          (date_trunc('day', fechaasignado1) = t.fecha) AND 
	          ((comlec_ordenlecturas.ruta)::TEXT = (t.ruta)::TEXT) AND 
	          ((comlec_ordenlecturas.sector)::TEXT = (t.sector)::TEXT) AND 
	          ((comlec_ordenlecturas.idciclo)::TEXT = (t.idciclo)::TEXT) AND 
	          (comlec_ordenlecturas.pfactura = t.pfactura) AND 
	          (comlec_ordenlecturas.glomas_unidadnegocio_id = t.glomas_unidadnegocio_id))) AS count_enviados_distriluz
	   FROM (SELECT DISTINCT glomas_unidadnegocio_id,pfactura,idciclo,sector,ruta,date_trunc('day', fechaasignado1) AS fecha FROM lecturas.comlec_ordenlecturas WHERE (1 = 1) ) t;
	   
	   TRUNCATE sumary.kpi_lecturas;
	   INSERT INTO sumary.kpi_lecturas select * from tmp_kpi_lecturas;
	   UPDATE sumary.kpi_lecturas set count_pendientes = count_suministros - count_finalizados, count_consistentes = count_finalizados - count_inconsistentes_actuales, count_observaciones = count_observaciones_con_lectura + count_observaciones_sin_lectura;
   
   END IF;

RETURN 1;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;

ALTER FUNCTION "sumary"."funcion_kpi_lecturas"() OWNER TO "ddanalytics";




----------------------------------------------------------------------------------

-- Function: lecturas.insertar_lecturao_inconsistencia(numeric, character varying, character varying, integer, integer, character varying)

-- DROP FUNCTION lecturas.insertar_lecturao_inconsistencia(numeric, character varying, character varying, integer, integer, character varying);

CREATE OR REPLACE FUNCTION lecturas.insertar_lecturao_inconsistencia(var_lecturao numeric, var_suministro character varying, tipolectura character varying, codigousuariologeado integer, codigoobservacion integer, disporigen character varying)
  RETURNS integer AS
$BODY$
DECLARE

declare var_metodo varchar;
declare var_consumo numeric;
declare var_inconsistencia varchar;
declare esconsistente integer;
declare vcursor refcursor;
declare varcursor_lecturao numeric;
declare var_select varchar;

BEGIN

SET TIME ZONE 'America/Lima';
if (tipolectura='L') then

	if (var_lecturao is null)then 

		if (codigoobservacion is null) then 

		    RETURN 1;
		    
		else

		   update lecturas.comlec_ordenlecturas set 
				lecturaoold=( select lecturao1 from lecturas.comlec_ordenlecturas where  trim(suministro)=cast(var_suministro as varchar) ),
				lecturao1=null,
				resultadoevaluacion='INCONSISTENTE',
				metodofinal1=null, 
				montoconsumo1=null, 
				usuario_id=codigousuariologeado, 
				up1='2', 
				fechaejecucion1=now(), 
				obs1=cast(codigoobservacion as varchar) 
		   where trim(suministro)=cast(var_suministro as varchar);
		
		end if;

	else

		if (codigoobservacion is null) then 
			update lecturas.comlec_ordenlecturas set 
				lecturao1=var_lecturao , 
				lecturaoold=(select lecturao1 from lecturas.comlec_ordenlecturas  where suministro=var_suministro ) ,
				obs1=( select case when ob.restriccion_inconsistencia=1 then obs1 else null end
					from lecturas.comlec_ordenlecturas ol inner join lecturas.comlec_observaciones ob on ol.obs1=cast(ob.codigocamp as varchar) where suministro=cast(var_suministro as varchar) )
								
			   where trim(suministro)=cast(var_suministro as varchar);

			--realizar funcion para calcular si es consistente o inconsitente
			--grabo estado evaluacion, y nuevo montoconsumo,metodofinal, usuario fecha
			
			var_select=(select lecturas.funcion_ejecutarminmax(var_suministro,codigoobservacion));

			--fin funcion es consistente o inconsitente

		else


			update lecturas.comlec_ordenlecturas set 
				lecturao1=var_lecturao , 
				lecturaoold=(select lecturao1 from lecturas.comlec_ordenlecturas  where suministro=var_suministro ) ,
				obs1=cast(codigoobservacion as varchar )
								
		   where trim(suministro)=cast(var_suministro as varchar);

			--realizar funcion para calcular si es consistente o inconsitente
			--grabo estado evaluacion, y nuevo montoconsumo,metodofinal, usuario fecha

			
			var_select=(select lecturas.funcion_ejecutarminmax(var_suministro,codigoobservacion));

			--fin funcion es consistente o inconsitente

		end if;
	


   

	end if;


end if;






if (tipolectura='R') then

	if (var_lecturao is null)then 

		if (codigoobservacion is null) then 

		    RETURN 1;
		    
		else
		   update lecturas.comlec_ordenlecturas set 
				lecturaoold=( select lecturao2 from lecturas.comlec_ordenlecturas where  trim(suministro)=cast(var_suministro as varchar) ),
				lecturao2=null,
				resultadoevaluacion='INCONSISTENTE',
				metodofinal2=null, 
				montoconsumo2=null, 
				usuario_id=codigousuariologeado, 
				up2='2', 
				fechaejecucion2=now(), 
				obs2=cast(codigoobservacion as varchar) 
			where trim(suministro)=cast(var_suministro as varchar);
		end if;

	else

		if (codigoobservacion is null) then 
			update lecturas.comlec_ordenlecturas set 
				lecturao2=var_lecturao , 

			--lecturaoold=(select lecturao2 from lecturas.comlec_ordenlecturas  where suministro=var_suministro ) ,
				
				obs2=( select case when restriccion_inconsistencia=1 then obs2 else null end
					from lecturas.comlec_ordenlecturas ol inner join lecturas.comlec_observaciones ob on ol.obs2=cast(ob.codigocamp as varchar) where  suministro=cast(var_suministro as varchar) )
								
		   where trim(suministro)=cast(var_suministro as varchar);

			--realizar funcion para calcular si es consistente o inconsitente
			--grabo estado evaluacion, y nuevo montoconsumo,metodofinal, usuario fecha
			
			var_select=(select lecturas.funcion_ejecutarminmax_r(var_suministro,codigoobservacion));

			--fin funcion es consistente o inconsitente

		else


			update lecturas.comlec_ordenlecturas set 
				lecturao2=var_lecturao , 
				lecturaoold=(select lecturao2 from lecturas.comlec_ordenlecturas  where suministro=var_suministro ) ,
				obs2=cast(codigoobservacion as varchar )
								
			   where trim(suministro)=cast(var_suministro as varchar);

			--realizar funcion para calcular si es consistente o inconsitente
			--grabo estado evaluacion, y nuevo montoconsumo,metodofinal, usuario fecha

			
			var_select=(select lecturas.funcion_ejecutarminmax_r(var_suministro,codigoobservacion));

			--fin funcion es consistente o inconsitente

		end if;
	


   

	end if;


end if;

    RETURN 1;

END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION lecturas.insertar_lecturao_inconsistencia(numeric, character varying, character varying, integer, integer, character varying)
  OWNER TO ddanalytics;



--------------------------------

-- Function: lecturas.limpiar()

-- DROP FUNCTION lecturas.limpiar();

CREATE OR REPLACE FUNCTION lecturas.limpiar()
  RETURNS integer AS
$BODY$
DECLARE


BEGIN

delete from auditoria.comlec_ordenlecturas;
delete from lecturas.comlec_ordenlecturas;
delete from lecturas.comlec_ordenlecturas1;
delete from lecturas.comlec_clientes;
delete from lecturas.comlec_medidores;
delete from lecturas.comlec_lecturametodo;
--delete from lecturas.comlec_suministros;
delete from lecturas.comlec_xml;
delete from sumary.kpi_lecturas;


   RETURN 1;
END;

$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION lecturas.limpiar()
  OWNER TO ddanalytics;
  
  
  
  
  ---------------------------------------------------
  
  CREATE OR REPLACE FUNCTION lecturas.cursor_casiano(id_grupo integer, id_xml integer)
  RETURNS integer AS
$BODY$
DECLARE

idrango INTEGER; 
formula_maxima VARCHAR; 
formula_minima VARCHAR; 
declare vcursor_metodostrue CURSOR FOR select id from lecturas.comlec_metodos where estado_id=1  ;
declare id_ot integer;
declare var_suministro varchar;
declare var_pfactura varchar;
declare prom numeric;
declare metodo_id integer;
declare vcursor refcursor;
declare vcursor_orden refcursor;
declare existe_suministro integer;
declare existe_cliente integer;
declare existe_medidor integer;
declare id_suministro integer;
declare id_unidadnegocio integer;
declare id_unidadnegocio_orden varchar;
declare id_sector_orden varchar;
declare id_ciclo_orden varchar;
declare v_sector varchar;
declare v_ruta varchar;

BEGIN
SET TIME ZONE 'America/Lima';

    OPEN vcursor FOR EXECUTE 'select id,suministro,case when promedio <0 then 0 else promedio end ,pfactura,unidad_neg,sector,ruta from lecturas.comlec_ordenlecturas' || id_grupo || ' where trim(concepto)=''1'' and comlec_xml_id='||id_xml||'  ';
    loop
    
        FETCH vcursor INTO id_ot,var_suministro,prom,var_pfactura,id_unidadnegocio,v_sector,v_ruta;
        EXIT WHEN NOT FOUND;
            ------------------inicio algotimo comlec_lecturametodo -------------------
            
	   delete from lecturas.comlec_ordenlecturas where comlec_xml_id=id_xml  and comlec_ordenlectura_id= id_unidadnegocio || '-' || var_pfactura || '-'  || id_ot;
    
           delete from lecturas.comlec_lecturametodo where comlec_ordenlectura_id=id_unidadnegocio || '-' || var_pfactura || '-'  || id_ot;
           idrango= (select id from lecturas.comlec_rangos where round(prom)>=r1 and round(prom)<=r2);
	       OPEN vcursor_metodostrue;
		loop
    		FETCH vcursor_metodostrue INTO metodo_id;
		EXIT WHEN NOT FOUND;

		  formula_maxima=(select case when (  select trim(formula_max) from lecturas.comlecuni_metodorango where comlec_rango_id=idrango and comlec_metodo_id= metodo_id) is null then '0'  else (  select trim(formula_max) from lecturas.comlecuni_metodorango where comlec_rango_id=idrango and comlec_metodo_id= metodo_id) end) ;
		  formula_minima=(select case when (  select trim(formula_min) from lecturas.comlecuni_metodorango where comlec_rango_id=idrango and comlec_metodo_id= metodo_id) is null then '0'  else (  select trim(formula_min) from lecturas.comlecuni_metodorango where comlec_rango_id=idrango and comlec_metodo_id= metodo_id) end) ;

		  execute  'insert into lecturas.comlec_lecturametodo (comlec_ordenlectura_id,maxima,minima,comlec_rango_id,comlec_metodo_id,glomas_grupo_id,glomas_unidadnegocio_id) 
		            select   unidad_neg || ''-'' || pfactura || ''-''  || id, ' || formula_maxima || ', ' || formula_minima || ', ' || idrango || ' , ' || metodo_id || ' , ' || id_grupo || ', unidad_neg  from lecturas.comlec_ordenlecturas'|| id_grupo ||' where trim(concepto)=''1'' and comlec_xml_id='||id_xml||' and id='||id_ot;

                  end loop;
		CLOSE vcursor_metodostrue;
		
           ------------------fin algotimo comlec_lecturametodo -------------------


	------------------inicio algotimo comlec_ordenlecturas -------------------


		execute  'insert into lecturas.comlec_ordenlecturas(nombciclo,pinicio,consant,ordtrabajo,direccion,seriefab,lcorrelati,lecaant,nombruta,idciclo,consret,factor,pfactura,suministro,comlec_ordenlectura_id,tipolectura,lecant,digitos,cliente,glomas_grupo_id,glomas_unidadnegocio_id,eliminado,comlec_xml_id , promedio,sector,ruta )
		        select nombciclo, pinicio,cast(consant as numeric),trim(ordtrabajo), trim(direccion),seriefab,lcorrelati,lecaant,nombruta, idciclo,cast(consret as numeric), cast(factor as numeric), pfactura, suministro, unidad_neg || ''-'' || pfactura || ''-''  || id,  ''L'',   lecant, digitos,nombre, '|| id_grupo ||',unidad_neg ,false ,'||id_xml||' , case when promedio <0 then 0 else promedio end ,sector,ruta from lecturas.comlec_ordenlecturas'|| id_grupo ||' where trim(concepto)=''1'' and comlec_xml_id='||id_xml||' and id='||id_ot;

	
        ------------------fin algotimo existe  comlec_ordenlecturas -------------------

	------------------inicio algotimo existe suministro -------------------
	/*
	existe_suministro=(select count(*) from lecturas.comlec_suministros where glomas_unidadnegocio_id=id_unidadnegocio and  suministro=var_suministro);

	if (existe_suministro=0) then
		execute  'insert into lecturas.comlec_suministros(suministro,direccion,glomas_unidadnegocio_id,eliminado)
		        select  suministro,direccion,unidad_neg,false  from lecturas.comlec_ordenlecturas'|| id_grupo ||' where trim(concepto)=''1'' and comlec_xml_id='||id_xml||' and id='||id_ot;


	end if;
	*/
        ------------------fin algotimo existe suministro -------------------

        ------------------inicio algotimo existe cliente -------------------
	/*
        id_suministro=(select case when id is null then 0 else id end from lecturas.comlec_suministros where glomas_unidadnegocio_id=id_unidadnegocio and suministro= var_suministro );
	existe_cliente=(select count(*) from lecturas.comlec_clientes where glomas_unidadnegocio_id=id_unidadnegocio and  comlec_suministro_id=id_suministro);

	if (existe_cliente=0) then
		execute  'insert into lecturas.comlec_clientes (nombre,comlec_suministro_id,glomas_unidadnegocio_id,eliminado)
		        select  nombre,'|| id_suministro ||',unidad_neg,false  from lecturas.comlec_ordenlecturas'|| id_grupo ||' where trim(concepto)=''1'' and comlec_xml_id='||id_xml||' and id='||id_ot;


	end if;
	*/
        ------------------fin algotimo existe cliente -------------------


	------------------inicio algotimo existe medidores -------------------

       -- id_suministro=(select case when id is null then 0 else id end from lecturas.comlec_suministros where suministro= var_suministro);
	/*existe_medidor=(select count(*) from lecturas.comlec_medidores where glomas_unidadnegocio_id=id_unidadnegocio and  comlec_suministro_id=id_suministro);

	if (existe_medidor=0) then
		execute  'insert into lecturas.comlec_medidores (seriefab,marca,modelo,digitos,comlec_suministro_id,glomas_unidadnegocio_id,eliminado)
		        select  seriefab,marca,modelo,digitos,'|| id_suministro ||',unidad_neg,false  from lecturas.comlec_ordenlecturas'|| id_grupo ||' where trim(concepto)=''1'' and comlec_xml_id='||id_xml||' and id='||id_ot;

	end if;
	*/
        ------------------fin algotimo existe medidores -------------------
        
    end loop;
    CLOSE vcursor;


	---------------------------inicio algoritmo orden ----------------------------

    OPEN vcursor_orden FOR EXECUTE 'select  glomas_unidadnegocio_id , idciclo,sector from lecturas.comlec_ordenlecturas where sector in (select distinct sector from lecturas.comlec_ordenlecturas where comlec_xml_id='||id_xml||') group by glomas_unidadnegocio_id,idciclo, sector ';
    loop
    
        FETCH vcursor_orden INTO id_unidadnegocio_orden,id_ciclo_orden,id_sector_orden;
        EXIT WHEN NOT FOUND;


		UPDATE lecturas.comlec_ordenlecturas
		SET orden = tab.rownum,ordenruta = tab.rowordenruta
		FROM 
		(
		(select comlec_ordenlectura_id,unidad_neg,id, pfactura,idciclo,sector, ruta,rownum,rowordenruta from (
		select comlec_ordenlectura_id,glomas_unidadnegocio_id as unidad_neg,id, pfactura,idciclo,sector, ruta ,suministro,
		RANK() OVER (PARTITION BY sector order by ruta,lcorrelati,suministro asc ) AS rownum ,
		RANK() OVER (PARTITION BY ruta order by lcorrelati,suministro asc ) AS rowordenruta
		from lecturas.comlec_ordenlecturas 
		 where idciclo=id_ciclo_orden and sector=id_sector_orden
		 order by RANK() OVER (PARTITION BY sector order by ruta,lcorrelati,suministro asc )
		) tabla 
		order by rownum 
		) ) tab where 
		tab.comlec_ordenlectura_id = lecturas.comlec_ordenlecturas.comlec_ordenlectura_id ;


	end loop;
CLOSE vcursor_orden;
	---------------------------fin algoritmo orden -------------------------------


	update lecturas.comlec_xml set numordenesvalidadas=(select count(suministro) from lecturas.comlec_ordenlecturas where comlec_xml_id=id_xml) where id=id_xml;

	-- Obteniendo datos de resumen para combos (Geynen)
	perform lecturas.function_populate_pfactura();

	perform lecturas.function_populate_glomas_ciclo();

	perform lecturas.function_populate_glomas_sector();

	perform lecturas.function_populate_glomas_ruta();
    
    RETURN 1;





END;

$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION lecturas.cursor_casiano(integer, integer)
  OWNER TO ddanalytics;
  
  

  
-------------------------------------------------------------------------
  
  
  CREATE OR REPLACE FUNCTION lecturas.cursor_prepare_xml_for_lectura(id_grupo integer, id_xml integer)
  RETURNS integer AS
$BODY$
DECLARE

idrango INTEGER; 
formula_maxima VARCHAR; 
formula_minima VARCHAR; 
declare vcursor_metodostrue CURSOR FOR select id, formula_evaluar from lecturas.comlec_metodos where estado_id=1  ;
declare id_ot integer;
declare var_suministro varchar;
declare var_pfactura varchar;
declare prom numeric;
declare metodo_id integer;
declare metodo_formula_evaluar varchar;
declare vcursor refcursor;
declare vcursor_orden refcursor;
declare existe_suministro integer;
declare existe_cliente integer;
declare existe_medidor integer;
declare id_suministro integer;
declare id_unidadnegocio integer;
declare id_unidadnegocio_orden varchar;
declare id_sector_orden varchar;
declare id_ciclo_orden varchar;
declare v_sector varchar;
declare v_ruta varchar;

BEGIN
SET TIME ZONE 'America/Lima';

    OPEN vcursor FOR EXECUTE 'select id,suministro,case when promedio <0 then 0 else promedio end ,pfactura,unidad_neg,sector,ruta from lecturas.comlec_ordenlecturas' || id_grupo || ' where trim(concepto)=''1'' and comlec_xml_id='||id_xml||'  ';
    loop
    
        FETCH vcursor INTO id_ot,var_suministro,prom,var_pfactura,id_unidadnegocio,v_sector,v_ruta;
        EXIT WHEN NOT FOUND;
            ------------------inicio algotimo comlec_lecturametodo -------------------
            
	   delete from lecturas.comlec_ordenlecturas where comlec_xml_id=id_xml  and comlec_ordenlectura_id= id_unidadnegocio || '-' || var_pfactura || '-'  || id_ot;
    
           delete from lecturas.comlec_lecturametodo where comlec_ordenlectura_id=id_unidadnegocio || '-' || var_pfactura || '-'  || id_ot;
	       OPEN vcursor_metodostrue;
		loop
    		FETCH vcursor_metodostrue INTO metodo_id, metodo_formula_evaluar;
		EXIT WHEN NOT FOUND;

		  execute 'select id from lecturas.comlec_rangos where round(' || metodo_formula_evaluar || ')>=r1 and round(' || metodo_formula_evaluar || ')<=r2' into idrango;

		  formula_maxima=(select case when (  select trim(formula_max) from lecturas.comlecuni_metodorango where comlec_rango_id=idrango and comlec_metodo_id= metodo_id) is null then '0'  else (  select trim(formula_max) from lecturas.comlecuni_metodorango where comlec_rango_id=idrango and comlec_metodo_id= metodo_id) end) ;
		  formula_minima=(select case when (  select trim(formula_min) from lecturas.comlecuni_metodorango where comlec_rango_id=idrango and comlec_metodo_id= metodo_id) is null then '0'  else (  select trim(formula_min) from lecturas.comlecuni_metodorango where comlec_rango_id=idrango and comlec_metodo_id= metodo_id) end) ;

		  execute  'insert into lecturas.comlec_lecturametodo (comlec_ordenlectura_id,maxima,minima,comlec_rango_id,comlec_metodo_id,glomas_grupo_id,glomas_unidadnegocio_id) 
		            select   unidad_neg || ''-'' || pfactura || ''-''  || id, ' || formula_maxima || ', ' || formula_minima || ', ' || idrango || ' , ' || metodo_id || ' , ' || id_grupo || ', unidad_neg  from lecturas.comlec_ordenlecturas'|| id_grupo ||' where trim(concepto)=''1'' and comlec_xml_id='||id_xml||' and id='||id_ot;

                  end loop;
		CLOSE vcursor_metodostrue;
		
           ------------------fin algotimo comlec_lecturametodo -------------------


	------------------inicio algotimo comlec_ordenlecturas -------------------


		execute  'insert into lecturas.comlec_ordenlecturas(nombciclo,pinicio,consant,ordtrabajo,direccion,seriefab,lcorrelati,lecaant,nombruta,idciclo,consret,factor,pfactura,suministro,comlec_ordenlectura_id,tipolectura,lecant,digitos,cliente,glomas_grupo_id,glomas_unidadnegocio_id,eliminado,comlec_xml_id , promedio,sector,ruta )
		        select nombciclo, pinicio,cast(consant as numeric),trim(ordtrabajo), trim(direccion),seriefab,lcorrelati,lecaant,nombruta, idciclo,cast(consret as numeric), cast(factor as numeric), pfactura, suministro, unidad_neg || ''-'' || pfactura || ''-''  || id,  ''L'',   lecant, digitos,nombre, '|| id_grupo ||',unidad_neg ,false ,'||id_xml||' , case when promedio <0 then 0 else promedio end ,sector,ruta from lecturas.comlec_ordenlecturas'|| id_grupo ||' where trim(concepto)=''1'' and comlec_xml_id='||id_xml||' and id='||id_ot;

	
        ------------------fin algotimo existe  comlec_ordenlecturas -------------------

	------------------inicio algotimo existe suministro -------------------
	/*
	existe_suministro=(select count(*) from lecturas.comlec_suministros where glomas_unidadnegocio_id=id_unidadnegocio and  suministro=var_suministro);

	if (existe_suministro=0) then
		execute  'insert into lecturas.comlec_suministros(suministro,direccion,glomas_unidadnegocio_id,eliminado)
		        select  suministro,direccion,unidad_neg,false  from lecturas.comlec_ordenlecturas'|| id_grupo ||' where trim(concepto)=''1'' and comlec_xml_id='||id_xml||' and id='||id_ot;


	end if;
	*/
        ------------------fin algotimo existe suministro -------------------

        ------------------inicio algotimo existe cliente -------------------
	/*
        id_suministro=(select case when id is null then 0 else id end from lecturas.comlec_suministros where glomas_unidadnegocio_id=id_unidadnegocio and suministro= var_suministro );
	existe_cliente=(select count(*) from lecturas.comlec_clientes where glomas_unidadnegocio_id=id_unidadnegocio and  comlec_suministro_id=id_suministro);

	if (existe_cliente=0) then
		execute  'insert into lecturas.comlec_clientes (nombre,comlec_suministro_id,glomas_unidadnegocio_id,eliminado)
		        select  nombre,'|| id_suministro ||',unidad_neg,false  from lecturas.comlec_ordenlecturas'|| id_grupo ||' where trim(concepto)=''1'' and comlec_xml_id='||id_xml||' and id='||id_ot;


	end if;
	*/
        ------------------fin algotimo existe cliente -------------------


	------------------inicio algotimo existe medidores -------------------

       -- id_suministro=(select case when id is null then 0 else id end from lecturas.comlec_suministros where suministro= var_suministro);
	/*existe_medidor=(select count(*) from lecturas.comlec_medidores where glomas_unidadnegocio_id=id_unidadnegocio and  comlec_suministro_id=id_suministro);

	if (existe_medidor=0) then
		execute  'insert into lecturas.comlec_medidores (seriefab,marca,modelo,digitos,comlec_suministro_id,glomas_unidadnegocio_id,eliminado)
		        select  seriefab,marca,modelo,digitos,'|| id_suministro ||',unidad_neg,false  from lecturas.comlec_ordenlecturas'|| id_grupo ||' where trim(concepto)=''1'' and comlec_xml_id='||id_xml||' and id='||id_ot;

	end if;
	*/
        ------------------fin algotimo existe medidores -------------------
        
    end loop;
    CLOSE vcursor;


	---------------------------inicio algoritmo orden ----------------------------

    OPEN vcursor_orden FOR EXECUTE 'select  glomas_unidadnegocio_id , idciclo,sector from lecturas.comlec_ordenlecturas where sector in (select distinct sector from lecturas.comlec_ordenlecturas where comlec_xml_id='||id_xml||') group by glomas_unidadnegocio_id,idciclo, sector ';
    loop
    
        FETCH vcursor_orden INTO id_unidadnegocio_orden,id_ciclo_orden,id_sector_orden;
        EXIT WHEN NOT FOUND;


		UPDATE lecturas.comlec_ordenlecturas
		SET orden = tab.rownum,ordenruta = tab.rowordenruta
		FROM 
		(
		(select comlec_ordenlectura_id,unidad_neg,id, pfactura,idciclo,sector, ruta,rownum,rowordenruta from (
		select comlec_ordenlectura_id,glomas_unidadnegocio_id as unidad_neg,id, pfactura,idciclo,sector, ruta ,suministro,
		RANK() OVER (PARTITION BY sector order by ruta,lcorrelati,suministro asc ) AS rownum ,
		RANK() OVER (PARTITION BY ruta order by lcorrelati,suministro asc ) AS rowordenruta
		from lecturas.comlec_ordenlecturas 
		 where idciclo=id_ciclo_orden and sector=id_sector_orden
		 order by RANK() OVER (PARTITION BY sector order by ruta,lcorrelati,suministro asc )
		) tabla 
		order by rownum 
		) ) tab where 
		tab.comlec_ordenlectura_id = lecturas.comlec_ordenlecturas.comlec_ordenlectura_id ;


	end loop;
CLOSE vcursor_orden;
	---------------------------fin algoritmo orden -------------------------------


	update lecturas.comlec_xml set numordenesvalidadas=(select count(suministro) from lecturas.comlec_ordenlecturas where comlec_xml_id=id_xml) where id=id_xml;

	-- Obteniendo datos de resumen para combos (Geynen)
	perform lecturas.function_populate_pfactura();

	perform lecturas.function_populate_glomas_ciclo();

	perform lecturas.function_populate_glomas_sector();

	perform lecturas.function_populate_glomas_ruta();
    
    RETURN 1;





END;

$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
  
------------------------------







CREATE OR REPLACE FUNCTION lecturas.funcion_ejecutarminmax(
    var_suministro character varying,
    codigoobservacion integer)
  RETURNS integer AS
$BODY$
DECLARE

declare var_metodo varchar;
declare var_consumo numeric;
declare var_inconsistencia varchar;
declare numerointentos_total integer;
declare vcursor refcursor;
declare varcursor_lecturao numeric;

BEGIN
SET TIME ZONE 'America/Lima';
numerointentos_total=0;

    OPEN vcursor FOR EXECUTE 'select case when validacionobs=2 then null else cast(a.lecturao as numeric) end as lecturao, comlec_metodo_id,consumo,case when consumo>=minima and consumo<=maxima then ''CONSISTENTE''
				else
				''INCONSISTENTE''
				end
				as inconsistencia
				 from (
				  select comlec_metodo_id,minima,maxima,loext.lecturao1,loext.lecant,
				case when COALESCE (cast(lecturao1 as numeric),''0'')>=lecant then 
							((COALESCE (cast(lecturao1 as numeric),''0'')-lecant)*factor)+consret 

						

					when (COALESCE (cast(lecturao1 as numeric),''0'')+1)>=lecant then 
					(((COALESCE (cast(lecturao1 as numeric),''0'')+1)-lecant)*factor)+consret 
					when (COALESCE (cast(lecturao1 as numeric),''0'')+2)>=lecant then 
					(((COALESCE (cast(lecturao1 as numeric),''0'')+2)-lecant)*factor)+consret 
					else	
					(((10^digitos)-lecant+COALESCE (cast(lecturao1 as numeric),''0''))*factor)+consret 
						end as consumo	,

			case 
				when (COALESCE (cast(lecturao1 as numeric),''0'')-lecant)=-1 then 
					cast(lecant as varchar)
				when (COALESCE (cast(lecturao1 as numeric),''0'')-lecant)=-2  then 
					cast(lecant as varchar)
				when (COALESCE (cast(lecturao1 as numeric),''0'')-lecant)=-3  then 
					cast(lecant as varchar)
				else	
					cast(lecturao1 as varchar)
				end as lecturao,
					cobs.comlec_condicionobs_id as validacionobs
						
						    from lecturas.comlec_ordenlecturas loext 
						    inner join lecturas.comlec_lecturametodo lm 
						    on loext.comlec_ordenlectura_id=lm.comlec_ordenlectura_id
						    left join lecturas.comlec_observaciones cobs on loext.obs1=cobs.codigocamp_varchar
						    where  loext.suministro= cast('''||var_suministro||''' as varchar)) a';
    loop
  

        FETCH vcursor INTO varcursor_lecturao,var_metodo,var_consumo,var_inconsistencia;
        EXIT WHEN NOT FOUND;
            ------------------inicio algotimo comlec_lecturametodo -------------------
        --RETURN 1;
	numerointentos_total=numerointentos_total+1;


        if (var_inconsistencia='CONSISTENTE') then 

		update lecturas.comlec_ordenlecturas set 
			lecturao1=varcursor_lecturao, 
			resultadoevaluacion=cast( var_inconsistencia as varchar),
			metodofinal1=cast(var_metodo as integer),
			montoconsumo1=var_consumo, 
			up1='2', 
			fechaup1=now(), 
			obs1=cast(codigoobservacion as varchar)  ,
			flag_auditoria=true
		where suministro=var_suministro;

		exit;

        else

		if (numerointentos_total=2)then
			update lecturas.comlec_ordenlecturas set 
				lecturao1=varcursor_lecturao, 
				resultadoevaluacion=cast( var_inconsistencia as varchar),
				metodofinal1=cast(var_metodo as integer),
				montoconsumo1=var_consumo, 
				up1='2', 
				fechaup1=now(), 
				obs1=cast(codigoobservacion as varchar)  ,
				flag_auditoria=true
			where suministro=var_suministro;
		else
			update lecturas.comlec_ordenlecturas set 
				lecturao1=varcursor_lecturao, 
				resultadoevaluacion=cast( var_inconsistencia as varchar),
				metodofinal1=cast(var_metodo as integer),
				montoconsumo1=var_consumo, 
				up1='2', 
				fechaup1=now(), 
				obs1=cast(codigoobservacion as varchar)  
			where suministro=var_suministro;
		
		end if ;

        end if;

        
        

        
	

		
        end loop;
	CLOSE vcursor;
		
           ------------------fin algotimo comlec_lecturametodo -------------------


update lecturas.comlec_ordenlecturas set flag_auditoria=false	where suministro=var_suministro;
    RETURN 1;

END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;










---------------------------------------------------









  CREATE OR REPLACE FUNCTION lecturas.funcion_ejecutarminmax_r(
    var_suministro character varying,
    codigoobservacion integer)
  RETURNS integer AS
$BODY$
DECLARE

declare var_metodo varchar;
declare var_consumo numeric;
declare var_inconsistencia varchar;
declare numerointentos_total integer;
declare vcursor refcursor;
declare varcursor_lecturao numeric;

BEGIN
SET TIME ZONE 'America/Lima';
numerointentos_total=0;

    OPEN vcursor FOR EXECUTE 'select case when validacionobs=2 then null else cast(a.lecturao as numeric) end as lecturao, comlec_metodo_id,consumo,case when consumo>=minima and consumo<=maxima then ''CONSISTENTE''
				else
				''INCONSISTENTE''
				end
				as inconsistencia
				 from (
				  select comlec_metodo_id,minima,maxima,loext.lecturao2,loext.lecant,
				case when COALESCE (cast(lecturao2 as numeric),''0'')>=lecant then 
							((COALESCE (cast(lecturao2 as numeric),''0'')-lecant)*factor)+consret 

						

					when (COALESCE (cast(lecturao2 as numeric),''0'')+1)>=lecant then 
					(((COALESCE (cast(lecturao2 as numeric),''0'')+1)-lecant)*factor)+consret 
					when (COALESCE (cast(lecturao2 as numeric),''0'')+2)>=lecant then 
					(((COALESCE (cast(lecturao2 as numeric),''0'')+2)-lecant)*factor)+consret 
					else	
					(((10^digitos)-lecant+COALESCE (cast(lecturao2 as numeric),''0''))*factor)+consret 
						end as consumo	,

			case 
				when (COALESCE (cast(lecturao2 as numeric),''0'')-lecant)=-1 then 
					cast(lecant as varchar)
				when (COALESCE (cast(lecturao2 as numeric),''0'')-lecant)=-2  then 
					cast(lecant as varchar)
				when (COALESCE (cast(lecturao2 as numeric),''0'')-lecant)=-3  then 
					cast(lecant as varchar)
				else	
					cast(lecturao2 as varchar)
				end as lecturao,
					cobs.comlec_condicionobs_id as validacionobs	
						
						    from lecturas.comlec_ordenlecturas loext 
						    inner join lecturas.comlec_lecturametodo lm 
						    on loext.comlec_ordenlectura_id=lm.comlec_ordenlectura_id
						    left join lecturas.comlec_observaciones cobs on loext.obs2=cobs.codigocamp_varchar
						    where  loext.suministro= cast('''||var_suministro||''' as varchar)) a';
    loop
  

        FETCH vcursor INTO varcursor_lecturao,var_metodo,var_consumo,var_inconsistencia;
        EXIT WHEN NOT FOUND;
            ------------------inicio algotimo comlec_lecturametodo -------------------
        --RETURN 1;


	numerointentos_total=numerointentos_total+1;

        if (var_inconsistencia='CONSISTENTE') then 

		update lecturas.comlec_ordenlecturas set 
			lecturao2=varcursor_lecturao, 
			lecturao1=varcursor_lecturao, 
			lecturaoold=(select lecturao1 from lecturas.comlec_ordenlecturas  where suministro=var_suministro ) ,
			resultadoevaluacion=cast( var_inconsistencia as varchar),
			metodofinal2=cast(var_metodo as integer),
			montoconsumo2=var_consumo, 
			montoconsumo1=var_consumo, 
			up2='2', 
			fechaup2=now(), 
			obs2=cast(codigoobservacion as varchar)  ,
			flag_auditoria=true
		where suministro=var_suministro;

		exit;

        else

		if (numerointentos_total=2)then
			update lecturas.comlec_ordenlecturas set 
				lecturao2=varcursor_lecturao, 
				resultadoevaluacion=cast( var_inconsistencia as varchar),
				metodofinal2=cast(var_metodo as integer),
				montoconsumo2=var_consumo, 
				up2='2', 
				fechaup2=now(), 
				obs2=cast(codigoobservacion as varchar)  ,
				flag_auditoria=true
			where suministro=var_suministro;
		else
			update lecturas.comlec_ordenlecturas set 
				lecturao2=varcursor_lecturao, 
				resultadoevaluacion=cast( var_inconsistencia as varchar),
				metodofinal2=cast(var_metodo as integer),
				montoconsumo2=var_consumo, 
				up2='2', 
				fechaup2=now(), 
				obs2=cast(codigoobservacion as varchar)  
			where suministro=var_suministro;
		
		end if ;

        end if;

        
        

        
	

		
        end loop;
	CLOSE vcursor;
		
           ------------------fin algotimo comlec_lecturametodo -------------------

update lecturas.comlec_ordenlecturas set flag_auditoria=false	where suministro=var_suministro;

    RETURN 1;

END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;





---------------------------------------------------






  CREATE OR REPLACE FUNCTION lecturas.funcion_ejecutarminmax_rr(
    var_suministro character varying,
    codigoobservacion integer)
  RETURNS integer AS
$BODY$
DECLARE

declare var_metodo varchar;
declare var_consumo numeric;
declare var_inconsistencia varchar;
declare numerointentos_total integer;
declare vcursor refcursor;
declare varcursor_lecturao numeric;

BEGIN
SET TIME ZONE 'America/Lima';
numerointentos_total=0;
    OPEN vcursor FOR EXECUTE 'select case when validacionobs=2 then null else cast(a.lecturao as numeric) end as lecturao, comlec_metodo_id,consumo,case when consumo>=minima and consumo<=maxima then ''CONSISTENTE''
				else
				''INCONSISTENTE''
				end
				as inconsistencia
				 from (
				  select comlec_metodo_id,minima,maxima,loext.lecturao3,loext.lecant,
				case when COALESCE (cast(lecturao3 as numeric),''0'')>=lecant then 
							((COALESCE (cast(lecturao3 as numeric),''0'')-lecant)*factor)+consret 

						

					when (COALESCE (cast(lecturao3 as numeric),''0'')+1)>=lecant then 
					(((COALESCE (cast(lecturao3 as numeric),''0'')+1)-lecant)*factor)+consret 
					when (COALESCE (cast(lecturao3 as numeric),''0'')+2)>=lecant then 
					(((COALESCE (cast(lecturao3 as numeric),''0'')+2)-lecant)*factor)+consret 
					else	
					(((10^digitos)-lecant+COALESCE (cast(lecturao3 as numeric),''0''))*factor)+consret 
						end as consumo	,

			case 
				when (COALESCE (cast(lecturao3 as numeric),''0'')-lecant)=-1 then 
					cast(lecant as varchar)
				when (COALESCE (cast(lecturao3 as numeric),''0'')-lecant)=-2  then 
					cast(lecant as varchar)
				when (COALESCE (cast(lecturao3 as numeric),''0'')-lecant)=-3  then 
					cast(lecant as varchar)
				else	
					cast(lecturao3 as varchar)
				end as lecturao,
					cobs.comlec_condicionobs_id as validacionobs	
						
						    from lecturas.comlec_ordenlecturas loext 
						    inner join lecturas.comlec_lecturametodo lm 
						    on loext.comlec_ordenlectura_id=lm.comlec_ordenlectura_id
						    left join lecturas.comlec_observaciones cobs on loext.obs3=cobs.codigocamp_varchar
						    where  loext.suministro= cast('''||var_suministro||''' as varchar)) a';
    loop
  

        FETCH vcursor INTO varcursor_lecturao,var_metodo,var_consumo,var_inconsistencia;
        EXIT WHEN NOT FOUND;
            ------------------inicio algotimo comlec_lecturametodo -------------------
        --RETURN 1;


	numerointentos_total=numerointentos_total+1;

        if (var_inconsistencia='CONSISTENTE') then 

		update lecturas.comlec_ordenlecturas set 
			lecturao3=varcursor_lecturao, 
			resultadoevaluacion=cast( var_inconsistencia as varchar),
			metodofinal3=cast(var_metodo as integer),
			montoconsumo3=var_consumo, 
			montoconsumo1=var_consumo, 
			up3='2', 
			fechaup3=now(), 
			obs3=cast(codigoobservacion as varchar)  ,
			flag_auditoria=true
		where suministro=var_suministro;

		exit;

        else

		if (numerointentos_total=2)then
			update lecturas.comlec_ordenlecturas set 
				lecturao3=varcursor_lecturao, 
				resultadoevaluacion=cast( var_inconsistencia as varchar),
				metodofinal3=cast(var_metodo as integer),
				montoconsumo3=var_consumo, 
				up3='2', 
				fechaup3=now(), 
				obs3=cast(codigoobservacion as varchar)  ,
				flag_auditoria=true
			where suministro=var_suministro;
		else
			update lecturas.comlec_ordenlecturas set 
				lecturao3=varcursor_lecturao, 
				resultadoevaluacion=cast( var_inconsistencia as varchar),
				metodofinal3=cast(var_metodo as integer),
				montoconsumo3=var_consumo, 
				up3='2', 
				fechaup3=now(), 
				obs2=cast(codigoobservacion as varchar)  
			where suministro=var_suministro;
		
		end if ;

        end if;

        
        

        
	

		
        end loop;
	CLOSE vcursor;
		
           ------------------fin algotimo comlec_lecturametodo -------------------


	update lecturas.comlec_ordenlecturas set flag_auditoria=false	where suministro=var_suministro;

    RETURN 1;

END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION lecturas.funcion_ejecutarminmax_rr(character varying, integer)
  OWNER TO ddanalytics;
  
  
  ---------------------------------------------------
