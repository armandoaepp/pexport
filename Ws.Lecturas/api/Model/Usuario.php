<?php
class Usuario{
	
	public $negocio = '';
		
	public function checkBrute($user_id){
		$cnx = loadModel('database',$this->negocio);
		$db = $cnx->getConnection();
		$now = time();
		$valid_attempts = $now - (0.5 * 60 * 60);
		$sql = "SELECT time FROM lecturas.login_attempts WHERE usuario_id = '$user_id' AND time > '$valid_attempts'";
		$arr_obj_checkbrute = pg_query($sql);
		
		if($arr_obj_checkbrute){ 
		$num_rows = pg_num_rows($arr_obj_checkbrute);
			if($num_rows >= 5){
				return true;
			}else{
				return false;
			}
		}
	}
	
    public function createUser($user, $password, $empleado) {
        $pass_hash = loadModel('PassHash','null');
        $response = array();
		date_default_timezone_set("America/Lima");
    	$now =  date("Y-m-d H:i:s");

        if (!$this->isUserExists($user)) {

            $password_hash = $pass_hash::hash($password);
			
            $api_key = $this->generateApiKey();
		
            $cnx = loadModel('database',$this->negocio);
			$db = $cnx->getConnection();
			$sql = "INSERT INTO lecturas.usuarios (nomusuario,passusuario,apikey,estado_id,created,glomas_empleado_id ) VALUES ('$user', '$password_hash', '$api_key','3','$now', '$empleado')";
			$result = pg_query($sql) or die ($sql);
            if ($result) {
                return 'USER_CREATED_SUCCESSFULLY';
            } else {
                return 'USER_CREATE_FAILED';
            }
        } else {
            return 'USER_ALREADY_EXISTED';
        }
        return $response;
    }

    public function editUser($id, $user, $password) {
    	$pass_hash = loadModel('PassHash','null');
    	$response = array();
    	$now = time();
    		$password_hash = $pass_hash::hash($password);
    			
    		$api_key = $this->generateApiKey();
    
    		$cnx = loadModel('database',$this->negocio);
    		$db = $cnx->getConnection();
    		$sql = "UPDATE lecturas.usuarios SET nomusuario='$user',passusuario='$password_hash' WHERE id='$id' ";
    		$result = pg_query($sql) or die ($sql);

    		if ($result) {
    			return 'USER_CREATED_SUCCESSFULLY';
    		} else {
    			return 'USER_CREATE_FAILED';
    		}
    	return $response;
    }
    	
	public function addBrute($user_id){
		$$cnx = loadModel('database',$this->negocio);
		$db = $cnx->getConnection();
		$now = time();
		$sql = "INSERT INTO lecturas.login_attempts (usuario_id, time) VALUES ('$user_id', '$now')";
		$arr_obj_checkbrute = pg_query($sql) or die ($sql);
	}
	
	public function validarUsuario($usuario,$pass){
		$cnx = loadModel('database',$this->negocio);
		$db = $cnx->getConnection();
		$data= array();
		$sql = "SELECT * FROM lecturas.usuarios WHERE nomusuario = '$usuario' and estado_id = '3'";
		$arr_obj_usuario = pg_query($sql) or die ($sql);
		$num_rows = pg_num_rows($arr_obj_usuario);		
		if($num_rows == 1){
			$obj_usuario=pg_fetch_object($arr_obj_usuario);
			$user_id = $obj_usuario->id;
			$db_password = $obj_usuario->passusuario;
			if($this->checkBrute($user_id)){
  
				$data['success'] = false;
				$data['message'] = 'Cuenta bloqueada';
                return $data;
			}else{
				$pass_hash = loadModel('PassHash','null');

				$is_valid_pass = $pass_hash::check_password($db_password, $pass);
				if($is_valid_pass){

					$user_id = preg_replace("/[^0-9]+/", "", $user_id); 						
					
					$data['IdUsuario'] =  $obj_usuario->id;
					$data['NomUsuario'] = $obj_usuario->nomusuario;
					$data['PassUsuario'] = $obj_usuario->passusuario;
					$data['ApiKey'] =  $obj_usuario->apikey;
					$data['created'] =  $obj_usuario->created;								
					$data['success'] = true;
					$data['message'] = 'Bienvenido: '.$obj_usuario->nomusuario;
					
					return $data;
				}else{
					$this->addBrute($user_id);	
					$data['success'] = false;					
					$data['message'] = 'contrasenia incorrecta';
					return $data;
				}				
			}		
		}else{
			$data['success'] = false;
			$data['message'] = 'Usuario no existe o duplicidad Usuarios';
			return $data;
		}
	}
	
	public function validarUsuarioWeb($usuario,$pass){
		$cnx = loadModel('database',$this->negocio);
		$db = $cnx->getConnection();
		$data= array();
		$sql = "SELECT lu.id, lu.nomusuario, lu.apikey, lu.created, lu.passusuario FROM lecturas.usuarios lu INNER JOIN lecturas.glomas_empleados ge on lu.glomas_empleado_id = ge.id WHERE glomas_tipoempleado_id = 2 AND nomusuario = '$usuario' and estado_id = '3'";
		$arr_obj_usuario = pg_query($sql) or die ($sql);
		$num_rows = pg_num_rows($arr_obj_usuario);
		if($num_rows == 1){
			$obj_usuario=pg_fetch_object($arr_obj_usuario);
			$user_id = $obj_usuario->id;
			$db_password = $obj_usuario->passusuario;
			if($this->checkBrute($user_id)){
				$data['success'] = false;
				$data['message'] = 'Cuenta bloqueada';
				return $data;
			}else{
				$pass_hash = loadModel('PassHash','null');
				$is_valid_pass = $pass_hash::check_password($db_password, $pass);
				if($is_valid_pass){
					$user_id = preg_replace("/[^0-9]+/", "", $user_id); //protecciÃ³n XSS ya que podemos imprimir este valor
						
					$data['IdUsuario'] =  $obj_usuario->id;
					$data['NomUsuario'] = $obj_usuario->nomusuario;
					$data['ApiKey'] =  $obj_usuario->apikey;
					$data['created'] =  $obj_usuario->created;
					$data['success'] = true;
					$data['message'] = 'Bienvenido: '.$obj_usuario->nomusuario;
						
					return $data;
				}else{
					$this->addBrute($user_id);
					$data['success'] = false;
					$data['message'] = 'contrasenia incorrecta';
					return $data;
				}
			}
		}else{
			$data['success'] = false;
			$data['message'] = 'Usuario no existe o duplicidad Usuarios';
			return $data;
		}
	}
	
    public function isValidApiKey($api_key = null) {
		$cnx = loadModel('database',$this->negocio);
		$db = $cnx->getConnection();
		$sql = "SELECT id FROM lecturas.usuarios WHERE apikey = '$api_key'";
		$arr_obj_usuario = pg_query($sql) or die ($sql);
		$num_rows = pg_num_rows($arr_obj_usuario);
        if($num_rows > 0){
			return true;
		}else{
			return false;
		}
    }
    
	public function isValidImei($usuario_id = null,$imei = null){
		$cnx = loadModel('database',$this->negocio);
		$db = $cnx->getConnection();
		$sql = "SELECT id FROM lecturas.usuario_dispositivos WHERE usuario_id=$usuario_id and imei = $imei and estado = '1'";
		$arr_obj_usuario = pg_query($sql) or die ($sql);
		$num_rows = pg_num_rows($arr_obj_usuario);
        if($num_rows > 0){
			return true;
		}else{
			return false;
		}
	}
	
    public function getUserId($api_key) {
		$cnx = loadModel('database',$this->negocio);
		$db = $cnx->getConnection();
		$sql = "SELECT IdUsuario FROM [ComMovTab.Usuario] WHERE ApiKey = '$api_key'";
		$arr_obj_usuario = pg_query($sql) or die ($sql);		
		$num_rows = pg_num_rows($arr_obj_usuario);
        if($num_rows > 0){
		$obj_usuario =pg_fetch_array($arr_obj_usuario);
		$user_id = $obj_usuario['IdUsuario'];
            return $user_id;
        } else {
            return NULL;
        }
    }
    
    public function getUserIdByUser($usuario) {
		$cnx = loadModel('database',$this->negocio);
		$db = $cnx->getConnection();
		$sql = "SELECT glomas_empleado_id FROM lecturas.usuarios WHERE nomusuario = '$usuario' and estado_id = '3'";
		$arr_obj_usuario = pg_query($sql) or die ($sql);		
		$num_rows = pg_num_rows($arr_obj_usuario);
        if($num_rows > 0){
		$obj_usuario=pg_fetch_array($arr_obj_usuario);
		$user_id = $obj_usuario['glomas_empleado_id'];
            return $user_id;
        } else {
            return 0;
        }
    }
    
    public function getUsuarios() {
		$cnx = loadModel('database',$this->negocio);
		$db = $cnx->getConnection();
		$sql = "SELECT * FROM [ComMovTab.Usuario]";
		$arr_obj_usuario = pg_query($sql) or die ($sql);		
		$num_rows = mssql_num_rows($arr_obj_usuario);
        if($num_rows > 0){
		$arr_obj_usuario=mssql_fetch_object($arr_obj_usuario);
            return $arr_obj_usuario;
        } else {
            return NULL;
        }
    }
    
    private function generateApiKey() {
        return md5(uniqid(rand(), true));
    }
	
    private function isUserExists($user) {
        $cnx = loadModel('database',$this->negocio);
		$db = $cnx->getConnection();
		$sql = "SELECT nomusuario FROM lecturas.usuarios WHERE nomusuario = '$user'";
		$arr_obj_usuario = pg_query($sql);
		$num_rows = pg_num_rows($arr_obj_usuario);		
		if($num_rows == 1){
			return true;
		}else{
			return false;
		}
    }
    
    public function getUsuarioEmpresa($usuario) {
        $cnx = loadModel('database',$this->negocio);
		$db = $cnx->getConnection();
		$sql = "SELECT glomas_grupo_id FROM lecturas.usuarios lu inner join lecturas.glomas_empleados ge on lu.glomas_empleado_id = ge.id WHERE nomusuario = '$usuario'";
		$arr_obj_usuario = pg_query($sql);
		
		$num_rows = pg_num_rows($arr_obj_usuario);		
		if($num_rows == 1){
			$obj_usuario = pg_fetch_array($arr_obj_usuario);
			$user_id = $obj_usuario['glomas_grupo_id'];
            return $user_id;
		}else{
			return 0;
		}
    }
	
}
?>