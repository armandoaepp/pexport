<?php
class OrdenLecturas{
	
	public $negocio = '';
	
	public function getDataOrdenLecturaByLecturista($idlecturista){
		set_time_limit(0);
		ini_set('max_execution_time', 30000);
		$cnx = loadModel('database',$this->negocio);
		$db = $cnx->getConnection();
	
		try {
			$header_sql = "comlec_ordenlectura_id,sector,ruta,suministro,cliente,digitos,direccion,seriefab,orden,factor,lecant,consret,lcorrelati,comlec_xml_id,resultadoevaluacion,medidornuevo,glomas_grupo_id, tipolectura";
				
			$sql1 = "SELECT ".$header_sql." from lecturas.comlec_ordenlecturas where (lecturista1_id = $idlecturista) AND (down1 = '0') AND (up1  = '0') AND (tipolectura = 'L')";
			$sql2 = "SELECT ".$header_sql." from lecturas.comlec_ordenlecturas where (lecturista2_id = $idlecturista) AND (down1 = '1') AND (up1 != '0') AND (tipolectura = 'R') AND (down2 = '0') AND (up2 = '0')";
			$sql3 = "SELECT ".$header_sql." from lecturas.comlec_ordenlecturas where (lecturista3_id = $idlecturista) AND (down1 = '1') AND (up1 != '0') AND (tipolectura = 'RR') AND (down2 = '1') AND (up2 != '0')  AND (down3 = '0') AND (up3 = '0')";
			$arr_obj_orden_lectura = pg_query($sql1.' UNION '.$sql2 .' UNION '.$sql3);
			if(!$arr_obj_orden_lectura){
				throw new Exception('error');
				exit();
			}
				
			$num_rows = pg_num_rows($arr_obj_orden_lectura);
			if($num_rows > 0){
				$data = array();
				while($obj_orden=pg_fetch_object($arr_obj_orden_lectura)) {
						
					$sql2= "select * from lecturas.comlec_lecturametodo where comlec_ordenlectura_id = trim('$obj_orden->comlec_ordenlectura_id')";
					$arr_obj_metodos = pg_query($sql2);
					if(!$arr_obj_metodos){
						throw new Exception('error');
						exit();
					}
						
					$metodo = array();
					while($obj_metodos=pg_fetch_object($arr_obj_metodos)) {
						$metodo[] =  $obj_metodos;				}
							
						$obj_orden->method = $metodo;
						$data[]= $obj_orden;
				}
				return $data;
			} else {
				return NULL;
			}
			 
		} catch (Exception $e) {
			error_log(date('Y-m-d H:i:s')." Error: ".$e." \n", 3, 'getDataOrdenLecturaByLecturistaCix-'.$this->negocio.'-'.date('Y-m-d').'.log');
			return NULL;
		}
	}

	public function getDataOrdenLecturaByLecturistaFac($idlecturista){
		set_time_limit(0);
		ini_set('max_execution_time', 30000);
		$cnx = loadModel('database',$this->negocio);
		$db = $cnx->getConnection();
		
		try {
			$header_sql = "comlec_ordenlectura_id,suministro,cliente,digitos,direccion,direccionr,seriefab,orden,factor,lecant,consret,lcorrelati,comlec_xml_id,resultadoevaluacion,medidornuevo,glomas_grupo_id, tipolectura, promedio, pinicio";
			$facturacion_element = " ,idplancondicion,idtarifa,tarifa,pfactura as periodo,idsector,sector,ruta,nombruta, glomas_unidadnegocio_id as idunidadnegocio, 
			tension, subestacion, tipoconexion, opciontarifaria, hilos, tipomedidor, potencia, to_char(inicio_contrato, 'DD/MM/YYYY') as inicio_contrato, to_char(fin_contrato, 'DD/MM/YYYY') as fin_contrato, to_char(fecha_emision, 'DD/MM/YYYY') as fecha_emision, 
					fecha_corte as fecha_corte, to_char(fecha_vencimiento, 'DD/MM/YYYY') as fecha_vencimiento, to_char(fecha_lecant, 'DD/MM/YYYY') as fecha_lecant, nro_recibo, fise_nro, fise_precio, to_char(fise_vencimiento, 'DD/MM/YYYY') as fise_vencimiento, fise_dni ";
			$sql1 = "SELECT ".$header_sql.$facturacion_element." from lecturas.comlec_ordenlecturas where (lecturista1_id = $idlecturista) AND (down1 = '0') AND (up1  = '0') AND (tipolectura = 'L')";
			$sql2 = "SELECT ".$header_sql.$facturacion_element." from lecturas.comlec_ordenlecturas where (lecturista2_id = $idlecturista) AND (down1 = '1') AND (up1 != '0') AND (tipolectura = 'R') AND (down2 = '0') AND (up2 = '0')";
			$sql3 = "SELECT ".$header_sql.$facturacion_element." from lecturas.comlec_ordenlecturas where (lecturista3_id = $idlecturista) AND (down1 = '1') AND (up1 != '0') AND (tipolectura = 'RR') AND (down2 = '1') AND (up2 != '0')  AND (down3 = '0') AND (up3 = '0')";
			$arr_obj_orden_lectura = pg_query($sql1.' UNION '.$sql2 .' UNION '.$sql3);
			if(!$arr_obj_orden_lectura){
				throw new Exception('error');
				exit();
			}
			
			$num_rows = pg_num_rows($arr_obj_orden_lectura);
			if($num_rows > 0){
				$data = array();
				while($obj_orden=pg_fetch_object($arr_obj_orden_lectura)) {
					
					$sql2= "select * from lecturas.comlec_lecturametodo where comlec_ordenlectura_id = trim('$obj_orden->comlec_ordenlectura_id')";
					$arr_obj_metodos = pg_query($sql2);
					if(!$arr_obj_metodos){
						throw new Exception('error');
						exit();
					}
					
					$metodo = array();
					while($obj_metodos=pg_fetch_object($arr_obj_metodos)) {
						$metodo[] =  $obj_metodos;
					}
					
					$obj_orden->method = $metodo;
					
					$obj_orden->grafico = array(
							"meses"=>"'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic', 'Ene', 'Feb'",
							"consumos"=>"290, 210, 210, 370, 300, 395, 330, 360, 280, 290, 310, 250, 150",
							"importes"=>"180, 140, 140, 230, 180, 250, 200, 220, 170, 179, 190, 140, 70"
					);
					$data[]= $obj_orden;
				}
					return $data;
	        } else {
	            return NULL;
	        }
	        
        } catch (Exception $e) {
        	error_log(date('Y-m-d H:i:s')." Error: ".$e." \n", 3, 'getDataOrdenLecturaByLecturistaCix-'.$this->negocio.'-'.date('Y-m-d').'.log');
        	return NULL;
        }
	}
	
	public function setUpdateBeforeSync($ordenes){
		set_time_limit(0);
		ini_set('max_execution_time', 30000);
		date_default_timezone_set("America/Lima");
		$cnx = loadModel('database',$this->negocio);
		$db = $cnx->getConnection();
		
		$fechadown = date("Y-m-d H:i:s");
		try {
			foreach($ordenes->Datos as $value){
				$tipo_lectura = trim($value->tipolectura);
				$suministro = trim($value->Suministro);
				
				if ($tipo_lectura == 'L') {
					$parametro = "down1 = '1', fechadown1 = '$fechadown'";
				} elseif ($tipo_lectura == 'R') {
					$parametro = "down2 = '1', fechadown2 = '$fechadown'";		
				} elseif ($tipo_lectura == 'RR') {
					$parametro = "down3 = '1', fechadown3 = '$fechadown'";		
				}
					 			 	
				$sql = "UPDATE lecturas.comlec_ordenlecturas SET ".$parametro." WHERE suministro = '$suministro'";

				$obj_orden = pg_query($sql) ;
				if(!$obj_orden){
					throw new Exception('error');
					exit();
				}	
			}
		return 1;
		} catch (Exception $e) {
			error_log(date('Y-m-d H:i:s')." Error: ".$e." \n", 3, 'setUpdateBeforeSync-'.$this->negocio.'-'.date('Y-m-d').'.log');
			return 0;
		}
	}
	
	public function SaveOt($json_lecturas){
		set_time_limit(0);
		ini_set('max_execution_time', 30000);
		date_default_timezone_set('America/Lima');
		$cnx = loadModel('database',$this->negocio);
		$db = $cnx->getConnection();
	
		try {
			foreach($json_lecturas as $value){
	
				$Suministro = $value->Suministro;
				$sql_orden_enviada = "select suministro from lecturas.comlec_ordenlecturas ol inner join lecturas.comlec_xml xl
				on ol.comlec_xml_id=xl.id where (ws_l_estado='1' or fechaexportacion is not null) and suministro='".$Suministro."'";
				$arr_obj_orden_enviada = pg_query($sql_orden_enviada);
				$num_rows_orden_enviada = pg_num_rows($arr_obj_orden_enviada);
				if($num_rows_orden_enviada > 0){
					$sql_save_auditoria = "insert into auditoria.comlec_ordenlecturas_after_close_process (suministro, json_request, created) values('".$Suministro."','".json_encode($value)."','".date('Y-m-d H:i:s')."')";
					pg_query($sql_save_auditoria);
				}else{
	
					$Lecturao = $value->LecturaO;
						
					if(isset($value->longitud)){
						$longitud = $value->longitud;
					}else{
						$longitud = 0;
					}
	
					if(isset($value->latitud)){
						$latitud = $value->latitud;
					}else{
						$latitud = 0;
					}
						
					$Medidornuevo = '';
					$CodLectura = $value->CodLectura; 
					$Codigo2 = $value->Codigo2; 

					if(isset($value->Resultadoevaluacion)){
						$Resultadoevaluacion = $value->Resultadoevaluacion;
					}else{
						$Resultadoevaluacion = 'INCONSISTENTE';
					}
					$consumo = $value->Consumo;
					$tipolectura = $value->tipolectura;
						
					if(trim($Lecturao)=='' && trim($CodLectura)==''){

						if(trim($Lecturao)=='' && trim($consumo)!=''){
							$fn_curita = $this->buscarLectura($tipolectura, $Suministro);
							$num_rows = pg_num_rows($fn_curita);
							if($num_rows > 0){
								$obj_orden = pg_fetch_object($fn_curita);
								$curita_lecant = $obj_orden->lecant;
								$curita_factor = $obj_orden->factor;
								if((int) $curita_factor == (int) 1){
									$Lecturao = (int) trim($consumo) + (int) trim($curita_lecant);
								}else{
									$Lecturao = ((int) trim($consumo)/(int)$curita_factor) + (int) trim($curita_lecant);
								}
							}
						}else{
							$Resultadoevaluacion = 'INCONSISTENTE';
						}
					}
						
					$fotografia = $value->fotografia;
						
					$Metodo = $value->Metodo;
					$fecha_movil = date_create($value->fecha);
					$fecha = date_format($fecha_movil, 'Y-m-d H:i:s');
					$fechaup = date("Y-m-d H:i:s");

					if(isset($value->intentos)){
						$intentos = $value->intentos;
					}else{
						$intentos = '1';
					}
						
					if($consumo == ''){
						$consumo = 'NULL';
					}
						
					if($Metodo == ''){
						$Metodo = 'NULL';
					}
						
					$direccion = $value->Direccion;
						
					if(isset($value->SuministroP)){
						$sum_siguiente = $value->SuministroP;
					}else{
						$sum_siguiente = '000000';
					}
						
					if(isset($value->SuministroA)){
						$sum_anterior = $value->SuministroA;
					}else{
						$sum_anterior = '000000';
					}
						
						
					$Lecturaold = '';
						
					if($this->negocio=='cix'){
						$cod_medidor_altura = 10;
						$cod_predio_deshabilitado = 12;
						$cod_fuera_de_ruta = 20;
						$cod_no_ubicado = 21;
					}else{
						$cod_medidor_altura = 3;
						$cod_predio_deshabilitado = 15;
						$cod_fuera_de_ruta = 24;
						$cod_no_ubicado = 41;
					}
						
					$sql_obss = "select codigofac,codigocamp from lecturas.comlec_observaciones where codigofac in ('AL','PD','FR','NU')";
					$arr_obss = pg_query($sql_obss);
					while($obj_obs=pg_fetch_object($arr_obss)) {
						if($obj_obs->codigofac=='AL'){
							$cod_medidor_altura = $obj_obs->codigocamp;
						}elseif($obj_obs->codigofac=='PD'){
							$cod_predio_deshabilitado = $obj_obs->codigocamp;
						}elseif($obj_obs->codigofac=='FR'){
							$cod_fuera_de_ruta = $obj_obs->codigocamp;
						}elseif($obj_obs->codigofac=='NU'){
							$cod_no_ubicado = $obj_obs->codigocamp;
						}
					}
						
					if ($tipolectura == 'L') {
						if($CodLectura == $cod_medidor_altura || $CodLectura == $cod_predio_deshabilitado){
							$arr_obj_orden_lectura = $this->buscarLectura($tipolectura, $Suministro);
							$num_rows = pg_num_rows($arr_obj_orden_lectura);
							if($num_rows > 0){
								$obj_orden = pg_fetch_object($arr_obj_orden_lectura) ;
								$lecturao1 = $obj_orden->lecturao1;
								if($lecturao1 !=''){
									$Lecturaold = $Lecturao;
									$Lecturao = $lecturao1;
									if($CodLectura == $cod_predio_deshabilitado){
										$CodLectura = NULL;
									}else{
										$CodLectura = $cod_predio_deshabilitado;
									}
										
									$Resultadoevaluacion = $obj_orden->resultadoevaluacion;
								}
							}
						}
	
						
						if($Lecturao!='' && $CodLectura == $cod_predio_deshabilitado){
							$CodLectura = $cod_medidor_altura;
						}
						if($Lecturao!='' && $CodLectura == $cod_fuera_de_ruta){
							$CodLectura = NULL;
						}
						if($Lecturao!='' && $CodLectura == $cod_no_ubicado){
							$CodLectura = NULL;
						}
						
	
						$parametro = "up1 = '1', fechaup1 = '$fechaup',fechaejecucion1= '$fecha', lecturao1 = '$Lecturao', montoconsumo1 = $consumo, obs1 = '$CodLectura', obs11='$Codigo2', foto1 = '$fotografia', latitud1 = '$latitud', longitud1='$longitud',intentos1 = '$intentos', metodofinal1 = $Metodo,lecturaoold = '$Lecturaold',";
						
					} elseif ($tipolectura == 'R') {
						$Lecturao1 = $Lecturao;
						$r_consistente = 'NO';
						if($Resultadoevaluacion == 'CONSISTENTE'){
							$arr_obj_orden_lectura = $this->buscarLectura($tipolectura, $Suministro);
							$num_rows = pg_num_rows($arr_obj_orden_lectura);
							if($num_rows > 0){
								$obj_orden = pg_fetch_object($arr_obj_orden_lectura);
								$Lecturaold = $obj_orden->lecturao1;
								$Lecturao1 = $Lecturao;
								$Lecturao2 = $Lecturao;
								$Resultadoevaluacion = 'CONSISTENTE';
								$r_consistente = 'SI';
							}
						}
	
						if($CodLectura == $cod_medidor_altura || $CodLectura == $cod_predio_deshabilitado){
							$arr_obj_orden_lectura = $this->buscarLectura($tipolectura, $Suministro);
							$num_rows = pg_num_rows($arr_obj_orden_lectura);
							if($num_rows > 0){
								$obj_orden = pg_fetch_object($arr_obj_orden_lectura) ;
								$lecturao2 = $obj_orden->lecturao2;
								if($lecturao2 !=''){
									$Lecturaold = $Lecturao;
									$Lecturao1 = $lecturao2;
									if($CodLectura == $cod_predio_deshabilitado){
										$CodLectura = NULL;
									}else{
										$CodLectura = $cod_predio_deshabilitado;
									}
									$Resultadoevaluacion = $obj_orden->resultadoevaluacion;
								}
							}
						}
	
						
						if($Lecturao!='' && $CodLectura == $cod_predio_deshabilitado){
							$CodLectura = $cod_medidor_altura;
						}
						if($Lecturao!='' && $CodLectura == $cod_fuera_de_ruta){
							$CodLectura = NULL;
						}
						if($Lecturao!='' && $CodLectura == $cod_no_ubicado){
							$CodLectura = NULL;
						}
						
	
						if($r_consistente == 'SI'){
							$parametro = "lecturao1 = '$Lecturao1', montoconsumo1 = $consumo, obs1 = '$CodLectura', up2 = '1', fechaup2 = '$fechaup',fechaejecucion2= '$fecha', lecturao2 = '$Lecturao2', montoconsumo2 = $consumo, obs2 = '$CodLectura', obs22='$Codigo2', foto2 = '$fotografia', latitud2 = '$latitud', longitud2='$longitud',intentos2 = '$intentos', metodofinal2 = $Metodo,lecturaoold = '$Lecturaold',";
						}else{
							$parametro = "up2 = '1', fechaup2 = '$fechaup',fechaejecucion2= '$fecha', lecturao2 = '$Lecturao1', montoconsumo2 = $consumo, obs2 = '$CodLectura', obs22='$Codigo2', foto2 = '$fotografia', latitud2 = '$latitud', longitud2='$longitud',intentos2 = '$intentos', metodofinal2 = $Metodo,lecturaoold = '$Lecturaold',";
						}
					} elseif ($tipolectura == 'RR') {
						if($CodLectura == $cod_medidor_altura || $CodLectura == $cod_predio_deshabilitado){
							$arr_obj_orden_lectura = $this->buscarLectura($tipolectura, $Suministro);
							$num_rows = pg_num_rows($arr_obj_orden_lectura);
							if($num_rows > 0){
								$obj_orden = pg_fetch_object($arr_obj_orden_lectura);
								$lecturao3 = $obj_orden->lecturao3;
								if($lecturao3 !=''){
									$Lecturaold = $Lecturao;
									$Lecturao = $lecturao3;
									if($CodLectura == $cod_predio_deshabilitado){
										$CodLectura = NULL;
									}else{
										$CodLectura = $cod_predio_deshabilitado;
									}
									$Resultadoevaluacion = $obj_orden->resultadoevaluacion;
								}
							}
						}
	
						
						if($Lecturao!='' && $CodLectura == $cod_predio_deshabilitado){
							$CodLectura = $cod_medidor_altura;
						}
						if($Lecturao!='' && $CodLectura == $cod_fuera_de_ruta){
							$CodLectura = NULL;
						}
						if($Lecturao!='' && $CodLectura == $cod_no_ubicado){
							$CodLectura = NULL;
						}
						
	
						$parametro = "up3 = '1', fechaup3 = '$fechaup',fechaejecucion3= '$fecha', lecturao3 = '$Lecturao', montoconsumo3 = $consumo, obs3 = '$CodLectura', obs33='$Codigo2', foto3 = '$fotografia', latitud3 = '$latitud', longitud3='$longitud',intentos3 = '$intentos', metodofinal3 = $Metodo,lecturaoold = '$Lecturaold',";
					}
	
					if($tipolectura == 'N'){
						$sql = "INSERT INTO lecturas.comlec_nuevosuministro (fechaup, fechaejecucion, lecturao, montoconsumo, obs1, obs11, foto1, latitud1, longitud1, medidornuevo, resultadoevaluacion, suministro, direccion, sum_siguiente, sum_anterior) VALUES ('$fechaup','$fecha', '$Lecturao',  $consumo, '$CodLectura', '$Codigo2', '$fotografia', '$latitud', '$longitud','$Medidornuevo', '$Resultadoevaluacion', '$Suministro', '$direccion','$sum_siguiente', '$sum_anterior')";
						error_log($sql, 3, 'sql.log');
						$arr_obj_orden = pg_query($sql);
					}else{
						$sql = "UPDATE lecturas.comlec_ordenlecturas SET ".$parametro." medidornuevo='$Medidornuevo', resultadoevaluacion = '$Resultadoevaluacion',flag_auditoria = TRUE WHERE suministro = '$Suministro'";
						$arr_obj_orden = pg_query($sql);
					}
						
					if(!$arr_obj_orden){
						throw new Exception('error');
						exit();
					}
				}
			}
			return 1;
		} catch (Exception $e) {
			error_log(date('Y-m-d H:i:s')." Error: ".$e." \n", 3, 'SaveOt-'.$this->negocio.'-'.date('Y-m-d').'.log');
			return 0;
		}
	}	
	
	public function SaveOtCix($json_lecturas){
		set_time_limit(0);
		ini_set('max_execution_time', 30000);
		date_default_timezone_set('America/Lima');
		$cnx = loadModel('database','cix');	
		$db = $cnx->getConnection();

		try {
			foreach($json_lecturas as $value){
			
				$Suministro = $value->Suministro;
				$sql_orden_enviada = "select suministro from lecturas.comlec_ordenlecturas ol inner join lecturas.comlec_xml xl 
				on ol.comlec_xml_id=xl.id where (ws_l_estado='1' or fechaexportacion is not null) and suministro='".$Suministro."'";
				$arr_obj_orden_enviada = pg_query($sql_orden_enviada);
				$num_rows_orden_enviada = pg_num_rows($arr_obj_orden_enviada);
				if($num_rows_orden_enviada > 0){
					$sql_save_auditoria = "insert into auditoria.comlec_ordenlecturas_after_close_process (suministro, json_request, created) values('".$Suministro."','".json_encode($value)."','".date('Y-m-d H:i:s')."')";
					pg_query($sql_save_auditoria);
				}else{				
					$Lecturao = $value->LecturaO;
					
					if(isset($value->longitud)){
						$longitud = $value->longitud;
					}else{
						$longitud = 0;
					}

					if(isset($value->latitud)){
						$latitud = $value->latitud;
					}else{
						$latitud = 0;
					}
					
					$Medidornuevo = '';
					$CodLectura = $value->CodLectura; 
					$Codigo2 = $value->Codigo2; 
					if(isset($value->Resultadoevaluacion)){
						$Resultadoevaluacion = $value->Resultadoevaluacion;
					}else{
						$Resultadoevaluacion = 'INCONSISTENTE';
					}
					
					if(trim($Lecturao)=='' && trim($CodLectura)==''){
						$Resultadoevaluacion = 'INCONSISTENTE';
					}
					
					$fotografia = $value->fotografia;
					$consumo = $value->Consumo;
					$Metodo = $value->Metodo;
					$fecha_movil = date_create($value->fecha);
					$fecha = date_format($fecha_movil, 'Y-m-d H:i:s');
					$fechaup = date("Y-m-d H:i:s");
					if(isset($value->intentos)){
						$intentos = $value->intentos;
					}else{
						$intentos = '1';
					}
					
					$tipolectura = $value->tipolectura;
					
					if($consumo == ''){
						$consumo = 'NULL';
					}
					
					if($Metodo == ''){
						$Metodo = 'NULL';
					}
					
					$Lecturaold = '';
					
					if($this->negocio=='cix'){
						$cod_medidor_altura = 10;
						$cod_predio_deshabilitado = 12;
						$cod_fuera_de_ruta = 20;
						$cod_no_ubicado = 21;
					}else{
						$cod_medidor_altura = 3;
						$cod_predio_deshabilitado = 15;
						$cod_fuera_de_ruta = 24;
						$cod_no_ubicado = 41;
					}
					
					$sql_obss = "select codigofac,codigocamp from lecturas.comlec_observaciones where codigofac in ('AL','PD','FR','NU')";
					$arr_obss = pg_query($sql_obss);
					while($obj_obs=pg_fetch_object($arr_obss)) {
						if($obj_obs->codigofac=='AL'){
							$cod_medidor_altura = $obj_obs->codigocamp;
						}elseif($obj_obs->codigofac=='PD'){
							$cod_predio_deshabilitado = $obj_obs->codigocamp;
						}elseif($obj_obs->codigofac=='FR'){
							$cod_fuera_de_ruta = $obj_obs->codigocamp;
						}elseif($obj_obs->codigofac=='NU'){
							$cod_no_ubicado = $obj_obs->codigocamp;
						}
					}
					
					if ($tipolectura == 'L') {
						if($CodLectura == $cod_medidor_altura || $CodLectura == $cod_predio_deshabilitado){
							$arr_obj_orden_lectura = $this->buscarLectura($tipolectura, $Suministro);
							$num_rows = pg_num_rows($arr_obj_orden_lectura);
							if($num_rows > 0){
								$obj_orden = pg_fetch_object($arr_obj_orden_lectura) ;
								$lecturao1 = $obj_orden->lecturao1;
								if($lecturao1 !=''){
									$Lecturaold = $Lecturao;
									$Lecturao = $lecturao1;
									if($CodLectura == $cod_predio_deshabilitado){
										$CodLectura = NULL;
									}else{
										$CodLectura = $cod_predio_deshabilitado;
									}
									
									$Resultadoevaluacion = $obj_orden->resultadoevaluacion;
								}
							}
						}
						
						if($Lecturao!='' && $CodLectura == $cod_predio_deshabilitado){
							$CodLectura = $cod_medidor_altura;
						}
						if($Lecturao!='' && $CodLectura == $cod_fuera_de_ruta){
							$CodLectura = NULL;
						}
						if($Lecturao!='' && $CodLectura == $cod_no_ubicado){
							$CodLectura = NULL;
						}
						
						$parametro = "up1 = '1', fechaup1 = '$fechaup',fechaejecucion1= '$fecha', lecturao1 = '$Lecturao', montoconsumo1 = $consumo, obs1 = '$CodLectura', obs11='$Codigo2', foto1 = '$fotografia', latitud1 = '$latitud', longitud1='$longitud',intentos1 = '$intentos', metodofinal1 = $Metodo,lecturaoold = '$Lecturaold',";
					
					} elseif ($tipolectura == 'R') {
						$Lecturao1 = $Lecturao;
						$r_consistente = 'NO';
						if($Resultadoevaluacion == 'CONSISTENTE'){
							$arr_obj_orden_lectura = $this->buscarLectura($tipolectura, $Suministro);
							$num_rows = pg_num_rows($arr_obj_orden_lectura);
							if($num_rows > 0){
								$obj_orden = pg_fetch_object($arr_obj_orden_lectura);						
								$Lecturaold = $obj_orden->lecturao1;
								$Lecturao1 = $Lecturao;
								$Lecturao2 = $Lecturao;
								$Resultadoevaluacion = 'CONSISTENTE';
								$r_consistente = 'SI';						
							}
						}
						
						if($CodLectura == $cod_medidor_altura || $CodLectura == $cod_predio_deshabilitado){
							$arr_obj_orden_lectura = $this->buscarLectura($tipolectura, $Suministro);
							$num_rows = pg_num_rows($arr_obj_orden_lectura);
							if($num_rows > 0){
								$obj_orden = pg_fetch_object($arr_obj_orden_lectura) ;
								$lecturao2 = $obj_orden->lecturao2;
								if($lecturao2 !=''){
									$Lecturaold = $Lecturao;
									$Lecturao1 = $lecturao2;
									if($CodLectura == $cod_predio_deshabilitado){
										$CodLectura = NULL;
									}else{
										$CodLectura = $cod_predio_deshabilitado;
									}
									$Resultadoevaluacion = $obj_orden->resultadoevaluacion;
								}
							}
						}
						
						if($Lecturao!='' && $CodLectura == $cod_predio_deshabilitado){
							$CodLectura = $cod_medidor_altura;
						}
						if($Lecturao!='' && $CodLectura == $cod_fuera_de_ruta){
							$CodLectura = NULL;
						}
						if($Lecturao!='' && $CodLectura == $cod_no_ubicado){
							$CodLectura = NULL;
						}
						
						if($r_consistente == 'SI'){
							$parametro = "lecturao1 = '$Lecturao1', montoconsumo1 = $consumo, obs1 = '$CodLectura', up2 = '1', fechaup2 = '$fechaup',fechaejecucion2= '$fecha', lecturao2 = '$Lecturao2', montoconsumo2 = $consumo, obs2 = '$CodLectura', obs22='$Codigo2', foto2 = '$fotografia', latitud2 = '$latitud', longitud2='$longitud',intentos2 = '$intentos', metodofinal2 = $Metodo,lecturaoold = '$Lecturaold',";
						}else{
							$parametro = "up2 = '1', fechaup2 = '$fechaup',fechaejecucion2= '$fecha', lecturao2 = '$Lecturao1', montoconsumo2 = $consumo, obs2 = '$CodLectura', obs22='$Codigo2', foto2 = '$fotografia', latitud2 = '$latitud', longitud2='$longitud',intentos2 = '$intentos', metodofinal2 = $Metodo,lecturaoold = '$Lecturaold',";
						}
					} elseif ($tipolectura == 'RR') {
						if($CodLectura == $cod_medidor_altura || $CodLectura == $cod_predio_deshabilitado){
							$arr_obj_orden_lectura = $this->buscarLectura($tipolectura, $Suministro);
							$num_rows = pg_num_rows($arr_obj_orden_lectura);
							if($num_rows > 0){
								$obj_orden = pg_fetch_object($arr_obj_orden_lectura);
								$lecturao3 = $obj_orden->lecturao3;
								if($lecturao3 !=''){
									$Lecturaold = $Lecturao;
									$Lecturao = $lecturao3;
									if($CodLectura == $cod_predio_deshabilitado){
										$CodLectura = NULL;
									}else{
										$CodLectura = $cod_predio_deshabilitado;
									}
									$Resultadoevaluacion = $obj_orden->resultadoevaluacion;
								}
							}
						}
						
						if($Lecturao!='' && $CodLectura == $cod_predio_deshabilitado){
							$CodLectura = $cod_medidor_altura;
						}
						if($Lecturao!='' && $CodLectura == $cod_fuera_de_ruta){
							$CodLectura = NULL;
						}
						if($Lecturao!='' && $CodLectura == $cod_no_ubicado){
							$CodLectura = NULL;
						}
						
						$parametro = "up3 = '1', fechaup3 = '$fechaup',fechaejecucion3= '$fecha', lecturao3 = '$Lecturao', montoconsumo3 = $consumo, obs3 = '$CodLectura', obs33='$Codigo2', foto3 = '$fotografia', latitud3 = '$latitud', longitud3='$longitud',intentos3 = '$intentos', metodofinal3 = $Metodo,lecturaoold = '$Lecturaold',";
					}

					$sql = "UPDATE lecturas.comlec_ordenlecturas SET ".$parametro." medidornuevo='$Medidornuevo', resultadoevaluacion = '$Resultadoevaluacion',flag_auditoria = TRUE WHERE suministro = '$Suministro'";
					$arr_obj_orden = pg_query($sql);
					
					if(!$arr_obj_orden){
						throw new Exception('error');
						exit();
					}
				}
			}			
			return 1;
		} catch (Exception $e) {
			error_log(date('Y-m-d H:i:s')." Error: ".$e." \n", 3, 'SaveOtCix-'.date('Y-m-d').'.log');

			return 0;
		}
	}
	
	public function SaveOtFac($json_lecturas){
		set_time_limit(0);
		ini_set('max_execution_time', 30000);
		date_default_timezone_set('America/Lima');
		$cnx = loadModel('database',$this->negocio);
		$db = $cnx->getConnection();
	
		try {
			foreach($json_lecturas as $value){
	
				$Suministro = $value->Suministro;
				$sql_orden_enviada = "select suministro from lecturas.comlec_ordenlecturas ol inner join lecturas.comlec_xml xl
				on ol.comlec_xml_id=xl.id where (ws_l_estado='1' or fechaexportacion is not null) and suministro='".$Suministro."'";
				$arr_obj_orden_enviada = pg_query($sql_orden_enviada);
				$num_rows_orden_enviada = pg_num_rows($arr_obj_orden_enviada);
				if($num_rows_orden_enviada > 0){
					$sql_save_auditoria = "insert into auditoria.comlec_ordenlecturas_after_close_process (suministro, json_request, created) values('".$Suministro."','".json_encode($value)."','".date('Y-m-d H:i:s')."')";
					pg_query($sql_save_auditoria);
				}else{
	
					$Lecturao = $value->LecturaO;
						
					if(isset($value->longitud)){
						$longitud = $value->longitud;
					}else{
						$longitud = 0;
					}
	
					if(isset($value->latitud)){
						$latitud = $value->latitud;
					}else{
						$latitud = 0;
					}
						
					$Medidornuevo = '';
					$CodLectura = $value->CodLectura;
					$Codigo2 = $value->Codigo2; 
					if(isset($value->Resultadoevaluacion)){
						$Resultadoevaluacion = $value->Resultadoevaluacion;
					}else{
						$Resultadoevaluacion = 'INCONSISTENTE';
					}
					$consumo = $value->Consumo;
					$tipolectura = $value->tipolectura;
						
					if(trim($Lecturao)=='' && trim($CodLectura)==''){

						if(trim($Lecturao)=='' && trim($consumo)!=''){
							$fn_curita = $this->buscarLectura($tipolectura, $Suministro);
							$num_rows = pg_num_rows($fn_curita);
							if($num_rows > 0){
								$obj_orden = pg_fetch_object($fn_curita);
								$curita_lecant = $obj_orden->lecant;
								$curita_factor = $obj_orden->factor;
								if((int) $curita_factor == (int) 1){
									$Lecturao = (int) trim($consumo) + (int) trim($curita_lecant);
								}else{
									$Lecturao = ((int) trim($consumo)/(int)$curita_factor) + (int) trim($curita_lecant);
								}
							}
						}else{
							$Resultadoevaluacion = 'INCONSISTENTE';
						}
					}
						
					$fotografia = $value->fotografia;
					
					if(isset($value->factura)){
						$factura = $value ->factura;
					}else{
						$factura = NULL;
					}
					
					$Metodo = $value->Metodo;
					$fecha_movil = date_create($value->fecha);
					$fecha = date_format($fecha_movil, 'Y-m-d H:i:s');
					$fechaup = date("Y-m-d H:i:s");

					if(isset($value->intentos)){
						$intentos = $value->intentos;
					}else{
						$intentos = '1';
					}
						
					if($consumo == ''){
						$consumo = 'NULL';
					}
						
					if($Metodo == ''){
						$Metodo = 'NULL';
					}
						
					$direccion = $value->Direccion;
						
					if(isset($value->SuministroP)){
						$sum_siguiente = $value->SuministroP;
					}else{
						$sum_siguiente = '000000';
					}
						
					if(isset($value->SuministroA)){
						$sum_anterior = $value->SuministroA;
					}else{
						$sum_anterior = '000000';
					}
						
						
					$Lecturaold = '';
						
					if($this->negocio=='cix'){
						$cod_medidor_altura = 10;
						$cod_predio_deshabilitado = 12;
						$cod_fuera_de_ruta = 20;
						$cod_no_ubicado = 21;
					}else{
						$cod_medidor_altura = 3;
						$cod_predio_deshabilitado = 15;
						$cod_fuera_de_ruta = 24;
						$cod_no_ubicado = 41;
					}
						
					$sql_obss = "select codigofac,codigocamp from lecturas.comlec_observaciones where codigofac in ('AL','PD','FR','NU')";
					$arr_obss = pg_query($sql_obss);
					while($obj_obs=pg_fetch_object($arr_obss)) {
						if($obj_obs->codigofac=='AL'){
							$cod_medidor_altura = $obj_obs->codigocamp;
						}elseif($obj_obs->codigofac=='PD'){
							$cod_predio_deshabilitado = $obj_obs->codigocamp;
						}elseif($obj_obs->codigofac=='FR'){
							$cod_fuera_de_ruta = $obj_obs->codigocamp;
						}elseif($obj_obs->codigofac=='NU'){
							$cod_no_ubicado = $obj_obs->codigocamp;
						}
					}
						
					if ($tipolectura == 'L') {
						if($CodLectura == $cod_medidor_altura || $CodLectura == $cod_predio_deshabilitado){
							$arr_obj_orden_lectura = $this->buscarLectura($tipolectura, $Suministro);
							$num_rows = pg_num_rows($arr_obj_orden_lectura);
							if($num_rows > 0){
								$obj_orden = pg_fetch_object($arr_obj_orden_lectura) ;
								$lecturao1 = $obj_orden->lecturao1;
								if($lecturao1 !=''){
									$Lecturaold = $Lecturao;
									$Lecturao = $lecturao1;
									if($CodLectura == $cod_predio_deshabilitado){
										$CodLectura = NULL;
									}else{
										$CodLectura = $cod_predio_deshabilitado;
									}
										
									$Resultadoevaluacion = $obj_orden->resultadoevaluacion;
								}
							}
						}
	

						if($Lecturao!='' && $CodLectura == $cod_predio_deshabilitado){
							$CodLectura = $cod_medidor_altura;
						}
						if($Lecturao!='' && $CodLectura == $cod_fuera_de_ruta){
							$CodLectura = NULL;
						}
						if($Lecturao!='' && $CodLectura == $cod_no_ubicado){
							$CodLectura = NULL;
						}

	
						$parametro = "up1 = '1', fechaup1 = '$fechaup',fechaejecucion1= '$fecha', lecturao1 = '$Lecturao', montoconsumo1 = $consumo, obs1 = '$CodLectura', obs11='$Codigo2', foto1 = '$fotografia',factura = '$factura', latitud1 = '$latitud', longitud1='$longitud',intentos1 = '$intentos', metodofinal1 = $Metodo,lecturaoold = '$Lecturaold',";

					} elseif ($tipolectura == 'R') {
						$Lecturao1 = $Lecturao;
						$r_consistente = 'NO';
						if($Resultadoevaluacion == 'CONSISTENTE'){
							$arr_obj_orden_lectura = $this->buscarLectura($tipolectura, $Suministro);
							$num_rows = pg_num_rows($arr_obj_orden_lectura);
							if($num_rows > 0){
								$obj_orden = pg_fetch_object($arr_obj_orden_lectura);
								$Lecturaold = $obj_orden->lecturao1;
								$Lecturao1 = $Lecturao;
								$Lecturao2 = $Lecturao;
								$Resultadoevaluacion = 'CONSISTENTE';
								$r_consistente = 'SI';
							}
						}
	
						if($CodLectura == $cod_medidor_altura || $CodLectura == $cod_predio_deshabilitado){
							$arr_obj_orden_lectura = $this->buscarLectura($tipolectura, $Suministro);
							$num_rows = pg_num_rows($arr_obj_orden_lectura);
							if($num_rows > 0){
								$obj_orden = pg_fetch_object($arr_obj_orden_lectura) ;
								$lecturao2 = $obj_orden->lecturao2;
								if($lecturao2 !=''){
									$Lecturaold = $Lecturao;
									$Lecturao1 = $lecturao2;
									if($CodLectura == $cod_predio_deshabilitado){
										$CodLectura = NULL;
									}else{
										$CodLectura = $cod_predio_deshabilitado;
									}
									$Resultadoevaluacion = $obj_orden->resultadoevaluacion;
								}
							}
						}
	

						if($Lecturao!='' && $CodLectura == $cod_predio_deshabilitado){
							$CodLectura = $cod_medidor_altura;
						}
						if($Lecturao!='' && $CodLectura == $cod_fuera_de_ruta){
							$CodLectura = NULL;
						}
						if($Lecturao!='' && $CodLectura == $cod_no_ubicado){
							$CodLectura = NULL;
						}

	
						if($r_consistente == 'SI'){
							$parametro = "lecturao1 = '$Lecturao1', montoconsumo1 = $consumo, obs1 = '$CodLectura', up2 = '1', fechaup2 = '$fechaup',fechaejecucion2= '$fecha', lecturao2 = '$Lecturao2', montoconsumo2 = $consumo, obs2 = '$CodLectura', obs22='$Codigo2', foto2 = '$fotografia', latitud2 = '$latitud', longitud2='$longitud',intentos2 = '$intentos', metodofinal2 = $Metodo,lecturaoold = '$Lecturaold',";
						}else{
							$parametro = "up2 = '1', fechaup2 = '$fechaup',fechaejecucion2= '$fecha', lecturao2 = '$Lecturao1', montoconsumo2 = $consumo, obs2 = '$CodLectura', obs22='$Codigo2', foto2 = '$fotografia', latitud2 = '$latitud', longitud2='$longitud',intentos2 = '$intentos', metodofinal2 = $Metodo,lecturaoold = '$Lecturaold',";
						}
					} elseif ($tipolectura == 'RR') {
						if($CodLectura == $cod_medidor_altura || $CodLectura == $cod_predio_deshabilitado){
							$arr_obj_orden_lectura = $this->buscarLectura($tipolectura, $Suministro);
							$num_rows = pg_num_rows($arr_obj_orden_lectura);
							if($num_rows > 0){
								$obj_orden = pg_fetch_object($arr_obj_orden_lectura);
								$lecturao3 = $obj_orden->lecturao3;
								if($lecturao3 !=''){
									$Lecturaold = $Lecturao;
									$Lecturao = $lecturao3;
									if($CodLectura == $cod_predio_deshabilitado){
										$CodLectura = NULL;
									}else{
										$CodLectura = $cod_predio_deshabilitado;
									}
									$Resultadoevaluacion = $obj_orden->resultadoevaluacion;
								}
							}
						}
	

						if($Lecturao!='' && $CodLectura == $cod_predio_deshabilitado){
							$CodLectura = $cod_medidor_altura;
						}
						if($Lecturao!='' && $CodLectura == $cod_fuera_de_ruta){
							$CodLectura = NULL;
						}
						if($Lecturao!='' && $CodLectura == $cod_no_ubicado){
							$CodLectura = NULL;
						}

	
						$parametro = "up3 = '1', fechaup3 = '$fechaup',fechaejecucion3= '$fecha', lecturao3 = '$Lecturao', montoconsumo3 = $consumo, obs3 = '$CodLectura', obs33='$Codigo2', foto3 = '$fotografia', latitud3 = '$latitud', longitud3='$longitud',intentos3 = '$intentos', metodofinal3 = $Metodo,lecturaoold = '$Lecturaold',";
					}
	
					if($tipolectura == 'N'){
						$sql = "INSERT INTO lecturas.comlec_nuevosuministro (fechaup, fechaejecucion, lecturao, montoconsumo, obs1, obs11, foto1, latitud1, longitud1, medidornuevo, resultadoevaluacion, suministro, direccion, sum_siguiente, sum_anterior) VALUES ('$fechaup','$fecha', '$Lecturao',  $consumo, '$CodLectura', '$Codigo2', '$fotografia', '$latitud', '$longitud','$Medidornuevo', '$Resultadoevaluacion', '$Suministro', '$direccion','$sum_siguiente', '$sum_anterior')";
						error_log($sql, 3, 'sql.log');
						$arr_obj_orden = pg_query($sql);
					}else{
						$sql = "UPDATE lecturas.comlec_ordenlecturas SET ".$parametro." medidornuevo='$Medidornuevo', resultadoevaluacion = '$Resultadoevaluacion',flag_auditoria = TRUE WHERE suministro = '$Suministro'";
						$arr_obj_orden = pg_query($sql);
					}
						
					if(!$arr_obj_orden){
						throw new Exception('error');
						exit();
					}
				}
			}
			return 1;
		} catch (Exception $e) {
			error_log(date('Y-m-d H:i:s')." Error: ".$e." \n", 3, 'SaveOt-'.$this->negocio.'-'.date('Y-m-d').'.log');
			return 0;
		}
	}
	
	public function buscarLectura($tipolectura, $suministro){
		set_time_limit(0);
		ini_set('max_execution_time', 30000);
		$cnx = loadModel('database', $this->negocio);
		$db = $cnx->getConnection();
		if($tipolectura == 'L'){
			$lectura = 'lecturao1, lecturao2';
		}elseif($tipolectura == 'R'){
			$lectura = 'lecturao1, lecturao2';
		}elseif ($tipolectura == 'RR'){
			$lectura = 'lecturao3';
		}
		$sql = "SELECT ".$lectura." , resultadoevaluacion,lecant,factor from lecturas.comlec_ordenlecturas where suministro = '$suministro'";
		return pg_query($sql);				
	}
	
	public function metodoEnvioLecturasToDistriluz($url, $parameters){

		$ch = curl_init(); 

		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); 
		curl_setopt($ch, CURLOPT_HEADER, false);
		curl_setopt($ch, CURLOPT_POST, count($parameters));
		curl_setopt($ch, CURLOPT_POSTFIELDS, $parameters); 

		$out = curl_exec($ch);

		curl_close($ch);			
		$array_data = json_decode(json_encode(simplexml_load_string($out)), true);
		return $array_data;
	}
	
	public function enviarLecturasConsistentesToDistriluz($negocio){
		set_time_limit(0);
		ini_set('max_execution_time', 30000);
		date_default_timezone_set("America/Lima");
		
		if(!isset($negocio)){
			return false;
		}
		
		if($negocio=='cix'){
			$url_lecturas = "http://oficinavirtual.distriluz.com.pe:62150/wsensa/facturacion/lecturas/TomaLectura.aspx";
			$url_fotos = "http://oficinavirtual.distriluz.com.pe:62150/wsensa/facturacion/lecturas/TomaFoto.aspx";
			$email_to = array('jvasquezv@gescom.com.pe'=>'Joel');
			$cnx = loadModel('database','cix');
			$domain_server = 'pexport.net';
		}elseif($negocio=='trux'){
			$url_lecturas = "http://oficinavirtual.distriluz.com.pe:62150/wshdna/facturacion/lecturas/TomaLectura.aspx";
			$url_fotos = "http://oficinavirtual.distriluz.com.pe:62150/wshdna/facturacion/lecturas/TomaFoto.aspx";
			$email_to = array('walterneyra@pexport.com.pe'=>'Walter','santosgutierrez@pexport.com.pe'=>'Santos','jenyminchola@pexport.com.pe'=>'Jeny');
			$cnx = loadModel('database','trux');
			$domain_server = '54.148.234.164';
		}elseif($negocio=='trux_valle'){
			$url_lecturas = "http://oficinavirtual.distriluz.com.pe:62150/wshdna/facturacion/lecturas/TomaLectura.aspx";
			$url_fotos = "http://oficinavirtual.distriluz.com.pe:62150/wshdna/facturacion/lecturas/TomaFoto.aspx";
			$email_to = array('walterneyra@pexport.com.pe'=>'Walter','santosgutierrez@pexport.com.pe'=>'Santos','jenyminchola@pexport.com.pe'=>'Jeny');
			$cnx = loadModel('database','trux_valle');
			$domain_server = '54.148.234.164';
		}elseif($negocio=='caj'){
			$url_lecturas = "http://oficinavirtual.distriluz.com.pe:62150/wshdna/facturacion/lecturas/TomaLectura.aspx";
			$url_fotos = "http://oficinavirtual.distriluz.com.pe:62150/wshdna/facturacion/lecturas/TomaFoto.aspx";
			$email_to = array('walterneyra@pexport.com.pe'=>'Walter','santosgutierrez@pexport.com.pe'=>'Santos','jenyminchola@pexport.com.pe'=>'Jeny');
			$cnx = loadModel('database','caj');
			$domain_server = '54.148.234.164';
		}elseif($negocio=='chimb'){
			$url_lecturas = "http://oficinavirtual.distriluz.com.pe:62150/wshdna/facturacion/lecturas/TomaLectura.aspx";
			$url_fotos = "http://oficinavirtual.distriluz.com.pe:62150/wshdna/facturacion/lecturas/TomaFoto.aspx";
			$email_to = array('rcampanaf@gescom.com.pe'=>'rcampanaf','jmincholam@gescom.com.pe'=>'Jeny');
			$cnx = loadModel('database','chimb');
			$domain_server = '54.148.234.164';
		}elseif($negocio=='huar'){
			$url_lecturas = "http://oficinavirtual.distriluz.com.pe:62150/wshdna/facturacion/lecturas/TomaLectura.aspx";
			$url_fotos = "http://oficinavirtual.distriluz.com.pe:62150/wshdna/facturacion/lecturas/TomaFoto.aspx";
			$email_to = array('hjamancav@gescom.com.pe'=>'hjamancav','javieralvino1@hotmail.com'=>'Javier Alvino','jmincholam@gescom.com.pe'=>'Jeny');
			$cnx = loadModel('database','huar');
			$domain_server = '54.148.234.164';
		}
		
		$str_xml_ids = '';
		$db = $cnx->getConnection();
		try {
			$sql_distinct_xml = "select comlec_xml_id, count(ws_l_estado) as pendientes from lecturas.comlec_ordenlecturas ol inner join lecturas.comlec_xml ox on ol.comlec_xml_id=ox.id and ox.eliminado = false and ox.fechaexportacion is null
WHERE procesando = false and resultadoevaluacion = 'CONSISTENTE' and ws_l_estado='0' and subestacion=false  
group by comlec_xml_id";
			$arr_distinct_xml = pg_query($sql_distinct_xml);
			$num_rows_distinct_xml = pg_num_rows($arr_distinct_xml);
			if($num_rows_distinct_xml > 0){
				$sum_cantidad_pendientes = 0;
				$xml_i = 0;
				while($obj_xml=pg_fetch_object($arr_distinct_xml)) {
					$xml_i++;
					$sum_cantidad_pendientes += $obj_xml->pendientes;
					if($sum_cantidad_pendientes<=500){
						$str_xml_ids .= "'".$obj_xml->comlec_xml_id."',";
					}elseif($sum_cantidad_pendientes>500 && $xml_i==1){
						$str_xml_ids .= "'".$obj_xml->comlec_xml_id."',";
						if($xml_i<=$num_rows_distinct_xml){
							exec("sh /var/www/html/cron/enviar_lecturas_consistentes_".$negocio.".sh > /dev/null 2>&1 &");
						}
						break;
					}elseif($sum_cantidad_pendientes>500 && $xml_i>1){
						if($xml_i<=$num_rows_distinct_xml){
							exec("sh /var/www/html/cron/enviar_lecturas_consistentes_".$negocio.".sh > /dev/null 2>&1 &");
						}
						break;
					}
				}
				$str_xml_ids = substr($str_xml_ids, 0, -1);
			
				$sql_update_xml = "UPDATE lecturas.comlec_xml SET procesando = true WHERE id in (".$str_xml_ids.")";
				pg_query($sql_update_xml);
				
				$sql = "SELECT fechaejecucion1,suministro,lecturista1_id,ordtrabajo,lecturao1,lecant, case when gobs.codigo is not null then cast(gobs.codigo as varchar) else obs1 end as obs1, 
						montoconsumo1, latitud1, longitud1,validacion, foto1, ws_l_estado, ws_l_pidefoto, cobs.comlec_condicionobs_id, consant, metodofinal1,
						ol.glomas_unidadnegocio_id, gun.descripcion as glomas_unidadnegocio
					FROM lecturas.comlec_ordenlecturas ol inner join lecturas.comlec_xml ox on ol.comlec_xml_id=ox.id and ox.eliminado = false and ox.fechaexportacion is null
					inner join lecturas.glomas_unidadnegocios gun on ol.glomas_unidadnegocio_id=gun.id
					left join lecturas.comlec_observaciones cobs on ol.obs1=cobs.codigocamp_varchar
					left join lecturas.glomas_observaciones gobs on cobs.codigofac=gobs.abreviatura
					WHERE procesando = true and resultadoevaluacion = 'CONSISTENTE' and ws_l_estado='0' and ox.id in (".$str_xml_ids.")";
				
				$arr_obj_orden = pg_query($sql);
				$num_rows = pg_num_rows($arr_obj_orden);
				if($num_rows > 0){
					while($obj_orden=pg_fetch_object($arr_obj_orden)) {
						if($obj_orden->ws_l_estado == 0){
							$consumo_tmp = (int) $obj_orden->lecturao1 - (int) $obj_orden->lecant;
							if($obj_orden->comlec_condicionobs_id==1 && trim($obj_orden->lecturao1)==''){

								$sql_update_suministro = "UPDATE lecturas.comlec_ordenlecturas SET resultadoevaluacion = 'INCONSISTENTE' WHERE suministro='".$obj_orden->suministro."' and comlec_xml_id in (".$str_xml_ids.")";
								pg_query($sql_update_suministro);
								error_log(date('Y-m-d H:i:s')." Actualizacion de suministro (No cumple condicion 1 de obs): ".$obj_orden->suministro." a INCONSISTENTE \n", 3, 'correccionWSDistriluz-'.$this->negocio.'-'.date('Y-m-d').'.log');								
							}elseif($obj_orden->comlec_condicionobs_id==2 && trim($obj_orden->lecturao1)!=''){

								$sql_update_suministro = "UPDATE lecturas.comlec_ordenlecturas SET resultadoevaluacion = 'INCONSISTENTE' WHERE suministro='".$obj_orden->suministro."' and comlec_xml_id in (".$str_xml_ids.")";
								pg_query($sql_update_suministro);
								error_log(date('Y-m-d H:i:s')." Actualizacion de suministro (No cumple condicion 2 de obs): ".$obj_orden->suministro." a INCONSISTENTE \n", 3, 'correccionWSDistriluz-'.$this->negocio.'-'.date('Y-m-d').'.log');
							}elseif(trim($obj_orden->lecturao1)=='' && $obj_orden->obs1=='' && $obj_orden->montoconsumo1==''){

								$sql_update_suministro = "UPDATE lecturas.comlec_ordenlecturas SET resultadoevaluacion = 'INCONSISTENTE' WHERE suministro='".$obj_orden->suministro."' and comlec_xml_id in (".$str_xml_ids.")";
								pg_query($sql_update_suministro);
								error_log(date('Y-m-d H:i:s')." Actualizacion de suministro (Sin monto consumo, sin lectura y sin obs):".$obj_orden->suministro." a INCONSISTENTE \n", 3, 'correccionWSDistriluz-'.$this->negocio.'-'.date('Y-m-d').'.log');
							}elseif(trim($obj_orden->lecturao1)=='' && $obj_orden->obs1=='' && $obj_orden->montoconsumo1>=0){

								$sql_update_suministro = "UPDATE lecturas.comlec_ordenlecturas SET resultadoevaluacion = 'INCONSISTENTE' WHERE suministro='".$obj_orden->suministro."' and comlec_xml_id in (".$str_xml_ids.")";
								pg_query($sql_update_suministro);
								error_log(date('Y-m-d H:i:s')." Actualizacion de suministro (Monto consumo sin lectura ni obs): ".$obj_orden->suministro." a INCONSISTENTE \n", 3, 'correccionWSDistriluz-'.$this->negocio.'-'.date('Y-m-d').'.log');
							}elseif(trim($obj_orden->lecturao1)!='' && $obj_orden->montoconsumo1==''){

								$sql_update_suministro = "UPDATE lecturas.comlec_ordenlecturas SET resultadoevaluacion = 'INCONSISTENTE' WHERE suministro='".$obj_orden->suministro."' and comlec_xml_id in (".$str_xml_ids.")";
								pg_query($sql_update_suministro);
								error_log(date('Y-m-d H:i:s')." Actualizacion de suministro (Con lectura y sin monto consumo): ".$obj_orden->suministro." a INCONSISTENTE \n", 3, 'correccionWSDistriluz-'.$this->negocio.'-'.date('Y-m-d').'.log');
							}elseif(($consumo_tmp=='-1' || $consumo_tmp=='-2' || $consumo_tmp=='-3') && $obj_orden->validacion=='0'){

								if($negocio=='cix'){// No validar para Gescom
									$newlecturao = $obj_orden->lecant;
									$lecturao = $obj_orden->lecturao1;
									$sql_update_suministro = "UPDATE lecturas.comlec_ordenlecturas SET lecturao1 = '".$newlecturao."', lecturaoold = '".$lecturao."', montoconsumo1=0 WHERE suministro='".$obj_orden->suministro."' and comlec_xml_id in (".$str_xml_ids.")";
									pg_query($sql_update_suministro);
									error_log(date('Y-m-d H:i:s')." Actualizacion de suministro (Monto consumo -1, -2, -3): ".$obj_orden->suministro." -> lecturao1 = lecant \n", 3, 'correccionWSDistriluz-'.$this->negocio.'-'.date('Y-m-d').'.log');
								}
							}elseif($consumo_tmp > ($obj_orden->consant*3) && $obj_orden->validacion<='1' && $obj_orden->consant>150 && $obj_orden->metodofinal1==2){

								$sql_update_suministro = "UPDATE lecturas.comlec_ordenlecturas SET resultadoevaluacion = 'INCONSISTENTE' WHERE suministro='".$obj_orden->suministro."' and comlec_xml_id in (".$str_xml_ids.")";
								pg_query($sql_update_suministro);
								error_log(date('Y-m-d H:i:s')." Actualizacion de suministro (Monto consumo > 200% que el anterior): ".$obj_orden->suministro." a INCONSISTENTE \n", 3, 'correccionWSDistriluz-'.$this->negocio.'-'.date('Y-m-d').'.log');
								$email = loadModel('SendEmail','null');
								$email->to = $email_to;
								$email->cc = array('gmontenegro@gescom.com.pe'=>'Geynen','jvasquezv@gescom.com.pe'=>'Joel','jsanchezh@gescom.com.pe'=>'Jonatan');
								$email->subject = 'SIGOF ('.$obj_orden->glomas_unidadnegocio.'): Suministro con datos inusuales';
								$email->body = strtr ( file_get_contents ( '../v2/template_email_consumo_mayor_200_porciento.html' ), array (
										'%date%' => date('Y-m-d'),
										'%suministro%' => $obj_orden->suministro,
										'%cons_actual%' => $consumo_tmp,
										'%cons_anterior%' => $obj_orden->consant,
										'%cons_anterior_200%' => ($obj_orden->consant*3),
										'%negocio%' => $obj_orden->glomas_unidadnegocio,
										'%dominio%' => $domain_server
								) );
								
								$email->send();
								
							}else{
								$validacion = $obj_orden->validacion;			
								if($validacion >= 1){
									$est = 0; 
								}else{
									$est = 1;
								}
								
								if(trim($obj_orden->lecturao1)!=''){
									$lecturao_envio = (int) $obj_orden->lecturao1;
								}else{
									$lecturao_envio = '';
								}
								$fecha_ejecucion = date('d/m/Y H:i:s', strtotime($obj_orden->fechaejecucion1));
								$parameters_lecturas = '<?xml version="1.0" encoding="iso-8859-1"?><l><u c="'.$obj_orden->lecturista1_id.'" id="1"><d ot="'.$obj_orden->ordtrabajo.'" s="'.$obj_orden->suministro.'" l="'.$lecturao_envio.'" o1="'.$obj_orden->obs1.'" f="'.$fecha_ejecucion.'" la="'.($obj_orden->latitud1 * 6000000).'" lo="'.($obj_orden->longitud1 * 6000000).'" est="'.$est.'" /></u></l>';
		
								$resultado_ws_lecturas = $this->metodoEnvioLecturasToDistriluz($url_lecturas, $parameters_lecturas);
								
								$suministro = $obj_orden->suministro;
								$fecha_envio = date("Y-m-d H:i:s");
								$cadena_envio = $parameters_lecturas;
								$xml_respuesta = json_encode($resultado_ws_lecturas);
								
								$pidefoto = $resultado_ws_lecturas['@attributes']['ef'];
								if($resultado_ws_lecturas['@attributes']['i']=='0'){
									$estado = '1';
								}else{
									$estado = '0';
								}
								
								$foto1 = $obj_orden->foto1;
								$ordtrabajo = $obj_orden->ordtrabajo;
								$ordtrabajo_pidefoto = $obj_orden->ws_l_pidefoto;
								if($ordtrabajo_pidefoto > 0){
									$pidefoto_save = $ordtrabajo_pidefoto;
								}else{
									$pidefoto_save = $pidefoto;
								}
								
								$sql_log = "UPDATE lecturas.comlec_ordenlecturas SET ws_l_fechaenvio = '$fecha_envio', ws_l_cadenaenvio = '$cadena_envio', ws_l_estado = '$estado', ws_l_xml = '$xml_respuesta', ws_l_pidefoto = '$pidefoto_save' WHERE suministro = '$suministro'";
								pg_query($sql_log);
								
								if($pidefoto > 0 || $ordtrabajo_pidefoto > 0){
									if($foto1 != ''){
										$parameters_foto = '<?xml version="1.0" ?>
											<l>
												<u>
													<d f="'.$fecha_ejecucion.'" ot="'.$ordtrabajo.'" s="'.$suministro.'">
													  '.$this->applyWatermark($foto1,$fecha_ejecucion).'
													</d>
												</u>
											</l>
											';
									} else{
										$parameters_foto = '<?xml version="1.0" ?>
											<l>
												<u>
													<d f="'.$fecha_ejecucion.'" ot="'.$ordtrabajo.'" s="'.$suministro.'">
													  iVBORw0KGgoAAAANSUhEUgAAAZAAAAGQCAIAAAAP3aGbAAAABGdBTUEAALGPC/xhBQAAAAFzUkdCAK7OHOkAAAAgY0hSTQAAeiYAAICEAAD6AAAAgOgAAHUwAADqYAAAOpgAABdwnLpRPAAAAAZiS0dEAP8A/wD/oL2nkwAAAAlwSFlzAAAXEQAAFxEByibzPwAAe1lJREFUeNrs/dmTZclx7ot97h6xhr13TjV2VfWAbnQ3GmgQBMF5uDzzuedKujLdB0kmM41m0rNepT9Ar3rXgx6uTPdBujKdo3MI8l6QIAGChwAIEMTcDfRY3V1zVU57WEOEu+thZWZlTY1uklVA4qyftVVn7tx7r7ViRXzLw8Pdg9wdIyMjIycB/nmfwMjIyMhHZRSskZGRE8MoWCMjIyeGUbBGRkZODKNgjYyMnBhGwRoZGTkxjII1MjJyYhgFa2Rk5MQwCtbIyMiJYRSskZGRE8MoWCMjIyeGUbBGRkZODKNgjYyMnBhGwRoZGTkxjII1MjJyYhgFa2Rk5MQwCtbIyMiJYRSskZGRE8MoWCMjIyeGUbBGRkZODKNgjYyMnBhGwRoZGTkxjII1MjJyYhgFa2Rk5MQwCtbIyMiJYRSskZGRE8MoWCMjIyeGUbBGRkZODKNgjYyMnBhGwRoZGTkxjII1MjJyYhgFa2Rk5MQwCtbIyMiJYRSskZGRE8MoWCMjIyeGUbBGRkZODKNgjYyMnBhGwRoZGTkxjII1MjJyYhgFa2Rk5MQwCtbIyMiJYRSskZGRE8MoWCMjIyeGUbBGRkZODKNgjYyMnBhGwRoZGTkxjII1MjJyYhgFa2Rk5MQwCtbIyMiJYRSskZGRE8MoWCMjIyeGUbBGRkZODKNgjYyMnBhGwRoZGTkxjII1MjJyYhgFa2Rk5MQwCtbIyMiJYRSskZGRE8MoWCMjIyeGUbBGRkZODKNgjYyMnBhGwRoZGTkxjII1MjJyYhgFa2Rk5MQwCtbIyMiJYRSskZGRE8MoWCMjIyeGUbBGRkZODKNgjYyMnBhGwRoZGTkxjII1MjJyYhgFa2Rk5MQwCtbIyMiJYRSskZGRE8MoWCMjIyeGUbBGRkZODKNgjYyMnBhGwRoZGTkxjII1MjJyYhgFa2Rk5MQwCtbIyMiJYRSskZGRE8MoWCMjIyeGUbBGRkZODKNgjYyMnBhGwRoZGTkxjII1MjJyYhgFa2Rk5MQwCtbIyMiJYRSskZGRE8MoWCMjIyeGUbBGRkZODKNgjYyMnBhGwRoZGTkxjII1MjJyYhgFa2Rk5MQwCtbIyMiJYRSskZGRE8MoWCMjIyeGUbBGRkZODKNgjYyMnBhGwRoZGTkxjII1MjJyYhgFa2Rk5MQwCtbIyMiJYRSskZGRE8MoWCMjIyeGUbBGRkZODKNgjYyMnBhGwRoZGTkxjII1MjJyYhgFa2Rk5MQwCtbIyMiJYRSskZGRE8MoWCMjIyeGUbBGRkZODKNgjYyMnBhGwRoZGTkxjII1MjJyYhgFa2Rk5MQwCtbIyMiJYRSskZGRE8MoWCMjIyeGUbBGRkZODKNgjYyMnBhGwRoZGTkxjII1MjJyYhgFa2Rk5MQwCtbIyMiJYRSskZGRE8MoWCMjIyeGUbBGRkZODKNgjYyMnBhGwRoZGTkxjII1MjJyYhgFa2Rk5MQwCtbIyMiJYRSskZGRE8MoWCMjIyeGUbBGRkZODKNgjYyMnBhGwRoZGTkxjII1MjJyYhgFa2Rk5MQwCtbIyMiJYRSskZGRE8MoWCMjIyeGUbBGRkZODKNgjYyMnBhGwRoZGTkxjII1MjJyYhgFa2Rk5MQwCtbIyMiJYRSskZGRE8MoWCMjIyeGUbBGRkZODKNgjYyMnBhGwRoZGTkxjII1MjJyYnj8gmVAAhxw9G2CA9rDu5/3hY+MjPz9aYFFtmSAA8O/sKzt4z4uufvjPUJvCAwC6O6lqWup+rivbWRk5HHBDB/GNYPMzIihsIDisR42PP4ryyCGKoRAlM2JCyfJQR7/oUdGRh4LIQECMLKpEBk8gCxlxMcrWI/fwnK4gQhwAwAwCDAgNI/3uCMjI4+NDrUD2eGWawkBgA3j+vEe97EL1j6Q3GsiwElzLTGnLkhwHi2skZGTCiGbK9PwG8Nj7sEEfrwG1uMXrD5fDaFylEBQTYUIugaR4ZuP98pGRkYeH7aCOKAA+vmqWD+jFDJQPubDPv4pYXsHMtlfeLE16QEADMQn4jwbGRl5TFwFKqDbT5fWo3crjhWYO0P5mOMOHrtg7QHXevxv/8//98tdXJVrjabJJKRmbnH98V7ZyMjIYyNQKOa3Pz3T/+b/8r+/FBF6BzJEII9XsR67oTNF/woXPyzOMjHYC5GUM8tZeHrEJ36xYllJnYSJCEwA1N0JANgeHpZh9PM+45GRx0+PQqaT72H9UkTQ5EVUxGCP/bg/t5mZyKOc7r9YgsVCZmZm7iAiEJHjsc+jR0ZGHsaTE6xhpBMRQEQnxg45OlUGETMR+YFcPfwSxrXPkf8UsJ/TEH5CgjUMeyLCwf/JHmmk/GJFwCfLRCRCfFet3M3sF8sQHBl5otAhT/i4T3RKOBhZR1f7hC/174e7DzNBd9eU3Z0BZvYTcv4jI4+J+4bwk/GSPNEpIY6mhKBH+7Aev+PuY512EDNzVTeDu4AYFOD6iCkhjd6tkf8EID6cMz1ZnohgHV7U0ZTwCanxP8q5C8PM3WOMk1hGCa6qKff+8KnraHeN/KfAz2uS9NgFix42hIlI7VGW1C+WhQUnVRWiSVWf2tisY9GumuVyOW9XP+8zGxn5xeAJKtdjFywGwcAOIhJhD2xuZEay5nf2/M4+9b0JqRCDosvK7zmlowACkYe//uCvw8/Hp5zH/8rMD/+GfiKszK15A8tOQhQIsWmaYEW3vvmbL9H/8elmsun/r6vhGx+sc/vw5G3+BfNt+bFFjHsumR9+nuz/aa1zGj28fYZ+Mrxy9Lq7B4oP73vy8FkDPWJ15qT4cB+FI0cKTq0AyI4iEeITmF88oSnh0ZqCHxqThTdNs+i29/KidSIwAAPxowID0seMz0qPWoU86ij3vSHUsAxSkGHIkVKCB6TVmdP18xcn/+LVzc+9vD4PVT1X2M6TaLd/DI4E+j78UX3rF2uR9rHzqPY5EpTjagUAdo/WHKzJHPp0HsR/wSYMJ50n5HRnZgYfznuJiEpOKafUatFXFspM5OjhWuR77vBR5xhmkB/xuUREdmzKeV8PO3zxnp7a9/twIBQopsiKVYs+Af2nXjr9L37twu//0xd+9/n4/OL6Nz+4ffs6ru+tKDziTH7B3HOPbLGTcfqPnUe1z/D6kR496j13//qo1ZaTbUh9GL/MYQ10DICYOYNS9m6lWBKX5FFADNc+8dFn7vsG3Nu9hp/1WOXSe/8aHvr+4/b88b+WLMnZUEAL9CuYbm0WF89s/o/+2bP/9A9f+ZXPnb20fw1//p3b371+m1+8E8+e4oc/On/RVgnNHn69j4qDY/7lHWEP41Htc79hdchxg+x4D32UT/aXtT2Jji2dER086dwftz/rsQuWucl9t58AwMIEZc1xz3RlbQ8jsMENVOPwrce/x+nQp3D/nO5+79Xx99/7JQ805XGfRatuhi4DC2B15nTxn/3m87/366/+/uf44qfO9sCNv31z8u/+Sn96J/36KbzySSx2H3fT/SPy4JPwkc/GXzDBfTI82BpDX3rQiCDQR/yGwy/6eV/b4+GXNnD0uIiYmQHmGZD9ZXt2rXzp1admbUOeE5NRcEU6dK4/6hGHh1lJD775oR8fLPyH/mmayZyM4EiF4MVnN//1Hz73z/7guYvryYF3f/xO/tJ/rP7jd3ju/GqHyYSWew+93l+05+mw+PDgJT/K5wL7JR1hjyA8qn0Oh+L9gnXYPPd9hB9VpeCXtD1/ecMaDqdybu5mSdU8mVNF+hu/8sx/8dLFT665+CqruU+hRaLl0dLMAA76xENfPMCPMfw6+LAe8VUPez31IjFwEIPnZnMmL79Qf2KSAME7706/+O/ly388ufmWrD8d6oAYHnmzfsHyogeBPkgqOtYI97nw7r7/l9Uk+NjtQw91Yw0/Dh3ynn74iFWMRz0Xfjny538JLSwmProwM1NTtWxGRZ4/feql3/3Vp17dSAFzNbBvEhh8OCU0g/vBfwAYw8/3aZOq+gMMxwLgH5k+hijlWrFec019dstVVcGa5Z1q+oPXqy//cfH9r8GRNkTFMd/7+7XGk8fMjov4Ufs8aiDJCV9u/7g8sn0OnVX3OU9F5MEnJQB/pA/rlzbp9JfTwlJYYLiYEHIy4kIouhXtxPZWOzWtAqRbhsSTpH3JRr7AA0IDoDfFQ22llIcDHenU8ENKM2ak3BA5mJhCjFXb9pb2wVEN2dxd4dmtZ1ihJdXk56e2saGMigVGaWdn+o2/Wvw//2v+wRvrWmc0eXq6pYpoEnz+0Ou9+0Amcnc7+JHY3KBUcAZlUEFBHNSrFQ8P48g5E4NBTA7NcBU4M6uX5u7MzqJwVSVyEZE+42FuY7cGcTrvnOFF7rt2lcJUi5kvbyBOkEVSOyMto2i1hckmNzeHNO+j1h5sDbOPtz7/KAvOJROJG5tiWDcGuzOKnNTRuxgCM0d39p5NTQJ+llvgo53PPbpz1IsQu2xVn4C+L7Tv+35lhVZryD3Eoa1YXqMQOOQ60to0LveTudZbrdTezaW72SXvi61pt+g45pxgqRIXCx5PU30m2LWyLNu2JfMQgpsxs6o+yWDLx4EDDBJhMwhzRjIgPP6LetL1sNzV3R3GufIUuoWhZiTn6JQ0a+oxxcOEKbs9qGLu7lkPv/kemOZknFIiDiGEWMUiStdmy0g5qQtLAUTVxFLEglFyFeuNciZStARA0SxkZ3vxzW/c+snr4daNWW4cMDMiYn5kuw1P1KOIMz+cYXU06VOTuza5mRORRKcI7pqH7ylLbkfCAYA5DD+vS0ccQAyYGDGBNFPu6YHq/8M0JxAT6FSBCxv1M+uzEmocO4+BagoTco5I7P2t/eVbu/2tblse+JL7fvgHktWFjQ3krpkMrp6zWzJ1EmKQHEzPiIhFHiWTf4/zefhHkgbOa1V14czk2bVQiTaZOi5ZGRHCVpAj2fWd5dv7i51+7iwMQ1pJ116c0aefOVvGsPCyzmspxkReRswC7y/zmzfSe/u7RnYo+veeyQkXrJ8XTyDS/QByOAwOAsy1AFchCjM8O7XuFsgESQfBgjscNPgJhvWaYffoY/+5O/zIdTC8aH4wjTRq3QUsZVnX9XRjbVYEtr7bafdYWCDmruoAQoxFXfK5yZrVEmooKCDCcf1G/rsf7H/jG3b57ZBWcuBFO6g++qgBw/cvKgGAu6ujFC6HqQcHUwrwUiD6iAhpzUEKELI7SCQW6pSzErdOMAggzCKOIBaAluRB7x4RQSlCz8yK3//k2X/yylPPbkZhXiVHFkMgoqqgVWq+9e61L71+ZXVzpcvDDx6e+UHE78e2aB71BzEHAwEQmBGcJBlCZOah8bK7w9mJjWUIwHvUKt7HOZ+7yntkBbu7kEyL8Mzm9Hc+ee4PXzx7YU26nJoM1tpFJVgZZH9p33jr+pffeP8n+6vV3AKBsk68+/WLT/0v/+ClZ7aqZYfUIQllyXVAFYu3byz/7bfeubV7tT0SXz7uCPtFi3752AxhDb+EPixgGF4EgHFQlCUwUt6Lxbl6WqBIZVEqiE2Yytr0wGl17N8HXU7HXQ84Nh88ckM0cqZte+4sFDHGWFUTFiqrWWH7MHiWtu1MUz2tNramoQzldGOmJTKywQE0c/zox8s/+lJ48831/UUJSIQnkDuR2Md5wg9n+9yaP3/+7OlKC4ZKmRUEL1zzo0r+5Y5jUONVn3tz49h2eblaXV/q9v5qv2k4TsuyKondySlSfFSAaAwc14vw3Nbkcxc3XlgDwboeoQhZwYTAWPl0Z1H/bV1WIa+ov/vRw2nU32NJ6FHvl1h41xdks5IjQUQQRZ2UEAmMnHNuk7aqCSET0b3+7w//8o94Skf6S0SeiQNtFPLC1vRzl9YuVAYUrXEgzgbmLlLc7/n2zuy79aRc2px7guTUc+pOlfGzT20+PXG4wCkzEmlAzwgTrP9FSU2aG9UHoj/EMx86yOyEO92PbspRDNaT4QlYWAcDkpmZORKMDA6m0GW5tUdn6rpL3DqJYQLJvjjcGuPgXwDuQ4AfHXnh3fnQoLjbld3J7GC5p6UKWSaFzCZVlEBMDuyuaDdt5LYR1ToU6+v15qmNtY01JypREjECSCB5hdd/0n/la+nLX8Xta3EIHVMogGwMp2CPjrs5FNC7Mfqmql84K//yC5deOVdXyBqqzoVJKbfED98YiQki0mffX3VNl7ps+6tusVh86f3p5as3b/gcUTjGPlniKlQl8hL3mg8HR+cAksBcMAXP5AS11CtzgBmTESEkxOxiIXgJHAjW0Vf9PQWLH96LHezQjSo8f3r99CSWRRCRDO+NIllkVdX9Jl1b5Kvzfqd1HBMs3Lt+9/fjPgtLEYb6jAEqWeGec0oaipKhS88tlZOYy9IhFigHZzU3p7L3rCamDHdve/IIScxJrOVyrSRxkT6Ukg+Uke9V/1+CCeEvbRwWAUIcCB6YQUTsFFjOvvdB+9/+0Q/OsK2QegIbl+7tMa/FPcsxen+YwvC64f4lm4G02n7lE+f+8AsvPHthPQiccHXbv/6Dy3/53etI85ef2fy9L7x08cJTGxtTDlAHZcAdgcSSvPP24s++0n71r+TKO0YWgAy4wQE2F3JD/pC7eN9sZbCwThX9q09Nf+1cCW3B0lPBMIHDHy5YDiUKDmp76ZMms7aPTVusX1i/s3vp1nx1a5HevHrn9Q9uzrNQEUT1vrEwnEbnPiVwEJEhlzNAiAssrWOIEwlRhhNi4CLcm0n4D7GwHvXYbVNfpPbs2a3f+tTTn3pqYy0yC9y91xBJC7HsdmPef//q/tffur272DsyhY6f1d9jnBzX8eMWFkRIJEQWEQUASog5hJVDwUJSuhizSAgs7CBTEFGMbDWqqUUCkxdF31hLJLGAa+mRmF0qKirS/uC4fk9LnnTJuifS/QnyBKaEPNwqYTEy5kGvpKf42tvXX3/7iu4tWwltFHYU2ZM8/JTue2AfiYLing59pGUz3vuf/Mvyd361CoGBTj1cub34+vff/X/827fObTj/YfHbv1GV04kEwCEACBZA1tHVK/6Vrzd/8hft6z+eoMs8MdPMpCBWLSgKA5Q+xIdlR3mPRHaY/LrKwRDhjqxg67WLBQHq+eG7B/VqzCbE4l4HqiEb5dRnk6culcnqRYe3bu3/Vd3ail67uVjsp7WyIqLBjD0akIP7xNmJlUnJM7Iagnsg8aHLGUjhFCiwwzr8Iz05H+WlcWdo3qjk05c2f/uF9XWBHMSrgB2RoYT3596q/OTKvqfkcjcs5viU8O/twzr+q7vnnNUYlgLlSApiZojAM1iiEJxYAYgLt4RFaQoOCmRL6lmBPneu1kvVMqII1Epny9C215wKkeNn++D09oTy97W8/6E8/sDRw/tCRCIEFhKQBY26zMv97X3sG6YlrIB5k/I9seLHbyrJhx2A7v/U8y9/4ennP3/h6bPw7F1jZe1hsrL67Aufe/5i+PQXXnzmhUtllR0tUSREFc4g7pb80zfar/+t/uA1Ws2zYK6Z4L36BKFCDKBABH9kTYNBNczssG8e3NKm2GpC0QKVTD2E1tA7CiZHfPgXFbDD0gnkYCAKiFCmBQinC9p8Kq6Vz81ObW6+cfP1a3vb2+3RAuU9Q4IMMCYtxCpRCiQuNcNFHOwOAnHgogxF9Mjp+IX8Q6aEjyJGocYL5PWAU4IaCdrCDbQGb2EWSU7FuFFQObh8Dm/7fb7/j3s+xy21418SYwiBy0BloCoC3kUOGaEU4DDBnQQhelHkMnTBwEytd8i9WxcDomRHar3OcMCFGITgCKQVKyMct+z+0Vdd/1Pj8U8JuScUpSupMVlBMrdUaCx5FUJJ5em+dp3WfXQkK0V6uoOHraYflem4b9ajwdAhlJvoBa7a71Wl/uqvfup/8YflP/2D9a0JFKBqIyvWqu43X1l/ar357d998Td+9amzZRM8wCYQdNA5+AyAH7/WfvGL+hd/Nrl9lVDsKc24qy0EQCSZUsvLjIZ1w2R51OMG629Qzkwdg5khRAZmNzB5lJJXVdeGVCKE/aRFyNHIksDgAhMwUADkntCbuKK0nNgtMpFzNiRjYomYuUMc69T+5vnq1fPrn1qjL/717T/eL5dEWhUT8bDc5ZxzOV1yMW27ChALixWaVMJj15sxlk61dxVaEun7OF/1fatF8kaqMnhNJt67qYMSFQ5Z613ZlHtj9aHuBoSdEs/UkpmReyBmAbm5e08qoUgpFQiSYGY+LZeeg7kTt5BOU8qpJO1sunQuFbGYiGRP1C/N5r31nZTEapUA3qumKEKBzIyJeyMOYiymStqLq7AFhhKbsjm7FAY2M3ZEQqZeDWriRMKRyZGzwM1ROLpVs+hsPxeFFMFQKBYhR2tLaynGlOO8a/s2FnkDRUeEDNMpQmBZOa1Pm4CYmxlK6mgi7Og0wqCktbE4hCwzsrjBlRGJCH1PBSlDVcjqQAVTAnXJeYh6OxLo4VfJC4mlgrOBJBCRaw6M/VUznU5VNUoIzH3fByZ3lyq0bcvMIYSUkruHEABwfnigyH0W39G8tXUSkeEZbGZCTESqGikHsUSUDZGYh6xwz6DHKylPZEp4b7WGg6h3Xm9Ss2ha3e+QlwiGjM4EscBhGs49XzNYWIeZEUevouzRI6cVVh1c45TOnVl/7rnzL70wPb0xLQKE2M1gvrVWv/LSs68+v/7s09PTkQoojABWdReaAdjfTT9+c/tHr9md2xN4YCK7x1d1FFT1Ee0NfrTb4ugVZjUCswUKBRE5RSoNxkaIxWAP5GxpWBX1A5e4Qh1eGhVMrzz9TPrtcjve+N7lK9cWq7ae1KEiZABi2QpamV1v+tdv7Z25HN+axVXnrZceuEBfIgfGTk8/urb8YJF2s5aBSVO2LAEhxOSAsROvaiYCRIicHVD2TNkpSI7hcGnFsruDjAimxqTGCKCyIDM4ctKmxwaoUETjwBKZEEGFDyssZmZwNmJjyTHkEOCcQ8lMZp1hiGqS4GyWXWGDtyFUQmyWG8/qJCTDSu6wuENubqiCK8ExhLexEUDEzJ32S9crq/SD67fLSdiI4k5dMitEtC+RRGSnlx9cb66s8nZnD7E1/aDi24M+HSIiUmG4CIgcZFCWwpiIShXNyIRQhApGSZO6BRY8MCtnZiomxsEcBhAP+QicgHqtlBitb91MVYmIQzQzJBErhCR4IAoOZ2d3T/IzpqJHzr4DxTSnwAxyT+SHQ4Af0qWfDE8mcHS4KnZXd3Ind1/2aboez750KjRdCt5TppTWKbaY4AHnursTyeCdUdUhduFA+ALIOXJslimInz83e+nlC7/9Gxc+88nZ6XWJBDiTg822ZvIrLz+zQRQjgAQDEIauFhDCchvf/u7iy1/rvvtjzPc6gnMKdv/+zg9OkXjorwfLlYOWkRzcy4NOzPc2xtHNdoBFGZkcarrXUbYAdlAOycpYFEUQphAlsJi6e85icCaQg/vsscDTa9j89LnGqgmtvvb2je2+W0gxDVUkVJZapo74Zq/fvbG706+mBTUalxYDCjYVUiG0hu1lurnUjuNa5GRObMzBweYEZhKBsUItmcLV3Q3qBA9xuS8iIuI4uC+DHy2YUwaMDMhE5OrZSs+LDCj3Jr3yQdtqFo+A8sGzTQzUA8vc76ZlzBuWmUVVnRjsJCRBipK6LARiV2j27J7BiUpnKsHBzF2VTEQYxARzJxrWLr3vlkmNJDKLB21JbnT49gfbV+bLUlgRGw9igUyZlIiaTDud3mzQgORgd0oTIj40rh1+UOaN7uknANh6hkJEwck9qWdHtiwcDUiuwZ0oI2u2REW82zeOZQgRUY9yWG4ehJ0Oc92YC3OFgpkdBGYD96YpmyrEEQAzcgcZ3GE/u9zNXSUiotR3sIICkUPCsM0dMfExXfvlCms4HKICwAzZc6JEOVKcf/4zZ/7Fv/70hVIStHeltpkQFnpPRPvxeKshjSvnnPNdw6dXI0ggSX1XFOHM6en5pzY/+cKpszOUg444QBwoCUmYUMgAZ6AHBWcoQEBw4Iffuf3Hf9J95a/Xb+xmoBMQrAKOKrf7MWfcPWp1OBk8XBY4CKhjkAEM4nvty6MvOfhBIhsc2F70r1/bv7VIxMxk6JdbW1tntjbPrNVnapQCZkL2CtlATgIKptZ1iWPcFPzzl9anxcuJ+cs/ubbbeJitFcGrrAZyDtlwbWe1s7NDsBZVw1WZOefMDBExMyOQsARibcsAIDpYnQaXHBFHL3PuUu5du4SkxCpsLJYSUgLgYADMLAJmRJApu1M/GBQO9i5wtG4Vcmb1nE0zIEbucBZyImeO5kQCdzXtclpJqlTMOJu3IgQpiYKTRCkQXN08Z237nC1L8KqEuucEU4KFSBAnYoHMeSoE9symuW9zSqGosvtkVsND6/hgp79xZ9c09TxJYRo7dXcSNneFcxAnYXEBCyEwJztW5mgYvQ+1sHKGqQeCkqlbViKzrrVQWoShdzOzhjIosEhNTmYWQhi0aZgPMnPXdgyQq2vHDnBwYnPz3FEIIQRmGW5lNjWzDtnZMzRZPnjwOwCE9GGCddzTN/wrfc+AO4E5SDS4mUGY9OEBvY+bJxI4OrSFkZllz5mMUuK0eOnCc//lP3nxuQkcbiDSHHPbSn8kVYNaDYtug7lrZsOEnJndXVUtSc6q2QGeTqfra9MYparJ4ITsxsQMIMQIZIIm58gGIIE6IADVqsHtG/3Xvrb9la9OL7+7AZsLGsIQkP3gHRl0Rw4MqgPbih3HNwfiIVD20OnO96byHze5ASIuFLiy6L765u0f3dynGEtCm7fPnVldOt1dXJ89u15fqOl8TWemdQFRzVnd2J0lu4XcSZTzNf/eS1u3Fs9cvrn3+nZyaMqI5JVCgT6bkEzLSSXeOq8QJqIawDGEInYp7a+a1GeSYKocIkgOJltEqjm1TWKrBKerYhakjOBD66KMW1ldVZ0IzO6U1HLOmsKi0/1EiQhFTcSFY31STbEsaPLMZrUWOQAgFrEAIXMhUrgTBcFmHZ4/PbvVbVJTVWUQIjfnEFvjRYvekoHatpVgZ6Zxo54gWSch16WqxravnUMZ9ti2m5Q7NcgcVeGptLRZypmt6aQoqqqaz+ethr0uKyQWcSNUIcbGQsehLjOYWNhZ2pyWXZfMCERSHaSKDvN9H3r4EBpKD/YTZze4m7HzVDBdrzYnBeWiy6zBlUOhWpkLhQV817ztMHTvo2nEICJnS95cmxRQ7ZnhJCG7OFF06w1LpSZD3c1MkNfXqmfLg+WF+5JAoz4yGfu4G+uoi1YyabLtrLpl1iQFsRtZhNsD1/tkeFIVRyGOYfcJKKkYqJug40JbcfVOQzmDZmgq9SA61Izch9vG7p6zAcg5QzWEUEjh8E47hzap6btcVZPTdVlNZwCQwEPkN5kbiAdzBzBwjMBgZbEDFYAP3uu++2392tf57bcjGoVkgzDCQXX3ey/kXlvpPrUaei6D4GCQE4mTgPLdD97fIQyIABPmHf3oxvzr7++FaloH761bW+yV7y8mls5X8uJm+RufvPCFT73w3IQlFCDt1IyJWAjEZEDalPJXz6/9Zy9c4LD75t5q2SaehBLUd8lSOnd69rufvvjsVt33fecC7dmNo8S63G7y999+//UrtxrVGAuQHMXfsyXk1vo2T5q12fSVs1ufPnfq+Y3pVhlLJnfltSKbajYjVuKu18VisVy17+3Ze9e337q9vNGuOk+EvF7H586f+dxLW0WMz53bemarrsQBKEgd0aHuGW5uZQgvnJ7+kxcvPf/UVpdlGrnwXsgTFdfm/Y+u7H2wvb9MZWqbyRp/5ulzX3jqwoxlkbqVUIgsi3ZCwaritZ29b7753vVFRxRYF5OArdi/eGrtxadOXXrqzLSur127/uWfbN/YuQ0KF8+d+63nty7NiiZpa4XYigkSmYvy5rL7wbvXfnrtzsoEB4u2gxrdG6r2sE5CsTJiNi/Zz9by4lObn7l0aiaWel56zsFK1xlLjLO39ubfevPym40eedyHb1NVEfmtZ0+/+NzF9ZLSfDsIuRS9khTVlNPV/fa1q/Of3tzfT9b37ayiT1w49a+e2RKRYTpytDZKRPpom+g+wRo+VZbFOzd3vvXG9d07KyVlBiPVwj3C3S7td7/icSvJk3O6Dx4oOowVQjjTamhS1r5fNhmp0L4vPTWZcVh04cjIAmAOVU0pE9FkUkg1M7POqFusmh7uUsbCYzE8+u86jXh4vhgDhMAkcMADCDJsfHHnNr73ve5Lfxq/91o9n4NoDk2OQhkGAz90V4YjG+o+tcKhD4uGeTAOtmS5z3I+7sMiBqlFYre8aNN2kytyy85W7SVfrJapbWeib63JzZZvy8Y/v1Se25hMg7DDfJhqETn6gGD5hY3qn7709G6H97d355Y0TDK0z1m1Ob2+8bufvvj5S7WtWjPrNTBUJMfJ2gdLXS3nP7myu7Q4K6a9mroLyFPn3p2eFmcubl48Leenk0+d23rlzNZzp9Y3KyqGvkMGwMAKGNBlLFazpus/2O3fOVu9dnP5w5vzt3b29vZ3Niabn7mw+V+9XIdYTeriVAmxFq7Zy0QonRWkyCCvxJ6ZlZOnz/4qqAt5SlRaKkg6rn50e9UlXa7mjUUzq4O/cmHrX7369NmIedfOWesy0rydhXpZRnqTv/vO25r7cv3sb03SpVPTS2v88rnJM2fWnn7qzHRavFW3f3UjrS5/ECicPz35/U8//blzVW6b3qOqEhKT86S+vJfn8/lPr+11NCndcO9TB4f69dApoUmhIHKrg11YL3/t6bXff/nM6RKU41yTRhJLFaSaTb5zbf3atQ9+cqM3uyePYnDafv5c9TufuXhuhtV2VUV2jk1GMZtM0b5+c9Unvba7XGVrch+MLp6e/RevnI8xqmpKaSjieDBTkY83JQyT8uuv4aeXr6WUOi6LIhRwQiaK+Hnw+MMa4IeRUsLMBGIKItJo0bjnCC4DHK16B0UMDQ0R7TRYxIO/0N2H5bxEzMyTtVo2ZmRGAe0yoxSJFDfWfBI6UgKy50oLMJzYYQAUBPXAhAwYIIgBlgzXrucf/XDxzb859cGNqDlFqEFAYqzIghpoPspl3tcL6N4ZItkjZ/sCAzXwQjwxcgBF4sJtkkO2QqUo1k7FAjt58b1ry2W4eqaXzzx/8bnT60EIBjewEQwpBDY9NS1eeWrj3FvXgvUQQVF13PTq1qUY06k1uljBbBFAKZwW691SMTGIVEHajHmKG9m6pCQMIlct2Z956vTnX/3Uf/X0ZMJYj1iPKKK6WCJkYJI7EAlYIABFksmk8Dqen4UXzsxefrG4eGX7Kz/+yes/ubGG1aVZePlU4KoAgfvMuQczxyAEyoPUMyFHsirQZBI90KpEpX2djKTwyPtteapEydmdcs5itFH6UxOcC9gK2heAd5z7WTlZVViLydOKiNZmG/+DX5m8/My5ZzfDUzVKa6a1c0RXatiocyBzrUrfmvpTkwRuFWo8ZXV4JxNKHkr2VWfLUKxze/z28T3PqnsDB4dMbnBwRKIq0GYlF2byiXU+UzlaXTlpGbTLQVNV4eK0nFA+ekLfHUHuZrZuq2dnOFujXZM6srGsktczVPBVI6fqUMUQgrllZJsVdH4tl6WYec4IgQDk7AAKfNRt3Abzwgq8NZUCWVWzcyQhEP38Ng99AoKVwWLeAwwzskAwT7bh+2vFjLgk9Ouc1mNC0Xtu1mV66GWHmZv54H1U22VZX+a6bb1OPGUyol6trnLOeX1t/czGFovAHeCCCqQegREYGIwkB3l2I2aOMgcUKD/4SfziH8mXv1JefmeumUBFOjptZVCPdlBbhelBuQgVt+hmSAAIUAIfxjuQQ4yczBhEoq5G5rDhyhMyODoQGGrKHKDOxqAygTuSIk4KadnbELWXEpwngFmfVDP8Rks337wWqF6fnnplcwbpIJWYGNF+KTAmFpBN1/nUqfX1erpoM3cry+06JrfzirjcMq5gPtlEDiLmVAQUQKoMdSiEbMrLGbdcb8x9bblYfLLWP3hm/fc+Wf/GC/GFdTKHszgf5CUVngrAkFkqR8zO7hwZ4oChqH0t5mcnsV+V3906067f6bhbj+2k3gDMnZWDh3U15OSFEAJRRiA2Dr14nFA5zKw5MkdEwDORTWI/LauqOkU7C8hMiStIxQqWspoKeXS3OqAIDlyqz3xh/dSL0v3KZyb/089unN2qIjQIgaZGsQeomJ1r73SJy2o27fFcFQjm1XpQYXYrKvaSXE9VIUjFjArLwM7OMBcqGKLmYHW480EHYABkOvyffapeSIgCkhBiVZXTMpbwhFoKc2fxelYgw9OsFCpOmd8gOHIKnqOQuZmBOOr66SKigBVr68lRMooyJUBRVsHP1vWkqtrOVlKd0/klytFiRQECP6w7RGVhhlaAg2VrZxADDnW3QiMAD+iH8FfAtY8SFDmUxVKqPiSHUtJJVWrbhYLYPAofrGsNpaAff7HCJ5Saw8zuxAwGyInYwY2SJYQOsSimBqhMEHtT3OdxHyCZLZcqtayvUTUVR2o9d+wsdV1IPVlniQDUEmDMTGXhBAMIzmwHS5XOBHFgAoTc4e0rN7/1nfaHP47zNnxMD+KB2Tz8jIPOikN3rNPd9xx//9HbiIiPbXL9YDC0u5MBTMwsB+Xw3cxu7nXX9ttby7y+LgYEAjNkWCFwB1kkPrNeXTyzudyer1K672uP48fMwKOnerIAksL6acivPnP2X/72S59/dutMheR24GZ3wEGqcAeDqQYKAzuQDOYIcCbvEStxQEDRlK2XHIq+Dzn3MRTD3rpwBsHJMxEDTmAaas07uZo5A7CeJLoNNXUYQ1Wg1OMROAgSAAhwZhZ+85XnuSg+88ozmxNIhCAABhxseTREuODe+mt0kGPv/rBNdO7eL7+nVXnIkD906Mjd4IB7Cv48PCPncGeWoxkZHdZTe0jEgB9W3oUf3T5FNtOhuK+TIETTDiiG8XPQV5nhVpqYGVyPUrgIEIlgqA4tY0PrkTOchVIgBCZmNohZMqNCxH5OkfpPrh6WELuIMBcUIodCyivX53/61Td+GNl6TcZKicjY41G8ydCgg3LNc0fd4nMvX/y1Tz9dTIoefu3W4srN+STh9Jkt51IBBliiIwHeAQQwYE4OYgLBCI4sKED7u/jh6/jjr6ZvfFd372wSdx/BX0jHleXoAo9FNuAwgoZBIGJ3I2LQ0LPYD4JWDmXukbecDjPxyAGmKJKJPGc3v73wn95uP7XTvVBNZoUMj8jhUQlPUC8lPHtq9umnz+yn/O7NOyYf9tQbRuXxVBWlKYymyM9uFF94euNzT289VcENiWmoW87qAgoiB1veHo4pJpQBBAgc0AwBmTOIXBwRIYaKZD1KgDMRZHB+kbOQsasNTWTkRuTEAmYHFTbcSB4ePSJRCPywRMVBf5WIORrcM52r8RsvnKnr8vwGBjOjV2OYursICJrTgTocdjO6d582d6d7ReZwNfAhJ3DX/XzUGfyer7p7noevHBUAx5FcHn7Lfc+wY/n+9/XJo0K7yi58ENpXcCCQkYDv1m5zZkKGHJhWdwvLATBGIhe4gFTVQewMZ5BFtihE5MQON1cPRXjkE+Mx8wS2+XJxwIiEYuCCQhaUWqqGH7528yd//hPab9wkBVYzkItVfrfm2YED3sx2897FDfpf/ZflS889e3qrTAk3b9kbb+8/vS7TjS1zyYYwWHCgIZOHFZaN2DWSDw+NIXgEoBtXmi9+Mf/RF4sr7wugHyf47fgq4X1Ro8ffAAeIhGioosA4WgXHPR984GsPOQghIycwhcPaDo1Pf3Jr+fyNxdlTa5sFQHBTGQr6wcEe4efX4kvn1t+6uf2eucr9JVn80bs+A+i8pL57+tTkD14+9wcvnn+6ApvBuXcSAyELkTD3wEqH5AIbkj1C5EgekaaRioBhqZRMS+tq6ytCEULn3iJGGrLNh5BSgJggxiCHu7IpmBSihAzUFBXBDFHgQO9iziyR6CEFMxxuRk5gozL4xfXi4nogGJCGDPdsSUSGEEsHot51SB06j45bVQ+99Xc3Mr/vTwc/HL3ywMePVxm5R36OWViHoS53D3LPsQ417lj1SmZmd4ProE6gsErWchRw9qFIIZlDCAYUDiLcrV/NyE5mTgQTDzi8PjuKLAu9khnIIURy8BS+P67wifH4k59xUFiDmUEchEOQyBzKeHOZb765g/1EVe0xwh3JQNsAMCy1Dc2hCjOU9ImLZ2abW7EQcggwmcw21re2ToWNjY1yQoP7gAA3I0IN8/nSVh2q6JsT5SHxmmDA/nb/wx/sff3r4c3XKvR94EYtPsLe8aN0hOFyDrkvDgv3+t39MH3HiQR0kIZ8t5McBKzi3jiv4/0giLgflIVmJwcCMTOyFz+5sfvMRvHrn7gga4AndWMviDKGwgbQU6VcXIsbhTDo+CzjYbPCu/Oew3sUNS9PT+IXXrz0maenrC1IQFxjqLTgTphn+2C3e/36/vu39xYZzBIllORR81qBZ89uXjxz6uyUEAnwivKE+pK6Num7N9//1vuXKuRTlZxbK6clMznDFCAO7BBjgiWlvT7dam2vtcIWRoW7FEQk/O6d+bV5s+jzo8YMDbuXEAoQkOAZMJgRR5AQO/MQ4zokOrRHF04PqwDhh9s1HWtDHCW03zPl94PJxMFscfhav/859JGGzHGh+lmfIBBYnJ3IyUwQlPzmYvXajUVV5ZQSgKGK/BC66DwVwHLrqVmbhLNbG5NKiCnCiMgHx5aDmImhht0cbux3TW/CCOIlSAjmIP75SNYT2DVniCFgPoixc3iCF1x0UgJRUBNNg7OCSmiAy11bwB1mcIH7iy+u/fN/8uoXPn/x1CYxNVUonr24MduYbhS0viaCwTs23G9GVixu0we3bb4KZ7d8+pSW9UGPSjurb36z/dOv0JtvwdoMZBDFAn368As5Hid3dKMeqlY4zG7AwaL/sTAIP7AAh3JufPexfHcMHHaDwcfjw95Bw6yImS3lqzu7794uVk1PXgG9gZjICU5CANwmYpslF0yE4H5vOqT7fcbg8Ssahq5qLqBbk1ARkFt4kSTGcHBF5n5zb/U3b934sx+896P3t7cR67quipr6Hl17uo6fevbiJ56Wf/7C9PmtYiqWhKUsYlXOu9Xr77/fdv0U6ZVz099+6dInylkJY0vmCRKYIEJA7Hq/fGf+/Su779zed+sk1I5QkhdiNxern95qbjR6lKp1914QCBTI1ckIanBNAY4gLkVvbsZtRpCQElhQMXqqzHaPBOve230gGkepKodv8+Nact9wPZjPHZrIR+bS8XY+9oPf+zE8KAH3q8LhjPTgiw/7vBNYQEAIhZFf3d770o9SURR93wNgHrIa2N1TqMl66pcbQZ9/aoPLUFXrJSCDs1kVEDNIRAIWSf/2SvPDK3t7nRVFFPGKKFDQx5zh/CE8kQPfTVU2wAAD5bZdAQRmIJktYT1oAp/C1I8ynIcgKmaE8Oqzk9/7wrMvPrs1jSuYk+XNSuoyDOFUB2rlQ509tsWKf/yD5vJ17TzSp+jCaSpLA8OBnWvpr75BX/l2df1mC7SAZa8+8jLtPZ3vqL8BOHJmDe4GpmHqRURMdNdDST/jy4/+ZQdAx5cgeTA5+67XvjPA6MB9HAtyGEHBESD3SmRWhgh2BPeE+6YVRxeC40PmcCJqPSODsggBhlBAJgcDyg0Md563+YPt7s1tfWs/9tSXvcRIKSXP/a3etz+4/ZP9dntv7dULaxdm8v683e45xbWupyv7+c3V7lperLpTz1w4f+ksymGgGfohKdcNCK3atX39/ge73718syeSMrFLib5E3+b+Rk8LmZV0f+mbwwD0RBAicQJR5YQEXmZc3Vk1qZu36hK7Lk2qeHZrutvHwSc9FCQ4pib357ffcyi/e7PuvmHYiO7oV3/wgw+bRdLhALl7FQdx8zh8bP1M+8zMiFwAdqtiQWK3dudf2+9ijF3XEdFQsGFwsDTYldydKu2zF04V5eYkTGpHQaaeBeIOIjJCBhrHuzdv/cVr+99759Z+59O6FO8ZJlwkRCF98vNBPAnBsqMKRHef4QyOPJtU5WSWIH2Y5lZX4nXI00y7ByNnmA0xF0VRFMUnL5x++emzGwVgEUboSHsYwHWXswYRQcDgoWrSYncur/947/JN4Wp26cLBiSg8o/3pj5ff/u7a65eDt11ACMQtlwg99GNdFh9zmh9fK8RRRCg98IZ7Be6hHPXaIy+eu+thbR0mCq4xxrKuQgg4sO+C+RBFMdRoJ7iVIQzLOo880LGzOG7cuRvImQFhBQczBKQhEsHzkAOrFI0n5drFTaepXYUEA2cqmNci2Sp3u9fee/dKfOu5zU+eLhaJ3tujpYYk02Vub3jYXfmzK50r5QN7MwRG69nNB5dda7zMdLvBtbkuiiKaCFGRvbI+W2rCxMuKUvuw+SABysRDciM5Z/Nbe83lmztv31ntr9qdRde5dF23tb72wjMXutXiqJ2PbyD4ELvp7rPqnsqlR8c9EJf7KowcFXK4p5EfctePO9rvmWk+JDXsroWFIT/XdHgnE4UgRGGxXN2kEIL2vTFzCJ4zhmq/83ZVWVo/t37m3DPPPX3+7DoKA7KaZ4nhII9NsAJ2lt271658/53+reu7Taxn05KTkimY4QcB1cdP78lUJHz8TnfOnGMsCzNTRWr6rkZudWnFrz2D/8OvP73padFCy4l5w3lf7Wn3IU1Q1RKRTyaT6XTy+5+Znol3op8GRTB8AhDYLFNZR5D2EFkBFYED+nd+cvov/rJ840b7hVdW/NnJosVsWtEO/ugv6v/b/3Xxxjt3ws4kUcji2Z2owYdU3QgEECkcBEKK4giUHlyoOghlONxR02EHssVKsFoiHSv9UFBExsFBnYWsZi05F5KZxck4FDiUNjkq0QPq2NiZLWcs4DVSXYo4gzAnWnMnZIC6CXUc0pLzzKkNipAlN0Fb0ppIuoAypxCiuRKs7D2ocgj71oV4MWOvqDlQG7wGl3AQoaA5WKBlBF5en1av+ufP3L6+u3e1u3Rrb35r0d1Y5vd392/3SKEy3lrz9o19vt2r9J1mP514nkV5o+q2jTixiK0mVoACEEHFjLIROQkRKl4FLJR0FcrQrwoycqh7JwXHqnSPzXJViCcAztoXQ8M6iTisJkKlCRmX79j/+839H97puOt3Pfe9ta2m1AKI+4tv3H6/KIqr2/2l9bV2ta2y1gYwHL0iMPoeRWGWmUh694AeJgaAhbiklLSLrgUBzhlUaidgZQIE8KbvYhFCJIMSqRCiu5Dx4OtiACmQqKFlEDw6BGwhYDBs2Q+Wkx1gh1BBfZkUwVZENQgGMisACIpQwkuiAG8MIBFFDUpd3xQIkrVfNRwIReg0O4eJtC+fld95Nn5q0w82iQoRFjNBMS89k6/d2g//3x/u/Hdv+Ls7u1wXU3bqVyByLuEevQEQGFngDnI7cG/4Y7e5noDT/W4Spppa1pTcEs2K9uVnTv3r3/rEp07POuMklWsrurQcj3KpUu5zzmVZTqdTDlTFOMxi3DH4hAJzSpkkAMUQkMlJ8eab9Hffe+db366ur+rz56rMqItCc/c335t/+Sv+xjuLW9uUlCmIkx2kcX7UVcIHn4Ef9s6DpcLh61kh7lCgA9y8NJBTm1Gzq3Gnlh2mzAcBGvcf9OCGFfFgpToWCAUUrUIJwWMediADC8Vee4JECZyTUBCJYFEJoKBm+46zFBsDeagIHhhDTCz5jGhlGVnVKJOI5p5syaxes7NARFDO8OxstnFutkq4tqfXbt1578b2+3d2NmO/21nv3ljb0GSxms/nfRUkxphBHXomi4GhHEIgDkoBFDsFOxKCOwIQaKhixUIoRNxFRMiHUirDipgTkefkau5kYIWAqR3SRgmJMCES1rdu3fjLH7zx/euLCVNZV8eLfxz5rYgrVRcwkTgFAIkoOSouEgAqhr06iIb1GjMnIzXAAXVP7nC0miZUNE6DKRyH9nQl13sK6Bqyep/hwXsuxdA4MsNMI4tphmYcuWIPnY2HSWDsJApqkhIHAnqnGQGOPmlWP1wLPniwBWaRIJAYYyzhREl7ABcK+5XnnvvNV5+5cO4UA4qgQHJEwA2BayVc2eu//tNr33rt3TfevlHN1h+0QHFvjfwnyRMrL3PPtQ0mVBF5bSaTSS46azwTpUpy8nn27EZEUrg6oRZUschhenTXcRh040DtNoSH9kNc0O2b+Muv9H/0R371vWJR1tsrzb5fon7zMv7ka/7Nv1levUVJa3DhDJhC1UjpI8XoEn62Wj3o+xj+7ZI1ne6tcqNpTsiagoI9ciHTkEWkya42rGKJPCx1fkAJ5jml1GlugP1Fu9N1HNg0kVARyol3tfhuZxlUMAUEl9hLVAqdkTHurPqrnc+NOiBKvrRe9QrNLm6F8NSTgfrO9lZpp7WQdNW2uzkbTciczQUeQpAQOEjJdqnyzbOTZzbCKp1ZdGne5/1ls70//+Ztunxl7/Zi1dTTKggFl+jCfbHy3kzV+2T7TVrAFh2ZIosxqI6hipg3fdO2ahmuQ7GtgwI+x9xMBXsVJDAppDHqV3q76Tp2d01sZybFqaqeB7nTd7dzOjudeBp8VQcFp9y9V3W3qqAQolvtkC773Hx33u6lqvKcylh4f6ku20RqFphAgxXnJOyBM3lrturTrnY7K+pREMl6yKfWohnYIXA7KORwUIAkm3Ypt5o+aDN11oO9kjVa1ZNalR0qICJnDL79g0xNAXWJugzq7fp8WWCi1mTK63FyOqJLOees7k5DVqMLQARl0mxwFxHTTLD1qvz1LfmdT1589cIpdPbB7sJBGZJM1n17Wk82Nze2M/7y3etfev3y23faENYPNi85NpW9L17sCfPkvP3MDBIOFIJLjKu+XPaTRctNi6bpu+SkmixpCKqk2cwcCDGUEWumUwoguKvTodIf/C8yHJmQHDF1ePO1na/8OX3rm3W2Ctlu3dH5fqxC//a7/Z9+Td74ySSBKRTOBNcDp9dRFN2HcV9ttkfJ1jAa8ICFlYzabIsu7Xbtjlubu6AOL6eToo9elugNjsAskUPJ3B6WjT861uDUU9IiUGDLOe/1uLFsb+4vQyDzwLxar3Ivtgy03VibTdwYwhzgktXa7Pu93Vo2H+y3My9as2lp60VIuVBVMi+E3BuOvNf1b1zf2Qrh/CSk5PP53twN7mRKMGGKTBKImbdiuVGWZzamEogD+ozd/WZnZ2cy7d+I+e3bq1uJ9rMDXkRhzgITNzNrVfdbzTlvr9AnM8ohhPWqWCtp0fRt6lWVyHlYmBkKPx4urAAUmYMQEWWjJiGt0gf7qxW0Ym89eyomoaZiUkzWygpSTu5mMhwu6UiIAMglCCv1mr1JOm/t5nx1vW3qhFQVs5DXwK3DDAwSAgWCwQO7BQXarAv0291yb5sychTpSqtKSupwF1AmMAaDjA3Uq3Upt5Te3zVuU4ZwxVY0Z4vSfIhUt8OYY2Cwq4iIPSuyYZnynf2Va87eJUmbFU+noc/WD0GvAGAEclfKKghGZATNnbbLrUn5zPkz//rFjZeeOXuqwN72fLFaZnBPMTtXa7kwubaLv72y/6UfX/2bq/tJ6/WNjaS7D6rVvQPh4Ncno15PqrzMvX7EYV1GCGEIdzA3V9Lk8P0ORGIuZh5jrGcbMqnzkFmLg8SNgyC/oeAkeQMDcWFO77+3/Na3lj/+0cZyucvokNKtK5vbtyvtcOfm7o9/FBe7ASJOgCkM4EhsQg5ofnjs7n3BhMf90w+/0rsBVnRQsWFYaojBhZWhoAROxEpELkGR2YIP86C7Yki4f8OFQbAqpo2N2aX1tVI4mS3ADUIN9zBxUwijCAu17c5XnZFDyBMMDAMrkRq3WVfJJlVMOTuxS4Dx0XUZEk3K7UzfvXx7RuX0+Yvrk2qW8mLegsmZQKxDqT0SJl5lDjCkBu4iFGOcBC5Pbf6bCX79wpkf32m+8eb17713bamOSa3gIpC7RQlMASJZkch6IBtKowwyYhNiDoNtBXvQuesgsj5BzV2c4ILE0jsnRp2JYZQIiYJyRUWBIL1SGR86Jew1mYu7w8iNDJyYE6goQiJWDogROdAQmGI+VKwfQhbUyRzZPWX0Ep0KA3qoUXCyoTqCHFhYGMq3qlEy6hyItfbk4OSancGiQ7QzaAgwp8NVQiEiB4UoAYbYK+CsLgmeuciO5Gp0sCQNUoDhStm4FCkqg2vTlZSe21r7jefPff4TW3WFrtU+WaaYHEocijg5c26+z3/34w/+ux+/93d3FnNUUULftxyOFs2Oj4gHwzKeEE9IsI5XEFV1qNY8n8VyrerLMiKDklPwirUoNlNKbdum1BcFJlVXFwxAfahJe7C+PhS9dVgmAiEAxe4t/cZfL7/6l/rBVQtBkGHQxU7+znfwlU+kb3272781IdgQFAwFEImdSXP2j/Z4uDcAhx51qXffcEywDOoYvFNgUKAgIsJFQRD2wzg1czf3rNazlHa4S/vRmqm7V8jPndl84ezGlDylZBxD1BjQG7N3pQSOvN+kK7vNXpslFu4rwAGTQFFYGCWHOjoziZDzcIhhr1V2I0SXWO+q/vTGYiPsnt848/KlYn3rTCyWydF67tRSdlP02dmxv5rHGAGDewhcxaIoijIWz8zs6TPF1uasWS52b4ery9wbO9duK7chEI2YqAhUCFl08RCFgtCwmOWHd4rJQcP98cOltIN8VGYOREIcAgqRKJSERbmSsi7KglA4RfiEeCZByUEQ5vvvViQosZgwIiGyFCEUhQSnHkbkzIdmDhFBSN1yHs5LQMwhSMXUSiATpny3lJU5EQmTyVAabXgRbGBlEiZnYuGezQgu7OTGMkTUAU7kh0GqTuRmGQd+MRcS5gIkJVdOPVhAcjfKA8ZuEgoD95pT6iprP3G6/o3nT//Wc5tlRYt2MV/2i5QcLDGs1eX6bLIwvLUz/+t3b3zv/TuLOFnbWBNVb1fDoHtobb8HYj+eBE/OwhqucHCbDnVmsnqbAmyKOOMYTLukS/ZJ6hY5cRHr6bQKkR2JABpi1dw9ZSJCDExwQIyJEbzB97/X/vGX5K//rtzv9iWUYCBLWjVf/vP07uX9yzcMCQGe2YjUiEAmpDkrhiKMH6PpP9TCergPyzwNcg1TdhbVaMxOpVhpFN3FjZCJFUTOdzdNOf7lZjYTf/H86U+eOz0h75tGc8EcxLKnXryrpGSyO/vz927c3pk3QUopVi4KymHIDTMNbpMQoA3MyNy1MxMiYg7kKMFK3Fq41dqPr+zWeLPpzz57cePZ2bRzNKpNyl3qc5+9d9fcRkIkGfZPFumyrhZNjLmq+ll5enNGrzwz221O0we7V1baIprBDGbmluEZDoYFV/OeXdiYbIjkcAWSc3VgaNwdj4P3fciaIMqMYafbTNYFhnKl5Ao3QrLU922Xu1zcDTo/asyDDLAqeG9CFsmjO2umnNmY0boamGA6lGIgEZgUbBkubg6CEYOJBGBPLUHgPmwDYCADgQtmPTqqgTEUWOKg/YJST1y75WHLggxVYqJDeTysp3ZgzWkaaiK4WmAYAebSq9UEOnIzgch5aB2P2dDnXnN3eqv6zU8//fsvnn9mogvzRdaVI3EhhFldnJ7FKqRvv2/fee/O6/ttU00qrspemRqLvaPGA/0ZP799FZ+oD4sgFMUKiBXJ6qu7/O0f3nqPry86akPlumLdL9si5WZtrX7+hacn61sKMlchGvzQy/39xf5cRNY21otJbfCQWai329f773+/+cZ3ZrdvGur3TCceCf0MsMtXFpevME/Wqrrrm2HpdfCyO+BwBmoUDdJHvJCP+FS5z4fFkUUoCglDsrlBVNkysYONVGA2+GuGVebhUY17u4i7b5TFpdOnnj61OfW9/ZzcI4PYtArFhMOslp54v2mub+/sLbLXFUc+KK5nSpq9B5KKSiGmhCBOcIYxs5OYWTCQwzi2ble2923/Vu5v9/yJem3dmCwwotRFlBCpNFGvI5tZ1yYzI2J1U7dAuN62m+2iqOuL59deSmevLtv3FrsrxUYo1DUQC4jd2IwtizssMzl5YhSBcOjXz2SHM+RDhqlcNlPTATe4JfZenDxOu9y1SBbApXgkD0DgIH48zwaDXcnUec5q0ESu4k5ZoSbuhSDAhEnIeRiczk5UxkhqAXB1uENB5q6IYggO5cB6mDLKQwLL8S5BEGdhDhEZMGZKAGBGgPOBG+tYDOogRQxIYBGwIxJFIncyNe6VKAzGHJiG2h7MYLD2DhERqYvJ88+c/83PfupTZ8nvXH1vJb2xcQnhKFQVgtTs3b79ozfwnXcuv7vbeLUxRRH7hkPyyrr+If4QDNGqv5QlkgkpiZRYKdVS9Bana7FrBQG3vv92/Ns/u5rnqzgzi13uTVIFq2/s3/j0y2f/T/+b5195npCXEMmh6gxV8tmPvrH8y6/JC58J/+Z/2HOoFLcKnL29Tf/hi/zv/zjeuNmCKeQNbQ0E0FB8LwLRmiFv7GDzJwDwQ78VfYhasSgp42Cr+kO/JmenQ6f48LZj0fxDxSHnoROC3ZxMzITZFCx1nzoLwZAkeiq5C4BI5phN3Arymtxiu0NFtQQycVVV1HWxbzdieFZWz6zrZBP7+74/X5aSSSZ7VFYS2XtxmMUb+/T+njdcRLGYmA3BuEAgZwhrIXPVMyQJBSWLXjtTxhKsRQoFdzOD5DaxpLXyOhVfvWnf/Oq7n3n61DNnNp4/NbtY5zNVODudziqGY9UumqT7ovPsK+0Se47ouVlPW7v7tNEsLtTZ1+i1qmSLrJxKNWsIE9ZSdNrTPAlWqIougaNK2UEyRJxrpbWk8xAmVtRGWXMPS8FIVAJPOt6j5MEjg9SSsIUZDLCmDhNRgmbXnlGW4rDOuHjQiHbHJMONJJYdUSvwqgAJXJKVQAFYNqgqPDH3cOq0YGHSHMSimFKPGHIgtKWpeHYblvwZLEqcG0ipJNaHGQn3oWvqjN59luO+1J1XnDMR7xO30q9hv4lCfdpkKzyragPpBVQECX0bUfVx6rQKtgCJcC5WoFMMCzlPcpqk7IoQajNd1bO97ffPl+3/+NUX/9WL5y/l1d4Obnuxkim0m1h7RvRsVcZq/ae387ff1q/u9HdU6hgi90BvpatLXnlEN+igEQ5KPgkTkQvFPhcgBQJUhpI9/FEW23+xBetoUNNxwO5r+7vN7ct7q1tz1BlFQgK0Am7UU10rtyh0DaPmCJAYeN/19f/Y/Pf/ff+dH9U+C4sVzdahmIn0b7+9++3v9D99s2hWDGLP9JjN1Q8xsog+3i7q7Ac2PzPHEBjQ1DeaiGrhQt37nNLeXFJ/fjb55MVLn7vkZzdnrugNKkFBqgrDLFpJ1Ca/1Xb7vSZz067i6njLf5TzWaEEoxAUTH22rk+NOUESbt5Yttf352dK3yrCRl2WIbr7JzYnk7Ku6rXN0mKzmvdtUiJI0sxOJlbFan2tqqo9ZiYBO5gZTAZPpkM5WnKoIwzOGBr8VWwsJiVh31yyE0iDEKA5ddpBMP3HcqD8zNi6j956DxLuGj2BghRlVZbImTKZy5G/CuwkjgKYqhOF4FFV3bkKZe0rLFapCymhJKibqoqEg4RtHdLdkN1MiESy565rVPPTG/VnL5z9zLMXT21OlLzJ2nVOuSHNMUioJ22M1xb5+zf2vnPlzs5u065W7g45nCwTBXaPNcyyK7kxsxAduKHDx+hU/4g8MR+WMIcjk56dp7NZW6yIM0dGObGocCWIT8tfe/Xcv/nNT7xycZMYHYoAUJ+6b395+0/+Hf7DH8XdNHnp12AqQkip3tlufvDj5u9+QO9fBUyZ3XP4x1tkva9aA+7t33fNq4Mwdwy1KYAh850YsA+9r25KxIMbQjwH75nKMoSOCqcgsJpdPE0r/uS59c+/eOE3no1FEZfLZtnkXoMRE6OK8XRoi7rabvWHV3be32kyJHIuvPu4KziN1BX69ejT0nPCHae9VLZc3Gly6/nmfC9qK5ajHKQN/fYzF5996synLlRn6zCNpaXUaDanlkzMctaCppOqiDFm18OqcUMCAPmhK0SIDAEkGIawq7sno6YH1yBxZ8CMGIEQPUTmpPffi0f3vQ97z7AAe/8q5AMfx8Gj6KE95MP6GpMHFiAktTabgZxgZj0skymbMBHYMirnc5PJhTXdb5YdXKUcPOgBVpiWHGjYkXIoR0kUhmVl1yGN3ohc2EFZU492Q/KvXDr/hy9eevXSmbLwZbNa9ZYaFNyWIa6vrYVJcbPxH9zY+eaV/R/s5GUCwiTGCBFTBUDMQ3iqELlBnAINVxRioKGU+bFGfEIurccvWM4HWe9ENAQngImobdvlYqdZ3rCVAhVKRjbK2Hzh4u98/vl/9vlPPLtZHiSm9H1z5X37sz+zv/ir2RtX6vUzHgoryx5ccYG//Vb3zW/U734wgSVgwV44aqB7DJdyvKbCUaGFeypbHWZ5wWmoBec01L15tGBlpSoSCeUctJ+iq8vy1Pp0v1cgC/msLM+vr1+YFc+dWX/uQnWqxqJt2jYnZyXKOceQJ4VsFO719OrtvW+9df3dOytEmZBwTn7okb17xA8dYES6WcnLp4rnz8wAvH1r8aMbi6vLLjU+b3xFZm6qqge6jJVtP723ujPf+5WnNi6sT+q6zK0s+kylW4LpQVDakPJmzAxxdyNwCFLErnM3Y5BKMFA2N3MBCcMtd30j0Vu25Ka5R+9QK6WQohyqJuCjWUZ0VC3jETf1IffF/fifjoez3HVwf4SHAbsxc3ZatunOznx3sez0jFlW8azqruCgbl3TlSQvXTj/47x47e3dq3v7Oqy3um1M+Omzm+e3TjMjZaMoQuJErjYk2ZtlCLuwsmVN5P3arHz5VPkbz5z+/IWtrei73Xy7bS2X0DAJXV2EuigWrf/w8vWv/ui9v70+v9WTdimEECMDerx8ZkRTl1UZi8GVT2pwL1ga2JM3r/AkLSw/TNA5MCl9tlltVKf3O24aiShDpV6n/OJG/rWLk6efmqB2g9ZdwmtvdH/91+Gr356+dbUGmlg2VTGd1QHoPrjafulP7/zH/7h1+0YNNPCkVAjxY9D744GjR//eV8BvWG3Uo9AbwoOLU/fhzOZkZlXkZ7cmZjZbX9vcmHVdw7BIODWbvnDhzKVT07XSK87tKnVtrx5YIrsxtGJMC+Y4vdH4a9fnr13fvdHkqqpi4KHQ9sei5v7stPzVZ7Z+9+WLdQjffeca5Tb2u7upAAMhaqgUlNyUSYSvpkXaaSrMN2O3sXZxY7bGnNqsFYwCixSK0CsUzgwmFYRhykHCHBiJAASQclD3bG5ORRG2ptXZWTxbYRFKZmK3soglB1VKio5CQflByRhU5sHQuQ+5BcMz5cPfc+yOf+x5EJkJeSZZ9d2tPL8znydFFYUFyMlMmMWc54s2FvULl576vG0329f61V7HNCniVhGeO732K88/d+7UpO89t8sM5yCksJSpKJjNzMBEIcLNutWs4GfPn/rd59Y/e+nMViW5Wy2aZdNZkCpILEKsgpim+by5devO7du3tU1bk/WNaTkk3ww3BziocUqqErT3PmuUGIzN+jaZUiH4Wbbt4+AJWFgHiwlu5ARVT71mz0D+zCdPv/LK2RljBfTmtDenO3de3ZDfvlTMTkkHmyDh3bfx51/1//An+tq7tr/bAstI1cZso6zRdTvf+Bt86+/88mWH67DdIBVwVzyuCq5316oeKIN7VL9NHHaYo+ME+dCb6uA+GYud3Vj7nVef/2yfY1VWgcgacpBRLXJqfbI+ja5t3y7mrbS9pmHu5L4xiWfWJ2uT8spO8/3r29/74M7tXp0M1gHisaKcf+aAPE4dLaIvPW1FOrcmeHozkn72E2fe3cO863dW+U7T3140bdMaIGWxWdmFjfr5i5tnz2yGEJqUV6ouwrkJcRLrshfeXbWdWhmLXjWAiEjNVqlrUkFEgcUyADNIykjidSHnN+tPX9rsk7+37OApEM6uz85unukyvb+9f3PRaNaP8lS6e78e8QZ72HTmvqphuGusHVu6/WjPAjZ1d3BQoqbXJmUwirqIc4kH+8ZSciy7fsZhY236Oxc3n8LLly+e7yyUZbley8X1yfmtWS28XC7bbtGqShgqxRkAYjconAkijsL9VJRPnT/3m09vnt2Y9rlbdn2vLKDgRGImFSREwlZdvvrcpWq6vqcep9NJtwtAVQEMW6y7ewhByuq9W/t/+9bNyztt67EITAw5VtvnvnZ73Ar25MIa3N1UU0pkKXkMWH72V577n//a8584Wy819V22m3fa9y7P7lw7vZVdUZjxm2/hz/4s/9mX8IPv9MvG4JEQkddub+Ptt3H5ffryn8V3Ppj2PRNWgCJGLi13H69SzEfmaB33+K06zFC9e6FEJKAjzfpwpUhK7pkCz8ry5YubFCSwIfcKJhM3tkw56d7ustc+mSYvFWSAwMpIp2bVrBRrF999v/3G29d+enPpoSzQBVdI2XtRPaxu0X02yHGMqTXfafL2fj8LcRb41WfOfKaur20vbu2vrmwv3ruzusLdHVKC1BN66dyZF85vffrimQuzwvtmf75MmSIXBRCLwEWx37VXbt7eXy6iUGkM80KCqm7v7Oyt+dqalGXs0kH1/d581WshvFnJp5+aTctw6dYipWVN+uyZrYvnzt9p87ewaLvFDoqHNuzx9ID7Qgoefk/pY5sJH8VxdkRgJgexUKzcWnVXmFkugSTi6maWXdxy03fU0KUJX3zpqS984nybiJnrAoHUcrvMVZ9Sdstwd2MfyutCyYeABlcVx7qUT00mz21svLC+1jFd79tlNkEsCYRsREuPpXPpulWFrU9svfqJLXUUEd6fGmLciEiEc1YimkyYgb9+a/b+td03ry9a5HpSlDzsSfRzMK/wJAULh9XZFaqu6DAN9fn1esJNQOcVlVsBbYWzFU4zkuLGUv/m9Vtf+vP+u3/DizvJWRgcke5cf/8v/mJNe1y/bl//G/3galC1iFUGWAKJAkKl+pMok3+QlHvsV6KD/aKFyICfuV6ZzdVUuj5AgyCaBzLPnTLBGKjgRZNsv+0zHLFSpRiKSVFU0SeB1qrQzHeuX7ny2gfTn17b3e0szEK0LphDZJWo+phX1DonLq1cz3HWKLo2Z2vL4C/W+Skpnp6eeeWpcGeZ58suhDCdTreiblRhM6Jqu7btQ28FYu9eFwVJANPu/vzq7ds7e/uuQ7ylhxDMbD6fz+eyvr5VhthZhiskZPPOcku2Vhfn14qyCM+d2czdorDu0ubGma3q8nZ/+Vp6Pyx3cLh71UfwuP/s+/gPG3sf4hY8WOhnYookYkCfU0tdIC6F1TyZGTkRN7nVRceFzWZrsyKWZERUFoHFVquOvRKRIJE9u7sw42gGRwRAVYPTrCw3qmqrrNaZr2va6bo2+zpVMWewJ0lNxzW6SNmByXStipG1ie1qQdWwisLMZISuY+ZJtVYyNrytoe6eIb2BoepKXD7QCE8iRecJCFYBIMSUMrNJkAk8EWlbPHW96be7+emaFzfbLpfobH3tnIXPzTYj9j5Y/tUfxf/PF2df+9vF3m5TFrUmgNoOJVD+zdfxN18HDktzgyyhAqAdtBOg/8d0YQ018NQdBoWquAVSHuKw6N5dc4aULrAdbZTjQ9wz1nM2sxSI+70Njrsae66ZudQlgFWvq/64XRiUZuxGyOKr4HlGKm6c+o6mNdvEF1FRTtYWLt+/bX/3TvuVD3ZA2KqAnIACKKB5jXMfe6W6bfreU0WxhpAmCtrzJWq2p9y4TxtEjkUMzH0sbFo6l2wiXXLa7tJuG0mnl9CHsri4Fp8RkackhmGQoHFt+tR2ad8tB25B5h4CnsKOB7mZy7+9Ff/yA1yek4vEwLFFIbLX5e/d2V/fWj9v9dlCi+L2dZsaGoW3QDJetagJWxy3atLNreT9WsSUrdAWqVZ6ek96WGKibDE7FY5gufPSwBNY5QqfGpjRe+H7sZw8oqhsQtVXlItlJ1pmbPbFDQ0JwlwEbyojT0VqrXSvC9rucZ6X8ySIVfQ2sBAVSP05yu9hZimGnEsgpBCdYqiYsSqZ2StWrBryfCv1r91YvHx6bVrM1WylrRoX5ZQ0WBdN5S2OYQ9MHsyDtTX7tOA6xjPVbu9yZyWdsYlkTWBJXPYkIQTqGSqteCwQ6yKy3xZtmnZDMTFHUKqLbGw5TcVW2VbDxe/OD5uBgjUAzGwozzDsFz3rw0ub3kp1C2Vfb4YYi9xulmXIuSs0AEU0N4AzPDAD9FF3af178+QsLCEyJhEhl0AhdMsphdIaJHSrHSs3J5uTSU1djJQzXn978Vffsu98b7a3LZDU9fXPI6z2UTw4JTw6OYcDxs7DKuHgyGIghWmLemXeJ14mNOpKJvBHrWYGWjKGTAsvhYsQKUpgOS1SVpGF5p2+fnvx+tWrP/zgzgd3Vo/y0lRdlaVstQ5WdZnmyZZOrZKkZZdWRZl6QRbqmFpoEpSeK4mleBm8LmgatWmafpWvs1GXQwghhOOlkTI055z7pEnZUcY4nU5n9aSoi5tLfefmzjvXbt2Zz7MpF4GJrIAzzdvO+3TrXKtcVDVstS5dmTWb5V6zkfWUG0Eky8kzU+99WwabVPNMjYfe5bSUO6mt1AJxBvbTau5NG0RJe81FxmmsC7jMPO1yQQHh4YK11iOzZrfg1Gnes2YP3VzUM7t3KxErCSJJqHX3Miy901hBQu6tRWpIK+/3+6YzIi4haMmWyCtCL5aik5qEg+gzomp73rx17daUwj95+fT+qk1t17erpCpcmbOBi9UdJi9FyoLrqiyLwBQU6ELlHPtmtWiXCAY4qTWdkndkBG+JE0lyVqWYdHfRnl0mWiVKSq6AuLll5aQPbwfjIX6dGGCmZMRAp3jJFabiJp7NmGHMQbgA8uHU+IkOvSfgdAcORvjddWgCIvZqrkt2aHZOUnF5amaEoG3+/g/7L34ZX/2mX33fgcBRfm47Y9/DR6yH5e6AAQIyMgzJq9H6qWCrjub1JPksswMB2oeHP5QqHKSkETFzIOEhhknE99rVXk9Xlvaj93f+9q0r7++2icppfLjjTgwCLV0nTLXweoWn0izlvnZrS55SmBbiQOmpzO3asPeKWtd1i1W7GcvZpARJr7xiyTnnociFQS0RUQhBzZisLjhEKkkmRVFFEesv9+WbN25/753rb1zb3W96Z2GGI3NZWAiay502vX1j/saV/enT63WcPEVF0txo1ykPy6xRSIhcVf3gyR9qiXUVI7Flps51PtQVnBWgWpY9e+CWjBO2Kp4WVAYVa7lvylAgxIe2T0ld0XfC7ZQ36oJmVTi9Jhmylrk3WhPMSvSEErlIKw4FiQgx52wpCem0kvVqtp8WFws2gDKm7JMSNXwafGJd5xoJUBMJHujmvPnBOzfYqlfPTIoynl7bKCV2OSemFCIonDaIexCtqihF0ZjcWKTdefvS6bh1pji/MRVLLCxOblpXvkFYEa1HmgqqlAvPE84Tsa1KSpKaij6rk4DIiN3vLhbdP0wFOCgPT8ycM5h5UkssGNSLJ7GWjURg4N6V7q0W/cR4EhYWAUMoIMzZjVyd2NmcC+M1l+xF50XBFAwe33ztxr//Y/13fzJ757IDCrildQT9mDXX/7F4VHmZR72fh90wCORmxMTORs68hm4z9OfqoqinnaJJHokCkj5y/YodrEAy6pKu+tw1OWV7t2suX7/z7p3lnVTcbPzaylsqQlHSoZl/H33ZZu1DbMoylSVixLNb02mqShblGVteCwLFBssp05i6zib7XX73zmK9Dlm3zm6uzU6HEGEJmi2llPselt2jDLv4cARQxSLGMjDcsb+/3L2z/d9v471rt6/c2tvrXYOIsEMtZ7CTcF3PMuJ7u91Xf/jGqjn/0lNbn91i98IQ3W2YRw9FqBcIycm82lirKsG0orUyz7h5hxaNLHMoJHgdsRHXN0IFL4cafmsVA/AAr6FVzoUVhz6v++iKvrc2ciqjl0WoYnxh89RpSKWUeRocU0bPWHNspJxyU5YbqSXvUgDWuJhF1AFPn30KuVMVdypgkzKuDBum691qp6wFTjAieCjnfff+blsUO38R80vPX3zx0nRzY9qsulWfE6JxscFnhLwIghD3erx7c/W9d26+c33nf/25yfkzG2tr2CxnPBhMSpO6JCoWPWYhVdjj1EUOpdTTcv3shHxS92tR80FrYrD6HzE1DuaD0x2AiAzLhVVVKIGLFEIIZIZWOA7pSvRL7HQfEoCFhxjSwIjOhQV+53bzle9deeVctbm1frperzLaazfiH385fenP9Qc/YqwSYekogQnQPPm2eRgPhgjcNyUEOdGwHToJXIlYHIbbufjp7Ta5F/AMzkoFmVibuHjUsdyQibqMVZ9XXV52KSd7fXf/8o3bV27NWypQTAGaRCuofZRp7s7CsBj2VL936/Yy1066NC21oFLcrHbe6flWF7RY0+xKxZ0+2+22zTs3Vnj6jJ9aXy+KcDo3MQo55URCXBZBgNynjiirLwOS2Cr5os83d5ZXb+x97YO9+XKVlLgomA53bAPQta6I5VqoZotm//vX7rTW3WiWq/50DFzHUIVQMLEbmZpZG6ORuPFip1Xr3ru1e3N/ZS45lGFyyop4eT//3dW2DlkzWCVHNifZb431tdvNKq5JzeD8qCiEnksTcy7v9MX3rq0WazmZ9iFK5ygZrlOi2y1up6DFWs5VcAMkEBexnHfpR+8vLqxNF9qrNRhCy9zCkq7Pu2sL71GVIbI5uZGzu0tRd336YHf1Nc63qVpRuDihiWVhBPas6Y4WBLPWln17Zb/78dXd713efe/W4lObtpjNT63P+mYlrgw3s6oio3BzN13e6/YSt1QR/HYjb9/u1qvh7rsTH9+9lR6xL0kwG7YvBCAiQ9ZgWTpHe+/2IgFlweYoxEWERZRaHISwPdEB+CSmhOwQkIkQR5IYuIpc9cR//Xc//tGfX//Pf/OV/93/7F+tQ/DO23t/+pXuv/5v/M23S6wU1LKDwUZJ88cq//KYOD4lfJRgMeDuSk5OTpDB2CL69h2+/sPbE126ZQ+VIZSknFar4uGCVVrj4OzI4Gyc3c1F3RdNu+ysodo4RPdAWrkG1cQPXw8s8owDG8V3dvr/3/ffKbjL7L3QZEVaiGcvNFiYXO67W8Uah6IK3ma73aRlt7yy06y9fT0KxO2ZyXRjc202qUvBpK42ZzEwulZvr9Ky65vs223+YHd/r9dlxv5q1WbNKkFk2EvRzVgksEjhpk59r+I5yFL5jTt7t5bLv/ngyqSIpyazU5PJWhlLYcAdatlF6qTULPe292/f3Fve7solZlOdFYHbTr/15s5bH9xseWVSQIsQCtUk1hak27m8s6jEY9EvEfND2yfmaUW1kr15Ky9WlyvqM6nGWDSqBcNyYZK5erdN2/WpgqfZb4UYEMqW/Ec3byz++lbF3IBSMclmIsFyYuYOxZU9Xc3O1zwHIOQBamYxBibquvzGfn/7p++9+8GVT6zFZzYnT53eqMsiaf6gK1ar1Z3dvZu7+9cX/a2Wd7J0xeSP303fXl6p2PpmEeDM7GCOgX3RJ769sDvKaVIo9LU7i9v77/zVG4KhRMqhUvswR3nEloIKGwyrIfxqEKwQwpnV1Vta3mmlqqeEHABC1oNtVB8Y64+/0PsTc7obO8gPI6/JhMP+MjfbfZfiZoy4fR1/+sXVf/tv0/e+LUBEyAEwjSQQb1V/bjs3PowPTQdxwMXJyBnsBCIL4P0uvX9nxd2CIDnk3lBCg/VdePiVRe+GohLmZMRDGWcjjuTMPKkjSGBOagA786MnlsQQJmkTXdnpc9c4WWYO2nfkMKq9pLDaZ4BDGXLlKZEaqDHKje0uGs69QPc38lqvs2k7KWRWtRttL2zap+2l7jdNk23e6rWd3UVyDTFlK+qKSCKBzWHGoDBcQhkkgVTJyIhSKDrQTuurZTcpbLeknYmtlSFGdnZn8zYRd26yWi135svttm9R5BCmuREvLNneftrb39/FMhcV+qJUUWjNfSm5o2kPEfcKqX/EAy94qjBEouv7XUPWK2kmD0l6STCtnCFhzpGlilAlF5AaZeI7nfarRlxWYCrQdZ0EgmYhRih7LyfFzNNQNMjFs4DcjYmCUGfYnq90ia6p9nq6laWexD738y7Ol6sbu7u395a7Xe6koKKeCm53Pr+5XSB77mBmLIroEgjb0cSduahKQs656XRnlRekwLDnuDPIoQdbrj7CwlJ2HqLmzUIIQ1KKiNxcLZeCplxH4ACPnrOZWqLwD8oJ/3vzBCwsH8L8zQ2uZtnQJwE8v/zCCy985rO/9zuvbAi6b/zl/pe/WP3gW1vCS7NMgBEZqNcsniPCRy1X9bg42pzu43+Q3O259XSqDpWsldWs43repIJ0Kub2cKlRCocF4j2p5pxzzmbWMNoutV1SM7C4hGyUHZEebkG0sXHiQuzUdP3sdDoxLkig6CeLxq0oqnWqVm1zdb4zXy7QJFSRnVnKWE7qsqyFJxF1jFmyMJR95Sn1/SLtB7jD1OvO2lgU56aTeiJdVgf3fX5339wBVwaIWERAnLPNra0gE+aArO7OPKk31+v6JQ4F04RRMoTNXZOrKlBGmJAUcXOtmIW6T/tNmPcRyw+YqklZPbW2tjGbdoVBKm9D2bgXVJYp6/JG09sqa98UaPo8fWj7GG0TUx3rSVFt1bO1soqEvu+NpRctCp5Blk1zY7maL5a63Mlbk6ieeqMqrq+fPvv/b+9+diRJsrWAf+eYuXtEZlVWVfdM91yGBRKzQFdI7GCBQGLFhgdhxYOw5Q1gj5B4ACSuhFghhFhwWcDVRTB3Bma6p7Iy/rjZOYeFeURmZaV3d3RXVreNvp9KqaqsSE93C/cvzM3d7VxN19OrOxmuYnc47DbTOA6iFvv98be/e3s8/uH/ttI84SohMFTAMeTpajMN19vPX7x4dXVV1f7PbGFzsfkVRIbx9Wc/H17/7GW1d/vD291+d3z7qy9ejRovpqvtoKWUvcHT1jXH+HM5lPnt7e3d7vbuDznSm82rL68/T6+OEeFWIkLb/KXfGFjJlh5WCywAZpZSwvZv/uZ2/vXdYbd7By85eYIk0RD/Iy1CMUgFdMjJ0jLVpHgNjMf41d/+8h//rTf/KP0a//bfTf/yX1392b+f7jCHbqERNQQQuACBMDy6vHH/ZN+FFxD9YdmlBxO6tdsCtJWlaGXEAgByxG6Q7NgAjjLcvR12+nb7i7+Ovzr/5PKIxKkuYVu3c+WbAEJ0k+Lq5c3NNHm1DeLVyyFkiKxqT88N0Prk7Xkud40YInJE7OfYp3TMaXY5mu/nOiOQVJENsYxWqABo/9xge0AxNZf55dXNZ5vpUPe7eniZ80tAFcMETNvxcChWSt6MmrP49SifXcXNJoYcbXqUGuruVrxYzMvvESBBTWSSSGZ4OW1fjGFmlpPV/b7ibfV9mydFMUCSyqv5TSgOcTTfbZN8cTW9mepW91evbwBExNGsjaRIqGqOWSNKkv005ml7M+TZ57ta/9/b4U/q4Z0MkTbp+uWLG6DWGqPLZukjbPLnsxzu7r7+/Yyv0qtRnr5oY/HyiDo7Xk7pZ69f3wwy23G4HrIDUFXN4yTT5neHmK3WzbXUsWrIcNjW+lpe/Px6ez3Fm3LrPqablx4FIvl6wpjTcb/7+uvJN3VIR8nAmCSGus/iL7by2Viurq6urkTkcDweW035lNJf2bthGMZxvBnzVcULlxfF99W+GPP19fUwDPM8I+ftJiOpu9+I3E7D/5pevD3I0aYrxTT6uN293r5w91a68OH+MGhadsv3Zz0e5ie+CWC6Ovp8t799N3stGCE6qU22qzKajKMfllunfZ7TdkQ899DNpzjTUgDmXku4eamhczUp9rtf/fJv/MNf/uzFf/yLw7/5D7d/9ufDO2m3skSb+jgcgJ+Kx6xPeHDhnGHLAlutwAdDT0vRkdQuo6ifg3CbbUx+EClJcpqml7L7ovxWYtkPzmVYPiy+83BurGJeStlDUS3CQmBwSRrzez2j8+vz6VTx0V3U0zgMw2DXcijl7e5YrLq7W6Bdh3vYLtEuTS0Lcfda7VDqsZSj1Sh1KaxtKCG1tol61cxylpzzMAyStFrYPEccy4OB2/PWoU2PEzAzEcmqbdBERF5cXQ82Yp51ng9ljoBGKMQlAi4SQ8qbzbDdXG3GIavsdrvWXO2sBKc6EVGTR1WEIcS9VI8ISFKVdh5sZvOxIqyU0u76johhiORoG9Xm5JZ4+pMt5VwRIeHu8zzvTI7lMEttt22p6lDjaK2zB1X102OGIuIWpfpRy3yYayma4F5T0kGilFZ2Kz3okjsQKaUhpZzzdju1OUsi4tT4y0W68z/b16urq81m01ZvrqWUUty0aErJEVnkGPm8eiLukOJxd3fn7hbe5sZY2hYxQD8MLAA5Dx/ery8iN8dU5tpWTCGp3ZWky5PPn76L9QlK1UcoIkIisHSMw8Xu1H7z9vY//+V2+h+/+fUfVH75p5vt3zlkH5aj3wC0y9sR4RJpJbAuzfNoXbLQBz+twP3FXl0OxeUbNb2skqXYK611wH//8he/fvMS9RbtCRxJbYzAAUXgVKJ5+V0Pju3qMRdL0ZLCDGFRkZYeVnwYB3N5NKVJ+3sC0tge9h9iG6rYH0spxRDh3qpDSQhU2r01Em2eXnH3YylHxKGUg5firTMoxaViuYy93OaqmlIKSdXDSimlWEgr6qOqsgy4LjtrjSoecaolo6qDJlWdho0kDUkRIuZlttNFpRA3EYw5T+M4jiNy9vC73d2jdjtN4qLhoeFeirgfDcU9Qs5HXbG6n4/iMc/zcpzDqntyzNXOZXLWqIigRa4dyiyz78t+Fk8u2pqixrl9VLVNKKoiCFj4oczhWo7HUlNWmB9VdVSZDW7LE1pthttlEquct2Max1HzWGstZX5QagQwg0qEoy5vR0opDeOgiloO82wRtVaLpfsnOSWXg0SYaXhSIFAdc/Gs7u7FLM4FwAAXVH9vNzur7niqh5V0nKu1MovQUFkqBj15f88fw8PPERJwhCsgSZKoqCZN2/qL//RffvNf/9v/Trua/94/GP/u37eU3kVJWCZFacdARJz6RPeN8t4OF5edE/p7DaoAlgu9ek4HPRfcAlCS1BhF0nVCxvEQ6W77xfDVqesUtWXWaUYs06ceuI0IiJrLbB4eAphHDRGEtt9+/qHzvuV+vucW9yNoSLbXUkIloHlIr663Q9bdznazQNXd2weoxPIzotHurgiXWv0Y9Wg2B0bNKi6B6uHw8/Gfc1JVN9Ra4bBi5oqUJWkscxu8357e2hDmBnPAS3uavx5EJEMmSRXq8LafqyBCFJElZVH32M9HmM/+cKap+w1XVZcUhghIRXGvFhbiXiPMBVa9VAdQIYFlYqHwSMXasF8r1rT2yWZWIBYS7rDqFWEhJhrQJBEhaFXolihEkmi/w6HVUczFo1QYIKIOjZBavbr4KVVCQgANkcAw6DjmKed9qWZmrb8ogkC4R4SkfD9DL6QaxEPEJ8lzLWYukhxqZgJJJgers6q3yjttixxH8ykGhwbEw+08UBDwdiFP7i92t7fe7fHB1V5wMCnRRktCwsVbNYTlg/mPsYcFSCDMEZYQETWFhdfXdfzt72//Z9zG9XT9xZ/ApVbTYRyO7RZTk9P4TeudnXtbbZn3PRFcFlj3c6+fnnta/qIKFdUEzaoq2iZ0TCWbR046fZ1kwDzNddr5zf7d7roCCFGROGWWApAH1+se9rBcdPYAAuYiEhCDAKr+3oPv939pc4i0IbCI0/XVcAsR9xqGGGJIQ2SRMaVj0naIuLSbBE/3W7TbV0WXXp6guLRJ6dvVq/AW1nrO6HZeVszd4dFquCRAI8LqMrvb/amrotUxF1FXhHmtBpjMs+ac0jBqsjR4QgmEICTgnkKyqELMrLiZWevpnOuinTf8rlgKV3iCivixRvX2oItKSqoRggJIoC7DBtqujFmEebRe4TdM+tq6mbLU4JIKMcnQqBYhS/u0FhCpASTB+cnRVrNCBLWVtwFCVOHVl912aVUglklsQnXImgDsj/NpmqLlk7k1wJJfbdXO730Ecp7bVYqsgLgEHKJSkTwQYdoK+QA1MLscqi1V9drigaWcR8S5iU+T5AiAEfq42xUQkeIpkFTyqFEiJFpPMYksK/wJAuSh5y9CsQxFh1cLzOGzydHVboevhgmfH+bY7a8Ou1zcDlVy8vL2PqrM1c3dFTjVZYtHXwX1ovUJWbpUIsv+tIyC5SQi0CwpaRrQChkDdXMlFTlPkBSjYIDpXDYS8QItLiUhrGXWB9t+38OqrnN1R6SAajhQw7X1sfBw7P9hci3xEYGIZXtrFc0ioh52mIuaq+pmszm413Za5976e36aUzCpGNQF5l6h1aMmnWdLghSeB4guZThlmbEbNcEji4i5lxB4GGorUVNrfZhZm1GHlIdhSCklTYE2E2YYbAhv41YppZyjmFV4klBgUJmSZiR31AgXcUM7rls5+vNg1m2dBT4IchJVNVd3BFLKIpKzhAuqWQSKxXlSModpqIWo5pzDJMnKieGgyTVajVgLL4biVkVL8aySESkAOaVW6/bE0lsMQfV2e4nua62uGibhg3o1hItKVgkPKCJaMwcCVotX83ODt7PC9pxm9VOAnc/7RET0cKy1egiywAEzoM0tE+JtyAWuIYCax2yYi7cGjdB6OuuEivqpsq/qeXwzInQpWP6wTipEcCiYTUIlaasqDV2KU/8490V+qkF3uLQ/EQozl6O4hlyLqvumzAOiapnno/munQaKeZhLmJqLRxL7MK0iQuWyR3bifpipffqdOkSSQhIiSWR4chuQMoBRZ3cf0lQiW50wbOYUeZJNOU8GHtA2UvH4xP5RD8tapdRAgtSI6q6iwP1wzJOXbx6tv4WoQVUAdXdETSkNqiml888u89sAhuXjtF3CNIvqtUaYiDlCoro7ot1/s+zEcvpkNovQ2bxUN5j5vYc9rLm6e7VAzm3y8lZTFVUiwqKW9v6HigWqh4apRtacdOmylXBXEVuKpIr7KTfNzO4iMqIihtqm7skerRRgFZGQMLMqaiEWWAZ3wl2RxMzMv+3S+1IqFXD3WjzM5rCK8BAJmHsOPw+Hi4jCWy9QRRFa3dwlqhU3kRAzhEPasytIktpobJu5BR4R5i6zlRr53HtuvaqwsFgenGwfCe0HU0qqKtUjJETm6iFLuJiFhiDsvHptYr9iPqPNR604zfUokDaSvJSfa/3xk316umCqHc28mkUWlRRo11TaB+IfZQ/rLEFSkiSq0BC92X1+sCI5yVbu5nfF97FNFcPN/AYR4VU8pFhLLniILoOCDw9vAdKFgeWPAus0xV4kDQFkCEmuOTS5JBHdpHrUEi+v3LLaZrLraS5j1XmqwFJR/Dz0FfeFKT7Y/JQgyaU9VqbhZiEIma2cbl+IcxwAaJfATuPcy1cAMmxKrVFt0CQqtdpc5uNc83gVgaTw8PO1SY1YStthuQBXoAVhEaIZEnBrM+y2U5IESSkjzCyOtWTJ1WI2+CkLlhIw7zVorQ4r5gZL0WLI3WuyVMzhimSCEDhQEeOplLUEvNpsdVZD0ryUXgsVRYSHV7dqVYYREe6l2FIwovWrzSzck4Y5ilu4VKQ4X/y16hLVl0rjpjGsHFrehn9kyWj3KB6mkdMIWb7t7bMBSKc+b3vbQ8UNgahWdRig7Vzdzcwkt1fF6QTyfsTHvNZazzUBVNswQnXzWqrrOUQe7gDX09iqQlh4+HJiJhGlWqi4u8BFxELa3cQHn1NKKVJLKztfx3gwqHL+VHD3Og7n7+PBJ6WWKhIqyCKDwqEu+iMVUQXOH+/PqOJtxj/95//6dzdf3uqkkBeoun+306fHnqbj03eI1nrZqd+atXvC174/X01Pfj+lp5/+l5Xv55XtfTCR9pJZ7Z9IT6/PoN805VB84DxAczq5uB+oevj1/Je1rn6xp78/pIdnEPd9zLXln3/Fo/V58sXf8HvTaSbVD5fzZHsKnn5fAuWi9mld0Ufwft/kvXYr5cnXrym+dur6jRNtf2AYnt7etf3NYuWG0vT0998Nr6bd778sX/+Lf/ZPXgFTnCbw6/0q4W3GV47tZzrgbaqQQM5ps6kDXj/5+sP0dDCl5w7Wld3oauVOdF/r2K1cA9CVKyqq94dEuzOgrc2ly2lOx9Xy5zwHOdBmucG5F//w8sV7Y4JrxWXi6XYQq6dh5dPgMiCyuvzzHv1offBBRrSv48qd2ecG+nA5T7fnWrNd2D5LL/r+lfHo9Y9ke/r1a/La+66Xve9rvaC1/S2t3M+4dmP2L3Z/eZPtr11VP0LUkCF6Gg5+Ts/fwzrskMff3+6nNy8roMAAaMW4smn27Jt8mXRh86weFxf+3lgNjh+nHWL9P55vHGN96vmfwKPwz7TJK99/7u29dLf6SpAAO8xvNiooMEOawkPWZx/5KD7BKaFBxeFQESQJoARUkNYeDlx5az7Wel54ePlKV0flss5prMznJT+ZQy+WS9yXPVHgUe87cd+4Ld+2/MevxHJRZa2dL53UcbVuzkdpn1g55C9/f3/oen7f9bn0iREFHGIRs4gGkuATPJnz/IF1C4TXSbNgTu2W/hAgrfUgfhpzi367tWaTH5C3P6Srcl7+Nyzk4To8etm3/vilu8l3Wf4P2fW+y3Iubc/v0T7fugmXtvOlvveK4Qe24XEXijQM+zKPw/VycbpifN4O1ifoYQGoBUlgjnajWqkAfLvSc4mfSo+jWRtyiJWPkrUarpe28lor/HjXZ35qJys/rf3kY5GVnk48+0f5ZT2ywFQ8VNOh+tgKUACIdmPtc7bPcwfWjOURu1qRM87zI6wF8YV3KTw7u7AOyMfp0H+85XwsP9b6/NTa4bmtja37MwfB9xg6NouURJaywVgeZHjmcffn72H5HpoBiUguYvA2KrTxp8cmjivb+7HWcq0515Y/xYU/sHYoybe9kY/fiB882vzkO/vgmtzqfz29tO+8Xd98dvHofx+tzxOzWK5Fkzy9nLXVWLtaESvLWWuftfVcbbcLX7+2ud89B77jb3y8gfHdXnZujdP0AQGb92mbgQoY8OI7r+j38UlOCYmIPoaf2E0ERETrGFhE1A0GFhF1g4FFRN1gYBFRNxhYRNQNBhYRdYOBRUTdYGARUTcYWETUDQYWEXWDgUVE3WBgEVE3GFhE1A0GFhF1g4FFRN1gYBFRNxhYRNQNBhYRdYOBRUTdYGARUTcYWETUDQYWEXWDgUVE3WBgEVE3GFhE1A0GFhF1g4FFRN1gYBFRNxhYRNQNBhYRdYOBRUTdYGARUTcYWETUDQYWEXWDgUVE3WBgEVE3GFhE1A0GFhF1g4FFRN1gYBFRNxhYRNQNBhYRdYOBRUTdYGARUTcYWETUDQYWEXWDgUVE3WBgEVE3GFhE1A0GFhF1g4FFRN1gYBFRNxhYRNQNBhYRdYOBRUTdYGARUTcYWETUDQYWEXWDgUVE3WBgEVE3GFhE1A0GFhF1g4FFRN1gYBFRNxhYRNQNBhYRdYOBRUTdYGARUTcYWETUDQYWEXWDgUVE3WBgEVE3GFhE1A0GFhF1g4FFRN1gYBFRNxhYRNQNBhYRdYOBRUTdYGARUTcYWETUDQYWEXWDgUVE3WBgEVE3GFhE1A0GFhF1g4FFRN1gYBFRNxhYRNQNBhYRdYOBRUTdYGARUTcYWETUDQYWEXWDgUVE3WBgEVE3GFhE1A0GFhF1g4FFRN1gYBFRNxhYRNQNBhYRdYOBRUTdYGARUTcYWETUDQYWEXWDgUVE3WBgEVE3GFhE1A0GFhF1g4FFRN1gYBFRNxhYRNQNBhYRdYOBRUTdYGARUTcYWETUDQYWEXWDgUVE3WBgEVE3GFhE1A0GFhF1g4FFRN1gYBFRNxhYRNQNBhYRdYOBRUTdYGARUTcYWETUDQYWEXWDgUVE3WBgEVE3GFhE1A0GFhF1g4FFRN1gYBFRNxhYRNQNBhYRdYOBRUTdYGARUTcYWETUDQYWEXWDgUVE3WBgEVE3GFhE1A0GFhF1g4FFRN1gYBFRNxhYRNQNBhYRdeP/AymVIk8A0+4+AAAAJXRFWHRkYXRlOmNyZWF0ZQAyMDE0LTExLTIwVDE0OjUxOjE5LTA1OjAwO/f96AAAACV0RVh0ZGF0ZTptb2RpZnkAMjAxNC0xMS0yMFQxNDo1MToxMy0wNTowMO7aGhoAAAARdEVYdGpwZWc6Y29sb3JzcGFjZQAyLHVVnwAAACB0RVh0anBlZzpzYW1wbGluZy1mYWN0b3IAMXgxLDF4MSwxeDHplfxwAAAAEnRFWHR4YXBNTTpEZXJpdmVkRnJvbQACsExZAAAAAElFTkSuQmCC
													</d>
												</u>
											</l>
											';
									}
									
									$resultado_ws_fotos = $this->metodoEnvioLecturasToDistriluz($url_fotos, $parameters_foto);
									
									$fecha_envio = date("Y-m-d H:i:s");
									$xml_respuesta = json_encode($resultado_ws_fotos);
									if($resultado_ws_fotos['@attributes']['i']=='0'){
										$estado_foto = '1';
									}else{
										$estado_foto = '0';
									}
		
									$sql_log = "UPDATE lecturas.comlec_ordenlecturas SET ws_l_estado = '$estado_foto', ws_f_fechaenvio = '$fecha_envio', ws_f_estado = '$estado_foto', ws_f_xml = '$xml_respuesta' WHERE suministro = '$suministro'";
									pg_query($sql_log);							
								}
							}
						}		
					}
				}
				
				$sql_update_xml = "UPDATE lecturas.comlec_xml SET procesando = false WHERE id in (".$str_xml_ids.")";
				pg_query($sql_update_xml);

				$sql_count_ordenes_enviadas_xml = "SELECT comlec_xml_id, count(*) as cantidad FROM lecturas.comlec_ordenlecturas WHERE ws_l_estado='1' and comlec_xml_id in (".$str_xml_ids.") group by comlec_xml_id ";
				$arr_count_enviadas = pg_query($sql_count_ordenes_enviadas_xml);
				$num_rows_count_enviadas = pg_num_rows($arr_count_enviadas);
				if($num_rows_count_enviadas > 0){
					while($obj_orden_xml=pg_fetch_object($arr_count_enviadas)) {
						$sql_count_ordenes_xml = "SELECT count(*) as cantidad FROM lecturas.comlec_ordenlecturas WHERE comlec_xml_id = ".$obj_orden_xml->comlec_xml_id." ";
						$arr_count_ordenes_xml = pg_query($sql_count_ordenes_xml);
						$num_rows_count_ordenes_xml = pg_num_rows($arr_count_ordenes_xml);
						if($num_rows_count_ordenes_xml > 0){
							while($obj_count_orden_xml=pg_fetch_object($arr_count_ordenes_xml)) {
								if($obj_orden_xml->cantidad==$obj_count_orden_xml->cantidad){

									$sql_actualiza_expor = "update lecturas.comlec_ordenlecturas1 set lecturao=a.lecturao,codlectura=a.obs,flectura='".date('Y-m-d H:i:s')."' from
									(select suministro, lecturao1 as lecturao,
									(select codigofac from lecturas.comlec_observaciones where cast(codigocamp as varchar)=lecturas.comlec_ordenlecturas.obs1 )
									as obs, now() as fecha
									from lecturas.comlec_ordenlecturas  where COMLEC_XML_ID in (".$obj_orden_xml->comlec_xml_id.") and resultadoevaluacion='CONSISTENTE'
									) a
									where lecturas.comlec_ordenlecturas1.suministro=a.suministro 				
									and lecturas.comlec_ordenlecturas1.COMLEC_XML_ID in (".$obj_orden_xml->comlec_xml_id.") ";
									pg_query($sql_actualiza_expor);
									
									$sql_actualiza_exportado = "update lecturas.comlec_xml set 
											exportadastotal=(select count(*) from  lecturas.comlec_ordenlecturas1 where comlec_xml_id=lecturas.comlec_xml.id), 
											exportadasvalidadas=(select count(*) from lecturas.comlec_ordenlecturas where comlec_xml_id=lecturas.comlec_xml.id and resultadoevaluacion='CONSISTENTE'), 
											fechaexportacion='".date('Y-m-d H:i:s')."' where id in (".$obj_orden_xml->comlec_xml_id.") ";
									pg_query($sql_actualiza_exportado);
								}
							}
						}
					}
				}

				exec("/var/www/html/lecturas/app/Console/cake -app /var/www/html/lecturas/app NotificationLecturas send_email_finalizado_envio_distriluz ".$negocio);

			}
			return 1;
		} catch (Exception $e) {
			error_log(date('Y-m-d H:i:s')." Error: ".$e." \n", 3, 'errorWSDistriluz-'.$this->negocio.'-'.date('Y-m-d').'.log');

			if($str_xml_ids!=''){
				$sql_update_xml = "UPDATE lecturas.comlec_xml SET procesando = false WHERE id in (".$str_xml_ids.")";
				pg_query($sql_update_xml);
			}

			return 0;
		}
	}
	
	public function applyWatermark($image_base64, $fecha) {
	
		$data = base64_decode($image_base64);
		$str_marca = explode(' ',$fecha);
	
		$im = imagecreatefromstring($data);
		if ($im !== false) {
		  
			$color_texto = imagecolorallocate($im, 246, 246, 85);
				
			$alto_imagen = imagesy($im)-10;
			$ancho_imagen = imagesx($im)-40;

			imagestring($im, 0, 3, $alto_imagen, $str_marca[0], $color_texto);
			imagestring($im, 0, $ancho_imagen, $alto_imagen, $str_marca[1], $color_texto);
	
			ob_start ();

			imagepng ($im);
			$image_data = ob_get_contents ();
				
			ob_end_clean ();
				
			$image_data_base64 = base64_encode ($image_data);
			return $image_data_base64;
		}
		else {
			error_log(date('Y-m-d H:i:s')." Imagen: ".$image_base64." \n", 3, 'applyWatermark-'.$this->negocio.'-'.date('Y-m-d').'.log');
			return 'Ocurrio un error.';
		}
	}
	
	public function getFactorDeCompensacion($suministro,$consumo,$lecturao) {
		
		if(trim($lecturao)==''){
			return '';
		}
		$lecturao_tmp = (int) $lecturao;
		
		if($consumo >= 10 && $consumo <= 100){
			$c1 = 10;
			$c2 = 100;
			$f_min = 0.1;
			$f_max = 0.3;
		}elseif($consumo >= 101 && $consumo <= 300){
			$c1 = 101;
			$c2 = 300;
			$f_min = 0.31;
			$f_max = 0.55;			
		}elseif($consumo >= 301 && $consumo <= 600){
			$c1 = 301;
			$c2 = 600;
			$f_min = 0.55;
			$f_max = 0.80;			
		}elseif($consumo >= 601 && $consumo <= 1000){
			$c1 = 601;
			$c2 = 1000;
			$f_min = 0.80;
			$f_max = 0.98;
		}elseif($consumo >= 1001 && $consumo <= 10000){
			$c1 = 1001;
			$c2 = 10000;
			$f_min = 0.50;
			$f_max = 0.98;
		}
		
		if($consumo >= 10000){
			$factor = 0.99;
		}else{
			$factor_tmp = ($f_max - $f_min) / ($c2 -$c1);
			$factor = $f_min + ( $lecturao_tmp  - $c1 ) * $factor_tmp;
		}
		
		return $factor;		
	}
	
	public function recuperarDataProcesoCerrado($fecha) {
		try {
			$cnx = loadModel('database',$this->negocio);
			$db = $cnx->getConnection();
			$sql_orden_enviada = "select * from auditoria.comlec_ordenlecturas_after_close_process where created >= '".$fecha."'";
			$arr_obj_orden_enviada = pg_query($sql_orden_enviada);
			$num_rows_orden_enviada = pg_num_rows($arr_obj_orden_enviada);
			
			$json_request = '';
			while($obj = pg_fetch_object($arr_obj_orden_enviada)) {
				$obj->suministro;
				$json_request .= $obj->json_request.',';
				
			}
			$json_lecturas = '['.substr($json_request,0,-1).']';

			return $this->SaveOt(json_decode($json_lecturas));
		} catch (Exception $e) {
			error_log(date('Y-m-d H:i:s')." Error: ".$e." \n", 3, 'recuperarDataProcesoCerrado-'.$this->negocio.'-'.date('Y-m-d').'.log');
			return 0;
		}
	}
	
	public function facturar ($tarifa, $sector, $plancondicion, $consumo, $periodo, $suministro){
		
		$cnx = loadModel('database',$this->negocio);
		$db = $cnx->getConnection();
			
		$s="SET search_path = public;"; 
		pg_query($s);
		
		$sql="SELECT facturacion.calcularmontofacturacion46($tarifa , $sector, $plancondicion, $consumo, '$periodo', '$suministro') as cantidad";
		$pa = pg_query($sql);
		$obj = pg_fetch_object($pa);
		$result = 0;
		
		if($obj->cantidad == 1){
			$sql2="select * from facturacion.detallefacturas df
					inner join facturacion.conceptos c
					on df.idconcepto=c.idconcepto where df.suministro='$suministro' and c.verenrecibo=true order by c.orden asc";
			$paq = pg_query($sql2);
			$result = pg_fetch_all($paq);
		}
		return  $result;
	
	}
	
}
?>
