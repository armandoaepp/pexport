<?php
class Observaciones{
	
	public $negocio = '';
	
	public function getObservacion(){
		$cnx = loadModel('database', $this->negocio);	
		$db = $cnx->getConnection();
		$sql = "select * from  lecturas.comlec_observaciones";
		$arr_obj_observacion = pg_query($sql);

		$num_rows = pg_num_rows($arr_obj_observacion);
		if($num_rows > 0){
			$data = array();
			while($obj_observacion=pg_fetch_object($arr_obj_observacion)) {
				$data[] = $obj_observacion;
			}
			return $data;
        } else {
            return 0;
        }
	 
	}
	
}
?>