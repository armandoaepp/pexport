<?php
require '../vendor/PHPMailer/PHPMailerAutoload.php';

class SendEmail{
	
	public $to = array();
	public $cc = array();
	public $subject = '';
	public $body = '';
	
	public function send(){
		date_default_timezone_set('America/Lima');
		
		$mail = new PHPMailer;
		
		$mail->isSMTP();
		
		$mail->setLanguage('es', '../vendor/PHPMailer/language/');
		
		$mail->SMTPDebug = 0;
		
		$mail->Debugoutput = 'html';
		
		$mail->Host = 'smtp.gmail.com';
		
		$mail->Port = 587;
		
		$mail->SMTPSecure = 'tls';
		
		$mail->SMTPAuth = true;
		
		$mail->Username = "ddanalytics05@gmail.com";
		
		$mail->Password = "QWERT1234";
		
		$mail->setFrom('ddanalytics05@gmail.com', 'SIGOF');
		
		foreach ($this->to as $k => $v){
			$mail->addAddress($k, $v);
		}
			
		foreach ($this->cc as $k => $v){
			$mail->addCC($k, $v);
		}

		$mail->isHTML(true); 
		
		$mail->Subject = $this->subject;
		$mail->Body    = $this->body;

		if (!$mail->send()) {
		    echo "Mailer Error: " . $mail->ErrorInfo;
		} else {
		    return true;
		}
	}
}