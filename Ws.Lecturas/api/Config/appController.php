<?php
function debug ($what){
    echo '<pre>';
    if (is_array($what)) {
		print_r ($what);
    }else{
        var_dump ($what);
    }
    echo '</pre>';
}

function loadModel($modelName,$negocio){
	if($modelName=='database'){
		$modelPath = '../Config/'.$negocio.'_'.$modelName.'.php';
		if(is_file($modelPath)){
			if (!class_exists($modelName)) {
				require_once($modelPath);
			}		
		}else{
			echo 'Not exist this conexion'.$modelPath;
			exit();
		}
		$model = new $modelName();
		return $model;	
	}else{
		$modelPath = '../Model/'.$modelName.'.php';
		if(is_file($modelPath)){
			if (!class_exists($modelName)) {
				require_once($modelPath);
			}
		}else{
			echo 'Not exist this model '.$modelPath;
			exit();
		}
		$model = new $modelName();
		if($negocio != 'null'){
			$model->negocio = $negocio;
		}		
		return $model;
	}
}
?>