<?php
require '../Slim/Slim.php';
require '../Config/appController.php';
 
$app = new Slim();

$app->post('/login','getLogin');
$app->post('/loginweb','getLoginWeb');
$app->post('/respuesta','respuestaServ');
$app->post('/recuperarOTProcesoCerrado','recuperarOTProcesoCerrado');
//$app->post('/login',function() use ($app){

//});
$app->post('/allot','getOt');
$app->post('/allobservacion','getObservacion');
$app->post('/saveot','setSaveOt');

$app->post('/register', 'createUser');
$app->get('/update/:id/:usuario/:pass', 'editUser');

$app->post('/sendtoconsistentes','enviarLecturasConsistentesToDistriluz');
$app->run();

	/**
	 * Agregando capa intermedia para autenticar todas las solicitudes
	 * Verifica si la solicitud tiene API KEY valida en el HEADER de la 'Autorizacion'
	 * @return bolean
	 * @version 18 Enero 2014
	 * @author Joel Vasquez Villalobos.
	 */
	function autenticate() {		
		$response = array();
		$app = Slim::getInstance()->response();
		$request = Slim::getInstance()->request();
		$headers = $request->headers();
		
		// Verifica Authorization en el Header
		if (isset($headers['apikey'])) {
			$model_usuario = loadModel('Usuario','cix');

			$api_key = $headers['apikey'];
			
			// Valida el Api key
			if (!($model_usuario->isValidApiKey($api_key))){			
				// Si el Api key no esta en la tabla usuario	
				$response['success'] = false;
				$response["message"] = "Acceso denegado. Invalido Api key";
				$response["data"] = array();	
				$app->status(401);
				$app->header('Content-Type', 'application/json');
				echo json_encode($response);
				exit();
			}
		} else {
			$response['success'] = false;
			$response["message"] = "Api key is misssing";
			$response["data"] = array();
			$app->status(401);
			$app->header('Content-Type', 'application/json');
			echo json_encode($response);
			exit();
		}
	}
	
	/**
	 * Hace que un equipo sea unico por persona logeada.
	 * Valida IMEI no esta en la tabla usuario Dispositivo
	 * @return bolean
	 * @version 11 Junio 2014
	 * @author Joel Vasquez Villalobos.
	 */
	function validateImei(){	
		$response = array();
		$app = Slim::getInstance()->response();
		$request = Slim::getInstance()->request();
		$headers = $request->headers();
		if (isset($headers['imei'])) {
			$model_usuario = loadModel('Usuario','cix');
			
			$id_usuario = $model_usuario->getUserIdByUser($headers['usuario']);
			if($id_usuario!=false){
				$imei= $headers['imei'];				
				// Valida el IMEI
				if (!($model_usuario->isValidImei($id_usuario,$imei))){			
					// Si el IMEI no esta en la tabla usuario Dispositivo
					$response['success'] = false;
					$response["message"] = "Acceso denegado.";				
					$app->status(401);
					$app->header('Content-Type', 'application/json');
					echo json_encode($response);
					exit();
				}
			}else{
				$response['success'] = false;
				$response["message"] = "Usuario no Existe o Clave Incorrecta.";				
				$app->status(401);
				$app->header('Content-Type', 'application/json');
				echo json_encode($response);
				exit();
			}
		} else {
			$response['success'] = false;
			$response["message"] = 'Acceso denegado. Missing Codigo';			
			$app->status(401);
			$app->header('Content-Type', 'application/json');
			echo json_encode($response);
			exit();
		}	
	}

	/**
	 * Hace la peticion de inicio de sesion de usuarios Movil.
	 * @param string $usuario
	 * @param string $pass
	 * @return bolean
	 * @version 18 Enero 2014
	 * @author Joel Vasquez Villalobos.
	*/
	function getLogin(){

		$app = Slim::getInstance()->response();
		//$req = $app->request();
		//echo $req->post('usuario');
		//$book = $app->request()->params('usuario');
		//$data = $app->request()->post('usuario');
		
		if(isset($_POST["usuario"]) && !empty($_POST["usuario"])){
			$usuario = $_POST['usuario'];
		}else{
			$response["error"] = true;
			$response["message"] = "El Usuario esta vacia";
			$response['success'] =false;
			$app->status(402);
			header('Content-Type: application/json; charset=UTF-8'); 
			echo json_encode($response);
			exit();
		}
		
		if(isset($_POST["pass"]) && !empty($_POST["pass"])){
			$pass = $_POST['pass'];
		}else{
			$response["error"] = true;
			$response["message"] = "La Contraña esta vacia";
			$response['success'] = false;
			$app->status(402);
			header('Content-Type: application/json; charset=UTF-8'); 
			echo json_encode($response);
			exit();
		}
		
		try {
			$model_usuario = loadModel('Usuario','cix');	
			$arr_obj_usuario = $model_usuario->validarUsuario($usuario , $pass);
			$app->header('Content-Type', 'application/json');
			$app->status(200);
			echo json_encode($arr_obj_usuario);
			exit();
			
		} catch(Exception $e){
			$response["error"] = true;
			$response["message"] = "Ha ocurrido un error inesperado";
			$response['success'] = $e->getMessage();
			$app->status(404);
			$app->header('Content-Type', 'application/json');
			echo json_encode($response);
			error_log('getLoginError: '.$e, 3, 'login.log');
		}
	}
	
	/**
	 * Hace la peticion de inicio de sesion para los usuarios de la web.
	 * @param string $usuario
	 * @param string $pass
	 * @return bolean
	 * @version 18 Enero 2014
	 * @author Joel Vasquez Villalobos.
	 */
	function getLoginWeb(){
	
		$app = Slim::getInstance()->response();
	
		if(isset($_POST["usuario"]) && !empty($_POST["usuario"])){
			$usuario = $_POST['usuario'];
		}else{
			$response["error"] = true;
			$response["message"] = "El Usuario esta vacia";
			$response['success'] =false;
			$app->status(402);
			header('Content-Type: application/json; charset=UTF-8');
			echo json_encode($response);
			exit();
		}
	
		if(isset($_POST["pass"]) && !empty($_POST["pass"])){
			$pass = $_POST['pass'];
		}else{
			$response["error"] = true;
			$response["message"] = "La Contraña esta vacia";
			$response['success'] = false;
			$app->status(402);
			header('Content-Type: application/json; charset=UTF-8');
			echo json_encode($response);
			exit();
		}
	
		try {
			$model_usuario = loadModel('Usuario','cix');
			$arr_obj_usuario = $model_usuario->validarUsuarioWeb($usuario , $pass);
			$app->header('Content-Type', 'application/json');
			$app->status(200);
			echo json_encode($arr_obj_usuario);
			exit();
				
		} catch(Exception $e){
			$response["error"] = true;
			$response["message"] = "Ha ocurrido un error inesperado";
			$response['success'] = $e->getMessage();
			$app->status(404);
			$app->header('Content-Type', 'application/json');
			echo json_encode($response);
			error_log('getLoginError: '.$e, 3, 'login.log');
		}
	}
	
	/**
	 * Envia al movil las ordenes que se le han sido asignadas al usuario logeado.
	 * @param string $usuario
	 * @return array json
	 * @version 11 Junio 2014
	 * @author Joel Vasquez Villalobos.
	 */
	function getOt(){
		$model_orden_lecturas = loadModel('OrdenLecturas','cix');
		$model_usuario = loadModel('Usuario','cix');

		$app = Slim::getInstance()->response();
		$response = array();
		
		if(isset($_POST["usuario"]) && !empty($_POST["usuario"])){
			$usuario = $_POST['usuario'];
		}else{
			$response["error"] = true;
			$response["message"] = "El Usuario esta vacia";
			$response['success'] =false;
			$app->status(402);
			header('Content-Type: application/json; charset=UTF-8'); 
			echo json_encode($response);
			exit();
		}
		
		try {
			$usuario_id = $model_usuario->getUserIdByUser($usuario);
			$arr_obj_orden_lectura = $model_orden_lecturas->getDataOrdenLecturaByLecturista($usuario_id);		
			if($arr_obj_orden_lectura){
				$app->header('Content-Type', 'application/json');
				$app->status(200);
				$response['success'] = true;
				$response["message"] = "Todo correcto";
				$response["datos"] = $arr_obj_orden_lectura;
				echo json_encode($response);
			}else{
				$app->header('Content-Type', 'application/json');
				$app->status(401);
				$response['success'] = false;
				$response["message"] = "No hay Ordenes Asignadas para Usted";
				$response["datos"] = array();
				echo json_encode($response);
				exit();
			}
		} catch (Exception $e) {
		    error_log($e, 3, 'getOt.log');
		    $app->header('Content-Type', 'application/json');
		    $app->status(402);
			$response = array();
			$response["error"] = $e;
			$response["message"] = "Genero un error al consultar descarga";
			$response['success'] =false;
			$response["datos"] = array();
			echo json_encode($response);
			exit();
		}
	}

	/**
	 * Crea usuarios y contrasenias cifradas ademas verifica un usuario duplicado.
	 * @param string $user, $pass
	 * @return array json
	 * @version 11 Junio 2014
	 * @author Joel Vasquez Villalobos.
	 */
	function createUser() {
		$model_usuario = loadModel('Usuario','cix');
		$app = Slim::getInstance()->response();
		
		if(isset($_POST["usuario"]) && !empty($_POST["usuario"])){
			$user = $_POST['usuario'];
		}else{
			$response["error"] = true;
			$response["message"] = "El Usuario esta vacia";
			$response['success'] =false;
			$app->status(402);
			header('Content-Type: application/json; charset=UTF-8');
			echo json_encode($response);
			exit();
		}
		
		if(isset($_POST["pass"]) && !empty($_POST["pass"])){
			$pass = $_POST['pass'];
		}else{
			$response["error"] = true;
			$response["message"] = "La Contraña esta vacia";
			$response['success'] = false;
			$app->status(402);
			header('Content-Type: application/json; charset=UTF-8');
			echo json_encode($response);
			exit();
		}
		
		if(isset($_POST["empleado"]) && !empty($_POST["empleado"])){
			$empleado = $_POST['empleado'];
		}else{
			$response["error"] = true;
			$response["message"] = "No existe este empleado";
			$response['success'] = false;
			$app->status(402);
			header('Content-Type: application/json; charset=UTF-8');
			echo json_encode($response);
			exit();
		}
		
		
		$response = array();
		try {
			$res = $model_usuario->createUser($user,$pass,$empleado);
				
			if ($res == 'USER_CREATED_SUCCESSFULLY') {
                $response["error"] = false;
                $response["message"] = "You are successfully registered";
            } else if ($res == 'USER_CREATE_FAILED') {
                $response["error"] = true;
                $response["message"] = "Oops! An error occurred while registereing";
            } else if ($res == 'USER_ALREADY_EXISTED') {
                $response["error"] = true;
                $response["message"] = "Sorry, this user already existed";
            }
			$app->status(201);
			$app->header('Content-Type', 'application/json');
			echo json_encode($response);			
			
		} catch(Exception $e) {
			echo '{"error":{"text":'. $e .'}}'; 
			$response["error"] = true;
			$response["message"] = "Api key is misssing";
			$app->status(404);
			$app->header('Content-Type', 'application/json');
			echo json_encode($response);
		}
	}
	
	/**
	 * edita usuarios y contrasenias cifradas ademas verifica un usuario duplicado.
	 * @param string $user, $pass
	 * @return array json
	 * @version 18 Julio 2014
	 * @author geynen.
	 */
	function editUser($id, $user,$pass) {
		$model_usuario = loadModel('Usuario','cix');
		$app = Slim::getInstance()->response();
	
		$response = array();
		try {
			$res = $model_usuario->editUser($id,$user,$pass);
	
			if ($res == 'USER_CREATED_SUCCESSFULLY') {
				$response["error"] = false;
				$response["message"] = "You are successfully Updated";
			} else if ($res == 'USER_CREATE_FAILED') {
				$response["error"] = true;
				$response["message"] = "Oops! An error occurred while Updating";
			} else if ($res == 'USER_ALREADY_EXISTED') {
				$response["error"] = true;
				$response["message"] = "Sorry, this user already existed";
			}
			$app->status(201);
			$app->header('Content-Type', 'application/json');
			echo json_encode($response);
				
		} catch(Exception $e) {
			//echo '{"error":{"text":'. $e->getMessage() .'}}';
			$response["error"] = true;
			$response["message"] = "Api key is misssing";
			$app->status(404);
			$app->header('Content-Type', 'application/json');
			echo json_encode($response);
		}
	}
	
	/**
	 * Envia al movil todas las observaciones en Codigos y Descripcion.
	 * @param NULL
	 * @return array json
	 * @version 11 Junio 2014
	 * @author Joel Vasquez Villalobos.
	 */
	function getObservacion() {
		$model_observacion = loadModel('Observaciones','cix');

		$app = Slim::getInstance()->response();
		$response = array();
		
		$arr_obj_observacion = $model_observacion->getObservacion();	
		if($arr_obj_observacion){
			$app->header('Content-Type', 'application/json');
			$app->status(200);
			$response['success'] = true;
			$response["message"] = "Todo correcto";
			$response["datos"] = $arr_obj_observacion;
			echo json_encode($response);
		}else{
			$app->header('Content-Type', 'application/json');
			$app->status(401);
			$response['success'] = false;
			$response["message"] = "No hay observaciones";
			$response["datos"] = array();
			echo json_encode($response);
			exit();
		}
	}

	/**
	 * Actualiza el estado de las lecturas descargadas a verificado la descarga (descarga correcta)
	 * @param array json
	 * @return array json
	 * @version 11 Junio 2014
	 * @author Joel Vasquez Villalobos.
	 */
	function respuestaServ() {
		$request = Slim::getInstance()->request();
		$app = Slim::getInstance()->response();
		$arr_json = $request->getBody();
		if(!isJSON($arr_json)){
			$response = array();
			$response["error"] = true;
			$response["message"] = "Json Incorrecto";
			$response['success'] = false;
			$app->status(402);
			header('Content-Type: application/json; charset=UTF-8');
			echo json_encode($response);
			exit();
		}
		$ordenes=json_decode(utf8_encode($arr_json));
		
		if(!isset($ordenes->Datos)){
			$response = array();
			$response["error"] = true;
			$response["message"] = "No se envio el indice Datos";
			$response['success'] =false;
			$app->status(402);
			header('Content-Type: application/json; charset=UTF-8');
			error_log($arr_json, 3, 'respuestaServ.log');
			echo json_encode($response);
			exit();
		}
	
		$model_orden = loadModel('OrdenLecturas','cix');

		try {
			$arr_obj_orden= $model_orden->setUpdateBeforeSync($ordenes);
			
			if($arr_obj_orden == 0){
				$response = array();
				$response["error"] = true;
				$response["message"] = "No se valido la descarga de OL";
				$response['success'] =false;
				$response["datos"] = $arr_obj_orden;
				$app->status(402);
				header('Content-Type: application/json; charset=UTF-8');
				echo json_encode($response);
				exit();
			}else {
				$response = array();
				$response["error"] = false;
				$response["message"] = "Datos Correctos";
				$response['success'] =true;
				$response["datos"] = $arr_obj_orden;
				$app->status(200);
				header('Content-Type: application/json; charset=UTF-8');
				echo json_encode($response);
				exit();				
			}
		} catch (Exception $e) {
		    error_log($e, 3, 'respuestaServCatch.log');
			$response = array();
			$response["error"] = true;
			$response["message"] = "Genero un error al consultar actualizar descarga";
			$response['success'] =false;
			$response["datos"] =  $e;
			$app->status(402);
			header('Content-Type: application/json; charset=UTF-8');
			echo json_encode($response);
			exit();
		}
	}
	/**
	 * Valida que la cadena enviada sea un JSON.
	 * @param array json
	 * @return True O False
	 * @version 21 agosto 2014
	 * @author Joel Vasquez Villalobos.
	 */
	function isJSON($string){
		json_decode($string);
		return (json_last_error() == JSON_ERROR_NONE);
	}

	/**
	 * Guarda los valores de lecturas y resultados CONSISTENTES O INCONSISTENTES y finaliza la orden lectura.
	 * @param array json
	 * @return array json
	 * @version 11 Junio 2014
	 * @author Joel Vasquez Villalobos.
	 */
	function setSaveOt(){
		try {
			$model_orden_lecturas = loadModel('OrdenLecturas','cix');
			$request = Slim::getInstance()->request();
			$app = Slim::getInstance()->response();
			$arr_json = $request->getBody();	
			error_log(date('Y-m-d H:i:s')." json: ".$arr_json." \n", 3, 'setSaveOt-'.date('Y-m-d').'.log');

			if(!isJSON($arr_json)){
				$response = array();
				$response["error"] = true;
				$response["message"] = "Json Incorrecto";
				$response['success'] = false;
				$app->status(402);
				header('Content-Type: application/json; charset=UTF-8');
				echo json_encode($response);
				exit();
			}
			$json_o2=json_decode($arr_json);
			$resultado = $model_orden_lecturas->SaveOtCix($json_o2);
			
			if($resultado == 0){
				$response = array();
				$response["error"] = true;
				$response["message"] = "Datos Incorrectos";
				$response['success'] =false;
				$app->status(402);
				header('Content-Type: application/json; charset=UTF-8');
				echo json_encode($response);
				exit();
			} else {
				$response = array();
				$response["error"] = false;
				$response["message"] = "Datos Correctos";
				$response['success'] =true;
				$app->status(200);
				header('Content-Type: application/json; charset=UTF-8'); 
				echo json_encode($response);
				exit();
			}
			
		} catch (Exception $e) {
		    error_log($e, 3, 'lectura_error.log');
			$response = array();
			$response["error"] = true;
			$response["message"] = "Genero un error al guardar Lecturas";
			$response['success'] =false;
			$app->status(402);
			header('Content-Type: application/json; charset=UTF-8');
			echo json_encode($response);
			exit();
		}
	}
	
	/**
	 * Envia Lecturas Consistentes al servicio web de Distriluz
	 * @param Post /
	 * @return array json
	 * @version 20 Noviembre 2014
	 * @author Joel Vasquez Villalobos, Geynen
	 */
	function enviarLecturasConsistentesToDistriluz(){
		$model_orden_lecturas = loadModel('OrdenLecturas','cix');
	
		$app = Slim::getInstance()->response();
		$response = array();
		
		if(isset($_POST['negocio'])){
			$negocio = $_POST['negocio'];
		}else{
			$app->header('Content-Type', 'application/json');
			$app->status(401);
			$response['success'] = false;
			$response["message"] = "Enviar el negocio $ _POST['negocio']";
			$response["datos"] = array();
			echo json_encode($response);
			exit();
		}
	
		try {
			$arr_obj_orden_lectura = $model_orden_lecturas->enviarLecturasConsistentesToDistriluz($negocio);
			if($arr_obj_orden_lectura){
				$app->header('Content-Type', 'application/json');
				$app->status(200);
				$response['success'] = true;
				$response["message"] = "Todo correcto, Lecturas Consistentes enviadas correctas";
				$response["datos"] = $arr_obj_orden_lectura;
				echo json_encode($response);
			}else{
				$app->header('Content-Type', 'application/json');
				$app->status(401);
				$response['success'] = false;
				$response["message"] = "No hay Lecturas Consistentes para enviar";
				$response["datos"] = array();
				echo json_encode($response);
				exit();
			}
		} catch (Exception $e) {
			error_log($e, 3, 'errorEnsa.log');
			$app->header('Content-Type', 'application/json');
			$app->status(402);
			$response = array();
			$response["error"] = $e;
			$response["message"] = "Genero un error al enviar lecturas a la Web Service";
			$response['success'] =false;
			$response["datos"] = array();
			echo json_encode($response);
			exit();
		}
	}
	
	function recuperarOTProcesoCerrado(){
		//echo 'init';
		$model_orden_lecturas = loadModel('OrdenLecturas','cix');
		
		$model_orden_lecturas->recuperarDataProcesoCerrado();
		//echo 'ok';
		exit;
	}
?>