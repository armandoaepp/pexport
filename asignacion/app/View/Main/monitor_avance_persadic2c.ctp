<?php
date_default_timezone_set('America/Lima');

//require_once "../conexion.php";
require_once(APP."View/Main/conexion.ctp");

$fechaactual=date("Y-m-d");


if(isset($_GET["idini1"])) 
{
  $idini1=$_GET["idini1"];
} else {
  $idini1=$fechaactual;
}


if(isset($_GET["idfin1"])) 
{
  $idfin1=$_GET["idfin1"];
} else {
  $idfin1=$fechaactual;
}




$querymondia2c = pg_query("select IdUsuario,NomPersona,ApePersona,
sum(GeneradaPexTotal) as GenPexTotal,
sum(GeneradaEnMes) as GenEnMes,
sum(GeneradaPexMes) as GenPexMes,
sum(GeneradaPexDia) as GenPexDia,
sum(GeneradaEnDia) as GenEnDia,
sum(FinalizadaDia) as FinDia,
sum(MesFin) as FinMes,
sum(TotalFin) as FinTotal,
((sum(GeneradaPexTotal))-sum(TotalFin)) as Pendientes,
((sum(TotalFin)*100)/sum(GeneradaPexTotal)) as Porcentaje 
from(
select 
ci.IdUsuario,
gp.NomPersona,
gp.ApePersona,
count(ci.IdEstado) as GeneradaPexTotal,
case when extract('month' from cast(co.FechaGeneracion as timestamp))=9  then count(ci.IdEstado) else 0 end as GeneradaEnMes,
case when extract('month' from ci.FechaAsignado)=9 then count(ci.IdEstado) else 0 end as GeneradaPexMes,
case when cast(ci.FechaAsignado as date)='$idfin1' then count(ci.IdEstado) else 0 end as GeneradaPexDia,
case when cast(co.FechaGeneracion as date)='$idfin1' then count(ci.IdEstado) else 0 end as GeneradaEnDia,
case when cast(ci.FechaAtencion as date)='$idfin1' and ci.IdEstado=15 then count(ci.IdEstado) else 0 end as FinalizadaDia, 
case when extract('month' from ci.FechaAtencion)=9 and ci.IdEstado=15 then count(ci.IdEstado) else 0 end as MesFin,  
case when ci.IdEstado=15 then count(ci.IdEstado) else 0 end as TotalFin  from ComMovTab_Intervencion ci
inner join ComMovTab_Orden co on ci.IdOrden=co.IdOrden
inner join ComMovTab_Usuario cu on ci.IdUsuario = cu.IdUsuario
inner join GlobalMaster_Persona gp on cu.IdPersona = gp.IdPersona
where co.SubActividad in (select DetalleTipoIntervencion from ComMovTab_TipoIntervencion)
and co.Condicion='0'
group by ci.IdUsuario,gp.NomPersona,gp.ApePersona,ci.FechaAtencion,ci.FechaAsignado,ci.IdEstado,co.FechaGeneracion) as superconsulta
group by IdUsuario,NomPersona,ApePersona");


											$pers2c="<div class='pers2'><div class='table-responsive'>
                                                      <table class='table table-bordered tableWithFloatingHeader' id='datatablepers2c' >
                                                      <thead class='primary-emphasis'>
                                                      <tr>
                                                          <th class='text-center primary-emphasis-dark'>Codigo</th>
                                                          <th class='text-center primary-emphasis-dark'>Tecnico Inspector</th>
                                                          <th class='text-center primary-emphasis-dark'>Ordenes Generadas</th>
                                                          <th class='text-center primary-emphasis-dark'>Finalizadas</th>
                                                          <th class='text-center danger-emphasis-dark'>Pendientes</th>
                                                          <th class='text-center primary-emphasis-dark'>Porcentaje de Avance</th>
                                                      </tr>
                                                      </thead>
                                                      <tbody>";
                                                        while ($retornocom2 = pg_fetch_object($querymondia2c)){
                                                                            $varidusuario=$retornocom2->idusuario ;
                                                                            $varnompersona=$retornocom2->nompersona;
                                                                            $varapepersona=$retornocom2->apepersona;

                                                                            
                                                                            $vargeneradasws=$retornocom2->genpextotal;
                                                                            $varfinalizadastot=$retornocom2->fintotal;

                                                                            $varpendientes=$retornocom2->pendientes;
                                                                            $varporcentajemon2=$retornocom2->porcentaje;


                                                                            $totaltecgen[]=$vargeneradasws;
                                                                            $totaltecfin[]=$varfinalizadastot;
                                    
          
                                                                            $totaltecpen[]=$varpendientes;

                                                                            if ($varporcentajemon2 < 33) {
                                                                              $color3='danger';
                                                                              
                                                                            } elseif ($varporcentajemon2 > 33 && $varporcentajemon2 < 70) {
                                                                              $color3='warning';
                                                                            } else {
                                                                              $color3='success';
                                                                            }

                                                                        

                                                        $pers2c.= "
                                                        <tr class='odd gradeX'>
                                                          <td><strong>$varidusuario</strong></td>
                                                          <td><strong>$varnompersona $varapepersona</strong></td>
                                                          <td><strong>$vargeneradasws</strong></td>
                                                          
                                                          <td><strong>$varfinalizadastot</strong></td> 
                                                          <td><strong>$varpendientes</strong></td>
                                                          <td class='center'><div class='progress progress-striped active'>
                                                                                  <div class='progress-bar progress-bar-$color3' style='width: $varporcentajemon2%'>".number_format($varporcentajemon2,2)."%</div>
                                                                                 </div></td>                    
                                                          </tr>";
                                                          
                                                        }

                                                          
                                                      $pers2c.="</tbody>
                                                      <tfoot >
                                                      <tr>
                                                      <td style='text-align:right;' colspan='2'><strong>Total</strong></td>
                                                      ";


                                                              $totaltecgenf=(!empty($totaltecgen))?array_sum($totaltecgen):0;
                                                              $totaltecfintotf=(!empty($totaltecfin))?array_sum($totaltecfin):0;
                                                              $totaltecpenf=(!empty($totaltecpen))?array_sum($totaltecpen):0;

                                                              $totalporc= ($totaltecgenf>=1)?($totaltecfintotf*100)/$totaltecgenf:0;
                                                              $totalporc= round($totalporc,0);
                                                              if ($totalporc < 33) {
                                                                              $color4='danger';
                                                                              
                                                                            } elseif ($totalporc > 33 && $totalporc < 70) {
                                                                              $color4='warning';
                                                                            } else {
                                                                              $color4='success';
                                                                            }
                                                              

                                                      $pers2c.="<td><strong>$totaltecgenf</strong></td>
                                                
                                                                                                  
                                                                                                   <td><strong>$totaltecfintotf</strong></td>
                                                                                                   <td><strong>$totaltecpenf</strong></td>
                                                                                                   <td>
                                                                                                      <div class='progress progress-striped active'>
                                                                                                          <div class='progress-bar progress-bar-$color4' style='width:$totalporc%'>".number_format($totalporc,2)."%</div>
                                                                                                      </div>
                                                                                                   </td>
                                                      </tr>
                                                      </tfoot>


                                                      </table>
                                              </div></div>";


                                              echo $pers2c;

?>

  <script type="text/javascript">

        function UpdateTableHeaders() {
            $("div.divTableWithFloatingHeader").each(function() {
                var originalHeaderRow = $(".tableFloatingHeaderOriginal", this);
                var floatingHeaderRow = $(".tableFloatingHeader", this);
                var offset = $(this).offset();
                var scrollTop = $(window).scrollTop();

                var navegador = navigator.userAgent;
                var inicioScroll = scrollTop-30;
                var marginheader = 30;
                if (navigator.userAgent.indexOf('MSIE') !=-1) {
                 
                } else if (navigator.userAgent.indexOf('Firefox') !=-1) {
                  inicioScroll = scrollTop-50;
                  marginheader = 40;
                } else if (navigator.userAgent.indexOf('Chrome') !=-1) {
                  inicioScroll = scrollTop-30;
                  var marginheader = 50;
                } else if (navigator.userAgent.indexOf('Opera') !=-1) {
                  inicioScroll = scrollTop-30;
                  marginheader = 40;
                } else {
                
                }

                if (( inicioScroll > offset.top ) && (scrollTop < offset.top + $(this).height())) {
                    floatingHeaderRow.css("visibility", "visible");
                    floatingHeaderRow.css("top", (Math.min(scrollTop - offset.top, $(this).height() - floatingHeaderRow.height()))+ marginheader  + "px");

                    // Copy cell widths from original header
                    $("th", floatingHeaderRow).each(function(index) {
                        var cellWidth = $("th", originalHeaderRow).eq(index).css('width');
                        var color = $("th", originalHeaderRow).eq(index).css('background-color');
                        
                        $(this).css('width', cellWidth);
                        $(this).css('background-color', color);
                    });

                    // Copy row width from whole table
                    floatingHeaderRow.css("width", $(this).css("width"));
                    floatingHeaderRow.css("background-color", $(this).css("background-color"));
                }
                else {
                    floatingHeaderRow.css("visibility", "hidden");
                    floatingHeaderRow.css("top", "0px");
                    
                }
            });
        }

        $(document).ready(function() {
            $("table.tableWithFloatingHeader").each(function() {
                $(this).wrap("<div class=\"divTableWithFloatingHeader\" style=\"position:relative\"></div>");

                var originalHeaderRow = $("tr:first", this)
                originalHeaderRow.before(originalHeaderRow.clone());
                var clonedHeaderRow = $("tr:first", this)

                clonedHeaderRow.addClass("tableFloatingHeader");
                clonedHeaderRow.css("position", "absolute");
                clonedHeaderRow.css("top", "0px");
                clonedHeaderRow.css("left", $(this).css("margin-left"));
                clonedHeaderRow.css("visibility", "hidden");
                clonedHeaderRow.css("z-index", "5");
                
                originalHeaderRow.addClass("tableFloatingHeaderOriginal");
            });

            UpdateTableHeaders();
            $(window).scroll(UpdateTableHeaders);
            $(window).resize(UpdateTableHeaders);
        });
</script>