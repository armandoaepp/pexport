<?php
date_default_timezone_set('America/Lima');

//require_once "../conexion.php";
require_once(APP."View/Main/conexion.ctp");

if(isset($_GET['criterios'])) {
	$criterio = $_GET['criterios'];
}else{
	$criterio = '';
}

$str_where = " ";
$str_where2 = " ";
$str_fecha_liquidacion = "";
if(strpos($criterio, 'Estado') !== false){
	if(isset($_GET['tags']) && $_GET['tags']!='') {
		$str_estados = "'".str_replace(",","','",$_GET['tags'])."'";
		if(strpos($str_estados, 'Descargado') !== false){
			$str_where2 .= " AND (NomEstado in (".$str_estados.") OR CI.IdEstado=9)";
		}else if(strpos($str_estados, 'Finalizado') !== false){
			$str_where2 .= " AND (NomEstado in (".$str_estados.") AND CI.IdEstado!=9)";
		}else{
			$str_where2 .= " AND (NomEstado in (".$str_estados."))";
		}	
	}
}
if(strpos($criterio, 'Ubicacion') !== false){
	if(isset($_GET['unidadneg']) && $_GET['unidadneg']!='0') {
		$str_where .= " AND co.IdUnidadNegocio = ".$_GET['unidadneg'];
		$str_where2 .= " AND co.IdUnidadNegocio = ".$_GET['unidadneg'];
	}
	if(isset($_GET['centroservicio']) && $_GET['centroservicio']!='0') {
		$str_where .= " AND co.IdCentroServicio = ".$_GET['centroservicio'];
		$str_where2 .= " AND co.IdCentroServicio = ".$_GET['centroservicio'];
	}
}
if(strpos($criterio, 'PeriodoLiquidacion') !== false){
	if(isset($_GET['periodoliquidacion']) && $_GET['periodoliquidacion']!='' && $_GET['periodoliquidacion']!='0') {
		$arr_periodo = explode('-',$_GET['periodoliquidacion']);
		$str_fecha_liquidacion = " AND extract('year' from FechaAtencion) = ".$arr_periodo[0];
		$str_fecha_liquidacion .= " AND extract('month' from FechaAtencion) = ".$arr_periodo[1];
		$str_where2 .= $str_fecha_liquidacion;
	}
}
if(strpos($criterio, 'RangoFechaA') !== false){
	if(isset($_GET['dpt_date_start'])) {
		$str_where2 .= " AND FechaAsignado >= '".$_GET['dpt_date_start']."'";
	}
	if(isset($_GET['dpt_date_end'])) {
		$str_where2 .= " AND FechaAsignado <= '".$_GET['dpt_date_end']." 23:59:59'";
	}
}
if(strpos($criterio, 'RangoFechaF') !== false){
	if(isset($_GET['dpt_date_start_finalizado'])) {
		$str_where2 .= " AND FechaAtencion >= '".$_GET['dpt_date_start_finalizado']."'";
	}
	if(isset($_GET['dpt_date_end_finalizado'])) {
		$str_where2 .= " AND FechaAtencion <= '".$_GET['dpt_date_end_finalizado']." 23:59:59'";
	}
}
if(strpos($criterio, 'Suministro') !== false){
	if(isset($_GET['txt_suministro']) && $_GET['txt_suministro']!='') {
		$str_where2 .= " AND suministro = ".$_GET['txt_suministro']."::TEXT";
	}
}
if(strpos($criterio, 'OT') !== false){
	if(isset($_GET['txt_orden_trabajo']) && $_GET['txt_orden_trabajo']!='') {
		$str_where2 .= " AND co.IdOrdenTrabajo = '".$_GET['txt_orden_trabajo']."'";
	}
}
if(strpos($criterio, 'FH') !== false){
	$str_fecha_finalizado_hoy = " AND FechaAtencion >= '".date('Y-m-d')." 00:00:00'";
	$str_where2 .= $str_fecha_finalizado_hoy." AND NomEstado in ('Finalizado')";
}
if(strpos($criterio, 'GH') !== false){
	$str_where2 .= " AND co.FechaGeneracion >= '".date('Y-m-d')." 00:00:00'";
}
if(strpos($criterio, 'Tecnico') !== false){
	if(isset($_GET['tecnico']) && $_GET['tecnico']!='') {
		$str_where2 .= " AND ci.IdUsuario = ".$_GET['tecnico'];
	}
}
if(strpos($criterio, 'I') !== false){
	$str_where2 .= " AND (select count(*) as infructuosa from ComMovTab_Detalle_Intervencion di inner join
ComMovTab_MaestrosIntervencion mi on di.IdMaestrosIntervencion=mi.IdMaestrosIntervencion
where di.IdIntervencion=Ci.IdIntervencion and  IdentificadorMaestroIntervencion like 'LabelInfructuosa%') > 0 ";
}
if(strpos($criterio, 'TT') !== false){
	if(isset($_GET['cbo_tiempotranscurrido']) && $_GET['cbo_tiempotranscurrido']!='') {
		$str_where2 .= " AND DATE_PART('day',NOW()-cast(FechaGeneracion as timestamp)) > ".$_GET['cbo_tiempotranscurrido'];
	}
}
/*
$unidad="26,30,29";
		$str_where .= " AND co.IdUnidadNegocio in( ".$unidad.")";
		$str_where2 .= $str_where;
		$centroser="258,272,255,273,270,271,251,268";
		$str_where .= " AND co.IdCentroServicio in( ".$centroser.")";
		$str_where2 .= $str_where;

echo 'a'.$str_where.'---<br>';
echo 'b'.$str_where2.'---<br>';
echo 'c'.$str_fecha_liquidacion.'--<br>';exit();*/

/*
if(isset($_GET['tags'])) {
$vartags=$_GET['tags'];
$tangente=$_GET['tags'];

$porciones = explode(",", $vartags);
$c=count($porciones);

$conocimiento=array("Asignado", "Exportado", "Descargado", "Finalizado");
$ori='';
$porcion='';
for ($i=0; $i < $c; $i++) { 
  if (in_array($porciones[$i], $conocimiento)) {
     $ori.=",".$porciones[$i];
     $porcion.=",'".$porciones[$i]."'";
  } else {
    echo "";
  }
}

$ori = substr($ori, 1);
$vartags = substr($porcion, 1);


if ($vartags==''){
  $vartags="'Asignado','Exportado','Descargado'";
  $origin="Asignado,Exportado,Descargado";
}else{
  /*
  $antes = array("Finalizado","Asignado","Exportado","Descargado");
  $despues   = array("'Finalizado'","'Asignado'","'Exportado'","'Descargado'");
  $vartags=str_replace($antes,$despues,$vartags);
  */
/*
  $vartags=$vartags;
  $origin=$ori;
}



}else{
$vartags="'Asignado','Exportado','Descargado'";
$origin="Asignado,Exportado,Descargado";
}



if(isset($_GET['periodoliquidacion'])) {
$mes=$_GET['periodoliquidacion'];

    if ($mes !='0' && $tangente=='Finalizado'){
      $agree1='and month(FechaAtencion) = '.$mes;
    } else
    {
      $agree1='';
    }

}
else {
$agree1='';
}





if(isset($_GET['unidadneg'])) {
$unidad=$_GET['unidadneg'];


  if ($unidad == 0){
    $unidad="26,30,29";
    $origin2a="0";
    $origin2b="Todos";
  }else{
    $origin2a=$unidad;
    $sqlconsulta="select * from [GlobalMaster.Ubicacion] where IdUbicacionConc = $origin2a";
    $result3 = pg_query($sqlconsulta) or die ($sqlconsulta);
    $retorno = pg_fetch_object($result3);
    $origin2b=$retorno->NomUbicacion;
  }
}else{
$unidad="26,30,29";
$origin2a="0";
$origin2b="Todos";
}


if(isset($_GET['centroservicio'])) {
$centroser=$_GET['centroservicio'];
  if ($centroser == 0){
    $centroser="258,272,255,273,270,271,251,268";
    $origin3a="0";
    $origin3b="Todos";
  }else{
    $centroser=$centroser;
    $origin3a=$centroser;
    $sqlconsultaa="select IdCentroServicio,CentroServicio from [ComMovTab.Orden] where IdCentroServicio = $origin3a group by IdCentroServicio,CentroServicio";
    $result3a = pg_query($sqlconsultaa) or die ($sqlconsultaa);
    $retornoa = pg_fetch_object($result3a);
    $origin3b=$retornoa->CentroServicio;
  }
}else{
$centroser="258,272,255,273,270,271,251,268";
$origin3a="0";
$origin3b="Todos";
}





if(isset($_GET['vari'])) 
{
     $vari=$_GET["vari"];

}
else 
{
     $vari='';
}
*/



$query = pg_query("select IdTipoIntervencion,DetalleTipoIntervencion from ComMovTab_TipoIntervencion where IdTipoIntervencionSup = 0 and IdTipoIntervencion !=4 order by IdTipoIntervencion asc");

$query5= pg_query("select count(*) as contador2  from ComMovTab_Intervencion CI
		join ComMovTab_Orden CO on CI.IdOrden = CO.IdOrden
		left outer join ComMovTab_TipoIntervencion TIS on CO.SubActividad=TIS.DetalleTipoIntervencion
		join ComMovTab_Documento CD on CO.IdDocumento = CD.IdDocumento
		left outer join ComMovTab_TipoIntervencion TI on CD.IdTipoIntervencion = TI.IdTipoIntervencion
		join ComMovTab_MaestrosEstado ME on CI.IdEstado = ME.IdEstado
		join ComMovTab_Usuario CU on CI.IdUsuario = CU.IdUsuario
		join GlobalMaster_Persona GMP on CU.IdPersona = GMP.IdPersona
		where
		co.SubActividad not in (select DetalleTipoIntervencion from ComMovTab_TipoIntervencion)
		and co.UsuarioGenero not like 'Pexport%'
		and co.Condicion = '0'
		and co.FechaGeneracion > '2014-10-01 16:23:19' ".$str_where);
$retorno5= pg_fetch_object($query5);
$nr3 = pg_num_rows($query5);
if ($nr3>0){

                                  $contador2=$retorno5->contador2;
                                    } else{
                                      $contador2=0;
                                    }


              $lista2= " 

              <ul class='nav nav-list treeview' >

              <li class='open'><label class='tree-toggler nav-header'><i class='fa fa-folder-open-o'></i>General</label>
                                <ul class='nav nav-list tree'>
                                    <li ><a href='#' onclick='showMonAv()'>Monitor de Avance</a></li>
                                    <li ><a href='#' onclick='showMon(100,open)'>Ordenes No Validadas <span class='badge badge-danger'>$contador2</span></a> </li>
                                </ul>
              </li>


              ";
              while ($retorno = pg_fetch_object($query)){
                   $id = $retorno->idtipointervencion;
                   $var= $retorno->detalletipointervencion;

                                  $query4= pg_query("select count(*) as contador  from ComMovTab_Intervencion ci
                                  inner join ComMovTab_MaestrosEstado cme on ci.IdEstado = cme.IdEstado   
                                  inner join ComMovTab_Orden co on ci.IdOrden=co.IdOrden
                                  inner join ComMovTab_TipoIntervencion cti on co.SubActividad = cti.DetalleTipoIntervencion 
                                  where co.UsuarioGenero not like 'Pexport%' and IdTipoIntervencionSup= $id and co.Condicion='0' ".$str_where2); 
                                  $retorno4= pg_fetch_object($query4);

                                  $nr2 = pg_num_rows($query4);
                                  if ($nr2>0){

                                  $contador=$retorno4->contador;
                                    } else{
                                      $contador=0;
                                    }

                                  


                                $lista2.= "<li class='open'><label class='tree-toggler nav-header'><i class='fa fa-folder-open-o'></i>$var <span class='badge badge-success'>$contador</span></label>
                                <ul class='nav nav-list tree'>

                                <li ><a href='#' onclick='showMon(1,open)'>Ver Todo Reclamos <span class='badge badge-success'>$contador</span></a> </li>
                                ";

                                $query2 = pg_query("select IdTipoIntervencion,DetalleTipoIntervencion from ComMovTab_TipoIntervencion where IdTipoIntervencionSup = $id order by IdTipoIntervencion asc");  

                                while ($retorno2 = pg_fetch_object($query2)){

                                  
                                  $id2= $retorno2->idtipointervencion;
                                  $var2= utf8_decode($retorno2->detalletipointervencion);
                                  
                                  $query3= pg_query($conexion, "select count(*) as contador  from ComMovTab_Intervencion ci
                                  inner join ComMovTab_MaestrosEstado cme on ci.IdEstado = cme.IdEstado  
                                  inner join ComMovTab_Orden co on ci.IdOrden=co.IdOrden
                                  inner join ComMovTab_TipoIntervencion cti on co.SubActividad = cti.DetalleTipoIntervencion 
                                  where co.UsuarioGenero not like 'Pexport%' and IdTipoIntervencion= $id2 and co.Condicion='0' ".$str_where2); 
                                  $retorno3= pg_fetch_object($query3);

                                  $nr = pg_num_rows($query3);
                                  if ($nr>0){

                                  $contador=$retorno3->contador;
                                  
                                    } else{
                                      $contador=0;
                                    }


                                 $query4= pg_query("select count(*) as contadornuevo from ComMovTab_Intervencion ci
                                  inner join ComMovTab_Orden co on ci.IdOrden=co.IdOrden
                                  inner join ComMovTab_TipoIntervencion cti on co.SubActividad = cti.DetalleTipoIntervencion 
                                  where co.UsuarioGenero not like 'Pexport%' and FechaAsignado > '".date('Y-m-d H:i:s', strtotime('-3 hour'))."' and cti.IdTipoIntervencion=$id2 ".$str_fecha_liquidacion);

                                 $retorno4= pg_fetch_object($query4);

                                  $nr4 = pg_num_rows($query4);
                                  

                                  $contador4=$retorno4->contadornuevo;


                                  if ($contador4>0){
                                    $animated="<span class='badge badge-danger'>+$contador4</span>";
                                    } else{
                                      
                                      $animated='';
                                    }


                                $lista2.="<li ><a href='#' onclick='showMon($id2,open)'>".utf8_encode($var2)." $animated <span class='badge badge-primary'>$contador</span></a></li>";
                                }
                            $lista2.="</ul>
                        </li>";
                   }

                $lista2.="</ul>
                ";

echo $lista2;



?>
