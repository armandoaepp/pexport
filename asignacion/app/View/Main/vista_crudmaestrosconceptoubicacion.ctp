<?php
date_default_timezone_set('America/Lima');
//require "../conexion.php";
require_once(APP."View/Main/conexion.ctp");

$query = pg_query("select 
GMCU.IdConceptosUbicacion as IdConceptosUbicacion,
GMCU.NomConceptosUbicacion as NomConceptosUbicacion,
GMCU.AbrConceptosUbicacion as AbrConceptosUbicacion,
GMCU.IdEstado as IdEstado,
ME.NomEstado as NomEstado
 from GlobalMaster_ConceptosUbicacion GMCU
inner join ComMovTab_MaestrosEstado ME on GMCU.IdEstado = ME.IdEstado ");


$lista="
    <div class='row'>
      <div class='col-sm-12 col-md-12'>
        <div class='block-flat'>


        <div class='panel-group accordion accordion-semi' id='accordion4'>
					  <div class='panel panel-default'>
						<div class='panel-heading success'>
						  <h4 class='panel-title'>
							<a data-toggle='collapse' data-parent='#accordion4' href='#ac4-1'>
								<i class='fa fa-angle-right'></i> Agregar Nuevo Concepto
							</a>
						  </h4>
						</div>
						<div id='ac4-1' class='panel-collapse collapse in'>
						  <div class='panel-body'>
							
						          <div class='content'>

							          <form role='form' id='formconceptoubicacion' method='post' style='width:60%;'> 
							            <div class='form-group'>
							              <input type='hidden' id='IdConceptosUbicacion' name='IdConceptosUbicacion'>
							              <label>Nombre Concepto Ubicacion</label> <input type='text' id='nameconceptoubi' name='nameconceptoubi' placeholder='Nombre de Concepto' class='form-control'>
							            </div>
							            <div class='form-group'> 
							              <label>Abreviacion Concepto Ubicacion</label> <input type='abreconceptoubi' id='abreconceptoubi' name='abreconceptoubi'  placeholder='Abreviacion de Concepto' class='form-control'>
							            </div> 
							              <button class='btn btn-danger' type='button' onclick='crudmaestrosconceptoubicacion(1)'>Grabar</button>
							              <button class='btn btn-default' type='button'>Cancelar</button>
							          </form>
						          
						          </div>

						  </div>
						</div>
					  </div>
		</div>


		

						      <div class='row'>
						        <div class='col-md-12'>
						          <div class='block-flat'>

						            

						            <div class='content'>
						            <div class='tabla'>

						          
						            <div class=''>

						              <div class='table-responsive'>
						              <form role='form' id='formconceptoubicacioncrud' method='post'> 
						                <table class='no-border' id='datatablecrudconceptosubicacion'>

						                  <thead class='primary-emphasis'>
						                    <tr>
						                      <th></th>
						                      <th>Codigo</th>
						                      <th>Nombre Concepto</th>
						                      <th>Abreviacion Concepto</th>
						                      <th>Estado</th>
						                    </tr>

						                  </thead>
						                  <tbody class='no-border'>";


						                    while ($retorno = pg_fetch_object($query)){

						                       $var=utf8_decode($retorno->idconceptosubicacion);
						                       $var1=utf8_decode($retorno->nomconceptosubicacion);
						                       $var2=utf8_decode($retorno->abrconceptosubicacion);
						                       $var4=utf8_decode($retorno->idestado);
						                       $var41=utf8_decode($retorno->nomestado);

						                       $lista.= "<tr class='odd gradeX'>
						                                  <input type ='hidden' id='idconcrud[]' name='idconcrud[]' value='$var'>
						                                  <td style='width:1%;'><input type ='checkbox' id='idchkconcrud[]' name='idchkconcrud[]' value='$var' ></td>
						                                  <td style='width:1%;'>$var</td>
						                                  <td style='width:20%;'><input type='text' id='nameconceptoubicrud[]' name='nameconceptoubicrud[]'  value='$var1' class='form-control'></td>
						                                  <td style='width:30%;'><input type='text' id='abreconceptoubicrud[]' name='abreconceptoubicrud[]'  value='$var2' class='form-control'></td>
						                                  <td style='width:10%;'><select class='form-control' id='estadocrud[]' name='estadocrud[]' value='$var41' >    
						                                      <option value='$var4'>$var41</option>
						                                      ";
						                                  $querydet = pg_query("select ME.IdEstado as IdEstado,NomEstado as NomEstado from ComMovTab_MaestrosEstado ME  
						                                  where ME.EstadoSup = 2 and ME.IdEstado!=$var4");
						                                   while ($retornodet = pg_fetch_object($querydet)){
						                                    $idest=$retornodet->idestado;
						                                    $nomest=$retornodet->nomestado;

						                                    $lista.="<option value='$idest'>$nomest</option>"; 
						                                   } 
						                                  $lista.="</select></td>
						                                </tr>";
						                       }
						                  $lista.="</tbody>

						                </table>
						                <button class='btn btn-success' type='button' onclick='crudmaestrosconceptoubicacion(2)'>Grabar</button>
						                <button class='btn btn-danger' type='button' onclick='crudmaestrosconceptoubicacion(3)'>Eliminar</button>
						              </form>              
						              </div>

						              </div>
						              </div>
						              
						            </div>
						          </div>        
						        </div>

						      </div>

      

        </div>				
      </div>
      

	</div>";

echo $lista;

?>