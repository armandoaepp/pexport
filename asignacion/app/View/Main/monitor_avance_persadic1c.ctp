<?php
date_default_timezone_set('America/Lima');

//require_once "../conexion.php";
require_once(APP."View/Main/conexion.ctp");

$fechaactual=date("Y-m-d");


if(isset($_GET["idini1"])) 
{
  $idini1=$_GET["idini1"];
} else {
  $idini1=$fechaactual;
}


if(isset($_GET["idfin1"])) 
{
  $idfin1=$_GET["idfin1"];
} else {
  $idfin1=$fechaactual;
}




$querymondia2c = pg_query("select TipoIntervencion,SubActividad,
sum(TotalGeneradas) as TotalGeneradas,
sum(TotalFinalizadas) as TotalFinalizadas,
((sum(TotalGeneradas))-(sum(TotalFinalizadas))) as TotalPendientes,
case when sum(TotalGeneradas) > 0 then ((sum(TotalFinalizadas)*100)/sum(TotalGeneradas)) else 0 end as PorcentajeTotal, 
sum(OrdenesGeneradas) as OrdenesGeneradasDia,
sum(OrdenesFinalizadasOrdenesGeneradasDia) as OrdenesFinalizadasDia,
((sum(OrdenesGeneradas))-(sum(OrdenesFinalizadasOrdenesGeneradasDia))) as OrdenesPendientesDia,
case when sum(OrdenesGeneradas) > 0 then ((sum(OrdenesFinalizadasOrdenesGeneradasDia)*100)/sum(OrdenesGeneradas)) else 0 end as PorcentajeDia,  
sum(OrdenesFinalizadas) as OrdenesFinalizadasAcarreo
from (
select ctis.DetalleTipoIntervencion as TipoIntervencion,
cti.DetalleTipoIntervencion as SubActividad,
(case when (cast(ci.FechaAtencion as date) BETWEEN '2014-06-23' AND '$idfin1') and ci.IdEstado=15 and co.Condicion='0'
then COUNT(*) else 0 end) as TotalFinalizadas,
(case when (cast(ci.FechaAtencion as date) BETWEEN '$idini1' AND '$idfin1') and ci.IdEstado=15 and co.Condicion='0'
then COUNT(*) else 0 end) as OrdenesFinalizadas,
(case when (cast(ci.FechaAtencion as date) BETWEEN '$idini1' AND '$idfin1') and (ci.IdEstado=15) and co.Condicion='0' and ci.IdIntervencion in (select ci.IdIntervencion from ComMovTab_Orden co 
inner join ComMovTab_Intervencion ci 
on co.IdOrden = ci.IdOrden where cast(ci.FechaAsignado as date) = '$idfin1' and ci.IdEstado = 15) and co.Condicion='0'
then COUNT(*) else 0 end) as OrdenesFinalizadasOrdenesGeneradasDia,
(case when cast(ci.FechaAsignado as date) BETWEEN '2014-06-23' AND '$idfin1' and co.Condicion='0'
then COUNT(*) else 0 end) as TotalGeneradas,
(case when cast(ci.FechaAsignado as date) BETWEEN '$idini1' AND '$idfin1' and co.Condicion='0'
then COUNT(*) else 0 end) as OrdenesGeneradas
from ComMovTab_TipoIntervencion cti 
inner join ComMovTab_TipoIntervencion ctis on ctis.IdTipoIntervencion = cti.IdTipoIntervencionSup 
left outer join ComMovTab_Orden co on cti.DetalleTipoIntervencion = co.SubActividad
left outer join ComMovTab_Intervencion ci on co.IdOrden = ci.IdOrden
group by ctis.DetalleTipoIntervencion,cti.DetalleTipoIntervencion,co.Condicion,ci.FechaAtencion,ci.FechaAsignado,co.FechaGeneracion,ci.IdEstado,ci.IdIntervencion) as consulta2
group by TipoIntervencion,SubActividad");


											$pers1c="<div class='pers1'><div class='table-responsive'>
                                                    <table class='table table-bordered tableWithFloatingHeader'  id='datatablepers1c' >
                                                      <thead class='primary-emphasis'>
                                                      <tr>
                                                          <th class='text-center primary-emphasis-dark'>Actividad</th>
                                                          <th class='text-center primary-emphasis-dark'>Sub Actividad</th>
                                                          <th class='text-center primary-emphasis-dark'>Total Generadas</th>
                                                          <th class='text-center primary-emphasis-dark'>Total Finalizadas</th>
                                                          <th class='text-center danger-emphasis-dark'>Total Pendientes</th>
                                                          <th  class='text-center primary-emphasis-dark'style='width:25%;'>% Total</th>
                                                      </tr>
                                                      </thead>
                                                      <tbody>
                                                      ";
                                                      while ($retornomondia2 = pg_fetch_object($querymondia2c)){
                                                                            $varmondia2TipoIntervencion=$retornomondia2->tipointervencion;
                                                                            $varmondia2SubActividad=$retornomondia2->subactividad;
                                                                            $varmondia2TotalGeneradas=$retornomondia2->totalgeneradas;
                                                                            $varmondia2TotalFinalizadas=$retornomondia2->totalfinalizadas;
                                                                            $varmondia2TotalPendientes=$retornomondia2->totalpendientes;
                                                                            $varmondia2PorcentajeTotal=$retornomondia2->porcentajetotal;
                                                                            $varmondia2OrdenesGeneradasDia=$retornomondia2->ordenesgeneradasdia;
                                                                            $varmondia2OrdenesFinalizadasDia=$retornomondia2->ordenesfinalizadasdia;
                                                                            $varmondia2OrdenesPendientesDia=$retornomondia2->ordenespendientesdia;
                                                                            $varmondia2PorcentajeDia=$retornomondia2->porcentajedia;
                                                                            $varmondia2OrdenesFinalizadasAcarreo=$retornomondia2->ordenesfinalizadasacarreo;


                                                                            $totalactgen[]=$varmondia2TotalGeneradas;
                                                                            $totalactfin[]=$varmondia2TotalFinalizadas;
                                                                            $totalactpen[]=$varmondia2TotalPendientes;
                                                             
                                                                            


                                                                            if ($varmondia2PorcentajeDia < 33) {
                                                                              $color3='danger';
                                                                              
                                                                            } elseif ($varmondia2PorcentajeDia > 33 && $varmondia2PorcentajeDia < 70) {
                                                                              $color3='warning';
                                                                            } else {
                                                                              $color3='success';
                                                                            }

                                                                            if ($varmondia2PorcentajeTotal < 33) {
                                                                              $color33='danger';
                                                                              
                                                                            } elseif ($varmondia2PorcentajeTotal > 33 && $varmondia2PorcentajeTotal < 70) {
                                                                              $color33='warning';
                                                                            } else {
                                                                              $color33='success';
                                                                            }

                                                                        

                                                      $pers1c.= "
                                                        <tr class='odd gradeX'>
                                                          <td><strong>$varmondia2TipoIntervencion</strong></td>
                                                          <td><strong>$varmondia2SubActividad</strong></td>
                                                          <td><strong>$varmondia2TotalGeneradas</strong></td>
                                                          <td><strong>$varmondia2TotalFinalizadas</strong></td>
                                                          <td><strong>$varmondia2TotalPendientes</strong></td>
                                                          <td class='center'><div class='progress progress-striped active'>
                                                                                  <div class='progress-bar progress-bar-$color33' style='width:$varmondia2PorcentajeTotal%'>".number_format($varmondia2PorcentajeTotal,2)."%</div>
                                                          </div></td>
                                                         </tr>
                                                          ";

                                                          }

                                                        $pers1c.="
                                                      </tbody>
                                                      <tfoot >
                                                      <tr>
                                                        <td style='text-align:right;' colspan='2'><strong>Total</strong></td>
                                                        ";

                                                                                     


                                                                                            $totalactgenf=array_sum($totalactgen);
                                                                                            $totalactfinf=array_sum($totalactfin);
                                                                                            $totalactpenf=array_sum($totalactpen);
                                                       
                                                                                            if ($totalactgenf==0){
                                                                                              $varfinalmondia2PorcentajeTotal=0;
                                                                                            }
                                                                                            else {
                                                                                              $varfinalmondia2PorcentajeTotal=($totalactfinf*100)/($totalactgenf);
                                                                                              $varfinalmondia2PorcentajeTotal=round($varfinalmondia2PorcentajeTotal,2);
                                                                                            }


                                                                                


                                                                                            if ($varfinalmondia2PorcentajeTotal< 33) {
                                                                                              $color44='danger';
                                                                                              
                                                                                            } elseif ($varfinalmondia2PorcentajeTotal > 33 && $varfinalmondia2PorcentajeTotal< 70) {
                                                                                              $color44='warning';
                                                                                            } else {
                                                                                              $color44='success';
                                                                                            }

                                                                                            $pers1c.="<td><strong>$totalactgenf<strong></td>
                                                                                                     
                                                                                                     <td><a href='#' data-toggle='modal' data-target='#mod-success2' onclick='showModalMonAv()'><strong>$totalactfinf<strong></a></td>
                                                                                                     <td><strong>$totalactpenf<strong></td>
                                                                                                     <td>
                                                                                                        <div class='progress progress-striped active'>
                                                                                                        <div class='progress-bar progress-bar-$color44' style='width: $varfinalmondia2PorcentajeTotal%'>".number_format($varfinalmondia2PorcentajeTotal,2)."%</div>
                                                                                                        </div>
                                                                                                     </td>
                                                      </tr>
                                                      </tfoot>


                                                      </table>
                                              </div></div>";


                                              echo $pers1c;

?>
  <script type="text/javascript">

        function UpdateTableHeaders() {
            $("div.divTableWithFloatingHeader").each(function() {
                var originalHeaderRow = $(".tableFloatingHeaderOriginal", this);
                var floatingHeaderRow = $(".tableFloatingHeader", this);
                var offset = $(this).offset();
                var scrollTop = $(window).scrollTop();

                var navegador = navigator.userAgent;
                var inicioScroll = scrollTop-30;
                var marginheader = 30;
                if (navigator.userAgent.indexOf('MSIE') !=-1) {
                 
                } else if (navigator.userAgent.indexOf('Firefox') !=-1) {
                  inicioScroll = scrollTop-50;
                  marginheader = 40;
                } else if (navigator.userAgent.indexOf('Chrome') !=-1) {
                  inicioScroll = scrollTop-30;
                  var marginheader = 50;
                } else if (navigator.userAgent.indexOf('Opera') !=-1) {
                  inicioScroll = scrollTop-30;
                  marginheader = 40;
                } else {
                
                }

                if (( inicioScroll > offset.top ) && (scrollTop < offset.top + $(this).height())) {
                    floatingHeaderRow.css("visibility", "visible");
                    floatingHeaderRow.css("top", (Math.min(scrollTop - offset.top, $(this).height() - floatingHeaderRow.height()))+ marginheader  + "px");

                    // Copy cell widths from original header
                    $("th", floatingHeaderRow).each(function(index) {
                        var cellWidth = $("th", originalHeaderRow).eq(index).css('width');
                        var color = $("th", originalHeaderRow).eq(index).css('background-color');
                        
                        $(this).css('width', cellWidth);
                        $(this).css('background-color', color);
                    });

                    // Copy row width from whole table
                    floatingHeaderRow.css("width", $(this).css("width"));
                    floatingHeaderRow.css("background-color", $(this).css("background-color"));
                }
                else {
                    floatingHeaderRow.css("visibility", "hidden");
                    floatingHeaderRow.css("top", "0px");
                    
                }
            });
        }

        $(document).ready(function() {
            $("table.tableWithFloatingHeader").each(function() {
                $(this).wrap("<div class=\"divTableWithFloatingHeader\" style=\"position:relative\"></div>");

                var originalHeaderRow = $("tr:first", this)
                originalHeaderRow.before(originalHeaderRow.clone());
                var clonedHeaderRow = $("tr:first", this)

                clonedHeaderRow.addClass("tableFloatingHeader");
                clonedHeaderRow.css("position", "absolute");
                clonedHeaderRow.css("top", "0px");
                clonedHeaderRow.css("left", $(this).css("margin-left"));
                clonedHeaderRow.css("visibility", "hidden");
                clonedHeaderRow.css("z-index", "5");
                
                originalHeaderRow.addClass("tableFloatingHeaderOriginal");
            });

            UpdateTableHeaders();
            $(window).scroll(UpdateTableHeaders);
            $(window).resize(UpdateTableHeaders);
        });
</script>