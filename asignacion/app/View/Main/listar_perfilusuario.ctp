<?php
date_default_timezone_set('America/Lima');
/*timer = setTimeout('auto_reload()',5000);*/
//session_start();

$user=$_SESSION['nompersona'];
$foto=$_SESSION['fotopersona'];
if(!isset($foto)){
	$foto = '../images/persona/001.png';
}

//require "../conexion.php";
require_once(APP."View/Main/conexion.ctp");

$mes_actual		=	(int) date('m');
$mes_anterior	=	(int) date('m',strtotime('-1 month', strtotime(date("Y-m-d"))));

$dia_actual		=	(int) date('d');
$fecha_actual	=	date('Y-m-d');

function getTiempoString($tiempo_segundos){
	$str_dias = 0;
	$str_horas = 0;
	$str_minutos = 0;
	if($tiempo_segundos>36400){
		$str_dias = floor ($tiempo_segundos/36400);
		$tiempo_segundos = $tiempo_segundos%36400;
	}
	if($tiempo_segundos>3600){
		$str_horas = floor ($tiempo_segundos/3600);
		$tiempo_segundos = $tiempo_segundos%3600;
	}
	if($tiempo_segundos>60){
		$str_minutos = floor ($tiempo_segundos/60);
		$tiempo_segundos = floor ($tiempo_segundos%60);
	}

	$str_horas      = str_pad( $str_horas, 2, "0", STR_PAD_LEFT );
	$str_minutos    = str_pad( $str_minutos, 2, "0", STR_PAD_LEFT );
	$tiempo_segundos= str_pad( $tiempo_segundos, 2, "0", STR_PAD_LEFT );

	return ($str_dias>0?$str_dias.' días ':' ').$str_horas.':'.$str_minutos.':'.$tiempo_segundos;
}

$query_resumen_2_ultimos_meses = pg_query("select consulta1.anio as Anio,consulta1.mes as Mes,
consulta2.Total as Generadas,
consulta1.TotalFin as Finalizadas,
((consulta2.Total)-consulta1.TotalFin) as Pendientes,
((consulta1.TotalFin*100)/consulta2.Total) as Porcentaje,
consulta3.TotalFin as Infructuosa,
consulta4.tiempo_promedio as TiempoPromedio,
consulta5.Total as GeneradasEnsa from
(select extract('year' from FechaAtencion) as anio, extract('month' from FechaAtencion) as mes,count(ci.IdEstado) as TotalFin  from ComMovTab_Intervencion ci
inner join ComMovTab_Orden co on ci.IdOrden=co.IdOrden
where co.SubActividad in (select DetalleTipoIntervencion from ComMovTab_TipoIntervencion)
and ci.IdEstado=15 and co.Condicion='0' and extract('day' from FechaAtencion)<=".$dia_actual."
group by extract('year' from FechaAtencion), extract('month' from FechaAtencion)) as consulta1
left outer join
(
select extract('year' from FechaAsignado) as anio, extract('month' from FechaAsignado) as mes,count(ci.IdEstado) as Total  from ComMovTab_Intervencion ci
inner join ComMovTab_Orden co on ci.IdOrden=co.IdOrden
where co.SubActividad in (select DetalleTipoIntervencion from ComMovTab_TipoIntervencion) and co.Condicion='0'
and extract('day' from FechaAsignado)<=".$dia_actual."
group by extract('year' from FechaAsignado), extract('month' from FechaAsignado)) as consulta2 on consulta1.anio = consulta2.anio and consulta1.mes = consulta2.mes
left outer join
(
select extract('year' from FechaAtencion) as anio, extract('month' from FechaAtencion) as mes,count(ci.IdEstado) as TotalFin  from ComMovTab_Intervencion ci
inner join ComMovTab_Orden co on ci.IdOrden=co.IdOrden
inner join ComMovTab_Detalle_Intervencion DI on DI.IdIntervencion=ci.IdIntervencion
where co.SubActividad in (select DetalleTipoIntervencion from ComMovTab_TipoIntervencion)
and co.Condicion='0' and extract('day' from FechaAtencion)<=".$dia_actual." and DI.IdMaestrosIntervencion in (
select IdMaestrosIntervencion from ComMovTab_MaestrosIntervencion
where IdentificadorMaestroIntervencion like 'LabelInfructuosa%')
group by extract('year' from FechaAtencion), extract('month' from FechaAtencion)) as consulta3 on consulta1.anio = consulta3.anio and consulta1.mes = consulta3.mes
left outer join
(
select extract('year' from FechaAtencion) as anio, extract('month' from FechaAtencion) as mes,avg(tiempo) as tiempo_promedio from (
select FechaAsignado, FechaAtencion, ((DATE_PART('day', FechaAtencion - FechaAsignado) * 24 + 
                DATE_PART('hour', FechaAtencion - FechaAsignado)) * 60 +
                DATE_PART('minute', FechaAtencion - FechaAsignado)) * 60 +
                DATE_PART('second', FechaAtencion - FechaAsignado) as tiempo
from ComMovTab_Intervencion where FechaAtencion is not null and extract('day' from FechaAtencion)<=".$dia_actual.") as T
group by extract('year' from FechaAtencion), extract('month' from FechaAtencion)) as consulta4 on consulta1.anio = consulta4.anio and consulta1.mes = consulta4.mes
left outer join
(
select extract('year' from cast(FechaGeneracion as timestamp)) as anio, extract('month' from cast(FechaGeneracion as timestamp)) as mes,count(ci.IdEstado) as Total  from ComMovTab_Intervencion ci
inner join ComMovTab_Orden co on ci.IdOrden=co.IdOrden
where co.SubActividad in (select DetalleTipoIntervencion from ComMovTab_TipoIntervencion) and co.Condicion='0'
and extract('day' from cast(FechaGeneracion as timestamp))<=".$dia_actual."
group by extract('year' from cast(FechaGeneracion as timestamp)), extract('month' from cast(FechaGeneracion as timestamp))) as consulta5 on consulta1.anio = consulta5.anio and consulta1.mes = consulta5.mes
 order by 1 desc,2 desc limit 1");

$count_rows=pg_num_rows($query_resumen_2_ultimos_meses);
$arr_datos = array();
while ($obj_resumen_2_ultimos_meses = pg_fetch_object($query_resumen_2_ultimos_meses)){
	$arr_datos[$obj_resumen_2_ultimos_meses->mes]['mes'] = $obj_resumen_2_ultimos_meses->mes;
	$arr_datos[$obj_resumen_2_ultimos_meses->mes]['generadasensa'] = $obj_resumen_2_ultimos_meses->generadasensa;
	$arr_datos[$obj_resumen_2_ultimos_meses->mes]['generadas'] = $obj_resumen_2_ultimos_meses->generadas;
	$arr_datos[$obj_resumen_2_ultimos_meses->mes]['finalizadas'] = $obj_resumen_2_ultimos_meses->finalizadas;
	$arr_datos[$obj_resumen_2_ultimos_meses->mes]['pendientes'] = $obj_resumen_2_ultimos_meses->pendientes;
	$arr_datos[$obj_resumen_2_ultimos_meses->mes]['infructuosa'] = $obj_resumen_2_ultimos_meses->infructuosa;
	$arr_datos[$obj_resumen_2_ultimos_meses->mes]['tiempoProm'] = $obj_resumen_2_ultimos_meses->tiempopromedio;

	$arr_datos[$obj_resumen_2_ultimos_meses->mes]['tiempopromedio'] = getTiempoString($obj_resumen_2_ultimos_meses->tiempopromedio);
}


$lista="

	<div class='cl-mcont'>
    <div class='row'>
      <div class='col-sm-12'>
        <div class='block-flat profile-info'>
          <div class='row'>
            <div class='col-sm-2'>
              <div class='avatar' style='width:100px;'>
                <img src='".FULL_URL.$foto."' height='120' width='80'  />
              </div>
            </div>
            <div class='col-sm-5'>
              <div class='personal'>
                <h1 class='name'>".$user."</h1>
                <p class='description'>Gestor Comercial de Electronorte S.A<p>
              </div>
            </div>
            <div class='col-sm-4'>
              <table class='no-border no-strip skills'>
                <tbody class='no-border-x no-border-y'>
				  <tr>
                    <td style='width:60%;'><i class='fa fa-sitemap'></i> Ordenes Generadas Ensa</td>
                    <td class='text-right'>".@$arr_datos[$mes_actual]['GeneradasEnsa']."</td>
                  </tr>
                  <tr>
                    <td><i class='fa fa-tasks'></i> Ordenes Finalizadas</td>
                    <td class='text-right'>".@$arr_datos[$mes_actual]['Finalizadas']."</td>
                  </tr>
                  <tr>
                    <td><i class='fa fa-signal'></i> Visitas Infructuosas</td>
                    <td class='text-right'>".@$arr_datos[$mes_actual]['Infructuosa']."</td>
                  </tr>
                  <tr>
                    <td><i class='fa fa-bolt'></i> Tiempo de Ejecuci&oacute;n</td>
                    <td class='text-right'>".@$arr_datos[$mes_actual]['TiempoPromedio']."</td>
                  </tr>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>

    ";

    echo $lista;
    
    $fechaactual=date("Y-m-d");
    $mes=date("m");

    
    if(isset($_GET["idini1"]))
    {
    	$idini1=$_GET["idini1"];
    } else {
    	$idini1=$fechaactual;
    }
    
    
    if(isset($_GET["idfin1"]))
    {
    	$idfin1=$_GET["idfin1"];
    } else {
    	$idfin1=$fechaactual;
    }
    
    
    
    
    $querymondia1 = pg_query("select TipoIntervencion,SubActividad,
    		sum(TotalGeneradas) as TotalGeneradas,
    		sum(TotalFinalizadas) as TotalFinalizadas,
    		((sum(TotalGeneradas))-(sum(TotalFinalizadas))) as TotalPendientes,
    		case when sum(TotalGeneradas) > 0 then ((sum(TotalFinalizadas)*100)/sum(TotalGeneradas)) else 0 end as PorcentajeTotal,
    		sum(OrdenesGeneradas) as OrdenesGeneradasDia,
    		sum(OrdenesFinalizadasOrdenesGeneradasDia) as OrdenesFinalizadasDia,
    		((sum(OrdenesGeneradas))-(sum(OrdenesFinalizadasOrdenesGeneradasDia))) as OrdenesPendientesDia,
    		case when sum(OrdenesGeneradas) > 0 then ((sum(OrdenesFinalizadasOrdenesGeneradasDia)*100)/sum(OrdenesGeneradas)) else 0 end as PorcentajeDia,
    		sum(OrdenesFinalizadas) as OrdenesFinalizadasAcarreo
    		from (
    		select ctis.DetalleTipoIntervencion as TipoIntervencion,
    		cti.DetalleTipoIntervencion as SubActividad,
    		(case when (cast(ci.FechaAtencion as date) BETWEEN '2014-06-23' AND '$idfin1') and ci.IdEstado=15 and co.Condicion='0'
    		then COUNT(*) else 0 end) as TotalFinalizadas,
    		(case when (cast(ci.FechaAtencion as date) BETWEEN '$idini1' AND '$idfin1') and ci.IdEstado=15 and co.Condicion='0'
    		then COUNT(*) else 0 end) as OrdenesFinalizadas,
    		(case when (cast(ci.FechaAtencion as date) BETWEEN '$idini1' AND '$idfin1') and (ci.IdEstado=15) and co.Condicion='0' and ci.IdIntervencion in (select ci.IdIntervencion from ComMovTab_Orden co
    		inner join ComMovTab_Intervencion ci
    		on co.IdOrden = ci.IdOrden where cast(ci.FechaAsignado as date) = '$idfin1' and ci.IdEstado = 15) and co.Condicion='0'
    		then COUNT(*) else 0 end) as OrdenesFinalizadasOrdenesGeneradasDia,
    		(case when cast(ci.FechaAsignado as date) BETWEEN '2014-06-23' AND '$idfin1' and co.Condicion='0'
    		then COUNT(*) else 0 end) as TotalGeneradas,
    		(case when cast(ci.FechaAsignado as date) BETWEEN '$idini1' AND '$idfin1' and co.Condicion='0'
    		then COUNT(*) else 0 end) as OrdenesGeneradas
    		from ComMovTab_TipoIntervencion cti
    		inner join ComMovTab_TipoIntervencion ctis on ctis.IdTipoIntervencion = cti.IdTipoIntervencionSup
    		left outer join ComMovTab_Orden co on cti.DetalleTipoIntervencion = co.SubActividad
    		left outer join ComMovTab_Intervencion ci on co.IdOrden = ci.IdOrden
    		group by ctis.DetalleTipoIntervencion,cti.DetalleTipoIntervencion,co.Condicion,ci.FechaAtencion,co.FechaGeneracion,ci.FechaAsignado,ci.IdEstado,ci.IdIntervencion) as consulta2
    		group by TipoIntervencion,SubActividad");
    
    
    		$rs=pg_num_rows($querymondia1);
    
    
    		if ($rs > 0) {
    
    		$lista="<div class='row'>
    		<div class='col-md-12'>
    		<div class='block-flat'>
    
    
    
    
    
    		<div class='header'>
    		<h3>Monitor Especializado </h3>
    
    		<button data-toggle='modal' data-target='#mod-success3' class='btn btn-info' type='button' onclick='enviarEmailMonitorear()' style='float:right;margin-top:-42px;'><i class='fa fa-envelope'></i> Enviar por Email</button>
    		</div>
    
    		<div class='form-group'>
    		<label class='col-sm-1 control-label'>Desde</label>
    		<div class='col-sm-3'>
    		<div class='input-group date datetime col-md-12 col-xs-7' data-min-view='2' data-date-format='yyyy-mm-dd'>
    		<input class='form-control' size='16' type='text' value='$idini1' id='fechaini1' name='fechaini1' readonly>
    		<span class='input-group-addon btn btn-primary'><span class='glyphicon glyphicon-th'></span></span>
    		</div>
    		</div>
    		<label class='col-sm-1 control-label'>Hasta</label>
    		<div class='col-sm-3'>
    		<div class='input-group date datetime col-md-12 col-xs-7' data-min-view='2' data-date-format='yyyy-mm-dd'>
    		<input class='form-control' size='16' type='text' value='$idfin1' id='fechafin1' name='fechafin1' readonly>
    		<span class='input-group-addon btn btn-primary'><span class='glyphicon glyphicon-th'></span></span>
    		</div>
    		</div>
    
    		<button class='btn btn-primary' onclick='showMonAv()' >Consultar</button>
    		</div>
    
    
    
    		<div class='panel-group accordion accordion-semi' id='accordion4' style='margin-bottom:0px;'>
    		<div class='panel panel-default' style='margin-bottom:0px;'>
    		<div class='panel-heading success'>
    		<h4 class='panel-title'>
    		<a data-toggle='collapse' data-parent='#accordion4' href='#ac4-1'>
    		<i class='fa fa-angle-right'></i> Perspectiva Actividades
    		</a>
    		</h4>
    		</div>
    		<div id='ac4-1' class='panel-collapse collapse in'>
    		<div class='panel-body'>
    
    		<div class='col-sm-12 col-md-12'>
    		<div class='tab-container'>
    		<ul class='nav nav-tabs'>
    		<li class='active'><a href='#pers1a' data-toggle='tab'>Diario</a></li>
    		<li><a href='#pers1b' onclick='showMonAvPers1b()' data-toggle='tab'>Consolidado Periodo</a></li>
    		<li><a href='#pers1c' onclick='showMonAvPers1c()' data-toggle='tab'>Consolidado Anual</a></li>
    		</ul>
    		<div class='tab-content'>
    
    		<div class='tab-pane active cont tablaspecial' id='pers1a'>";
    
    		$lista.="<div class='table-responsive'>
    		<table class='table table-bordered' id='datatablepers1a' >
    		<thead class='primary-emphasis'>
    		<tr>
    		<th>Actividad</th>
    		<th>Sub Actividad</th>
    
    		<th>Generadas</th>
    		<th>Finalizadas</th>
    		<th class='text-center danger-emphasis'>Pendientes</th>
    		<th style='width:15%;'>% Avance</th>
    		<th class='text-center primary-emphasis-dark'>Total Finalizadas</th>
    		<th class='text-center danger-emphasis-dark'>% Total Avance</th>
    		<th class='text-center danger-emphasis-dark'>Pendientes Final</th>
    
    		</tr>
    		</thead>
    		<tbody>
    
    		";
    
    		while ($retornomondia1 = pg_fetch_object($querymondia1)){
	    		$varmondia1TipoIntervencion=$retornomondia1->tipointervencion;
	    		$varmondia1SubActividad=$retornomondia1->subactividad;
	    		$varmondia1TotalGeneradas=$retornomondia1->totalgeneradas;
	    		$varmondia1TotalFinalizadas=$retornomondia1->totalfinalizadas;
	    		$varmondia1TotalPendientes=$retornomondia1->totalpendientes;
	    		$varmondia1PorcentajeTotal=$retornomondia1->porcentajetotal;
	    		$varmondia1OrdenesGeneradasDia=$retornomondia1->ordenesgeneradasdia;
				$varmondia1OrdenesFinalizadasDia=$retornomondia1->ordenesfinalizadasdia;
                $varmondia1OrdenesPendientesDia=$retornomondia1->ordenespendientesdia;
                $varmondia1PorcentajeDia=$retornomondia1->porcentajedia;
                $varmondia1OrdenesFinalizadasAcarreo=$retornomondia1->ordenesfinalizadasacarreo;
    
				$totaldiaant=$varmondia1TotalPendientes+$varmondia1OrdenesFinalizadasAcarreo;
    
				if ($totaldiaant>0) {
                	$porcdiaant=($varmondia1OrdenesFinalizadasAcarreo*100)/$totaldiaant;
                	$porcdiaant=round($porcdiaant, 2);
    
                } else {
                	$porcdiaant=0;
                }
    
                if ($varmondia1PorcentajeDia < 33) {
    				$color='danger';
    				$varcolor='color:black;';
    } elseif ($varmondia1PorcentajeDia > 33 && $varmondia1PorcentajeDia < 70) {
    	$color='warning';
    		$varcolor='';
    } else {
    	$color='success';
    		$varcolor='';
    }
    
    
    if ($porcdiaant < 33) {
    $coloro='danger';
    $varcolor='color:black;';
    } elseif ($porcdiaant > 33 && $porcdiaant < 70) {
    $coloro='warning';
    $varcolor='';
    } else {
    $coloro='success';
    
    }
    
    
    
    
    
    $lista.= "
    <tr class='odd gradeX'>
    <td><strong>$varmondia1TipoIntervencion</strong></td>
    <td><strong>$varmondia1SubActividad</strong></td>
    <td><strong>$varmondia1OrdenesGeneradasDia</strong></td>
    <td><strong>$varmondia1OrdenesFinalizadasDia</strong></td>
    <td><strong>$varmondia1OrdenesPendientesDia</strong></td>
    <td class='center'><div class='progress progress-striped active'>
    <div class='progress-bar progress-bar-$color' style='width:$varmondia1PorcentajeDia%;$varcolor'>$varmondia1PorcentajeDia%</div>
    </div></td>
    <td><strong>$varmondia1OrdenesFinalizadasAcarreo</strong></td>
    <td class='center'><div class='progress progress-striped active'>
    <div class='progress-bar progress-bar-$coloro' style='width:$porcdiaant%;$varcolor' >$porcdiaant%</div>
    </div></td>
    <td><strong>$varmondia1TotalPendientes</strong></td>
    </tr>
    ";
    
    
    }
    
    
    
    $lista.="
    </tbody>
    <tfoot >
    <tr>
    <td style='text-align:right;' colspan='2'><strong>Total</strong></td>
    ";
    
    
    $queryfinalmondia1=pg_query("select
    sum(TotalGeneradas) as TotalGeneradas,
    sum(TotalFinalizadas) as TotalFinalizadas,
    sum(TotalPendientes) as TotalPendientes,
    case when sum(TotalGeneradas) > 0 then ((sum(TotalFinalizadas)*100)/sum(TotalGeneradas)) else 0 end as PorcentajeTotal,
    sum(OrdenesGeneradasDia) as OrdenesGeneradasDia,
    sum(OrdenesFinalizadasDia) as OrdenesFinalizadasDia,
    sum(OrdenesPendientesDia) as OrdenesPendientesDia,
    case when sum(OrdenesGeneradasDia) > 0 then ((sum(OrdenesFinalizadasDia)*100)/sum(OrdenesGeneradasDia)) else 0 end as PorcentajeDia,
    case when (sum(TotalPendientes)+sum(OrdenesFinalizadasAcarreo)) > 0 then ((sum(OrdenesFinalizadasAcarreo)*100)/(sum(TotalPendientes)+sum(OrdenesFinalizadasAcarreo))) else 0 end as PorcentajeDiaAcarreo,
    sum(OrdenesFinalizadasAcarreo) as OrdenesFinalizadasAcarreo
    from (select TipoIntervencion,SubActividad,
    sum(TotalGeneradas) as TotalGeneradas,
    sum(TotalFinalizadas) as TotalFinalizadas,
    	((sum(TotalGeneradas))-(sum(TotalFinalizadas))) as TotalPendientes,
    	sum(OrdenesGeneradas) as OrdenesGeneradasDia,
    	sum(OrdenesFinalizadasOrdenesGeneradasDia) as OrdenesFinalizadasDia,
    	((sum(OrdenesGeneradas))-(sum(OrdenesFinalizadasOrdenesGeneradasDia))) as OrdenesPendientesDia,
    	sum(OrdenesFinalizadas) as OrdenesFinalizadasAcarreo
    	from (
    	select ctis.DetalleTipoIntervencion as TipoIntervencion,
    	cti.DetalleTipoIntervencion as SubActividad,
    		(case when (cast(ci.FechaAtencion as date) BETWEEN '2014-06-23' AND '$idfin1') and ci.IdEstado=15 and co.Condicion='0'
    		then COUNT(*) else 0 end) as TotalFinalizadas,
    		(case when (cast(ci.FechaAtencion as date) BETWEEN '$idini1' AND '$idfin1') and ci.IdEstado=15 and co.Condicion='0'
    		then COUNT(*) else 0 end) as OrdenesFinalizadas,
    			(case when (cast(ci.FechaAtencion as date) BETWEEN '$idini1' AND '$idfin1') and (ci.IdEstado=15) and co.Condicion='0' and ci.IdIntervencion in (select ci.IdIntervencion from ComMovTab_Orden co
    			inner join ComMovTab_Intervencion ci
    			on co.IdOrden = ci.IdOrden where cast(ci.FechaAsignado as date) = '$idini1' and ci.IdEstado = 15) and co.Condicion='0'
    			then COUNT(*) else 0 end) as OrdenesFinalizadasOrdenesGeneradasDia,
    			(case when cast(ci.FechaAsignado as date) BETWEEN '2014-06-23' AND '$idfin1' and co.Condicion='0'
    			then COUNT(*) else 0 end) as TotalGeneradas,
    			(case when cast(ci.FechaAsignado as date) BETWEEN '$idini1' AND '$idfin1' and co.Condicion='0'
    				then COUNT(*) else 0 end) as OrdenesGeneradas
    				from ComMovTab_TipoIntervencion cti
    				inner join ComMovTab_TipoIntervencion ctis on ctis.IdTipoIntervencion = cti.IdTipoIntervencionSup
    				left outer join ComMovTab_Orden co on cti.DetalleTipoIntervencion = co.SubActividad
    					left outer join ComMovTab_Intervencion ci on co.IdOrden = ci.IdOrden
    					group by ctis.DetalleTipoIntervencion,cti.DetalleTipoIntervencion,co.Condicion,ci.FechaAtencion,ci.FechaAsignado,co.FechaGeneracion,ci.IdEstado,ci.IdIntervencion) as consulta2
    	group by TipoIntervencion,SubActividad) as ConsultaSumaMon1");
    
    		while ($retornofinalmondia1 = pg_fetch_object($queryfinalmondia1)){
    		$varfinalmondia1TotalGeneradas=$retornofinalmondia1->totalgeneradas;
    		$varfinalmondia1TotalFinalizadas=$retornofinalmondia1->totalfinalizadas;
    		$varfinalmondia1TotalPendientes=$retornofinalmondia1->totalpendientes;
    		$varfinalmondia1PorcentajeTotal=$retornofinalmondia1->porcentajetotal;
    		$varfinalmondia1OrdenesGeneradasDia=$retornofinalmondia1->ordenesgeneradasdia;
    		$varfinalmondia1OrdenesFinalizadasDia=$retornofinalmondia1->ordenesfinalizadasdia;
    		$varfinalmondia1OrdenesPendientesDia=$retornofinalmondia1->ordenespendientesdia;
    		$varfinalmondia1PorcentajeDia=$retornofinalmondia1->porcentajedia;
    		$varfinalmondia1OrdenesFinalizadasAcarreo=$retornofinalmondia1->ordenesfinalizadasacarreo;
    		$varfinalmondia1PorcentajeDiaAcarreo=$retornofinalmondia1->porcentajediaacarreo;
    
    		if ($varfinalmondia1PorcentajeDia< 33) {
    		$color2='danger';
    
    } elseif ($varfinalmondia1PorcentajeDia > 33 && $varfinalmondia1PorcentajeDia< 70) {
    $color2='warning';
    } else {
    $color2='success';
    }
    
    
    if ($varfinalmondia1PorcentajeDiaAcarreo< 33) {
    $color22='danger';
    
    } elseif ($varfinalmondia1PorcentajeDiaAcarreo > 33 && $varfinalmondia1PorcentajeDiaAcarreo< 70) {
    $color22='warning';
    } else {
    $color22='success';
    }
    
    $lista.="
    <td><strong>$varfinalmondia1OrdenesGeneradasDia<strong></td>
    <td><strong>$varfinalmondia1OrdenesFinalizadasDia<strong></td>
    <td><strong>$varfinalmondia1OrdenesPendientesDia<strong></td>
    <td>
    <div class='progress progress-striped active'>
    <div class='progress-bar progress-bar-$color2' style='width: $varfinalmondia1PorcentajeDia%'>$varfinalmondia1PorcentajeDia%</div>
    </div>
    </td>
    
     
    <td><a href='#' data-toggle='modal' data-target='#mod-success' onclick='showModalReportFin(15,10000,1)'><strong>$varfinalmondia1OrdenesFinalizadasAcarreo<strong></a></td>
    <td>
    <div class='progress progress-striped active'>
    <div class='progress-bar progress-bar-$color22' style='width: $varfinalmondia1PorcentajeDiaAcarreo%'>$varfinalmondia1PorcentajeDiaAcarreo%</div>
    </div>
    </td>
    <td><strong>$varfinalmondia1TotalPendientes<strong></td>
    ";
    
    }
    
    
    
    $lista.="</tr>
    </tfoot>
    
    
    </table>
    </div>
    ";
    
    
    
    $lista.="</div>
    
    <div class='tab-pane cont tablaspecial' id='pers1b'>
    <span class='ciclo_loading6' style=''><img src='../js/jquery.select2/select2-spinner.gif'></span>
                                            <div id='tab1b'>
    
                                            </div>
                                            </div>
    
                                            <div class='tab-pane cont tablaspecial' id='pers1c'>
    		<span class='ciclo_loading6' style=''><img src='../js/jquery.select2/select2-spinner.gif'></span>
    		<div id='tab1c'>
    
    		</div>
    		</div>
    
    
    		</div>
    
    
    		</div>
    		</div>
    
    
    		</div>
    
    
    		</div>
    		</div>
    
    
    
    
    		";
    
    } else {
    
    $lista="Hoy no se generaron ni resolvieron ordenes";
    
    }
    
    echo $lista;
?>

