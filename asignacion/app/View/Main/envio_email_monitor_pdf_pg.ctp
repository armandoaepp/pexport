<?php
ini_set('max_execution_time', 300);
date_default_timezone_set('America/Lima');
//require "conexion.php";
require_once(APP."View/Main/conexion.ctp");

//require_once "createactas/dompdf_config.inc.php";
//spl_autoload_register('DOMPDF_autoload');

$mes_actual   = (int) date('m');
$mes_anterior = (int) date('m',strtotime('-1 month', strtotime(date("Y-m-d"))));

$dia_actual   = (int) date('d');
$fecha_actual = date('Y-m-d');

/*
sacar promedio entre cada atencion
select avg(tiempo) as tiempo_promedio from (
    select * from (select *, datediff ( ss, F2 , F1 ) as tiempo from
    (select FechaAtencion AS F1, ROW_NUMBER() OVER(order by FechaAtencion asc) as I1 from ComMovTab_Intervencion
    WHERE FechaAtencion is not null) as t11
    LEFT OUTER JOIN
    (select FechaAtencion AS F2, ROW_NUMBER() OVER(order by FechaAtencion asc) as I2 from ComMovTab_Intervencion
    WHERE FechaAtencion is not null) as t22
    on t11.I1=t22.I2+1) as t3
    ) as t4

 */

function getTiempoString($tiempo_segundos){
	$str_dias = 0;
	$str_horas = 0;
	$str_minutos = 0;
	if($tiempo_segundos>36400){
		$str_dias = floor ($tiempo_segundos/36400);
		$tiempo_segundos = $tiempo_segundos%36400;
	}
	if($tiempo_segundos>3600){
		$str_horas = floor ($tiempo_segundos/3600);
		$tiempo_segundos = $tiempo_segundos%3600;
	}
	if($tiempo_segundos>60){
		$str_minutos = floor ($tiempo_segundos/60);
		$tiempo_segundos = floor ($tiempo_segundos%60);
	}
	
	$str_horas      = str_pad( $str_horas, 2, "0", STR_PAD_LEFT );
	$str_minutos    = str_pad( $str_minutos, 2, "0", STR_PAD_LEFT );
	$tiempo_segundos= str_pad( $tiempo_segundos, 2, "0", STR_PAD_LEFT );
	
	return ($str_dias>0?$str_dias.' días ':' ').$str_horas.':'.$str_minutos.':'.$tiempo_segundos;
}

function getIntervalString($tiempo_pg_interval){
	$tiempo_pg_interval = str_replace('years','a&ntilde;os',$tiempo_pg_interval);
	$tiempo_pg_interval = str_replace('year','a&ntilde;o',$tiempo_pg_interval);
	$tiempo_pg_interval = str_replace('mons','meses',$tiempo_pg_interval);
	$tiempo_pg_interval = str_replace('mon','mes',$tiempo_pg_interval);
	$tiempo_pg_interval = str_replace('days','d&iacute;as',$tiempo_pg_interval);
	$tiempo_pg_interval = str_replace('day','d&iacute;a',$tiempo_pg_interval);
	$tiempo_pg_interval = substr($tiempo_pg_interval,0,-7);
	return $tiempo_pg_interval;
}

$query_dia_actual = pg_query("select 
(select count(ci.IdEstado) as TotalFin  from ComMovTab_Intervencion ci
inner join ComMovTab_Orden co on ci.IdOrden=co.IdOrden
where co.SubActividad in (select DetalleTipoIntervencion from ComMovTab_TipoIntervencion)
and ci.IdEstado in (14,15) and co.Condicion='0' and FechaAtencion>='".$fecha_actual."') as Finalizadas,
(select count(ci.IdEstado) as TotalFin  from ComMovTab_Intervencion ci
inner join ComMovTab_Orden co on ci.IdOrden=co.IdOrden
where co.SubActividad in (select DetalleTipoIntervencion from ComMovTab_TipoIntervencion)
and ci.IdEstado in (15) and co.Condicion='0' and FechaAtencion>='".$fecha_actual."') as FinalizadasEnsa,
(
select count(ci.IdEstado) as Total  from ComMovTab_Intervencion ci
inner join ComMovTab_Orden co on ci.IdOrden=co.IdOrden
where co.SubActividad in (select DetalleTipoIntervencion from ComMovTab_TipoIntervencion) and co.Condicion='0'
and FechaAsignado>='".$fecha_actual."') as Generadas,
(
select count(ci.IdEstado) as Total  from ComMovTab_Intervencion ci
inner join ComMovTab_Orden co on ci.IdOrden=co.IdOrden
where co.SubActividad in (select DetalleTipoIntervencion from ComMovTab_TipoIntervencion) and co.Condicion='0'
and created>='".$fecha_actual."') as GeneradasEnsa,
(
select count(ci.IdEstado) as TotalFin  from ComMovTab_Intervencion ci
inner join ComMovTab_Orden co on ci.IdOrden=co.IdOrden
where co.SubActividad in (select DetalleTipoIntervencion from ComMovTab_TipoIntervencion)
and ci.IdEstado not in (15) and co.Condicion='0') as Pendientes,
(
select count(ci.IdEstado) as TotalFin  from ComMovTab_Intervencion ci
inner join ComMovTab_Orden co on ci.IdOrden=co.IdOrden
inner join ComMovTab_Detalle_Intervencion DI on DI.IdIntervencion=ci.IdIntervencion
where co.SubActividad in (select DetalleTipoIntervencion from ComMovTab_TipoIntervencion)
and co.Condicion='0' and FechaAtencion>='".$fecha_actual."' and DI.IdMaestrosIntervencion in (
select IdMaestrosIntervencion from ComMovTab_MaestrosIntervencion
where IdentificadorMaestroIntervencion like 'LabelInfructuosa%')
) as Infructuosas,
(
select avg(tiempo) as tiempo_promedio from (
select FechaAsignado, FechaAtencion, ((DATE_PART('day', FechaAtencion - FechaAsignado) * 24 + 
                DATE_PART('hour', FechaAtencion - FechaAsignado)) * 60 +
                DATE_PART('minute', FechaAtencion - FechaAsignado)) * 60 +
                DATE_PART('second', FechaAtencion - FechaAsignado) as tiempo
from ComMovTab_Intervencion where FechaAtencion is not null and FechaAtencion>='".$fecha_actual."') as T
) as TiempoPromedio,
(
select count(ci.IdEstado) as TotalFin  from ComMovTab_Intervencion ci
inner join ComMovTab_Orden co on ci.IdOrden=co.IdOrden
where co.SubActividad in (select DetalleTipoIntervencion from ComMovTab_TipoIntervencion)
and ci.IdEstado not in (15) and co.Condicion='0' and DATE_PART('day',NOW()-cast(FechaGeneracion as timestamp))>2) AS pendientes_mayor_2_dias,
(select count(ci.IdEstado) as TotalFin  from ComMovTab_Intervencion ci
inner join ComMovTab_Orden co on ci.IdOrden=co.IdOrden
where co.SubActividad in (select DetalleTipoIntervencion from ComMovTab_TipoIntervencion)
and ci.IdEstado in (14) and co.Condicion='0' and DATE_PART('day',NOW()-cast(FechaGeneracion as timestamp))>2) AS pendientes_exportado_2_dias ");

$count_rows=pg_num_rows($query_dia_actual);
$arr_datos = array();
while ($obj_dia_actual = pg_fetch_object($query_dia_actual)){
  $dia_actual_generadas_ensa = $obj_dia_actual->generadasensa;
  $dia_actual_generadas = $obj_dia_actual->generadas;
  $dia_actual_finalizadas = $obj_dia_actual->finalizadasensa;
  $dia_actual_pendientes = $obj_dia_actual->pendientes;
  $dia_actual_infructuosas = $obj_dia_actual->infructuosas;
  $dia_actual_tiempo_promedio = getTiempoString($obj_dia_actual->tiempopromedio);
}

$query_resumen_2_ultimos_meses = pg_query("select consulta1.anio as Anio,consulta1.mes as Mes,
consulta1.Total as Generadas,
consulta2.TotalFin as Finalizadas,
consulta22.TotalFin as FinalizadasEnsa,
((consulta1.Total)-consulta2.TotalFin) as Pendientes,
((consulta2.TotalFin*100)/consulta1.Total) as Porcentaje,
consulta3.TotalFin as Infructuosa,
consulta4.tiempo_promedio as TiempoPromedio,
consulta5.Total as GeneradasEnsa from
(
select extract('year' from FechaAsignado) as anio, extract('month' from FechaAsignado) as mes,count(ci.IdEstado) as Total  from ComMovTab_Intervencion ci
inner join ComMovTab_Orden co on ci.IdOrden=co.IdOrden
where co.SubActividad in (select DetalleTipoIntervencion from ComMovTab_TipoIntervencion) and co.Condicion='0'
and extract('day' from FechaAsignado)<=".$dia_actual."
group by extract('year' from FechaAsignado), extract('month' from FechaAsignado)) as consulta1
left outer join
(
select extract('year' from FechaAtencion) as anio, extract('month' from FechaAtencion) as mes,count(ci.IdEstado) as TotalFin  from ComMovTab_Intervencion ci
inner join ComMovTab_Orden co on ci.IdOrden=co.IdOrden
where co.SubActividad in (select DetalleTipoIntervencion from ComMovTab_TipoIntervencion)
and ci.IdEstado in (14,15) and co.Condicion='0' and extract('day' from FechaAtencion)<=".$dia_actual."
group by extract('year' from FechaAtencion), extract('month' from FechaAtencion)) as consulta2 on consulta1.anio = consulta2.anio and consulta1.mes = consulta2.mes
left outer join
(
select extract('year' from FechaAtencion) as anio, extract('month' from FechaAtencion) as mes,count(ci.IdEstado) as TotalFin  from ComMovTab_Intervencion ci
inner join ComMovTab_Orden co on ci.IdOrden=co.IdOrden
where co.SubActividad in (select DetalleTipoIntervencion from ComMovTab_TipoIntervencion)
and ci.IdEstado=15 and co.Condicion='0' and extract('day' from FechaAtencion)<=".$dia_actual."
group by extract('year' from FechaAtencion), extract('month' from FechaAtencion)) as consulta22 on consulta1.anio = consulta22.anio and consulta1.mes = consulta22.mes		
left outer join
(
select extract('year' from FechaAtencion) as anio, extract('month' from FechaAtencion) as mes,count(ci.IdEstado) as TotalFin  from ComMovTab_Intervencion ci
inner join ComMovTab_Orden co on ci.IdOrden=co.IdOrden
inner join ComMovTab_Detalle_Intervencion DI on DI.IdIntervencion=ci.IdIntervencion
where co.SubActividad in (select DetalleTipoIntervencion from ComMovTab_TipoIntervencion)
and co.Condicion='0' and extract('day' from FechaAtencion)<=".$dia_actual." and DI.IdMaestrosIntervencion in (
select IdMaestrosIntervencion from ComMovTab_MaestrosIntervencion
where IdentificadorMaestroIntervencion like 'LabelInfructuosa%')
group by extract('year' from FechaAtencion), extract('month' from FechaAtencion)) as consulta3 on consulta1.anio = consulta3.anio and consulta1.mes = consulta3.mes
left outer join
(
select extract('year' from FechaAtencion) as anio, extract('month' from FechaAtencion) as mes,avg(tiempo) as tiempo_promedio from (
select FechaAsignado, FechaAtencion, ((DATE_PART('day', FechaAtencion - FechaAsignado) * 24 + 
                DATE_PART('hour', FechaAtencion - FechaAsignado)) * 60 +
                DATE_PART('minute', FechaAtencion - FechaAsignado)) * 60 +
                DATE_PART('second', FechaAtencion - FechaAsignado) as tiempo
from ComMovTab_Intervencion where FechaAtencion is not null and extract('day' from FechaAtencion)<=".$dia_actual.") as T
group by extract('year' from FechaAtencion), extract('month' from FechaAtencion)) as consulta4 on consulta1.anio = consulta4.anio and consulta1.mes = consulta4.mes
left outer join
(
select extract('year' from created) as anio, extract('month' from created) as mes,count(ci.IdEstado) as Total  from ComMovTab_Intervencion ci
inner join ComMovTab_Orden co on ci.IdOrden=co.IdOrden
where co.SubActividad in (select DetalleTipoIntervencion from ComMovTab_TipoIntervencion) and co.Condicion='0'
and extract('day' from created)<=".$dia_actual."
group by extract('year' from created), extract('month' from created)) as consulta5 on consulta1.anio = consulta5.anio and consulta1.mes = consulta5.mes 
 order by 1 desc,2 desc limit 2");

$count_rows=pg_num_rows($query_resumen_2_ultimos_meses);
$arr_datos = array();
while ($obj_resumen_2_ultimos_meses = pg_fetch_object($query_resumen_2_ultimos_meses)){
  $arr_datos[$obj_resumen_2_ultimos_meses->mes]['Mes'] = $obj_resumen_2_ultimos_meses->mes;
  $arr_datos[$obj_resumen_2_ultimos_meses->mes]['GeneradasEnsa'] = $obj_resumen_2_ultimos_meses->generadasensa;
  $arr_datos[$obj_resumen_2_ultimos_meses->mes]['Generadas'] = $obj_resumen_2_ultimos_meses->generadas;
  $arr_datos[$obj_resumen_2_ultimos_meses->mes]['Finalizadas'] = $obj_resumen_2_ultimos_meses->finalizadasensa;
  $arr_datos[$obj_resumen_2_ultimos_meses->mes]['Pendientes'] = $obj_resumen_2_ultimos_meses->pendientes;
  $arr_datos[$obj_resumen_2_ultimos_meses->mes]['Infructuosa'] = $obj_resumen_2_ultimos_meses->infructuosa;
  $arr_datos[$obj_resumen_2_ultimos_meses->mes]['TiempoProm'] = $obj_resumen_2_ultimos_meses->tiempopromedio;

  $arr_datos[$obj_resumen_2_ultimos_meses->mes]['TiempoPromedio'] = getTiempoString($obj_resumen_2_ultimos_meses->tiempopromedio);
}

if(@$arr_datos[$mes_actual]['GeneradasEnsa']>@$arr_datos[$mes_anterior]['GeneradasEnsa']){
  $icon_generadas_ensa = "<img src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAB8ElEQVR42mNgoDb4/OfD/9UPpv6vv5z4v+C83/+sM17/c0+F/595o/f/u+/v/uPVfOHrgf8114L+L33f+H/j997/s98V/O95Hvu/5UHI/5wzLv+dtuv+3/Vy23+cmuvvRvw/9H/h/6U/qv53fgj93/DM93/VA/f/Jbcd/xdctfmffcH8v+kaqf/bnqMZAnJ22RW///v+zP0/52f+/76vUXADah97gQ3IvWz1P/OcKRgrLZNG9c6K+1P+z3lZDrcZhFtfB4E1V9xz/V980wFsQNoZ4/9xJ/T/W62T+d9+oh1hQOm5iP8bPvX9b30ZCMfozgfZnAjUHHlM+7/HdpX/NkusEQYknHACuwCkAWQrzGYQBmkGYZjtQYc0/1tvlv0vN1kSYUDkYdv/Ex6lgG1DxjDNyLb77FcDB6T0BAmEATGHvP933ouCa4BhUKiDMLJmj70q/7Xmi/63mm2JMKD7Yis4nmGhjIzjoJoDD2qANYNsl53C+79lbwvCAFCUGG5QBfsTZBsMgzSC/AyzGeR3ldlC/yXbRDFT5bon68DxC9OErNFpjyLYZpBmgWbO/+uurcOeGtc8WPNfbr44OJ6dtyuCMUij1kJRsLPFW0Vwa4aBF19f/K/eW/3faLbBf9FuATA2mqz/v2ZrzX+QHNVzLwAjy7LsIPIjLQAAAABJRU5ErkJggg=='>";
}elseif(@$arr_datos[$mes_actual]['GeneradasEnsa']<@$arr_datos[$mes_anterior]['GeneradasEnsa']){
  $icon_generadas_ensa = "<img src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAABrElEQVR42q2TyytEURzH/QFW1JRHIxsTUiNWHgtCFOWRPEpZyAJZkKSZhccgsVBIFqRkZdKUZCFFNLEQJUnJQk0eGcQCk+bj/O6dyZ3rsXLr272dcz7f3/d3zrlRUf/98PIIK1Mw0gzOKnCU6+/FUYJPD/wNH23DcB14F+DcA7sz4B5ScK8+XmuDvXV+h6da4GkfzpZhywWePljqhLlWmGiEwWoCFdbvJlpsNcm1MjmZVwnGfzaQdpT8RdbIdrSe16fhcFEHRWsOtRfdsNCuJ5MWxKCrgMeadPzTowYDp5o8dutQWAKb4tNTqhm8NmdxWZVnMFCDbM3ogFECTzZFVKctl7eGDE6zEwwGalDbbQHCkthSOQyHqstaaeHYHmcw6CjSj0oAoyS2CaY1i6vCJI7Kcr4MgrMD+mK5MEYJaIKlusS/GHMZDNSRvFWmfFUzgyE4UJ/GZW48ezYLAf+96S5srvJckhgJhUCRFBDYmxyNf83z820Mbrjx5cVpMeWoZLfl26cujsTeSbH8Doefj7sbfCMOToozOUyN5cAWw0G+nbN+J+93t/z73/sJB/zttLkU5gUAAAAASUVORK5CYII='>";
}else{
  $icon_generadas_ensa = "<img src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAABpUlEQVR42q2TyytEURzH/QFWRCFTVhJF2WFla0XJwkKxlJ2VRxI1lA2RlRRWnqVGiRqPmJJXYqKGPEOGYdIwg/Nxzpm55ozByq1v3c6538fve85NSPjvhzcfnPTBYQ3slyP2ymC3Ajx21N7fZN8S4qgKAkO8M4N4HQBvB9w26nXhzIabOX4lc1qHn3WCjMFHJ7w0S4EGvc5JtRYJztviRXRsGTeAU5KH+KALQh0xAsJTiXCXadyN22S6h6iImlk89krX4bCz5f7YKN3qowK6j1IeHLncu+yGgCopNBEmWVBkI75KqMgKgeVCPCMlpkBppLCGWFzXaugC3WF3tovxL+aw05NmCMhF3XaE8AVVXGR2y53NQj1CrIDakEelo5pQsY3o2kgKnI3a2OovigqIg/boxyYsokH2ObJw96fgcXQaAvJI1FxWy3HECPllKY/j4XRW2lIJPXu/3YWLaZ5mM+IcLShnRV5rTcS7O/3zbRSnk5yPpOk0odV87ajeL8czdWxnU+rvZOt5f77haqGZvcECNuzJuNqTcHUX4J5qIei/5d//3k/ScyAkq+CgnAAAAABJRU5ErkJggg=='>";
}

if(@$arr_datos[$mes_actual]['Generadas']>@$arr_datos[$mes_anterior]['Generadas']){
  $icon_generadas = "<img src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAB8ElEQVR42mNgoDb4/OfD/9UPpv6vv5z4v+C83/+sM17/c0+F/595o/f/u+/v/uPVfOHrgf8114L+L33f+H/j997/s98V/O95Hvu/5UHI/5wzLv+dtuv+3/Vy23+cmuvvRvw/9H/h/6U/qv53fgj93/DM93/VA/f/Jbcd/xdctfmffcH8v+kaqf/bnqMZAnJ22RW///v+zP0/52f+/76vUXADah97gQ3IvWz1P/OcKRgrLZNG9c6K+1P+z3lZDrcZhFtfB4E1V9xz/V980wFsQNoZ4/9xJ/T/W62T+d9+oh1hQOm5iP8bPvX9b30ZCMfozgfZnAjUHHlM+7/HdpX/NkusEQYknHACuwCkAWQrzGYQBmkGYZjtQYc0/1tvlv0vN1kSYUDkYdv/Ex6lgG1DxjDNyLb77FcDB6T0BAmEATGHvP933ouCa4BhUKiDMLJmj70q/7Xmi/63mm2JMKD7Yis4nmGhjIzjoJoDD2qANYNsl53C+79lbwvCAFCUGG5QBfsTZBsMgzSC/AyzGeR3ldlC/yXbRDFT5bon68DxC9OErNFpjyLYZpBmgWbO/+uurcOeGtc8WPNfbr44OJ6dtyuCMUij1kJRsLPFW0Vwa4aBF19f/K/eW/3faLbBf9FuATA2mqz/v2ZrzX+QHNVzLwAjy7LsIPIjLQAAAABJRU5ErkJggg=='>";
}elseif(@$arr_datos[$mes_actual]['Generadas']<@$arr_datos[$mes_anterior]['Generadas']){
  $icon_generadas = "<img src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAABrElEQVR42q2TyytEURzH/QFW1JRHIxsTUiNWHgtCFOWRPEpZyAJZkKSZhccgsVBIFqRkZdKUZCFFNLEQJUnJQk0eGcQCk+bj/O6dyZ3rsXLr272dcz7f3/d3zrlRUf/98PIIK1Mw0gzOKnCU6+/FUYJPD/wNH23DcB14F+DcA7sz4B5ScK8+XmuDvXV+h6da4GkfzpZhywWePljqhLlWmGiEwWoCFdbvJlpsNcm1MjmZVwnGfzaQdpT8RdbIdrSe16fhcFEHRWsOtRfdsNCuJ5MWxKCrgMeadPzTowYDp5o8dutQWAKb4tNTqhm8NmdxWZVnMFCDbM3ogFECTzZFVKctl7eGDE6zEwwGalDbbQHCkthSOQyHqstaaeHYHmcw6CjSj0oAoyS2CaY1i6vCJI7Kcr4MgrMD+mK5MEYJaIKlusS/GHMZDNSRvFWmfFUzgyE4UJ/GZW48ezYLAf+96S5srvJckhgJhUCRFBDYmxyNf83z820Mbrjx5cVpMeWoZLfl26cujsTeSbH8Doefj7sbfCMOToozOUyN5cAWw0G+nbN+J+93t/z73/sJB/zttLkU5gUAAAAASUVORK5CYII='>";
}else{
  $icon_generadas = "<img src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAABpUlEQVR42q2TyytEURzH/QFWRCFTVhJF2WFla0XJwkKxlJ2VRxI1lA2RlRRWnqVGiRqPmJJXYqKGPEOGYdIwg/Nxzpm55ozByq1v3c6538fve85NSPjvhzcfnPTBYQ3slyP2ymC3Ajx21N7fZN8S4qgKAkO8M4N4HQBvB9w26nXhzIabOX4lc1qHn3WCjMFHJ7w0S4EGvc5JtRYJztviRXRsGTeAU5KH+KALQh0xAsJTiXCXadyN22S6h6iImlk89krX4bCz5f7YKN3qowK6j1IeHLncu+yGgCopNBEmWVBkI75KqMgKgeVCPCMlpkBppLCGWFzXaugC3WF3tovxL+aw05NmCMhF3XaE8AVVXGR2y53NQj1CrIDakEelo5pQsY3o2kgKnI3a2OovigqIg/boxyYsokH2ObJw96fgcXQaAvJI1FxWy3HECPllKY/j4XRW2lIJPXu/3YWLaZ5mM+IcLShnRV5rTcS7O/3zbRSnk5yPpOk0odV87ajeL8czdWxnU+rvZOt5f77haqGZvcECNuzJuNqTcHUX4J5qIei/5d//3k/ScyAkq+CgnAAAAABJRU5ErkJggg=='>";
}

if(@$arr_datos[$mes_actual]['Finalizadas']>@$arr_datos[$mes_anterior]['Finalizadas']){
  $icon_finalizadas = "<img src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAB8ElEQVR42mNgoDb4/OfD/9UPpv6vv5z4v+C83/+sM17/c0+F/595o/f/u+/v/uPVfOHrgf8114L+L33f+H/j997/s98V/O95Hvu/5UHI/5wzLv+dtuv+3/Vy23+cmuvvRvw/9H/h/6U/qv53fgj93/DM93/VA/f/Jbcd/xdctfmffcH8v+kaqf/bnqMZAnJ22RW///v+zP0/52f+/76vUXADah97gQ3IvWz1P/OcKRgrLZNG9c6K+1P+z3lZDrcZhFtfB4E1V9xz/V980wFsQNoZ4/9xJ/T/W62T+d9+oh1hQOm5iP8bPvX9b30ZCMfozgfZnAjUHHlM+7/HdpX/NkusEQYknHACuwCkAWQrzGYQBmkGYZjtQYc0/1tvlv0vN1kSYUDkYdv/Ex6lgG1DxjDNyLb77FcDB6T0BAmEATGHvP933ouCa4BhUKiDMLJmj70q/7Xmi/63mm2JMKD7Yis4nmGhjIzjoJoDD2qANYNsl53C+79lbwvCAFCUGG5QBfsTZBsMgzSC/AyzGeR3ldlC/yXbRDFT5bon68DxC9OErNFpjyLYZpBmgWbO/+uurcOeGtc8WPNfbr44OJ6dtyuCMUij1kJRsLPFW0Vwa4aBF19f/K/eW/3faLbBf9FuATA2mqz/v2ZrzX+QHNVzLwAjy7LsIPIjLQAAAABJRU5ErkJggg=='>";
}elseif(@$arr_datos[$mes_actual]['Finalizadas']<@$arr_datos[$mes_anterior]['Finalizadas']){
  $icon_finalizadas = "<img src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAABrElEQVR42q2TyytEURzH/QFW1JRHIxsTUiNWHgtCFOWRPEpZyAJZkKSZhccgsVBIFqRkZdKUZCFFNLEQJUnJQk0eGcQCk+bj/O6dyZ3rsXLr272dcz7f3/d3zrlRUf/98PIIK1Mw0gzOKnCU6+/FUYJPD/wNH23DcB14F+DcA7sz4B5ScK8+XmuDvXV+h6da4GkfzpZhywWePljqhLlWmGiEwWoCFdbvJlpsNcm1MjmZVwnGfzaQdpT8RdbIdrSe16fhcFEHRWsOtRfdsNCuJ5MWxKCrgMeadPzTowYDp5o8dutQWAKb4tNTqhm8NmdxWZVnMFCDbM3ogFECTzZFVKctl7eGDE6zEwwGalDbbQHCkthSOQyHqstaaeHYHmcw6CjSj0oAoyS2CaY1i6vCJI7Kcr4MgrMD+mK5MEYJaIKlusS/GHMZDNSRvFWmfFUzgyE4UJ/GZW48ezYLAf+96S5srvJckhgJhUCRFBDYmxyNf83z820Mbrjx5cVpMeWoZLfl26cujsTeSbH8Doefj7sbfCMOToozOUyN5cAWw0G+nbN+J+93t/z73/sJB/zttLkU5gUAAAAASUVORK5CYII='>";
}else{
  $icon_finalizadas = "<img src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAABpUlEQVR42q2TyytEURzH/QFWRCFTVhJF2WFla0XJwkKxlJ2VRxI1lA2RlRRWnqVGiRqPmJJXYqKGPEOGYdIwg/Nxzpm55ozByq1v3c6538fve85NSPjvhzcfnPTBYQ3slyP2ymC3Ajx21N7fZN8S4qgKAkO8M4N4HQBvB9w26nXhzIabOX4lc1qHn3WCjMFHJ7w0S4EGvc5JtRYJztviRXRsGTeAU5KH+KALQh0xAsJTiXCXadyN22S6h6iImlk89krX4bCz5f7YKN3qowK6j1IeHLncu+yGgCopNBEmWVBkI75KqMgKgeVCPCMlpkBppLCGWFzXaugC3WF3tovxL+aw05NmCMhF3XaE8AVVXGR2y53NQj1CrIDakEelo5pQsY3o2kgKnI3a2OovigqIg/boxyYsokH2ObJw96fgcXQaAvJI1FxWy3HECPllKY/j4XRW2lIJPXu/3YWLaZ5mM+IcLShnRV5rTcS7O/3zbRSnk5yPpOk0odV87ajeL8czdWxnU+rvZOt5f77haqGZvcECNuzJuNqTcHUX4J5qIei/5d//3k/ScyAkq+CgnAAAAABJRU5ErkJggg=='>";
}

if(@$arr_datos[$mes_actual]['Infructuosa']>@$arr_datos[$mes_anterior]['Infructuosa']){
  $icon_infructuosas = "<img src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAB8ElEQVR42mNgoDb4/OfD/9UPpv6vv5z4v+C83/+sM17/c0+F/595o/f/u+/v/uPVfOHrgf8114L+L33f+H/j997/s98V/O95Hvu/5UHI/5wzLv+dtuv+3/Vy23+cmuvvRvw/9H/h/6U/qv53fgj93/DM93/VA/f/Jbcd/xdctfmffcH8v+kaqf/bnqMZAnJ22RW///v+zP0/52f+/76vUXADah97gQ3IvWz1P/OcKRgrLZNG9c6K+1P+z3lZDrcZhFtfB4E1V9xz/V980wFsQNoZ4/9xJ/T/W62T+d9+oh1hQOm5iP8bPvX9b30ZCMfozgfZnAjUHHlM+7/HdpX/NkusEQYknHACuwCkAWQrzGYQBmkGYZjtQYc0/1tvlv0vN1kSYUDkYdv/Ex6lgG1DxjDNyLb77FcDB6T0BAmEATGHvP933ouCa4BhUKiDMLJmj70q/7Xmi/63mm2JMKD7Yis4nmGhjIzjoJoDD2qANYNsl53C+79lbwvCAFCUGG5QBfsTZBsMgzSC/AyzGeR3ldlC/yXbRDFT5bon68DxC9OErNFpjyLYZpBmgWbO/+uurcOeGtc8WPNfbr44OJ6dtyuCMUij1kJRsLPFW0Vwa4aBF19f/K/eW/3faLbBf9FuATA2mqz/v2ZrzX+QHNVzLwAjy7LsIPIjLQAAAABJRU5ErkJggg=='>";
}elseif(@$arr_datos[$mes_actual]['Infructuosa']<@$arr_datos[$mes_anterior]['Infructuosa']){
  $icon_infructuosas = "<img src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAABrElEQVR42q2TyytEURzH/QFW1JRHIxsTUiNWHgtCFOWRPEpZyAJZkKSZhccgsVBIFqRkZdKUZCFFNLEQJUnJQk0eGcQCk+bj/O6dyZ3rsXLr272dcz7f3/d3zrlRUf/98PIIK1Mw0gzOKnCU6+/FUYJPD/wNH23DcB14F+DcA7sz4B5ScK8+XmuDvXV+h6da4GkfzpZhywWePljqhLlWmGiEwWoCFdbvJlpsNcm1MjmZVwnGfzaQdpT8RdbIdrSe16fhcFEHRWsOtRfdsNCuJ5MWxKCrgMeadPzTowYDp5o8dutQWAKb4tNTqhm8NmdxWZVnMFCDbM3ogFECTzZFVKctl7eGDE6zEwwGalDbbQHCkthSOQyHqstaaeHYHmcw6CjSj0oAoyS2CaY1i6vCJI7Kcr4MgrMD+mK5MEYJaIKlusS/GHMZDNSRvFWmfFUzgyE4UJ/GZW48ezYLAf+96S5srvJckhgJhUCRFBDYmxyNf83z820Mbrjx5cVpMeWoZLfl26cujsTeSbH8Doefj7sbfCMOToozOUyN5cAWw0G+nbN+J+93t/z73/sJB/zttLkU5gUAAAAASUVORK5CYII='>";
}else{
  $icon_infructuosas = "<img src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAABpUlEQVR42q2TyytEURzH/QFWRCFTVhJF2WFla0XJwkKxlJ2VRxI1lA2RlRRWnqVGiRqPmJJXYqKGPEOGYdIwg/Nxzpm55ozByq1v3c6538fve85NSPjvhzcfnPTBYQ3slyP2ymC3Ajx21N7fZN8S4qgKAkO8M4N4HQBvB9w26nXhzIabOX4lc1qHn3WCjMFHJ7w0S4EGvc5JtRYJztviRXRsGTeAU5KH+KALQh0xAsJTiXCXadyN22S6h6iImlk89krX4bCz5f7YKN3qowK6j1IeHLncu+yGgCopNBEmWVBkI75KqMgKgeVCPCMlpkBppLCGWFzXaugC3WF3tovxL+aw05NmCMhF3XaE8AVVXGR2y53NQj1CrIDakEelo5pQsY3o2kgKnI3a2OovigqIg/boxyYsokH2ObJw96fgcXQaAvJI1FxWy3HECPllKY/j4XRW2lIJPXu/3YWLaZ5mM+IcLShnRV5rTcS7O/3zbRSnk5yPpOk0odV87ajeL8czdWxnU+rvZOt5f77haqGZvcECNuzJuNqTcHUX4J5qIei/5d//3k/ScyAkq+CgnAAAAABJRU5ErkJggg=='>";
}

if(@$arr_datos[$mes_actual]['TiempoProm']>@$arr_datos[$mes_anterior]['TiempoProm']){
  $icon_tiempo_promedio = "<img src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAABrElEQVR42q2TyytEURzH/QFW1JRHIxsTUiNWHgtCFOWRPEpZyAJZkKSZhccgsVBIFqRkZdKUZCFFNLEQJUnJQk0eGcQCk+bj/O6dyZ3rsXLr272dcz7f3/d3zrlRUf/98PIIK1Mw0gzOKnCU6+/FUYJPD/wNH23DcB14F+DcA7sz4B5ScK8+XmuDvXV+h6da4GkfzpZhywWePljqhLlWmGiEwWoCFdbvJlpsNcm1MjmZVwnGfzaQdpT8RdbIdrSe16fhcFEHRWsOtRfdsNCuJ5MWxKCrgMeadPzTowYDp5o8dutQWAKb4tNTqhm8NmdxWZVnMFCDbM3ogFECTzZFVKctl7eGDE6zEwwGalDbbQHCkthSOQyHqstaaeHYHmcw6CjSj0oAoyS2CaY1i6vCJI7Kcr4MgrMD+mK5MEYJaIKlusS/GHMZDNSRvFWmfFUzgyE4UJ/GZW48ezYLAf+96S5srvJckhgJhUCRFBDYmxyNf83z820Mbrjx5cVpMeWoZLfl26cujsTeSbH8Doefj7sbfCMOToozOUyN5cAWw0G+nbN+J+93t/z73/sJB/zttLkU5gUAAAAASUVORK5CYII='>";
}elseif(@$arr_datos[$mes_actual]['TiempoProm']<@$arr_datos[$mes_anterior]['TiempoProm']){
  $icon_tiempo_promedio = "<img src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAB8ElEQVR42mNgoDb4/OfD/9UPpv6vv5z4v+C83/+sM17/c0+F/595o/f/u+/v/uPVfOHrgf8114L+L33f+H/j997/s98V/O95Hvu/5UHI/5wzLv+dtuv+3/Vy23+cmuvvRvw/9H/h/6U/qv53fgj93/DM93/VA/f/Jbcd/xdctfmffcH8v+kaqf/bnqMZAnJ22RW///v+zP0/52f+/76vUXADah97gQ3IvWz1P/OcKRgrLZNG9c6K+1P+z3lZDrcZhFtfB4E1V9xz/V980wFsQNoZ4/9xJ/T/W62T+d9+oh1hQOm5iP8bPvX9b30ZCMfozgfZnAjUHHlM+7/HdpX/NkusEQYknHACuwCkAWQrzGYQBmkGYZjtQYc0/1tvlv0vN1kSYUDkYdv/Ex6lgG1DxjDNyLb77FcDB6T0BAmEATGHvP933ouCa4BhUKiDMLJmj70q/7Xmi/63mm2JMKD7Yis4nmGhjIzjoJoDD2qANYNsl53C+79lbwvCAFCUGG5QBfsTZBsMgzSC/AyzGeR3ldlC/yXbRDFT5bon68DxC9OErNFpjyLYZpBmgWbO/+uurcOeGtc8WPNfbr44OJ6dtyuCMUij1kJRsLPFW0Vwa4aBF19f/K/eW/3faLbBf9FuATA2mqz/v2ZrzX+QHNVzLwAjy7LsIPIjLQAAAABJRU5ErkJggg=='>";
}else{
  $icon_tiempo_promedio = "<img src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAABpUlEQVR42q2TyytEURzH/QFWRCFTVhJF2WFla0XJwkKxlJ2VRxI1lA2RlRRWnqVGiRqPmJJXYqKGPEOGYdIwg/Nxzpm55ozByq1v3c6538fve85NSPjvhzcfnPTBYQ3slyP2ymC3Ajx21N7fZN8S4qgKAkO8M4N4HQBvB9w26nXhzIabOX4lc1qHn3WCjMFHJ7w0S4EGvc5JtRYJztviRXRsGTeAU5KH+KALQh0xAsJTiXCXadyN22S6h6iImlk89krX4bCz5f7YKN3qowK6j1IeHLncu+yGgCopNBEmWVBkI75KqMgKgeVCPCMlpkBppLCGWFzXaugC3WF3tovxL+aw05NmCMhF3XaE8AVVXGR2y53NQj1CrIDakEelo5pQsY3o2kgKnI3a2OovigqIg/boxyYsokH2ObJw96fgcXQaAvJI1FxWy3HECPllKY/j4XRW2lIJPXu/3YWLaZ5mM+IcLShnRV5rTcS7O/3zbRSnk5yPpOk0odV87ajeL8czdWxnU+rvZOt5f77haqGZvcECNuzJuNqTcHUX4J5qIei/5d//3k/ScyAkq+CgnAAAAABJRU5ErkJggg=='>";
}


$time=date('Y-m-d');

$lista="
  <html>
<head>
<link rel='stylesheet' href='../fonts/font-awesome-4/css/font-awesome.css'>
<style>

body{
  font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif; 
  color: #333;
}
.imagen{
  padding-left:5px;
  padding-right: 5px;
}

.text-header{
  width:100%; 
  height: 50px; 
  padding-top:20px; 
  padding-bottom:10px; 
  position: absolute; 
  top:-50px; 
  display: inline; 
  text-align:center;
  color: rgb(36,148,242); 
  margin-bottom: 15px;
  border-bottom: 1px solid #E3E3E3;
  box-shadow: 0px 1px 1px rgba(0, 0, 0, 0.05) inset;
  font-size: 25px;
  font-weight: bold;

}

.subtitulo{
  padding: 8px 22px;
  width: 40%;
  color: #444;
  margin-bottom: 20px;
  background-color: #f5f5f5;
  border-left: 5px solid rgb(36,148,242);
  font-weight: bold;
}

.titulo {
  width:80%; 
  color: #333;
  margin-left:40%; 
  position: absolute; 
  top: 0px;
}

.tabla{
  padding: 20px; 
  margin: none;
  zoom:0.9;
  font-size:12.5px;
  
}

.tabla-titulo{
  background: rgb(36,148,242); 
  border-bottom-color:#E3E3E3; 
  color:#fff;
  font-size:12px;

}

.tabla-fila{
  width:50%; 
  box-shadow: none;
  font-size:12px; 
}

.tabla-campo{
  text-align: center; 
  display: table-cell;
  width: 40%;
  background-color: #F5F5F5;
  box-shadow: none; 
  height: auto;
  line-height: 21px;
  padding-top: 10px;
  padding-bottom: 5px;
  padding-left: 2px;
  padding-right: 2px;
  font-size: 12px;
}

.tabla-foot{
  text-align: center; 
  width:40%; 
  background-color: #ddd;
  border: 1px #ddd; 
  box-shadow: none; 
  padding: 8px 8px;  
  height: auto;
  font-size:12px;
  font-weight: bold ;
}

a{
  text-decoration: none;
}


.tabla-col{
  text-align:center; 
  padding: 8px 2px; 
  width:20%; 
  border: 1px #ddd; 
  box-shadow: none; 
  height: auto;
  font-weight: 200;
  line-height: 21px;
  background-color:  #F5F5F5;
  font-size:12px;
}

.tabla-colr{
  text-align: left; 
  width:40%; 
  background-color: #F5F5F5;
  padding: 8px 4px; 
  border: 1px #ddd; 
  box-shadow: none; 
  padding: 8px 8px;  
  height: auto;
  font-size:12px;
  font-weight: bold ;

}

.espacio{
  height:80px; 
}


.text-right{
  text-align: right; 
}

</style>
</head>

<body>
 <h2 class='text-header' >RESUMEN DEL D&Iacute;A - ACTIVIDADES COMERCIALES ENSA</h2>
 <p class='titulo' > ".$time."</p>

   <div class='espacio'></div>

   <div class='fondo' style='width:90%; padding: 0; height: auto;' >
        <h3 class='subtitulo' >KPI 1 - Indicadores Clave de Proceso</h3>

        <table class='tabla' style='page-break-after:always' >
          <thead class='tabla-titulo'>
            <tr class='tabla-fila'>
              <th class='tabla-campo'>Name</th>
              <th class='tabla-campo'><span></span>D&iacute;a Actual</th>
              <th class='tabla-campo'><span></span>Mes Actual</th>
              <th class='tabla-campo'><span></span>Mes Anterior</th>
              <th class='tabla-campo'><span></span>KPI</th>
            </tr>
          </thead>

          <tbody class='no-border-x'>
            
            <tr class='tabla-fila'  >
              
              <td class='tabla-colr' ><img class='imagen' src='./images/img1.png'> Ordenes Generadas Ensa</td>
              <td class='tabla-col'>".$dia_actual_generadas_ensa."</td>
              <td class='tabla-col'>".@$arr_datos[$mes_actual]['GeneradasEnsa']."</td>
              <td class='tabla-col'>".@$arr_datos[$mes_anterior]['GeneradasEnsa']."</td>
              <td class='tabla-col'>
              ".$icon_generadas_ensa."
              </td>
            </tr>
            <tr class='tabla-fila' >
              <td class='tabla-colr'><img class='imagen' src='./images/img1.png'> Ordenes Recibidas Pexport</td>
              <td class='tabla-col'>".$dia_actual_generadas."</td>
              <td class='tabla-col'>".@$arr_datos[$mes_actual]['Generadas']."</td>
              <td class='tabla-col'>".@$arr_datos[$mes_anterior]['Generadas']."</td>
              <td class='tabla-col'>
              ".$icon_generadas."
              </td>
            </tr>
            <tr class='tabla-fila'>
              <td class='tabla-colr'><img class='imagen' src='./images/img2.png'>Ordenes Finalizadas</td>
              <td class='tabla-col'>".$dia_actual_finalizadas."</td>
              <td class='tabla-col'>".@$arr_datos[$mes_actual]['Finalizadas']."</td>
              <td class='tabla-col'>".@$arr_datos[$mes_anterior]['Finalizadas']."</td>
              <td class='tabla-col'>
              ".$icon_finalizadas."
              </td>
            </tr>
            <tr class='tabla-fila' >
              <td class='tabla-colr'><img class='imagen' src='./images/img3.png'> Visitas Infructuosas</td>
              <td class='tabla-col'><a href='#' data-toggle='modal' data-target='#mod-success4' onclick='showModalReportInfructuosas()'>".$dia_actual_infructuosas."</a></td>
              <td class='tabla-col'>".@$arr_datos[$mes_actual]['Infructuosa']."</td>
              <td class='tabla-col'>".@$arr_datos[$mes_anterior]['Infructuosa']."</td>
              <td class='tabla-col'>
              ".$icon_infructuosas."
              </td>
            </tr>
            <tr class='tabla-fila' >
              <td class='tabla-colr'><img class='imagen' src='./images/img4.png'>Tiempo de Ejecucion</td>
              <td class='tabla-col'>".$dia_actual_tiempo_promedio."</td>
              <td class='tabla-col'>".@$arr_datos[$mes_actual]['TiempoPromedio']."</td>
              <td class='tabla-col'>".@$arr_datos[$mes_anterior]['TiempoPromedio']."</td>
              <td class='tabla-col'>
              ".$icon_tiempo_promedio."
              </td>
            </tr>
            <tr class='tabla-fila'>
              <td class='tabla-foot'><i ></i><b> Ordenes Pendientes</b></td>
              <td class='tabla-foot'><b>".$dia_actual_pendientes."</b></td>
              <td class='tabla-foot' class='text-right'></td>
              <td class='tabla-foot' class='text-right'></td>
              <td class='tabla-foot' class='text-right'></td>
            </tr >
            <tr class='tabla-fila'>
            <td class='text-right' colspan='5'>
             * Datos comparados entre los ".$dia_actual." primeros d&iacute;as de cada mes.
            </td>
            </tr>

          </tbody>
        </table>

    </div>


";


$mes=date("m");


  $idini1=$fecha_actual ;
  $idfin1=$fecha_actual ;




$querymondia1 = pg_query("select TipoIntervencion,SubActividad,
sum(TotalGeneradas) as TotalGeneradas,
sum(TotalFinalizadas) as TotalFinalizadas,
((sum(TotalGeneradas))-(sum(TotalFinalizadas))) as TotalPendientes,
case when sum(TotalGeneradas) > 0 then ((sum(TotalFinalizadas)*100)/sum(TotalGeneradas)) else 0 end as PorcentajeTotal, 
sum(OrdenesGeneradas) as OrdenesGeneradasDia,
sum(OrdenesFinalizadasOrdenesGeneradasDia) as OrdenesFinalizadasDia,
((sum(OrdenesGeneradas))-(sum(OrdenesFinalizadasOrdenesGeneradasDia))) as OrdenesPendientesDia,
case when sum(OrdenesGeneradas) > 0 then ((sum(OrdenesFinalizadasOrdenesGeneradasDia)*100)/sum(OrdenesGeneradas)) else 0 end as PorcentajeDia,  
sum(OrdenesFinalizadas) as OrdenesFinalizadasAcarreo
from (
select ctis.DetalleTipoIntervencion as TipoIntervencion,
cti.DetalleTipoIntervencion as SubActividad,
(case when (cast(ci.FechaAtencion as date) BETWEEN '2014-06-23' AND '$idfin1') and ci.IdEstado=15 and co.Condicion='0'
then COUNT(*) else 0 end) as TotalFinalizadas,
(case when (cast(ci.FechaAtencion as date) BETWEEN '$idini1' AND '$idfin1') and ci.IdEstado=15 and co.Condicion='0'
then COUNT(*) else 0 end) as OrdenesFinalizadas,
(case when (cast(ci.FechaAtencion as date) BETWEEN '$idini1' AND '$idfin1') and (ci.IdEstado=15) and co.Condicion='0' and ci.IdIntervencion in (select ci.IdIntervencion from ComMovTab_Orden co 
inner join ComMovTab_Intervencion ci 
on co.IdOrden = ci.IdOrden where cast(ci.FechaAsignado as date) = '$idfin1' and ci.IdEstado = 15) and co.Condicion='0'
then COUNT(*) else 0 end) as OrdenesFinalizadasOrdenesGeneradasDia,
(case when cast(ci.FechaAsignado as date) BETWEEN '2014-06-23' AND '$idfin1' and co.Condicion='0'
then COUNT(*) else 0 end) as TotalGeneradas,
(case when cast(ci.FechaAsignado as date) BETWEEN '$idini1' AND '$idfin1' and co.Condicion='0'
then COUNT(*) else 0 end) as OrdenesGeneradas
from ComMovTab_TipoIntervencion cti 
inner join ComMovTab_TipoIntervencion ctis on ctis.IdTipoIntervencion = cti.IdTipoIntervencionSup 
left outer join ComMovTab_Orden co on cti.DetalleTipoIntervencion = co.SubActividad
left outer join ComMovTab_Intervencion ci on co.IdOrden = ci.IdOrden
group by ctis.DetalleTipoIntervencion,cti.DetalleTipoIntervencion,co.Condicion,ci.FechaAtencion,co.FechaGeneracion,ci.FechaAsignado,ci.IdEstado,ci.IdIntervencion) as consulta2
group by TipoIntervencion,SubActividad");


$lista.="

  <div>
    <h3 class='subtitulo' >Perspectiva Actividades</h3>
    <table class='tabla' style='page-break-after:always;' >
    <thead class='tabla-titulo'>
    <tr class='tabla-fila'>
      <th class='tabla-campo'>Actividad</th>
      <th class='tabla-campo'>Sub Actividad</th>
      <th class='tabla-campo'>Generadas</th>
      <th class='tabla-campo'>Finalizadas</th>
      <th class='tabla-campo'>Pendientes</th>
      <th class='tabla-campo'>% Avance</th>
      <th class='tabla-campo'>Total Finalizadas</th>
      <th class='tabla-campo'>% Total Avance</th>
      <th class='tabla-campo'>Pendientes Final</th>

    </tr>
    </thead>
    <tbody>

    ";

    while ($retornomondia1 = pg_fetch_object($querymondia1)){
                        $varmondia1TipoIntervencion=$retornomondia1->tipointervencion;
                        $varmondia1SubActividad=$retornomondia1->subactividad;
                        $varmondia1TotalGeneradas=$retornomondia1->totalgeneradas;
                        $varmondia1TotalFinalizadas=$retornomondia1->totalfinalizadas;
                        $varmondia1TotalPendientes=$retornomondia1->totalpendientes;
                        $varmondia1PorcentajeTotal=$retornomondia1->porcentajetotal;
                        $varmondia1OrdenesGeneradasDia=$retornomondia1->ordenesgeneradasdia;
                        $varmondia1OrdenesFinalizadasDia=$retornomondia1->ordenesfinalizadasdia;
                        $varmondia1OrdenesPendientesDia=$retornomondia1->ordenespendientesdia;
                        $varmondia1PorcentajeDia=$retornomondia1->porcentajedia;
                        $varmondia1OrdenesFinalizadasAcarreo=$retornomondia1->ordenesfinalizadasacarreo;

                        $totaldiaant=$varmondia1TotalPendientes+$varmondia1OrdenesFinalizadasAcarreo;

                        if ($totaldiaant>0) {
                          $porcdiaant=($varmondia1OrdenesFinalizadasAcarreo*100)/$totaldiaant;
                          $porcdiaant=round($porcdiaant, 2);

                        } else {
                          $porcdiaant=0;
                          
                        }

                        

                        if ($varmondia1PorcentajeDia < 33) {
                          $color='danger';
                          $varcolor='color:black;';
                        } elseif ($varmondia1PorcentajeDia > 33 && $varmondia1PorcentajeDia < 70) {
                          $color='warning';
                          $varcolor='';
                        } else {
                          $color='success';
                          $varcolor='';
                        }


                        if ($porcdiaant < 33) {
                          $coloro='danger';
                          $varcolor='color:black;';
                        } elseif ($porcdiaant > 33 && $porcdiaant < 70) {
                          $coloro='warning';
                          $varcolor='';
                        } else {
                          $coloro='success';
                          
                        }

                        



    $lista.= "
      
     <tr class='tabla-fila'>
      <td class='tabla-col' ><strong>$varmondia1TipoIntervencion</strong></td>
      <td class='tabla-col' ><strong>$varmondia1SubActividad</strong></td>
      <td class='tabla-col' ><strong>$varmondia1OrdenesGeneradasDia</strong></td>
      <td class='tabla-col' ><strong>$varmondia1OrdenesFinalizadasDia</strong></td>
      <td class='tabla-col' ><strong>$varmondia1OrdenesPendientesDia</strong></td>
      <td class='tabla-col' ><div >
                              <div >".number_format($varmondia1PorcentajeDia,2)."%</div>
      </div></td>
      <td class='tabla-col' ><strong>$varmondia1OrdenesFinalizadasAcarreo</strong></td>
      <td class='tabla-col'  ><div >
                              <div>".number_format($porcdiaant,2)."%</div>
      </div></td>
      <td class='tabla-col' ><strong>$varmondia1TotalPendientes</strong></td>
     </tr>
      ";


      }



    $lista.="
    </tbody>
    <tfoot >
    <tr>
    <td class='tabla-foot'  colspan='2'><strong>Total</strong></td>
    ";


    $queryfinalmondia1=pg_query("select 
                          sum(TotalGeneradas) as TotalGeneradas,
                          sum(TotalFinalizadas) as TotalFinalizadas,
                          sum(TotalPendientes) as TotalPendientes,
                          case when sum(TotalGeneradas) > 0 then ((sum(TotalFinalizadas)*100)/sum(TotalGeneradas)) else 0 end as PorcentajeTotal, 
                          sum(OrdenesGeneradasDia) as OrdenesGeneradasDia,
                          sum(OrdenesFinalizadasDia) as OrdenesFinalizadasDia,
                          sum(OrdenesPendientesDia) as OrdenesPendientesDia,
                          case when sum(OrdenesGeneradasDia) > 0 then ((sum(OrdenesFinalizadasDia)*100)/sum(OrdenesGeneradasDia)) else 0 end as PorcentajeDia,
                          case when (sum(TotalPendientes)+sum(OrdenesFinalizadasAcarreo)) > 0 then ((sum(OrdenesFinalizadasAcarreo)*100)/(sum(TotalPendientes)+sum(OrdenesFinalizadasAcarreo))) else 0 end as PorcentajeDiaAcarreo,  
                          sum(OrdenesFinalizadasAcarreo) as OrdenesFinalizadasAcarreo
                          from (select TipoIntervencion,SubActividad,
                          sum(TotalGeneradas) as TotalGeneradas,
                          sum(TotalFinalizadas) as TotalFinalizadas,
                          ((sum(TotalGeneradas))-(sum(TotalFinalizadas))) as TotalPendientes, 
                          sum(OrdenesGeneradas) as OrdenesGeneradasDia,
                          sum(OrdenesFinalizadasOrdenesGeneradasDia) as OrdenesFinalizadasDia,
                          ((sum(OrdenesGeneradas))-(sum(OrdenesFinalizadasOrdenesGeneradasDia))) as OrdenesPendientesDia, 
                          sum(OrdenesFinalizadas) as OrdenesFinalizadasAcarreo
                          from (
                          select ctis.DetalleTipoIntervencion as TipoIntervencion,
                          cti.DetalleTipoIntervencion as SubActividad,
                          (case when (cast(ci.FechaAtencion as date) BETWEEN '2014-06-23' AND '$idfin1') and ci.IdEstado=15 and co.Condicion='0'
                          then COUNT(*) else 0 end) as TotalFinalizadas,
                          (case when (cast(ci.FechaAtencion as date) BETWEEN '$idini1' AND '$idfin1') and ci.IdEstado=15 and co.Condicion='0'
                          then COUNT(*) else 0 end) as OrdenesFinalizadas,
                          (case when (cast(ci.FechaAtencion as date) BETWEEN '$idini1' AND '$idfin1') and (ci.IdEstado=15) and co.Condicion='0' and ci.IdIntervencion in (select ci.IdIntervencion from ComMovTab_Orden co 
                          inner join ComMovTab_Intervencion ci 
                          on co.IdOrden = ci.IdOrden where cast(ci.FechaAsignado as date) = '$idini1' and ci.IdEstado = 15) and co.Condicion='0'
                          then COUNT(*) else 0 end) as OrdenesFinalizadasOrdenesGeneradasDia,
                          (case when cast(ci.FechaAsignado as date) BETWEEN '2014-06-23' AND '$idfin1' and co.Condicion='0'
                          then COUNT(*) else 0 end) as TotalGeneradas,
                          (case when cast(ci.FechaAsignado as date) BETWEEN '$idini1' AND '$idfin1' and co.Condicion='0'
                          then COUNT(*) else 0 end) as OrdenesGeneradas
                          from ComMovTab_TipoIntervencion cti 
                          inner join ComMovTab_TipoIntervencion ctis on ctis.IdTipoIntervencion = cti.IdTipoIntervencionSup 
                          left outer join ComMovTab_Orden co on cti.DetalleTipoIntervencion = co.SubActividad
                          left outer join ComMovTab_Intervencion ci on co.IdOrden = ci.IdOrden
                          group by ctis.DetalleTipoIntervencion,cti.DetalleTipoIntervencion,co.Condicion,ci.FechaAtencion,ci.FechaAsignado,co.FechaGeneracion,ci.IdEstado,ci.IdIntervencion) as consulta2
                          group by TipoIntervencion,SubActividad) as ConsultaSumaMon1");     
                              while ($retornofinalmondia1 = pg_fetch_object($queryfinalmondia1)){
                                        $varfinalmondia1TotalGeneradas=$retornofinalmondia1->totalgeneradas;
                                        $varfinalmondia1TotalFinalizadas=$retornofinalmondia1->totalfinalizadas;
                                        $varfinalmondia1TotalPendientes=$retornofinalmondia1->totalpendientes;
                                        $varfinalmondia1PorcentajeTotal=$retornofinalmondia1->porcentajetotal;
                                        $varfinalmondia1OrdenesGeneradasDia=$retornofinalmondia1->ordenesgeneradasdia;
                                        $varfinalmondia1OrdenesFinalizadasDia=$retornofinalmondia1->ordenesfinalizadasdia;
                                        $varfinalmondia1OrdenesPendientesDia=$retornofinalmondia1->ordenespendientesdia;
                                        $varfinalmondia1PorcentajeDia=$retornofinalmondia1->porcentajedia;
                                        $varfinalmondia1OrdenesFinalizadasAcarreo=$retornofinalmondia1->ordenesfinalizadasacarreo;
                                        $varfinalmondia1PorcentajeDiaAcarreo=$retornofinalmondia1->porcentajediaacarreo;

                                        if ($varfinalmondia1PorcentajeDia< 33) {
                                          $color2='danger';
                                          
                                        } elseif ($varfinalmondia1PorcentajeDia > 33 && $varfinalmondia1PorcentajeDia< 70) {
                                          $color2='warning';
                                        } else {
                                          $color2='success';
                                        }


                                        if ($varfinalmondia1PorcentajeDiaAcarreo< 33) {
                                          $color22='danger';
                                          
                                        } elseif ($varfinalmondia1PorcentajeDiaAcarreo > 33 && $varfinalmondia1PorcentajeDiaAcarreo< 70) {
                                          $color22='warning';
                                        } else {
                                          $color22='success';
                                        }

                                        $lista.="
                                                 <td class='tabla-foot' ><strong>$varfinalmondia1OrdenesGeneradasDia<strong></td>
                                                 <td class='tabla-foot' ><strong>$varfinalmondia1OrdenesFinalizadasDia<strong></td>
                                                 <td class='tabla-foot' ><strong>$varfinalmondia1OrdenesPendientesDia<strong></td>
                                                 <td class='tabla-foot' >
                                                    <div >
                                                    <div >".number_format($varfinalmondia1PorcentajeDia,2)."%</div>
                                                    </div>
                                                 </td>
                      

                                                 <td class='tabla-foot' ><a href='#' data-toggle='modal' data-target='#mod-success' onclick='showModalReportFin(15,10000,1)'><strong>$varfinalmondia1OrdenesFinalizadasAcarreo<strong></a></td>
                                                 <td class='tabla-foot' >
                                                    <div >
                                                    <div >".number_format($varfinalmondia1PorcentajeDiaAcarreo,2)."%</div>
                                                    </div>
                                                 </td>
                                                 <td class='tabla-foot' ><strong>$varfinalmondia1TotalPendientes<strong></td>
                                        ";

                                    }



    $lista.="</tr>
    </tfoot>


</table>
</div>";



$querymon2 = pg_query("
select IdUsuario,NomPersona,ApePersona,
sum(GeneradaPexTotal) as GenPexTotal,
sum(GeneradaEnMes) as GenEnMes,
sum(GeneradaPexMes) as GenPexMes,
sum(GeneradaPexDia) as GenPexDia,
sum(GeneradaEnDia) as GenEnDia,
sum(FinalizadaDia) as FinDia,
sum(MesFin) as FinMes,
sum(TotalFin) as FinTotal,
(sum(GeneradaPexTotal)-sum(TotalFin)) as Pendientes,
((sum(TotalFin)*100)/sum(GeneradaPexTotal)) as Porcentaje 
from(
select 
ci.IdUsuario,
gp.NomPersona,
gp.ApePersona,
count(ci.IdEstado) as GeneradaPexTotal,
case when extract('month' from cast(co.FechaGeneracion as timestamp))=$mes  then count(ci.IdEstado) else 0 end as GeneradaEnMes,
case when extract('month' from ci.FechaAsignado)=$mes then count(ci.IdEstado) else 0 end as GeneradaPexMes,
case when cast(ci.FechaAsignado as date)='$idfin1' then count(ci.IdEstado) else 0 end as GeneradaPexDia,
case when cast(co.FechaGeneracion as date)='$idfin1' then count(ci.IdEstado) else 0 end as GeneradaEnDia,
case when cast(ci.FechaAtencion as date) BETWEEN '$idini1' and '$idfin1' and ci.IdEstado=15 then count(ci.IdEstado) else 0 end as FinalizadaDia, 
case when extract('month' from ci.FechaAtencion)=$mes and ci.IdEstado=15 then count(ci.IdEstado) else 0 end as MesFin,  
case when ci.IdEstado=15 then count(ci.IdEstado) else 0 end as TotalFin  from ComMovTab_Intervencion ci
inner join ComMovTab_Orden co on ci.IdOrden=co.IdOrden
inner join ComMovTab_Usuario cu on ci.IdUsuario = cu.IdUsuario
inner join GlobalMaster_Persona gp on cu.IdPersona = gp.IdPersona
where co.SubActividad in (select DetalleTipoIntervencion from ComMovTab_TipoIntervencion)
and co.Condicion='0'
group by ci.IdUsuario,gp.NomPersona,gp.ApePersona,ci.FechaAtencion,ci.FechaAsignado,ci.IdEstado,co.FechaGeneracion) as superconsulta
group by IdUsuario,NomPersona,ApePersona
");

$lista.="

       <div class='table-responsive' style='page-break-after:always;'>
              <h3 class='subtitulo' > Perspectiva Tecnico de Campo</h3>
              <table  class='table table-bordered' id='datatablepers2aa' >
              <thead class='primary-emphasis'>
              <tr class='tabla-titulo' style='page-break-after:always' >
                  <th class='tabla-campo' >Codigo</th>
                  <th class='tabla-campo' >Tecnico Inspector</th>
                  <th class='tabla-campo' >Ordenes Generadas</th>
                  <th class='tabla-campo' >Importada WS</th>
                  <th class='tabla-campo' >Porcentaje de Avance</th>";
                  
                  $query2a = pg_query("select * from ComMovTab_MaestrosEstado where EstadoSup=11 and IdEstado!=12 and IdEstado!=15");

                  while ($retornocom2a = pg_fetch_object($query2a)){
                                                $varidestado=$retornocom2a->idestado;
                                                $varnomestado=$retornocom2a->nomestado;
                                                $varestadosup=$retornocom2a->estadosup;

                                                $lista.="<th class='tabla-campo' >$varnomestado</th>";

                                            }
                  
              $lista.="
                  <th class='tabla-campo'>Finalizada Dia</th>
                  <th class='tabla-campo' >Finalizada Mes</th>
                  <th class='tabla-campo' class='text-center danger-emphasis-dark'>Pendientes</th>
              </tr>
              </thead>
              <tbody>";
                while ($retornocom2 = pg_fetch_object($querymon2)){
                                    $varidusuario=$retornocom2->idusuario;
                                    $varnompersona=$retornocom2->nompersona;
                                    $varapepersona=$retornocom2->apepersona;

                                    $vargeneradas=$retornocom2->genendia;
                                    $vargeneradasws=$retornocom2->genpexdia;
                                    $varfinalizadasdia=$retornocom2->findia;
                                    $varfinalizadasmes=$retornocom2->finmes;

                                    $varpendientes=$retornocom2->pendientes;
                                    $varporcentajemon2=$retornocom2->porcentaje;


                                    $totaltecgen[]=$vargeneradas;
                                    $totaltecgenpex[]=$vargeneradasws;
                                    $totaltecfindia[]=$varfinalizadasdia;
                                    $totaltecfinmes[]=$varfinalizadasmes;
                                    $totaltecpen[]=$varpendientes;

                                    if ($varporcentajemon2 < 33) {
                                      $color3='danger';
                                      
                                    } elseif ($varporcentajemon2 > 33 && $varporcentajemon2 < 70) {
                                      $color3='warning';
                                    } else {
                                      $color3='success';
                                    }

                                

                $lista.= "
                <tr class='odd gradeX'>
                  <td class='tabla-col'><strong>$varidusuario</strong></td>
                  <td class='tabla-col'><strong>$varnompersona $varapepersona</strong></td>
                  <td class='tabla-col'><strong>$vargeneradas</strong></td>
                  <td class='tabla-col'><strong>$vargeneradasws</strong></td>
                  <td class='tabla-col' class='center'><div class='progress progress-striped active'>
                <div class='progress-bar progress-bar-$color3' style='width: $varporcentajemon2%'>".number_format($varporcentajemon2,2)."%</div>
               </div></td>"
                  ;
                  $query3mon2 = pg_query("select me.IdEstado as IdEstado,me.NomEstado,consulta1.cantidad as cantidad from ComMovTab_MaestrosEstado me 
                                              left outer join ( select ci.IdEstado as Id,COUNT(*) as cantidad from ComMovTab_Intervencion ci join
                                              ComMovTab_Orden co on ci.IdOrden = co.IdOrden where ci.IdUsuario=$varidusuario and co.Condicion='0'
                                              and co.SubActividad in (select DetalleTipoIntervencion from ComMovTab_TipoIntervencion)
                                              group by ci.IdEstado) as consulta1
                                              on consulta1.Id = me.IdEstado where me.EstadoSup=11 and me.IdEstado !=12 and me.IdEstado!=15
                                              ");

                  while ($retornocom3mon2 = pg_fetch_object($query3mon2)){
                                    $varidestado=$retornocom3mon2->idestado;
                                    $varcontadorestadomon2=$retornocom3mon2->cantidad;

                                    $lista.="<td class='tabla-col'>
                                    <a href='#' data-toggle='modal' data-target='#mod-success2' onclick='showModalMonAv($varidusuario,$varidestado,1)'><strong>$varcontadorestadomon2</strong></a>
                                    
                                    </td>";

                                }


               $lista.="
                  <td class='tabla-col'><a href='#' data-toggle='modal' data-target='#mod-success2' onclick='showModalMonAv($varidusuario,15,2)'><strong>$varfinalizadasdia</strong>
                      </a> -
                      <a href='#' data-toggle='modal' data-target='#mod-success' onclick='showModalReportFin(15,$varidusuario,2)'><strong>A<strong></a>
                  <td class='tabla-col'><a href='#' data-toggle='modal' data-target='#mod-success2' onclick='showModalMonAv($varidusuario,15,3)'><strong>$varfinalizadasmes</strong>
                      </a> -
                      <a href='#' data-toggle='modal' data-target='#mod-success' onclick='showModalReportFin(15,$varidusuario,3)'><strong>A<strong></a>
                  <td class='tabla-col'><strong>$varpendientes</strong></td>                    
                  </tr>";
                  
                }

                  
              $lista.="</tbody>
              <tfoot >
              <tr>
              <td class='tabla-foot' colspan='2'><strong>Total</strong></td>
              ";


                      $totaltecgenf=array_sum($totaltecgen);
                      $totaltecgenpexf=array_sum($totaltecgenpex);
                      $totaltecfindiaf=array_sum($totaltecfindia);
                      $totaltecfinmesf=array_sum($totaltecfinmes);
                      $totaltecpenf=array_sum($totaltecpen);
                      

              $lista.="
              <td class='tabla-foot'><strong>$totaltecgenf</strong></td>
                        <td class='tabla-foot'><strong>$totaltecgenpexf</strong></td>
                           <td class='tabla-foot'>
                              <div class='progress progress-striped active'>
                                  <div class='progress-bar progress-bar-$color2' style='width: '>%</div>
                              </div>
                           </td>
                           <td class='tabla-foot'></td>
                           <td class='tabla-foot'></td>
                           <td class='tabla-foot'></td>
                           <td class='tabla-foot'><strong>$totaltecfindiaf</strong></td>
                           <td class='tabla-foot'><strong>$totaltecfinmesf</strong></td>
                           <td class='tabla-foot'><strong>$totaltecpenf</strong></td>
              </tr>
              </tfoot>


              </table>
              </div>


";




$querymon3 = pg_query("
select 
Cuadrilla,
SubCuadrilla,
IdConcesionaria,
setfinal.Sector,
TotalAsignadas,
TotalDescargadas,
TotalExportadas,
TotalFinalizadas from (
select IdSector,Sector,
SUM(TotalAsignadas) as TotalAsignadas,
SUM(TotalDescargadas) as TotalDescargadas,
SUM(TotalExportadas) as TotalExportadas,
SUM(TotalFinalizadas) as TotalFinalizadas from (
select  
co.IdSector as IdSector,
co.Sector as Sector,
(case when ci.IdEstado = 13 and co.Condicion='0' and co.SubActividad 
in (select DetalleTipoIntervencion from ComMovTab_TipoIntervencion)
then 1 else 0 end) as TotalDescargadas,
(case when ci.IdEstado = 5 and co.Condicion='0' and co.SubActividad 
in (select DetalleTipoIntervencion from ComMovTab_TipoIntervencion)
then 1 else 0 end) as TotalAsignadas,
(case when ci.IdEstado = 14 and co.Condicion='0' and co.SubActividad 
in (select DetalleTipoIntervencion from ComMovTab_TipoIntervencion)
then 1 else 0 end) as TotalExportadas,
(case when ci.IdEstado = 15 and co.Condicion='0' and co.SubActividad 
in (select DetalleTipoIntervencion from ComMovTab_TipoIntervencion)
then 1 else 0 end) as TotalFinalizadas
from ComMovTab_Orden co 
inner join ComMovTab_Intervencion ci on co.IdOrden = ci.IdOrden
where co.Condicion='0' and co.SubActividad 
in (select DetalleTipoIntervencion from ComMovTab_TipoIntervencion)) as superconsulta
group by IdSector,Sector) as setfinal
inner join (
select
gu.IdUbicacionConc as IdConcesionaria,
case when (select IdConceptosUbicacion from GlobalMaster_Ubicacion where IdUbicacion =dr2.IdUbicacionSup)=8
or (select IdConceptosUbicacion from GlobalMaster_Ubicacion where IdUbicacion =dr2.IdUbicacionSup)=7  
then gu3.NomUbicacion else gu2.NomUbicacion end as Cuadrilla,
gu2.NomUbicacion as SubCuadrilla,
gu.NomUbicacion as Sector
from GlobalMaster_DetalleUbicacionRegla dr
inner join GlobalMaster_DetalleUbicacionRegla dr2 on dr.IdUbicacionSup = dr2.IdUbicacion
inner join GlobalMaster_Ubicacion gu on dr.IdUbicacion = gu.IdUbicacion
inner join GlobalMaster_Ubicacion gu2 on dr.IdUbicacionSup = gu2.IdUbicacion
inner join GlobalMaster_Ubicacion gu3 on dr2.IdUbicacionSup = gu3.IdUbicacion
where gu.IdConceptosUbicacion = 5 ) as setfinal2 on
setfinal.IdSector=cast(setfinal2.IdConcesionaria as integer)
order by Cuadrilla
");



  

$lista.="
           <div class='table-responsive'>
              <h3 class='subtitulo' > Perspectiva Ubicacion</h3>
              <table class='table table-bordered' id='datatablepers3c' >
              <thead class='primary-emphasis'>
              <tr class='tabla-titulo'>
                  <th class='tabla-campo'>Cuadrilla</th>
                  <th class='tabla-campo'>SubCuadrilla</th>
                  <th class='tabla-campo'>Codigo</th>
                  <th class='tabla-campo'>Sector</th>
                  <th class='tabla-campo'>Asignadas</th>
                  <th class='tabla-campo'>Descargadas</th>
                  <th class='tabla-campo'>Exportadas</th>
                  <th class='tabla-campo'>Finalizadas</th>
                  <th class='tabla-campo'>% Avance</th>
                  <th class='tabla-campo'>Total Pendientes</th>
              </tr>
              </thead>
               <tbody>";
                while ($retornocom3 = pg_fetch_object($querymon3)){
                                    $varcuadrilla=$retornocom3->cuadrilla;
                                    $varsubcuadrilla=$retornocom3->subcuadrilla;
                                    $varidsector=$retornocom3->idconcesionaria;
                                    $varsector=$retornocom3->sector;
                                    $varcuaasignada=$retornocom3->totalasignadas;
                                    $varcuadescargada=$retornocom3->totaldescargadas;
                                    $varcuaexportada=$retornocom3->totalexportadas;
                                    $varcuafinalizada=$retornocom3->totalfinalizadas;
                                    $varcuapendientes=$varcuaasignada+$varcuadescargada+$varcuaexportada;

                                    if ($varcuafinalizada==0){
                                      $varcuaporcentaje=0;
                                    }
                                    else {
                                      $varcuaporcentaje=($varcuafinalizada*100)/($varcuapendientes+$varcuafinalizada);
                                      $varcuaporcentaje=round($varcuaporcentaje,2);
                                    }
                                    


                                    $totalcuaasigv[]=$varcuaasignada;
                                    $totalcuadesv[]=$varcuadescargada;
                                    $totalcuaexpov[]=$varcuaexportada;
                                    $totalcuafinv[]=$varcuafinalizada;

                                    $totalcuapenv[]=$varcuapendientes;

                                    if ($varcuaporcentaje < 33) {
                                      $color3='danger';
                                      
                                    } elseif ($varcuaporcentaje > 33 && $varcuaporcentaje < 70) {
                                      $color3='warning';
                                    } else {
                                      $color3='success';
                                    }

                             

                $lista.= "
                <tr class='odd gradeX'>
                  <td class='tabla-col'><strong>$varcuadrilla</strong></td>
                  <td class='tabla-col'><strong>$varsubcuadrilla</strong></td>
                  <td class='tabla-col'><strong>$varidsector</strong></td>
                  <td class='tabla-col'><strong>$varsector</strong></td>
                  <td class='tabla-col' style='color:red;'><strong>$varcuaasignada</strong></td>
                  <td class='tabla-col' style='color:red;'><strong>$varcuadescargada</strong></td>
                  <td class='tabla-col' style='color:red;'><strong>$varcuaexportada</strong></td>
                  <td class='tabla-col' style='color:blue;'><strong>$varcuafinalizada</strong></td>
                  <td class='tabla-col' class='center'><div class='progress progress-striped active'>
                                          <div class='progress-bar progress-bar-$color3' style='width: $varcuaporcentaje%'>".number_format($varcuaporcentaje,2)."%</div>
                                         </div></td>
                 
                  <td class='tabla-col' style='color:red;'><strong>$varcuapendientes</strong></td>

                </tr>";

                } 

                 $lista.= "</tbody>


              <tfoot >
                <tr>";

                $totalcuaasig=array_sum($totalcuaasigv);
                $totalcuades=array_sum($totalcuadesv);
                $totalcuaexp=array_sum($totalcuaexpov);
                $totalcuafin=array_sum($totalcuafinv);
                $totalcuapen=array_sum($totalcuapenv);

                  $lista.= "<td class='tabla-foot' colspan='4'><strong>Total</strong></td>
                  <td class='tabla-foot'><strong>$totalcuaasig</strong></td>
                  <td class='tabla-foot'><strong>$totalcuades</strong></td>
                  <td class='tabla-foot'><strong>$totalcuaexp</strong></td>
                  <td class='tabla-foot'><strong>$totalcuafin</strong></td>
                  <td class='tabla-foot'><strong></strong></td>
                  <td class='tabla-foot'><strong>$totalcuapen</strong></td>
                </tr>
              </tfoot>


              </table>
      </div>


";






//echo $lista;exit;



App::import('Vendor', 'dompdf', true, array(), 'dompdf' . DS . 'dompdf_config.inc.php');
$dompdf = new DOMPDF();
$dompdf->load_html($lista);
$dompdf->set_paper("A4", "portrait");
$dompdf->render();
                  //$dompdf->stream('ReportExample_'.date('d_M_Y').'.pdf');
                  //$dompdf->stream("xx.pdf", array("Attachment" => 0));

$output = $dompdf->output();
$nombre = "reporte-".date('Y-m-d').".pdf";
$file_to_save = APP.'tmp/dompdf/'.$nombre;
file_put_contents($file_to_save, $output);

$email = new CakeEmail('gmail');
	
if(isset($_POST['arr_emails'])){
	$email->to($_POST['arr_emails']);
}else{
	//$email->to(array('geynen@gmail.com'));
	$email->to(array('fzapatar@distriluz.com.pe'));
}
//$email->to(array('geynen@gmail.com','vasquezader@gmail.com'));
//$email->to(array('yyaipenq@distriluz.com.pe','wlopezg@distriluz.com.pe','vinonanp@distriluz.com.pe'));
//$email->cc(array('walterneyra@pexport.com.pe','jenyminchola@pexport.com.pe'));
//$email->bcc(array('geynen@gmail.com','vasquezader@gmail.com'));
$email->subject('RESUMEN DEL DIA - ACTIVIDADES COMERCIALES ENSA');
//$this->Email->replyTo = 'support@ejemplo.com';
//$email->from('ddanalytics05@gmail.com');
$email->template('sumary_by_day','default'); // NOTAR QUE NO HAY '.ctp'
$email->emailFormat('html');
//Variables de la vista
//$this->set('User', $User);
	
$email->attachments(
		array(
				'reporte-'.Date('Y-m-d').'.pdf' =>  array(
						'file' => APP.'tmp/dompdf/reporte-'.Date('Y-m-d').'.pdf',
						'mimetype' => 'application/pdf'
				)
		));

if(!$email->send()) {
	echo '{"success": false, "msg": "El reporte no ha podido ser entregado.", "msg_error": "Mailer Error: '.$mail->ErrorInfo.'"}';
} else {
	echo '{"success": true, "msg": "El reporte a sido enviado.", "msg_error": ""}';
}
?>