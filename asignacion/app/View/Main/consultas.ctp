<!DOCTYPE html>
<html lang="es" >
  <head>

    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="<?php echo FULL_URL;?>images/favicon.png">

    <title>Pexport Asignaciones</title>
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,400italic,700,800' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Raleway:100' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Open+Sans+Condensed:300,700' rel='stylesheet' type='text/css'>
  

    <!-- Bootstrap core CSS -->
    <link href="<?php echo FULL_URL;?>js/bootstrap/dist/css/bootstrap.css" rel="stylesheet" />
    <link rel="stylesheet" href="<?php echo FULL_URL;?>fonts/font-awesome-4/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo FULL_URL;?>js/jquery.magnific-popup/dist/magnific-popup.css" />
    
    <style type="text/css" title="currentStyle">
      thead input { width: 100% }
      input.search_init { color: #999 }
      p { text-align: left; }
    
		.styled-select {
   background: url(http://i62.tinypic.com/15xvbd5.png) no-repeat 96% 0;
   height: 29px;
   overflow: hidden;
   width: 240px;
}

.styled-select select {
   background: transparent;
   border: none;
   font-size: 14px;
   height: 29px;
   padding: 5px; /* If you add too much padding here, the options won't show in IE */
   width: 268px;
}


.semi-square {
   -webkit-border-radius: 5px;
   -moz-border-radius: 5px;
   border-radius: 5px;
}

/* -------------------- Colors: Background */
.yellow  { background-color: #eec111; }


/* -------------------- Colors: Text */
.yellow select  { color: #000; }
      
    </style>

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!--[if lt IE 9]>
	  <script src="../../assets/js/html5shiv.js"></script>
	  <script src="../../assets/js/respond.min.js"></script>
	<![endif]-->

	<!-- Custom styles for this template -->
	<link href="<?php echo FULL_URL;?>css/style.css" rel="stylesheet" />	
	<style type="text/css">
	#background {
    width: 100%; 
    height: 100%; 
    position: fixed; 
    left: 0px; 
    top: 0px; 
    z-index: -1; /* Ensure div tag stays behind content; -999 might work, too. */
	}

	.stretch {
	    width:100%;
	    height:100%;
	}
	</style>
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="../../assets/js/html5shiv.js"></script>
      <script src="../../assets/js/respond.min.js"></script>
    <![endif]-->
    <link rel="stylesheet" type="text/css" href="<?php echo FULL_URL;?>js/jquery.gritter/css/jquery.gritter.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo FULL_URL;?>/js/jquery.nanoscroller/nanoscroller.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo FULL_URL;?>js/jquery.easypiechart/jquery.easy-pie-chart.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo FULL_URL;?>js/bootstrap.switch/bootstrap-switch.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo FULL_URL;?>js/bootstrap.datetimepicker/css/bootstrap-datetimepicker.min.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo FULL_URL;?>js/jquery.select2/select2.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo FULL_URL;?>js/bootstrap.slider/css/slider.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo FULL_URL;?>js/dropzone/css/dropzone.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo FULL_URL;?>js/jquery.niftymodals/css/component.css" />
    <!-- Custom styles for this template -->
    <link href="<?php echo FULL_URL;?>css/style.css" rel="stylesheet" />


  <script type="text/javascript">
 /*var RecaptchaOptions = {
    theme : 'red',
    lang : 'es',
 };*/
 </script>

</head>


<body class="texture">
<?php $_SESSION['intentos'] = 0;?>
<div id="cl-wrapper" class="error-container">
	<div class="page-error">
		<h2 class="description text-center">Modulo de Consulta!</h2>

		<div id="background">
		    <img src="<?php echo FULL_URL;?>		images/ENSA.jpg" class="stretch" alt=""  />


		    

		</div>

		<div id="" style="margin-left:8%; width:70%;z-index:10000;">
		

		<div class="col-sm-6 col-md-6" style="margin-top:1%;">
        <div class="block-flat">
          <div class="header">							
            <h3 style="color:black;">Formulario de Consulta</h3>
          </div>
          <div class="content">

          <form role="form" id="consultaorden"> 

          	<p>Ingresa aquí el código que le proporciono el Técnico en el ticket de atención a la hora 
          		de finalizar la visita en su domicilio, se mostrar a continuación si existe ese registro, 
          		y el estado en el que se encuentra.</p>
          	<div class="form-group">
              
              <div class="input-group">
			  <div class="styled-select  yellow semi-square">
					<select id='patron' name ='patron'>
					  <option value="NroAtencion">Numero de Atencion</option>
					  <option value="Suministro">Suministro</option>
					</select><br><br>
				</div>
                    <input type="text" id="ida" name="ida" placeholder="Digite aqui..." class="form-control" style="width: 241PX; margin-top: 3PX;">
                    <?php
                      //require_once('recaptchalib.php');
                      //$publickey = "6Lft6PYSAAAAAKPY5l2CrYsPUORysDdkWNGdz6KY"; // you got this from the signup page
                      //echo recaptcha_get_html($publickey);
                    ?>
                    <span class="input-group-btn">
                    <button class="btn btn-primary" type="button" onclick="resultado()">Buscar!</button>
                    </span>
              </div>
              </div>
           </form>

           <span class="ciclo_loading" style="display:none;"><img src="<?php echo FULL_URL;?>js/jquery.select2/select2-spinner.gif"></span>

           <div id="resultado">
           					 


							 <!--
							 <div class="overflow-hidden">
								<i class="fa fa-rocket fa-4x pull-left color-danger"></i> 
								<h3 class="no-margin">Ir</h3>
								<p class="color-danger">Ver detalle</p>
							</div>
							-->

           </div>
          
          </div>
        </div>
        </div>




     

		<div class="text-center copy">&copy; 2014 <a href="#">PEXPORT SAC</a> Empresa Colaboradora de ELECTRONORTE S.A</div>
	</div>
	

	
</div>
</div>
  <!-- <meta http-equiv="refresh" content="2;url=main.php">-->
  <script type="text/javascript" src="<?php echo FULL_URL;?>js/jquery11.js"></script>
  <script type="text/javascript" src="<?php echo FULL_URL;?>js/jquery.gritter/js/jquery.gritter.min.js"></script>
  <script type="text/javascript" src="<?php echo FULL_URL;?>js/bootstrap-fileupload.js"></script>
  <script type="text/javascript" src="<?php echo FULL_URL;?>js/jquery.nanoscroller/jquery.nanoscroller.js"></script>
  
  <script type="text/javascript" src="<?php echo FULL_URL;?>js/behaviour/general.js"></script>
  <script type="text/javascript" src="<?php echo FULL_URL;?>js/jquery.ui/jquery-ui.js" type="text/javascript"></script>
  <script type="text/javascript" src="<?php echo FULL_URL;?>js/jquery.sparkline/jquery.sparkline.min.js"></script>
  <script type="text/javascript" src="<?php echo FULL_URL;?>js/jquery.easypiechart/jquery.easy-pie-chart.js"></script>
  <script type="text/javascript" src="<?php echo FULL_URL;?>js/jquery.nestable/jquery.nestable.js"></script>
  <script type="text/javascript" src="<?php echo FULL_URL;?>js/bootstrap.switch/bootstrap-switch.min.js"></script>
  <script type="text/javascript" src="<?php echo FULL_URL;?>js/bootstrap.datetimepicker/js/bootstrap-datetimepicker.min.js"></script>
  <script src="<?php echo FULL_URL;?>js/jquery.select2/select2.min.js" type="text/javascript"></script>
  <script src="<?php echo FULL_URL;?>js/skycons/skycons.js" type="text/javascript"></script>
  <script src="<?php echo FULL_URL;?>js/bootstrap.slider/js/bootstrap-slider.js" type="text/javascript"></script>
  <script src="https://maps.googleapis.com/maps/api/js?v=3.exp&amp;sensor=false"></script>
  <script type="text/javascript" src="<?php echo FULL_URL;?>js/jquery.datatables/jquery.datatables.min.js"></script>
  <script type="text/javascript" src="<?php echo FULL_URL;?>js/jquery.datatables/bootstrap-adapter/js/datatables.js"></script>

  <script type="text/javascript" src="<?php echo FULL_URL;?>js/dropzone/dropzone.js"></script>
  <script type="text/javascript" src="<?php echo FULL_URL;?>js/FileSaver.js/FileSaver.js"></script>
  <!--<script type="text/javascript" src="libs/Blob.js/Blob.js"></script>
  <!--<script type="text/javascript" src="libs/BlobBuilder.js/BlobBuilder.js"></script>

  <script type="text/javascript" src="libs/Deflate/deflate.js"></script>
  <script type="text/javascript" src="libs/Deflate/adler32cs.js"></script>-->
 
 
  <script type="text/javascript" src="<?php echo FULL_URL;?>js/jquery.niftymodals/js/jquery.modalEffects.js"></script>   

  <!-- Code editor -->

  <script type="text/javascript" src="<?php echo FULL_URL;?>js/jquery.magnific-popup/dist/jquery.magnific-popup.min.js"></script>

  <script type="text/javascript">
    function resultado(){
    $('.ciclo_loading').show();
//    var r_challenge = document.getElementById("recaptcha_challenge_field").value;
//    var r_response = document.getElementById("recaptcha_response_field").value;
//		r_response = r_response.replace(" ","%20");
	var patron = document.getElementById("patron").value;
    var id = document.getElementById("ida").value;
    //$('#resultado').load('../Asignacion/controler/controler-consulta-orden-pg.php?ida='+id+'&patron='+patron+'&r_challenge='+r_challenge+'&r_response='+r_response,function(){ 
    $('#resultado').load('<?php echo FULL_URL;?>main/consulta_orden?ida='+id+'&patron='+patron,function(){
    $('.ciclo_loading').hide(); 
    //Recaptcha.reload();     
    });

    }
  </script>


<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script type="text/javascript" src="<?php echo FULL_URL;?>js/jquery.nestable/jquery.nestable.js"></script>
<script src="<?php echo FULL_URL;?>js/behaviour/voice-commands.js"></script>
<script src="<?php echo FULL_URL;?>js/bootstrap/dist/js/bootstrap.min.js"></script>
<script type="text/javascript" src="<?php echo FULL_URL;?>js/jquery.flot/jquery.flot.js"></script>
<script type="text/javascript" src="<?php echo FULL_URL;?>js/jquery.flot/jquery.flot.pie.js"></script>
<script type="text/javascript" src="<?php echo FULL_URL;?>js/jquery.flot/jquery.flot.labels.js"></script>
<script type="text/javascript" src="<?php echo FULL_URL;?>js/jquery.flot/jquery.flot.resize.js"></script>
<?php echo $this->element('ga');?>
</body>
</html>