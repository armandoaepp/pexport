<?php

date_default_timezone_set('America/Lima');
//require "../conexion.php";
require_once(APP."View/Main/conexion.ctp");

$querycomb1 = pg_query("select * from GlobalMaster_ReglaJerarquiaUbicacion");
$querycomb2 = pg_query("select * from GlobalMaster_ConceptosUbicacion");
$querycomb3 = pg_query("select * from GlobalMaster_ConceptosUbicacion");


$query = pg_query("select 
GMDRJU.IdDetalleReglaJerarquiaUbi as IdDetalleReglaJerarquiaUbi,
GMRJU.IdReglaJerarquiaUbicacion as IdReglaJerarquiaUbicacion,
GMRJU.NombreReglaJerarquiaUbicacion as NombreReglaJerarquiaUbicacion,
GMCU.IdConceptosUbicacion as IdConceptosUbicacion,
GMCU.NomConceptosUbicacion as NomConceptosUbicacion,
GMCUS.IdConceptosUbicacion as IdConceptosUbicacionSup,
GMCUS.NomConceptosUbicacion as NomConceptosUbicacionSup
from GlobalMaster_DetalleReglaJerarquiaUbi GMDRJU
inner join GlobalMaster_ReglaJerarquiaUbicacion GMRJU on GMDRJU.IdReglaJerarquiaUbicacion=GMRJU.IdReglaJerarquiaUbicacion
inner join GlobalMaster_ConceptosUbicacion GMCU on GMDRJU.IdConceptosUbicacion = GMCU.IdConceptosUbicacion
inner join GlobalMaster_ConceptosUbicacion GMCUS on GMDRJU.IdConceptosUbicacionSup = GMCUS.IdConceptosUbicacion");


$lista="
	
	

    <div class='row'>
      <div class='col-sm-12 col-md-12'>
        <div class='block-flat'>


        <div class='panel-group accordion accordion-semi' id='accordion4'>
					  <div class='panel panel-default'>
						<div class='panel-heading success'>
						  <h4 class='panel-title'>
							<a data-toggle='collapse' data-parent='#accordion4' href='#ac4-1'>
								<i class='fa fa-angle-right'></i> Agregar Nueva Composicion de Regla
							</a>
						  </h4>
						</div>
						<div id='ac4-1' class='panel-collapse collapse in'>
						  <div class='panel-body'>
							
						          <div class='content'>

							          <form role='form' id='formdetallereglajerarquiaubi' method='post' style=width:30%;> 
							            <div class='form-group'>
							              " ;

											 $lista.= " <label>Selecciona la Regla : </label> <select class='form-control' id='idreglacruddetallereglaubi' name='idreglacruddetallereglaubi'>";
														 while ($retorno = pg_fetch_object($querycomb1)){
															$lista.= "<option value='$retorno->idreglajerarquiaubicacion'>$retorno->nombrereglajerarquiaubicacion</option>";
														}
											 $lista.= "</select>
											 </div>";


											 $lista.= "<div class='form-group'>
											 <label>Selecciona Tipo de Ubicacion : </label> <select class='form-control' id='idconceptosubicruddetalleregla' name='idconceptosubicruddetalleregla'>";
														 while ($retorno = pg_fetch_object($querycomb2)){
															$lista.= "<option value='$retorno->idconceptosubicacion'>$retorno->nomconceptosubicacion</option>";
														}
											 $lista.= "</select></div>";

											 $lista.= "<div class='form-group'>
											 <label>Selecciona a quien pertenece : </label> <select class='form-control' id='idconcetosubisupcruddetalleregla' name='idconcetosubisupcruddetalleregla'>";
														 while ($retorno = pg_fetch_object($querycomb3)){
															$lista.= "<option value='$retorno->idconceptosubicacion'>$retorno->nomconceptosubicacion</option>";
														}
											 $lista.= "</select></div>"


											 ;				 
												          $lista.="

							            </div>
							              <button class='btn btn-danger' type='button' onclick='crudmaestrosdetallereglajerarquiaubi(1)'>Grabar</button>
							              <button class='btn btn-default' type='button'>Cancel</button>
							          </form>
						          
						          </div>

						  </div>
						</div>
					  </div>
		</div>


		

						      <div class='row'>
						        <div class='col-md-12'>
						          <div class='block-flat'>

						            

						            <div class='content'>
						            <div class='tabla'>

						          
						            <div class=''>

						              <div class='table-responsive'>
						              <form role='form' id='formdetallereglajerarquiaubicrud' method='post'> 
						                <table class='no-border' id='datatablecrudmaestrosdetallereglajerarquiaubi'>

						                  <thead class='primary-emphasis'>
						                    <tr>
						                      <th></th>
						                      <th>Codigo</th>
						                      <th>Regla</th>
						                      <th>Nombre Ubicacion</th>
						                      <th>Pertenece a</th>
						                    </tr>

						                  </thead>
						                  <tbody class='no-border'>";


						                    while ($retorno = pg_fetch_object($query)){

						                       $var=utf8_decode($retorno->iddetallereglajerarquiaubi);
						                       $var11=utf8_decode($retorno->idreglajerarquiaubicacion);
						                       $var1=utf8_decode($retorno->nombrereglajerarquiaubicacion);
						                       $var21=utf8_decode($retorno->idconceptosubicacion);
						                       $var2=utf8_decode($retorno->nomconceptosubicacion);
						                       $var31=utf8_decode($retorno->idconceptosubicacionsup);
						                       $var3=utf8_decode($retorno->nomconceptosubicacionsup);

						                       $lista.= "<tr class='odd gradeX'>
						                                  <input type ='hidden' id='iddetallereglacruddetallereglaubi[]' name='iddetallereglacruddetallereglaubi[]' value='$var'>
						                                  <td style='width:1%;'><input type ='checkbox' id='idchkdetallereglacruddetallereglaubi[]' name='idchkdetallereglacruddetallereglaubi[]' value='$var' ></td>
						                                  <td style='width:1%;'>$var</td>


						                                  <td style='width:10%;'><select class='form-control' id='nombrereglajerarquiaubicacionubicrud[]' name='nombrereglajerarquiaubicacionubicrud[]' value='$var1' >
						                                      <option value='$var11'>$var1</option>
						                                      ";
						                                  $querydet1 = pg_query("select * from GlobalMaster_ReglaJerarquiaUbicacion GMRJU where GMRJU.IdReglaJerarquiaUbicacion!=$var11");
						                                   while ($retornodet = pg_fetch_object($querydet1)){
						                                    $idrju=$retornodet->idreglajerarquiaubicacion;
						                                    $nomrju=$retornodet->nombrereglajerarquiaubicacion;

						                                    $lista.="<option value='$idrju'>$nomrju</option>"; 
						                                   } 
						                                  $lista.="</select></td>


						                           

						                                  <td style='width:10%;'><select class='form-control' id='nombreconceptoubicaciondetalleregubicrud[]' name='nombreconceptoubicaciondetalleregubicrud[]' value='$var1' >
						                                      <option value='$var21'>$var2</option>
						                                      ";
						                                  $querydet2 = pg_query("select * from GlobalMaster_ConceptosUbicacion GMCU where GMCU.IdConceptosUbicacion!=$var21");
						                                   while ($retornodet = pg_fetch_object($querydet2)){
						                                    $idcu=$retornodet->idconceptosubicacion;
						                                    $nomcu=$retornodet->nomconceptosubicacion;

						                                    $lista.="<option value='$idcu'>$nomcu</option>"; 
						                                   } 
						                                  $lista.="</select></td>



						                                  <td style='width:10%;'><select class='form-control' id='nombreconceptoubicacionsupdetalleregubicrud[]' name='nombreconceptoubicacionsupdetalleregubicrud[]' value='$var1' >
						                                      <option value='$var31'>$var3</option>
						                                      ";
						                                  $querydet3 = pg_query("select * from GlobalMaster_ConceptosUbicacion GMCU where GMCU.IdConceptosUbicacion!=$var31");
						                                   while ($retornodet = pg_fetch_object($querydet3)){
						                                    $idcus=$retornodet->idconceptosubicacion;
						                                    $nomcus=$retornodet->nomconceptosubicacion;

						                                    $lista.="<option value='$idcus'>$nomcus</option>"; 
						                                   } 
						                                  $lista.="</select></td>

						                                </tr>";
						                       }
						                  $lista.="</tbody>

						                </table>
						                <button class='btn btn-success' type='button' onclick='crudmaestrosdetallereglajerarquiaubi(2)'>Grabar</button>
						                <button class='btn btn-danger' type='button' onclick='crudmaestrosdetallereglajerarquiaubi(3)'>Eliminar</button>
						              </form>              
						              </div>

						              </div>
						              </div>
						              
						            </div>
						          </div>        
						        </div>

						      </div>

      

        </div>				
      </div>
      

	</div>";

echo $lista;

?>