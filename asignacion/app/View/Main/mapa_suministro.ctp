<?php
date_default_timezone_set('America/Lima');
//require "conexion.php";
require_once(APP."View/Main/conexion.ctp");

if(isset($_GET['suministro'])){
	$str_suministro = $_GET['suministro'];
}else{
	echo 'Url no valida';
	exit();
}
?>
<?php 
$str_rutas = '';
$i = 1;
$page = 0; 
	
$query_ultima_intervencion = pg_query("select *  from ComMovTab_Intervencion CI
inner join ComMovTab_Orden O on CI.IdOrden=O.IdOrden 
left join GlobalMaster_Gps GPS on O.Suministro=GPS.suministro 
where GPS.suministro = '".$str_suministro."' order by FechaAtencion desc limit 1");
  		
$rs2=pg_num_rows($query_ultima_intervencion);
if ($rs2 > 0) {
  	$obj_intervencion = pg_fetch_object($query_ultima_intervencion);
}else{
	echo 'No tenemos datos de la ubicaci&oacute;n del suministro.';
	exit();
}
  		
if($obj_intervencion->latitud1!='' && $obj_intervencion->longitud1!=''){
				
	$color_flag = '72BAF7';
	

	$latitud_center = $obj_intervencion->latitud1;
	$longitud_center = $obj_intervencion->longitud1;
	
	
	
	$str_rutas .="
		  var contentString = '<div id=\"content\"><div id=\"siteNotice\"></div><h1 id=\"firstHeading\" class=\"firstHeading\"></h1><div id=\"bodyContent\"><p>' +
		  		'Suministro: <strong>".$obj_intervencion->suministro."</strong><br>'+
				'Cliente: <strong>".$obj_intervencion->nombresuministro."</strong><br>'+
				'Direcci&oacute;n: <strong>".$obj_intervencion->direccionsuministro."</strong><br>'+
		  		'</p></div></div>';
		  var infowindow".($i+$page)." = new google.maps.InfoWindow({
		      content: contentString
		  });
		  var marker".($i+$page)." = new google.maps.Marker({
		    position: new google.maps.LatLng(".$obj_intervencion->latitud1.", ".$obj_intervencion->longitud1."),
		    title: '# ".($i+$page)."',
		    map: map,
		    icon: '".FULL_URL."images/markers/townhouse.png',
		    size: 'tiny'
		  });
		     
		  google.maps.event.addListener(marker".($i+$page).", 'click', function() {
			  infowindow".($i+$page).".open(map,marker".($i+$page).");
			});
		  ";
}else{
	echo 'No tenemos datos de la ubicaci&oacute;n del suministro.';
	exit();
}
?>

<!DOCTYPE html>
<html lang="es">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="images/favicon.png">

    <title>ENSA</title>
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,400italic,700,800' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Raleway:100' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Open+Sans+Condensed:300,700' rel='stylesheet' type='text/css'>
  

    <!-- Bootstrap core CSS -->
    <link href="<?php echo FULL_URL;?>js/bootstrap/dist/css/bootstrap.css" rel="stylesheet" />
    <link rel="stylesheet" href="<?php echo FULL_URL;?>fonts/font-awesome-4/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo FULL_URL;?>js/jquery.magnific-popup/dist/magnific-popup.css" />
    
    <style type="text/css" title="currentStyle">
      thead input { width: 100% }
      input.search_init { color: #999 }
      p { text-align: left; }


      
    </style>


    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="../../assets/js/html5shiv.js"></script>
      <script src="../../assets/js/respond.min.js"></script>
    <![endif]-->
    <link rel="stylesheet" type="text/css" href="<?php echo FULL_URL;?>js/jquery.gritter/css/jquery.gritter.css" />
    <script type="text/javascript" src="<?php echo FULL_URL;?>js/jquery.nestable/jquery.nestable.js"></script>
    <link rel="stylesheet" type="text/css" href="<?php echo FULL_URL;?>js/jquery.nanoscroller/nanoscroller.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo FULL_URL;?>js/jquery.easypiechart/jquery.easy-pie-chart.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo FULL_URL;?>js/bootstrap.switch/bootstrap-switch.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo FULL_URL;?>js/bootstrap.datetimepicker/css/bootstrap-datetimepicker.min.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo FULL_URL;?>js/jquery.select2/select2.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo FULL_URL;?>js/bootstrap.slider/css/slider.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo FULL_URL;?>js/dropzone/css/dropzone.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo FULL_URL;?>js/jquery.niftymodals/css/component.css" />
    <!-- Custom styles for this template -->
    <link href="<?php echo FULL_URL;?>css/style.css" rel="stylesheet" />

</head>
<body>
  
<!-- INICIO CODIGO MAPA -->
  
<!DOCTYPE html>
<html>
  <head>
    <meta name="viewport" content="initial-scale=1.0, user-scalable=no">
    <meta charset="utf-8">
    <title>Monitoreo de T&eacute;cnicos</title>
    <style>
      html, body, #map-canvas {
        height: 100%;
        margin: 0px;
        padding: 0px
      }
    </style>
    <script src="https://maps.googleapis.com/maps/api/js?v=3.exp"></script>
    <script src="<?php echo FULL_URL;?>js/google-maps-label.js"></script>
  </head>
  <body>

    <div id="arbol" class="page-aside app filters" style="display:none">
        <div>
        
						<div style="display: none;">
						  <div id="legend" style="text-align: right;">
						  
			              
						  </div>
					    </div>
		  
      </div>
    </div>

    <div id="map-canvas"></div>
  </body>
</html>

<script type="text/javascript">
//function initialize() {
  var myLatlng = new google.maps.LatLng(<?php echo isset($latitud_center)?$latitud_center:'-6.77361';?>, <?php echo isset($longitud_center)?$longitud_center:'-79.8417';?>);
  var mapOptions = {
    zoom: 17,
    center: myLatlng
  }
  var map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);

	<?php echo $str_rutas;?>

//}

//google.maps.event.addDomListener(window, 'load', initialize);

function openInfo(i){
	$('.item_lecturista').each(function(){
		$k = $(this).data('item');
		if(i==$k){     						
	    	eval('infowindow'+$k+'.open(map,marker'+$k+');');
		}else{
			eval('infowindow'+$k+'.close();');
		}
	});
}

map.controls[google.maps.ControlPosition.RIGHT_BOTTOM].push(document.getElementById('legend'));

</script>
  
  <!-- FIN CODIGO MAPA -->
  
  


	<script src="<?php echo FULL_URL;?>js/jquery.js"></script>
	<script type="text/javascript" src="<?php echo FULL_URL;?>js/jquery.nanoscroller/jquery.nanoscroller.js"></script>
	<script type="text/javascript" src="<?php echo FULL_URL;?>js/jquery.sparkline/jquery.sparkline.min.js"></script>
	<script type="text/javascript" src="<?php echo FULL_URL;?>js/jquery.easypiechart/jquery.easy-pie-chart.js"></script>
	<script type="text/javascript" src="<?php echo FULL_URL;?>js/behaviour/general.js"></script>
  	<script src="<?php echo FULL_URL;?>js/jquery.ui/jquery-ui.js" type="text/javascript"></script>
	<script type="text/javascript" src="<?php echo FULL_URL;?>js/jquery.nestable/jquery.nestable.js"></script>
	<script type="text/javascript" src="<?php echo FULL_URL;?>js/bootstrap.switch/bootstrap-switch.min.js"></script>
	<script type="text/javascript" src="<?php echo FULL_URL;?>js/bootstrap.datetimepicker/js/bootstrap-datetimepicker.min.js"></script>
	<script src="<?php echo FULL_URL;?>js/jquery.select2/select2.min.js" type="text/javascript"></script>
	<script src="<?php echo FULL_URL;?>js/bootstrap.slider/js/bootstrap-slider.js" type="text/javascript"></script>
	<script type="text/javascript" src="<?php echo FULL_URL;?>js/jquery.gritter/js/jquery.gritter.js"></script>
   
  <script type="text/javascript">
    $(document).ready(function(){
      //initialize the javascript
      App.init();
    });
  </script>
  <script type='text/javascript' src='<?php echo FULL_URL;?>js/jquery.fullcalendar/fullcalendar/fullcalendar.js'></script>

<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
  <script src="<?php echo FULL_URL;?>js/behaviour/voice-commands.js"></script>
  <script src="<?php echo FULL_URL;?>js/bootstrap/dist/js/bootstrap.min.js"></script>
<script type="text/javascript" src="<?php echo FULL_URL;?>js/jquery.flot/jquery.flot.js"></script>
<script type="text/javascript" src="<?php echo FULL_URL;?>js/jquery.flot/jquery.flot.pie.js"></script>
<script type="text/javascript" src="<?php echo FULL_URL;?>js/jquery.flot/jquery.flot.resize.js"></script>
<script type="text/javascript" src="<?php echo FULL_URL;?>js/jquery.flot/jquery.flot.labels.js"></script>
</body>
</html>