<?php
date_default_timezone_set('America/Lima');

//require_once "../conexion.php";
require_once(APP."View/Main/conexion.ctp");

if(isset($_GET['tags'])) {
$vartags=$_GET['tags'];


$porciones = explode(",", $vartags);
$c=count($porciones);

$conocimiento=array("Asignado", "Exportado", "Descargado", "Finalizado");
$ori='';
$porcion='';
for ($i=0; $i < $c; $i++) { 
  if (in_array($porciones[$i], $conocimiento)) {
     $ori.=",".$porciones[$i];
     $porcion.=",'".$porciones[$i]."'";
  } else {
    echo "";
  }
}

$ori = substr($ori, 1);
$vartags = substr($porcion, 1);


if ($vartags==''){
  $vartags="'Asignado','Exportado','Descargado'";
  $origin="Asignado,Exportado,Descargado";
}else{
  /*
  $antes = array("Finalizado","Asignado","Exportado","Descargado");
  $despues   = array("'Finalizado'","'Asignado'","'Exportado'","'Descargado'");
  $vartags=str_replace($antes,$despues,$vartags);
  */
  $vartags=$vartags;
  $origin=$ori;
}



}else{
$vartags="'Asignado','Exportado','Descargado'";
$origin="Asignado,Exportado,Descargado";
}


if(isset($_GET['unidadneg'])) {
$unidad=$_GET['unidadneg'];


  if ($unidad == 0){
    $unidad="26,30,29";
    $origin2a="0";
    $origin2b="Todos";
  }else{
    $origin2a=$unidad;
    $sqlconsulta="select * from GlobalMaster_Ubicacion where trim(IdUbicacionConc) = $origin2a";
    $result3 = pg_query($sqlconsulta) or die ($sqlconsulta);
    $retorno = pg_fetch_object($result3);
    $origin2b=$retorno->nomubicacion;
  }
}else{
$unidad="26,30,29";
$origin2a="0";
$origin2b="Todos";
}


if(isset($_GET['centroservicio'])) {
$centroser=$_GET['centroservicio'];
  if ($centroser == 0){
    $centroser="258,272,255,273,270,271,251,268";
    $origin3a="0";
    $origin3b="Todos";
  }else{
    $centroser=$centroser;
    $origin3a=$centroser;
    $sqlconsultaa="select IdCentroServicio,CentroServicio from ComMovTab_Orden where IdCentroServicio = $origin3a group by IdCentroServicio,CentroServicio";
    $result3a = pg_query($sqlconsultaa) or die ($sqlconsultaa);
    $retornoa = pg_fetch_object($result3a);
    $origin3b=$retornoa->centroservicio;
  }
}else{
$centroser="258,272,255,273,270,271,251,268";
$origin3a="0";
$origin3b="Todos";
}





if(isset($_GET['vari'])) 
{
     $vari=$_GET["vari"];

}
else 
{
     $vari='';
}




$query = pg_query("select IdTipoIntervencion,DetalleTipoIntervencion from ComMovTab_TipoIntervencion where IdTipoIntervencionSup = 0 and IdTipoIntervencion !=4");

$arr_combos_negocio = pg_query("select * from GlobalMaster_Ubicacion where IdConceptosUbicacion = 3 and trim(IdUbicacionConc)!='$origin2a'");

$arr_combos_negociocentroservicio = pg_query("select IdCentroServicio,CentroServicio from ComMovTab_Orden where IdCentroServicio !=$origin3a
group by IdCentroServicio,CentroServicio");


$arr_tecnicos = pg_query("
        select cu.IdUsuario,NomPersona,ApePersona from ComMovTab_Intervencion ci
        inner join ComMovTab_Orden co on ci.IdOrden=co.IdOrden
        inner join ComMovTab_Usuario cu on ci.IdUsuario = cu.IdUsuario
        inner join GlobalMaster_Persona gp on cu.IdPersona = gp.IdPersona
        group by cu.IdUsuario,NomPersona,ApePersona
");

$meses = array (
    1 => "Enero",
    2 => "Febrero",
    3 => "Marzo",
    4 => "Abril",
    5 => "Mayo",
    6 => "Junio",
    7 => "Julio",
    8 => "Agosto",
    9 => "Septiembre",
    10 => "Octubre",
    11 => "Noviembre",
    12 => "Diciembre" 
);

$str_opciones_periodo_liquidacion = '';
for($j = date ( 'Y' ); $j >= 2014; $j --) {
  for($k = 12; $k >= 1; $k --) {
    if ($j == date ( 'Y' ) && $k <= date ( 'm' )) {
      $str_opciones_periodo_liquidacion .= '<option value="' . $j . '-' . $k . '" selected>' . $meses [$k] . ' ' . $j . '</option>';
    } elseif ($j < date ( 'Y' )) {
      $str_opciones_periodo_liquidacion .= '<option value="' . $j . '-' . $k . '">' . $meses [$k] . ' ' . $j . '</option>';
    }
  }
}
        
$lista="

              <div class='header'>
                <h2>Asignaciones</h2>
              </div>";

               $lista.="

              <div class='app-nav collapse'>             
                <div class='panel-group accordion' id='accordion' >
                  <div class='panel panel-default'>
                    <div class='panel-heading'>
                      <h4 class='panel-title'>
                        <a data-toggle='collapse' data-parent='#accordion' href='#collapseOne' class='filtro_accordion'>
                          <i class='fa fa-angle-right'></i> Filtro Especializado
                        </a>
                      </h4>
                    </div>
                    <div id='collapseOne' class='panel-collapse collapse in'>
                      <div class='panel-body'>
                        <form id='filtroarbolmon' method='POST'>

                          <div class='form-group' style=''>
                            <label class='control-label'>Selecciona Criterios:</label>
                            <input class='tags' id='criterios' name='criterios' type='hidden' />
                          </div>

                          <div class='porrangofecha' style='display:none;'>

                            <div class='form-group'>
                              <label class='control-label'>Por Fecha de Asignaci&oacute;n (Inicio) :</label>
                              <div class='input-group date datetime' data-min-view='2' data-date-format='yyyy-mm-dd'  style='width:45%;' >
                                <input id='dpt_date_start' name='dpt_date_start' class='form-control' size='16' type='text' value='".date('Y-m-d')."' readonly>
                                <span class='input-group-addon btn btn-primary'><span class='glyphicon glyphicon-th'></span></span>
                              </div>  
                            </div>

                            <div class='form-group'>
                              <label class='control-label'>Por Fecha de Asignaci&oacute;n (Fin) :</label>
                              <div class='input-group date datetime' data-min-view='2' data-date-format='yyyy-mm-dd'  style='width:45%;' >
                                <input id='dpt_date_end' name='dpt_date_end' class='form-control' size='16' type='text' value='".date('Y-m-d')."' readonly>
                                <span class='input-group-addon btn btn-primary'><span class='glyphicon glyphicon-th'></span></span>
                              </div>  
                            </div>

                          </div>
                          
                          
                          <div class='porrangofechaF' style='display:none;'>
     
                            <div class='form-group'>
                              <label class='control-label'>Por Fecha de Atenci&oacute;n (Inicio) :</label>
                              <div class='input-group date datetime' data-min-view='2' data-date-format='yyyy-mm-dd'  style='width:45%;'  >
                                <input id='dpt_date_start_finalizado' name='dpt_date_start_finalizado' class='form-control' size='16' type='text' value='".date('Y-m-d')."' readonly>
                                <span class='input-group-addon btn btn-primary'><span class='glyphicon glyphicon-th'></span></span>
                              </div>  
                            </div>
       
                            <div class='form-group'>
                              <label class='control-label'>Por Fecha de Atenci&oacute;n (Fin) :</label>
                              <div class='input-group date datetime' data-min-view='2' data-date-format='yyyy-mm-dd'  style='width:45%;' >
                                <input id='dpt_date_end_finalizado' name='dpt_date_end_finalizado' class='form-control' size='16' type='text' value='".date('Y-m-d')."' readonly>
                                <span class='input-group-addon btn btn-primary'><span class='glyphicon glyphicon-th'></span></span>
                              </div>  
                            </div>
     
                          </div>


                          <div class='porperiodoliquidacion' style='display:none;'>
                            <div class='form-group'><label>Periodo Liquidacion:</label> </br>
                              <select class='select2' id='periodoliquidacion' name='periodoliquidacion'>
                              <option value='0'>Todos</option>
                              ".$str_opciones_periodo_liquidacion."
                              </select>
                            </div>
                          </div>



                          <div class='porubicacion' style='display:none;'>
                            <div class='form-group' style=''>
                              <label class='control-label'>Por Unidad de Negocio:</label>
                              <select class='' name='unidadneg' id='unidadneg'>
                              <option value='$origin2a'>$origin2b</option>";
                              while ($negocio = pg_fetch_object($arr_combos_negocio)){
                                  $lista.= "<option value='$negocio->idubicacionconc'>$negocio->nomubicacion</option>";
                              }
                              $lista.= "<option value='0'>Todos</option></select>
                            </div>

                            <div class='form-group' style=''>
                            <label class='control-label'>Por Centro de Servicio:</label>
                            <select class='' name='centroservicio' id='centroservicio'><option value='$origin3a'>$origin3b</option>";

                            while ($negocio = pg_fetch_object($arr_combos_negociocentroservicio)){
                                $lista.= "<option value='$negocio->idcentroservicio'>".utf8_decode($negocio->centroservicio)."</option>";
                            }
                            $lista.= "<option value='0'>Todos</option></select>
                            </div>
                          </div>


                          <div class='porestado' style=''>
                            <div class='form-group' style=''>
                              <label class='control-label'>Estado Orden:</label>
                              <input class='tags' id='tags' name='tags' type='hidden' />
                            </div>
                          </div>
                              
                          <div class='porsuministro' style='display:none;'>
                            <div class='form-group' style=''>
                              <label class='control-label'>Suministro:</label>
                              <input class='form-control' id='txt_suministro' name='txt_suministro' type='text' placeholder='Ingrese Suministro' />
                            </div>
                          </div>
                              
                          <div class='porordentrabajo' style='display:none;'>
                            <div class='form-group' style=''>
                              <label class='control-label'>Orden de Trabajo:</label>
                              <input class='form-control' id='txt_orden_trabajo' name='txt_orden_trabajo' type='text' placeholder='Ingrese OT' />
                            </div>
                          </div>
                          
                          <div class='portecnico' style='display:none;'>
                            <div class='form-group' style=''>
                            <label class='control-label'>Por T&eacute;cnico:</label>
                            <select class='' name='tecnico' id='tecnico'>
                            <option value=''>Todos</option>";
                            while ($tecnico = pg_fetch_object($arr_tecnicos)){
                                $lista.= "<option value='$tecnico->idusuario'>$tecnico->nompersona $tecnico->apepersona</option>";
                            }
                            $lista.= "</select>
                            </div>
                          </div>
                                
                          <div class='portiempotranscurrido' style='display:none;'>
     
                            <div class='form-group'><label>Tiempo transcurrido mayor a:</label> </br>
                                      <select class='select2' id='cbo_tiempotranscurrido' name='cbo_tiempotranscurrido'>
                                      <option value='1'>1 d&iacute;a</option>
                                      <option value='2'>2 d&iacute;as</option>
                                      <option value='3'>3 d&iacute;as</option>
                                      <option value='4'>4 d&iacute;as</option>
                                      <option value='5'>5 d&iacute;as</option>
                                      <option value='10'>10 d&iacute;as</option>
                                      </select>
                            </div>
                          </div>

                          <button class='btn btn-success' type='button' onclick='showRamaArbol();if($(\".porsuministro\").css(\"display\")!=\"none\" || $(\".porordentrabajo\").css(\"display\")!=\"none\"){showMon(1)}else{showMon(2)}'><i class='fa fa-search'></i> Generar</button> 

                        </form>
                      </div>
                    </div>
                  </div>

                  </div>
                </div>
                          
                <span class='ciclo_loading4' style=''><img src='".FULL_URL."js/jquery.select2/select2-spinner.gif'></span>
                <div id='ramaarbol' style='background-color:#fff; height:100%;'></div>
                <br/>
                <br/>
                <br/>
                <br/>
                <br/>

                ";

echo $lista;


?>
