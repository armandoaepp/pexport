<?php
date_default_timezone_set('America/Lima');
//require "../conexion.php";
require_once(APP."View/Main/conexion.ctp");

$usu=$_GET["idusu"];
$est=$_GET["idest"];

$query2 = mssql_query("select IdUsuario, NomPersona, ApePersona from [GlobalMaster.Persona] p inner JOIN [ComMovTab.Usuario] u on p.IdPersona = u.IdPersona");


$lista="
<div class='alert alert-success alert-white rounded'>
								<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>
								<div class='icon'><i class='fa fa-check'></i></div>
								<strong>Correcto!</strong> <span id = 'respuesta'></span>
							 </div>
							 
<form role='form'  id='formreasigna' method='post'>
              

              <div class='form-group'>";

                                $lista.= "<label>Busca Persona:</label> </br>
                                <select class='select2' id='IdUsuario' name='IdUsuario'>";
                                while ($retorno = mssql_fetch_object($query2)){
                              $lista.= "<option value='$retorno->IdUsuario'>$retorno->NomPersona  $retorno->ApePersona</option>";
                            }
                                $lista.= "</select>
                                

              <button class='btn btn-warning' type='button' onclick='reasignatecnico()'><i class='fa fa-floppy-o'></i> Grabar</button>
              </div>

";


/*
$lista="

<input id='check-all' type='checkbox' name='checkall' style='position: absolute; opacity: 1;' />

<div class='items products'>
        <div class='item'>
          <div><input type='checkbox' name='c[]'' style='position: absolute; opacity: 1;' /> </div>
          <div>
            <span class='date pull-right price'>$25</span>
            <img class='product-image' src='http://placehold.it/50/CCCCCC/808080'>
            <h4 class='from'>Product 1</h4>
            <p class='msg'>This is the product description.</p>
          </div>
        </div>   
        <div class='item'>
          <div><input type='checkbox' name='c[]' class='tech' style='position: absolute; opacity: 1;' /> </div>
          <div>
            <span class='date pull-right price'>$38</span>
            <img class='product-image' src='http://placehold.it/50/CCCCCC/808080'>
            <h4 class='from'>Product 2</h4>
            <p class='msg'>This is the product description.</p>
          </div>
        </div>   
          
      </div>";
*/


$query3 = mssql_query("select IdSector, Cuadrilla,
SubCuadrilla,
IdConcesionaria,
setfinal.Sector,
setfinal.IdUsuario as IdUsuario,
gp.NomPersona as NomPersona,
TotalAsignadas,
TotalDescargadas,
TotalExportadas,
TotalFinalizadas from (
select IdSector,Sector,IdUsuario,
SUM(TotalAsignadas) as TotalAsignadas,
SUM(TotalDescargadas) as TotalDescargadas,
SUM(TotalExportadas) as TotalExportadas,
SUM(TotalFinalizadas) as TotalFinalizadas from (
select  
co.IdSector as IdSector,
co.Sector as Sector,
ci.IdUsuario as IdUsuario,
(case when ci.IdEstado = 13 and co.Condicion='0' and co.SubActividad 
in (select DetalleTipoIntervencion from [ComMovTab.TipoIntervencion])
then 1 else 0 end) as TotalDescargadas,
(case when ci.IdEstado = 5 and co.Condicion='0' and co.SubActividad 
in (select DetalleTipoIntervencion from [ComMovTab.TipoIntervencion])
then 1 else 0 end) as TotalAsignadas,
(case when ci.IdEstado = 14 and co.Condicion='0' and co.SubActividad 
in (select DetalleTipoIntervencion from [ComMovTab.TipoIntervencion])
then 1 else 0 end) as TotalExportadas,
(case when ci.IdEstado = 15 and co.Condicion='0' and co.SubActividad 
in (select DetalleTipoIntervencion from [ComMovTab.TipoIntervencion])
then 1 else 0 end) as TotalFinalizadas
from [ComMovTab.Orden] co 
inner join [ComMovTab.Intervencion] ci on co.IdOrden = ci.IdOrden
where co.Condicion='0' and co.SubActividad 
in (select DetalleTipoIntervencion from [ComMovTab.TipoIntervencion])) as superconsulta
group by IdSector,Sector,IdUsuario) as setfinal
inner join (
select
gu.IdUbicacionConc as IdConcesionaria,
case when (select IdConceptosUbicacion from [GlobalMaster.Ubicacion] where IdUbicacion =dr2.IdUbicacionSup)=8
or (select IdConceptosUbicacion from [GlobalMaster.Ubicacion] where IdUbicacion =dr2.IdUbicacionSup)=7  
then gu3.NomUbicacion else gu2.NomUbicacion end as Cuadrilla,
gu2.NomUbicacion as SubCuadrilla,
gu.NomUbicacion as Sector
from [GlobalMaster.DetalleUbicacionRegla] dr
inner join [GlobalMaster.DetalleUbicacionRegla] dr2 on dr.IdUbicacionSup = dr2.IdUbicacion
inner join [GlobalMaster.Ubicacion] gu on dr.IdUbicacion = gu.IdUbicacion
inner join [GlobalMaster.Ubicacion] gu2 on dr.IdUbicacionSup = gu2.IdUbicacion
inner join [GlobalMaster.Ubicacion] gu3 on dr2.IdUbicacionSup = gu3.IdUbicacion
where gu.IdConceptosUbicacion = 5 ) as setfinal2 on
setfinal.IdSector=setfinal2.IdConcesionaria
inner join [ComMovTab.Usuario] cu on setfinal.IdUsuario=cu.IdUsuario
inner join [GlobalMaster.Persona] gp on cu.IdPersona = gp.IdPersona
where SubCuadrilla = 'Cuadrilla Reclamos 02' and cu.IdUsuario = '1'
order by Cuadrilla");


$lista.="<table class='table table-bordered' id='example'>
                   <thead class='primary-emphasis'>
                      <th><input type ='checkbox' id='all' name='all' value='' ></th>
                      <th>Cuadrilla</th>
                      <th>Sub-Cuadrilla</th>
                      <th>Sector</th>
                      <th>Persona</th>
                      <th>Asignadas</th>
					  <th>Descargadas</th>
                  </thead>
                  <tbody>";

                    
                    while ($retorno = mssql_fetch_object($query3)){
                       $varo=$retorno->IdSector;
                       $vari=$retorno->IdSector;
                       $var=$retorno->Cuadrilla;
                       $var44=utf8_decode($retorno->SubCuadrilla);
                       $var43=utf8_decode($retorno->Sector);
                       $var2=utf8_decode($retorno->NomPersona);
                       $var3=utf8_decode($retorno->TotalAsignadas);
                       $var4=utf8_encode($retorno->TotalDescargadas);
					   $var5=utf8_encode($retorno->IdUsuario);
                       $codigo = $vari.'_'.$var5;
                       $lista.= "<tr class='odd gradeX'>
                                  <td><input type ='checkbox' id='idchk[]' name='idchk[]' value='$codigo' ></td>
                       			      <td>$var</td>
									  <td>$var44</td>
									  <td>$var43</td>
									  <td>$var2</td>
									  <td>$var3</td>
									  <td>$var4</td>
                                </tr>"; 
                       }                       
                  $lista.="</tbody>
                </table>
                </form>
                ";

echo $lista;

?>

<script>
function reasignatecnico(){
	var count_checked = $("[name='idchk[]']:checked").length; // count the checked
	if(count_checked == 0) {
		alert("Por favor, seleccione una Orden(s) que desea reasignar.");
		return false;
	} 
	if(count_checked == 1) {
		var rpta = confirm("Seguro que desea reasignar esta Orden?");
		if(rpta){
			var idintervencion = new Array();
			$( "input[name='idchk[]']:checked" ).each( function() {
				idintervencion.push($( this ).val());
			});
			$.ajax({
				type: "POST",
				url: FULL_URL+"main/asignar_orden",
				dataType: 'html',
				data: 'idintervencion='+idintervencion+'&idusuario='+$('#IdUsuario').val(),
				success: function(data){
					$('#respuesta').html(data)
				}
			});
		}
		return false;		
	} else {
		var rpta = confirm("Seguro que quieres reasignar estas Ordenes?");
		if(rpta){
			var idintervencion = new Array();
			$( "input[name='idchk[]']:checked" ).each( function() {
				idintervencion.push($( this ).val());
			});
			$.ajax({
				type: "POST",
				url: FULL_URL+"main/asignar_orden",
				dataType: 'html',
				data: 'idintervencion='+idintervencion+'&idusuario='+$('#IdUsuario').val(),
				success: function(data){
					$('#respuesta').html(data)
				}
			});
		}
	}
}
</script>