<?php
ini_set ( 'max_execution_time', 300 );
//require_once "../conexion.php";
require_once(APP."View/Main/conexion.ctp");

function getTiempoString($tiempo_segundos){
	$str_dias = 0;
	$str_horas = 0;
	$str_minutos = 0;
	if($tiempo_segundos>36400){
		$str_dias = floor ($tiempo_segundos/36400);
		$tiempo_segundos = $tiempo_segundos%36400;
	}
	if($tiempo_segundos>3600){
		$str_horas = floor ($tiempo_segundos/3600);
		$tiempo_segundos = $tiempo_segundos%3600;
	}
	if($tiempo_segundos>60){
		$str_minutos = floor ($tiempo_segundos/60);
		$tiempo_segundos = floor ($tiempo_segundos%60);
	}

	$str_horas      = str_pad( $str_horas, 2, "0", STR_PAD_LEFT );
	$str_minutos    = str_pad( $str_minutos, 2, "0", STR_PAD_LEFT );
	$tiempo_segundos= str_pad( $tiempo_segundos, 2, "0", STR_PAD_LEFT );

	return ($str_dias>0?$str_dias.' días ':' ').$str_horas.':'.$str_minutos.':'.$tiempo_segundos;
}

$query = pg_query ( "SELECT TT.anio, TT.mes, sum(count_intervenciones) as count_intervenciones, sum(count_suministros) as count_suministros, tiempo_promedio 
FROM (
SELECT extract('year' from FechaAtencion) as anio, extract('month' from FechaAtencion) as mes, 
COUNT(*) as count_intervenciones, 0 as count_suministros
FROM ComMovTab_Intervencion CI
inner join ComMovTab_Orden CO on CI.IdOrden = CO.IdOrden
where CO.SubActividad in (select DetalleTipoIntervencion from ComMovTab_TipoIntervencion)
and CI.IdEstado=15 and CO.Condicion='0'
GROUP BY extract('year' from FechaAtencion), extract('month' from FechaAtencion)
UNION
SELECT *, 0 as count_intervenciones, COUNT(*) as count_suministros FROM (
SELECT extract('year' from FechaAtencion) as anio, extract('month' from FechaAtencion) as mes
FROM ComMovTab_Intervencion CI
inner join ComMovTab_Orden CO on CI.IdOrden = CO.IdOrden
where CO.SubActividad in (select DetalleTipoIntervencion from ComMovTab_TipoIntervencion)
and CI.IdEstado=15 and CO.Condicion='0'		
GROUP BY extract('year' from FechaAtencion), extract('month' from FechaAtencion), suministro
) T 
GROUP BY anio, mes
) TT
INNER JOIN (
select extract('year' from FechaAtencion) as anio, extract('month' from FechaAtencion) as mes,avg(tiempo) as tiempo_promedio from (
select FechaAsignado, FechaAtencion, ((DATE_PART('day', FechaAtencion - FechaAsignado) * 24 + 
                DATE_PART('hour', FechaAtencion - FechaAsignado)) * 60 +
                DATE_PART('minute', FechaAtencion - FechaAsignado)) * 60 +
                DATE_PART('second', FechaAtencion - FechaAsignado) as tiempo
from ComMovTab_Intervencion where FechaAtencion is not null) as T
group by extract('year' from FechaAtencion), extract('month' from FechaAtencion)
) TT_TIEMPO_PROM ON TT.anio=TT_TIEMPO_PROM.anio and TT.mes=TT_TIEMPO_PROM.mes
GROUP BY TT.anio, TT.mes, tiempo_promedio
ORDER BY 1 DESC, 2 ASC" );

$rs_count = pg_num_rows ( $query );

?>
<div class="col-md-12">
	<div class="block-flat">

		<div class="header">
			<h3>Reporte de Intervenciones y Suministros por Mes</h3>
		</div>


		<table id="tabla_reporte1" class="red">
			<thead>
				<tr>
					<th>A&ntilde;o</th>
					<th>Mes</th>
					<th>Intervenciones</th>
					<th>Suministros</th>
					<th>Tiempo Promedio</th>
				</tr>
			</thead>
			<tbody>
<?php
$sum_intervenciones = 0;
$sum_suministros = 0;
$sum_tiempo_prom = 0;
while ( $obj = pg_fetch_object ( $query ) ) {
	?>
	<tr>
					<td>
	<?php echo $obj->anio;?>
	</td>
					<td>
	<?php echo $obj->mes;?>
	</td>
					<td>
	<?php echo number_format($obj->count_intervenciones);?>
	</td>
					<td>
	<?php echo number_format($obj->count_suministros);?>
	</td>
	<td>
	<?php echo getTiempoString($obj->tiempo_promedio);?>
	</td>
				</tr>
	<?php
	$sum_intervenciones += $obj->count_intervenciones;
	$sum_suministros += $obj->count_suministros;
	$sum_tiempo_prom += $obj->tiempo_promedio;
}
?>
</tbody>
<tfoot>
<tr>
<td colspan="2">Total</td>
<td>
<?php echo number_format($sum_intervenciones);?> (prom: <?php echo number_format($sum_intervenciones/$rs_count,0);?>)
</td>
<td>
<?php echo number_format($sum_suministros);?> (prom: <?php echo number_format($sum_suministros/$rs_count,0);?>)
</td>
<td>
<?php echo getTiempoString($sum_tiempo_prom);?> (prom: <?php echo getTiempoString($sum_tiempo_prom/$rs_count);?>)
</td>
</tr>
</tfoot>
		</table>
	</div>
	
	<div class="block-flat">
	<iframe src="<?php echo FULL_URL?>main/grafico_intervenciones_mes" width="100%" height="380" frameborder='0' scrolling='no'></iframe>
	</div>
	
	<div class="block-flat">
	<iframe src="<?php echo FULL_URL?>main/grafico_tiempo_promedio" width="100%" height="310" frameborder='0' scrolling='no'></iframe>
	</div>
</div>

<script>
var oTable = $('#tabla_reporte1').dataTable( {
    "sDom": 'T<"clear">lfrtip',
      "oTableTools": {
          "sSwfPath": "http://cdn.datatables.net/tabletools/2.2.3/swf/copy_csv_xls_pdf.swf"
          }

    } );
</script>