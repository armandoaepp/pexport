<?php
date_default_timezone_set('America/Lima');
//require "../conexion.php";
require_once(APP."View/Main/conexion.ctp");

$form=$_GET["id"];

/*
 1 Insertar
2 Actualizar
3 Eliminar
*/
switch ($form)
{
	case 1:

		$iden=$_POST["identificadormaestro"];
		$eti=$_POST["etiquetamaestrosintervencion"];
		$tip=$_POST["tipodatomaestrosintervencion"];
		$det=$_POST["descripcionmaestrosintervencion"];
		$lon=$_POST["longitudmaestrosintervencion"];
		$cob=$_POST["objetoformulariomaestrosintervencion"];
		$est=$_POST["estiloobjetomaestrosintervencion"];
		$ord=$_POST["ordenformulariomaestrosintervencion"];
		$raz=$_POST["raizmaestrosintervencion"];

		$fechahora = date("Y-m-d H:i:s");


		$sql = "INSERT INTO ComMovTab_MaestrosIntervencion (IdentificadorMaestroIntervencion,EtiquetaMaestroIntervencion,TipoDatoMaestroIntervencion,DetalleMaestroIntervencion,LongitudTipoDatoMaestrosIntervencion,
		ControlObjetoMaestroIntervencion,EstiloObjetoMaestroIntervencion,PosicionMaestroIntervencion,RaizMaestroIntervencion,DownMovil) VALUES ('$iden','$eti','$tip','$det','$lon','$cob','$est',$ord,$raz,'0')";
		$result = pg_query($sql) or die ($sql);

		if($result){
			echo true;
		}else{
			echo false;
		}

		break;

	case 2:

		$lista=$_POST["idmaestrosintervencioncrud"];
		$lista2=$_POST["identificadormaestrointervencioncrud"];
		$lista3=$_POST["etiquetamaestrointervencioncrud"];
		$lista4=$_POST["tipodatomaestrointervencioncrud"];
		$lista5=$_POST["detallemaestrointervencioncrud"];
		$lista6=$_POST["longitudtipodatomaestrosintervencioncrud"];
		$lista7=$_POST["controlobjetomaestrointervencioncrud"];
		$lista8=$_POST["estiloobjetomaestrointervencioncrud"];
		$lista9=$_POST["posicionmaestrointervencioncrud"];
		$lista10=$_POST["raizmaestrointervencioncrud"];


		$n=count($lista);
		if($n>0)

			$mi = new MultipleIterator();
		$mi->attachIterator(new ArrayIterator($lista));
		$mi->attachIterator(new ArrayIterator($lista2));
		$mi->attachIterator(new ArrayIterator($lista3));
		$mi->attachIterator(new ArrayIterator($lista4));
		$mi->attachIterator(new ArrayIterator($lista5));
		$mi->attachIterator(new ArrayIterator($lista6));
		$mi->attachIterator(new ArrayIterator($lista7));
		$mi->attachIterator(new ArrayIterator($lista8));
		$mi->attachIterator(new ArrayIterator($lista9));
		$mi->attachIterator(new ArrayIterator($lista10));


		foreach ( $mi as $value ) {
			list($code,$iden,$eti,$tip,$det,$lon,$cob,$est,$ord,$raz) = $value;

			$sql="update ComMovTab_MaestrosIntervencion set IdentificadorMaestroIntervencion ='$iden',EtiquetaMaestroIntervencion ='$eti',TipoDatoMaestroIntervencion='$tip',DetalleMaestroIntervencion='$det',LongitudTipoDatoMaestrosIntervencion='$lon',
			ControlObjetoMaestroIntervencion='$cob',EstiloObjetoMaestroIntervencion='$est',PosicionMaestroIntervencion=$ord,RaizMaestroIntervencion='$raz' where IdMaestrosIntervencion = $code";
			$result = pg_query($sql) or die($sql);
		}


		if($result){
			echo true;
		}else{
			echo false;
		}
		break;

	case 3:

		$lista=$_POST["idmaestrosintervencioncrudchk"];
		$cuenta=count($lista);
		if ($cuenta > 0) {
			foreach($lista as $indice => $valor) {
				$sql="delete from ComMovTab_MaestrosIntervencion where IdMaestrosIntervencion = $valor";
				$result = pg_query($sql) or die($sql);
			}
		} else {
			$sql="";
		}

		if($result){
			echo true;
		}else{
			echo false;
		}

		break;
}

?>