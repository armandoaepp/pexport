<?php
date_default_timezone_set('America/Lima');

//require_once "../conexion.php";
require_once(APP."View/Main/conexion.ctp");

$fechaactual=date("Y-m-d");
$mes=date("m");
$mesant=$mes-1;

if(isset($_GET["idini1"])) 
{
  $idini1=$_GET["idini1"];
} else {
  $idini1=$fechaactual;
}


if(isset($_GET["idfin1"])) 
{
  $idfin1=$_GET["idfin1"];
} else {
  $idfin1=$fechaactual;
}





$querymondia2 = pg_query("select 
TipoIntervencion,
SubActividad,
(sum(PendientesMesAnterior)+sum(TotalFinalizadasMesAnterior)) as PendientesAntes,
sum(TotalFinalizadasMesAnterior) as TotalFinalizadasDeuda,
sum(PendientesMesAnterior) as PendientesMesAnteriorActual,
sum(TotalGeneradas) as TotalGeneradasMes,
(sum(PendientesMesAnterior)+sum(TotalGeneradas)+sum(TotalFinalizadasMesAnterior)) as SumaGenerada,
sum(TotalFinalizadas) as TotalFinalizadas,
((sum(TotalGeneradas)+sum(PendientesMesAnterior)+sum(TotalFinalizadasMesAnterior))-(sum(TotalFinalizadas))) as PendientesFinal
from (
select ctis.DetalleTipoIntervencion as TipoIntervencion,
cti.DetalleTipoIntervencion as SubActividad,
(case when extract('month' from cast(ci.FechaAsignado as date)) BETWEEN '01' AND '$mesant' and co.Condicion='0' and ci.IdEstado!=15
then COUNT(*) else 0 end) as PendientesMesAnterior,
(case when cast(ci.FechaAsignado as date) BETWEEN '$idini1' AND '$idfin1' and co.Condicion='0'
then COUNT(*) else 0 end) as TotalGeneradas,
(case when (cast(ci.FechaAtencion as date) BETWEEN '$idini1' AND '$idfin1') and ci.IdEstado=15 and co.Condicion='0'
then COUNT(*) else 0 end) as TotalFinalizadas,
(case when extract('month' from cast(ci.FechaAsignado as date)) BETWEEN '01' AND '$mesant' and (cast(ci.FechaAtencion as date) BETWEEN '$idini1' AND '$idfin1') and ci.IdEstado=15 and co.Condicion='0'
then COUNT(*) else 0 end) as TotalFinalizadasMesAnterior
from ComMovTab_TipoIntervencion cti 
inner join ComMovTab_TipoIntervencion ctis on ctis.IdTipoIntervencion = cti.IdTipoIntervencionSup 
left outer join ComMovTab_Orden co on cti.DetalleTipoIntervencion = co.SubActividad
left outer join ComMovTab_Intervencion ci on co.IdOrden = ci.IdOrden
group by ctis.DetalleTipoIntervencion,cti.DetalleTipoIntervencion,co.Condicion,ci.FechaAtencion,ci.FechaAsignado,co.FechaGeneracion,ci.IdEstado,ci.IdIntervencion) as consulta2
group by TipoIntervencion,SubActividad");


											$pers1b="<div class='pers1'>
											<div class='table-responsive'>
                                                     <table class='table table-bordered tableWithFloatingHeader ' id='datatablepers1b' >
                                                      <thead class='primary-emphasis'>
                                                      <tr>
                                                          <th class='text-center primary-emphasis-dark'>Actividad</th>
                                                          <th class='text-center primary-emphasis-dark'>Sub Actividad</th>
                                                          <th class='text-center primary-emphasis-dark'>Pendientes Meses Anteriores</th>
                                                          <th class='text-center primary-emphasis-dark'>Generada Mes Actual</th>
                                                          <th class='text-center primary-emphasis-dark'>Finalizada Mes Actual</th>
                                                          <th class='text-center danger-emphasis-dark'>Pendiente Mes Anterior Actual</th>
                                                          <th class='text-center danger-emphasis-dark'>Total Pendientes</th>
                                                          <th  class='text-center primary-emphasis-dark'style='width:25%;'>% Total</th>
                                                      </tr>
                                                      </thead>
                                                      <tbody>";
                                                      while ($retornomondia2 = pg_fetch_object($querymondia2)){
                                                                            $varmondia2TipoIntervencion=$retornomondia2->tipointervencion;
                                                                            $varmondia2SubActividad=$retornomondia2->subactividad;
                                                                            $varmondia2PendientesAntes=$retornomondia2->pendientesantes;
                                                                            $varmondia2TotalGeneradasMes=$retornomondia2->totalgeneradasmes;
                                                                            $varmondia2TotalFinalizadas=$retornomondia2->totalfinalizadas;
                                                                            $varmondia2PendientesMesAnteriorActual=$retornomondia2->pendientesmesanterioractual;
                                                                            $varmondia2PendientesFinal=$retornomondia2->pendientesfinal;
                                                                            

                                                                            $totalpenantv[]=$varmondia2PendientesAntes;
                                                                            $totaltotgenv[]=$varmondia2TotalGeneradasMes;
                                                                            $totaltotfinv[]=$varmondia2TotalFinalizadas;
                                                                            $totalpenmesantv[]=$varmondia2PendientesMesAnteriorActual;

                                                                            $totaltotpenv[]=$varmondia2PendientesFinal;


                                                                            if ($varmondia2TotalFinalizadas==0){
                                                                              $varmodia2parcialporcentaje=0;
                                                                            }
                                                                            else {
                                                                            	if(($varmondia2PendientesAntes+$varmondia2TotalGeneradasMes)>0){
                                                                              		$varmodia2parcialporcentaje=($varmondia2TotalFinalizadas*100)/($varmondia2PendientesAntes+$varmondia2TotalGeneradasMes);
                                                                            	}else{
                                                                            		$varmodia2parcialporcentaje=0;
                                                                            	}
                                                                              $varmodia2parcialporcentaje=round($varmodia2parcialporcentaje,2);
                                                                            }
                                                                            
                                                                            if ($varmodia2parcialporcentaje < 33) {
                                                                              $color3='danger';
                                                                              
                                                                            } elseif ($varmodia2parcialporcentaje> 33 && $varmodia2parcialporcentaje < 70) {
                                                                              $color3='warning';
                                                                            } else {
                                                                              $color3='success';
                                                                            }

                                                                            if ($varmodia2parcialporcentaje < 33) {
                                                                              $color33='danger';
                                                                              
                                                                            } elseif ($varmodia2parcialporcentaje > 33 && $varmodia2parcialporcentaje < 70) {
                                                                              $color33='warning';
                                                                            } else {
                                                                              $color33='success';
                                                                            }

                                                                        

                                                      $pers1b.= "
                                                        <tr class='odd gradeX'>
                                                          <td><strong>$varmondia2TipoIntervencion</strong></td>
                                                          <td><strong>$varmondia2SubActividad</strong></td>
                                                          <td><strong>$varmondia2PendientesAntes</strong></td>
                                                          <td><strong>$varmondia2TotalGeneradasMes</strong></td>
                                                          <td><strong>$varmondia2TotalFinalizadas</strong></td>
                                                          <td><strong>$varmondia2PendientesMesAnteriorActual</strong></td>
                                                          <td><strong>$varmondia2PendientesFinal</strong></td>
                                                          <td class='center'><div class='progress progress-striped active'>
                                                                                  <div class='progress-bar progress-bar-$color33' style='width:$varmodia2parcialporcentaje%'>".number_format($varmodia2parcialporcentaje,2)."%</div>
                                                          </div></td>
                                                         </tr>
                                                          ";

                                                          }

                                                        $pers1b.="
	                                                      </tbody>
	                                                      <tfoot >
	                                                      <tr>
                                                        <td style='text-align:right;' colspan='2'><strong>Sub Total</strong></td>
                                                        ";

                                                        $totalpen=array_sum($totalpenantv);
                                                        $totalgen=array_sum($totaltotgenv);
                                                        $totalfin=array_sum($totaltotfinv);
                                                        $totalpenma=array_sum($totalpenmesantv);
                                                        $totalpenfin=array_sum($totaltotpenv);
                                                        
                                                        $pengen=$totalpen+$totalgen;
                                                                                        
                                                        if ($totalfin==0){
                                                                              $varportot=0;
                                                                            }
                                                                            else {
                                                                              $varportot=($totalfin*100)/($pengen);
                                                                              $varportot=round($varportot,2);
                                                                            }
                                                                            


                                                                            if ($varportot < 33) {
                                                                              $color4='danger';
                                                                              
                                                                            } elseif ($varportot> 33 && $varportot < 70) {
                                                                              $color4='warning';
                                                                            } else {
                                                                              $color4='success';
                                                                            }

                                                                            if ($varportot < 33) {
                                                                              $color44='danger';
                                                                              
                                                                            } elseif ($varportot > 33 && $varportot < 70) {
                                                                              $color44='warning';
                                                                            } else {
                                                                              $color44='success';
                                                                            }

                                                                                            $pers1b.="
                                                                                            			<td><strong>$totalpen<strong></td>
                                                                                            			<td><strong>$totalgen<strong></td>
                                                                                                     
	                                                                                                    <td><a href='#' data-toggle='modal' data-target='#mod-success2' onclick='showModalMonAv()'><strong>$totalfin<strong></a></td>
	                                                                                                    <td><strong>$totalpenma<strong></td>
                                                                                                      <td><strong>$totalpenfin<strong></td>
	                                                                                                    <td>
                                                                                                        
                                                                                                     </td>
                                                                                            ";

                                                                                        
                                                                                        
                                                                                       

                                                        $pers1b.="</tr>
                                                                  <tr>
                                                                  <td style='text-align:right;' colspan='2'><strong>Total</strong></td>
                                                                  <td style='text-align:right;' colspan='2'><strong>$pengen</strong></td>
                                                                  <td ><strong>$totalfin</strong></td>
                                                                  <td ><strong>$totalpenma</strong></td>
                                                                  <td ><strong>$totalpenfin</strong></td>
                                                                  <td ><div class='progress progress-striped active'>
                                                                                                        <div class='progress-bar progress-bar-$color44' style='width: $varportot%'>".number_format($varportot,2)."%</div>
                                                                                                        </div></td>
                                                                  </tr>
                                                      </tfoot>


                                                      </table>
                                              </div></div>";


                                              echo $pers1b;

?>

  <script type="text/javascript">

        function UpdateTableHeaders() {
            $("div.divTableWithFloatingHeader").each(function() {
                var originalHeaderRow = $(".tableFloatingHeaderOriginal", this);
                var floatingHeaderRow = $(".tableFloatingHeader", this);
                var offset = $(this).offset();
                var scrollTop = $(window).scrollTop();

                var navegador = navigator.userAgent;
                var inicioScroll = scrollTop-30;
                var marginheader = 30;
                if (navigator.userAgent.indexOf('MSIE') !=-1) {
                 
                } else if (navigator.userAgent.indexOf('Firefox') !=-1) {
                  inicioScroll = scrollTop-50;
                  marginheader = 40;
                } else if (navigator.userAgent.indexOf('Chrome') !=-1) {
                  inicioScroll = scrollTop-30;
                  var marginheader = 50;
                } else if (navigator.userAgent.indexOf('Opera') !=-1) {
                  inicioScroll = scrollTop-30;
                  marginheader = 40;
                } else {
                
                }

                if (( inicioScroll > offset.top ) && (scrollTop < offset.top + $(this).height())) {
                    floatingHeaderRow.css("visibility", "visible");
                    floatingHeaderRow.css("top", (Math.min(scrollTop - offset.top, $(this).height() - floatingHeaderRow.height()))+ marginheader  + "px");

                    // Copy cell widths from original header
                    $("th", floatingHeaderRow).each(function(index) {
                        var cellWidth = $("th", originalHeaderRow).eq(index).css('width');
                        var color = $("th", originalHeaderRow).eq(index).css('background-color');
                        
                        $(this).css('width', cellWidth);
                        $(this).css('background-color', color);
                    });

                    // Copy row width from whole table
                    floatingHeaderRow.css("width", $(this).css("width"));
                    floatingHeaderRow.css("background-color", $(this).css("background-color"));
                }
                else {
                    floatingHeaderRow.css("visibility", "hidden");
                    floatingHeaderRow.css("top", "0px");
                    
                }
            });
        }

        $(document).ready(function() {
            $("table.tableWithFloatingHeader").each(function() {
                $(this).wrap("<div class=\"divTableWithFloatingHeader\" style=\"position:relative\"></div>");

                var originalHeaderRow = $("tr:first", this)
                originalHeaderRow.before(originalHeaderRow.clone());
                var clonedHeaderRow = $("tr:first", this)

                clonedHeaderRow.addClass("tableFloatingHeader");
                clonedHeaderRow.css("position", "absolute");
                clonedHeaderRow.css("top", "0px");
                clonedHeaderRow.css("left", $(this).css("margin-left"));
                clonedHeaderRow.css("visibility", "hidden");
                clonedHeaderRow.css("z-index", "5");
                
                originalHeaderRow.addClass("tableFloatingHeaderOriginal");
            });

            UpdateTableHeaders();
            $(window).scroll(UpdateTableHeaders);
            $(window).resize(UpdateTableHeaders);
        });
</script>