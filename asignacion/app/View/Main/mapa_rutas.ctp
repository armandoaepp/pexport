<?php
date_default_timezone_set('America/Lima');
//require_once "../conexion.php";
require_once(APP."View/Main/conexion.ctp");
if(!isset($_SESSION['idusuario'])) header("location: ../index.php");

if(isset($_GET['id'])) {
	$id_usuario = $_GET['id'];
}else{
	echo 'Link no valido';
	exit();
}

if(isset($_GET['p'])) {
	$perspectiva = $_GET['p'];
}else{
	$perspectiva = 'F';
}

if(isset($_GET['c'])) {
	$criterio = $_GET['c'];
}else{
	$criterio = 'F';
}

if(isset($_GET['date_start'])) {
	$date_start = $_GET['date_start'];
}else{
	$date_start = date('Y-m-d');
}

if(isset($_GET['date_end'])) {
	$date_end = $_GET['date_end'];
}else{
	$date_end = date('Y-m-d');
}
?>


<!DOCTYPE html>

<html lang="es">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="">
<meta name="author" content="">
<link rel="shortcut icon" href="../images/favicon.png">

<title>ENSA</title>
<link
	href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,400italic,700,800'
	rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Raleway:100'
	rel='stylesheet' type='text/css'>
<link
	href='http://fonts.googleapis.com/css?family=Open+Sans+Condensed:300,700'
	rel='stylesheet' type='text/css'>


<!-- Bootstrap core CSS -->
<link href="../js/bootstrap/dist/css/bootstrap.css" rel="stylesheet" />
<link rel="stylesheet"
	href="../fonts/font-awesome-4/css/font-awesome.min.css">
<link rel="stylesheet" type="text/css"
	href="../js/jquery.magnific-popup/dist/magnific-popup.css" />

<style type="text/css" title="currentStyle">
thead input {
	width: 100%
}

input.search_init {
	color: #999
}

p {
	text-align: left;
}
</style>


<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
      <script src="../../assets/js/html5shiv.js"></script>
      <script src="../../assets/js/respond.min.js"></script>
    <![endif]-->
<link rel="stylesheet" type="text/css"
	href="../js/jquery.gritter/css/jquery.gritter.css" />
<script type="text/javascript"
	src="../js/jquery.nestable/jquery.nestable.js"></script>
<link rel="stylesheet" type="text/css"
	href="../js/jquery.nanoscroller/nanoscroller.css" />
<link rel="stylesheet" type="text/css"
	href="../js/jquery.easypiechart/jquery.easy-pie-chart.css" />
<link rel="stylesheet" type="text/css"
	href="../js/bootstrap.switch/bootstrap-switch.css" />
<link rel="stylesheet" type="text/css"
	href="../js/bootstrap.datetimepicker/css/bootstrap-datetimepicker.min.css" />
<link rel="stylesheet" type="text/css"
	href="../js/jquery.select2/select2.css" />
<link rel="stylesheet" type="text/css"
	href="../js/bootstrap.slider/css/slider.css" />
<link rel="stylesheet" type="text/css"
	href="../js/dropzone/css/dropzone.css" />
<link rel="stylesheet" type="text/css"
	href="../js/jquery.niftymodals/css/component.css" />
<!-- Custom styles for this template -->
<link href="../css/style.css" rel="stylesheet" />

</head>
<body>


	<!-- Fixed navbar -->
	<div id="head-nav" class="navbar navbar-default navbar-fixed-top">
		<div class="container-fluid">
			<div class="navbar-header" style="margin-left: -30px;">
				<button type="button" class="navbar-toggle" data-toggle="collapse"
					data-target=".navbar-collapse">
					<span class="fa fa-gear"></span>
				</button>
				<a class="" href="#"><img alt="Avatar" width="80" height="49"
					src="../images/ENSA3.png" /></a><span>Pexport</span>

			</div>
			<div class="navbar-collapse collapse">
				<ul class="nav navbar-nav">
				</ul>
				<ul class="nav navbar-nav navbar-right user-nav"
					data-position="bottom" data-step="3" data-intro=" Cierra tu sesion.">
					<li class="dropdown profile_menu"><a href="#"
						class="dropdown-toggle" data-toggle="dropdown"><img alt="Avatar"
							width="30" height="30" src="../images/us.png" /><span><?php echo $_SESSION['nompersona'] ?></span>
							<b class="caret"></b></a>
						<ul class="dropdown-menu">
							<li><a href="#">Actividad</a></li>
							<li><a href="#">Mensajes</a></li>
							<li class="divider"></li>
							<li><a href="<?php echo FULL_URL ?>usuarios/logout">Salir</a></li>
						</ul></li>
				</ul>
				<ul class="nav navbar-nav navbar-right not-nav">

					<li class="button dropdown"><a href="javascript:;"
						class="dropdown-toggle" data-toggle="dropdown"><i
							class="fa fa-globe"></i><span class="bubble">3</span></a>
						<ul class="dropdown-menu">
							<li>
								<div class="nano nscroller">
									<div class="content">
										<ul>
											<li><a href="#"><i class="fa fa-cloud-upload info"></i><b>Distriluz
														Tecnico</b> Te ha enviado mensaje <span class="date">2
														minutos atras.</span></a></li>
											<li><a href="#"><i class="fa fa-male success"></i> <b>Usuario
														A</b> requiere aprobacion <span class="date">15 minutos
														atras.</span></a></li>
											<li><a href="#"><i class="fa fa-bug warning"></i> <b>Distriluz
														Call Center</b> Modifico un Evento <span class="date">30
														minutos atras.</span></a></li>
										</ul>
									</div>
								</div>
								<ul class="foot">
									<li><a href="#">Ver mensajes anteriores </a></li>
								</ul>
							</li>
						</ul></li>

				</ul>

			</div>
			<!--/.nav-collapse animate-collapse -->
		</div>
	</div>


	<div id="cl-wrapper" class="fixed-menu">

		<!-- INICIO CODIGO MAPA -->

		<!-- Icons Marker: https://developers.google.com/chart/infographics/docs/dynamic_icons?csw=1#pins -->
<?php
$coordenadas = '';
$str_rutas = '';
$i = 1;
$total_rutas = 0;
$tecnico = '';
$fechaejecucion_tmp = '';
$count_finalizadas_no_gps = 0;
$count_rows = 0;
$count_rows_intervenciones = 0;

$latitud_center = '';
$longitud_center = '';

if($criterio=='F'){
	$str_where_fecha = " and (cast(ci.FechaAtencion as date) BETWEEN '".$date_start."' AND '".$date_end."')";
	}else{
	$str_where_fecha = "";
}
		
if(strpos($perspectiva, 'F') !== false){

$query_intervenciones = pg_query("select *, CI.IdEstado as id_estado, COUNT(*) OVER() AS count  from ComMovTab_Intervencion CI
inner join ComMovTab_Orden O on CI.IdOrden=O.IdOrden
inner join ComMovTab_Usuario cu on ci.IdUsuario = cu.IdUsuario
inner join GlobalMaster_Persona gp on cu.IdPersona = gp.IdPersona
inner join ComMovTab_MaestrosEstado me on CI.IdEstado = me.IdEstado
left join GlobalMaster_Gps GPS on O.Suministro=GPS.suministro
where cu.IdUsuario=".$id_usuario." and O.SubActividad in (select DetalleTipoIntervencion from ComMovTab_TipoIntervencion) and O.Condicion='0' 
".$str_where_fecha." and CI.IdEstado in (14,15)
order by FechaAtencion Asc");
$count_rows=pg_num_rows($query_intervenciones);
while ($obj_intervencion = pg_fetch_object($query_intervenciones)){
	if(isset($obj_intervencion->latitud1) && isset($obj_intervencion->longitud1) && $obj_intervencion->latitud1!='0' && $obj_intervencion->longitud1!='0'){
		if($i==1 || $i==$count_rows_intervenciones-$count_finalizadas_no_gps){
			$color_flag = 'EF9D3F';	
		}else{
			$color_flag = '72BAF7';
		}		
		
		if($i>1){
			$latitud_center = $obj_intervencion->latitud1;
			$longitud_center = $obj_intervencion->longitud1;
		}
		
		$fechaejecucion = strtotime($obj_intervencion->fechaatencion);
		if($i>1){
			$tiempo = $fechaejecucion - $fechaejecucion_tmp;
		}else{
			$tiempo = '0';
		}
		$fechaejecucion_tmp = strtotime($obj_intervencion->fechaatencion);

		$horas              = floor ( $tiempo / 3600 );
		$minutes            = ( ( $tiempo / 60 ) % 60 );
		$seconds            = ( $tiempo % 60 );
		 
		$time['horas']      = str_pad( $horas, 2, "0", STR_PAD_LEFT );
		$time['minutes']    = str_pad( $minutes, 2, "0", STR_PAD_LEFT );
		$time['seconds']    = str_pad( $seconds, 2, "0", STR_PAD_LEFT );
		 
		$str_tiempo               = implode( ':', $time );

		$coordenadas .= ' /'.$obj_intervencion->latitud1.','.$obj_intervencion->longitud1.'';
		if($obj_intervencion->id_estado==13){
			$color_flag = '00FF00';
		}else{
		$str_rutas .="
		path.push(new google.maps.LatLng(".$obj_intervencion->latitud1.", ".$obj_intervencion->longitud1."));";
		}
		$str_rutas .="
		  var contentString = '<div id=\"content\"><div id=\"siteNotice\"></div><h1 id=\"firstHeading\" class=\"firstHeading\">Suministro: ".$obj_intervencion->suministro."</h1><div id=\"bodyContent\"><p>' +
      			'Cliente: <strong>".$obj_intervencion->nombresuministro."</strong><br>'+
				'Direcci&oacute;n: <strong>".$obj_intervencion->direccionsuministro."</strong><br>'+
		  		'Fecha de Atenci&oacute;n: <strong>".$obj_intervencion->fechaatencion."</strong><br>'+
		  		'Tiempo transcurrido desde el punto anterior: <strong>".$str_tiempo."</strong><br>'+
    			'Estado: <strong>".$obj_intervencion->nomestado."</strong>'+
		  		'</p></div></div>';
		  var infowindow".($i)." = new google.maps.InfoWindow({
		      content: contentString
		  });
		  var marker".($i)." = new google.maps.Marker({
		    position: new google.maps.LatLng(".$obj_intervencion->latitud1.", ".$obj_intervencion->longitud1."),
		    title: '#' + path.getLength(),
		    map: map,
		    icon: 'https://chart.googleapis.com/chart?chst=d_map_pin_letter&chld=".($i)."|".$color_flag."|000000',
		    size: 'tiny'
		  });
		  google.maps.event.addListener(marker".($i).", 'click', function() {
			  infowindow".($i).".open(map,marker".($i).");
			});  		
		  ";
		$i++;
		
		$total_rutas = $obj_intervencion->count;
		$tecnico = $obj_intervencion->nompersona.' '.$obj_intervencion->apepersona;
	}else{
		$count_finalizadas_no_gps++;
	}
}
}
$count_finalizadas = $i-1;
?>

<?php
//PENDIENTES
if(strpos($perspectiva, 'P') !== false){
$coordenadas = '';
$fechaejecucion_tmp = '';
$x=1;
$latitud_center2 = '';
$longitud_center2 = '';
$count_pendientes_no_gps = 0;

$query_pendientes = pg_query("select *, CI.IdEstado as id_estado, COUNT(*) OVER() AS count  from ComMovTab_Intervencion CI
inner join ComMovTab_Orden O on CI.IdOrden=O.IdOrden
inner join ComMovTab_Usuario cu on ci.IdUsuario = cu.IdUsuario
inner join GlobalMaster_Persona gp on cu.IdPersona = gp.IdPersona
inner join ComMovTab_MaestrosEstado me on CI.IdEstado = me.IdEstado
left join GlobalMaster_Gps GPS on O.Suministro=GPS.suministro
where cu.IdUsuario=".$id_usuario." and O.SubActividad in (select DetalleTipoIntervencion from ComMovTab_TipoIntervencion) and O.Condicion='0' 
and CI.IdEstado in (13)
order by FechaAtencion Asc");
$count_rows=pg_num_rows($query_pendientes);
while ($obj_intervencion = pg_fetch_object($query_pendientes)){
	if(isset($obj_intervencion->latitud1) && isset($obj_intervencion->longitud1) && $obj_intervencion->latitud1!='0' && $obj_intervencion->longitud1!='0'){
		if($x==1 || $x==$count_rows-$count_pendientes_no_gps){
			$color_flag = 'EF9D3F';	
		}else{
			$color_flag = '72BAF7';
		}
		if($x>1 && $latitud_center=='' && $longitud_center==''){
			$latitud_center2 = $obj_intervencion->latitud1;
			$longitud_center2 = $obj_intervencion->longitud1;
		}
		
		$fechaejecucion = strtotime(date('Y-m-d H:i:s'));
		$fechaejecucion_tmp = strtotime($obj_intervencion->fechaatencion);
		$tiempo = $fechaejecucion - $fechaejecucion_tmp;

		$horas              = floor ( $tiempo / 3600 );
		$minutes            = ( ( $tiempo / 60 ) % 60 );
		$seconds            = ( $tiempo % 60 );
		 
		$time['horas']      = str_pad( $horas, 2, "0", STR_PAD_LEFT );
		$time['minutes']    = str_pad( $minutes, 2, "0", STR_PAD_LEFT );
		$time['seconds']    = str_pad( $seconds, 2, "0", STR_PAD_LEFT );
		 
		$str_tiempo               = implode( ':', $time );

		$coordenadas .= ' /'.$obj_intervencion->latitud1.','.$obj_intervencion->longitud1.'';
		if($obj_intervencion->id_estado==13){
			$color_flag = 'FFFF00';
		}else{
		$str_rutas .="
		path.push(new google.maps.LatLng(".$obj_intervencion->latitud1.", ".$obj_intervencion->longitud1."));";
		}
		$str_rutas .="
		  var contentString = '<div id=\"content\"><div id=\"siteNotice\"></div><h1 id=\"firstHeading\" class=\"firstHeading\">Suministro: ".$obj_intervencion->suministro."</h1><div id=\"bodyContent\"><p>' +
      			'Cliente: <strong>".($obj_intervencion->nombresuministro)."</strong><br>'+
				'Direcci&oacute;n: <strong>".($obj_intervencion->direccionsuministro)."</strong><br>'+
		  		'Fecha Asignado: <strong>".$obj_intervencion->fechaatencion."</strong><br>'+
		  		'Tiempo de espera: <strong>".$str_tiempo."</strong><br>'+
    			'Estado: <strong>".$obj_intervencion->nomestado."</strong>'+
		  		'</p></div></div>';
		  var infowindow".($i)." = new google.maps.InfoWindow({
		      content: contentString
		  });
		  var marker".($i)." = new google.maps.Marker({
		    position: new google.maps.LatLng(".$obj_intervencion->latitud1.", ".$obj_intervencion->longitud1."),
		    title: '#".$x."',
		    map: map,
		    icon: 'https://chart.googleapis.com/chart?chst=d_map_pin_letter&chld=".($x)."|".$color_flag."|000000',
		    size: 'tiny'
		  });
		  google.maps.event.addListener(marker".($i).", 'click', function() {
			  infowindow".($i).".open(map,marker".($i).");
			});  		
		  ";
		$i++;
		$x++;
		
		if($tecnico==''){
			$tecnico = $obj_intervencion->nompersona.' '.$obj_intervencion->apepersona;
		}
	}else{
		$count_pendientes_no_gps++;
	}
}
if($latitud_center2!=''){
	$latitud_center = $latitud_center2;
	$longitud_center = $longitud_center2;
}
}
?>

<?php
//ULTIMO PUNTO
if(strpos($perspectiva, 'L') !== false){
$coordenadas = '';
$fechaejecucion_tmp = '';
$x=1;
$latitud_center3 = '';
$longitud_center3 = '';

$query_ultimo_punto = pg_query("select *, CI.IdEstado as id_estado, COUNT(*) OVER() AS count  from ComMovTab_Intervencion CI
inner join ComMovTab_Orden O on CI.IdOrden=O.IdOrden
inner join ComMovTab_Usuario cu on ci.IdUsuario = cu.IdUsuario
inner join GlobalMaster_Persona gp on cu.IdPersona = gp.IdPersona
inner join ComMovTab_MaestrosEstado me on CI.IdEstado = me.IdEstado
left join GlobalMaster_Gps GPS on O.Suministro=GPS.suministro
where cu.IdUsuario=".$id_usuario." and O.SubActividad in (select DetalleTipoIntervencion from ComMovTab_TipoIntervencion) and O.Condicion='0' 
and CI.IdEstado in (14,15) and latitud1 is not null
order by FechaAtencion Desc limit 1");
$count_rows=pg_num_rows($query_ultimo_punto);
while ($obj_intervencion = pg_fetch_object($query_ultimo_punto)){
	if(isset($obj_intervencion->latitud1) && isset($obj_intervencion->longitud1) && $obj_intervencion->latitud1!='0' && $obj_intervencion->longitud1!='0'){
		$color_flag = '00FF00';
		
		if($latitud_center=='' && $longitud_center==''){
			$latitud_center3 = $obj_intervencion->latitud1;
			$longitud_center3 = $obj_intervencion->longitud1;
		}
		
		$fechaejecucion = strtotime($obj_intervencion->fechaatencion);
		if($x>1){
			$tiempo = $fechaejecucion - $fechaejecucion_tmp;
		}else{
			$tiempo = '0';
		}
		$fechaejecucion_tmp = strtotime($obj_intervencion->fechaatencion);

		$horas              = floor ( $tiempo / 3600 );
		$minutes            = ( ( $tiempo / 60 ) % 60 );
		$seconds            = ( $tiempo % 60 );
		 
		$time['horas']      = str_pad( $horas, 2, "0", STR_PAD_LEFT );
		$time['minutes']    = str_pad( $minutes, 2, "0", STR_PAD_LEFT );
		$time['seconds']    = str_pad( $seconds, 2, "0", STR_PAD_LEFT );
		 
		$str_tiempo               = implode( ':', $time );

		$str_rutas .="
		  var contentString = '<div id=\"content\"><div id=\"siteNotice\"></div><h1 id=\"firstHeading\" class=\"firstHeading\">Suministro: ".$obj_intervencion->suministro."</h1><div id=\"bodyContent\"><p>' +
      			'Cliente: <strong>".utf8_decode($obj_intervencion->nombresuministro)."</strong><br>'+
				'Direcci&oacute;n: <strong>".utf8_decode($obj_intervencion->direccionsuministro)."</strong><br>'+
		  		'Fecha de Atenci&oacute;n: <strong>".$obj_intervencion->fechaatencion."</strong><br>'+
    			'Estado: <strong>".$obj_intervencion->nomestado."</strong>'+
		  		'</p></div></div>';
		  var infowindow".($i)." = new google.maps.InfoWindow({
		      content: contentString
		  });
		  var marker".($i)." = new google.maps.Marker({
		    position: new google.maps.LatLng(".$obj_intervencion->latitud1.", ".$obj_intervencion->longitud1."),
		    title: 'Ultimo Punto',
		    map: map,
		    icon: 'https://chart.googleapis.com/chart?chst=d_map_pin_letter&chld=U|".$color_flag."|000000',
		    size: 'tiny'
		  });
		  google.maps.event.addListener(marker".($i).", 'click', function() {
			  infowindow".($i).".open(map,marker".($i).");
			});  		
		  ";
		$i++;
		$x++;
		
		if($tecnico==''){
			$tecnico = $obj_intervencion->nompersona.' '.$obj_intervencion->apepersona;
		}
	}
}
if($latitud_center3!=''){
	$latitud_center = $latitud_center3;
	$longitud_center = $longitud_center3;
}
}
?>
<!DOCTYPE html>
		<html>
<head>
<meta name="viewport" content="initial-scale=1.0, user-scalable=no">
<meta charset="utf-8">
<title>Complex Polylines</title>
<style>
html, body, #map-canvas {
	height: 100%;
	margin: 0px;
	padding: 0px
}
</style>
<script src="https://maps.googleapis.com/maps/api/js?v=3.exp"></script>
<style type="text/css">
.form-group {
	margin-bottom: 0px !important;
}
</style>
</head>
<body>
	<div id="arbol" class="page-aside app filters">
		<div>

			<div class="app-nav collapse">
				<div class="content" style="padding-bottom: 0px;">

					<div class="form-group">
						<label class="control-label">T&eacute;cnico :</label> <br> <span
							style="font-size: 16px;"><strong><?php echo $tecnico;?></strong></span>
					</div>

				</div>
			</div>
			<div class="panel-group accordion" id="accordion">
				<div class="panel panel-default">

					<div class="panel-heading">
						<h4 class="panel-title">
							<a class="accordion-perspectiva" data-toggle="collapse"
								data-parent="#accordion" href="#collapseOne"> <i
								class="fa fa-angle-right"></i> Perspectiva
							</a>
						</h4>
					</div>

					<div id="collapseOne" class="panel-collapse collapse in"
						style="height: auto;">
						<div class="panel-body">
            <?php 
            if(strpos($criterio, 'F') !== false){
				$show_tab2 = 'display: block;';
			}else{
				$show_tab2 = 'display:none;';
			}
			if(strpos($criterio, 'U') !== false){
				$show_tab1 = 'display: block;';
			}else{
				$show_tab1 = 'display:none;';
			}
            ?>
            
            <div class="form-group">
								<label class="control-label">Perspectiva :</label> <input
									id="cbo_perspectiva" name="cbo_perspectiva"
									style="width: 240px;" placeholder="Seleccione una perspectiva">
								<br> <br>
					<?php if(isset($count_finalizadas_no_gps) && $count_finalizadas_no_gps>0){?>
					<label class="control-label">Suministros Finalizados sin GPS :</label>
								<span class="badge badge-info"><?php echo $count_finalizadas_no_gps;?></span>
					<?php }?>
					<?php if(isset($count_pendientes_no_gps) && $count_pendientes_no_gps>0){?>
					<label class="control-label">Suministros Pendientes sin GPS :</label>
								<span class="badge badge-info"><?php echo $count_pendientes_no_gps;?></span>
					<?php }?>
			</div>


							<div class="">
								<button class="navbar-toggle" data-target=".app-nav"
									data-toggle="collapse" type="button">
									<span class="fa fa-chevron-down"></span>
								</button>
								<h2 class="page-title">Filtros</h2>
								<p class="description">Criterios de Busqueda para la perspectiva
									"Ruta Finalizada"</p>

							</div>

							<div class="form-group">
								<label class="control-label">Por :</label> <input
									id="cbo_criterio" name="cbo_criterio" style="width: 240px;"
									placeholder="Seleccione un criterio">
							</div>

							<div class="por_fecha_asignacion" style="<?php echo $show_tab2;?>">
								<div class="form-group">
									<label class="control-label">Por Fecha de Atenci&oacute;n
										(Inicio) :</label>
									<div class="input-group date datetime" data-min-view="2"
										data-date-format="yyyy-mm-dd">
										<input id="dpt_date_start" class="form-control" size="16"
											type="text" value="<?php echo $date_start;?>" readonly> <span
											class="input-group-addon btn btn-primary"><span
											class="glyphicon glyphicon-th"></span></span>
									</div>
								</div>

								<div class="form-group">
									<label class="control-label">Por Fecha de Atenci&oacute;n (Fin)
										:</label>
									<div class="input-group date datetime" data-min-view="2"
										data-date-format="yyyy-mm-dd">
										<input id="dpt_date_end" class="form-control" size="16"
											type="text" value="<?php echo $date_end;?>" readonly> <span
											class="input-group-addon btn btn-primary"><span
											class="glyphicon glyphicon-th"></span></span>
									</div>
								</div>
							</div>


							<button class='btn btn-primary' id='generar-mapa'>Generar Mapa</button>
							<button class='btn' id='imprimir-mapa'
								onClick="$('.accordion-pendiente').click();setTimeout(function(){window.print();}, 400);">Imprimir</button>

						</div>
					</div>
					<?php
					if(strpos($perspectiva, 'P') !== false && $count_pendientes_no_gps>0){
					?>
					<div class="panel-heading">
						<h4 class="panel-title">
							<a class="accordion-pendiente collapsed" data-toggle="collapse"
								data-parent="#accordion" href="#collapseTwo"> <i
								class="fa fa-angle-right"></i> Pendientes
							</a>
						</h4>
					</div>

					<div id="collapseTwo" class="panel-collapse collapse"
						style="height: auto;">
						<div class="panel-body">

							<ol class="dd-list">
			            <?php 
			            if(strpos($perspectiva, 'P') !== false){
			            $k = 1;
			            pg_result_seek($query_pendientes, 0) ;
			            while ($obj_pendiente = pg_fetch_object($query_pendientes)){
							if(isset($obj_pendiente->latitud1) && isset($obj_pendiente->longitud1) && $obj_pendiente->latitud1!='0' && $obj_pendiente->longitud1!='0'){
						?>
						<li class="dd-item" data-id="1">
									<div class="dd-handle">
		                  	<?php 
							$str_class_item = 'item_pendiente';
							$str_action = 'openInfo('.($count_rows_intervenciones-$count_finalizadas_no_gps + $k).');';
							?>
		                  <a class="<?php echo $str_class_item;?>" data-item="<?php echo ($count_rows_intervenciones-$count_finalizadas_no_gps + $k);?>" href="javascript: <?php echo $str_action;?>">
											<div>
		                  <?php echo ($k).' - Suministro: <strong>'.$obj_pendiente->suministro.'</strong>';?><br>
		                  <?php echo 'Cliente: <strong>'.$obj_pendiente->nombresuministro.'</strong>';?><br>
		                  <?php echo 'Direcci&oacute;n: <strong>'.$obj_pendiente->direccionsuministro.'</strong>';?><br>
											</div>
										</a>
									</div>
								</li>
						<?php 
							$k++;
							}
			            }
			            }
			            ?>    
			       		</ol>

						</div>
					</div>
					<?php }?>
				</div>
			</div>

			<div style="display: none;">
				<div id="legend" style="text-align: right;">

					<div class="">
						<label class="control-label"
							style="background: white; padding-left: 3px; padding-right: 3px;">Finalizada
							(Inicio - Fin)</label> <img
							src="https://chart.googleapis.com/chart?chst=d_map_pin_letter&chld=?|EF9D3F|000000">
					</div>

					<div class="">
						<label class="control-label"
							style="background: white; padding-left: 3px; padding-right: 3px;">Finalizada</label>
						<img
							src="https://chart.googleapis.com/chart?chst=d_map_pin_letter&chld=?|72BAF7|000000">
					</div>

					<div class="">
						<label class="control-label"
							style="background: white; padding-left: 3px; padding-right: 3px;">Pendiente</label>
						<img
							src="https://chart.googleapis.com/chart?chst=d_map_pin_letter&chld=?|FFFF00|000000">
					</div>

					<div class="">
						<label class="control-label"
							style="background: white; padding-left: 3px; padding-right: 3px;">Ultimo
							Punto</label> <img
							src="https://chart.googleapis.com/chart?chst=d_map_pin_letter&chld=U|00FF00|000000">
					</div>

				</div>
			</div>

		</div>
	</div>
	<div class="container-fluid" id="pcont"
		style="height: 100%; margin: 0px; padding: 0px;">
		<div id="map-canvas"></div>
	</div>
</body>
		</html>

		<!-- FIN CODIGO MAPA -->


	</div>

	<script src="../js/jquery.js"></script>
	<script type="text/javascript"
		src="../js/jquery.nanoscroller/jquery.nanoscroller.js"></script>
	<script type="text/javascript"
		src="../js/jquery.sparkline/jquery.sparkline.min.js"></script>
	<script type="text/javascript"
		src="../js/jquery.easypiechart/jquery.easy-pie-chart.js"></script>
	<script type="text/javascript" src="../js/behaviour/general.js"></script>
	<script src="../js/jquery.ui/jquery-ui.js" type="text/javascript"></script>
	<script type="text/javascript"
		src="../js/jquery.nestable/jquery.nestable.js"></script>
	<script type="text/javascript"
		src="../js/bootstrap.switch/bootstrap-switch.min.js"></script>
	<script type="text/javascript"
		src="../js/bootstrap.datetimepicker/js/bootstrap-datetimepicker.min.js"></script>
	<script src="../js/jquery.select2/select2.min.js"
		type="text/javascript"></script>
	<script src="../js/bootstrap.slider/js/bootstrap-slider.js"
		type="text/javascript"></script>
	<script type="text/javascript"
		src="../js/jquery.gritter/js/jquery.gritter.js"></script>

	<script type="text/javascript">
    $(document).ready(function(){
      //initialize the javascript
      App.init();
    });
  </script>
	<script type='text/javascript'
		src='../js/jquery.fullcalendar/fullcalendar/fullcalendar.js'></script>

	<!-- Bootstrap core JavaScript
================================================== -->
	<!-- Placed at the end of the document so the pages load faster -->
	<script src="../js/behaviour/voice-commands.js"></script>
	<script src="../js/bootstrap/dist/js/bootstrap.min.js"></script>
	<script type="text/javascript" src="../js/jquery.flot/jquery.flot.js"></script>
	<script type="text/javascript"
		src="../js/jquery.flot/jquery.flot.pie.js"></script>
	<script type="text/javascript"
		src="../js/jquery.flot/jquery.flot.resize.js"></script>
	<script type="text/javascript"
		src="../js/jquery.flot/jquery.flot.labels.js"></script>
</body>
</html>
<script type="text/javascript">
$('#rango_cantidades').slider();

var preload_data1 = [
	{ id: 'F', text: 'Ruta Finalizada'}
    , { id: 'P', text: 'Pendientes'}
    , { id: 'L', text: 'Ultimo Punto'}
];
               
$('#cbo_perspectiva').select2({
	multiple: true
	,query: function (query){
		var data = {results: []};
		               
		$.each(preload_data1, function(){
			if(query.term.length == 0 || this.text.toUpperCase().indexOf(query.term.toUpperCase()) >= 0 ){
				data.results.push({id: this.id, text: this.text });
			}
		});
		               
		query.callback(data);
	}
});
                
$('#cbo_perspectiva').select2('data', [<?php if(strpos($perspectiva, 'F') !== false){ echo "{id: 'F', text: 'Ruta Finalizada'},";}if(strpos($perspectiva, 'P') !== false){echo "{id: 'P', text: 'Pendientes'},";}if(strpos($perspectiva, 'L') !== false){echo "{id: 'L', text: 'Ultimo Punto'}";}?>] )

var preload_data = [
    	            { id: 'F', text: 'Fecha de Atencion'}
    	            //, { id: 'U', text: 'Ubicación Geográfica'}
    	          ];
               
                $('#cbo_criterio').select2({
                    multiple: true
                    ,query: function (query){
                        var data = {results: []};
               
                        $.each(preload_data, function(){
                            if(query.term.length == 0 || this.text.toUpperCase().indexOf(query.term.toUpperCase()) >= 0 ){
                                data.results.push({id: this.id, text: this.text });
                            }
                        });
               
                        query.callback(data);
                    }
                });
                
                $('#cbo_criterio').select2('data', [<?php if(strpos($criterio, 'F') !== false){ echo "{id: 'F', text: 'Fecha de Atencion'},";}if(strpos($criterio, 'U') !== false){echo "{id: 'U', text: 'Ubicación Geográfica'}";}?>] )
                
                $('#cbo_criterio').on("change",function (e) { 
    				console.log("change "+e.val);
    				if(e.val.indexOf("F")>-1){
    					$('.por_fecha_asignacion').show();					
    				}else{
    					$('.por_fecha_asignacion').hide();
    				}
    				if(e.val.indexOf("U")>-1){
    					$('.por_ubicacion').show();
    				}else{
    					$('.por_ubicacion').hide();
    				}
    			});

                $('body').on('click','#generar-mapa',function(e){
					perspectiva = $('#cbo_perspectiva').val();
                	criterio = $('#cbo_criterio').val();
                	date_start = $('#dpt_date_start').val();
                	date_end = $('#dpt_date_end').val();
                	window.open("<?php echo FULL_URL;?>main/mapa_rutas?id=<?php echo $id_usuario;?>&p="+perspectiva+"&c="+criterio+"&date_start="+date_start+"&date_end="+date_end,"_top");
				});
                	                	
</script>
<script>
// This example creates an interactive map which constructs a
// polyline based on user clicks. Note that the polyline only appears
// once its path property contains two LatLng coordinates.

var poly;
var map;
<?php if(isset($count_rows) && ($count_rows + $count_rows_intervenciones)>0 ){?>
//function initialize() {
  var mapOptions = {
    zoom: 14,
    // Center the map on Chiclayo.
    center: new google.maps.LatLng(<?php echo isset($latitud_center)?$latitud_center:'-6.77361';?>, <?php echo isset($longitud_center)?$longitud_center:'-79.8417';?>)
  };

  map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);

  	  var iconsetngs = {
	  	path: google.maps.SymbolPath.FORWARD_CLOSED_ARROW
	  };

	  var polyOptions = {
		  strokeColor: '#000000',
		  strokeOpacity: 1.0,
		  strokeWeight: 2,
		  icons: [{
		  repeat: '70px',
		  icon: iconsetngs,
		  offset: '100%'}]
	  };
  
  poly = new google.maps.Polyline(polyOptions);
  poly.setMap(map);

  var path = poly.getPath();

  /*
  // Because path is an MVCArray, we can simply append a new coordinate
  // and it will automatically appear.
  path.push(new google.maps.LatLng(42.879535, -90.624333));

  // Add a new marker at the new plotted point on the polyline.
  var marker = new google.maps.Marker({
    position: new google.maps.LatLng(42.879535, -90.624333),
    title: '#' + path.getLength(),
    map: map
  });
	*/

  	<?php echo $str_rutas;?>
  // Add a listener for the click event
  //google.maps.event.addListener(map, 'click', addLatLng);

  	//map.controls[google.maps.ControlPosition.RIGHT_BOTTOM].push(document.getElementById('legend'));
//}

//google.maps.event.addDomListener(window, 'load', initialize);

function openInfo(i){
	$('.item_pendiente').each(function(){
		$k = $(this).data('item');
		if(i==$k){     						
	    	eval('infowindow'+$k+'.open(map,marker'+$k+');');
		}else{
			eval('infowindow'+$k+'.close();');
		}
	});
}
<?php }?>
</script>