<?php
date_default_timezone_set('America/Lima');

$fechahora = date("Y-m-d H:i:s");
//require "../conexion.php";
require_once(APP."View/Main/conexion.ctp");

$sel=$_POST["a"];

switch ($sel)
    {
    case 1:

                    $sqldocu="insert into ComMovTab_Documento
                        (NomDocumento,
                         FecDescargado,
                         RutaDocumento,
                         IdTipoIntervencion,
                         IdEstado) values
                        ('Lote Excel',
                         '$fechahora',
                         '',
                         4,
                         1)";
                         
                    $rsdocu = pg_query($sqldocu) or die($sqldocu);


                    $querydoc = pg_query("select IdDocumento from ComMovTab_Documento order by IdDocumento desc limit 1");
                        while ($retorno = pg_fetch_object($querydoc)){
                        $vardoc=$retorno->iddocumento;
                    } 

                    $filename = WWW_ROOT.'images/uploads/emergencia.xlsx';
                    //require_once '../php/ext/PHPExcel-1.7.7/Classes/PHPExcel/IOFactory.php';
                    require_once(APP.'Vendor/PHPExcel-1.7.7/Classes/PHPExcel/IOFactory.php');
                    $objPHPExcel = PHPExcel_IOFactory::load($filename);
                    $objHoja=$objPHPExcel->getActiveSheet()->toArray(null,true,true,true);
                    $i=0;
                    foreach ($objHoja as $iIndice=>$objCelda) {

                            $i=$i+1;
                            if ($i > 4){

                                        $var1=utf8_encode(utf8_decode($objCelda['F']));
                                        $var2=utf8_encode(utf8_decode($objCelda['G']));
                                        $var3=utf8_encode(utf8_decode($objCelda['H']));

                                        $var4=utf8_encode(utf8_decode($objCelda['I']));
                                        $var4=str_replace('"', '', $var4); 

                                        $var5=utf8_encode(utf8_decode($objCelda['J']));
                                        $var6=utf8_encode(utf8_decode($objCelda['O']));
                                        $var7=utf8_encode(utf8_decode($objCelda['AB']));
                                        $var8=utf8_encode(utf8_decode($objCelda['AC']));
                                        $var9=utf8_encode(utf8_decode($objCelda['AD']));

                                        $varcompara=$var2.$var6;
                                      
                                        
                                        $sqlcompara="select Suministro,FechaGeneracion from ComMovTab_Orden";
                                        $rscompara = pg_query($sqlcompara) or die($sqlcompara);
                                        $rscomparacuenta=pg_num_rows($rscompara);
                                        
                                        if ($rscomparacuenta = 0){
                                                
                                                $sql="insert into ComMovTab_Orden (IdDocumento,TipoServicio,NombreSector,Condicion,Suministro,NombreCliente,DireccionCliente,Cartera,FechaGeneracion,Alimentador,SED,Medidor,DownMovil) 
                                                values ($vardoc,'Emergencia','$var1','Pendiente','$var2','$var3','$var4','$var5','$var6','$var7','$var8','$var9',0)";
                                                $rs = pg_query($sql) or die($sql);

                                        } 
                                        else
                                        {       
                                                $varcompara2= array();
                                                while ($retornocom = pg_fetch_object($rscompara)){
                                                    $varcomsum=$retornocom->suministro;
                                                    $varcomfec=$retornocom->fechageneracion;
                                                    $varcompara2[]=$varcomsum.$varcomfec;
                                                }

                                                if (in_array($varcompara, $varcompara2)) {
                                                }else {
                                                    $sql="insert into ComMovTab_Orden (IdDocumento,TipoServicio,NombreSector,Condicion,Suministro,NombreCliente,DireccionCliente,Cartera,FechaGeneracion,Alimentador,SED,Medidor,DownMovil) 
                                                        values ($vardoc,'Emergencia','$var1','Pendiente','$var2','$var3','$var4','$var5','$var6','$var7','$var8','$var9',0)";
                                                        $rs = pg_query($sql) or die($sql);
                                                }
                                        }

                            } 

                            else 

                            {
                            echo '';
                            }

                    }


                    $query = pg_query("select * from ComMovTab_Orden where IdDocumento = $vardoc");
                        while ($retorno = pg_fetch_object($query)){
                        
                        $varorden=$retorno->idorden;

                        $sql2="insert into ComMovTab_Intervencion
                        (IdOrden,
                         IdUsuario,
                         IdUbicacionRegla,
                         FechaAtencion,
                         FechaAsignado,
                         Coordenadas,
                         IdEstado,
                         DownMovil) values
                        ($varorden,
                         1,
                         1,
                         '2014-01-01',
                         '$fechahora',
                         'A',
                         6,
                         0)";
                         
                        $rs2 = pg_query($sql2) or die($sql2);


                        }



                        $disp = pg_query("select 
                        CI.IdIntervencion as IdIntervencion,
                        CI.IdOrden as IdOrden,
                        CI.IdUsuario as IdUsuario
                        from ComMovTab_Intervencion CI join ComMovTab_Orden CO on CI.IdOrden = CO.IdOrden where CO.IdDocumento = $vardoc");
                        while ($retorno2 = pg_fetch_object($disp)){
                        
                        $varinter=$retorno2->idintervencion;


                        $sql3="insert into ComMovTab_MovimientosIntervencion
                        (IdIntervencion,
                         AccionMovimiento,
                         ValorMovimiento,
                         FechaHora,
                         IdUsuario) values
                        ($varinter,
                         'Actualizacion de Estado',
                         '6',
                         '$fechahora',
                         1)";
                         
                        $rs3 = pg_query($sql3) or die($sql3);
                        }
						
                        if (is_file($filename)){
                        	unlink($filename);      
                        }                  
    break;

    case 2:

                    $sqldocu="insert into ComMovTab_Documento
                        (NomDocumento,
                         FecDescargado,
                         RutaDocumento,
                         IdTipoIntervencion,
                         IdEstado) values
                        ('Lote Excel',
                         '$fechahora',
                         '',
                         1,
                         1)";
                         
                    $rsdocu = pg_query($sqldocu) or die($sqldocu);

                    $querydoc = pg_query("select IdDocumento from ComMovTab_Documento order by IdDocumento desc limit 1");
                        while ($retorno = pg_fetch_object($querydoc)){
                        $vardoc=$retorno->iddocumento;
                    }

                    $filename = WWW_ROOT.'images/uploads/reclamo.xlsx';
                    //incluimos la clase
                    require_once(APP.'Vendor/PHPExcel-1.7.7/Classes/PHPExcel/IOFactory.php');
                    //cargamos el archivo que deseamos leer
                    $objPHPExcel = PHPExcel_IOFactory::load($filename);
                    //obtenemos los datos de la hoja activa (la primera)
                    $objHoja=$objPHPExcel->getActiveSheet()->toArray(null,true,true,true);
                    
                    //recorremos las filas obtenidas
                    $i=0;
                    foreach ($objHoja as $iIndice=>$objCelda) {
                      //imprimimos el contenido de la celda utilizando la letra de cada columna

                       $var1=utf8_decode($objCelda['A']);
                       $var2=utf8_encode($objCelda['B']);
                       $var3=utf8_encode($objCelda['C']);
                       $var4=utf8_encode($objCelda['G']);
                       $var5=utf8_decode($objCelda['J']);
                       $var6=utf8_encode($objCelda['K']);
                       $var7=utf8_decode($objCelda['L']);
                       $var8=utf8_decode($objCelda['M']);
                       $var9=utf8_decode($objCelda['N']);
                       $var10=utf8_decode($objCelda['O']);
                       $var11=utf8_decode($objCelda['Q']);
                       $var12=utf8_decode($objCelda['R']);
                       $var13=utf8_decode($objCelda['S']);
                       $var14=utf8_decode($objCelda['T']);
                       $var15=utf8_decode($objCelda['U']);
                       $var16=utf8_decode($objCelda['V']);
                       $var17=utf8_decode($objCelda['W']);
                       $var18=utf8_decode($objCelda['X']);//Sector


                       $var7=str_replace('"', '', $var7); 

                       switch ($var5) {
                            case 'Por Exceso de Consumo':
                                $var5='Reclamo por Exceso de Consumo';
                                break;

                            case 'Cambio de Nombre':
                                $var5='Reclamo por Cambio de Nombre';
                                break;

                            case 'Clientes Ausentes':
                                $var5='Clientes Ausentes';
                                break;
                        } 

                      $i=$i+1;
                      if ($i > 1 && $var1!=''){


                        $sql="insert into ComMovTab_Orden 
                        (IdDocumento,
                         IdOrdenTrabajo,
                         NroSolicitud,
                         TipoServicio,
                         Suministro,
                         Responsable, 
                         Supervisor, 
                         Ejecutor,
                         FechaGeneracion,
                         NombreCliente,
                         DireccionCliente,
                         Zona, 
                         Localidad,
                         Medidor,
                         Marca,
                         Modelo,
                         AnoFabricacion,
                         PotenciaAsignada,
                         TarifaAsignada,
                         TipoSuministro,
                         Sistema,
                         TipoConexion,
                         SubEstacion,
                         SectorServicio,
                         DownMovil) 
                        values ($vardoc,
                            '$var2',
                            '$var1',
                            '$var5',
                            '$var3',
                            'Pexport',
                            'Pexport',
                            'Pexport',
                            '$var4',
                            '$var6',
                            '$var7',
                            'La union',
                            'Pomalca',
                            '$var8',
                            '$var9',
                            '$var10',
                            '$var11',
                            '$var12',
                            '$var13',
                            '$var14',
                            '$var15',
                            '$var16',
                            '$var17',
                            '$var18',
                            0)";

                        $rs = pg_query($sql) or die($sql);


                    } else {
                      echo '';
                      
                    }

                    }


                    $query = pg_query("select * from ComMovTab_Orden where IdDocumento = $vardoc");
                        while ($retorno = pg_fetch_object($query)){
                        
                        $varorden=$retorno->idorden;
                        $varsector=$retorno->sectorservicio;

                        $querycomp = pg_query("select 
                        GDU.IdUbicacionReglaDetalle as IdUbicacionReglaDetalle,
                        GDU.IdUbicacionRegla as IdUbicacionRegla,
                        GU.NomUbicacion as NomUbicacion,
                        GDU.IdUbicacion as IdUbicacion,
                        GDU.IdUbicacionSup as IdUbicacionSup,
                        CU.IdUsuario as IdUsuario,
                        CU.NomUsuario as NomUsuario 
                        from GlobalMaster_DetalleUbicacionRegla GDU
                        left join GlobalMaster_Ubicacion GU on GDU.IdUbicacion = GU.IdUbicacion
                        left join ComMovTab_Usuario CU on GDU.IdUsuario = CU.IdUsuario where GDU.IdUbicacionSup = 198 and GU.NomUbicacion ='$varsector'
                        or GDU.IdUbicacionSup = 199 and GU.NomUbicacion ='$varsector' 
                        or GDU.IdUbicacionSup=200 and GU.NomUbicacion ='$varsector'");
                        while ($retorno = pg_fetch_object($querycomp)){

                        $varsectorcom=$retorno->nomubicacion;
                        $varusuariocom=$retorno->idusuario;
                        $varubireglacom=$retorno->idubicacionregla;
                        

                        if($varsectorcom==$varsector){

                        $sql2="insert into ComMovTab_Intervencion
                        (IdOrden,
                         IdUsuario,
                         IdUbicacionRegla,
                         FechaAtencion,
                         FechaAsignado,
                         Coordenadas,
                         IdEstado,
                         DownMovil) values
                        ($varorden,
                         $varusuariocom,
                         $varubireglacom,
                         '2014-01-01',
                         '$fechahora',
                         'A',
                         5,
                         0)";

                         
                        $rs2 = pg_query($sql2) or die($sql2);
                        }
                        else
                        {

                        }

                        }

                        }



                        $disp = pg_query("select 
                        CI.IdIntervencion as IdIntervencion,
                        CI.IdOrden as IdOrden,
                        CI.IdUsuario as IdUsuario
                        from ComMovTab_Intervencion CI join ComMovTab_Orden CO on CI.IdOrden = CO.IdOrden where CO.IdDocumento = $vardoc");
                        while ($retorno2 = pg_fetch_object($disp)){
                        
                        $varinter=$retorno2->idintervencion;

                        $sql4="insert into ComMovTab_MovimientosIntervencion
                        (IdIntervencion,
                         AccionMovimiento,
                         ValorMovimiento,
                         FechaHora,
                         IdUsuario) values
                        ($varinter,
                         'Actualizacion de Estado',
                         '12',
                         '$fechahora',
                         1)";
                         
                        $rs4 = pg_query($sql4) or die($sql4);


                        }


                        $disp2 = pg_query("select 
                        CI.IdIntervencion as IdIntervencion,
                        CI.IdOrden as IdOrden,
                        CI.IdUsuario as IdUsuario
                        from ComMovTab_Intervencion CI join ComMovTab_Orden CO on CI.IdOrden = CO.IdOrden where CO.IdDocumento = $vardoc");
                        while ($retorno3 = pg_fetch_object($disp2)){
                        
                        $varinter3=$retorno3->idintervencion;

                        $sql3="insert into ComMovTab_MovimientosIntervencion
                        (IdIntervencion,
                         AccionMovimiento,
                         ValorMovimiento,
                         FechaHora,
                         IdUsuario) values
                        ($varinter3,
                         'Actualizacion de Estado',
                         '5',
                         '$fechahora',
                         1)";
                         
                        $rs3 = pg_query($sql3) or die($sql3);


                        }

						if(is_file($filename)){
                        	unlink($filename);
						}
    break;
	
	//Insert Reconexion
	case 4:

                    $sqldocu="insert into ComMovTab_Documento
                        (NomDocumento,
                         FecDescargado,
                         RutaDocumento,
                         IdTipoIntervencion,
                         IdEstado) values
                        ('Lote Excel',
                         '$fechahora',
                         '',
                         1,
                         1)";
                         
                    $rsdocu = pg_query($sqldocu);

                    $querydoc = pg_query("select IdDocumento from ComMovTab_Documento order by IdDocumento desc limit 1");
                    $retorno = pg_fetch_object($querydoc) ;                    
                    $vardoc=$retorno->iddocumento;

                    $filename = WWW_ROOT."images/uploads/rx.xlsx";
                    //incluimos la clase
                    require_once(APP.'Vendor/PHPExcel-1.7.7/Classes/PHPExcel/IOFactory.php');
                    //cargamos el archivo que deseamos leer
                    $objPHPExcel = PHPExcel_IOFactory::load($filename);
                    //obtenemos los datos de la hoja activa (la primera)
                    $objHoja=$objPHPExcel->getActiveSheet()->toArray(null,true,true,true);
                    
                    //recorremos las filas obtenidas
                    $i=0;
                    foreach ($objHoja as $iIndice=>$objCelda) {
                      //imprimimos el contenido de la celda utilizando la letra de cada columna

                      $unidad_negocio=htmlentities($objCelda['A']);
					  $orden_trabajo=htmlentities($objCelda['D']);
					  $centro_servicio=htmlentities($objCelda['E']);
					  $sector=htmlentities($objCelda['F']);
					  $suministro=htmlentities($objCelda['G']);
					  $nombre=htmlentities($objCelda['H']);
					  $direccion=htmlentities($objCelda['I']);
					  $direccionr=htmlentities($objCelda['J']);
					  $seriemedidor=htmlentities($objCelda['K']);
					  $var10=htmlentities($objCelda['L']);
					  $var11=htmlentities($objCelda['M']);
					  $fecha_generacion=htmlentities($objCelda['N']);
					  $var14=htmlentities($objCelda['P']);
					  $var15=htmlentities($objCelda['Q']);
					  $ruta=htmlentities($objCelda['R']);
					  $lcorrelati=htmlentities($objCelda['S']);                     
					  $var22=htmlentities($objCelda['X']);
					  $tipoconexion=htmlentities($objCelda['Z']);
					  $var25=htmlentities($objCelda['AA']);
						
						if($unidad_negocio == 'Chiclayo'){
							$id_negocio = '26';
						}else{
							$id_negocio = '30';
						}


                      $i=$i+1;
                      if ($i > 4 && $unidad_negocio!=''){


                        $sql="insert into ComMovTab_Orden 
                        (IdDocumento,
                         IdEstado,
                         Created,
                         Exportado,
                         Suministro,
                         IdOrdenTrabajo, 
                         Empresa, 
                         IdUnidadNegocio,
                         UnidadNegocio,
                         IdCentroServicio,
                         CentroServicio,
                         Proveedor, 
                         Actividad,
                         SubActividad,
                         FechaGeneracion,
                         Periodo,
                         UsuarioGenero,
                         NombreSuministro,
                         DireccionSuministro,
                         Sector,
                         Ruta,
                         LCorrelati,
                         TipoConexion,
                         SerieMedidor) 
                        values ($vardoc,
							'3',
                            '$fechahora',
                            '$fechahora',
                            '$suministro',
                            '$orden_trabajo',
                            'Electronorte SAC',
                            '$id_negocio',
							'$unidad_negocio',
                            '251',							
                            '$centro_servicio',
                            'Pexport S.A.C.',
                            'Reconexion',
                            'En Linea Aerea - Trifasico RX',
                            '$fecha_generacion',
                            201409,
                            'Pexport',
                            '$nombre',
                            '$direccion',
                            '$sector',
                            '$ruta',
                            '$lcorrelati',
                            '$tipoconexion',
                            '$seriemedidor')";

                        $rs = pg_query($sql) or die($sql);


                    } else {
                      echo '';
                      
                    }

                    }

        date_default_timezone_set('America/Lima');
		$fechahora = date("Y-m-d H:i:s");
			
		$querydoc = pg_query("select IdDocumento from ComMovTab_Documento order by IdDocumento desc limit 1");
        while($obj_documento = pg_fetch_object($querydoc)){           
        	$vardoc = $obj_documento->iddocumento;
        }
        
        $query = pg_query("select * from ComMovTab_Orden where IdDocumento = $vardoc");
        while ($arr_obj_orden = pg_fetch_object($query)){
        	$varorden = $arr_obj_orden->idorden;
        	$varsector = $arr_obj_orden->sector;

        	$querycomp = pg_query("select
                        GDU.IdUbicacionReglaDetalle as IdUbicacionReglaDetalle,
                        GDU.IdUbicacionRegla as IdUbicacionRegla,
                        GU.IdUbicacionConc as IdUbicacionConc,
                        GU.NomUbicacion as NomUbicacion,
                        GDU.IdUbicacion as IdUbicacion,
                        GDU.IdUbicacionSup as IdUbicacionSup,
                        CU.IdUsuario as IdUsuario,
                        CU.NomUsuario as NomUsuario 
                        from GlobalMaster_DetalleUbicacionRegla GDU
                        left join GlobalMaster_Ubicacion GU on GDU.IdUbicacion = GU.IdUbicacion
                        left join ComMovTab_Usuari] CU on GDU.IdUsuario = CU.IdUsuario 
						where GDU.IdUbicacionSup = 198 and GU.NomUbicacion ='$varsector'
                        or GDU.IdUbicacionSup = 199 and GU.NomUbicacion ='$varsector'
                        or GDU.IdUbicacionSup=200 and GU.NomUbicacion ='$varsector'
                        or GDU.IdUbicacionSup=203 and GU.NomUbicacion ='$varsector'");
        	while ($arr_obj_asignacion = pg_fetch_object($querycomp)){
        		$varsectorcom=$arr_obj_asignacion->nomubicacion;
				$varidubicacioncon=$arr_obj_asignacion->idubicacionconc;
        		$varusuariocom=$arr_obj_asignacion->idusuario;
        		$varubireglacom=$arr_obj_asignacion->idubicacionregla;
				error_log($varidubicacioncon.'=='.$varsector, 3, 'intervencion.log');
        		if($varidubicacioncon==$varsector){
					$existe_intervencion = $this->validarDuplicadoIntervencion($varorden);

					if($existe_intervencion==0){
						$sql2="insert into ComMovTab_Intervencion
                        (IdOrden,
                         IdUsuario,
                         IdUbicacionRegla,
                         FechaAtencion,
                         FechaAsignado,
                         Coordenadas,
                         IdEstado,
                         DownMovil) values
                        ($varorden,
                        $varusuariocom,
                        $varubireglacom,
                         '$fechahora',
                         '$fechahora',
                         'A',
                         5,
                         0)";
						pg_query($sql2);
					}
        		}
        	}
        }              

        $disp = pg_query("select
                        CI.IdIntervencion as IdIntervencion,
                        CI.IdOrden as IdOrden,
                        CI.IdUsuario as IdUsuario
                        from ComMovTab_Intervencion CI join ComMovTab_Orden CO on CI.IdOrden = CO.IdOrden where CO.IdDocumento = $vardoc");
        while ($retorno2 = pg_fetch_object($disp)){
        	$varinter=$retorno2->idintervencion;
        	$sql4="insert into ComMovTab_MovimientosIntervencion
                        (IdIntervencion,
                         AccionMovimiento,
                         ValorMovimiento,
                         FechaHora,
                         IdUsuario) values
                        ($varinter,
                         'Actualizacion de Estado',
                         '12',
                         '$fechahora',
                         1)";
        	pg_query($sql4);
        }

        $disp2 = pg_query("select
                        CI.IdIntervencion as IdIntervencion,
                        CI.IdOrden as IdOrden,
                        CI.IdUsuario as IdUsuario
                        from ComMovTab_Intervencion CI join ComMovTab_Orden CO on CI.IdOrden = CO.IdOrden where CO.IdDocumento = $vardoc");
        while ($retorno3 = pg_fetch_object($disp2)){
        	$varinter3=$retorno3->idintervencion;
        	$sql3="insert into ComMovTab_MovimientosIntervencion
                        (IdIntervencion,
                         AccionMovimiento,
                         ValorMovimiento,
                         FechaHora,
                         IdUsuario) values
                        ($varinter3,
                         'Actualizacion de Estado',
                         '5',
                         '$fechahora',
                         1)";
        	pg_query($sql3);
        }                        //unlink('../images/uploads/reclamo.xlsx');
    break;
    }

              
?>