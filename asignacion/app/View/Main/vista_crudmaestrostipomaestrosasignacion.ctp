<?php

date_default_timezone_set('America/Lima');
//require "../conexion.php";
require_once(APP."View/Main/conexion.ctp");

$querycomb1 = pg_query("select * from ComMovTab_TipoIntervencion");


$table1 = pg_query("select
CMI.IdMaestrosIntervencion as IdMaestrosIntervencion,
CMI.IdentificadorMaestroIntervencion as var1,
CMI.EtiquetaMaestroIntervencion as EtiquetaMaestroIntervencion,
CMI.RaizMaestroIntervencion as RaizMaestroIntervencion,
CMIS.IdentificadorMaestroIntervencion as var11
from ComMovTab_MaestrosIntervencion CMI left join 
ComMovTab_MaestrosIntervencion CMIS on CMI.RaizMaestroIntervencion::INT4 = CMIS.IdMaestrosIntervencion");


$query = pg_query("select
MCTI.IdMaestrosIntervencion as IdMaestrosIntervencion,
MCTI.IdTipoIntervencion as IdTipoIntervencion,
CMI.IdentificadorMaestroIntervencion as var1,
CTI.DetalleTipoIntervencion as DetalleTipoIntervencion
from ComMovTab_TipoMaestrosIntervencion MCTI 
join ComMovTab_MaestrosIntervencion CMI on MCTI.IdMaestrosIntervencion=CMI.IdMaestrosIntervencion
join ComMovTab_TipoIntervencion CTI on MCTI.IdTipoIntervencion = CTI.IdTipoIntervencion
");



$lista="
    <div class='row'>
      <div class='col-sm-12 col-md-12'>
        <div class='block-flat'>


        <div class='panel-group accordion accordion-semi' id='accordion4'>
					  <div class='panel panel-default'>
						<div class='panel-heading success'>
						  <h4 class='panel-title'>
							<a data-toggle='collapse' data-parent='#accordion4' href='#ac4-1'>
								<i class='fa fa-angle-right'></i> Insertar Elementos a un Formulario
							</a>
						  </h4>
						</div>
						<div id='ac4-1' class='panel-collapse collapse in'>
						  <div class='panel-body'>
							
						          <div class='content'>

							          <form role='form' id='formtipomaestrosintervencion' method='post' style='width:100%;'>

							          	<div class='form-group'  style='width:60%;'>
							              " ;

											 $lista.= " <label>Selecciona la Regla : </label> <select class='form-control' id='idtipointervencion' name='idtipointervencion'>";
														 while ($retorno = pg_fetch_object($querycomb1)){
															$lista.= "<option value='$retorno->idtipointervencion'>$retorno->detalletipointervencion</option>";
														}
											 $lista.= "</select>
											 </div>

							            <div class='form-group'>
							            	<table class='no-border' id='datatableformasignamaestrostipomaestros'>

							                  <thead class='primary-emphasis'>
							                    <tr>
							                      <th></th>
							                      <th>Codigo</th>
							                      <th>Campo</th>
							                      <th>Etiqueta</th>
							                      <th>Pertenece a</th>
							                    </tr>

							                  </thead>
							                  <tbody class='no-border'>";


							                    while ($retorno = pg_fetch_object($table1)){

							                       $var=utf8_decode($retorno->idmaestrosintervencion);
							                       $var1=utf8_decode($retorno->var1);
							                       $var2=utf8_decode($retorno->etiquetamaestrointervencion);
							                       $var3=utf8_decode($retorno->var11);
							                       


							                       $lista.= "<tr class='odd gradeX'>
							                                  <input type ='hidden' id='idmaestrosintervencion[]' name='idmaestrosintervencion[]' value='$var'>
							                                  <td style='width:1%;'><input type ='checkbox' id='idmaestrosintervencionchk[]' name='idmaestrosintervencionchk[]' value='$var' ></td>
							                                  <td style='width:1%;'>$var</td>
							                                  <td style=''><input type='text' id='identificadormaestrointervencion[]' name='identificadormaestrointervencion[]'  value='.$var1' class='form-control' disabled></td>
							                                  <td style=''><input type='text' id='etiquetamaestrosintervencion[]' name='etiquetamaestrosintervencion[]'  value='$var2' class='form-control' style='width:100%' disabled></td>
							                                  <td style=''><input type='text' id='identificadormaestrointervencionsup[]' name='identificadormaestrointervencionsup[]'  value='$var3' class='form-control' style='width:100%' disabled></td>
							                                  
							                                </tr>";
							                       }
							                  $lista.="</tbody>

							                </table>
							            </div>



							              <button class='btn btn-danger' type='button' onclick='crudtipomaestrosintervencion(1)'>Grabar</button>
							              <button class='btn btn-default' type='button'>Cancelar</button>
							             
							          </form>

						          </div>

						  </div>
						</div>
					  </div>
		</div>


		
						      <div class='row'>
						        <div class='col-md-12'>
						          <div class='block-flat'>

						            <button class='btn btn-success btn-flat md-trigger' data-modal='form-green' onclick='showDesign()'>Ver Formulario</button>

						            <div class='content'>
						            <div class='tabla'>

						          
						            <div class=''>

						              <div class='table-responsive'>
						              <form role='form' id='formtipomaestrosintervencioncrud' method='post' > 
						                <table class='no-border' id='datatableasignamaestrostipomaestroscrud'>

						                  <thead class='primary-emphasis'>
						                    <tr>
						                      <th></th>
						                      <th>Elemento Formulario</th>
						                      <th>Editar Elemento Formulario</th>
						                      <th>Tipo Formulario</th>
						                      <th>Editar Tipo Formulario</th>
						                    </tr>

						                  </thead>
						                  <tbody class='no-border'>";


						                    while ($retorno = pg_fetch_object($query)){

						                       $var=utf8_decode($retorno->idmaestrosintervencion);
						                       $vaar=utf8_decode($retorno->var1);
						                       $var1=utf8_decode($retorno->idtipointervencion);
						                       $var11=$retorno->detalletipointervencion;


						                       $lista.= "<tr class='odd gradeX'>

						                       			  <td style='width:1%;'><input type ='checkbox' id='idmaestrosintervencioncrudchk[]' name='idmaestrosintervencioncrudchk[]' value='$var1 $var'></td>

						                       			  <td style=''>$vaar</td>

						                                  <td style='width:10%;'><select class='form-control' id='estado[]' name='estado[]' value='$vaar' >
						                                      <option value='$var'>$vaar</option>
						                                      ";
						                                  $querydet = pg_query("select IdMaestrosIntervencion,IdentificadorMaestroIntervencion as var from ComMovTab_MaestrosIntervencion M  where M.IdMaestrosIntervencion!=$var");
						                                   while ($retornodet = pg_fetch_object($querydet)){
						                                    $idm=$retornodet->idmaestrosintervencion;
						                                    $nomm=$retornodet->var;

						                                    $lista.="<option value='$idm'>$nomm</option>"; 
						                                   } 
						                                  $lista.="</select></td>

						                                  <td style=''>$var11</td>

						                                  <td style='width:10%;'><select class='form-control' id='estado[]' name='estado[]' value='$var11' >
						                                      <option value='$var1'>$var11</option>
						                                      ";
						                                  $querydet = pg_query("select IdTipoIntervencion,DetalleTipoIntervencion from ComMovTab_TipoIntervencion T where T.IdTipoIntervencion!=$var1");
						                                   while ($retornodet = pg_fetch_object($querydet)){
						                                    $idt=$retornodet->idtipointervencion;
						                                    $nomt=$retornodet->detalletipointervencion;

						                                    $lista.="<option value='$idt'>$nomt</option>"; 
						                                   } 
						                                  $lista.="</select></td>




						
						                                </tr>";
						                       }
						                  $lista.="</tbody>

						                </table>
						                <button class='btn btn-success' type='button' onclick='crudtipomaestrosintervencion(2)'>Grabar</button>
						                <button class='btn btn-danger' type='button' onclick='crudtipomaestrosintervencion(3)'>Eliminar</button>
						              </form>              
						              </div>

						              </div>
						              </div>
						              
						            </div>
						          </div>        
						        </div>

						      </div>

        </div>				
      </div>

	</div>";

echo $lista;

?>