<?php
date_default_timezone_set('America/Lima');
//require "../conexion.php";
require_once(APP."View/Main/conexion_emergencia.ctp");

                                        function tiempo($str, $t1, $t2 = false) {
                                          // Si nos dan un segundo parámetros calculamos el tiempo entre dos fechas
                                          $t = $t1-($t2?$t2:time());

                                          // Un array con todos los reemplazos que vamos a usar
                                          $p = array(
                                            '{s}'=>1,
                                            '{i}'=>60,
                                            '{h}'=>60*60,
                                            '{d}'=>60*60*24,
                                            '{w}'=>60*60*24*7,
                                            '{m}'=>60*60*24*30,
                                            '{y}'=>60*60*24*365
                                          );
                                          
                                          // Obtenemos todos los tiempos que fueron proveídos en la string
                                          preg_match_all("/\{[sihdwmy]\}/", $str, $ma);
                                          
                                          // Creamos un array ordenado del mayor al menor tiempo requerido
                                          $found = Array();
                                          foreach ($ma[0] as &$m) {
                                            $found[$m] = $p[$m];
                                          }
                                          arsort($found);
                                          
                                          // Reemplazamos la string con los tiempos
                                          foreach ($found as $i => &$fo) {
                                            $str = str_replace($i, (int) ($t/$fo), $str);
                                            $t = $t % $fo;
                                          }
                                          
                                          return $str;
                                        }

function getColorEstado($var2){
	if($var2==1){
		//Universo
		$color_flag = 'E6E6E6';
	}elseif($var2==2){
		//Eliminacion Logica
		$color_flag = 'FF4000';
	}elseif($var2==3){
		//Activo
		$color_flag = '81F7F3';
	}elseif($var2==4){
		//Inactivo
		$color_flag = 'F3E2A9';
	}elseif($var2==5){
		//Asignado
		$color_flag = '5FB404';
	}elseif($var2==6){
		//Pendiente
		$color_flag = 'FFFF00';
	}elseif($var2==7){
		//En Camino
		$color_flag = 'DF7401';
	}elseif($var2==8){
		//Ejecutado
		$color_flag = '04B45F';
	}elseif($var2==9){
		//Finalizado
		$color_flag = '088A08';
	}elseif($var2==10){
		//Observado
		$color_flag = '81DAF5';
	}elseif($var2==11){
		//Asignaciones
		$color_flag = 'D8D8D8';
	}elseif($var2==12){
		//Generado
		$color_flag = 'E1F5A9';
	}elseif($var2==13){
		//Descargado
		$color_flag = 'A9D0F5';
	}elseif($var2==14){
		//Exportado
		$color_flag = '2E9AFE';
	}elseif($var2==15){
		//Finalizado
		$color_flag = '088A29';
	}
	return $color_flag;
}

$query = pg_query("
select 
CI.IdIntervencion as IdIntervencion,
CO.NombreSector as NombreSector,
CI.IdEstado as IdEstado,
ME.NomEstado as NomEstado,
CO.FechaGeneracion as NuevaFecha,
CO.Suministro as Suministro,
GMP.nompersona as nompersona,
GMP.ApePersona as ApePersona,
GMP.idpersona as idpersona,
CU.idusuario as idusuario,
GMG.longitud1 as longitud,
GMG.latitud1 as latitud,
CO.NombreCliente as NombreCliente,
CO.DireccionCliente as DireccionCliente,
CO.Cartera as Cartera,
CO.FechaGeneracion as FechaGeneracion,
CO.Alimentador as Alimentador,
CO.Sed as Sed,
CO.Medidor as Medidor,
date_part('hour',cast(CO.FechaGeneracion as timestamp)) as NuevaFechaHora,
date_part('minute',cast(CO.FechaGeneracion as timestamp)) as NuevaFechaMinute
from ComMovTab_Intervencion CI 
join ComMovTab_Orden CO on CI.IdOrden = CO.IdOrden
join ComMovTab_Usuario CU on CU.IdUsuario = CI.IdUsuario
join GlobalMaster_Persona GMP on GMP.IdPersona = CU.IdPersona
join ComMovTab_Documento CD on CO.IdDocumento = CD.IdDocumento
join ComMovTab_TipoIntervencion TI on CD.IdTipoIntervencion = TI.IdTipoIntervencion
join ComMovTab_MaestrosEstado ME on CI.IdEstado = ME.IdEstado
join GlobalMaster_Gps GMG on GMG.suministro = CO.suministro
where CD.IdTipoIntervencion=4 order by cast(CO.FechaGeneracion as timestamp) desc");

$lista="
      

      <div class='row'>
        <div class='col-md-12'>
          <div class='block-flat'>

            <div class='panel-group accordion accordion-semi' id='accordion3' style='margin: 0;'>

            <div class='panel panel-default'>
            <div class='panel-heading'>
              <h4 class='panel-title'>
              <a data-toggle='collapse' data-toggle='collapse' data-parent='#accordion3' href='#ac3-1'>
                <i class='fa fa-angle-right'></i> Emergencias
              </a>
              </h4>
            </div>
            <div id='ac3-1' class='panel-collapse collapse in'>
              <div class='panel-body'>

<div class='tabla2'>

          
            <div class='farmsmallasig'>

              <div class='table-responsive'>

                <table class='table table-bordered tableWithFloatingHeader' id='datatable'>

                  <thead class='primary-emphasis'>
                    <tr>
                      <th>Intervencion</th>
                      <th>Sector</th>
                      <th>Accion</th>
                      <th>Estado</th>
                      <th>Tiempo en Estado de Espera</th>
                      <th>Accion</th>
					  <th>Asignado</th>
                      <th>Servicio</th>
                      <th>Cartera</th>
                      <th>Fecha</th>
                      <th>Alimentador</th>
                      <th>SED</th>
					  <th>Cliente</th>
                    </tr>
                    <tr>
                      <td><input type='text' name='' value=''  class='search_init' style='color: darkgray;' /></td>
                      <td><input type='text' name='' value=''  class='search_init' style='color: darkgray;' /></td>
                      <td><input type='text' name='' value=''  class='search_init' style='color: darkgray;' /></td>
                      <td><input type='text' name='' value=''  class='search_init' style='color: darkgray;' /></td>
				   	  <td><input type='text' name='' value=''  class='search_init' style='color: darkgray;' /></td>
                      <td><input type='text' name='' value=''  class='search_init' style='color: darkgray;' /></td>
                      <td><input type='text' name='' value=''  class='search_init' style='color: darkgray;' /></td>
                      <td><input type='text' name='' value=''  class='search_init' style='color: darkgray;' /></td>
                      <td><input type='text' name='' value=''  class='search_init' style='color: darkgray;' /></td>
                      <td><input type='text' name='' value=''  class='search_init' style='color: darkgray;' /></td>
                      <td><input type='text' name='' value=''  class='search_init' style='color: darkgray;' /></td>
                      <td><input type='text' name='' value=''  class='search_init' style='color: darkgray;' /></td>
					  <td></td>
                    </tr>
                  </thead>
                  <tbody class='no-border'>";

                    while ($retorno = pg_fetch_object($query)){

                       $var=($retorno->idintervencion);
                       $var1=($retorno->nombresector);
                       $var2=($retorno->idestado);
                       $var22=($retorno->nomestado);
                       $varnf=($retorno->nuevafecha);
                       $standard = strtotime($varnf);
                       $varnf=date("Y-m-d g:i:s a", $standard);

                       $var3=($retorno->suministro);
                       $var33=($retorno->nompersona);
                       $var332=($retorno->apepersona);
                       $var331=($retorno->idpersona);
                       $var4=($retorno->nombrecliente);
                       $var5=($retorno->direccioncliente);
                       $var6=($retorno->cartera);
                       $var7=($retorno->fechageneracion);
                       $var8=($retorno->alimentador);
                       $var9=($retorno->sed);

                       $var9 = strstr($var9, 'EN' );
                       $var9=substr("$var9", 2);
                       $var10=($retorno->medidor);
                       $idusuario = $retorno->idusuario;


                       $var5nombre=utf8_encode(utf8_decode($retorno->nombrecliente));
                       $cliente = explode(" ", $var5nombre);
                       $cliente = $cliente[0];                       

                                        $varnf10 = new DateTime("$varnf");
                                        $ti = strtotime(date_format($varnf10, 'd-m-Y'));
                                        $varnf11 = strtotime(date("Y-m-d"));

                                        $varhora = tiempo("{h}", time(), strtotime("$varnf"));

                                        
                                        if ($ti >= $varnf11){
                                          //$varTime = tiempo("{d}d, {h}h, {i}m", time(), strtotime("$varnf"));
                                          $varTime = sprintf('%02d',$retorno->nuevafechahora).'h '.sprintf('%02d',$retorno->nuevafechaminute).'m';
                                          //$varTime_expl = explode("d", $varTime);
                                          //$varTime = substr($varTime_expl[1], 1,-5);
                                          /*
                                          $varTime = tiempo("Hace {d} días, {h} horas, {i} minutos y {s} segundos", time(), strtotime("$varnf"));
                                          $varTime = tiempo("Hace {h} horas, {i} minutos", time(), strtotime("$var6"));*/
                                        } else
                                        {

                                          //$varTime = tiempo("{d}d, {h}h, {i}m", time(), strtotime("$varnf"));
                                       	  $varTime = sprintf('%02d',$retorno->nuevafechahora).'h '.sprintf('%02d',$retorno->nuevafechaminute).'m';
                                          //$varTime_expl = explode("d", $varTime);
                                          //$varTime = substr($varTime_expl[1], 1,-5);
                                          /*
                                          $fecha = new DateTime("$varnf");
                                          $v1 = date_format($fecha, 'y');
                                          $v2 = date_format($fecha, 'm');
                                          $v3 = date_format($fecha, 'd');
                                          $v4 = date_format($fecha, 'h');
                                          $v5 = date_format($fecha, 'i');
                                          $v6 = date_format($fecha, 's');

                                          setlocale(LC_ALL,"es_ES@euro","es_ES","esp");
                                          $varTime = utf8_encode(strftime(" %X %A %d de %B del %Y",mktime($v4,$v5,$v6,$v2,$v3,$v1)));
                                          /*$varTime = utf8_encode(strftime(" %d %B %Y",mktime($v4,$v5,$v6,$v2,$v3,$v1)));  */
                                        }



                                        switch ($varhora) {

                                            case ($varhora <= 2):
                                                $flag='btn btn-success .btn-flat';
                                              break;
                                            case ($varhora > 2 && $varhora <= 4):
                                                $flag='btn btn-warning .btn-flat';
                                              break;
                                            case ($varhora > 4):
                                                $flag='btn btn-danger .btn-flat';
                                              break;

                                          }


                       switch ($var2)
                        {
                        case 6:
                          $color='btn btn-danger .btn-flat';
                        break;

                        case 7:
                          $color='btn btn-warning .btn-flat';
                        break;

                        case 8:
                          $color='btn btn-primary .btn-flat';
                        break;

                        case 9:
                          $color='btn btn-success .btn-flat';
                        break;
                        
                        default:
                          $color='btn btn-primary .btn-flat';
                        break;
                        }

                       

                       $lista.= "<tr class='odd gradeX'>
                                  <td style='width:6%;'>$var</td>
                                  <td style='width:12%;'>$var1</td>
                                  <td style='width:6%;'><select class='form-control' id='$var' onchange='actualizaremergencia($var)' >
                                      ";
                                  $querydet = pg_query("select ME.IdEstado as IdEstado,NomEstado as NomEstado from ComMovTab_MaestrosEstado ME");
                                   while ($retornodet = pg_fetch_object($querydet)){
                                    $idest=$retornodet->idestado;
                                    $nomest=$retornodet->nomestado;

                                    $lista.="<option value='$idest'>$nomest</option>"; 
                                   } 
                                  $lista.="</select></td>
                                  <td style='width:10%;'>
                                  <button data-popover='popover' type='button' style='width:90%;' id='buttonestado$var' class='$color' data-original-title='Eventos de Asignacion' 
                                  data-content='";$querydet = pg_query("select 
                                  MI.IdMovimientoIntervencion as IdMovimientoIntervencion,
                                  MI.IdIntervencion as IdIntervencion,
                                  ME.NomEstado as NomEstado,
                                  MI.FechaHora as FechaHora from ComMovTab_MovimientosIntervencion  MI inner join 
                                    ComMovTab_MaestrosEstado ME on MI.ValorMovimiento=ME.IdEstado::TEXT where MI.IdIntervencion = $var");
                                   while ($retornodet = pg_fetch_object($querydet)){
                                    $idmov=$retornodet->idmovimientointervencion;
                                    $idint=$retornodet->idintervencion;
                                    $est=$retornodet->nomestado;
                                    $fec=$retornodet->fechahora;
                                    $standard = strtotime($fec);
                                    $fecha=date("Y-m-d g:i:s a", $standard);
                                    $texto="";
                                    if ($var==$idint){
                                      $lista.= "<p>$fecha Actualizado Estado $est</p>";
                                    } else {
                                    $texto= "";
                                    }
                                   } 
                                  $lista.="' data-placement='right' data-trigger='hover'>
                                  $var22</button></td>
                                  <td style='width:10%;'><button type='button' style='width:90%;' class='$flag' >$varTime</button></td>
                                  <td style='width:8%;'><select class='form-control' id='actualizarlecturista$var' onchange='actualizarlecturista($var)'>
                                        <option value='115' ".(($idusuario==115)?'selected':'').">Movil 01 Pexport</option>
                                        <option value='116' ".(($idusuario==116)?'selected':'').">Movil 02 Ensa</option>
                                      </select></td>
                                  <td style='width:7%;' class='movillectura$var'>$var33 $var332</td>
                                  <td style='width:7%;'>$var3</td>
                                  <td style='width:7%;'>$var6</td>
                                  <td style='width:8%;'>$var7</td>
                                  <td style='width:7%;'>$var8</td>
                                  <td style='width:7%;'>EN $var9</td>
                                  <td><button data-popover='popover2' type='button' style='width:100%;' class='btn btn-primary .btn-flat' data-original-title='Informacion Adicional'
                                  data-content='<p>Medidor: $var10</p><p>Nombre: $var4</p><p>Direccion: $var5</p>' data-placement='left' data-trigger='hover'>$cliente</td>
                                </tr>";
                                    
                        
                       }
                  $lista.="</tbody>

                </table>              
              </div>

              </div>
              </div>

              </div>

            </div>
            </div>
            </div>

            <div class='content'>
			        <div class='block-flat'>
			            <div class='header'>
			              <h3>Mapa Distribucion Emergencias</h3>
			            </div>
						<iframe class='preview-pane' src='".FULL_URL."dashboard/mapa_emergencia' width='100%' height='650' frameborder='0' scrolling='si'></iframe>          
			        </div>
            </div>
          </div>        
        </div>



        
        </div>
      </div>
      ";

      echo $lista;

?>

<style>
input {
	width: 100%
}

.tabla2 {
	min-width: 100%
}
</style>