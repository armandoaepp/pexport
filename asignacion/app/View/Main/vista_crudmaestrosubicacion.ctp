<?php

date_default_timezone_set('America/Lima');
//require "../conexion.php";
require_once(APP."View/Main/conexion.ctp");

$querycomb1 = pg_query("select * from GlobalMaster_ConceptosUbicacion");

$query = pg_query("select 
GMU.IdUbicacion as IdUbicacion,
GMU.IdUbicacionConc as IdUbicacionConc,
GMU.NomUbicacion as NomUbicacion,
GMU.AbrUbicacion as AbrUbicacion,
GMU.IdConceptosUbicacion as IdConceptosUbicacion,
GMCU.NomConceptosUbicacion as NomConceptosUbicacion
 from GlobalMaster_Ubicacion GMU inner join
  GlobalMaster_ConceptosUbicacion GMCU on GMU.IdConceptosUbicacion = GMCU.IdConceptosUbicacion
");

$lista="
    <div class='row'>
      <div class='col-sm-12 col-md-12'>
        <div class='block-flat'>


        <div class='panel-group accordion accordion-semi' id='accordion4'>
					  <div class='panel panel-default'>
						<div class='panel-heading success'>
						  <h4 class='panel-title'>
							<a data-toggle='collapse' data-parent='#accordion4' href='#ac4-1'>
								<i class='fa fa-angle-right'></i> Agregar Nueva Ubicacion
							</a>
						  </h4>
						</div>
						<div id='ac4-1' class='panel-collapse collapse in'>
						  <div class='panel-body'>
							
						          <div class='content'>

							          <form role='form' id='formubicacion' method='post' style=width:60%;>
							          	<div class='form-group'>

							              <label>Codigo Concesionaria Ubicacion</label> <input type='text' id='codconcesionariaubicacion' name='codconcesionariaubicacion' placeholder='Codigo Concesionaria' class='form-control'>
							            </div> 
							            <div class='form-group'>
							              <input type='hidden' id='idubicacion' name='idubicacion'>
							              <label>Nombre Concepto Ubicacion</label> <input type='text' id='nameubicacion' name='nameubicacion' placeholder='Nombre de Ubicacion' class='form-control'>
							            </div>
							            <div class='form-group'> 
							              <label>Abreviacion Concepto Ubicacion</label> <input type='abreconceptoubi' id='abreconceptoubi' name='abreconceptoubi'  placeholder='Abreviacion de Ubicacion' class='form-control'>
							            </div> 
							            <div class='form-group'> 
							            ";

											 $lista.= " <label>Selecciona Tipo Ubicacion : </label> <select class='form-control' id='idconceptoubicacionubi' name='idconceptoubicacionubi'>";
														 while ($retorno = pg_fetch_object($querycomb1)){
															$lista.= "<option value='$retorno->idconceptosubicacion'>$retorno->nomconceptosubicacion</option>";
														}
											 $lista.= "</select>
											 </div> 



							              <button class='btn btn-danger' type='button' onclick='crudmaestrosubicacion(1)'>Grabar</button>
							              <button class='btn btn-default' type='button'>Cancelar</button>
							          </form>
						          
						          </div>

						  </div>
						</div>
					  </div>
		</div>

						      <div class='row'>
						        <div class='col-md-12'>
						          <div class='block-flat'>

						            

						            <div class='content'>
						            <div class='tabla'>

						          
						            <div class=''>

						              <div class='table-responsive'>
						              <form role='form' id='formubicacioncrud' method='post'> 
						                <table class='no-border' id='datatablecrudmaestrosubicacion'>

						                  <thead class='primary-emphasis'>
						                    <tr>
						                      <th></th>
						                      <th>Codigo Pexport</th>
						                      <th>Codigo Concesionaria</th>
						                      <th>Nombre Ubicacion</th>
						                      <th>Abreviatura Ubicacion</th>
						                      <th>Tipo Ubicacion</th>
						                    </tr>

						                  </thead>
						                  <tbody class='no-border'>";


						                    while ($retorno = pg_fetch_object($query)){

						                       $var=utf8_decode($retorno->idubicacion);
						                       $var1=utf8_decode($retorno->idubicacionconc);
						                       $var2=utf8_decode($retorno->nomubicacion);
						                       $var3=utf8_decode($retorno->abrubicacion);
						                       $var41=utf8_decode($retorno->idconceptosubicacion);
						                       $var4=utf8_decode($retorno->nomconceptosubicacion);

						                       $lista.= "<tr class='odd gradeX'>
						                                  <input type ='hidden' id='idubicacioncrud[]' name='idubicacioncrud[]' value='$var'>
						                                  <td style='width:1%;'><input type ='checkbox' id='idchkubicacioncrud[]' name='idchkubicacioncrud[]' value='$var' ></td>
						                                  <td style='width:10%;'>$var</td>
						                                  <td style='width:10%;'><input type='text' id='codconcesionariaubicacioncrud[]' name='codconcesionariaubicacioncrud[]'  value='$var1' class='form-control'></td>
						                                  <td style='width:20%;'><input type='text' id='nameubicacioncrud[]' name='nameubicacioncrud[]'  value='$var2' class='form-control'></td>
						                                  <td style='width:5%;'><input type='text' id='abrubicacioncrud[]' name='abrubicacioncrud[]'  value='$var3' class='form-control'></td>
						                                  <td style='width:15%;'><select class='form-control' id='idubicacionconceptoubicacioncrud[]' name='idubicacionconceptoubicacioncrud[]' value='$var3' >
						                                      <option value='$var41'>$var4</option>
						                                      ";
						                                  $querydet = pg_query("select * from GlobalMaster_ConceptosUbicacion CU  
						                                  where CU.IdConceptosUbicacion!=$var41");
						                                   while ($retornodet = pg_fetch_object($querydet)){
						                                    $idcon=$retornodet->idconceptosubicacion;
						                                    $nomcon=$retornodet->nomconceptosubicacion;

						                                    $lista.="<option value='$idcon'>$nomcon</option>"; 
						                                   } 
						                                  $lista.="</select></td>
						                                </tr>";
						                       }
						                  $lista.="</tbody>

						                </table>
						                <button class='btn btn-success' type='button' onclick='crudmaestrosubicacion(2)'>Grabar</button>
						                <button class='btn btn-danger' type='button' onclick='crudmaestrosubicacion(3)'>Eliminar</button>
						              </form>              
						              </div>

						              </div>
						              </div>
						              
						            </div>
						          </div>        
						        </div>

						      </div>

        </div>				
      </div>
      

	</div>";

echo $lista;

?>