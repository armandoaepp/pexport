<?php
date_default_timezone_set('America/Lima');
/*timer = setTimeout('auto_reload()',5000);*/

$lista="


      <div class='col-sm-4'>

        <div class='panel-group accordion accordion-semi' id='accordion4' style='margin-bottom:0px;'>
                      <div class='panel panel-default' style='margin-bottom:0px;'>
                        <div class='panel-heading success'>
                          <h4 class='panel-title'>
                            <a class='mi_tab_1' data-toggle='collapse' data-parent='#accordion4' href='#ac4-1'>
                                <i class='fa fa-angle-right'></i>1. Usa el filtro rapido
                            </a>
                          </h4>
                        </div>

                        <div id='ac4-1' class='panel-collapse collapse in'>
                          <div class='panel-body'>
                            <div class='personal'>
                                                  <h1 class='name'>Búsqueda Rápida</h1>

                                                  <form role='form' id='form-consulta-individual' method='post'>

                                                                <div class='form-group'>
                                                                  <label>Criterio de Busqueda:</label> </br>
                                                                  <select class='select2' id='cbo_opcion' name='cbo_opcion'>;
                                                                  <option value='Suministro'>Suministro</option>
                                                                  <option value='OT'>Orden de Trabajo</option>
                                                                  <option value='DNI'>DNI Cliente</option>
                                                                  <option value='TI'>Tipo de Inspeccion</option>
                                                                  </select>
                                                                </div>
                                                                <div class='form-group'>
                                                                  <label>Valor de Busqueda:</label><input type='text' id='txt_value' name='txt_value' placeholder='Escriba aqui' class='form-control' style='width:50%;'>
                                                                </div>
                                                                <button class='btn btn-success' type='button' onclick='BuscarConsulta()'>Buscar</button>

                                                </form>
                                                </div>
		
								<span class='loadinx1' style='display: none;'><img src='../js/jquery.select2/select2-spinner.gif'></span>
                                     



 
                          </div>
                      </div>

                      <div class='panel panel-default'>
                      <div class='panel-heading'>
                        <h4 class='panel-title'>
                        <a class='collapsed mi_tab_2' data-toggle='collapse' data-parent='#accordion3' href='#ac3-2'>
                          <i class='fa fa-angle-right'></i>2. Registros Encontrados
                        </a>
                        </h4>
                      </div>
                      <div id='ac3-2' class='panel-collapse collapse'>
                        <div class='panel-body'>
						<div id='result_items'>El resultado se mostrara aqu&iacute;.</div>
                        </div>
                      </div>
                      </div>


              </div>
        </div>
    </div>



    <div class='col-sm-8 col-md-8'>


        <div class='panel-group accordion accordion-semi' id='accordion6' style='margin-bottom:0px;'>
                      <div class='panel panel-default' style='margin-bottom:0px;'>
                        <div class='panel-heading success'>
                          <h4 class='panel-title'>
                            <a data-toggle='collapse' data-parent='#accordion6' href='#ac6-1'>
                                <i class='fa fa-angle-right'></i>3. Detalles Especificos
                            </a>
                          </h4>
                        </div>
                        <div id='ac6-1' class='panel-collapse collapse in'>
                          <div class='panel-body'>

                                     <div class='col-sm-12 col-md-12'>
                                        <div class='tab-container'>
                                          <ul class='nav nav-tabs'>
                                            <li class='active'><a href='#cronologia' data-toggle='tab'>Cronologia</a></li>
                                            <li><a href='#acta' data-toggle='tab'>Acta</a></li>
											<li><a id='tab_gps' href='#gps' data-toggle='tab'>GPS</a></li>
                                          </ul>
                                          <div class='tab-content'>

											<span class='loadinx2' style='display: none;'><img src='../js/jquery.select2/select2-spinner.gif'></span>
                                            <div class='tab-pane active cont' id='cronologia'>
                                                      
                                                      El resultado se mostrara aqu&iacute;.
                                                     
                       
                                             </div>

                                            <div class='tab-pane cont' id='acta'>
                                                
                                            	<iframe id='iframe_acta' class='preview-pane' src='about:blank' width='100%' height='700' frameborder='0'></iframe>
                                            		
                                            </div>
		
											<div class='tab-pane cont' id='gps'>
                                                
                                            	<iframe id='iframe_gps' class='preview-pane' src='about:blank' width='100%' height='500' frameborder='0'></iframe>
                                            		
                                            </div>


                                            </div>

                                          </div>
                                        </div>

                                        
                                      </div>  

                    
                                </div>
                            </div>
        </div>
        </div>
    ";

    echo $lista;

?>
<script>
function BuscarConsulta(){
	$('.loadinx1').show();
	$('#result_items').load(FULL_URL+'main/buscar_ordenes',$('#form-consulta-individual').serialize(),function(){
		$('.mi_tab_1').not('.collapsed').click();
		$('.mi_tab_2.collapsed').click();
		$('.items a').eq(1).click();
		$('.loadinx1').hide();
	});
}
$('#cbo_opcion').select2({
    width: '50%'
});
</script>