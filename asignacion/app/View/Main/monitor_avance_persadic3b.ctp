<?php
date_default_timezone_set('America/Lima');

//require_once "../conexion.php";
require_once(APP."View/Main/conexion.ctp");

$fechaactual=date("Y-m-d");
$mes=date("m");


if(isset($_GET["idini1"])) 
{
  $idini1=$_GET["idini1"];
} else {
  $idini1=$fechaactual;
}


if(isset($_GET["idfin1"])) 
{
  $idfin1=$_GET["idfin1"];
} else {
  $idfin1=$fechaactual;
}





$querymon3 = pg_query("
select Cuadrilla,
SubCuadrilla,
SUM(TotalAsignadas) as TotalAsignadas,
SUM(TotalDescargadas) as TotalDescargadas,
SUM(TotalExportadas) as TotalExportadas,
SUM(TotalFinDia) as TotalFinalDia,
SUM(TotalFinMes) as TotalFinalMes,
SUM(TotalFinalizadas) as TotalFinalizadas  from (
select 
Cuadrilla,
SubCuadrilla,
TotalAsignadas,
TotalDescargadas,
TotalExportadas,
TotalFinDia,
TotalFinMes,
TotalFinalizadas from (
select IdSector,Sector,
SUM(TotalAsignadas) as TotalAsignadas,
SUM(TotalDescargadas) as TotalDescargadas,
SUM(TotalExportadas) as TotalExportadas,
SUM(TotalFinalizadasDia) as TotalFinDia,
SUM(TotalFinalizadasMes) as TotalFinMes,
SUM(TotalFinalizadas) as TotalFinalizadas from (
select  
co.IdSector as IdSector,
co.Sector as Sector,
(case when ci.IdEstado = 13 and co.Condicion='0' and co.SubActividad
in (select DetalleTipoIntervencion from ComMovTab_TipoIntervencion)
or ci.IdEstado in (select IdEstado from ComMovTab_MaestrosEstado where EstadoSup=13) and co.Condicion='0' and co.SubActividad 
in (select DetalleTipoIntervencion from ComMovTab_TipoIntervencion)
then 1 else 0 end) as TotalDescargadas,
(case when ci.IdEstado = 5 and co.Condicion='0' and co.SubActividad 
in (select DetalleTipoIntervencion from ComMovTab_TipoIntervencion)
then 1 else 0 end) as TotalAsignadas,
(case when ci.IdEstado = 14 and co.Condicion='0' and co.SubActividad 
in (select DetalleTipoIntervencion from ComMovTab_TipoIntervencion)
then 1 else 0 end) as TotalExportadas,
(case when ci.IdEstado = 15 and cast(ci.FechaAtencion as date)='$idfin1' and co.Condicion='0' and co.SubActividad 
in (select DetalleTipoIntervencion from ComMovTab_TipoIntervencion)
then 1 else 0 end) as TotalFinalizadasDia,
(case when ci.IdEstado = 15 and extract('month' from ci.FechaAtencion)=$mes and co.Condicion='0' and co.SubActividad 
in (select DetalleTipoIntervencion from ComMovTab_TipoIntervencion)
then 1 else 0 end) as TotalFinalizadasMes,
(case when ci.IdEstado = 15 and co.Condicion='0' and co.SubActividad 
in (select DetalleTipoIntervencion from ComMovTab_TipoIntervencion)
then 1 else 0 end) as TotalFinalizadas
from ComMovTab_Orden co 
inner join ComMovTab_Intervencion ci on co.IdOrden = ci.IdOrden
where co.Condicion='0' and co.SubActividad 
in (select DetalleTipoIntervencion from ComMovTab_TipoIntervencion)) as superconsulta
group by IdSector,Sector) as setfinal
inner join (
select
gu.IdUbicacionConc as IdConcesionaria,
case when (select IdConceptosUbicacion from GlobalMaster_Ubicacion where IdUbicacion =dr2.IdUbicacionSup)=8
or (select IdConceptosUbicacion from GlobalMaster_Ubicacion where IdUbicacion =dr2.IdUbicacionSup)=7  
then gu3.NomUbicacion else gu2.NomUbicacion end as Cuadrilla,
gu2.NomUbicacion as SubCuadrilla,
gu.NomUbicacion as Sector
from GlobalMaster_DetalleUbicacionRegla dr
inner join GlobalMaster_DetalleUbicacionRegla dr2 on dr.IdUbicacionSup = dr2.IdUbicacion
inner join GlobalMaster_Ubicacion gu on dr.IdUbicacion = gu.IdUbicacion
inner join GlobalMaster_Ubicacion gu2 on dr.IdUbicacionSup = gu2.IdUbicacion
inner join GlobalMaster_Ubicacion gu3 on dr2.IdUbicacionSup = gu3.IdUbicacion
where gu.IdConceptosUbicacion = 5 ) as setfinal2 on
setfinal.IdSector=cast(setfinal2.IdConcesionaria as integer)) as Cuadrilla
group by Cuadrilla,SubCuadrilla
");







											$pers3b="<div class='pers3'>
                      <div class='table-responsive'>
                                                      <table class='table table-bordered tableWithFloatingHeader' id='datatablepers3b' >
                                                      <thead class='primary-emphasis'>
                                                      <tr>
                                                          <th class='text-center primary-emphasis-dark'>Cuadrilla</th>
                                                          <th class='text-center primary-emphasis-dark'>SubCuadrilla</th>
                                                          <th class='text-center primary-emphasis-dark'>Asignadas</th>
                                                          <th class='text-center primary-emphasis-dark'>Descargadas</th>
                                                          <th class='text-center primary-emphasis-dark'>Exportadas</th>
                                                          <th class='text-center primary-emphasis-dark'>Finalizadas (Dia)</th>
                                                          <th class='text-center primary-emphasis-dark'>Finalizadas (Mes)</th>
                                                          <th class='text-center primary-emphasis-dark'>Finalizadas</th>
                                                          <th class='text-center primary-emphasis-dark'>% Avance</th>
                                                          <th class='text-center primary-emphasis-dark'>Total Pendientes</th>
                                                      </tr>
                                                      </thead>
                                                       <tbody>";
                                                        while ($retornocom3 = pg_fetch_object($querymon3)){
                                                                            $varcuadrilla=$retornocom3->cuadrilla;
                                                                            $varsubcuadrilla=$retornocom3->subcuadrilla;
                                                                            $varcuaasignada=$retornocom3->totalasignadas;
                                                                            $varcuadescargada=$retornocom3->totaldescargadas;
                                                                            $varcuaexportada=$retornocom3->totalexportadas;

                                                                            $varcuafinalizadadia=$retornocom3->totalfinaldia;
                                                                            $varcuafinalizadames=$retornocom3->totalfinalmes;

                                                                            $varcuafinalizada=$retornocom3->totalfinalizadas;
                                                                            $varcuapendientes=$varcuaasignada+$varcuadescargada+$varcuaexportada;
                                                                     

                                                                             if ($varcuafinalizada==0){
                                                                              $varcuaporcentaje=0;
                                                                            }
                                                                            else {
                                                                              $varcuaporcentaje=($varcuafinalizada*100)/($varcuapendientes+$varcuafinalizada);
                                                                              $varcuaporcentaje=round($varcuaporcentaje,2);
                                                                            }

                                                                            $totalcuaasigv[]=$varcuaasignada;
                                                                            $totalcuadesv[]=$varcuadescargada;
                                                                            $totalcuaexpov[]=$varcuaexportada;
                                                                            $totalcuafindiav[]=$varcuafinalizadadia;
                                                                            $totalcuafinmesv[]=$varcuafinalizadames;
                                                                            $totalcuafinv[]=$varcuafinalizada;

                                                                            $totalcuapenv[]=$varcuapendientes;

                                                                            if ($varcuaporcentaje < 33) {
                                                                              $color3='danger';
                                                                              
                                                                            } elseif ($varcuaporcentaje > 33 && $varcuaporcentaje < 70) {
                                                                              $color3='warning';
                                                                            } else {
                                                                              $color3='success';
                                                                            }

                                                                     

                                                        $pers3b.= "
                                                        <tr class='odd gradeX'>
                                                          <td><strong>$varcuadrilla</strong></td>
                                                          <td><strong>$varsubcuadrilla</strong></td>
                                                          <td style='color:red;'><strong>$varcuaasignada</strong></td>
                                                          <td style='color:red;'><a href='#' data-toggle='modal' data-target='#mod-success2' onclick=\"showModalAsignarSector(1,2)\"><strong>$varcuadescargada</strong></a> </td>
                                                          <td style='color:red;'><strong>$varcuaexportada</strong></td>
                                                          <td style='color:blue;'><strong>$varcuafinalizadadia</strong></td>
                                                          <td style='color:blue;'><strong>$varcuafinalizadames</strong></td>
                                                          <td style='color:blue;'><strong>$varcuafinalizada</strong></td>
                                                          <td class='center'><div class='progress progress-striped active'>
                                                                                  <div class='progress-bar progress-bar-$color3' style='width: $varcuaporcentaje%'>".number_format($varcuaporcentaje,2)."%</div>
                                                                                 </div></td>
                                                         
                                                          <td style='color:red;'><strong>$varcuapendientes</strong></td>

                                                        </tr>";

                                                        } 

                                                         $pers3b.= "</tbody>


                                                      <tfoot >
                                                        <tr>";

                                                        $totalcuaasig=array_sum($totalcuaasigv);
                                                        $totalcuades=array_sum($totalcuadesv);
                                                        $totalcuaexp=array_sum($totalcuaexpov);
                                                        $totalcuafindia=array_sum($totalcuafindiav);
                                                        $totalcuafinmes=array_sum($totalcuafinmesv);
                                                        $totalcuafin=array_sum($totalcuafinv);
                                                        $totalcuapen=array_sum($totalcuapenv);

                                                          $pers3b.= "<td style='text-align:right;' colspan='2'><strong>Total</strong></td>
                                                          <td><strong>$totalcuaasig</strong></td>
                                                          <td><strong>$totalcuades</strong></td>
                                                          <td><strong>$totalcuaexp</strong></td>
                                                          <td><strong>$totalcuafindia</strong></td>
                                                          <td><strong>$totalcuafinmes</strong></td>
                                                          <td><strong>$totalcuafin</strong></td>
                                                          <td><strong></strong></td>
                                                          <td><strong>$totalcuapen</strong></td>
                                                        </tr>
                                                      </tfoot>


                                                      </table>
                                              </div></div>";


                                              echo $pers3b;

?>

  <script type="text/javascript">

        function UpdateTableHeaders() {
            $("div.divTableWithFloatingHeader").each(function() {
                var originalHeaderRow = $(".tableFloatingHeaderOriginal", this);
                var floatingHeaderRow = $(".tableFloatingHeader", this);
                var offset = $(this).offset();
                var scrollTop = $(window).scrollTop();

                var navegador = navigator.userAgent;
                var inicioScroll = scrollTop-30;
                var marginheader = 30;
                if (navigator.userAgent.indexOf('MSIE') !=-1) {
                 
                } else if (navigator.userAgent.indexOf('Firefox') !=-1) {
                  inicioScroll = scrollTop-50;
                  marginheader = 40;
                } else if (navigator.userAgent.indexOf('Chrome') !=-1) {
                  inicioScroll = scrollTop-30;
                  var marginheader = 50;
                } else if (navigator.userAgent.indexOf('Opera') !=-1) {
                  inicioScroll = scrollTop-30;
                  marginheader = 40;
                } else {
                
                }

                if (( inicioScroll > offset.top ) && (scrollTop < offset.top + $(this).height())) {
                    floatingHeaderRow.css("visibility", "visible");
                    floatingHeaderRow.css("top", (Math.min(scrollTop - offset.top, $(this).height() - floatingHeaderRow.height()))+ marginheader  + "px");

                    // Copy cell widths from original header
                    $("th", floatingHeaderRow).each(function(index) {
                        var cellWidth = $("th", originalHeaderRow).eq(index).css('width');
                        var color = $("th", originalHeaderRow).eq(index).css('background-color');
                        
                        $(this).css('width', cellWidth);
                        $(this).css('background-color', color);
                    });

                    // Copy row width from whole table
                    floatingHeaderRow.css("width", $(this).css("width"));
                    floatingHeaderRow.css("background-color", $(this).css("background-color"));
                }
                else {
                    floatingHeaderRow.css("visibility", "hidden");
                    floatingHeaderRow.css("top", "0px");
                    
                }
            });
        }

        $(document).ready(function() {
            $("table.tableWithFloatingHeader").each(function() {
                $(this).wrap("<div class=\"divTableWithFloatingHeader\" style=\"position:relative\"></div>");

                var originalHeaderRow = $("tr:first", this)
                originalHeaderRow.before(originalHeaderRow.clone());
                var clonedHeaderRow = $("tr:first", this)

                clonedHeaderRow.addClass("tableFloatingHeader");
                clonedHeaderRow.css("position", "absolute");
                clonedHeaderRow.css("top", "0px");
                clonedHeaderRow.css("left", $(this).css("margin-left"));
                clonedHeaderRow.css("visibility", "hidden");
                clonedHeaderRow.css("z-index", "5");
                
                originalHeaderRow.addClass("tableFloatingHeaderOriginal");
            });

            UpdateTableHeaders();
            $(window).scroll(UpdateTableHeaders);
            $(window).resize(UpdateTableHeaders);
        });
</script>