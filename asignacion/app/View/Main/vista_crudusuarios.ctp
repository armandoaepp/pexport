<?php

date_default_timezone_set('America/Lima');
//require "../conexion.php";
require_once(APP."View/Main/conexion.ctp");

$query2 = pg_query("select * from GlobalMaster_Persona");


$query = pg_query("select 
CU.IdUsuario as IdUsuario,
CU.NomUsuario as NomUsuario,
CU.PassUsuario as PassUsuario,
CU.ApiKey as ApiKey,
CU.IdEstado as IdEstado,
CU.IdPersona as IdPersona,
GMP.NomPersona as NomPersona,
ME.NomEstado as NomEstado
from ComMovTab_Usuario CU
inner join ComMovTab_MaestrosEstado ME on CU.IdEstado = ME.IdEstado
left join GlobalMaster_Persona GMP on CU.IdPersona = GMP.IdPersona order by CU.IdUsuario asc");


$lista="
    <div class='row'>
      <div class='col-sm-12 col-md-12'>
        <div class='block-flat'>


		<div class='panel-group accordion accordion-semi' id='accordion4'>
							  <div class='panel panel-default'>
								<div class='panel-heading success'>
								  <h4 class='panel-title'>
									<a data-toggle='collapse' data-parent='#accordion4' href='#ac4-1'>
										<i class='fa fa-angle-right'></i> Agregar Nuevo Usuario
									</a>
								  </h4>
								</div>
								<div id='ac4-1' class='panel-collapse collapse in'>
								  <div class='panel-body'>
									
								          <div class='content'>

									          <form role='form' id='formusuario' method='post' style='width:60%;'>

									          	<div class='form-group'>";

								                $lista.= "<label>Busca Persona:</label> </br>
								                <select class='select2' id='idpersona' name='idpersona'><option value='0'>Todos</option>";
								                while ($retorno = pg_fetch_object($query2)){
															$lista.= "<option value='$retorno->idpersona'>$retorno->nompersona  $retorno->apepersona</option>";
														}
								                $lista.= "</select>
								                </div>

									          

									            <div class='form-group'>
									              <input type='hidden' id='idusuario' name='idusuario'>
									              <label>Nombre Usuario</label> <input type='text' id='nameusuario' name='nameusuario' placeholder='Nombre de Usuario' class='form-control'>
									            </div>
									            <div class='form-group'> 
									              <label>Contraseña</label> <input type='password' id='password' name='password'  placeholder='Contraseña' class='form-control'>
									            </div> 
									              <button class='btn btn-danger' type='button' onclick='crudusuario(1)'>Grabar</button>
									              <button class='btn btn-default' type='button'>Cancelar</button>
									          </form>
								          
								          </div>

								  </div>
								</div>
							  </div>
				</div>

          
          
			      <div class='row'>
			        <div class='col-md-12'>
			          <div class='block-flat'>

			            

			            <div class='content'>
			            <div class='tabla5'>

			          
			            <div class=''>
			            <div style='z-index:10000;'>
			              <div class='table-responsive'>
			              <form role='form' id='formusuariocrud' method='post'> 
			                <table class='no-border' id='datatablecrudusuarios'>

			                  <thead class='primary-emphasis'>
			                    <tr>
			                      <th></th>
			                      <th>Codigo</th>
			                      <th>Usuario</th>
			                      <th>Password</th>
			                      <th>Persona</th>
			                      <th>Token</th>
			                      <th>Estado</th>
			                    </tr>

			                  </thead>
			                  <tbody class='no-border'>";


			                    while ($retorno = pg_fetch_object($query)){

			                       $var=utf8_decode($retorno->idusuario);
			                       $var1=utf8_decode($retorno->nomusuario);
			                       $var2=$retorno->passusuario;
			                       $var55=$retorno->idpersona;
			                       $var5=utf8_decode($retorno->nompersona);
			                       $var4=utf8_decode($retorno->apikey);
			                       $var3=utf8_decode($retorno->nomestado); 
			                       $var31=utf8_decode($retorno->idestado); 

			                       $lista.= "<tr class='odd gradeX'>

			                                  

			                                  <input type ='hidden' id='idusu[]' name='idusu[]' value='$var'>
			                                  <td style='width:1%;'><input type ='checkbox' id='idchk[]' name='idchk[]' value='$var' ></td>
			                                  <td style='width:1%;'>$var</td>
			                                  <td style='width:20%;'><input type='text' id='nameusuario[]' name='nameusuario[]'  value='$var1' class='form-control'></td>
			                                  <td style='width:30%;'><input readonly type='text' id='password[]' name='password[]'  value='$var2' class='form-control'></td>
			                                  
			                                  <td style='width:10%;'><select class='form-control' id='idpersonacrud[]' name='idpersonacrud[]' value='$var5' >
			                                      <option value='$var55'>$var5</option>
			                                      ";
			                                  $querydetper = pg_query("select * from GlobalMaster_Persona where IdPersona!=$var55");
			                                   while ($retornodetper = pg_fetch_object($querydetper)){
			                                    $idestper=$retornodetper->idpersona;
			                                    $nomestper=$retornodetper->nompersona;
			                                    $apeestper=$retornodetper->apepersona;
			                                    $lista.="<option value='$idestper'>$nomestper $apeestper</option>"; 
			                                   } 
			                                  $lista.="</select></td>


			                                  <td style='width:30%;'><input type='text' id='apikey[]' name='apikey[]'  value='$var4' class='form-control'></td>
			                                  <td style='width:10%;'><select  class='form-control'  id='estado[]' name='estado[]' value='$var3' >
			                                      <option value='$var31'>$var3</option>
			                                      ";
			                                  $querydet = pg_query("select ME.IdEstado as IdEstado,NomEstado as NomEstado from ComMovTab_MaestrosEstado ME  
			                                  where ME.EstadoSup = 2 and ME.IdEstado!=$var31");
			                                   while ($retornodet = pg_fetch_object($querydet)){
			                                    $idest=$retornodet->idestado;
			                                    $nomest=$retornodet->nomestado;

			                                    $lista.="<option value='$idest'>$nomest</option>"; 
			                                   } 
			                                  $lista.="</select></td>
			                                </tr>";
			                       }
			                  $lista.="</tbody>

			                </table>
			                <button class='btn btn-success' type='button' onclick='crudusuario(2)'>Grabar</button>
			                <button class='btn btn-danger' type='button' onclick='crudusuario(3)'>Eliminar</button>
			                <button class='btn btn-default' type='button' onclick='showUsu1()'>Nuevo</button>
			              </form>              
			              </div>
			              </div>
			              </div>
			              </div>
			              
			            </div>
			          </div>        
			        </div>

			      </div>

        </div>				
      </div>
      

	</div>";

echo $lista;

?>