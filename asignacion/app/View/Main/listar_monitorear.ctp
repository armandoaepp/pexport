<?php
date_default_timezone_set('America/Lima');
//require_once "../conexion.php";
require_once(APP."View/Main/conexion.ctp");


function tiempo($str, $t1, $t2 = false) {
	// Si nos dan un segundo parámetros calculamos el tiempo entre dos fechas
	$t = $t1 - ($t2 ? $t2 : time ());
	
	// Un array con todos los reemplazos que vamos a usar
	$p = array (
			'{s}' => 1,
			'{i}' => 60,
			'{h}' => 60 * 60,
			'{d}' => 60 * 60 * 24,
			'{w}' => 60 * 60 * 24 * 7,
			'{m}' => 60 * 60 * 24 * 30,
			'{y}' => 60 * 60 * 24 * 365 
	);
	
	// Obtenemos todos los tiempos que fueron proveídos en la string
	preg_match_all ( "/\{[sihdwmy]\}/", $str, $ma );
	
	// Creamos un array ordenado del mayor al menor tiempo requerido
	$found = Array ();
	foreach ( $ma [0] as &$m ) {
		$found [$m] = $p [$m];
	}
	arsort ( $found );
	
	// Reemplazamos la string con los tiempos
	foreach ( $found as $i => &$fo ) {
		$str = str_replace ( $i, ( int ) ($t / $fo), $str );
		$t = $t % $fo;
	}
	
	return $str;
}

$str_where = " ";
$str_where2 = " ";
$str_fecha_liquidacion = "";

if(isset($_GET['criterios'])) {
	$criterio = $_GET['criterios'];
}else{
	$criterio = '';
	$str_where2 .= " AND NomEstado in ('Asignado','Descargado','Exportado')";
}

if(strpos($criterio, 'Estado') !== false){
	if(isset($_GET['tags']) && $_GET['tags']!='') {
		$str_estados = "'".str_replace(",","','",$_GET['tags'])."'";
		if(strpos($str_estados, 'Descargado') !== false){
			$str_where2 .= " AND (NomEstado in (".$str_estados.") OR CI.IdEstado=9)";
		}else if(strpos($str_estados, 'Finalizado') !== false){
			$str_where2 .= " AND (NomEstado in (".$str_estados.") AND CI.IdEstado!=9)";
		}else{
			$str_where2 .= " AND (NomEstado in (".$str_estados."))";
		}	
	}
}

if(strpos($criterio, 'Ubicacion') !== false){
	if(isset($_GET['unidadneg']) && $_GET['unidadneg']!='0') {
		$str_where .= " AND IdUnidadNegocio = ".$_GET['unidadneg'];
		$str_where2 .= " AND IdUnidadNegocio = ".$_GET['unidadneg'];
	}
	if(isset($_GET['centroservicio']) && $_GET['centroservicio']!='0') {
		$str_where .= " AND IdCentroServicio = ".$_GET['centroservicio'];
		$str_where2 .= " AND IdCentroServicio = ".$_GET['centroservicio'];
	}
}
if(strpos($criterio, 'PeriodoLiquidacion') !== false){
	if(isset($_GET['periodoliquidacion']) && $_GET['periodoliquidacion']!='' && $_GET['periodoliquidacion']!='0') {
		$arr_periodo = explode('-',$_GET['periodoliquidacion']);
		$str_fecha_liquidacion = " AND extract('year' from FechaAtencion) = ".$arr_periodo[0];
		$str_fecha_liquidacion .= " AND extract('month' from FechaAtencion) = ".$arr_periodo[1];
		$str_where2 .= $str_fecha_liquidacion;
	}
}
if(strpos($criterio, 'RangoFechaA') !== false){
	if(isset($_GET['dpt_date_start'])) {
		$str_where2 .= " AND FechaAsignado >= '".$_GET['dpt_date_start']."'";
	}
	if(isset($_GET['dpt_date_end'])) {
		$str_where2 .= " AND FechaAsignado <= '".$_GET['dpt_date_end']." 23:59:59'";
	}
}
if(strpos($criterio, 'RangoFechaF') !== false){
	if(isset($_GET['dpt_date_start_finalizado'])) {
		$str_where2 .= " AND FechaAtencion >= '".$_GET['dpt_date_start_finalizado']."'";
	}
	if(isset($_GET['dpt_date_end_finalizado'])) {
		$str_where2 .= " AND FechaAtencion <= '".$_GET['dpt_date_end_finalizado']." 23:59:59'";
	}
}
if(strpos($criterio, 'Suministro') !== false){
	if(isset($_GET['txt_suministro']) && $_GET['txt_suministro']!='') {
		$str_where2 .= " AND suministro = ".$_GET['txt_suministro']."::TEXT";
	}
}
if(strpos($criterio, 'OT') !== false){
	if(isset($_GET['txt_orden_trabajo']) && $_GET['txt_orden_trabajo']!='') {
		$str_where2 .= " AND co.IdOrdenTrabajo = '".$_GET['txt_orden_trabajo']."'";
	}
}
if(strpos($criterio, 'FH') !== false){
	$str_fecha_finalizado_hoy = " AND FechaAtencion >= '".date('Y-m-d')." 00:00:00'";
	$str_where2 .= $str_fecha_finalizado_hoy." AND NomEstado in ('Finalizado')";
}
if(strpos($criterio, 'GH') !== false){
	$str_where2 .= "AND FechaGeneracion >= '".date('Y-m-d')." 00:00:00'";
}
if(strpos($criterio, 'Tecnico') !== false){
	if(isset($_GET['tecnico']) && $_GET['tecnico']!='') {
		$str_where2 .= " AND ci.IdUsuario = ".$_GET['tecnico'];
	}
}
if(strpos($criterio, 'I') !== false){
	$str_where2 .= " AND (select count(*) as infructuosa from ComMovTab_Detalle_Intervencion di inner join
ComMovTab_MaestrosIntervencion mi on di.IdMaestrosIntervencion=mi.IdMaestrosIntervencion
where di.IdIntervencion=Ci.IdIntervencion and  IdentificadorMaestroIntervencion like 'LabelInfructuosa%') > 0 ";
}
if(strpos($criterio, 'TT') !== false){
	if(isset($_GET['cbo_tiempotranscurrido']) && $_GET['cbo_tiempotranscurrido']!='') {
		$str_where2 .= " AND DATE_PART('day',NOW()-cast(FechaGeneracion as timestamp)) > ".$_GET['cbo_tiempotranscurrido'];
	}
}


/*
if(isset($_GET['unidadneg'])) {
	$unidad=$_GET['unidadneg'];
	
	
  if ($unidad == 0){
    $unidad="26,30,29";
  }else{
    $unidad=$unidad;
  }
}else{
$unidad="26,30,29";
}


if(isset($_GET['centroservicio'])) {
$centroser=$_GET['centroservicio'];

  if ($centroser == 0){
    $centroser="258,272,255,273,270,271,251,268";
  }else{
    $centroser=$centroser;
  }
}else{
$centroser="258,272,255,273,270,271,251,268";
}



if(isset($_GET['tags'])) {
$vartags=$_GET['tags'];
$tangente=$_GET['tags'];

$porciones = explode(",", $vartags);
$c=count($porciones);

$conocimiento=array("Asignado", "Exportado", "Descargado", "Finalizado");
$ori='';
$porcion='';
for ($i=0; $i < $c; $i++) {
  if (in_array($porciones[$i], $conocimiento)) {
     $ori.=",".$porciones[$i];
     $porcion.=",'".$porciones[$i]."'";
  } else {
    echo "";
  }
}

$ori = substr($ori, 1);
$vartags = substr($porcion, 1);


if ($vartags==''){
  $vartags="'Asignado','Exportado','Descargado'";
  $origin="Asignado,Exportado,Descargado";
}else{
  /*
  $antes = array("Finalizado","Asignado","Exportado","Descargado");
  $despues   = array("'Finalizado'","'Asignado'","'Exportado'","'Descargado'");
  $vartags=str_replace($antes,$despues,$vartags);
  */
/*
  $vartags=$vartags;
  $origin=$ori;
}



}else{
$vartags="'Asignado','Exportado','Descargado'";
$origin="Asignado,Exportado,Descargado";
}


if(isset($_GET['periodoliquidacion'])) {
$mes=$_GET['periodoliquidacion'];

    if ($mes !='0' && $tangente=='Finalizado'){
      $agree1='and month(CI.FechaAtencion) = '.$mes;
    } else
    {
      $agree1='';
    }

}
else {
$agree1='';
}
*/

if(isset($_GET['ida']))
{


     $id=$_GET["ida"];

     if ($id==100){
      $flage="display:none;";
      $flaga="";
      $lsubactividad=1;



      $query = pg_query($conexion, "select
      CO.IdOrden as IdOrden,
      CI.IdIntervencion as IdIntervencion,
      CO.IdUnidadNegocio as IdUnidadNegocio,
      CO.UnidadNegocio as UnidadNegocio,
      CO.IdCentroServicio as IdCentroServicio,
      CO.SubActividad as SubActividad,
      TIS.IdTipoIntervencion as TipoIntervencion,
      CI.IdEstado as IdEstado,
      cast(CO.FechaGeneracion as timestamp) as NuevaFecha,
      ME.NomEstado as NomEstado,
      GMP.NomPersona as NomPersona,
      GMP.ApePersona as ApePersona,
      CI.IdUsuario as IdUsuario,
      CU.NomUsuario as NomUsuario,
      CO.IdOrdenTrabajo as IdOrdenTrabajo,
      CO.Observacion as Observacion,
      CO.NroAtencion as NroAtencion,
      CI.FechaAtencion as FechaAtencion,
      CI.FechaAtencion as FechaAtencion1,
      CO.NroReclamo as NroReclamo,
      CO.Suministro as Suministro,
      CO.UsuarioGenero as UsuarioGenero,
      CO.FechaGeneracion as FechaGeneracion,
      CO.NombreSuministro as NombreSuministro,
      CO.DireccionSuministro as DireccionSuministro,
      CO.SerieMedidor as SerieMedidor,
      CO.Sector as Sector,
      0 as infructuosa,
      date_part('month',case when CI.IdEstado in (14,15) then age(CI.FechaAtencion,cast(CO.FechaGeneracion as timestamp)) else age(NOW(), cast(CO.FechaGeneracion as timestamp)) end) as tiempotranscurridomes,
      date_part('day',case when CI.IdEstado in (14,15) then age(CI.FechaAtencion,cast(CO.FechaGeneracion as timestamp)) else age(NOW(), cast(CO.FechaGeneracion as timestamp)) end) as tiempotranscurridodia,
      date_part('hour',case when CI.IdEstado in (14,15) then age(CI.FechaAtencion,cast(CO.FechaGeneracion as timestamp)) else age(NOW(), cast(CO.FechaGeneracion as timestamp)) end) as tiempotranscurridohora
      from ComMovTab_Intervencion CI
      join ComMovTab_Orden CO on CI.IdOrden = CO.IdOrden
      left outer join ComMovTab_TipoIntervencion TIS on CO.SubActividad=TIS.DetalleTipoIntervencion
      join ComMovTab_Documento CD on CO.IdDocumento = CD.IdDocumento
      left outer join ComMovTab_TipoIntervencion TI on CD.IdTipoIntervencion = TI.IdTipoIntervencion
      join ComMovTab_MaestrosEstado ME on CI.IdEstado = ME.IdEstado
      join ComMovTab_Usuario CU on CI.IdUsuario = CU.IdUsuario
      join GlobalMaster_Persona GMP on CU.IdPersona = GMP.IdPersona
      where
	  co.SubActividad not in (select DetalleTipoIntervencion from ComMovTab_TipoIntervencion)
	  and co.UsuarioGenero not like 'Pexport%'
	  and co.Condicion = '0'
	  and co.FechaGeneracion > '2014-10-01 16:23:19' ".$str_where." order by case when CI.IdEstado in (14,15) then age(CI.FechaAtencion,cast(CO.FechaGeneracion as timestamp)) else age(NOW(), cast(CO.FechaGeneracion as timestamp)) end desc");

     } else {

     $flage='';
     $flaga="display:none;";
     $lsubactividad=0;
     $query = pg_query("select
      CO.IdOrden as IdOrden,
      CI.IdIntervencion as IdIntervencion,
      CO.SubActividad as SubActividad,
      CO.IdUnidadNegocio as IdUnidadNegocio,
      CO.UnidadNegocio as UnidadNegocio,
      CO.IdCentroServicio as IdCentroServicio,
      TIS.IdTipoIntervencion as TipoIntervencion,
      CI.IdEstado as IdEstado,
      cast(CO.FechaGeneracion as timestamp) as NuevaFecha,
      ME.NomEstado as NomEstado,
      GMP.NomPersona as NomPersona,
      GMP.ApePersona as ApePersona,
      CI.IdUsuario as IdUsuario,
      CU.NomUsuario as NomUsuario,
      CO.IdOrdenTrabajo as IdOrdenTrabajo,
      CO.Observacion as Observacion,
      CO.NroAtencion as NroAtencion,
      CI.FechaAtencion as FechaAtencion1,
      cast(CI.FechaAtencion as varchar)as FechaAtencion,
      CO.NroReclamo as NroReclamo,
      CO.Suministro as Suministro,
      CO.UsuarioGenero as UsuarioGenero,
      CO.FechaGeneracion as FechaGeneracion,
      CO.NombreSuministro as NombreSuministro,
      CO.DireccionSuministro as DireccionSuministro,
      CO.SerieMedidor as SerieMedidor,
      CO.Sector as Sector,
      (select count(*) as infructuosa from ComMovTab_Detalle_Intervencion di inner join 
      ComMovTab_MaestrosIntervencion mi on di.IdMaestrosIntervencion=mi.IdMaestrosIntervencion
      where di.IdIntervencion=Ci.IdIntervencion and  IdentificadorMaestroIntervencion like 'LabelInfructuosa%') as infructuosa,
      date_part('month',case when CI.IdEstado in (14,15) then age(CI.FechaAtencion,cast(CO.FechaGeneracion as timestamp)) else age(NOW(), cast(CO.FechaGeneracion as timestamp)) end) as tiempotranscurridomes,
      date_part('day',case when CI.IdEstado in (14,15) then age(CI.FechaAtencion,cast(CO.FechaGeneracion as timestamp)) else age(NOW(), cast(CO.FechaGeneracion as timestamp)) end) as tiempotranscurridodia,
      date_part('hour',case when CI.IdEstado in (14,15) then age(CI.FechaAtencion,cast(CO.FechaGeneracion as timestamp)) else age(NOW(), cast(CO.FechaGeneracion as timestamp)) end) as tiempotranscurridohora
      from ComMovTab_Intervencion CI
      join ComMovTab_Orden CO on CI.IdOrden = CO.IdOrden
      join ComMovTab_TipoIntervencion TIS on CO.SubActividad=TIS.DetalleTipoIntervencion
      join ComMovTab_Documento CD on CO.IdDocumento = CD.IdDocumento
      join ComMovTab_TipoIntervencion TI on CD.IdTipoIntervencion = TI.IdTipoIntervencion
      join ComMovTab_MaestrosEstado ME on CI.IdEstado = ME.IdEstado
      join ComMovTab_Usuario CU on CI.IdUsuario = CU.IdUsuario
      join GlobalMaster_Persona GMP on CU.IdPersona = GMP.IdPersona
      where co.Condicion = '0' and co.UsuarioGenero not like 'Pexport%' and (TIS.IdTipoIntervencion=$id or TI.IdTipoIntervencion=$id) ".$str_where2."  order by case when CI.IdEstado in (14,15) then age(CI.FechaAtencion,cast(CO.FechaGeneracion as timestamp)) else age(NOW(), cast(CO.FechaGeneracion as timestamp)) end desc");
   
      }

}
else
{
     $id=100;
      $query = pg_query($conexion, "select
      CO.IdOrden as IdOrden,
      CI.IdIntervencion as IdIntervencion,
      CO.SubActividad as SubActividad,
      CO.IdUnidadNegocio as IdUnidadNegocio,
      CO.UnidadNegocio as UnidadNegocio,
      CO.IdCentroServicio as IdCentroServicio,
      TIS.IdTipoIntervencion as TipoIntervencion,
      CI.IdEstado as IdEstado,
      cast(CO.FechaGeneracion as timestamp) as NuevaFecha,
      ME.NomEstado as NomEstado,
      GMP.NomPersona as NomPersona,
      GMP.ApePersona as ApePersona,
      CI.IdUsuario as IdUsuario,
      CU.NomUsuario as NomUsuario,
      CO.IdOrdenTrabajo as IdOrdenTrabajo,
      CO.Observacion as Observacion,
      CO.NroAtencion as NroAtencion,
      CI.FechaAtencion as FechaAtencion,
      CI.FechaAtencion as FechaAtencion1,
      CO.NroReclamo as NroReclamo,
      CO.Suministro as Suministro,
      CO.UsuarioGenero as UsuarioGenero,
      CO.FechaGeneracion as FechaGeneracion,
      CO.NombreSuministro as NombreSuministro,
      CO.DireccionSuministro as DireccionSuministro,
      CO.SerieMedidor as SerieMedidor,
      CO.Sector as Sector,
      (select count(*) as infructuosa from ComMovTab_Detalle_Intervencion di inner join 
      ComMovTab_MaestrosIntervencion mi on di.IdMaestrosIntervencion=mi.IdMaestrosIntervencion
      where di.IdIntervencion=Ci.IdIntervencion and  IdentificadorMaestroIntervencion like 'LabelInfructuosa%') as infructuosa,
      date_part('month',case when CI.IdEstado in (14,15) then age(CI.FechaAtencion,cast(CO.FechaGeneracion as timestamp)) else age(NOW(), cast(CO.FechaGeneracion as timestamp)) end) as tiempotranscurridomes,
      date_part('day',case when CI.IdEstado in (14,15) then age(CI.FechaAtencion,cast(CO.FechaGeneracion as timestamp)) else age(NOW(), cast(CO.FechaGeneracion as timestamp)) end) as tiempotranscurridodia,
      date_part('hour',case when CI.IdEstado in (14,15) then age(CI.FechaAtencion,cast(CO.FechaGeneracion as timestamp)) else age(NOW(), cast(CO.FechaGeneracion as timestamp)) end) as tiempotranscurridohora
      from ComMovTab_Intervencion CI
      join ComMovTab_Orden CO on CI.IdOrden = CO.IdOrden
      join ComMovTab_TipoIntervencion TIS on CO.SubActividad=TIS.DetalleTipoIntervencion
      join ComMovTab_Documento CD on CO.IdDocumento = CD.IdDocumento
      join ComMovTab_TipoIntervencion TI on CD.IdTipoIntervencion = TI.IdTipoIntervencion
      join ComMovTab_MaestrosEstado ME on CI.IdEstado = ME.IdEstado
      join ComMovTab_Usuario CU on CI.IdUsuario = CU.IdUsuario
      join GlobalMaster_Persona GMP on CU.IdPersona = GMP.IdPersona
      where TI.IdTipoIntervencion=1 ".$str_where2." order by case when CI.IdEstado in (14,15) then age(CI.FechaAtencion,cast(CO.FechaGeneracion as timestamp)) else age(NOW(), cast(CO.FechaGeneracion as timestamp)) end desc");
}


/**/
$lista="


      <div class='row'>
        <div class='col-md-12'>
          <div class='block-flat'>
            <div class='header'>
              <h3>Relacion de Asignaciones segun Tipo</h3>
            </div>

            <div id='tablamonit' class='tabla2'>
            <div id='monit' class='farmsmallasig3'>
            <div class='content'>
              <form role='form' id='formordenmonit' method='post'>
              <button data-toggle='modal' data-target='#mod-success3' class='btn btn-info' type='button' onclick='ExportarMonitorear($id)'><i class='fa fa-files-o'></i> Reporte</button>
              <button class='btn btn-warning' style='$flaga' type='button' onclick='validarorden()'><i class='fa fa-floppy-o'></i> Grabar</button>
              <a href='".FULL_URL."dashboard/mapa_monitoreo' target='_blank' class='btn btn-primary' type='button'> <i class='fa fa-globe'></i> Ver Mapa</a>
              
              <div class='table-responsive'>
                <table class='table table-bordered tableWithFloatingHeader' id='datatable3' >
                  <thead class='primary-emphasis'>
                      <th class='text-center primary-emphasis-dark'>#</th>
                      <th class='text-center primary-emphasis-dark' style='width:20%;'>Sub Actividad</th>
                      <th class='text-center primary-emphasis-dark' style='$flage'>Estado</th>
                      <th class='text-center primary-emphasis-dark' style='$flage'>Acta</th>
                      <th class='text-center primary-emphasis-dark'>Transcurrido</th>
                      <th class='text-center primary-emphasis-dark'>Resultado</th>
                      <th class='text-center primary-emphasis-dark' style='$flage'>Asignado</th>
                      <th class='text-center primary-emphasis-dark' style='$flage'>Ejecutado</th>
                      <th class='text-center primary-emphasis-dark'>OT</th>
                      <th class='text-center primary-emphasis-dark' style='$flaga'>Observacion</th>
                      <th class='text-center primary-emphasis-dark'>Generador</th>
                      <th class='text-center primary-emphasis-dark' style='$flage'>Atencion</th>
                      <th class='text-center primary-emphasis-dark'>Suministro</th>
                      <th class='text-center primary-emphasis-dark'>Origen</th>
                      <th class='text-center primary-emphasis-dark'>Sector</th>
                      <th class='text-center primary-emphasis-dark'>Cliente</th>
                  </thead>
                  <tbody>";


                    while ($retorno = pg_fetch_object($query)){
                       $un=$retorno->idunidadnegocio;
                       $varo=$retorno->idorden;
                       $vari=$retorno->idintervencion;
                       $var=$retorno->subactividad;
                       $var_idtipointervencion=$retorno->tipointervencion;                       

                       $var31=utf8_decode($retorno->idestado);
                       $var322=utf8_decode($retorno->nomestado);
                       $var32 = substr("$var322", 0,1);

                       $var44=utf8_decode($retorno->nompersona);
                       $var43=utf8_decode($retorno->apepersona);
                       $var33=utf8_decode($retorno->idusuario);
                       $var34=utf8_decode($retorno->nomusuario);
                       $var2=utf8_decode($retorno->idordentrabajo);
                       $var88=utf8_decode($retorno->observacion);
                       $var21=utf8_decode($retorno->nroatencion);

                       $var211=utf8_encode($retorno->fechaatencion);
                       $fecha_atencion_time = strtotime($var211);
                       /*$var211=date("Y-m-d g:i:s a", $var212);*/

		                   $varcap=utf8_encode($retorno->fechaatencion1);
                       $var2111 = date( "Y-m-d H:i:s", strtotime( $varcap ) );

                       $var22=utf8_decode($retorno->nroreclamo);
                       $var3=utf8_decode($retorno->suministro);
                       $var4=utf8_encode($retorno->fechageneracion);
                       $var41=utf8_encode($retorno->sector);

                       $varnf=utf8_decode($retorno->nuevafecha);
                       $standard = strtotime($varnf);
                       $varnf=date("Y-m-d g:i:s a", $standard);

                       $var5nombre=utf8_decode($retorno->nombresuministro);
                       $var5 = explode(" ", $var5nombre);
                       $var5 = $var5[0];


                       $var6=utf8_encode($retorno->direccionsuministro);
                       $var7=utf8_decode($retorno->seriemedidor);

                       $var8=$retorno->usuariogenero;

                    	$var_infructuosa = $retorno->infructuosa;

 
                       if($var31==15 or $var31==14){
                           if($var_infructuosa){
                            $infructuosa = "I";
                            $label_infructuosa = "Infructuosa";
                            $flag_infructuosa = "btn-warning";
                           }else{
                            $infructuosa = "OK";
                            $label_infructuosa = "OK";
                            $flag_infructuosa = "btn-success";
                           }
                       }else{
                        $infructuosa = "P";
                        $label_infructuosa = "Pendiente";
                        $flag_infructuosa = "";
                       }
 	
                       $varTime = $retorno->tiempotranscurridomes.'m '.sprintf('%02d',$retorno->tiempotranscurridodia).'d '.sprintf('%02d',$retorno->tiempotranscurridohora).'h';
                       if($var31==15 or $var31==14){
                           //$varTime = tiempo("{d}d {h}h{i}m", $fecha_atencion_time, strtotime("$varnf"));
                           $vardia = tiempo("{d}", $fecha_atencion_time, strtotime("$varnf"));
                           $vardiai = intval($vardia);
                       }else{
                           //$varTime = tiempo("{d}d {h}h{i}m", time(), strtotime("$varnf"));
                           $vardia = tiempo("{d}", time(), strtotime("$varnf"));
                           $vardiai = intval($vardia);
                       }


                      switch (true) {
 
                             case in_array($vardiai, range(0,2)):
                                  $flag='btn btn-success .btn-flat';
                                  break;
                             case in_array($vardiai, range(3,4)):
                                  $flag='btn btn-warning .btn-flat';
                                  break;
                             case in_array($vardiai, range(5,1000)):
                                  $flag='btn btn-danger .btn-flat';
                                  break;
 
 
                      }

                      $clase="btn btn-badge";
                      $estado="disabled";

                      switch ($var31)
                        {
                        case 5:
                          $color='btn btn-danger .btn-flat';
                        break;

                        case 9:
                        	$color='btn btn-warning .btn-flat';
                        break;
                        	
                        case 13:
                          $color='btn btn-warning .btn-flat';
                        break;

                        case 14:
                          $color='btn btn-primary .btn-flat';
                          $clase='btn btn-success';
                          $estado='';
                        break;

                        case 15:
                          $color='btn btn-success .btn-flat';
                          $clase='btn btn-success';
                          $estado='';
                        break;
                        }


                        if ($lsubactividad==1) {
                          $codesubactividad="<select class='form-control' style='width:100%;' id='idsubactividad[]' name='idsubactividad[]' value='$var' >
                                                  <option value='$varo 0'>$var</option>
                                                  ";
                                              $querydetasi2 = pg_query($conexion, "select * from ComMovTab_TipoIntervencion where IdTipoIntervencionSup = 1
                                                ");
                                               while ($retornodet2 = pg_fetch_object($querydetasi2)){
                                                $idtip=$retornodet2->idtipointervencion;
                                                $nomtip=$retornodet2->detalletipointervencion;
                                                $tipsup=$retornodet2->idtipointervencionsup;

                                                $codesubactividad.="<option value='$varo $idtip'>$nomtip</option>";
                                               }
                                              $codesubactividad.="</select>";

                        } else {
                          $codesubactividad=$var;
                        }

                       $lista.= "<tr class='odd gradeX'><input type ='hidden' id='i[]' name='i[]' value='$varo' >
                                <input type ='hidden' id='idchk[]' name='idchk[]' value='$varo' >  
				<input type='hidden' id='valorantiguosubact[]' name='valorantiguosubact[]' value='$varo $var'>
                                  <td style='width:1%;'>$vari</td>
                       			      <td style='width:17%;'>$codesubactividad</td>
                                  <td style='$flage'>
                                  <button id='boton' id_vari='$vari' data-popover='popover' type='button' style='width:100%;' class='$color' data-original-title='Eventos de Asignacion'
                                  data-content='<div class=tooltip1></div>' onclick='extensionmonitorear1($vari)'  data-placement='right' data-trigger='hover'>
                                  $var32</td>
                                  <td style='$flage'>
                                  <button data-toggle='modal' data-target='#mod-success' type='button' onclick='showActa($vari)' style='width:100%;' class='$clase' $estado><i class='fa fa-file-text-o'></i></button></td>
                                  <td><button type='button' style='width:90%;' class='$flag' title='Hasta la fecha de atenci&oacute;n'>$varTime</button></td>
								  
								  <td><button type='button' style='width:90%;' class='btn $flag_infructuosa' title='$label_infructuosa'>$infructuosa</button></td>

                                  <td style='$flage'>$var34 - $var44 $var43
                                                  </td>
                                  <td style='$flage'>$var2111</td>
                                  <td >$var2</td>
                                  <td style='$flaga'>$var88</td>
                                  <td>$var8</td>
                                  <td style='$flage'>$var21</td>

                                  <td>$var3</td>

                                  <td>$var4</td>
                                  <td>$var41</td>
                                  <td style='width:40px;'>

                                  <button data-popover='popover2' type='button' style='width:100%;' class='btn btn-primary .btn-flat' data-original-title='Informacion Adicional'
                                  data-content='<p>Medidor: $var7</p><p>Nombre: $var5nombre</p><p>Direccion: $var6</p>' data-placement='left' data-trigger='hover'>$var5

                                  </td>

                                </tr>";


                       }
                  $lista.="</tbody>
                </table>


                </form>
                 </div>
              </div>
            </div>

            </div>
          </div>
        </div>
      </div>




      ";

      echo $lista;


?>


<!--Prueba-->
  <script type="text/javascript">

        function UpdateTableHeaders() {
            $("div.divTableWithFloatingHeader").each(function() {
                var originalHeaderRow = $(".tableFloatingHeaderOriginal", this);
                var floatingHeaderRow = $(".tableFloatingHeader", this);
                var offset = $(this).offset();
                var scrollTop = $(window).scrollTop();

                var navegador = navigator.userAgent;
                var inicioScroll = scrollTop-30;
                var marginheader = 30;
                if (navigator.userAgent.indexOf('MSIE') !=-1) {
                 
                } else if (navigator.userAgent.indexOf('Firefox') !=-1) {
                  inicioScroll = scrollTop-50;
                  marginheader = 40;
                } else if (navigator.userAgent.indexOf('Chrome') !=-1) {
                  inicioScroll = scrollTop-30;
                  var marginheader = 50;
                } else if (navigator.userAgent.indexOf('Opera') !=-1) {
                  inicioScroll = scrollTop-30;
                  marginheader = 20;
                } else {
                
                }

                if (( inicioScroll > offset.top ) && (scrollTop < offset.top + $(this).height())) {
                    floatingHeaderRow.css("visibility", "visible");
                    floatingHeaderRow.css("top", (Math.min(scrollTop - offset.top, $(this).height() - floatingHeaderRow.height()))+ marginheader  + "px");

                    // Copy cell widths from original header
                    $("th", floatingHeaderRow).each(function(index) {
                        var cellWidth = $("th", originalHeaderRow).eq(index).css('width');
                        var color = $("th", originalHeaderRow).eq(index).css('background-color');
                        
                        $(this).css('width', cellWidth);
                        $(this).css('background-color', color);
                    });

                    // Copy row width from whole table
                    floatingHeaderRow.css("width", $(this).css("width"));
                    floatingHeaderRow.css("background-color", $(this).css("background-color"));
                }
                else {
                    floatingHeaderRow.css("visibility", "hidden");
                    floatingHeaderRow.css("top", "0px");
                    
                }
            });
        }

        $(document).ready(function() {
            $("table.tableWithFloatingHeader").each(function() {
                $(this).wrap("<div class=\"divTableWithFloatingHeader\" style=\"position:relative\"></div>");

                var originalHeaderRow = $("tr:first", this)
                originalHeaderRow.before(originalHeaderRow.clone());
                var clonedHeaderRow = $("tr:first", this)

                clonedHeaderRow.addClass("tableFloatingHeader");
                clonedHeaderRow.css("position", "absolute");
                clonedHeaderRow.css("top", "0px");
                clonedHeaderRow.css("left", $(this).css("margin-left"));
                clonedHeaderRow.css("visibility", "hidden");
                clonedHeaderRow.css("z-index", "5");
                
                originalHeaderRow.addClass("tableFloatingHeaderOriginal");
            });

            UpdateTableHeaders();
            $(window).scroll(UpdateTableHeaders);
            $(window).resize(UpdateTableHeaders);
        });
</script>
