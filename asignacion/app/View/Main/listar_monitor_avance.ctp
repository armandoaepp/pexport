<?php
date_default_timezone_set('America/Lima');

//require_once "../conexion.php";
require_once(APP."View/Main/conexion.ctp");

$fechaactual=date("Y-m-d");
$mes=date("m");

if(isset($_GET["idini1"])) 
{
  $idini1=$_GET["idini1"];
} else {
  $idini1=$fechaactual;
}


if(isset($_GET["idfin1"])) 
{
  $idfin1=$_GET["idfin1"];
} else {
  $idfin1=$fechaactual;
}




$querymondia1 = pg_query("select TipoIntervencion,SubActividad,
sum(TotalGeneradas) as TotalGeneradas,
sum(TotalFinalizadas) as TotalFinalizadas,
((sum(TotalGeneradas))-(sum(TotalFinalizadas))) as TotalPendientes,
case when sum(TotalGeneradas) > 0 then ((sum(TotalFinalizadas)*100)/sum(TotalGeneradas)) else 0 end as PorcentajeTotal, 
sum(OrdenesGeneradas) as OrdenesGeneradasDia,
sum(OrdenesFinalizadasOrdenesGeneradasDia) as OrdenesFinalizadasDia,
((sum(OrdenesGeneradas))-(sum(OrdenesFinalizadasOrdenesGeneradasDia))) as OrdenesPendientesDia,
case when sum(OrdenesGeneradas) > 0 then ((sum(OrdenesFinalizadasOrdenesGeneradasDia)*100)/sum(OrdenesGeneradas)) else 0 end as PorcentajeDia,  
sum(OrdenesFinalizadas) as OrdenesFinalizadasAcarreo
from (
select ctis.DetalleTipoIntervencion as TipoIntervencion,
cti.DetalleTipoIntervencion as SubActividad,
(case when (cast(ci.FechaAtencion as date) BETWEEN '2014-06-23' AND '$idfin1') and ci.IdEstado=15 and co.Condicion='0'
then COUNT(*) else 0 end) as TotalFinalizadas,
(case when (cast(ci.FechaAtencion as date) BETWEEN '$idini1' AND '$idfin1') and ci.IdEstado=15 and co.Condicion='0'
then COUNT(*) else 0 end) as OrdenesFinalizadas,
(case when (cast(ci.FechaAtencion as date) BETWEEN '$idini1' AND '$idfin1') and (ci.IdEstado=15) and co.Condicion='0' and ci.IdIntervencion in (select ci.IdIntervencion from ComMovTab_Orden co 
inner join ComMovTab_Intervencion ci 
on co.IdOrden = ci.IdOrden where cast(ci.FechaAsignado as date) = '$idfin1' and ci.IdEstado = 15) and co.Condicion='0'
then COUNT(*) else 0 end) as OrdenesFinalizadasOrdenesGeneradasDia,
(case when cast(ci.FechaAsignado as date) BETWEEN '2014-06-23' AND '$idfin1' and co.Condicion='0'
then COUNT(*) else 0 end) as TotalGeneradas,
(case when cast(ci.FechaAsignado as date) BETWEEN '$idini1' AND '$idfin1' and co.Condicion='0'
then COUNT(*) else 0 end) as OrdenesGeneradas
from ComMovTab_TipoIntervencion cti 
inner join ComMovTab_TipoIntervencion ctis on ctis.IdTipoIntervencion = cti.IdTipoIntervencionSup 
left outer join ComMovTab_Orden co on cti.DetalleTipoIntervencion = co.SubActividad
left outer join ComMovTab_Intervencion ci on co.IdOrden = ci.IdOrden
group by ctis.DetalleTipoIntervencion,cti.DetalleTipoIntervencion,co.Condicion,ci.FechaAtencion,co.FechaGeneracion,ci.FechaAsignado,ci.IdEstado,ci.IdIntervencion) as consulta2
group by TipoIntervencion,SubActividad");


$rs=pg_num_rows($querymondia1);


if ($rs > 0) {

    $lista="<div class='row'>
        <div class='col-md-12'>
          <div class='block-flat'>





            <div class='header'>              
              <h3>Monitor Especializado </h3>
              
              <button data-toggle='modal' data-target='#mod-success3' class='btn btn-info' type='button' onclick='enviarEmailMonitorear()' style='float:right;margin-top:-42px;'><i class='fa fa-envelope'></i> Enviar por Email</button>
            </div>

            <div class='form-group'>
            <label class='col-sm-1 control-label'>Desde</label>
            <div class='col-sm-3'>
            <div class='input-group date datetime col-md-12 col-xs-7' data-min-view='2' data-date-format='yyyy-mm-dd'>
                <input class='form-control' size='16' type='text' value='$idini1' id='fechaini1' name='fechaini1' readonly>
                    <span class='input-group-addon btn btn-primary'><span class='glyphicon glyphicon-th'></span></span>
            </div>          
            </div>
            <label class='col-sm-1 control-label'>Hasta</label>
            <div class='col-sm-3'>
            <div class='input-group date datetime col-md-12 col-xs-7' data-min-view='2' data-date-format='yyyy-mm-dd'>
               <input class='form-control' size='16' type='text' value='$idfin1' id='fechafin1' name='fechafin1' readonly>
                    <span class='input-group-addon btn btn-primary'><span class='glyphicon glyphicon-th'></span></span>
            </div>          
            </div>
            
            <button class='btn btn-primary' onclick='showMonAv()' >Consultar</button>
            </div>



            <div class='panel-group accordion accordion-semi' id='accordion4' style='margin-bottom:0px;'>
                      <div class='panel panel-default' style='margin-bottom:0px;'>
                        <div class='panel-heading success'>
                          <h4 class='panel-title'>
                            <a data-toggle='collapse' data-parent='#accordion4' href='#ac4-1'>
                                <i class='fa fa-angle-right'></i> Perspectiva Actividades
                            </a>
                          </h4>
                        </div>
                        <div id='ac4-1' class='panel-collapse collapse in'>
                          <div class='panel-body'>

                                     <div class='col-sm-12 col-md-12'>
                                        <div class='tab-container'>
                                          <ul class='nav nav-tabs'>
                                            <li class='active'><a href='#pers1a' data-toggle='tab'>Diario</a></li>
                                            <li><a href='#pers1b' onclick='showMonAvPers1b()' data-toggle='tab'>Consolidado Periodo</a></li>
                                            <li><a href='#pers1c' onclick='showMonAvPers1c()' data-toggle='tab'>Consolidado Anual</a></li>
                                          </ul>
                                          <div class='tab-content'>

                                            <div class='tab-pane active cont tablaspecial' id='pers1a'>";
                                              
                                                      $lista.="<div class='table-responsive'>
                                                      <table class='table table-bordered tableWithFloatingHeader' id='datatablepers1a' >
                                                      <thead class='primary-emphasis'>
                                                      <tr>
                                                          <th class='text-center primary-emphasis-dark' >Actividad</th>
                                                          <th class='text-center primary-emphasis-dark'>Sub Actividad</th>
                                                          
                                                          <th class='text-center primary-emphasis-dark' >Generadas</th>
                                                          <th class='text-center primary-emphasis-dark'>Finalizadas</th>
                                                          <th class='text-center danger-emphasis'>Pendientes</th>
                                                          <th class='text-center primary-emphasis-dark'style='width:15%;'>% Avance</th>
                                                          <th class='text-center primary-emphasis-dark'>Total Finalizadas</th>
                                                          <th class='text-center danger-emphasis-dark'>% Total Avance</th>
                                                          <th class='text-center danger-emphasis-dark'>Pendientes Final</th>

                                                      </tr>
                                                      </thead>
                                                      <tbody>
                                                      
                                                      ";

                                                      while ($retornomondia1 = pg_fetch_object($querymondia1)){
                                                                            $varmondia1TipoIntervencion=$retornomondia1->tipointervencion;
                                                                            $varmondia1SubActividad=$retornomondia1->subactividad;
                                                                            $varmondia1TotalGeneradas=$retornomondia1->totalgeneradas;
                                                                            $varmondia1TotalFinalizadas=$retornomondia1->totalfinalizadas;
                                                                            $varmondia1TotalPendientes=$retornomondia1->totalpendientes;
                                                                            $varmondia1PorcentajeTotal=$retornomondia1->porcentajetotal;
                                                                            $varmondia1OrdenesGeneradasDia=$retornomondia1->ordenesgeneradasdia;
                                                                            $varmondia1OrdenesFinalizadasDia=$retornomondia1->ordenesfinalizadasdia;
                                                                            $varmondia1OrdenesPendientesDia=$retornomondia1->ordenespendientesdia;
                                                                            $varmondia1PorcentajeDia=$retornomondia1->porcentajedia;
                                                                            $varmondia1OrdenesFinalizadasAcarreo=$retornomondia1->ordenesfinalizadasacarreo;

                                                                            $totaldiaant=$varmondia1TotalPendientes+$varmondia1OrdenesFinalizadasAcarreo;

                                                                            if ($totaldiaant>0) {
                                                                              $porcdiaant=($varmondia1OrdenesFinalizadasAcarreo*100)/$totaldiaant;
                                                                              $porcdiaant=round($porcdiaant, 2);

                                                                            } else {
                                                                              $porcdiaant=0;
                                                                              
                                                                            }

                                                                            

                                                                            if ($varmondia1PorcentajeDia < 33) {
                                                                              $color='danger';
                                                                              $varcolor='color:black;';
                                                                            } elseif ($varmondia1PorcentajeDia > 33 && $varmondia1PorcentajeDia < 70) {
                                                                              $color='warning';
                                                                              $varcolor='';
                                                                            } else {
                                                                              $color='success';
                                                                              $varcolor='';
                                                                            }


                                                                            if ($porcdiaant < 33) {
                                                                              $coloro='danger';
                                                                              $varcolor='color:black;';
                                                                            } elseif ($porcdiaant > 33 && $porcdiaant < 70) {
                                                                              $coloro='warning';
                                                                              $varcolor='';
                                                                            } else {
                                                                              $coloro='success';
                                                                              
                                                                            }

                                                                            



                                                      $lista.= "
                                                        <tr class='odd gradeX'>
                                                          <td><strong>$varmondia1TipoIntervencion</strong></td>
                                                          <td><strong>$varmondia1SubActividad</strong></td>
                                                          <td><strong>$varmondia1OrdenesGeneradasDia</strong></td>
                                                          <td><strong>$varmondia1OrdenesFinalizadasDia</strong></td>
                                                          <td><strong>$varmondia1OrdenesPendientesDia</strong></td>
                                                          <td class='center'><div class='progress progress-striped active'>
                                                                                  <div class='progress-bar progress-bar-$color' style='width:$varmondia1PorcentajeDia%;$varcolor'>".number_format($varmondia1PorcentajeDia,2)."%</div>
                                                          </div></td>
                                                          <td><strong>$varmondia1OrdenesFinalizadasAcarreo</strong></td>
                                                          <td class='center'><div class='progress progress-striped active'>
                                                                                  <div class='progress-bar progress-bar-$coloro' style='width:$porcdiaant%;$varcolor' >".number_format($porcdiaant)."%</div>
                                                          </div></td>
                                                          <td><strong>$varmondia1TotalPendientes</strong></td>
                                                         </tr>
                                                          ";


                                                          }



                                                        $lista.="
                                                      </tbody>
                                                      <tfoot >
                                                      <tr>
                                                        <td style='text-align:right;' colspan='2'><strong>Total</strong></td>
                                                        ";


                                                        $queryfinalmondia1=pg_query("select 
                                                                              sum(TotalGeneradas) as TotalGeneradas,
                                                                              sum(TotalFinalizadas) as TotalFinalizadas,
                                                                              sum(TotalPendientes) as TotalPendientes,
                                                                              case when sum(TotalGeneradas) > 0 then ((sum(TotalFinalizadas)*100)/sum(TotalGeneradas)) else 0 end as PorcentajeTotal, 
                                                                              sum(OrdenesGeneradasDia) as OrdenesGeneradasDia,
                                                                              sum(OrdenesFinalizadasDia) as OrdenesFinalizadasDia,
                                                                              sum(OrdenesPendientesDia) as OrdenesPendientesDia,
                                                                              case when sum(OrdenesGeneradasDia) > 0 then ((sum(OrdenesFinalizadasDia)*100)/sum(OrdenesGeneradasDia)) else 0 end as PorcentajeDia,
                                                                              case when (sum(TotalPendientes)+sum(OrdenesFinalizadasAcarreo)) > 0 then ((sum(OrdenesFinalizadasAcarreo)*100)/(sum(TotalPendientes)+sum(OrdenesFinalizadasAcarreo))) else 0 end as PorcentajeDiaAcarreo,  
                                                                              sum(OrdenesFinalizadasAcarreo) as OrdenesFinalizadasAcarreo
                                                                              from (select TipoIntervencion,SubActividad,
                                                                              sum(TotalGeneradas) as TotalGeneradas,
                                                                              sum(TotalFinalizadas) as TotalFinalizadas,
                                                                              ((sum(TotalGeneradas))-(sum(TotalFinalizadas))) as TotalPendientes, 
                                                                              sum(OrdenesGeneradas) as OrdenesGeneradasDia,
                                                                              sum(OrdenesFinalizadasOrdenesGeneradasDia) as OrdenesFinalizadasDia,
                                                                              ((sum(OrdenesGeneradas))-(sum(OrdenesFinalizadasOrdenesGeneradasDia))) as OrdenesPendientesDia, 
                                                                              sum(OrdenesFinalizadas) as OrdenesFinalizadasAcarreo
                                                                              from (
                                                                              select ctis.DetalleTipoIntervencion as TipoIntervencion,
                                                                              cti.DetalleTipoIntervencion as SubActividad,
                                                                              (case when (cast(ci.FechaAtencion as date) BETWEEN '2014-06-23' AND '$idfin1') and ci.IdEstado=15 and co.Condicion='0'
                                                                              then COUNT(*) else 0 end) as TotalFinalizadas,
                                                                              (case when (cast(ci.FechaAtencion as date) BETWEEN '$idini1' AND '$idfin1') and ci.IdEstado=15 and co.Condicion='0'
                                                                              then COUNT(*) else 0 end) as OrdenesFinalizadas,
                                                                              (case when (cast(ci.FechaAtencion as date) BETWEEN '$idini1' AND '$idfin1') and (ci.IdEstado=15) and co.Condicion='0' and ci.IdIntervencion in (select ci.IdIntervencion from ComMovTab_Orden co 
                                                                              inner join ComMovTab_Intervencion ci 
                                                                              on co.IdOrden = ci.IdOrden where cast(ci.FechaAsignado as date) = '$idini1' and ci.IdEstado = 15) and co.Condicion='0'
                                                                              then COUNT(*) else 0 end) as OrdenesFinalizadasOrdenesGeneradasDia,
                                                                              (case when cast(ci.FechaAsignado as date) BETWEEN '2014-06-23' AND '$idfin1' and co.Condicion='0'
                                                                              then COUNT(*) else 0 end) as TotalGeneradas,
                                                                              (case when cast(ci.FechaAsignado as date) BETWEEN '$idini1' AND '$idfin1' and co.Condicion='0'
                                                                              then COUNT(*) else 0 end) as OrdenesGeneradas
                                                                              from ComMovTab_TipoIntervencion cti 
                                                                              inner join ComMovTab_TipoIntervencion ctis on ctis.IdTipoIntervencion = cti.IdTipoIntervencionSup 
                                                                              left outer join ComMovTab_Orden co on cti.DetalleTipoIntervencion = co.SubActividad
                                                                              left outer join ComMovTab_Intervencion ci on co.IdOrden = ci.IdOrden
                                                                              group by ctis.DetalleTipoIntervencion,cti.DetalleTipoIntervencion,co.Condicion,ci.FechaAtencion,ci.FechaAsignado,co.FechaGeneracion,ci.IdEstado,ci.IdIntervencion) as consulta2
                                                                              group by TipoIntervencion,SubActividad) as ConsultaSumaMon1");     
                                                                                  while ($retornofinalmondia1 = pg_fetch_object($queryfinalmondia1)){
                                                                                            $varfinalmondia1TotalGeneradas=$retornofinalmondia1->totalgeneradas;
                                                                                            $varfinalmondia1TotalFinalizadas=$retornofinalmondia1->totalfinalizadas;
                                                                                            $varfinalmondia1TotalPendientes=$retornofinalmondia1->totalpendientes;
                                                                                            $varfinalmondia1PorcentajeTotal=$retornofinalmondia1->porcentajetotal;
                                                                                            $varfinalmondia1OrdenesGeneradasDia=$retornofinalmondia1->ordenesgeneradasdia;
                                                                                            $varfinalmondia1OrdenesFinalizadasDia=$retornofinalmondia1->ordenesfinalizadasdia;
                                                                                            $varfinalmondia1OrdenesPendientesDia=$retornofinalmondia1->ordenespendientesdia;
                                                                                            $varfinalmondia1PorcentajeDia=$retornofinalmondia1->porcentajedia;
                                                                                            $varfinalmondia1OrdenesFinalizadasAcarreo=$retornofinalmondia1->ordenesfinalizadasacarreo;
                                                                                            $varfinalmondia1PorcentajeDiaAcarreo=$retornofinalmondia1->porcentajediaacarreo;

                                                                                            if ($varfinalmondia1PorcentajeDia< 33) {
                                                                                              $color2='danger';
                                                                                              
                                                                                            } elseif ($varfinalmondia1PorcentajeDia > 33 && $varfinalmondia1PorcentajeDia< 70) {
                                                                                              $color2='warning';
                                                                                            } else {
                                                                                              $color2='success';
                                                                                            }


                                                                                            if ($varfinalmondia1PorcentajeDiaAcarreo< 33) {
                                                                                              $color22='danger';
                                                                                              
                                                                                            } elseif ($varfinalmondia1PorcentajeDiaAcarreo > 33 && $varfinalmondia1PorcentajeDiaAcarreo< 70) {
                                                                                              $color22='warning';
                                                                                            } else {
                                                                                              $color22='success';
                                                                                            }

                                                                                            $lista.="
                                                                                                     <td><strong>$varfinalmondia1OrdenesGeneradasDia<strong></td>
                                                                                                     <td><strong>$varfinalmondia1OrdenesFinalizadasDia<strong></td>
                                                                                                     <td><strong>$varfinalmondia1OrdenesPendientesDia<strong></td>
                                                                                                     <td>
                                                                                                        <div class='progress progress-striped active'>
                                                                                                        <div class='progress-bar progress-bar-$color2' style='width: $varfinalmondia1PorcentajeDia%'>".number_format($varfinalmondia1PorcentajeDia,2)."%</div>
                                                                                                        </div>
                                                                                                     </td>
                                                                          
                     
                                                                                                     <td><a href='#' data-toggle='modal' data-target='#mod-success' onclick='showModalReportFin(15,10000,1)'><strong>$varfinalmondia1OrdenesFinalizadasAcarreo<strong></a></td>
                                                                                                     <td>
                                                                                                        <div class='progress progress-striped active'>
                                                                                                        <div class='progress-bar progress-bar-$color22' style='width: $varfinalmondia1PorcentajeDiaAcarreo%'>$varfinalmondia1PorcentajeDiaAcarreo%</div>
                                                                                                        </div>
                                                                                                     </td>
                                                                                                     <td><strong>$varfinalmondia1TotalPendientes<strong></td>
                                                                                            ";

                                                                                        }


                                                        
                                                        $lista.="</tr>
                                                      </tfoot>


                                                      </table>
                                                      </div>
                                                      ";



                                            $lista.="</div>

                                            <div class='tab-pane cont tablaspecial' id='pers1b'>
                                            <span class='ciclo_loading6' style=''><img src='../js/jquery.select2/select2-spinner.gif'></span>
                                            <div id='tab1b'>

                                            </div>           
                                            </div>
 
                                            <div class='tab-pane cont tablaspecial' id='pers1c'>
                                            <span class='ciclo_loading6' style=''><img src='../js/jquery.select2/select2-spinner.gif'></span>           
                                            <div id='tab1c'>

                                            </div> 
                                            </div>


                                            </div>


                                          </div>
                                        </div>

                                        
                                      </div>  

                    
                                </div>
                            </div>  
 



         ";

} else {

  $lista="Hoy no se generaron ni resolvieron ordenes";

}



$querymon2 = pg_query("
select IdUsuario,NomPersona,ApePersona,
sum(GeneradaPexTotal) as GenPexTotal,
sum(GeneradaEnMes) as GenEnMes,
sum(GeneradaPexMes) as GenPexMes,
sum(GeneradaPexDia) as GenPexDia,
sum(GeneradaEnDia) as GenEnDia,
sum(FinalizadaDia) as FinDia,
sum(MesFin) as FinMes,
sum(TotalFin) as FinTotal,
(sum(GeneradaPexTotal)-sum(TotalFin)) as Pendientes,
((sum(TotalFin)*100)/sum(GeneradaPexTotal)) as Porcentaje 
from(
select 
ci.IdUsuario,
gp.NomPersona,
gp.ApePersona,
count(ci.IdEstado) as GeneradaPexTotal,
case when extract('month' from cast(co.FechaGeneracion as timestamp))=$mes  then count(ci.IdEstado) else 0 end as GeneradaEnMes,
case when extract('month' from ci.FechaAsignado)=$mes then count(ci.IdEstado) else 0 end as GeneradaPexMes,
case when cast(ci.FechaAsignado as date)='$idfin1' then count(ci.IdEstado) else 0 end as GeneradaPexDia,
case when cast(co.FechaGeneracion as date)='$idfin1' then count(ci.IdEstado) else 0 end as GeneradaEnDia,
case when cast(ci.FechaAtencion as date) BETWEEN '$idini1' and '$idfin1' and ci.IdEstado=15 then count(ci.IdEstado) else 0 end as FinalizadaDia, 
case when extract('month' from ci.FechaAtencion)=$mes and ci.IdEstado=15 then count(ci.IdEstado) else 0 end as MesFin,  
case when ci.IdEstado=15 then count(ci.IdEstado) else 0 end as TotalFin  from ComMovTab_Intervencion ci
inner join ComMovTab_Orden co on ci.IdOrden=co.IdOrden
inner join ComMovTab_Usuario cu on ci.IdUsuario = cu.IdUsuario
inner join GlobalMaster_Persona gp on cu.IdPersona = gp.IdPersona
where co.SubActividad in (select DetalleTipoIntervencion from ComMovTab_TipoIntervencion)
and co.Condicion='0'
group by ci.IdUsuario,gp.NomPersona,gp.ApePersona,ci.FechaAtencion,ci.FechaAsignado,ci.IdEstado,co.FechaGeneracion) as superconsulta
group by IdUsuario,NomPersona,ApePersona
");




$rs=pg_num_rows($querymon2);


$lista2="




            <div class='panel-group accordion accordion-semi' id='accordion5' style='margin-bottom:0px;'>
                      <div class='panel panel-default' style='margin-bottom:0px;'>
                        <div class='panel-heading success'>
                          <h4 class='panel-title'>
                            <a data-toggle='collapse' data-parent='#accordion5' href='#ac5-1'>
                                <i class='fa fa-angle-right'></i> Perspectiva Tecnico de Campo
                            </a>
                          </h4>
                        </div>

                        <div id='ac5-1' class='panel-collapse collapse in'>
                          <div class='panel-body'>


                                    <div class='col-sm-12 col-md-12'>
                                        <div class='tab-container'>
                                          <ul class='nav nav-tabs'>
                                            <li class='active'><a href='#pers2a' data-toggle='tab'>Diario</a></li>
                                            <li><a href='#pers2b' onclick='showMonAvPers2b()' data-toggle='tab'>Consolidado Periodo</a></li>
                                            <li><a href='#pers2c' onclick='showMonAvPers2c()' data-toggle='tab'>Consolidado Anual</a></li>
                                          </ul>
                                          <div class='tab-content'>

                                            <div class='tab-pane active cont tablaspecial' id='pers2a'>";
                                              
                                                      $lista2.="<div class='table-responsive'>
                                                      <table class='table table-bordered tableWithFloatingHeader' id='datatablepers2aa'  >

                                                      <thead class='primary-emphasis'>
                                                      <tr>
                                                          <th class='text-center primary-emphasis-dark'>Codigo</th>
                                                          <th class='text-center primary-emphasis-dark'>Tecnico Inspector</th>
                                                          <th class='text-center primary-emphasis-dark'>Ordenes Generadas</th>
                                                          <th class='text-center primary-emphasis-dark'>Importada WS</th>
                                                          <th class='text-center primary-emphasis-dark'>Porcentaje de Avance</th>";
                                                          
                                                          $query2a = pg_query("select * from ComMovTab_MaestrosEstado where EstadoSup=11 and IdEstado!=12 and IdEstado!=15");

                                                          while ($retornocom2a = pg_fetch_object($query2a)){
                                                                                        $varidestado=$retornocom2a->idestado;
                                                                                        $varnomestado=$retornocom2a->nomestado;
                                                                                        $varestadosup=$retornocom2a->estadosup;

                                                                                        $lista2.="<th class='text-center primary-emphasis-dark'>$varnomestado</th>";

                                                                                    }
                                                          
                                                      $lista2.="
                                                          <th class='text-center primary-emphasis-dark'>Finalizada Dia</th>
                                                          <th class='text-center primary-emphasis-dark'>Finalizada Mes</th>
                                                          <th class='text-center danger-emphasis-dark'>Pendientes</th>
                                                      </tr>
                                                      </thead>
                                                      <tbody>";
                                                        while ($retornocom2 = pg_fetch_object($querymon2)){
                                                                            $varidusuario=$retornocom2->idusuario ;
                                                                            $varnompersona=$retornocom2->nompersona;
                                                                            $varapepersona=$retornocom2->apepersona;

                                                                            $vargeneradas=$retornocom2->genendia;
                                                                            $vargeneradasws=$retornocom2->genpexdia;
                                                                            $varfinalizadasdia=$retornocom2->findia;
                                                                            $varfinalizadasmes=$retornocom2->finmes;

                                                                            $varpendientes=$retornocom2->pendientes;
                                                                            $varporcentajemon2=$retornocom2->porcentaje;


                                                                            $totaltecgen[]=$vargeneradas;
                                                                            $totaltecgenpex[]=$vargeneradasws;
                                                                            $totaltecfindia[]=$varfinalizadasdia;
                                                                            $totaltecfinmes[]=$varfinalizadasmes;
                                                                            $totaltecpen[]=$varpendientes;

                                                                            if ($varporcentajemon2 < 33) {
                                                                              $color3='danger';
                                                                              
                                                                            } elseif ($varporcentajemon2 > 33 && $varporcentajemon2 < 70) {
                                                                              $color3='warning';
                                                                            } else {
                                                                              $color3='success';
                                                                            }

                                                                        

                                                        $lista2.= "
                                                        <tr class='odd gradeX'>
                                                          <td><strong>$varidusuario</strong></td>
                                                          <td><strong>$varnompersona $varapepersona</strong></td>
                                                          <td><strong>$vargeneradas</strong></td>
                                                          <td><strong>$vargeneradasws</strong></td>
                                                          <td class='center'><div class='progress progress-striped active'>
                                                                                  <div class='progress-bar progress-bar-$color3' style='width: $varporcentajemon2%'>".number_format($varporcentajemon2,2)."%</div>
                                                                                 </div></td>"
                                                          ;


                                                          $query3mon2 = pg_query("select me.IdEstado as IdEstado,me.NomEstado,consulta1.cantidad as cantidad from ComMovTab_MaestrosEstado me 
                                                                                      left outer join ( select ci.IdEstado as Id,COUNT(*) as cantidad from ComMovTab_Intervencion ci join
                                                                                      ComMovTab_Orden co on ci.IdOrden = co.IdOrden where ci.IdUsuario=$varidusuario and co.Condicion='0'
                                                                                      and co.SubActividad in (select DetalleTipoIntervencion from ComMovTab_TipoIntervencion)
                                                                                      group by ci.IdEstado) as consulta1
                                                                                      on consulta1.Id = me.IdEstado where me.EstadoSup=11 and me.IdEstado !=12 and me.IdEstado!=15 order by me.IdEstado asc
                                                                                      ");

                                                          while ($retornocom3mon2 = pg_fetch_object($query3mon2)){
                                                                            $varidestado=$retornocom3mon2->idestado;
                                                                            $varcontadorestadomon2=$retornocom3mon2->cantidad;

                                                                            $lista2.="<td>
                                                                            <a href='#' data-toggle='modal' data-target='#mod-success2' onclick='showModalMonAv($varidusuario,$varidestado,1)'><strong>$varcontadorestadomon2</strong></a>
                                                                            
                                                                            </td>";

                                                                        }


                                                       $lista2.="
                                                          <td><a href='#' data-toggle='modal' data-target='#mod-success2' onclick='showModalMonAv($varidusuario,15,2)'><strong>$varfinalizadasdia</strong>
                                                              </a> -
                                                              <a href='#' data-toggle='modal' data-target='#mod-success' onclick='showModalReportFin(15,$varidusuario,2)'><strong>A<strong></a>
                                                          <td><a href='#' data-toggle='modal' data-target='#mod-success2' onclick='showModalMonAv($varidusuario,15,3)'><strong>$varfinalizadasmes</strong>
                                                              </a> -
                                                              <a href='#' data-toggle='modal' data-target='#mod-success' onclick='showModalReportFin(15,$varidusuario,3)'><strong>A<strong></a>
                                                          <td><strong>$varpendientes</strong></td>                    
                                                          </tr>";
                                                          
                                                        }

                                                          
                                                      $lista2.="</tbody>
                                                      <tfoot >
                                                      <tr>
                                                      <td style='text-align:right;' colspan='2'><strong>Total</strong></td>
                                                      ";


                                                              $totaltecgenf=(!empty($totaltecgen))?array_sum($totaltecgen):0;
                                                              $totaltecgenpexf=(!empty($totaltecgenpex))?array_sum($totaltecgenpex):0;
                                                              $totaltecfindiaf=(!empty($totaltecfindia))?array_sum($totaltecfindia):0;
                                                              $totaltecfinmesf=(!empty($totaltecfinmes))?array_sum($totaltecfinmes):0;
                                                              $totaltecpenf=(!empty($totaltecpen))?array_sum($totaltecpen):0;
                                                              

                                                      $lista2.="<td><strong>$totaltecgenf</strong></td>
                                                                <td><strong>$totaltecgenpexf</strong></td>
                                                                                                   <td>
                                                                                                      <div class='progress progress-striped active'>
                                                                                                          <div class='progress-bar progress-bar-$color2' style='width: '>%</div>
                                                                                                      </div>
                                                                                                   </td>
                                                                                                   <td></td>
                                                                                                   <td></td>
                                                                                                   <td></td>
                                                                                                   <td><strong>$totaltecfindiaf</strong></td>
                                                                                                   <td><strong>$totaltecfinmesf</strong></td>
                                                                                                   <td><strong>$totaltecpenf</strong></td>
                                                      </tr>
                                                      </tfoot>


                                                      </table>
                                                      </div>
                                            </div>

                                            <div class='tab-pane cont tablaspecial' id='pers2b'>
                                            <span class='ciclo_loading9' style=''><img src='../js/jquery.select2/select2-spinner.gif'></span>
                                            <div id='tab2b'>

                                            </div>           
                                            </div>


                                            <div class='tab-pane cont tablaspecial'  id='pers2c'>
                                            <span class='ciclo_loading9' style=''><img src='../js/jquery.select2/select2-spinner.gif'></span>
                                            <div id='tab2c'>

                                            </div>           
                                            </div>

                                


                                            </div>


                                          </div>
                                        </div>

                                        
                                      </div>  

                    
                                </div>
                            </div>  


     

        ";













$querymon3 = pg_query("
select Cuadrilla,
SUM(TotalAsignadas) as TotalAsignadas,
SUM(TotalDescargadas) as TotalDescargadas,
SUM(TotalExportadas) as TotalExportadas,
SUM(TotalFinDia) as TotalFinalDia,
SUM(TotalFinMes) as TotalFinalMes,
SUM(TotalFinalizadas) as TotalFinalizadas  from (
select 
Cuadrilla,
TotalAsignadas,
TotalDescargadas,
TotalExportadas,
TotalFinDia,
TotalFinMes,
TotalFinalizadas from (
select IdSector,Sector,
SUM(TotalAsignadas) as TotalAsignadas,
SUM(TotalDescargadas) as TotalDescargadas,
SUM(TotalExportadas) as TotalExportadas,
SUM(TotalFinalizadasDia) as TotalFinDia,
SUM(TotalFinalizadasMes) as TotalFinMes,
SUM(TotalFinalizadas) as TotalFinalizadas from (
select  
co.IdSector as IdSector,
co.Sector as Sector,
(case when ci.IdEstado = 13 and co.Condicion='0' and co.SubActividad 
in (select DetalleTipoIntervencion from ComMovTab_TipoIntervencion)
or ci.IdEstado in (select IdEstado from ComMovTab_MaestrosEstado where EstadoSup=13) and co.Condicion='0' and co.SubActividad 
in (select DetalleTipoIntervencion from ComMovTab_TipoIntervencion)
then 1 else 0 end) as TotalDescargadas,
(case when ci.IdEstado = 5 and co.Condicion='0' and co.SubActividad 
in (select DetalleTipoIntervencion from ComMovTab_TipoIntervencion)
then 1 else 0 end) as TotalAsignadas,
(case when ci.IdEstado = 14 and co.Condicion='0' and co.SubActividad 
in (select DetalleTipoIntervencion from ComMovTab_TipoIntervencion)
then 1 else 0 end) as TotalExportadas,
(case when ci.IdEstado = 15 and cast(ci.FechaAtencion as date)='$idfin1' and co.Condicion='0' and co.SubActividad 
in (select DetalleTipoIntervencion from ComMovTab_TipoIntervencion)
then 1 else 0 end) as TotalFinalizadasDia,
(case when ci.IdEstado = 15 and extract('month' from ci.FechaAtencion)=$mes and co.Condicion='0' and co.SubActividad 
in (select DetalleTipoIntervencion from ComMovTab_TipoIntervencion)
then 1 else 0 end) as TotalFinalizadasMes,
(case when ci.IdEstado = 15 and co.Condicion='0' and co.SubActividad 
in (select DetalleTipoIntervencion from ComMovTab_TipoIntervencion)
then 1 else 0 end) as TotalFinalizadas
from ComMovTab_Orden co 
inner join ComMovTab_Intervencion ci on co.IdOrden = ci.IdOrden
where co.Condicion='0' and co.SubActividad 
in (select DetalleTipoIntervencion from ComMovTab_TipoIntervencion)) as superconsulta
group by IdSector,Sector) as setfinal
inner join (
select
gu.IdUbicacionConc as IdConcesionaria,
case when (select IdConceptosUbicacion from GlobalMaster_Ubicacion where IdUbicacion =dr2.IdUbicacionSup)=8
or (select IdConceptosUbicacion from GlobalMaster_Ubicacion where IdUbicacion =dr2.IdUbicacionSup)=7  
then gu3.NomUbicacion else gu2.NomUbicacion end as Cuadrilla,
gu2.NomUbicacion as SubCuadrilla,
gu.NomUbicacion as Sector
from GlobalMaster_DetalleUbicacionRegla dr
inner join GlobalMaster_DetalleUbicacionRegla dr2 on dr.IdUbicacionSup = dr2.IdUbicacion
inner join GlobalMaster_Ubicacion gu on dr.IdUbicacion = gu.IdUbicacion
inner join GlobalMaster_Ubicacion gu2 on dr.IdUbicacionSup = gu2.IdUbicacion
inner join GlobalMaster_Ubicacion gu3 on dr2.IdUbicacionSup = gu3.IdUbicacion
where gu.IdConceptosUbicacion = 5 ) as setfinal2 on
setfinal.IdSector=cast(setfinal2.IdConcesionaria as integer)) as Cuadrilla
group by Cuadrilla
");




$rs=pg_num_rows($querymon3);


$lista3="







            <div class='panel-group accordion accordion-semi' id='accordion6' style='margin-bottom:0px;'>
                      <div class='panel panel-default' style='margin-bottom:0px;'>
                        <div class='panel-heading success'>
                          <h4 class='panel-title'>
                            <a data-toggle='collapse' data-parent='#accordion6' href='#ac6-1'>
                                <i class='fa fa-angle-right'></i> Perspectiva Ubicacion
                            </a>
                          </h4>
                        </div>
                        <div id='ac6-1' class='panel-collapse collapse in'>
                          <div class='panel-body'>

                                     <div class='col-sm-12 col-md-12'>
                                        <div class='tab-container'>
                                          <ul class='nav nav-tabs'>
                                            <li class='active'><a href='#pers3a' data-toggle='tab'>Cuadrilla</a></li>
                                            <li><a href='#pers3b' onclick='showMonAvPers3b()' data-toggle='tab'>SubCuadrilla</a></li>
                                            <li><a href='#pers3c' onclick='showMonAvPers3c()' data-toggle='tab'>Sectores</a></li>
                                            <li><a href='#pers3d' onclick='showMonAvPers3d()' data-toggle='tab'>Tecnicos x Sector</a></li>
                                          </ul>
                                          <div class='tab-content'>

                                            <div class='tab-pane active cont tablaspecial' id='pers3a'>";
                                              
                                                      $lista3.="<div class='table-responsive'>
                                                      <table class='table table-bordered tableWithFloatingHeader' id='datatablepers3a' >
                                                      <thead class='primary-emphasis'>
                                                      <tr>
                                                          <th class='text-center primary-emphasis-dark'>Cuadrilla</th>
                                                          <th class='text-center primary-emphasis-dark'>Asignadas</th>
                                                          <th class='text-center primary-emphasis-dark'>Descargadas</th>
                                                          <th class='text-center primary-emphasis-dark'>Exportadas</th>
                                                          <th class='text-center primary-emphasis-dark'>Finalizadas (Dia)</th>
                                                          <th class='text-center primary-emphasis-dark'>Finalizadas (Mes)</th>
                                                          <th class='text-center primary-emphasis-dark'>Finalizadas</th>
                                                          <th class='text-center primary-emphasis-dark'>% Avance</th>
                                                          <th class='text-center primary-emphasis-dark'>Total Pendientes</th>
                                                      </tr>
                                                      </thead>
                                                       <tbody>";
                                                        while ($retornocom3 = pg_fetch_object($querymon3)){
                                                                            $varcuadrilla=$retornocom3->cuadrilla;
                                                                            $varcuaasignada=$retornocom3->totalasignadas;
                                                                            $varcuadescargada=$retornocom3->totaldescargadas;
                                                                            $varcuaexportada=$retornocom3->totalexportadas;

                                                                            $varcuafinalizadadia=$retornocom3->totalfinaldia;
                                                                            $varcuafinalizadames=$retornocom3->totalfinalmes;

                                                                            $varcuafinalizada=$retornocom3->totalfinalizadas;
                                                                            $varcuapendientes=$varcuaasignada+$varcuadescargada+$varcuaexportada;
                                                       

                                                                             if ($varcuafinalizada==0){
                                                                              $varcuaporcentaje=0;
                                                                            }
                                                                            else {
                                                                              $varcuaporcentaje=($varcuafinalizada*100)/($varcuapendientes+$varcuafinalizada);
                                                                              $varcuaporcentaje=round($varcuaporcentaje,2);
                                                                            }


                                                                            $totalcuaasigv[]=$varcuaasignada;
                                                                            $totalcuadesv[]=$varcuadescargada;
                                                                            $totalcuaexpov[]=$varcuaexportada;

                                                                            $totalcuafindiav[]=$varcuafinalizadadia;
                                                                            $totalcuafinmesv[]=$varcuafinalizadames;
                                                                            $totalcuafinv[]=$varcuafinalizada;

                                                                            $totalcuapenv[]=$varcuapendientes;

                                                                            if ($varcuaporcentaje < 33) {
                                                                              $color3='danger';
                                                                              
                                                                            } elseif ($varcuaporcentaje > 33 && $varcuaporcentaje < 70) {
                                                                              $color3='warning';
                                                                            } else {
                                                                              $color3='success';
                                                                            }

                                                                     

                                                        $lista3.= "
                                                        <tr class='odd gradeX'>
                                                          <td><strong>$varcuadrilla</strong></td>
                                                          <td style='color:red;'><strong>$varcuaasignada</strong></td>
                                                          <td style='color:red;'><strong>$varcuadescargada</strong></td>
                                                          <td style='color:red;'><strong>$varcuaexportada</strong></td>
                                                          <td style='color:blue;'><strong>$varcuafinalizadadia</strong></td>
                                                          <td style='color:blue;'><strong>$varcuafinalizadames</strong></td>
                                                          <td style='color:blue;'><strong>$varcuafinalizada</strong></td>
                                                          <td class='center'><div class='progress progress-striped active'>
                                                                                  <div class='progress-bar progress-bar-$color3' style='width: $varcuaporcentaje%'>".number_format($varcuaporcentaje,2)."%</div>
                                                                                 </div></td>
                                                         
                                                          <td style='color:red;'><strong>$varcuapendientes</strong></td>

                                                        </tr>";

                                                        } 

                                                         $lista3.= "</tbody>


                                                      <tfoot >
                                                        <tr>";

                                                        $totalcuaasig=(!empty($totalcuaasigv))?array_sum($totalcuaasigv):0;
                                                        $totalcuades=(!empty($totalcuadesv))?array_sum($totalcuadesv):0;
                                                        $totalcuaexp=(!empty($totalcuaexpov))?array_sum($totalcuaexpov):0;

                                                        $totalcuafindia=(!empty($totalcuafindiav))?array_sum($totalcuafindiav):0;
                                                        $totalcuafinmes=(!empty($totalcuafinmesv))?array_sum($totalcuafinmesv):0;
                                                        $totalcuafin=(!empty($totalcuafinv))?array_sum($totalcuafinv):0;
                                                        $totalcuapen=(!empty($totalcuapenv))?array_sum($totalcuapenv):0;

                                                          $lista3.= "<td style='text-align:right;' colspan='1'><strong>Total</strong></td>
                                                          <td><strong>$totalcuaasig</strong></td>
                                                          <td><strong>$totalcuades</strong></td>
                                                          <td><strong>$totalcuaexp</strong></td>
                                                          <td><strong>$totalcuafindia</strong></td>
                                                          <td><strong>$totalcuafinmes</strong></td>
                                                          <td><strong>$totalcuafin</strong></td>
                                                          <td><strong></strong></td>
                                                          <td><strong>$totalcuapen</strong></td>
                                                        </tr>
                                                      </tfoot>


                                                      </table>
                                                      </div>
                                                      </div>

                                            <div class='tab-pane cont tablaspecial' id='pers3b'>
                                            <span class='ciclo_loading7' style=''><img src='../js/jquery.select2/select2-spinner.gif'></span>
                                            <div id='tab3b'>

                                            </div>           
                                            </div>

                                            <div class='tab-pane cont tablaspecial' id='pers3c'>
                                            <span class='ciclo_loading7' style=''><img src='../js/jquery.select2/select2-spinner.gif'></span>           
                                            <div id='tab3c'>

                                            </div> 
                                            </div>



                                            <div class='tab-pane cont tablaspecial' id='pers3d'>
                                            <span class='ciclo_loading7' style=''><img src='../js/jquery.select2/select2-spinner.gif'></span>           
                                            <div id='tab3d'>

                                            </div> 
                                            </div>


                                            </div>


                                          </div>
                                        </div>

                                        
                                      </div>  

                    
                                </div>
                            </div>  
 








        ";




$lista4="







            <div class='panel-group accordion accordion-semi' id='accordion7' style='margin-bottom:0px;'>
                      <div class='panel panel-default' style='margin-bottom:0px;'>
                        <div class='panel-heading success'>
                          <h4 class='panel-title'>
                            <a data-toggle='collapse' data-parent='#accordion7' href='#ac7-1'>
                                <i class='fa fa-angle-right'></i> Perspectiva Gestores Comerciales
                            </a>
                          </h4>
                        </div>
                        <div id='ac7-1' class='panel-collapse collapse in'>
                          <div class='panel-body'>

                                     <div class='col-sm-12 col-md-12'>
                                        <div class='tab-container'>
                                          <ul class='nav nav-tabs'>
                                            <li class='active'><a href='#pers4a' data-toggle='tab'>Gestores</a></li>
                                            <li><a href='#pers4b' onclick='showMonAvPers4b()' data-toggle='tab'>Ciclo</a></li>
                                            <li><a href='#pers4c' onclick='showMonAvPers4c()' data-toggle='tab'>Sectores</a></li>
                                          </ul>
                                          <div class='tab-content'>

                                            <div class='tab-pane active cont' id='pers4a'>";
                                              
                                                      $lista4.="<div class='table-responsive'>
                                                      <table class='table table-bordered tableWithFloatingHeader' id='datatablepers4a' >
                                                      <thead class='primary-emphasis'>
                                                      <tr>
                                                          <th class='text-center primary-emphasis-dark'>Actividad</th>
                                                          <th class='text-center primary-emphasis-dark'>Sub Actividad</th>
                                                          
                                                          <th class='text-center primary-emphasis-dark'>Generadas</th>
                                                          <th class='text-center primary-emphasis-dark'>Finalizadas</th>
                                                          <th class='text-center danger-emphasis'>Pendientes</th>
                                                          <th class='text-center primary-emphasis-dark' style='width:15%;'>% Avance</th>
                                                          <th class='text-center primary-emphasis-dark'>Total Finalizadas</th>
                                                          <th class='text-center danger-emphasis-dark'>% Total Avance</th>
                                                          <th class='text-center danger-emphasis-dark'>Pendientes Final</th>

                                                      </tr>
                                                      </thead>
                                                      <tbody>

                                                      </tbody>
                                                      <tfoot >
                                                      <tr>
                                                        <td style='text-align:right;' colspan='2'><strong>Total</strong></td>
                                                       </tr>
                                                      </tfoot>


                                                      </table>
                                                      </div>
                                                      </div>

                                            <div class='tab-pane cont' id='pers4b'>
                                            <span class='ciclo_loading6' style=''><img src='../js/jquery.select2/select2-spinner.gif'></span>
                                            <div id='tab4b'>

                                            </div>           
                                            </div>

                                            <div class='tab-pane cont' id='pers4c'>
                                            <span class='ciclo_loading6' style=''><img src='../js/jquery.select2/select2-spinner.gif'></span>           
                                            <div id='tab4c'>

                                            </div> 
                                            </div>


                                            </div>


                                          </div>
                                        </div>

                                        
                                      </div>  

                    
                                </div>
                            </div>  
 







         </div>        
        </div>
        </div>
        ";



echo $lista;
echo $lista2;
echo $lista3;
/*
echo $lista4;
*/
?>



  <script type="text/javascript">

        function UpdateTableHeaders() {
            $("div.divTableWithFloatingHeader").each(function() {
                var originalHeaderRow = $(".tableFloatingHeaderOriginal", this);
                var floatingHeaderRow = $(".tableFloatingHeader", this);
                var offset = $(this).offset();
                var scrollTop = $(window).scrollTop();

                var navegador = navigator.userAgent;
                var inicioScroll = scrollTop-30;
                var marginheader = 30;
                if (navigator.userAgent.indexOf('MSIE') !=-1) {
                 
                } else if (navigator.userAgent.indexOf('Firefox') !=-1) {
                  inicioScroll = scrollTop-50;
                  marginheader = 40;
                } else if (navigator.userAgent.indexOf('Chrome') !=-1) {
                  inicioScroll = scrollTop-30;
                  var marginheader = 50;
                } else if (navigator.userAgent.indexOf('Opera') !=-1) {
                  inicioScroll = scrollTop-30;
                  marginheader = 40;
                } else {
                
                }

                if (( inicioScroll > offset.top ) && (scrollTop < offset.top + $(this).height())) {
                    floatingHeaderRow.css("visibility", "visible");
                    floatingHeaderRow.css("top", (Math.min(scrollTop - offset.top, $(this).height() - floatingHeaderRow.height()))+ marginheader  + "px");

                    // Copy cell widths from original header
                    $("th", floatingHeaderRow).each(function(index) {
                        var cellWidth = $("th", originalHeaderRow).eq(index).css('width');
                        var color = $("th", originalHeaderRow).eq(index).css('background-color');
                        
                        $(this).css('width', cellWidth);
                        $(this).css('background-color', color);
                    });

                    // Copy row width from whole table
                    floatingHeaderRow.css("width", $(this).css("width"));
                    floatingHeaderRow.css("background-color", $(this).css("background-color"));
                }
                else {
                    floatingHeaderRow.css("visibility", "hidden");
                    floatingHeaderRow.css("top", "0px");
                    
                }
            });
        }

        $(document).ready(function() {
            $("table.tableWithFloatingHeader").each(function() {
                $(this).wrap("<div class=\"divTableWithFloatingHeader\" style=\"position:relative\"></div>");

                var originalHeaderRow = $("tr:first", this)
                originalHeaderRow.before(originalHeaderRow.clone());
                var clonedHeaderRow = $("tr:first", this)

                clonedHeaderRow.addClass("tableFloatingHeader");
                clonedHeaderRow.css("position", "absolute");
                clonedHeaderRow.css("top", "0px");
                clonedHeaderRow.css("left", $(this).css("margin-left"));
                clonedHeaderRow.css("visibility", "hidden");
                clonedHeaderRow.css("z-index", "5");
                
                originalHeaderRow.addClass("tableFloatingHeaderOriginal");
            });

            UpdateTableHeaders();
            $(window).scroll(UpdateTableHeaders);
            $(window).resize(UpdateTableHeaders);
        });
</script>