<?php
date_default_timezone_set('America/Lima');
//require_once "../conexion.php";
require_once(APP."View/Main/conexion.ctp");


function tiempo($str, $t1, $t2 = false) {
	// Si nos dan un segundo parámetros calculamos el tiempo entre dos fechas
	$t = $t1 - ($t2 ? $t2 : time ());
	
	// Un array con todos los reemplazos que vamos a usar
	$p = array (
			'{s}' => 1,
			'{i}' => 60,
			'{h}' => 60 * 60,
			'{d}' => 60 * 60 * 24,
			'{w}' => 60 * 60 * 24 * 7,
			'{m}' => 60 * 60 * 24 * 30,
			'{y}' => 60 * 60 * 24 * 365 
	);
	
	// Obtenemos todos los tiempos que fueron proveídos en la string
	preg_match_all ( "/\{[sihdwmy]\}/", $str, $ma );
	
	// Creamos un array ordenado del mayor al menor tiempo requerido
	$found = Array ();
	foreach ( $ma [0] as &$m ) {
		$found [$m] = $p [$m];
	}
	arsort ( $found );
	
	// Reemplazamos la string con los tiempos
	foreach ( $found as $i => &$fo ) {
		$str = str_replace ( $i, ( int ) ($t / $fo), $str );
		$t = $t % $fo;
	}
	
	return $str;
}

$str_where = " ";
$str_where2 = " ";
$str_fecha_liquidacion = "";

if(isset($_GET['criterios'])) {
	$criterio = $_GET['criterios'];
}else{
	$criterio = '';
	$str_where2 .= " AND NomEstado in ('Asignado','Descargado','Exportado')";
}

if(strpos($criterio, 'Estado') !== false){
	if(isset($_GET['tags']) && $_GET['tags']!='') {
		$str_estados = "'".str_replace(",","','",$_GET['tags'])."'";
		if(strpos($str_estados, 'Descargado') !== false){
			$str_where2 .= " AND (NomEstado in (".$str_estados.") OR CI.IdEstado=9)";
		}else if(strpos($str_estados, 'Finalizado') !== false){
			$str_where2 .= " AND (NomEstado in (".$str_estados.") AND CI.IdEstado!=9)";
		}else{
			$str_where2 .= " AND (NomEstado in (".$str_estados."))";
		}	
	}
}
if(strpos($criterio, 'Ubicacion') !== false){
	if(isset($_GET['unidadneg']) && $_GET['unidadneg']!='0') {
		$str_where .= " AND IdUnidadNegocio = ".$_GET['unidadneg'];
		$str_where2 .= " AND IdUnidadNegocio = ".$_GET['unidadneg'];
	}
	if(isset($_GET['centroservicio']) && $_GET['centroservicio']!='0') {
		$str_where .= " AND IdCentroServicio = ".$_GET['centroservicio'];
		$str_where2 .= " AND IdCentroServicio = ".$_GET['centroservicio'];
	}
}
if(strpos($criterio, 'PeriodoLiquidacion') !== false){
	if(isset($_GET['periodoliquidacion']) && $_GET['periodoliquidacion']!='' && $_GET['periodoliquidacion']!='0') {
		$arr_periodo = explode('-',$_GET['periodoliquidacion']);
		$str_fecha_liquidacion = " AND extract('year' from FechaAtencion) = ".$arr_periodo[0];
		$str_fecha_liquidacion .= " AND extract('month' from FechaAtencion) = ".$arr_periodo[1];
		$str_where2 .= $str_fecha_liquidacion;
	}
}
if(strpos($criterio, 'RangoFechaA') !== false){
	if(isset($_GET['dpt_date_start'])) {
		$str_where2 .= " AND FechaAsignado >= '".$_GET['dpt_date_start']."'";
	}
	if(isset($_GET['dpt_date_end'])) {
		$str_where2 .= " AND FechaAsignado <= '".$_GET['dpt_date_end']." 23:59:59'";
	}
}
if(strpos($criterio, 'RangoFechaF') !== false){
	if(isset($_GET['dpt_date_start_finalizado'])) {
		$str_where2 .= " AND FechaAtencion >= '".$_GET['dpt_date_start_finalizado']."'";
	}
	if(isset($_GET['dpt_date_end_finalizado'])) {
		$str_where2 .= " AND FechaAtencion <= '".$_GET['dpt_date_end_finalizado']." 23:59:59'";
	}
}
if(strpos($criterio, 'Suministro') !== false){
	if(isset($_GET['txt_suministro']) && $_GET['txt_suministro']!='') {
		$str_where2 .= " AND suministro = ".$_GET['txt_suministro'];
	}
}
if(strpos($criterio, 'OT') !== false){
	if(isset($_GET['txt_orden_trabajo']) && $_GET['txt_orden_trabajo']!='') {
		$str_where2 .= " AND co.IdOrdenTrabajo = '".$_GET['txt_orden_trabajo']."'";
	}
}
if(strpos($criterio, 'FH') !== false){
	$str_fecha_finalizado_hoy = " AND FechaAtencion >= '".date('Y-m-d')." 00:00:00'";
	$str_where2 .= $str_fecha_finalizado_hoy." AND NomEstado in ('Finalizado')";
}
if(strpos($criterio, 'GH') !== false){
	$str_where2 .= " AND co.FechaGeneracion >= '".date('Y-m-d')." 00:00:00'";
}
if(strpos($criterio, 'Tecnico') !== false){
	if(isset($_GET['tecnico']) && $_GET['tecnico']!='') {
		$str_where2 .= " AND ci.IdUsuario = ".$_GET['tecnico'];
	}
}
if(strpos($criterio, 'I') !== false){
	$str_where2 .= " AND (select count(*) as infructuosa from ComMovTab_Detalle_Intervencion di inner join
ComMovTab_MaestrosIntervencion mi on di.IdMaestrosIntervencion=mi.IdMaestrosIntervencion
where di.IdIntervencion=Ci.IdIntervencion and  IdentificadorMaestroIntervencion like 'LabelInfructuosa%') > 0 ";
}
if(strpos($criterio, 'TT') !== false){
	if(isset($_GET['cbo_tiempotranscurrido']) && $_GET['cbo_tiempotranscurrido']!='') {
		$str_where2 .= " AND DATE_PART('day',NOW()-cast(FechaGeneracion as timestamp)) > ".$_GET['cbo_tiempotranscurrido'];
	}
}

if(isset($_GET['ida']))
{


     $id=$_GET["ida"];

     if ($id==100){
      $flage="display:none;";
      $flaga="";
      $lsubactividad=1;



      $query = pg_query($conexion, "select
      CO.IdOrden as IdOrden,
      CI.IdIntervencion as IdIntervencion,
      CO.IdUnidadNegocio as IdUnidadNegocio,
      CO.UnidadNegocio as UnidadNegocio,
      CO.IdCentroServicio as IdCentroServicio,
      CO.SubActividad as SubActividad,
      TIS.IdTipoIntervencion as TipoIntervencion,
      CI.IdEstado as IdEstado,
      cast(CO.FechaGeneracion as timestamp) as NuevaFecha,
      ME.NomEstado as NomEstado,
      GMP.NomPersona as NomPersona,
      GMP.ApePersona as ApePersona,
      CI.IdUsuario as IdUsuario,
      CU.NomUsuario as NomUsuario,
      CO.IdOrdenTrabajo as IdOrdenTrabajo,
      CO.Observacion as Observacion,
      CO.NroAtencion as NroAtencion,
      CI.FechaAtencion as FechaAtencion,
      CI.FechaAtencion as FechaAtencion1,
      CO.NroReclamo as NroReclamo,
      CO.Suministro as Suministro,
      CO.UsuarioGenero as UsuarioGenero,
      CO.FechaGeneracion as FechaGeneracion,
      CO.NombreSuministro as NombreSuministro,
      CO.DireccionSuministro as DireccionSuministro,
      CO.SerieMedidor as SerieMedidor,
      CO.Sector as Sector,
      0 as infructuosa
      from ComMovTab_Intervencion CI
      join ComMovTab_Orden CO on CI.IdOrden = CO.IdOrden
      left outer join ComMovTab_TipoIntervencion TIS on CO.SubActividad=TIS.DetalleTipoIntervencion
      join ComMovTab_Documento CD on CO.IdDocumento = CD.IdDocumento
      left outer join ComMovTab_TipoIntervencion TI on CD.IdTipoIntervencion = TI.IdTipoIntervencion
      join ComMovTab_MaestrosEstado ME on CI.IdEstado = ME.IdEstado
      join ComMovTab_Usuario CU on CI.IdUsuario = CU.IdUsuario
      join GlobalMaster_Persona GMP on CU.IdPersona = GMP.IdPersona
      where
	  co.SubActividad not in (select DetalleTipoIntervencion from ComMovTab_TipoIntervencion)
	  and co.UsuarioGenero not like 'Pexport%'
	  and co.Condicion = '0'
	  and co.FechaGeneracion > '2014-10-01 16:23:19' ".$str_where);




     } else {

     $flage='';
     $flaga="display:none;";
     $lsubactividad=0;
     $query = pg_query("select
      CO.IdOrden as IdOrden,
      CI.IdIntervencion as IdIntervencion,
      CO.SubActividad as SubActividad,
      CO.IdUnidadNegocio as IdUnidadNegocio,
      CO.UnidadNegocio as UnidadNegocio,
      CO.IdCentroServicio as IdCentroServicio,
      TIS.IdTipoIntervencion as TipoIntervencion,
      CI.IdEstado as IdEstado,
      cast(CO.FechaGeneracion as timestamp) as NuevaFecha,
      ME.NomEstado as NomEstado,
      GMP.NomPersona as NomPersona,
      GMP.ApePersona as ApePersona,
      CI.IdUsuario as IdUsuario,
      CU.NomUsuario as NomUsuario,
      CO.IdOrdenTrabajo as IdOrdenTrabajo,
      CO.Observacion as Observacion,
      CO.NroAtencion as NroAtencion,
      CI.FechaAtencion as FechaAtencion1,
      cast(CI.FechaAtencion as varchar)as FechaAtencion,
      CO.NroReclamo as NroReclamo,
      CO.Suministro as Suministro,
      CO.UsuarioGenero as UsuarioGenero,
      CO.FechaGeneracion as FechaGeneracion,
      CO.NombreSuministro as NombreSuministro,
      CO.DireccionSuministro as DireccionSuministro,
      CO.SerieMedidor as SerieMedidor,
      CO.Sector as Sector,
      0 as infructuosa
      from ComMovTab_Intervencion CI
      join ComMovTab_Orden CO on CI.IdOrden = CO.IdOrden
      join ComMovTab_TipoIntervencion TIS on CO.SubActividad=TIS.DetalleTipoIntervencion
      join ComMovTab_Documento CD on CO.IdDocumento = CD.IdDocumento
      join ComMovTab_TipoIntervencion TI on CD.IdTipoIntervencion = TI.IdTipoIntervencion
      join ComMovTab_MaestrosEstado ME on CI.IdEstado = ME.IdEstado
      join ComMovTab_Usuario CU on CI.IdUsuario = CU.IdUsuario
      join GlobalMaster_Persona GMP on CU.IdPersona = GMP.IdPersona
      where co.Condicion = '0' and co.UsuarioGenero not like 'Pexport%' and (TIS.IdTipoIntervencion=$id or TI.IdTipoIntervencion=$id) ".$str_where2);

      }

}
else
{
     $id=100;
      $query = pg_query($conexion, "select
      CO.IdOrden as IdOrden,
      CI.IdIntervencion as IdIntervencion,
      CO.SubActividad as SubActividad,
      CO.IdUnidadNegocio as IdUnidadNegocio,
      CO.UnidadNegocio as UnidadNegocio,
      CO.IdCentroServicio as IdCentroServicio,
      TIS.IdTipoIntervencion as TipoIntervencion,
      CI.IdEstado as IdEstado,
      cast(CO.FechaGeneracion as timestamp) as NuevaFecha,
      ME.NomEstado as NomEstado,
      GMP.NomPersona as NomPersona,
      GMP.ApePersona as ApePersona,
      CI.IdUsuario as IdUsuario,
      CU.NomUsuario as NomUsuario,
      CO.IdOrdenTrabajo as IdOrdenTrabajo,
      CO.Observacion as Observacion,
      CO.NroAtencion as NroAtencion,
      CI.FechaAtencion as FechaAtencion,
      CI.FechaAtencion as FechaAtencion1,
      CO.NroReclamo as NroReclamo,
      CO.Suministro as Suministro,
      CO.UsuarioGenero as UsuarioGenero,
      CO.FechaGeneracion as FechaGeneracion,
      CO.NombreSuministro as NombreSuministro,
      CO.DireccionSuministro as DireccionSuministro,
      CO.SerieMedidor as SerieMedidor,
      CO.Sector as Sector,
      (select count(*) as infructuosa from ComMovTab_Detalle_Intervencion di inner join 
      ComMovTab_MaestrosIntervencion mi on di.IdMaestrosIntervencion=mi.IdMaestrosIntervencion
      where di.IdIntervencion=Ci.IdIntervencion and  IdentificadorMaestroIntervencion like 'LabelInfructuosa%') as infructuosa
      from ComMovTab_Intervencion CI
      join ComMovTab_Orden CO on CI.IdOrden = CO.IdOrden
      join ComMovTab_TipoIntervencion TIS on CO.SubActividad=TIS.DetalleTipoIntervencion
      join ComMovTab_Documento CD on CO.IdDocumento = CD.IdDocumento
      join ComMovTab_TipoIntervencion TI on CD.IdTipoIntervencion = TI.IdTipoIntervencion
      join ComMovTab_MaestrosEstado ME on CI.IdEstado = ME.IdEstado
      join ComMovTab_Usuario CU on CI.IdUsuario = CU.IdUsuario
      join GlobalMaster_Persona GMP on CU.IdPersona = GMP.IdPersona
      where TI.IdTipoIntervencion=1 ".$str_where2);
}


$lista="<table class='table table-bordered' id='example'>
                   <thead class='primary-emphasis'>
                      <th>#</th>
                      <th>Sub Actividad</th>
                      <th>Estado</th>
                      <th>Transcurrido</th>
					  <th>Resultado</th>
                      <th>Tecnico</th>
                      <th>Fecha Eje</th>
                      <th>OT</th>
                      <th>Generador</th>
                      <th>Suministro</th>
                      <th>Fecha Gen</th>
                      <th>Sector</th>
					  <th>Cliente</th>
                  </thead>
                  <tbody>";


                    while ($retorno = pg_fetch_object($query)){
                       $varo=$retorno->idorden;
                       $vari=$retorno->idintervencion;
                       $var=$retorno->subactividad;

                       $var31=utf8_decode($retorno->idestado);
                       $var322=utf8_decode($retorno->nomestado);
                       $var32 = substr("$var322", 0,1);

                       $var44=utf8_decode($retorno->nompersona);
                       $var43=utf8_decode($retorno->apepersona);
                       $var33=utf8_decode($retorno->idusuario);
                       $var34=utf8_decode($retorno->nomusuario);
                       $var2=utf8_decode($retorno->idordentrabajo);
                       $var88=utf8_decode($retorno->observacion);
                       $var21=utf8_decode($retorno->nroatencion);

                       $var211=utf8_encode($retorno->fechaatencion);
                       $fecha_atencion_time = strtotime($var211);
                       
                       $var22=utf8_decode($retorno->nroreclamo);
                       $var3=utf8_decode($retorno->suministro);
                       $var4=utf8_encode($retorno->fechageneracion);
                       $var41=utf8_encode($retorno->sector);

                       $varnf=utf8_decode($retorno->nuevafecha);
                       $standard = strtotime($varnf);
                       $varnf=date("Y-m-d g:i:s a", $standard);

                       $var5nombre=utf8_decode($retorno->nombresuministro);
                       $var5 = explode(" ", $var5nombre);
                       $var5 = $var5[0]; 


                       $var6=utf8_encode($retorno->direccionsuministro);
                       $var7=utf8_decode($retorno->seriemedidor);

                       $var8=$retorno->usuariogenero;
                       
                       $var_infructuosa = $retorno->infructuosa;
                       if($var31==15 or $var31==14){
                       	if($var_infructuosa){
                       		$infructuosa = "Infructuosa";
                       		$label_infructuosa = "Infructuosa";
                       		$flag_infructuosa = "btn-warning";
                       	}else{
                       		$infructuosa = "OK";
                       		$label_infructuosa = "OK";
                       		$flag_infructuosa = "btn-success";
                       	}
                       }else{
                       	$infructuosa = "Pendiente";
                       	$label_infructuosa = "Pendiente";
                       	$flag_infructuosa = "";
                       }

                       if($var31==15 or $var31==14){
                           $varTime = tiempo("{d}d {h}h{i}m", $fecha_atencion_time, strtotime("$varnf"));
                       }else{
                           $varTime = tiempo("{d}d {h}h{i}m", time(), strtotime("$varnf"));
                       }



                       $lista.= "<tr class='odd gradeX'>
                                  <td>$vari</td>
                       			  <td>$var</td>
                                  <td>$var32</td>
                                  <td>$varTime</td>
                                  <td>$infructuosa</td>
                                  <td>$var44 $var43</td>
                                  <td>$var211</td>
                                  <td>$var2</td>
                                  <td>$var8</td>
                                  <td>$var3</td>
                                  <td>$var4</td>
                                  <td>$var41</td>
                                  <td>$var5nombre</td>
                                </tr>";
                                    
                        
                       }
                  $lista.="</tbody>
                </table>";

echo $lista;

?>
