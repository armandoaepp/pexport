<?php
date_default_timezone_set('America/Lima');
//require_once "../conexion.php";
require_once(APP."View/Main/conexion.ctp");

$fechaactual=date("Y-m-d");
$mes=date("m");

$usu=$_GET["idusu"];
$est=$_GET["idest"];
$m=$_GET["m"];

$ini=$_GET["idini1"];
$fin=$_GET["idfin1"];

if ($est==15 && $m==2){
  $agree="and cast(ci.FechaAtencion as timestamp) between '$ini' and '$fin'";
  } elseif ($est==15 && $m==3){
    $agree="and extract('month' from ci.FechaAtencion)=$mes";
  } else {
    $agree="";
  }

$query2 = pg_query("select IdUsuario, NomPersona, ApePersona from GlobalMaster_Persona p inner JOIN ComMovTab_Usuario u on p.IdPersona = u.IdPersona");


$lista="
<div class='alert alert-success alert-white rounded'>
								<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>x</button>
								<div class='icon'><i class='fa fa-check'></i></div>
								<strong>Correcto!</strong> <span id = 'respuesta'></span>
							 </div>
							 
<form role='form'  id='formreasigna' method='post'>
              

              <div class='form-group'>";

                                $lista.= "<label>Busca Persona:</label> </br>
                                <select class='select2' id='IdUsuario' name='IdUsuario'>";
                                while ($retorno = pg_fetch_object($query2)){
                              $lista.= "<option value='$retorno->idusuario'>$retorno->nompersona  $retorno->apepersona</option>";
                            }
                                $lista.= "</select>
                                

              <button class='btn btn-warning' type='button' onclick='reasignatecnico()'><i class='fa fa-floppy-o'></i> Grabar</button>
              </div>

";


/*
$lista="

<input id='check-all' type='checkbox' name='checkall' style='position: absolute; opacity: 1;' />

<div class='items products'>
        <div class='item'>
          <div><input type='checkbox' name='c[]'' style='position: absolute; opacity: 1;' /> </div>
          <div>
            <span class='date pull-right price'>$25</span>
            <img class='product-image' src='http://placehold.it/50/CCCCCC/808080'>
            <h4 class='from'>Product 1</h4>
            <p class='msg'>This is the product description.</p>
          </div>
        </div>   
        <div class='item'>
          <div><input type='checkbox' name='c[]' class='tech' style='position: absolute; opacity: 1;' /> </div>
          <div>
            <span class='date pull-right price'>$38</span>
            <img class='product-image' src='http://placehold.it/50/CCCCCC/808080'>
            <h4 class='from'>Product 2</h4>
            <p class='msg'>This is the product description.</p>
          </div>
        </div>   
          
      </div>";
*/


$query3 = pg_query("select 
co.IdOrden as IdOrden,
ci.IdIntervencion as IdIntervencion,
ci.IdEstado as IdEstado,
co.SubActividad as SubActividad,
co.FechaGeneracion as FechaGeneracion,
co.Sector as Sector,
co.Suministro as Suministro,
co.IdOrdenTrabajo as IdOrdenTrabajo,
gp.NomPersona as NomPersona,
gp.ApePersona as ApePersona
from ComMovTab_Orden co 
inner join ComMovTab_Intervencion ci 
on co.IdOrden = ci.IdOrden
inner join ComMovTab_Usuario cu
on ci.IdUsuario = cu.IdUsuario
inner join GlobalMaster_Persona gp
on cu.IdPersona = gp.IdPersona
where ci.IdEstado=$est and ci.IdUsuario=$usu and co.Condicion='0'
and co.SubActividad in (select DetalleTipoIntervencion from ComMovTab_TipoIntervencion)
$agree
");


$lista.="<table class='table table-bordered' id='example'>
                   <thead class='primary-emphasis'>
                      <th><input type ='checkbox' id='all' name='all' value='' ></th>
                      <th>Sub Actividad</th>
                      <th>Fecha Gen</th>
                      <th>OT</th>
                      <th>Suministro</th>
                      <th>Sector</th>
                  </thead>
                  <tbody>";

                    
                    while ($retorno = pg_fetch_object($query3)){
                       $varo=$retorno->idorden;
                       $vari=$retorno->idintervencion;
                       $var=$retorno->subactividad;
                       $var44=utf8_decode($retorno->nompersona);
                       $var43=utf8_decode($retorno->apepersona);
                       $var2=utf8_decode($retorno->idordentrabajo);
                       $var3=utf8_decode($retorno->suministro);
                       $var4=utf8_encode($retorno->fechageneracion);
                       $var41=utf8_encode($retorno->sector);

  

                       
                       $lista.= "<tr class='odd gradeX'>
                                  <td><input type ='checkbox' id='idchk[]' name='idchk[]' value='$vari' ></td>
                       			      <td>$var</td>
                                  <td>$var4</td>
                                  <td>$var2</td>
                                  <td>$var3</td>
                                  <td>$var41</td>
                
   
                                </tr>";
                                    
                        
                       }
                       
                  $lista.="</tbody>
                </table>

                </form>
                ";

echo $lista;

?>

<script>
function reasignatecnico(){
	var count_checked = $("[name='idchk[]']:checked").length; // count the checked
	if(count_checked == 0) {
		alert("Por favor, seleccione una Orden(s) que desea reasignar.");
		return false;
	} 
	if(count_checked == 1) {
		var rpta = confirm("Seguro que desea reasignar esta Orden?");
		if(rpta){
			var idintervencion = new Array();
			$( "input[name='idchk[]']:checked" ).each( function() {
				idintervencion.push($( this ).val());
			});
			$.ajax({
				type: "POST",
				url: FULL_URL+"main/asignar_orden",
				dataType: 'html',
				data: 'idintervencion='+idintervencion+'&idusuario='+$('#IdUsuario').val(),
				success: function(data){
					$('#respuesta').html(data)
				}
			});
		}
		return false;		
	} else {
		var rpta = confirm("Seguro que quieres reasignar estas Ordenes?");
		if(rpta){
			var idintervencion = new Array();
			$( "input[name='idchk[]']:checked" ).each( function() {
				idintervencion.push($( this ).val());
			});
			$.ajax({
				type: "POST",
				url: FULL_URL+"main/asignar_orden",
				dataType: 'html',
				data: 'idintervencion='+idintervencion+'&idusuario='+$('#IdUsuario').val(),
				success: function(data){
					$('#respuesta').html(data)
				}
			});
		}
	}
}
</script>