<?php
ini_set('max_execution_time', 300);
date_default_timezone_set('America/Lima');
//require "../conexion.php";
require_once(APP."View/Main/conexion.ctp");

$query = pg_query("select
CMI.IdMaestrosIntervencion as IdMaestrosIntervencion,
CMI.IdentificadorMaestroIntervencion as var1,
CMI.EtiquetaMaestroIntervencion as EtiquetaMaestroIntervencion,
CMI.TipoDatoMaestroIntervencion as TipoDatoMaestroIntervencion,
CMI.DetalleMaestroIntervencion as DetalleMaestroIntervencion,
CMI.LongitudTipoDatoMaestrosIntervencion as var5,
CMI.ControlObjetoMaestroIntervencion as var6,
CMI.EstiloObjetoMaestroIntervencion as var7,
CMI.PosicionMaestroIntervencion as PosicionMaestroIntervencion,
CMI.RaizMaestroIntervencion as RaizMaestroIntervencion,
CMIS.IdentificadorMaestroIntervencion as var11,
CMI.DownMovil as DownMovil 
from ComMovTab_MaestrosIntervencion CMI left join 
ComMovTab_MaestrosIntervencion CMIS on CMI.RaizMaestroIntervencion::INT4 = CMIS.IdMaestrosIntervencion

");

$querycomb1 = pg_query("select 
IdMaestrosIntervencion as var1,
IdentificadorMaestroIntervencion as var2 from ComMovTab_MaestrosIntervencion");

$lista="
    <div class='row'>
      <div class='col-sm-12 col-md-12'>
        <div class='block-flat'>


        <div class='panel-group accordion accordion-semi' id='accordion4'>
					  <div class='panel panel-default'>
						<div class='panel-heading success'>
						  <h4 class='panel-title'>
							<a data-toggle='collapse' data-parent='#accordion4' href='#ac4-1'>
								<i class='fa fa-angle-right'></i> Agregar Elementos de Formulario
							</a>
						  </h4>
						</div>
						<div id='ac4-1' class='panel-collapse collapse in'>
						  <div class='panel-body'>
							
						          <div class='content'>

							          <form role='form' id='formmaestrosintervencion' method='post' style='width:50%;'> 
								            <div class='form-group'>
								              <input type='hidden' id='idmaestrointervencion' name='idmaestrosintervencion'>
								              <label>Identificador Campo Asignacion</label> <input type='text' id='identificadormaestro' name='identificadormaestro' placeholder='Nombre del Campo Asignacion' class='form-control'>
								            </div>

							            	<div class='form-group'>";
											 $lista.= " <label>Pertenece A : </label> 
											 <select class='form-control' id='raizmaestrosintervencion' name='raizmaestrosintervencion'>
											 <option value='0'>Ninguno</option>
											 ";
														 while ($retornodet = pg_fetch_object($querycomb1)){
						                                    $idmiq=$retornodet->var1;
						                                    $ideq=$retornodet->var2;

						                                    $lista.="<option value='$idmiq'>$ideq</option>"; 
						                                   }
											 $lista.= "</select>
											 </div>


											 <div class='form-group'>
								              <label>Etiqueta</label> <input type='text' id='etiquetamaestrosintervencion' name='etiquetamaestrosintervencion' placeholder='Etiqueta en Formulario' class='form-control'>
								             </div>

								             <div class='form-group'>
								              <label>Tipo de Dato</label> <input type='text' id='tipodatomaestrosintervencion' name='tipodatomaestrosintervencion' placeholder='Tipo de dato' class='form-control'>
								             </div>

								             <div class='form-group'>
								              <label>Longitud</label> <input type='text' id='longitudmaestrosintervencion' name='longitudmaestrosintervencion' placeholder='Longitud de tipo de dato' class='form-control'>
								             </div>

								             <div class='form-group'>
								              <label>Descripcion</label> <input type='text' id='descripcionmaestrosintervencion' name='descripcionmaestrosintervencion' placeholder='Descripcion del Elemento' class='form-control'>
								             </div>

								             <div class='form-group'>
								              <label>Objeto Formulario</label> <input type='text' id='objetoformulariomaestrosintervencion' name='objetoformulariomaestrosintervencion' placeholder='Objeto de Formulario' class='form-control'>
								             </div>

								             <div class='form-group'>
								              <label>Estilo Objeto</label> <input type='text' id='estiloobjetomaestrosintervencion' name='estiloobjetomaestrosintervencion' placeholder='Estilo Formulario' class='form-control'>
								             </div>

								             <div class='form-group'>
								              <label>Orden Formulario</label> <input type='text' id='ordenformulariomaestrosintervencion' name='ordenformulariomaestrosintervencion' placeholder='Posicion en Formulario' class='form-control'>
								             </div>
				



							              <button class='btn btn-danger' type='button' onclick='crudmaestrosintervencion(1)'>Grabar</button>
							              <button class='btn btn-default' type='button'>Cancelar</button>
							          </form>
						          
						          </div>

						  </div>
						</div>
					  </div>
		</div>


		

						      <div class='row'>
						        <div class='col-md-12'>
						          <div class='block-flat'>

						            

						            <div class='content'>
						            <div class='tabla'>

						          
						            <div class='farmsmallasig4'>

						              <div class='table-responsive'>
						              <form role='form' id='formmaestrosintervencioncrud' method='post'> 
						                <table class='no-border' id='datatablecrudmaestrosintervencion'>

						                  <thead class='primary-emphasis'>
						                    <tr>
						                      <th></th>
						                      <th>Codigo</th>
						                      <th>Identificador</th>
						                      <th>Pertenece A</th>
						                      <th>Etiqueta</th>
						                      <th>Tipo de Dato</th>
						                      <th style='width:5%;'>Orden</th>
						                      <th>Descripcion</th>
						                      <th style='width:5%;'>Longitud</th>
						                      <th style='width:5%;'>Objeto Clonar</th>
						                      <th style='width:5%;'>Estilo</th>
						                      
						                    </tr>
						                  </thead>
						                  <tbody class='no-border'>";


						                    while ($retorno = pg_fetch_object($query)){

						                       $var=utf8_decode($retorno->idmaestrosintervencion);
						                       $var1=utf8_decode($retorno->var1);
						                       $var2=utf8_decode($retorno->etiquetamaestrointervencion);
						                       $var3=utf8_decode($retorno->tipodatomaestrointervencion);
						                       $var4=utf8_decode($retorno->detallemaestrointervencion);
						                       $var5=utf8_decode($retorno->var5);
						                       $var6=utf8_decode($retorno->var6);
						                       $var7=utf8_decode($retorno->var7);
						                       $var8=utf8_decode($retorno->posicionmaestrointervencion);
						                       $var9=utf8_decode($retorno->raizmaestrointervencion);
						                       $var11=utf8_decode($retorno->var11);





						                       $lista.= "<tr class='odd gradeX'>
						                                  <input type ='hidden' id='idmaestrosintervencioncrud[]' name='idmaestrosintervencioncrud[]' value='$var'>
						                                  <td style=''><input type ='checkbox' id='idmaestrosintervencioncrudchk[]' name='idmaestrosintervencioncrudchk[]' value='$var'></td>
						                                  <td style='width:3%;'>$var</td>


						                                  <td style=''>
						                                  <input style='width:100%;' type ='text' id='identificadormaestrointervencioncrud[]' name='identificadormaestrointervencioncrud[]' value='$var1'>
						                                  </td>

						                                  <td style=''><select style='width:100%;' class='form-control' id='raizmaestrointervencioncrud[]' name='raizmaestrointervencioncrud[]' value='$var11' >
						                                      <option value='$var9'>$var11</option>
						                                      ";
						                                  $querydetasi1 = pg_query("select 
						                                   IdMaestrosIntervencion as var1,
						                                   IdentificadorMaestroIntervencion as var2 from ComMovTab_MaestrosIntervencion where IdMaestrosIntervencion !=$var9");
						                                   while ($retornodet = pg_fetch_object($querydetasi1)){
						                                    $idmi=$retornodet->var1;
						                                    $ide=$retornodet->var2;

						                                    $lista.="<option value='$idmi'>$ide</option>"; 
						                                   } 
						                                  $lista.="</select></td>


						                                  <td style=''>
						                                  <input style='width:100%;' type ='text' id='etiquetamaestrointervencioncrud[]' name='etiquetamaestrointervencioncrud[]' value='$var2'>
						                                  </td>

						                                  <td style=''>
						                                  <input style='width:100%;' type ='text' id='tipodatomaestrointervencioncrud[]' name='tipodatomaestrointervencioncrud[]' value='$var3'>
						                                  </td>

						                                  <td style=''>
						                                  <input style='width:100%;' type ='text' id='posicionmaestrointervencioncrud[]' name='posicionmaestrointervencioncrud[]' value='$var8'>
						                                  </td>

						                                  <td style=''>
						                                  <input style='width:100%;' type ='text' id='detallemaestrointervencioncrud[]' name='detallemaestrointervencioncrud[]' value='$var4'>
						                                  </td>

						                                  <td style=''>
						                                  <input style='width:100%;' type ='text' id='longitudtipodatomaestrosintervencioncrud[]' name='longitudtipodatomaestrosintervencioncrud[]' value='$var5'>
						                                  </td>

						                                  <td style=''>
						                                  <input style='width:100%;' type ='text' id='controlobjetomaestrointervencioncrud[]' name='controlobjetomaestrointervencioncrud[]' value='$var6'>
						                                  </td>

						                                  <td style=''>
						                                  <input style='width:100%;' type ='text' id='estiloobjetomaestrointervencioncrud[]' name='estiloobjetomaestrointervencioncrud[]' value='$var7'>
						                                  </td>

						                                  

						                                  

						                                 
						                                </tr>";
						                       }
						                  $lista.="</tbody>

						                </table>
						                <button class='btn btn-success' type='button' onclick='crudmaestrosintervencion(2)'>Grabar</button>
						                <button class='btn btn-danger' type='button' onclick='crudmaestrosintervencion(3)'>Eliminar</button>
						              </form>              
						              </div>

						              </div>
						              </div>
						              
						            </div>
						          </div>        
						        </div>

						      </div>      

        </div>				
      </div>
      

	</div>";

echo $lista;
?>