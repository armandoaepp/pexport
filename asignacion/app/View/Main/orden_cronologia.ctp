<?php
date_default_timezone_set('America/Lima');
setlocale(LC_ALL,"es_ES");
//require "../conexion.php";
require_once(APP."View/Main/conexion.ctp");

if(isset($_GET['id_intervencion'])){
	$id_intervencion = $_GET['id_intervencion'];
}else{
	$id_intervencion = '';
}

$query = pg_query("select cmi.*, cmo.*, gp.*, cmi.IdEstado as IdEstado, ctis.DetalleTipoIntervencion as tipopadre
		from ComMovTab_Intervencion cmi
		inner join ComMovTab_Orden cmo on cmi.IdOrden = cmo.IdOrden
		inner join ComMovTab_Usuario cu on cmi.IdUsuario=cu.IdUsuario 
		inner join GlobalMaster_Persona gp on cu.IdPersona = gp.IdPersona
		inner join ComMovTab_TipoIntervencion cti	on cmo.SubActividad = cti.DetalleTipoIntervencion 
		inner join ComMovTab_TipoIntervencion ctis on cti.IdTipoIntervencionSup=ctis.IdTipoIntervencion
		where cmi.IdIntervencion=".$id_intervencion." ORDER BY FechaAtencion DESC");

$rs=pg_num_rows($query);
//debug($rs);
if ($rs > 0) {
	$retornocom = pg_fetch_object($query );
		//debug($retornocom);exit();
}else{
	echo 'Orden no disponible';
	exit();
}
//debug($retornocom);exit();
$querydet = pg_query("select
		* from ComMovTab_MovimientosIntervencion
		where ValorMovimiento='13' and IdIntervencion = $id_intervencion");

while ($retornodet = pg_fetch_object($querydet)){
	$fecha_descargado = $retornodet->fechahora;
}
$querydet = pg_query("select
		* from ComMovTab_MovimientosIntervencion
		where ValorMovimiento='14' and IdIntervencion = $id_intervencion");

while ($retornodet = pg_fetch_object($querydet)){
	$fecha_exportado = $retornodet->fechahora;
}
$querydet = pg_query("select
		* from ComMovTab_MovimientosIntervencion
		where ValorMovimiento='15' and IdIntervencion = $id_intervencion");

while ($retornodet = pg_fetch_object($querydet)){
	$fecha_finalizado = $retornodet->fechahora;
}

$query_persona_genero = pg_query("select * from GlobalMaster_Persona where (ApePersona || ' ' || NomPersona) like '%".$retornocom->usuariogenero."%' limit 1");

$rsx=pg_num_rows($query_persona_genero);

if ($rsx > 0) {
	$retorno_persona_genero = pg_fetch_object($query_persona_genero );
	//print_r($retornocom);
	$str_persona_genero = $retornocom->usuariogenero;
	$foto_persona_genero = $retorno_persona_genero->foto;
}else{
	$str_persona_genero = $retornocom->usuariogenero;
	$foto_persona_genero = '../images/persona/001.png';
}

$consultaacta=pg_query("select * from (
		select
		cdi.IdIntervencion,
		case
		when cmi2.IdMaestrosIntervencion in (select IdMaestrosIntervencion from ComMovTab_MaestrosIntervencion where RaizMaestroIntervencion::INT4=0) then cmi2.IdMaestrosIntervencion
		when cmi4.IdMaestrosIntervencion in (select IdMaestrosIntervencion from ComMovTab_MaestrosIntervencion where RaizMaestroIntervencion::INT4=0) then cmi4.IdMaestrosIntervencion
		else cmi5.IdMaestrosIntervencion end as Id,
		case
		when cmi2.IdMaestrosIntervencion in (select IdMaestrosIntervencion from ComMovTab_MaestrosIntervencion where RaizMaestroIntervencion::INT4=0) then cmi2.EtiquetaMaestroIntervencion
		when cmi4.IdMaestrosIntervencion in (select IdMaestrosIntervencion from ComMovTab_MaestrosIntervencion where RaizMaestroIntervencion::INT4=0) then cmi4.EtiquetaMaestroIntervencion
		else cmi5.EtiquetaMaestroIntervencion end as Supremo,
		case
		when cmi2.IdMaestrosIntervencion in (select IdMaestrosIntervencion from ComMovTab_MaestrosIntervencion where RaizMaestroIntervencion::INT4=0) then cmi2.PosicionMaestroIntervencion
		when cmi4.IdMaestrosIntervencion in (select IdMaestrosIntervencion from ComMovTab_MaestrosIntervencion where RaizMaestroIntervencion::INT4=0) then cmi4.PosicionMaestroIntervencion
		else cmi5.PosicionMaestroIntervencion end as Posicion,
		cmi4.EtiquetaMaestroIntervencion as c4,
		cmi3.EtiquetaMaestroIntervencion as c3,
		cmi2.IdMaestrosIntervencion as id2,
		cmi2.EtiquetaMaestroIntervencion as c2,
		cdi.IdMaestrosIntervencion as id1,
		cmi.EtiquetaMaestroIntervencion as c1,
		cmi.TipoDatoMaestroIntervencion as Tipo,
		case
		when cmi.TipoDatoMaestroIntervencion ='multi' or cmi.TipoDatoMaestroIntervencion ='multimaestro'  then
		(select EtiquetaMaestroIntervencion from ComMovTab_MaestrosIntervencion where IdMaestrosIntervencion::TEXT like cdi.DetalleIntervencion)
		else cdi.DetalleIntervencion end as Total,
		cdi.DetalleIntervencion as Detalle
		from ComMovTab_Detalle_Intervencion cdi
		left join ComMovTab_MaestrosIntervencion cmi on cdi.IdMaestrosIntervencion = cmi.IdMaestrosIntervencion
		left join ComMovTab_MaestrosIntervencion cmi2 on cmi.RaizMaestroIntervencion::INT4 = cmi2.IdMaestrosIntervencion
		left join ComMovTab_MaestrosIntervencion cmi3 on cmi2.RaizMaestroIntervencion::INT4 = cmi3.IdMaestrosIntervencion
		left join ComMovTab_MaestrosIntervencion cmi4 on cmi3.RaizMaestroIntervencion::INT4 = cmi4.IdMaestrosIntervencion
		left join ComMovTab_MaestrosIntervencion cmi5 on cmi4.RaizMaestroIntervencion::INT4 = cmi5.IdMaestrosIntervencion
		where cdi.IdIntervencion = $id_intervencion and
		case
		when cmi2.IdMaestrosIntervencion in (select IdMaestrosIntervencion from ComMovTab_MaestrosIntervencion where RaizMaestroIntervencion::INT4=0) then cmi2.IdMaestrosIntervencion
		when cmi4.IdMaestrosIntervencion in (select IdMaestrosIntervencion from ComMovTab_MaestrosIntervencion where RaizMaestroIntervencion::INT4=0) then cmi4.IdMaestrosIntervencion
		else cmi5.IdMaestrosIntervencion end = 40
		and cmi.TipoDatoMaestroIntervencion = 'fotografia'
		) as sc
		order by Posicion");

$fotos = array();
while ($retorno_data = pg_fetch_object($consultaacta)){
	//echo '<br>';
	//print_r($retorno_data);
	$fotos[] = $retorno_data->total;
}

$consulta_obs=pg_query("select * from (
		select
		cdi.IdIntervencion,
		case
		when cmi2.IdMaestrosIntervencion in (select IdMaestrosIntervencion from ComMovTab_MaestrosIntervencion where RaizMaestroIntervencion::INT4=0) then cmi2.IdMaestrosIntervencion
		when cmi4.IdMaestrosIntervencion in (select IdMaestrosIntervencion from ComMovTab_MaestrosIntervencion where RaizMaestroIntervencion::INT4=0) then cmi4.IdMaestrosIntervencion
		else cmi5.IdMaestrosIntervencion end as Id,
		case
		when cmi2.IdMaestrosIntervencion in (select IdMaestrosIntervencion from ComMovTab_MaestrosIntervencion where RaizMaestroIntervencion::INT4=0) then cmi2.EtiquetaMaestroIntervencion
		when cmi4.IdMaestrosIntervencion in (select IdMaestrosIntervencion from ComMovTab_MaestrosIntervencion where RaizMaestroIntervencion::INT4=0) then cmi4.EtiquetaMaestroIntervencion
		else cmi5.EtiquetaMaestroIntervencion end as Supremo,
		case
		when cmi2.IdMaestrosIntervencion in (select IdMaestrosIntervencion from ComMovTab_MaestrosIntervencion where RaizMaestroIntervencion::INT4=0) then cmi2.PosicionMaestroIntervencion
		when cmi4.IdMaestrosIntervencion in (select IdMaestrosIntervencion from ComMovTab_MaestrosIntervencion where RaizMaestroIntervencion::INT4=0) then cmi4.PosicionMaestroIntervencion
		else cmi5.PosicionMaestroIntervencion end as Posicion,
		cmi4.EtiquetaMaestroIntervencion as c4,
		cmi3.EtiquetaMaestroIntervencion as c3,
		cmi2.IdMaestrosIntervencion as id2,
		cmi2.EtiquetaMaestroIntervencion as c2,
		cdi.IdMaestrosIntervencion as id1,
		cmi.EtiquetaMaestroIntervencion as c1,
		cmi.TipoDatoMaestroIntervencion as Tipo,
		case
		when cmi.TipoDatoMaestroIntervencion ='multi' or cmi.TipoDatoMaestroIntervencion ='multimaestro'  then
		(select EtiquetaMaestroIntervencion from ComMovTab_MaestrosIntervencion where IdMaestrosIntervencion::TEXT like cdi.DetalleIntervencion)
		else cdi.DetalleIntervencion end as Total,
		cdi.DetalleIntervencion as Detalle
		from ComMovTab_Detalle_Intervencion cdi
		left join ComMovTab_MaestrosIntervencion cmi on cdi.IdMaestrosIntervencion = cmi.IdMaestrosIntervencion
		left join ComMovTab_MaestrosIntervencion cmi2 on cmi.RaizMaestroIntervencion::INT4=cmi2.IdMaestrosIntervencion
		left join ComMovTab_MaestrosIntervencion cmi3 on cmi2.RaizMaestroIntervencion::INT4=cmi3.IdMaestrosIntervencion
		left join ComMovTab_MaestrosIntervencion cmi4 on cmi3.RaizMaestroIntervencion::INT4=cmi4.IdMaestrosIntervencion
		left join ComMovTab_MaestrosIntervencion cmi5 on cmi4.RaizMaestroIntervencion::INT4=cmi5.IdMaestrosIntervencion
		where cdi.IdIntervencion = $id_intervencion and
		case
		when cmi2.IdMaestrosIntervencion in (select IdMaestrosIntervencion from ComMovTab_MaestrosIntervencion where RaizMaestroIntervencion::INT4=0) then cmi2.IdMaestrosIntervencion
		when cmi4.IdMaestrosIntervencion in (select IdMaestrosIntervencion from ComMovTab_MaestrosIntervencion where RaizMaestroIntervencion::INT4=0) then cmi4.IdMaestrosIntervencion
		else cmi5.IdMaestrosIntervencion end in (74,150,151,152,153,154,155,156) 
) as sc
		order by Posicion");

$tipo_obs = '';
$obs = '';
$var2 = array();
$busca = array();
while ($retorno_data = pg_fetch_object($consulta_obs)){
	$c1=$retorno_data->c1;
	//echo '<br>';
	//print_r($retorno_data);
	$Tipo = $retorno_data->tipo;
	$Total = $retorno_data->total;
	if($Tipo=='areatexto'){
		$obs=$Total;
		$od=1;
			
	} elseif ($Tipo=='multimaestro'){
		$tipo_obs="$Total";
		$tipo_obs= strtoupper($tipo_obs);
	} else {
		$var2[]= strtoupper($Total);
		$extra= substr($c1,0,4);
		$busca[] = $extra;
	}
}
$obs = ($tipo_obs!=''?$tipo_obs.' - ':' ').$obs;
$str_obs = str_replace($busca, $var2, $obs);


$consulta_firma_cliente=pg_query("select * from (
		select
		cdi.IdIntervencion,
		case
		when cmi2.IdMaestrosIntervencion in (select IdMaestrosIntervencion from ComMovTab_MaestrosIntervencion where RaizMaestroIntervencion::INT4=0) then cmi2.IdMaestrosIntervencion
		when cmi4.IdMaestrosIntervencion in (select IdMaestrosIntervencion from ComMovTab_MaestrosIntervencion where RaizMaestroIntervencion::INT4=0) then cmi4.IdMaestrosIntervencion
		else cmi5.IdMaestrosIntervencion end as Id,
		case
		when cmi4.IdMaestrosIntervencion in (select IdMaestrosIntervencion from ComMovTab_MaestrosIntervencion where RaizMaestroIntervencion::INT4=0) then cmi4.EtiquetaMaestroIntervencion
		when cmi2.IdMaestrosIntervencion in (select IdMaestrosIntervencion from ComMovTab_MaestrosIntervencion where RaizMaestroIntervencion::INT4=0) then cmi2.EtiquetaMaestroIntervencion
		else cmi5.EtiquetaMaestroIntervencion end as Supremo,
		case
		when cmi2.IdMaestrosIntervencion in (select IdMaestrosIntervencion from ComMovTab_MaestrosIntervencion where RaizMaestroIntervencion::INT4=0) then cmi2.PosicionMaestroIntervencion
		when cmi4.IdMaestrosIntervencion in (select IdMaestrosIntervencion from ComMovTab_MaestrosIntervencion where RaizMaestroIntervencion::INT4=0) then cmi4.PosicionMaestroIntervencion
		else cmi5.PosicionMaestroIntervencion end as Posicion,
		cmi4.EtiquetaMaestroIntervencion as c4,
		cmi3.EtiquetaMaestroIntervencion as c3,
		cmi2.IdMaestrosIntervencion as id2,
		cmi2.EtiquetaMaestroIntervencion as c2,
		cdi.IdMaestrosIntervencion as id1,
		cmi.EtiquetaMaestroIntervencion as c1,
		cmi.TipoDatoMaestroIntervencion as Tipo,
		case
		when cmi.TipoDatoMaestroIntervencion ='multi' or cmi.TipoDatoMaestroIntervencion ='multimaestro'  then
		(select EtiquetaMaestroIntervencion from ComMovTab_MaestrosIntervencion where IdMaestrosIntervencion::TEXT like cdi.DetalleIntervencion)
		else cdi.DetalleIntervencion end as Total,
		cdi.DetalleIntervencion as Detalle
		from ComMovTab_Detalle_Intervencion cdi
		left join ComMovTab_MaestrosIntervencion cmi on cdi.IdMaestrosIntervencion = cmi.IdMaestrosIntervencion
		left join ComMovTab_MaestrosIntervencion cmi2 on cmi.RaizMaestroIntervencion::INT4=cmi2.IdMaestrosIntervencion
		left join ComMovTab_MaestrosIntervencion cmi3 on cmi2.RaizMaestroIntervencion::INT4=cmi3.IdMaestrosIntervencion
		left join ComMovTab_MaestrosIntervencion cmi4 on cmi3.RaizMaestroIntervencion::INT4=cmi4.IdMaestrosIntervencion
		left join ComMovTab_MaestrosIntervencion cmi5 on cmi4.RaizMaestroIntervencion::INT4=cmi5.IdMaestrosIntervencion
		where cdi.IdIntervencion = $id_intervencion and
		case
		when cmi2.IdMaestrosIntervencion in (select IdMaestrosIntervencion from ComMovTab_MaestrosIntervencion where RaizMaestroIntervencion::INT4=0) then cmi2.IdMaestrosIntervencion
		when cmi4.IdMaestrosIntervencion in (select IdMaestrosIntervencion from ComMovTab_MaestrosIntervencion where RaizMaestroIntervencion::INT4=0) then cmi4.IdMaestrosIntervencion
		else cmi5.IdMaestrosIntervencion end = 40
		and cmi.TipoDatoMaestroIntervencion = 'firma'
) as sc
		order by Posicion");

$firma_cliente = '';
while ($retorno_data_firma_cliente = pg_fetch_object($consulta_firma_cliente)){
	//echo '<br>';
	//print_r($retorno_data_firma_cliente);
	$firma_cliente = $retorno_data_firma_cliente->total;
}
?>

<ul class='timeline'>
	<li><i class='fa fa-comment'></i> <span class='date'><?php echo date('d M',strtotime($retornocom->fechageneracion));?></span>
		<div class='content'>
			<?php if($foto_persona_genero!=''){?>
			<a class='image-zoom' href='<?php echo $foto_persona_genero;?>' style="float: left;"><img
				src='<?php echo $foto_persona_genero;?>' class='img-thumbnail'
				style='height: 80px;' /> </a>
			<?php }?>
			<p style="margin-top:13px;">
				<strong><?php echo utf8_decode($str_persona_genero);?></strong> atendio el reclamo.
				<br>
				<small><?php echo date('Y-m-d H:i:s',strtotime($retornocom->fechageneracion));?></small>
			</p>
			<i class='fa fa-paperclip pull-right'></i>
			<p>
				Se genero la siguiente orden de trabajo en Electronorte.
			</p>
			<table class="table-ordenes-deatlle">
			<thead>
			<tr>
			<th>OT</th>
			<th>Nro Reclamo</th>
			<th>Suministro</th>
			<th>Cliente</th>
			<th>Direcci&oacute;n</th>
			</tr>
			</thead>
			<tbody>
			<tr>
			<td><?php echo $retornocom->idordentrabajo;?></td>
			<td><?php echo $retornocom->nroreclamo;?></td>
			<td><?php echo $retornocom->suministro;?></td>
			<td><?php echo $retornocom->nombresuministro;?></td>
			<td><?php echo $retornocom->direccionsuministro;?></td>
			</tr>
			<tr>
			<td>Actividad</td>
			<td><?php echo $retornocom->tipopadre;?></td>
			<td>SubActividad</td>
			<td colspan="2"><?php echo $retornocom->subactividad;?></td>
			</tr>
			</tbody>
			</table>
		</div>
	</li>

	<li><i class='fa fa-level-down purple'></i><span class='date'><?php echo date('d M',strtotime($retornocom->fechaasignado));?></span>
		<div class='content'>
			<p>
				<strong>ACSION</strong> Recibe y Asigna la orden.
			</p>
			<small><?php echo date('Y-m-d H:i:s',strtotime($retornocom->fechaasignado));?></small>
			<table class="table-ordenes-deatlle">
			<thead>
			<tr>
			<th>Responsable</th>
			<th>Supervisor</th>
			<th>Ejecutor (T&eacute;cnico)</th>
			</tr>
			</thead>
			<tbody>
			<tr>
			<td><?php echo $retornocom->supervisor;?></td>
			<td><?php echo $retornocom->responsable;?></td>
			<td><?php echo $retornocom->nompersona.' '.$retornocom->apepersona;?></td>
			</tr>
			</tbody>
			</table>
		</div>
	</li>

	<?php 
	if($retornocom->downmovil=='1'){
	?>
	<li><i class='fa fa-flag' style="background: rgb(253, 156, 53);"></i><span class='date'><?php echo date('d M',strtotime($fecha_descargado));?></span>
		<div class='content'>
			<p>
				T&eacute;cnico <strong><?php echo $retornocom->nompersona.' '.$retornocom->apepersona;?></strong> ha descargado y esta listo para ejecutar la orden de trabajo.
			</p>
			<small><?php echo date('Y-m-d H:i:s',strtotime($fecha_descargado));?></small>
		</div>
	</li>
	<?php }?>

	<?php 
	if($retornocom->idestado=='9' || $retornocom->idestado=='14' || $retornocom->idestado=='15'){
	?>
	
	<li><i class='fa fa-ambulance red'></i> <span class='date'><?php echo date('d M',strtotime($retornocom->fechaatencion));?></span>
		<div class='content'>
			<i class='fa fa-paperclip pull-right'></i>
			<p>
				T&eacute;cnico <strong><?php echo $retornocom->nompersona.' '.$retornocom->apepersona;?></strong><br />Realiza visita y captura evidencia del
				acto.
			</p>
			<small><?php echo date('Y-m-d H:i:s',strtotime($retornocom->fechaatencion));?></small><br>
			<?php if(isset($fotos[0])){?>
			<a class='image-zoom' href='data:image/jpeg;base64,<?php echo $this->Image->applyWatermark(str_replace('data:image/jpeg;base64,','',$fotos[0]),date('Y-m-d H:i:s',strtotime($retornocom->fechaatencion)));?>'><img
				src='data:image/jpeg;base64,<?php echo $this->Image->applyWatermark(str_replace('data:image/jpeg;base64,','',$fotos[0]),date('Y-m-d H:i:s',strtotime($retornocom->fechaatencion)));?>' class='img-thumbnail'
				style='height: 120px;' />
			</a>
			<?php }?>
			<?php if(isset($fotos[1])){?>
			<a class='image-zoom'
				href='data:image/jpeg;base64,<?php echo $this->Image->applyWatermark(str_replace('data:image/jpeg;base64,','',$fotos[1]),date('Y-m-d H:i:s',strtotime($retornocom->fechaatencion)));?>'><img
				src='data:image/jpeg;base64,<?php echo $this->Image->applyWatermark(str_replace('data:image/jpeg;base64,','',$fotos[1]),date('Y-m-d H:i:s',strtotime($retornocom->fechaatencion)));?>' class='img-thumbnail'
				style='height: 120px;' /> </a>
			<?php }?>
			<?php if(isset($retornocom->foto)){?>
			<a class='image-zoom' href='<?php echo FULL_URL.$retornocom->foto;?>' title='T&eacute;cnico: <?php echo $retornocom->nompersona.' '.$retornocom->apepersona;?>'><img
				src='<?php echo FULL_URL.$retornocom->foto;?>' class='img-thumbnail'
				style='height: 120px; width:90px;' alt='T&eacute;cnico: <?php echo $retornocom->nompersona.' '.$retornocom->apepersona;?>' /> </a>
			<?php }else{?>
			<a class='image-zoom' href='<?php echo FULL_URL;?>images/persona/001.png' title='T&eacute;cnico: <?php echo $retornocom->nompersona.' '.$retornocom->apepersona;?>'><img
				src='<?php echo FULL_URL;?>images/persona/001.png' class='img-thumbnail'
				style='height: 120px; width:90px;' alt='T&eacute;cnico: <?php echo $retornocom->nompersona.' '.$retornocom->apepersona;?>' /> </a>
			<?php }?>
			<a class='image-zoom' href='<?php echo FULL_URL.$retornocom->firpersona;?>' title='T&eacute;cnico Firma: <?php echo $retornocom->nompersona.' '.$retornocom->apepersona;?>'><img
				src='<?php echo FULL_URL.$retornocom->firpersona;?>' class='img-thumbnail'
				style='height: 120px; width:90px;' alt='T&eacute;cnico Firma: <?php echo $retornocom->nompersona.' '.$retornocom->apepersona;?>' /> </a>
			<a class='image-zoom' href='<?php echo $firma_cliente;?>' title='Cliente: <?php echo $retornocom->nombresuministro;?>'><img
				src='<?php echo $firma_cliente;?>' class='img-thumbnail'
				style='height: 120px; width:90px;' alt='Cliente Firma: <?php echo $retornocom->nombresuministro;?>' /> </a>
			<?php if($retornocom->idestado=='9'){?>
			<p>
			<div class="alert alert-warning">
				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
				<i class="fa fa-warning sign"></i><strong>Alerta!</strong> Datos pendientes de env&iacute;o a <strong>ACSION</strong>.
			 </div>
			</p>
			<?php }?>
		</div>
	</li>
	<?php 
	}
	if($retornocom->idestado=='14' || $retornocom->idestado=='15'){
	?>
	
	<li><i class='fa fa-level-down green'></i> <span class='date'><?php echo date('d M',strtotime($fecha_exportado));?></span>
		<div class='content'>
			<p>
				T&eacute;cnico <strong><?php echo $retornocom->nompersona.' '.$retornocom->apepersona;?></strong><br />Envia los datos de la intervenci&oacute;n a <strong>ACSION</strong>.
			</p>
			<small><?php echo date('Y-m-d H:i:s',strtotime($fecha_exportado));?></small><br>
		</div>
	</li>
	<?php 
	}
	if($retornocom->idestado=='15'){
	?>

	<li><i class='fa fa-file red'></i> <span class='date'><?php echo date('d M',strtotime($fecha_finalizado));?></span>
		<div class='content'>
			<p>
				<strong>ACSION</strong> Genera acta de intervencion y retorna a
				Electronorte :
			</p>
			<small><?php echo date('Y-m-d H:i:s',strtotime($fecha_finalizado));?></small>
			<blockquote>
				<p>
					Observacion: <small><?php echo $str_obs;?></small>
			
			</blockquote>
		</div>
	</li>

	<li><i class='fa fa-envelope'></i><span class='date'><?php echo date('d M',strtotime($fecha_exportado));?></span>
		<div class='content'>
			<p>
				<strong>Cliente</strong> Puede visualizar su acta.<br>
				<a href="<?php echo FULL_URL;?>main/generaacta_monitorear?id=<?php echo $id_intervencion?>" target="_blank" title="ver acta">
						<i class="fa fa-file-text"></i> ver aqu&iacute;</a>
			</p>
		</div>
	</li>
	<?php }?>
</ul>
