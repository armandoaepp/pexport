<?php
date_default_timezone_set('America/Lima');
//require "../conexion.php";
require_once(APP."View/Main/conexion.ctp");
?>
<div class="">
    <div class="row">
      
      <div class="col-sm-12 col-md-12">
        <div class="block-flat">
          <div class="header">							
            <h3>Enviar Monitor de Avance</h3>
          </div>
          <div class="content">
            <form class="form-horizontal" role="form">
              <div class="form-group">
              <label for="inputEmail3" class="col-sm-2 control-label">Email(s)</label>
              <div class="col-sm-8">
                <input type="email" class="form-control" id="txt_emails" placeholder="email1@email.com, email2@email.com">
              </div>
              <button type="button" class="btn btn-primary" id="btn_enviar_email_monitor">Enviar</button>
              </div>

            </form>
          </div>
          <span class="email_loading" style="display: none;"><img src="../js/jquery.select2/select2-spinner.gif"></span>
        </div>				
      </div>
    </div>
</div>
<script>
$('#btn_enviar_email_monitor').click(function (){

	if($.trim($('#txt_emails').val())==''){
		alert('Por favor al menos ingrese un email.');
	}else{
	
	$('.email_loading').show();
	$('#btn_enviar_email_monitor').attr('disabled',true);
	$.ajax({
		url : FULL_URL+'main/envio_email_monitor_pdf_pg',
		dataType: 'json',
		type: "POST",
		data : { arr_emails: $('#txt_emails').val()},
		success:function(data, textStatus, jqXHR){
			if(data.success){
				//alert(data.msg);
				$.gritter.add({
			        title: 'Enviado',
			        text: data.msg,
			        class_name: 'success'
			      });
			}else{
				//alert(data.msg);
				$.gritter.add({
			        title: 'Error',
			        text: data.msg,
			        class_name: 'danger'
			      });
			}
			$('.email_loading').hide();
			$('#btn_enviar_email_monitor').attr('disabled',false);
			$('#mod-success3').modal('hide');
		}
	});
	
	}
});
</script>