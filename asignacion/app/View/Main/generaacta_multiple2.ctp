<?php
ini_set('max_execution_time', 300);
date_default_timezone_set('America/Lima');
//require "../conexion.php";
require_once(APP."View/Main/conexion.ctp");

//require_once "../createactas/dompdf_config.inc.php";
//spl_autoload_register('DOMPDF_autoload');

$fechaactual=date("Y-m-d");

if(isset($_GET["tipo"])){
	$tipo = $_GET["tipo"];
}else{
	$tipo = '';
}

if(isset($_GET["est"])){
	$est = $_GET["est"];
}else{
	$est = null;
}

if(isset($_GET["ini"])){
	$ini = $_GET["ini"];
}else{
	$ini = date('Y-m-d');
}

if(isset($_GET["fin"])){
	$fin = $_GET["fin"];
}else{
	$fin = date('Y-m-d');
}



if($tipo=='I'){
	//Infructuosas	
	$sqlvector=pg_query("select ci.IdIntervencion as IdIntervencion from ComMovTab_Intervencion ci
inner join ComMovTab_Orden co on ci.IdOrden=co.IdOrden
inner join ComMovTab_Detalle_Intervencion DI on DI.IdIntervencion=ci.IdIntervencion
where co.SubActividad in (select DetalleTipoIntervencion from ComMovTab_TipoIntervencion)
and co.Condicion='0' and cast(FechaAtencion as date) BETWEEN '$ini' and '$fin' and DI.IdMaestrosIntervencion in (
select IdMaestrosIntervencion from ComMovTab_MaestrosIntervencion
where IdentificadorMaestroIntervencion like 'LabelInfructuosa%')");
}elseif($tipo=='F'){
	//Finalizadas
	$sqlvector=pg_query("select ci.IdIntervencion as IdIntervencion from ComMovTab_Intervencion ci
			inner join ComMovTab_Orden co on ci.IdOrden=co.IdOrden
			where co.SubActividad in (select DetalleTipoIntervencion from ComMovTab_TipoIntervencion)
			and ci.IdEstado in (14,15) and co.Condicion='0' and cast(FechaAtencion as date) BETWEEN '$ini' and '$fin'");
}else{
	$sqlvector=pg_query("select IdIntervencion from ComMovTab_Intervencion where IdEstado = $est and cast(FechaAtencion as date) BETWEEN '$ini' and '$fin'");
}

$rs=pg_num_rows($sqlvector);
if ($rs == 0) {
	echo 'No hay datos';
	exit;
}

$code="";

while ($retornovector = pg_fetch_object($sqlvector)){
	    $id=$retornovector->idintervencion;

			$querytipo = pg_query("select cti.DetalleTipoIntervencion as tipohijo,ctis.DetalleTipoIntervencion as tipopadre,
						ci.IdUsuario,cu.NomUsuario as codtecnico,
						gp.NomPersona as nomtecnico,
						gp.ApePersona as apetecnico,
						gp.DniPersona as dnitecnico,
						gp.FirPersona as firmatecnico,
						ci.IdIntervencion,
						ci.CodigoActa,
						ci.FechaAtencion
						from ComMovTab_Intervencion ci join ComMovTab_Usuario cu
						on ci.IdUsuario=cu.IdUsuario join GlobalMaster_Persona gp
						on cu.IdPersona = gp.IdPersona join ComMovTab_Orden co
						on ci.IdOrden = co.IdOrden join ComMovTab_TipoIntervencion cti
						on co.SubActividad = cti.DetalleTipoIntervencion join ComMovTab_TipoIntervencion ctis
						on cti.IdTipoIntervencionSup=ctis.IdTipoIntervencion where ci.IdIntervencion=$id");
							            	$retornotipo = pg_fetch_object($querytipo);
							            	$var4tipohijo=$retornotipo->tipohijo;
							            	$var4tipopadre=$retornotipo->tipopadre;
							            	$nomtecnico=$retornotipo->nomtecnico;
							            	$apetecnico=$retornotipo->apetecnico;
							            	$codtecnico=$retornotipo->codtecnico;
							            	$dnitecnico=$retornotipo->dnitecnico;
							            	$firmatecnico=$retornotipo->firmatecnico;
							            	$acta=$retornotipo->codigoacta;
											$acta=trim($acta);
											$fechaatencion=$retornotipo->fechaatencion;


	    	$consultaacta=pg_query("select * from (
							select 
							cdi.IdIntervencion,
							case 
							when cmi2.IdMaestrosIntervencion in (select IdMaestrosIntervencion from ComMovTab_MaestrosIntervencion where RaizMaestroIntervencion::INT4=0) then cmi2.IdMaestrosIntervencion 
							when cmi4.IdMaestrosIntervencion in (select IdMaestrosIntervencion from ComMovTab_MaestrosIntervencion where RaizMaestroIntervencion::INT4=0) then cmi4.IdMaestrosIntervencion
							else cmi5.IdMaestrosIntervencion end as Id,
							case 
							when cmi2.IdMaestrosIntervencion in (select IdMaestrosIntervencion from ComMovTab_MaestrosIntervencion where RaizMaestroIntervencion::INT4=0) then cmi2.EtiquetaMaestroIntervencion 
							when cmi4.IdMaestrosIntervencion in (select IdMaestrosIntervencion from ComMovTab_MaestrosIntervencion where RaizMaestroIntervencion::INT4=0) then cmi4.EtiquetaMaestroIntervencion
							else cmi5.EtiquetaMaestroIntervencion end as Supremo,
							case 
							when cmi2.IdMaestrosIntervencion in (select IdMaestrosIntervencion from ComMovTab_MaestrosIntervencion where RaizMaestroIntervencion::INT4=0) then cmi2.PosicionMaestroIntervencion 
							when cmi4.IdMaestrosIntervencion in (select IdMaestrosIntervencion from ComMovTab_MaestrosIntervencion where RaizMaestroIntervencion::INT4=0) then cmi4.PosicionMaestroIntervencion
							else cmi5.PosicionMaestroIntervencion end as Posicion,
							cmi4.EtiquetaMaestroIntervencion as c4,
							cmi3.EtiquetaMaestroIntervencion as c3,
							cmi2.IdMaestrosIntervencion as id2,
							cmi2.EtiquetaMaestroIntervencion as c2,
							cdi.IdMaestrosIntervencion as id1,
							cmi.PosicionMaestroIntervencion as Posicion1,
							cmi.EtiquetaMaestroIntervencion as c1,
							cmi.TipoDatoMaestroIntervencion as Tipo,
							case
							when cmi.TipoDatoMaestroIntervencion ='multi' or cmi.TipoDatoMaestroIntervencion ='multimaestro'  then 
							(select EtiquetaMaestroIntervencion from ComMovTab_MaestrosIntervencion where IdMaestrosIntervencion::TEXT like cdi.DetalleIntervencion  )
							else cdi.DetalleIntervencion end as Total,
							cdi.DetalleIntervencion as Detalle
							from ComMovTab_Detalle_Intervencion cdi
							left join ComMovTab_MaestrosIntervencion cmi on cdi.IdMaestrosIntervencion = cmi.IdMaestrosIntervencion
							left join ComMovTab_MaestrosIntervencion cmi2 on cmi.RaizMaestroIntervencion::INT4=cmi2.IdMaestrosIntervencion
							left join ComMovTab_MaestrosIntervencion cmi3 on cmi2.RaizMaestroIntervencion::INT4=cmi3.IdMaestrosIntervencion
							left join ComMovTab_MaestrosIntervencion cmi4 on cmi3.RaizMaestroIntervencion::INT4=cmi4.IdMaestrosIntervencion
							left join ComMovTab_MaestrosIntervencion cmi5 on cmi4.RaizMaestroIntervencion::INT4=cmi5.IdMaestrosIntervencion
							where cdi.IdIntervencion = $id) as sc
							order by Posicion,Posicion1");
							
				
							$tabla="";
	    					$tabla1="";
	    					$tabla2="";
	    					$tabla3="";
	    					$tabla4="";
	    					$tabla5="";

	    					$tabla6="";
	    					$tabla66="";
	    					$tabla666="";
	    					$tabla6666="";
	    					$tabla66666="";
	    					$tabla666666="";

	    					$tabla7="";
	    					$tabla8="";
	    					$tabla9="";
	    					$tabla10="";

	    					$detalle="";
	    					$detalle1="";
	    					$detalle2="";

	    					$detalle7="";
	    					$detalle77="";

	    					$var2="";
	    					$extra="";
	    					$busca="";


	    					$var2= array();
	    					$od=2;

							while ($retornoacta = pg_fetch_object($consultaacta)){
	    					$Id=$retornoacta->id;
	    					$id1=$retornoacta->id1;
	    					$c1=$retornoacta->c1;
	    					$Total=$retornoacta->total;
	    					$Detalle=$retornoacta->detalle;
	    					$Tipo=$retornoacta->tipo;

		    					switch ($Id) {
		    						case 1:

		    				

		    							if ($id1==6) {
		    								$tabla.="<th>$c1</th>";
		    								$detalle.="<th>$nomtecnico</th>";
		    							} elseif ($id1==7) {
		    								$tabla.="<th>$c1</th>";
		    								$detalle.="<th>$fechaatencion</th>";
		    							} else {
		    								$tabla.="<th>$c1</th>";
		    							    $detalle.="<th>$Total</th>";
		    							}

		    							
		    							
		    				
		    							break;

		    						case 8:
		    							$tabla1.="<th>$c1</th>";
		    							$detalle1.="<th>$Total</th>";
		    							break;

		    						case 49:
		    							$tabla2.="<th>$c1</th>";
		    							$detalle2.="<th>$Total</th>";
		    							break;

		    						case 19:
		    							$tabla3.="<tr><td>$c1</td><td>$Detalle</td></tr>";
		    							break;

		    						case 13:
		    							$tabla4.="<tr><td>$c1</td><td>$Total</td></tr>";
		    							break;

		    						case 94:
		    							$tabla5.="<tr><td>$c1</td><td>$Total</td></tr>";
		    							break;

		    						case 33:
		    							if($id1==34){
		    							$ar6=$c1;
		    							$tabla6.="<td>$Total</td>";
		    							} elseif ($id1==35) {
		    								$ar66=$c1;
		    								$tabla66.="<td>$Total</td>";
		    							} elseif ($id1==36) {
		    								$ar666=$c1;
		    								$tabla666.="<td>$Total</td>";
		    							} elseif ($id1==37) {
		    								$ar6666=$c1;
		    								$tabla6666.="<td>$Total</td>";
		    							} elseif ($id1==38) {
		    								$ar66666=$c1;
		    								$tabla66666.="<td>$Total</td>";
		    							} elseif ($id1==39) {
		    								$ar666666=$c1;
		    								$tabla666666.="<td>$Total</td>";
		    							}


		    							break;

		    						case 146:
		    							$tabla8.="<tr><td>$c1</td><td>$Total</td></tr>";
		    							break;

		    						case 40:
		    							if($Tipo=='fotografia'){
		    								$Total="<img src='$Total'  width='200' height='170' border='1'>";
		    								$detalle77.="<th>$c1</th>";
		    								$detalle7.="<th>$Total</th>";
		    							} elseif ($Tipo=='firma') {
		    								$firma="$Total";
		    								$c1="";
		    								$Total="";
		    							} else {
		    								$tabla7.="<tr><td>$c1</td><td>$Total</td></tr>";
		    							}

		    							if($id1==45){
		    								$nombre="$Total";
		    							}elseif ($id1==46) {
		    								$dni="$Total";
		    							}

		    							
		    							
		    							
		    							break;
		    						
		    						case 74:

		    							if ($Tipo=='areatexto' || $Tipo=='multimaestro' || $Tipo=='cadena' || $Tipo=='numero' || $Tipo=='multi') {
		    								

		    								if($Tipo=='areatexto'){
			    							$obs=$Total;
			    							$tabla9.="$Total";
			    							$od=1;
			    								
			    							} elseif ($Tipo=='multimaestro'){
			    								$tipo="$Total";
			    								$tipo= strtoupper($tipo);
			    							} else {
			    								$var2[]= strtoupper($Total);
			    								$extra= substr($c1,0,4);
			    								$busca[] = $extra;
			    							}

		    							} else {
		    								$od=2;
		    							}
		    							
		    							break;

		    						case 150:
		    							if ($Tipo=='areatexto' || $Tipo=='multimaestro' || $Tipo=='cadena' || $Tipo=='numero' || $Tipo=='multi') {
		    								

		    								if($Tipo=='areatexto'){
			    							$obs=$Total;
			    							$tabla9.="$Total";
			    							$od=1;
			    								
			    							} elseif ($Tipo=='multimaestro'){
			    								$tipo="$Total";
			    								$tipo= strtoupper($tipo);
			    							} else {
			    								$var2[]= strtoupper($Total);
			    								$extra= substr($c1,0,4);
			    								$busca[] = $extra;
			    							}

		    							} else {
		    								$od=2;
		    							}
		    							
		    							break;

		    						case 151:
		    							if ($Tipo=='areatexto' || $Tipo=='multimaestro' || $Tipo=='cadena' || $Tipo=='numero' || $Tipo=='multi') {
		    								

		    								if($Tipo=='areatexto'){
			    							$obs=$Total;
			    							$tabla9.="$Total";
			    							$od=1;
			    								
			    							} elseif ($Tipo=='multimaestro'){
			    								$tipo="$Total";
			    								$tipo= strtoupper($tipo);
			    							} else {
			    								$var2[]= strtoupper($Total);
			    								$extra= substr($c1,0,4);
			    								$busca[] = $extra;
			    							}

		    							} else {
		    								$od=2;
		    							}
		    							
		    							break;

		    						case 152:
		    							if ($Tipo=='areatexto' || $Tipo=='multimaestro' || $Tipo=='cadena' || $Tipo=='numero' || $Tipo=='multi') {
		    								

		    								if($Tipo=='areatexto'){
			    							$obs=$Total;
			    							$tabla9.="$Total";
			    							$od=1;
			    								
			    							} elseif ($Tipo=='multimaestro'){
			    								$tipo="$Total";
			    								$tipo= strtoupper($tipo);
			    							} else {
			    								$var2[]= strtoupper($Total);
			    								$extra= substr($c1,0,4);
			    								$busca[] = $extra;
			    							}

		    							} else {
		    								$od=2;
		    							}
		    							
		    							break;

		    						case 153:
		    							if ($Tipo=='areatexto' || $Tipo=='multimaestro' || $Tipo=='cadena' || $Tipo=='numero' || $Tipo=='multi') {
		    								

		    								if($Tipo=='areatexto'){
			    							$obs=$Total;
			    							$tabla9.="$Total";
			    							$od=1;
			    								
			    							} elseif ($Tipo=='multimaestro'){
			    								$tipo="$Total";
			    								$tipo= strtoupper($tipo);
			    							} else {
			    								$var2[]= strtoupper($Total);
			    								$extra= substr($c1,0,4);
			    								$busca[] = $extra;
			    							}

		    							} else {
		    								$od=2;
		    							}
		    							
		    							break;

		    						case 154:
		    							if ($Tipo=='areatexto' || $Tipo=='multimaestro' || $Tipo=='cadena' || $Tipo=='numero' || $Tipo=='multi') {
		    								

		    								if($Tipo=='areatexto'){
			    							$obs=$Total;
			    							$tabla9.="$Total";
			    							$od=1;
			    								
			    							} elseif ($Tipo=='multimaestro'){
			    								$tipo="$Total";
			    								$tipo= strtoupper($tipo);
			    							} else {
			    								$var2[]= strtoupper($Total);
			    								$extra= substr($c1,0,4);
			    								$busca[] = $extra;
			    							}

		    							} else {
		    								$od=2;
		    							}
		    							
		    							break;

		    						case 155:
		    							if ($Tipo=='areatexto' || $Tipo=='multimaestro' || $Tipo=='cadena' || $Tipo=='numero' || $Tipo=='multi') {
		    								

		    								if($Tipo=='areatexto'){
			    							$obs=$Total;
			    							$tabla9.="$Total";
			    							$od=1;
			    								
			    							} elseif ($Tipo=='multimaestro'){
			    								$tipo="$Total";
			    								$tipo= strtoupper($tipo);
			    							} else {
			    								$var2[]= strtoupper($Total);
			    								$extra= substr($c1,0,4);
			    								$busca[] = $extra;
			    							}

		    							} else {
		    								$od=2;
		    							}
		    							
		    							break;

		    						case 250:
		    							if ($Tipo=='areatexto' || $Tipo=='multimaestro' || $Tipo=='cadena' || $Tipo=='numero' || $Tipo=='multi') {
		    								

		    								if($Tipo=='areatexto'){
			    							$obs=$Total;
			    							$tabla9.="$Total";
			    							$od=1;
			    								
			    							} elseif ($Tipo=='multimaestro'){
			    								$tipo="$Total";
			    								$tipo= strtoupper($tipo);
			    							} else {
			    								$var2[]= strtoupper($Total);
			    								$extra= substr($c1,0,4);
			    								$busca[] = $extra;
			    							}

		    							} else {
		    								$od=2;
		    							}
		    							
		    							break;

		    						case 156:
		    							if ($Tipo=='areatexto' || $Tipo=='multimaestro' || $Tipo=='cadena' || $Tipo=='numero' || $Tipo=='multi') {
		    								

		    								if($Tipo=='areatexto'){
			    							$obs=$Total;
			    							$tabla9.="$Total";
			    							$od=1;
			    								
			    							} elseif ($Tipo=='multimaestro'){
			    								$tipo="$Total";
			    								$tipo= strtoupper($tipo);
			    							} else {
			    								$var2[]= strtoupper($Total);
			    								$extra= substr($c1,0,4);
			    								$busca[] = $extra;
			    							}

		    							} else {
		    								$od=2;
		    							}
		    							
		    							break;
		    							

		    						default:
		    							$tabla10.="";
		    							break;
		    					}


	    					}



	    					/*TABLAS PRINCIPALES ENCABEZADO*/
	    					$tablafinal="<table border=1 style='width:100%; margin-top:10px; font-size:70%; text-align:left; border-collapse: collapse;'>
	    								 <tr>
										  <th colspan=7>DATOS GENERALES</th>
										 </tr>
										 <tr>
										  $tabla
										 </tr>
										 <tr>
										  $detalle
										 </tr>
										 <tr>
										 <td>Actividad</td>
										 <td colspan='2'>$var4tipopadre</td>
										 <td>Sub Actividad</td>
										 <td colspan='3'>$var4tipohijo</td>
										 </tr>
	    								 </table>";

	    					$tablafinal1="<table border=1 style='width:100%; margin-top:10px; font-size:70%; text-align:left; border-collapse: collapse;'>
	    								 <tr>
										  <th colspan=4>DATOS CLIENTE</th>
										 </tr>
										 <tr>
										  $tabla1
										 </tr>
										 <tr>
										  $detalle1
										 </tr>
	    								 </table>";

	    					$tablafinal2="<table border=1 style='width:100%; margin-top:10px; font-size:70%; text-align:left; border-collapse: collapse;'>
	    								 <tr>
										  <th colspan=4>DATOS PREDIO</th>
										 </tr>
										 <tr>
										  $tabla2
										 </tr>
										 <tr>
										  $detalle2
										 </tr>
	    								 </table>";
							
							/*TABLAS PRINCIPALES CUERPO*/

							$tablafinal3="<table border=1 style='width:100%; margin-top:10px; font-size:70%; text-align:left; border-collapse: collapse;'>
	    								 <tr>
										  <th colspan=2>DATOS SUMINISTRO</th>
										 </tr>
										 <tr>
										  <th>Datos</th>
										  <th>Detalle</th>
										 </tr>
										  $tabla3
	    								 </table>";

	    					$tablafinal4="<table border=1 style='width:100%; margin-top:10px; font-size:70%; text-align:left; border-collapse: collapse;'>
	    								 <tr>
										  <th colspan=2>DATOS MEDIDOR</th>
										 </tr>
										 <tr>
										  <th>Datos</th>
										  <th>Detalle</th>
										 </tr>
										  $tabla4
	    								 </table>";


	    					if ($tabla5!='') {
	    						$tablafinal5="<table border=1 style='width:100%; margin-top:10px; font-size:70%; text-align:left; border-collapse: collapse;'>
	    								 <tr>
										  <th colspan=2>PRUEBA MEDIDOR</th>
										 </tr>
										 <tr>
										  <th>Datos</th>
										  <th>Detalle</th>
										 </tr>
										  $tabla5
	    								 </table>";
	    					} else {
	    						$tablafinal5='';
	    					}


	    					$varfila="";
	    					if (isset($ar6)) {
	    						$varfila.="<tr><td>$ar6</td>$tabla6</tr>";
	    						} else {
	    							
	    						}
	    					if (isset($ar66)) {
	    							$varfila.="<tr><td>$ar66</td>$tabla66</tr>";
	    						} else {
	    							
	    						}

	    					if (isset($ar666)) {
	    							$varfila.="<tr><td>$ar666</td>$tabla666</tr>";
	    						} else {
	    							
	    						}

	    					if (isset($ar6666)) {
	    							$varfila.="<tr><td>$ar6666</td>$tabla6666</tr>";
	    						} else {
	    							
	    						}

	    					if (isset($ar66666)) {
	    							$varfila.="<tr><td>$ar66666</td>$tabla66666</tr>";
	    						} else {
	    							
	    						}

	    					if (isset($ar666666)) {
	    							$varfila.="<tr><td>$ar666666</td>$tabla666666</tr>";
	    						} else {
	    							
	    						}

	    					if ($tabla6!='' or $tabla6666!='') {
	    						$tablafinal6="<table border=1 style='width:100%; marggiin-top:-10px; font-size:70%; text-align:left; border-collapse: collapse;'>
	    								 <tr>
										  <th colspan=4>ARTEFACTOS / ARTEFACTOS QUEMADOS</th>
										 </tr>
										 <tr>
										  <th>Item</th>
										  <th>C</th>
										  <th>TA</th>
										  <th>F</th>
										 </tr>
											  $varfila	 
	    								 </table>";
	    					} else {
	    						$tablafinal6='';
	    					}
	    					
	    					$tablafinal8='';
	    					if ($tipo=='INFRUCTUOSA' or $tipo=='') {
	    						$tablafinal5='';
					    	    $tablafinal6='';
	    					} else {
								$tablafinal5=$tablafinal5;
								$tablafinal8=$tablafinal8;
								$tablafinal6=$tablafinal6;
	    					}






	    					$tablafinal7="<table border=1 style='width:100%; margin-top:10px; font-size:70%; text-align:left; border-collapse: collapse;'>
	    								 <tr>
										  <th colspan=2>VALIDACION</th>
										 </tr>
										 <tr>
										  <th>Datos</th>
										  <th>Detalle</th>
										 </tr>
										  $tabla7
										 <tr>
										  $detalle77
										 </tr>
										 <tr>
										  $detalle7
										 </tr>
										
	    								 </table>";

	    					if ($tabla8!='') {
	    						$tablafinal8="<table border=1 style='width:100%; margin-top:10px; font-size:70%; text-align:left; border-collapse: collapse;'>
	    								 <tr>
										  <th colspan=2>VARIACION</th>
										 </tr>
										 <tr>
										  <th>Datos</th>
										  <th>Detalle</th>
										 </tr>
										  $tabla8
	    								 </table>";
	    					} else {
	    						$tablafinal8='';
	    					}
	    					

	    					if ($od==1) {
	    						$observacion_intervencion = str_replace($busca, $var2, $obs);
	    					} else {
	    						$tipo='';
	    						$observacion_intervencion = 'Error no se compilo la observacion';
	    					}




	    					
	    					$tablafirma="<table border=1 style='width:100%; margin-top:10px; text-align:left; font-size:70%;border-collapse: collapse;'>
									  		<caption></caption>
											<tr>
											  <th colspan=2>Observaciones</th>
											</tr>
											<tr>
											  <td colspan='2'>$tipo - $observacion_intervencion</td>
											</tr>
											<tr>
											  <th>Firma Cliente</th>
											  <th>Firma Tecnico</th>
											</tr>
											<tr>
											  <td>$nombre $dni</td>
											  <td>$nomtecnico $apetecnico - $dnitecnico</td>
											</tr>
											<tr>
											  <td rowspan='1'><img src='$firma'  width='230' height='65'></td>
											  <td rowspan='1'><img src='$firmatecnico'  width='230' height='65'></td>
											</tr>
										</table>";

		    $code.="
		     	   <body>
			    	    <img src='./images/image001.jpg' width='110' height='35' style='position:absolute;top:-15;'/>
			    	    <div id='header' style='
			    	    text-align:center;
					    height:45px;
						top:17;
						left:0;'>
						   	<p>ACTA DE INTERVENCION DE SUMINISTRO ELECTRICO - $acta</p>
					    </div>


					    <div id='a' style='height:auto; width:100%;'>
					    	$tablafinal
						</div>

					    <div id='b' style='height:auto; width:100%;'>
					    	$tablafinal1
					    </div>

					    <div id='c' style='height:auto; width:100%;'>
					    	$tablafinal2
					    </div>




					    <div id='d' style='height:auto; width:28%;float:left;'>
					    	$tablafinal3
					    </div>

					    <div id='e' style='height:auto%; width:28%;float:left;padding-top:-16.8%;margin-left:30%'>
					    	$tablafinal4
					    </div>

					    <div id='f' style='height:auto%; width:40%;float:left;padding-top:-16.8%;margin-left:60%'>
					    	$tablafinal5
					    	$tablafinal8
					    </div>

					    <div id='g' style='height:auto%; width:58%;'>
					    	$tablafinal7
					    </div>

					    <div id='h' style='height:auto%; width:40%;float:left;padding-top:0%;margin-left:60%'>
					    	$tablafinal6
					    </div>





					    <div id='i' style='height:auto; padding-top:33.8%; width:100%;'>
					    	$tablafirma
					    </div>


						<div id='footer' style='width:100%;
						height:45px;
						position:absolute;
						bottom:0;
						left:0;
						'>
							<img src='./images/ban.jpg' width='100%' height='45'/>
						</div>
					</body>
				  ";


		   }
//echo $code;

/*  
		   $dompdf = new DOMPDF();
		   $dompdf->set_paper('letter','portrait');
		   //$dompdf->set_paper('legal','landscape');
		   $dompdf->load_html($code);
		   $dompdf->render();
		   //$dompdf->output($lista);
		   //$dompdf->stream("pdf".Date('Y-m-d').".pdf");
		   //$paper_size = array(0,0,360,460);
		   //$dompdf->set_paper($paper_size);
		   $dompdf->set_paper("A4", "portrait");

		   $dompdf->stream("$fechaactual.pdf", array("Attachment" => 0));
*/
		   App::import('Vendor', 'dompdf', true, array(), 'dompdf' . DS . 'dompdf_config.inc.php');
		   $dompdf = new DOMPDF();
		   $dompdf->load_html($code);
		   $dompdf->set_paper("A4", "portrait");
		   $dompdf->render();
		   //$dompdf->stream('ReportExample_'.date('d_M_Y').'.pdf');
		   $dompdf->stream("$fechaactual.pdf", array("Attachment" => 0));
?>