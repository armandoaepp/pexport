<?php

date_default_timezone_set('America/Lima');

//require_once "../conexion.php";
require_once(APP."View/Main/conexion.ctp");

$query_grafico = pg_query("select extract('year' from FechaAtencion) as anio, extract('month' from FechaAtencion) as mes,avg(tiempo) as tiempo_promedio from (
select FechaAsignado, FechaAtencion, ((DATE_PART('day', FechaAtencion - FechaAsignado) * 24 + 
                DATE_PART('hour', FechaAtencion - FechaAsignado)) * 60 +
                DATE_PART('minute', FechaAtencion - FechaAsignado)) * 60 +
                DATE_PART('second', FechaAtencion - FechaAsignado) as tiempo
from ComMovTab_Intervencion where FechaAtencion is not null) as T group by extract('year' from FechaAtencion), extract('month' from FechaAtencion)");


$rs=pg_num_rows($query_grafico);

$series = '';
$mons = array(1 => "Ene", 2 => "Feb", 3 => "Mar", 4 => "Abr", 5 => "May", 6 => "Jun", 7 => "Jul", 8 => "Ago", 9 => "Sep", 10 => "Oct", 11 => "Nov", 12 => "Dic");
if ($rs > 0) {
	
	while ($obj = pg_fetch_object($query_grafico)){
		$series .= "{name: '".$mons[$obj->mes]." ".$obj->anio."',data: [".($obj->tiempo_promedio/36400)."]},";
	}
	$series = substr($series,0,-1);
}else{
	echo 'No hay datos';
	exit();
}

?>
<!DOCTYPE html>
<html lang="es" >
<head>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta http-equiv="content-type" content="text/html; charset=UTF-8">
<title>Grafico Pexport</title>

<script type="text/javascript" src="//code.jquery.com/jquery-1.9.1.js"></script>



<script type="text/javascript">//<![CDATA[ 

$(function () {
    $('#container').highcharts({
        chart: {
            type: 'bar'
        },
        title: {
            text: 'Reporte tiempo de ejecución por meses'
        },
        subtitle: {
            text: 'Fuente: PEXPORT S.A.C'
        },
        xAxis: {
            categories: ['Tiempo Promedio'],
            title: {
                text: null
            }
        },
        yAxis: {
            min: 0,
            title: {
                text: 'días',
                align: 'high'
            },
            labels: {
                overflow: 'justify'
            }
        },
        tooltip: {
            valueSuffix: ' días'
        },
        plotOptions: {
            bar: {
                dataLabels: {
                    enabled: true
                }
            }
        },
        /*legend: {
            layout: 'vertical',
            align: 'right',
            verticalAlign: 'top',
            x: -40,
            y: 100,
            floating: true,
            borderWidth: 1,
            backgroundColor: ((Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'),
            shadow: true
        },*/
        credits: {
            enabled: false
        },
        series: [<?php echo $series;?>]
    });
});
//]]>  

</script>


</head>
<body>
	<!-- 
	<script src="http://code.highcharts.com/highcharts.js"></script>
	<script src="http://code.highcharts.com/modules/exporting.js"></script>
	 -->
	
	<script src="../js/jquery.high/highcharts.js"></script>
	<script src="../js/jquery.high/modules/exporting.js"></script>

	<div id="container" style="min-width: 310px; max-width: 800px; height: 300px; margin: 0 auto">
	</div>
	
</body>
</html>