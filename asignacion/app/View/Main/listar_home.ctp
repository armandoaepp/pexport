<?php
$lista="
		<div class='gallery-cont'>
			<div class='item'>
				<div class='photo'>
					<div class='head'>
						<span class='pull-right active'> <span
							class='label label-success pull-right' style='color: white;'><i class='fa fa-star-o'></i> Nuevo</span>
						</span>
						<h4 style='color: #444; font-weight: bold;'>Gestores</h4>
						<span class='desc' style='color: #444;'>Monitor Especializado</span>
					</div>
					<div class='img'>
						<img
							src='".FULL_URL."images/modulos/gestores.png' />
						<div class='over'>
							<div class='func'>
								<a href='".FULL_URL."main/consulta' ><i
									class='fa fa-link'></i> </a><a class='image-zoom'
									href='".FULL_URL."images/modulos/gestores.png'><i
									class='fa fa-search'></i> </a>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class='item'>
				<div class='photo'>
					<div class='head'>
						<h4 style='color: #444; font-weight: bold;'>Dashboard</h4>
						<span class='desc' style='color: #444;'>Detalle del sistema</span>
					</div>
					<div class='img'>
						<img
							src='".FULL_URL."images/modulos/dashboard2.png' />
						<div class='over'>
							<div class='func'>
								<a href='".FULL_URL."dashboard/dashboard' ><i
									class='fa fa-link'></i> </a><a class='image-zoom'
									href='".FULL_URL."images/modulos/dashboard2.png'><i
									class='fa fa-search'></i> </a>
							</div>
						</div>
					</div>
				</div>
			</div>
			
			
	
			<div class='item'>
				<div class='photo'>
					<div class='head'>
						<h4 style='color: #444; font-weight: bold;'>Asignar</h4>
						<span class='desc' style='color: #444;'>Relacion de Asignaciones segun Tipo</span>
					</div>
					<div class='img'>
						<img
							src='".FULL_URL."images/modulos/asignar.png' />
						<div class='over'>
							<div class='func'>
								<a href='".FULL_URL."main/asignar' ><i
									class='fa fa-link'></i> </a><a class='image-zoom'
									href='".FULL_URL."images/modulos/asignar.png'><i
									class='fa fa-search'></i> </a>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class='item'>
				<div class='photo'>
					<div class='head'>
						<h4 style='color: #444; font-weight: bold;'>Reportes</h4>
						<span class='desc' style='color: #444;'>Reportes generales</span>
					</div>
					<div class='img'>
						<img
							src='".FULL_URL."images/modulos/reporte.png' />
						<div class='over'>
							<div class='func'>
								<a href='".FULL_URL."main/reporte_intervenciones'><i
									class='fa fa-link'></i> </a><a class='image-zoom'
									href='".FULL_URL."images/modulos/reporte.png'><i
									class='fa fa-search'></i> </a>
							</div>
						</div>
					</div>
				</div>
			</div>
			
			<div class='item'>
				<div class='photo'>
					<div class='head'>
						<h4 style='color: #444; font-weight: bold;'>Monitoreo</h4>
						<span class='desc' style='color: #444;'>Lecturas</span>
					</div>
					<div class='img'>
						<img
							src='".FULL_URL."images/modulos/monitoreo.png' />
						<div class='over'>
							<div class='func'>
								<a href='".FULL_URL."main/monitorear' ><i
									class='fa fa-link'></i> </a><a class='image-zoom'
									href='".FULL_URL."images/modulos/monitoreo.png'><i
									class='fa fa-search'></i> </a>
							</div>
						</div>
					</div>
				</div>
			</div>
	
			<div class='item'>
				<div class='photo'>
					<div class='head'>
						<h4 style='color: #444; font-weight: bold;'>Monitor GPS</h4>
						<span class='desc' style='color: #444;'>Ubicacion en Tiempo Real</span>
					</div>
					<div class='img'>
						<img
							src='".FULL_URL."images/modulos/monitorgps.png' />
						<div class='over'>
							<div class='func'>
								<a href= '".FULL_URL."dashboard/mapa_monitoreo' ><i
									class='fa fa-link'></i> </a><a class='image-zoom'
									href='".FULL_URL."images/modulos/monitorgps.png'><i
									class='fa fa-search'></i> </a>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class='item'>
				<div class='photo'>
					<div class='head'>
						<span class='pull-right active'> 
							<span class='label label-success pull-right' style='color: white;'><i class='fa fa-star-o'></i> Nuevo</span>
						</span>
						<h4 style='color: #444; font-weight: bold;'>Sincronizar</h4>
						<span class='desc' style='color: #444;'>Importar y consultar archivos</span>
					</div>
					<div class='img'>
						<img
							src='".FULL_URL."images/modulos/sincronizar.png' />
						<div class='over'>
							<div class='func'>
								<a onclick='showImp()' ><i
									class='fa fa-link'></i> </a><a class='image-zoom'
									href='".FULL_URL."images/modulos/sincronizar.png'><i
									class='fa fa-search'></i> </a>
							</div>
						</div>
					</div>
				</div>
			</div>
			
		</div>
    ";
      echo $lista;
?>



<script type="text/javascript">
    $(document).ready(function(){
      
      //Initialize Mansory
      var $container = $('.gallery-cont');
      // initialize
      setTimeout(function(){
      $container.masonry({
        columnWidth: 0,
        itemSelector: '.item'
      });
      }, 300);
      
      //Resizes gallery items on sidebar collapse
      $("#sidebar-collapse").click(function(){
        setTimeout(function(){$container.masonry();}, 300);      
      });
    //MagnificPopup for images zoom
      $('.image-zoom').magnificPopup({ 
        type: 'image',
        mainClass: 'mfp-with-zoom', // this class is for CSS animation below
        zoom: {
        enabled: true, // By default it's false, so don't forget to enable it
        duration: 300, // duration of the effect, in milliseconds
        easing: 'ease-in-out', // CSS transition easing function 
        // The "opener" function should return the element from which popup will be zoomed in
        // and to which popup will be scaled down
        // By defailt it looks for an image tag:
        opener: function(openerElement) {
          // openerElement is the element on which popup was initialized, in this case its <a> tag
          // you don't need to add "opener" option if this code matches your needs, it's defailt one.
          var parent = $(openerElement).parents("div.img");
          return parent;
        }
        }
      });
    });
  </script>