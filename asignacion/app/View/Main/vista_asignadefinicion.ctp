<?php

//require "../conexion.php";
require_once(APP."View/Main/conexion.ctp");

$id=$_GET["sel"];

$query = pg_query("select 
GMDUR.IdUbicacion as IdUbicacion,
GMU.NomUbicacion as NomUbicacion,
GMCU.NomConceptosUbicacion as NomConceptosUbicacion,
GMDUR.IdUbicacionSup as IdUbicacionSup,
CU.IdUsuario as IdUsuario,
CU.NomUsuario as NomUsuario  
from GlobalMaster_DetalleUbicacionRegla GMDUR
left join GlobalMaster_Ubicacion GMU on GMDUR.IdUbicacion = GMU.IdUbicacion
left join GlobalMaster_ConceptosUbicacion GMCU on GMU.IdConceptosUbicacion = GMCU.IdConceptosUbicacion
left join ComMovTab_Usuario CU on GMDUR.IdUsuario = CU.IdUsuario
where GMCU.IdConceptosUbicacion=7 and GMDUR.IdUbicacionRegla=$id");



$lista="
      <div class='row'>
        <div class='col-md-12'>
          <div class='block-flat'>

            <div class='content'>
            <div class='columnaA'>
            
                <div class='dd' id='list1'>
                    <ol class='dd-list'>";
                    
                    while ($retorno = pg_fetch_object($query)){

                      $varid=utf8_decode($retorno->idubicacion);
                      $varnom=utf8_decode($retorno->nomubicacion);
                      $varnomu=utf8_decode($retorno->nomusuario);
                      
                      $lista.="
                      <li class='dd-item' data-id='$varid'>
                       <div class='dd-handle'>$varnom  - ( Supervisor a : $varnomu) </div>";
                      
                      $sql2=pg_query("select 
                      GMDUR.IdUbicacion as IdUbicacion,
                      GMU.NomUbicacion as NomUbicacion,
                      GMCU.NomConceptosUbicacion as NomConceptosUbicacion,
                      GMDUR.IdUbicacionSup as IdUbicacionSup,
                      CU.IdUsuario as IdUsuario,
                      CU.NomUsuario as NomUsuario,
                      GP.NomPersona as NomPesona,
                      GP.ApePersona as ApePersona
                      from GlobalMaster_DetalleUbicacionRegla GMDUR
                      left join GlobalMaster_Ubicacion GMU on GMDUR.IdUbicacion = GMU.IdUbicacion
                      left join GlobalMaster_ConceptosUbicacion GMCU on GMU.IdConceptosUbicacion = GMCU.IdConceptosUbicacion
                      left join ComMovTab_Usuario CU on GMDUR.IdUsuario = CU.IdUsuario
                      inner join GlobalMaster_Persona GP on CU.IdPersona = GP.IdPersona
                      where GMDUR.IdUbicacionSup=$varid");
                        
                      while ($retorno2 = pg_fetch_object($sql2)){
                        
                      $varid2=utf8_decode($retorno2->idubicacion);
                      $varnom2=utf8_decode($retorno2->nomubicacion);
                      $varnomu2=utf8_decode($retorno2->nomusuario);
                      $varnomp=utf8_decode($retorno2->nompesona);
                      $varapep=utf8_decode($retorno2->apepersona);  

                        $lista.="<ol class='dd-list'>
                          <li class='dd-item' data-id='$varid2'>
                            <div class='dd-handle'>$varnom2  - ( Asignado a : $varnomp $varapep $varnomu2) </div>";
                              
                            $sql3=pg_query("select 
                            GMDUR.IdUbicacion as IdUbicacion,
                            GMU.NomUbicacion as NomUbicacion,
                            GMCU.NomConceptosUbicacion as NomConceptosUbicacion,
                            GMDUR.IdUbicacionSup as IdUbicacionSup,
                            CU.IdUsuario as IdUsuario,
                            CU.NomUsuario as NomUsuario,
                            GP.NomPersona as NomPesona,
                            GP.ApePersona as ApePersona
                            from GlobalMaster_DetalleUbicacionRegla GMDUR
                            left join GlobalMaster_Ubicacion GMU on GMDUR.IdUbicacion = GMU.IdUbicacion
                            left join GlobalMaster_ConceptosUbicacion GMCU on GMU.IdConceptosUbicacion = GMCU.IdConceptosUbicacion
                            left join ComMovTab_Usuario CU on GMDUR.IdUsuario = CU.IdUsuario
                            inner join GlobalMaster_Persona GP on CU.IdPersona = GP.IdPersona
                            where GMDUR.IdUbicacionSup=$varid2");
                       
                           while ($retorno3 = pg_fetch_object($sql3)){
                        
                            $varid3=utf8_decode($retorno3->idubicacion);
                            $varnom3=utf8_decode($retorno3->nomubicacion);
                            $varnomu3=utf8_decode($retorno3->nomusuario);
                            $varnomp3=utf8_decode($retorno3->nompesona);
                            $varapep3=utf8_decode($retorno3->apepersona); 

                            $lista.="<ol class='dd-list'>
                             <li class='dd-item' data-id='$varid3'>
                              <div class='dd-handle'>$varnom3  - ( Asignado a : $varnomp3 $varapep3 $varnomu3) </div>
                            </li>
                            </ol>";


                            }

                          $lista.="</li>
                        </ol>";
                      }

                        

                      $lista.="</li>
                      

                    ";
                      }
                      
                    $lista."</ol>
                  </div>
                  
              
            </div>
            </div>
          </div>        
        </div>

      </div>

      ";

      echo $lista;

?>