<?php
date_default_timezone_set('America/Lima');

$sel=$_GET["sel"];

switch ($sel)
  {
    case 'Emergencia':
      $sel ='emergencia';

      $fichero = WWW_ROOT.'images/uploads/'.$sel.'.xlsx';
      

      if (file_exists($fichero)){

      //incluimos la clase
		require_once(APP.'Vendor/PHPExcel-1.7.7/Classes/PHPExcel/IOFactory.php');
                    
        //cargamos el archivo que deseamos leer
        $objPHPExcel = PHPExcel_IOFactory::load($fichero);

      $lista2="

        <div class='tabla'>
        <div class='farmsmall'>
        <form class='' action='' method='POST'> 

        <div class='row'>
        <div class='col-md-12'>
          <div class='block-flat'>

            <div class='content'>
              <div class='table-responsive'>
                <table class='table table-bordered' id='datatable2' >
                  <thead class='success-emphasis'>
                    <tr>
                      <th>Sector</th>
                      <th>Servicio</th>
                      <th>Nombre</th>
                      <th>Direccion</th>
                      <th>Cartera</th>
                      <th>Fecha</th>
                      <th>Alimentador</th>
                      <th>SED</th>
                      <th>Medidor</th>

                    </tr>
                  </thead>
                  <tbody>";
        
                    
                    //obtenemos los datos de la hoja activa (la primera)
                    $objHoja=$objPHPExcel->getActiveSheet()->toArray(null,true,true,true);
                    
                    //recorremos las filas obtenidas
                    $i=0;
                    foreach ($objHoja as $iIndice=>$objCelda) {
                      //imprimimos el contenido de la celda utilizando la letra de cada columna

                      $var1=utf8_decode($objCelda['F']);
                      $var2=utf8_encode($objCelda['G']);
                      $var3=utf8_encode($objCelda['H']);
                      $var4=utf8_encode($objCelda['I']);
                      $var5=utf8_encode($objCelda['J']);
                      $var6=utf8_decode($objCelda['O']);
                      $var7=utf8_decode($objCelda['AB']);
                      $var8=utf8_decode($objCelda['AC']);
                      $var9=utf8_decode($objCelda['AD']);

                      $i=$i+1;
                      if ($i > 4){
                        $lista2.="
                        <tr>
                          <td>$var1</td>
                          <td>$var2</td>
                          <td>$var3</td>
                          <td>$var4</td>
                          <td>$var5</td>
                          <td>$var6</td>
                          <td>$var7</td>
                          <td>$var8</td>
                          <td>$var9</td>
       
                        </tr>
                      ";
                    } else {
                      echo '';
                      
                    }
                    }
  
                    
                  $lista2.="</tbody>
                </table>
                <button class='btn btn-danger' type='button' onclick='insertarconvertirarchivo(1)'>Generar</button>
                <button class='btn btn-default'>Cancelar</button>              
              </div>
            </div>
          </div>        
        </div>
      </div>
              
              
      </form>
      </div>
      </div>";}

      else{
           $lista2= 'No existe el archivo en el repositorio';
                      
          }


	  echo $lista2;
      break;


    case 'Reclamo':
      $sel ='reclamo';

      $fichero = WWW_ROOT.'images/uploads/'.$sel.'.xlsx';
      

      if (file_exists($fichero)){

      //incluimos la clase
		require_once(APP.'Vendor/PHPExcel-1.7.7/Classes/PHPExcel/IOFactory.php');
                    
        //cargamos el archivo que deseamos leer
        $objPHPExcel = PHPExcel_IOFactory::load($fichero);


      $lista2="
      <div class='tabla2'>
      <div class='farmsmallasig'>
      <form class='farm' action='' method='POST'> 

        <div class='row'>
        <div class='col-md-12'>
          <div class='block-flat'>
            <div class='content'>
              <div class='table-responsive'>
                <table class='table table-bordered' id='datatable5' >
                  <thead class='success-emphasis'>
                    <tr>
                      <th >Solicitud</th>
                      <th style='width:7%;'>Orden de Trabajo</th>
                      <th >NroServicio</th>
                      <th >Fecha</th>
                      <th >Tipo de Servicio</th>
                      <th >Cliente</th>
                      <th >Direccion</th>
                      <th>Medidor</th>
                      <th>Marca</th>
                      <th>Modelo</th>
                      <th>Año</th>
                      <th>Potencia</th>
                      <th>Tarifa</th>
                      <th>TipoSum</th>
                      <th>Sistema</th>
                      <th>TipoCon</th>
                      <th>SubEstacion</th>
                      <th>SectorServicio</th>
                    </tr>
                  </thead>
                  <tbody>";
        
                    
                    //obtenemos los datos de la hoja activa (la primera)
                    $objHoja=$objPHPExcel->getActiveSheet()->toArray(null,true,true,true);
                    
                    //recorremos las filas obtenidas
                    $i=0;
                    foreach ($objHoja as $iIndice=>$objCelda) {
                      //imprimimos el contenido de la celda utilizando la letra de cada columna

                      $var1=utf8_decode($objCelda['A']);
                      $var2=utf8_encode($objCelda['B']);
                      $var3=utf8_encode($objCelda['C']);
                      $var4=utf8_encode($objCelda['G']);
                      $var5=utf8_decode($objCelda['J']);
                      $var6=utf8_encode($objCelda['K']);
                      $var7=utf8_decode($objCelda['L']);
                      $var8=utf8_decode($objCelda['M']);
                      $var9=utf8_decode($objCelda['N']);
                      $var10=utf8_decode($objCelda['O']);
                      $var11=utf8_decode($objCelda['Q']);
                      $var12=utf8_decode($objCelda['R']);
                      $var13=utf8_decode($objCelda['S']);
                      $var14=utf8_decode($objCelda['T']);
                      $var15=utf8_decode($objCelda['U']);
                      $var16=utf8_decode($objCelda['V']);
                      $var17=utf8_decode($objCelda['W']);
                      $var18=utf8_decode($objCelda['X']);
                      $i=$i+1;

                      if ($i > 1 && $var1!=''){
                        $lista2.="
                        <tr>
                          <td>$var1</td>
                          <td>$var2</td>
                          <td>$var3</td>
                          <td>$var4</td>
                          <td>$var5</td>
                          <td>$var6</td>
                          <td>$var7</td>
                          <td>$var8</td>
                          <td>$var9</td>
                          <td>$var10</td>
                          <td>$var11</td>
                          <td>$var12</td>
                          <td>$var13</td>
                          <td>$var14</td>
                          <td>$var15</td>
                          <td>$var16</td>
                          <td>$var17</td>
                          <td>$var18</td>
                        </tr>
                      ";
                    } else {
                      echo '';
                      
                    }
                    }
  
                    
                  $lista2.="</tbody>
                </table>
                <button class='btn btn-danger' type='button' onclick='insertarconvertirarchivo(2)'>Generar</button>
              <button class='btn btn-default'>Cancelar</button>              
              
              </div>
            </div>
          </div>        
        </div>
      </div>
              
              
      </form>
      </div>
      </div>";
      }else
      {
          $lista2= "No existe el archivo en el repositorio <a class='btn btn-info'";
                      
                    }


      echo $lista2;


      break;
	
	case 'Corte':
      $sel ='Cortes';

      $fichero = WWW_ROOT.'images/uploads/'.$sel.'.xlsx';

      if (file_exists($fichero)){
		require_once(APP.'Vendor/PHPExcel-1.7.7/Classes/PHPExcel/IOFactory.php');
		
		//cargamos el archivo que deseamos leer
		$objPHPExcel = PHPExcel_IOFactory::load($fichero);


      $lista2="
      <div class='tabla2'>
      <div class='farmsmallasig'>
      <form class='farm' action='' method='POST'> 

        <div class='row'>
        <div class='col-md-12'>
          <div class='block-flat'>
            <div class='content'>
              <div class='table-responsive'>
                <table class='table table-bordered' id='datatable5' >
                  <thead class='success-emphasis'>
                    <tr>
                      <th >Solicitud</th>
                      <th style='width:7%;'>Orden de Trabajo</th>
                      <th >NroServicio</th>
                      <th >Fecha</th>
                      <th >Tipo de Servicio</th>
                      <th >Cliente</th>
                      <th >Direccion</th>
                      <th>Medidor</th>
                      <th>Marca</th>
                      <th>Modelo</th>
                      <th>Año</th>
                      <th>Potencia</th>
                      <th>Tarifa</th>
                      <th>TipoSum</th>
                      <th>Sistema</th>
                      <th>TipoCon</th>
                      <th>SubEstacion</th>
                      <th>SectorServicio</th>
                    </tr>
                  </thead>
                  <tbody>";
        
                    
                    //obtenemos los datos de la hoja activa (la primera)
                    $objHoja=$objPHPExcel->getActiveSheet()->toArray(null,true,true,true);
                    
                    //recorremos las filas obtenidas
                    $i=0;
                    foreach ($objHoja as $iIndice=>$objCelda) {
                      //imprimimos el contenido de la celda utilizando la letra de cada columna

                      $var1=utf8_decode($objCelda['A']);
                      $var2=utf8_encode($objCelda['B']);
                      $var3=utf8_encode($objCelda['C']);
                      $var4=utf8_encode($objCelda['G']);
                      $var5=utf8_decode($objCelda['J']);
                      $var6=utf8_encode($objCelda['K']);
                      $var7=utf8_decode($objCelda['L']);
                      $var8=utf8_decode($objCelda['M']);
                      $var9=utf8_decode($objCelda['N']);
                      $var10=utf8_decode($objCelda['O']);
                      $var11=utf8_decode($objCelda['Q']);
                      $var12=utf8_decode($objCelda['R']);
                      $var13=utf8_decode($objCelda['S']);
                      $var14=utf8_decode($objCelda['T']);
                      $var15=utf8_decode($objCelda['U']);
                      $var16=utf8_decode($objCelda['V']);
                      $var17=utf8_decode($objCelda['W']);
                      $var18=utf8_decode($objCelda['X']);
                      $i=$i+1;

                      if ($i > 1 && $var1!=''){
                        $lista2.="
                        <tr>
                          <td>$var1</td>
                          <td>$var2</td>
                          <td>$var3</td>
                          <td>$var4</td>
                          <td>$var5</td>
                          <td>$var6</td>
                          <td>$var7</td>
                          <td>$var8</td>
                          <td>$var9</td>
                          <td>$var10</td>
                          <td>$var11</td>
                          <td>$var12</td>
                          <td>$var13</td>
                          <td>$var14</td>
                          <td>$var15</td>
                          <td>$var16</td>
                          <td>$var17</td>
                          <td>$var18</td>
                        </tr>
                      ";
                    } else {
                      echo '';
                      
                    }
                    }
  
                    
                  $lista2.="</tbody>
                </table>
                <button class='btn btn-danger' type='button' onclick='insertarconvertirarchivo(2)'>Generar</button>
              <button class='btn btn-default'>Cancelar</button>              
              
              </div>
            </div>
          </div>        
        </div>
      </div>
              
              
      </form>
      </div>
      </div>";
      }else
      {
          $lista2= "No existe el archivo en el repositorio <a class='btn btn-info'";
                      
                    }


      echo $lista2;


      break;
	
	case 'Reconexion':
      $sel ='rx';

      $fichero = WWW_ROOT.'images/uploads/'.$sel.'.xlsx';

      if (file_exists($fichero)){
		require_once(APP.'Vendor/PHPExcel-1.7.7/Classes/PHPExcel/IOFactory.php');
		
		//cargamos el archivo que deseamos leer
		$objPHPExcel = PHPExcel_IOFactory::load($fichero);
      $lista2="
      <div class='tabla2'>
      <div class='farmsmallasig'>
      <form class='farm' action='' method='POST'> 

        <div class='row'>
        <div class='col-md-12'>
          <div class='block-flat'>
            <div class='content'>
              <div class='table-responsive'>
                <table class='table table-bordered' id='datatable5' >
                  <thead class='success-emphasis'>
                    <tr>
                      <th >Unidad Negocio</th>
                      <th style='width:7%;'>Orden de Trabajo</th>
                      <th >CentroServicio</th>
                      <th >Sector</th>
					  <th >NroServicio</th>
                      <th >Cliente</th>
                      <th >Direccion</th>
					  <th >DireccionReferencial</th>
                      <th>Medidor</th>
                      <th>Deuda</th>
                      <th>Fecha Pago</th>
                      <th>Fecha Generacion</th>
                      <th>Estado</th>
                      <th>Corte</th>
                      <th>Ruta Lectura</th>
                      <th>Correlativo</th>
					  <th>Sub Actividad Ejecutada</th>
					  <th>TipoConexion</th>
					  <th>Fase</th>
                    </tr>
                  </thead>
                  <tbody>";
        
                    
                    //obtenemos los datos de la hoja activa (la primera)
                    $objHoja=$objPHPExcel->getActiveSheet()->toArray(null,true,true,true);
                    
                    //recorremos las filas obtenidas
                    $i=0;
                    foreach ($objHoja as $iIndice=>$objCelda) {
                      //imprimimos el contenido de la celda utilizando la letra de cada columna

                      $var1=htmlentities($objCelda['A']);
                      $var2=htmlentities($objCelda['D']);
                      $var3=htmlentities($objCelda['E']);
                      $var4=htmlentities($objCelda['F']);
                      $var5=htmlentities($objCelda['G']);
                      $var6=htmlentities($objCelda['H']);
                      $var7=htmlentities($objCelda['I']);
                      $var8=htmlentities($objCelda['J']);
                      $var9=htmlentities($objCelda['K']);
                      $var10=htmlentities($objCelda['L']);
                      $var11=htmlentities($objCelda['M']);
                      $var12=htmlentities($objCelda['N']);
                      $var14=htmlentities($objCelda['P']);
                      $var15=htmlentities($objCelda['Q']);
                      $var16=htmlentities($objCelda['R']);
                      $var17=htmlentities($objCelda['S']);                     
					  $var22=htmlentities($objCelda['X']);
					  $var24=htmlentities($objCelda['Z']);
					  $var25=htmlentities($objCelda['AA']);
                      $i=$i+1;

                      if ($i > 4 && $var1!=''){
                        $lista2.="
                        <tr>
                          <td>$var1</td>
                          <td>$var2</td>
                          <td>$var3</td>
                          <td>$var4</td>
                          <td>$var5</td>
                          <td>$var6</td>
                          <td>$var7</td>
                          <td>$var8</td>
                          <td>$var9</td>
                          <td>$var10</td>
                          <td>$var11</td>
                          <td>$var12</td>
                          <td>$var14</td>
                          <td>$var15</td>
                          <td>$var16</td>
                          <td>$var17</td>                         
						  <td>$var22</td>
						  <td>$var24</td>
						  <td>$var25</td>
                        </tr>
                      ";
                    } else {
                      echo '';
                      
                    }
                    }
  
                    
                  $lista2.="</tbody>
                </table>
                <button class='btn btn-danger' type='button' onclick='insertarconvertirarchivo(4)'>Generar</button>
              <button class='btn btn-default'>Cancelar</button>              
              
              </div>
            </div>
          </div>        
        </div>
      </div>
              
              
      </form>
      </div>
      </div>";
      }else
      {
          $lista2= "No existe el archivo en el repositorio <a class='btn btn-info'";
                      
                    }


      echo $lista2;


      break;
  }




?>

