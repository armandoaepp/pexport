<style>
.table-ordenes-deatlle th {
	padding: 2px 2px;
}
.table-ordenes-deatlle td {
	padding: 2px 2px;
}
</style>
<?php 
date_default_timezone_set('America/Lima');
//require "../conexion.php";
require_once(APP."View/Main/conexion.ctp");

if(isset($_GET['cbo_opcion'])){
	$opcion = $_GET['cbo_opcion'];
}else{
	$opcion = '';
}
if(isset($_GET['txt_value'])){
	$valor = $_GET['txt_value'];
}else{
	$valor = '';
}

$str_where = '';
if($opcion=='OT'){
	$str_where = "cmo.IdOrdenTrabajo = '".$valor."'";
}elseif($opcion=='Suministro'){
	$str_where = "cmo.Suministro = '".$valor."'";
}elseif($opcion=='DNI'){
	$str_where = "cmo.NroIdentidad = '".$valor."'";
}elseif($opcion=='TI'){
	$str_where = "cmo.SubActividad = '".$valor."'";
}

$query = pg_query("select cmi.IdIntervencion as IdIntervencion, cmi.FechaAtencion as FechaAtencion,
        cmo.NroAtencion as NroAtencion,cmo.Suministro,cmo.NombreSuministro as NombreCliente, ME.IdEstado, ME.NomEstado as NomEstado,
		IdOrdenTrabajo, cmo.SubActividad, fechageneracion, max(fechahora) as fechaestado
		from ComMovTab_Intervencion cmi
		inner join ComMovTab_Orden cmo on cmi.IdOrden = cmo.IdOrden
		inner join ComMovTab_MaestrosEstado ME on cmi.IdEstado = ME.IdEstado
		left join ComMovTab_MovimientosIntervencion MI on cmi.IdIntervencion=MI.IdIntervencion and cmi.IdEstado::TEXT = MI.valormovimiento
		where $str_where
		and cmo.SubActividad in (select DetalleTipoIntervencion from ComMovTab_TipoIntervencion)
		and cmo.UsuarioGenero not like 'Pexport%'
		and cmo.Condicion = '0'
		GROUP BY cmi.IdIntervencion, cmi.FechaAtencion,
        cmo.NroAtencion,cmo.Suministro,cmo.NombreSuministro, ME.IdEstado, ME.NomEstado,
		IdOrdenTrabajo, cmo.SubActividad, fechageneracion ORDER BY FechaAtencion DESC");

$rs=pg_num_rows($query);

if ($rs > 0) {
	$items = '';
	while ($retornocom = pg_fetch_object($query )){
		$varcliente=$retornocom->nombrecliente;
		$varintervencion=$retornocom->idintervencion;
		$fechaGenerado=$retornocom->fechageneracion;
		$idEstado = $retornocom->idestado;
		if($idEstado==9){
			$fechaEstado=$retornocom->fechaatencion;
			$nomEstado = $retornocom->nomestado.' Tableta';
		}else if($idEstado==13){
			$fechaEstado=$retornocom->fechaatencion;
			$nomEstado = $retornocom->nomestado.' Tableta';
		}else{
			$fechaEstado=$retornocom->fechaestado;
			$nomEstado = $retornocom->nomestado;
		}
		$ordenTrab = $retornocom->idordentrabajo;
		$subActividad = $retornocom->subactividad;
		$suministro = $retornocom->suministro;
		
		$items .= '<li>
				<a href='.FULL_URL.'"main/generaacta_monitorear_pg?id='.$varintervencion.'" target="_blank" title="ver acta">
						<i class="fa fa-file-text"></i></a>
				<a href="#" onClick="javascript: buscarDetalleCronologia('.$varintervencion.','.$retornocom->suministro.')">
				<strong>'.$varcliente.'</strong> <span class="pull-right value"><i class="fa fa-angle-right"></i></span>
				<table class="table-ordenes-deatlle">
				<tr><td>Suministro</td><td><b>'.$suministro.'</b></td></tr>
				<tr><td>OT</td><td><b>'.$ordenTrab.'</b></td></tr>
				<tr><td>Fecha Generado</td><td><b>'.$fechaGenerado.'</b></td></tr>
				<tr><td>Estado</td><td><b>'.$nomEstado.'</b></td></tr>
				<tr><td>Fecha '.$nomEstado.'</td><td><b>'.date('Y-m-d H:i:s',strtotime($fechaEstado)).'</b></td></tr>
				<tr><td>SubActividad</td><td><b>'.$subActividad.'</b></td></tr>
				</table>
				</a>
			</li>';
		}
} else {

	$items_false ="
	<div class='alert alert-danger alert-white rounded'>
                        <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>
                        <div class='icon'><i class='fa fa-check'></i></div>
                        <strong>Registro No Encontrado!</strong>
                       </div>";
}
?>
<?php 
if(isset($items)){?>
<div class="block">
	<div class="header">
		<h2>Ordenes <span class="pull-right">#<?php echo $rs;?></span></h2>
		<h3>Seleccione uno de estos registros</h3>
	</div>
	<div class="content no-padding ">
		<ul class="items">
			<?php echo $items;?>
		</ul>
	</div>
</div>
<?php 
}else{
	echo $items_false;
}
?>
<script>
function buscarDetalleCronologia(id_intervencion,suministro){
	$('.loadinx2').show();
	$('#cronologia').load(FULL_URL+'main/orden_cronologia?id_intervencion='+id_intervencion,function(){
		$('.loadinx2').hide();

		var preview = $("#iframe_acta");
	    preview.attr("src", FULL_URL+'main/generaacta_monitorear?id='+id_intervencion);

	    $("#tab_gps").off('click');
	    $("#tab_gps").on('click', function (){
	    	var preview = $("#iframe_gps");
	    	preview.attr("src", FULL_URL+'main/mapa_suministro?suministro='+suministro);
	    });

	    $('.image-zoom').magnificPopup({ 
	        type: 'image',
	        mainClass: 'mfp-with-zoom', // this class is for CSS animation below
	        zoom: {
	          enabled: true, // By default it's false, so don't forget to enable it
	          duration: 300, // duration of the effect, in milliseconds
	          easing: 'ease-in-out', // CSS transition easing function 
	          opener: function(openerElement) {
	            var parent = $(openerElement);
	            return parent;
	          }
	        }
	      });
	});
}
</script>
