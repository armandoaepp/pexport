<?php

date_default_timezone_set('America/Lima');
//require "../conexion.php";
require_once(APP."View/Main/conexion.ctp");

$query = pg_query("select 
GMUR.IdUbicacionRegla as IdUbicacionRegla,
GMUR.NomUbicacionRegla as NomUbicacionRegla,
GMUR.ReglaJerarquia as ReglaJerarquia,
GMRJU.IdReglaJerarquiaUbicacion as IdReglaJerarquiaUbicacion,
GMRJU.NombreReglaJerarquiaUbicacion as NombreReglaJerarquiaUbicacion,
GMUR.FechaVersion as FechaVersion,
GMUR.IdUbicacionReglaSup as IdUbicacionReglaSup,
GURS.NomUbicacionRegla as NomUbicacionReglaSup,
GMUR.IdEstado as IdEstado,
ME.NomEstado as NomEstado
from GlobalMaster_UbicacionRegla GMUR 
inner join GlobalMaster_ReglaJerarquiaUbicacion GMRJU
on GMUR.ReglaJerarquia = GMRJU.IdReglaJerarquiaUbicacion
left join GlobalMaster_UbicacionRegla GURS 
on GMUR.IdUbicacionReglaSup = GURS.IdUbicacionRegla
join ComMovTab_MaestrosEstado ME
on GMUR.IdEstado = ME.IdEstado");

$querycomb1 = pg_query("select * from GlobalMaster_ReglaJerarquiaUbicacion");
$querycomb2 = pg_query("select * from GlobalMaster_UbicacionRegla");

$lista="
	
	

    <div class='row'>
      <div class='col-sm-12 col-md-12'>
        <div class='block-flat'>


        <div class='panel-group accordion accordion-semi' id='accordion4'>
					  <div class='panel panel-default'>
						<div class='panel-heading success'>
						  <h4 class='panel-title'>
							<a data-toggle='collapse' data-parent='#accordion4' href='#ac4-1'>
								<i class='fa fa-angle-right'></i> Agregar Nueva Ubicacion Regla
							</a>
						  </h4>
						</div>
						<div id='ac4-1' class='panel-collapse collapse in'>
						  <div class='panel-body'>
							
						          <div class='content'>

							          <form role='form' id='formmaestrosubicacionregla' method='post' style='width:60%;'> 
							            <div class='form-group'>
							              <input type='hidden' id='idubicacionregla' name='idusuario'>
							              <label>Nombre de Regla Jerarquia</label> <input type='text' id='nomubicacionregla' name='nomubicacionregla' placeholder='Nombre de Regla' class='form-control'>
							            </div>
							            <div class='form-group'>";

											 $lista.= " <label>Selecciona la Regla Jerarquia : </label> <select class='form-control' id='idreglaubicacionregla' name='idreglaubicacionregla'>";
														 while ($retorno = pg_fetch_object($querycomb1)){
															$lista.= "<option value='$retorno->idreglajerarquiaubicacion'>$retorno->nombrereglajerarquiaubicacion</option>";
														}
											 $lista.= "</select>
											 </div>
											 <div class='form-group'>";

											 $lista.= " <label>Selecciona Pertenece Regla Ubicacion : </label> <select class='form-control' id='idubicacionreglasup' name='idubicacionreglasup'>
														 <option value='0'>Ninguna</option>";
														 while ($retorno = pg_fetch_object($querycomb2)){
															$lista.= "<option value='$retorno->idubicacionregla'>$retorno->nomubicacionregla</option>";
														}
											 $lista.= "</select>
											 </div>



							              <button class='btn btn-danger' type='button' onclick='crudmaestrosubicacionregla(1)'>Grabar</button>
							              <button class='btn btn-default' type='button'>Cancelar</button>
							          </form>
						          
						          </div>

						  </div>
						</div>
					  </div>
		</div>

						      <div class='row'>
						        <div class='col-md-12'>
						          <div class='block-flat'>

						            

						            <div class='content'>
						            <div class='tabla'>

						          
						            <div class=''>

						              <div class='table-responsive'>
						              <form role='form' id='formmaestrosubicacionreglacrud' method='post'> 
						                <table class='no-border' id='datatablecrudmaestrosubicacionregla'>

						                  <thead class='primary-emphasis'>
						                    <tr>
						                      <th></th>
						                      <th>Codigo</th>
						                      <th>Nombre Regla Ubicacion</th>
						                      <th>Pertenece a Jerarquia</th>
						                      <th>Fecha Creacion</th>
						                      <th>Regla Ubicacion Superior</th>
						                      <th>Estado</th>
						                    </tr>

						                  </thead>
						                  <tbody class='no-border'>";


						                    while ($retorno = pg_fetch_object($query)){

						                       $var=utf8_decode($retorno->idubicacionregla);
						                       $var1=utf8_decode($retorno->nomubicacionregla);

						                       $var2=utf8_decode($retorno->nombrereglajerarquiaubicacion);
						                       $var21=utf8_decode($retorno->idreglajerarquiaubicacion);
						                       $var3=utf8_decode($retorno->fechaversion);

						                       $var4=utf8_decode($retorno->nomubicacionreglasup);
						                       $var41=utf8_decode($retorno->idubicacionreglasup);

						                       $var5=utf8_decode($retorno->idestado);
						                       $var51=utf8_decode($retorno->nomestado);

						                       $lista.= "<tr class='odd gradeX'>
						                                  <input type ='hidden' id='idubicacionregla[]' name='idubicacionregla[]' value='$var'>
						                                  <td style='width:1%;'><input type ='checkbox' id='idubicacionreglachk[]' name='idubicacionreglachk[]' value='$var'></td>
						                                  <td style='width:1%;'>$var</td>
						                                  <td style='width:20%;'><input type='text' id='nomubicacionreglacrud[]' name='nomubicacionreglacrud[]'  value='$var1' class='form-control'></td>
						                                  
						                                  <td style='width:20%;'><select class='form-control' id='idreglajerarquiaubiubireglacrud[]' name='idreglajerarquiaubiubireglacrud[]' value='$var2' >
						                                      <option value='$var21'>$var2</option>
						                                      ";
						                                  $querydetasi1 = pg_query("select * from GlobalMaster_ReglaJerarquiaUbicacion where  IdReglaJerarquiaUbicacion !=$var21 ");
						                                   while ($retornodet = pg_fetch_object($querydetasi1)){
						                                    $idregu=$retornodet->idreglajerarquiaubicacion;
						                                    $nomregu=$retornodet->nombrereglajerarquiaubicacion;

						                                    $lista.="<option value='$idregu'>$nomregu</option>"; 
						                                   } 
						                                  $lista.="</select></td>

						                                  <td style='width:20%;'><input type='text' id='fecubicacionregla[]' name='fecubicacionregla[]'  value='$var3' class='form-control' disabled></td>
						                                  <td style='width:20%;'><select class='form-control' id='idubicacionreglaubireglasupcrud[]' name='idubicacionreglaubireglasupcrud[]' value='$var4' >
						                                      <option value='$var41'>$var4</option>
						                                      ";
						                                  $querydetasi2 = pg_query("select * from GlobalMaster_UbicacionRegla where  IdUbicacionRegla !='$var41'");
						                                   while ($retornodet2 = pg_fetch_object($querydetasi2)){
						                                    $idur=$retornodet2->idubicacionregla;
						                                    $nomur=$retornodet2->nomubicacionregla;

						                                    $lista.="<option value='$idur'>$nomur</option>"; 
						                                   } 
						                                  $lista.="</select></td>
						                                  <td style='width:10%;'><select class='form-control' id='estadoubicacionreglacrud[]' name='estadoubicacionreglacrud[]' value='$var51' >    
						                                      <option value='$var5'>$var51</option>
						                                      ";
						                                  $querydetest = pg_query("select ME.IdEstado as IdEstado,NomEstado as NomEstado from ComMovTab_MaestrosEstado ME  
						                                  where ME.EstadoSup = 2 and ME.IdEstado!=$var5");
						                                   while ($retornodetest = pg_fetch_object($querydetest)){
						                                    $idest=$retornodetest->idestado;
						                                    $nomest=$retornodetest->nomestado;

						                                    $lista.="<option value='$idest'>$nomest</option>"; 
						                                   } 
						                                  $lista.="</select></td>

						                                </tr>";
						                       }
						                  $lista.="</tbody>

						                </table>
						                <button class='btn btn-success' type='button' onclick='crudmaestrosubicacionregla(2)'>Grabar</button>
						                <button class='btn btn-danger' type='button' onclick='crudmaestrosubicacionregla(3)'>Eliminar</button>
						              </form>              
						              </div>

						              </div>
						              </div>
						              
						            </div>
						          </div>        
						        </div>

						      </div>

        </div>				
      </div>
      

	</div>";

echo $lista;

?>