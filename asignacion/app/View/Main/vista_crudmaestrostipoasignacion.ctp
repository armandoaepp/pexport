<?php

date_default_timezone_set('America/Lima');
//require "../conexion.php";
require_once(APP."View/Main/conexion.ctp");

$query = pg_query("select 
CT.IdTipoIntervencion as IdTipoIntervencion,
CT.DetalleTipoIntervencion as DetalleTipoIntervencion,
CT.IdTipoIntervencionSup as IdTipoIntervencionSup,
CTS.DetalleTipoIntervencion as DetalleTipoIntervencionSup
from ComMovTab_TipoIntervencion CT
left join ComMovTab_TipoIntervencion CTS on CT.IdTipoIntervencionSup = CTS.IdTipoIntervencion
");

$querycomb1 = pg_query("select * from ComMovTab_TipoIntervencion");

$lista="
	
	

    <div class='row'>
      <div class='col-sm-12 col-md-12'>
        <div class='block-flat'>


        <div class='panel-group accordion accordion-semi' id='accordion4'>
					  <div class='panel panel-default'>
						<div class='panel-heading success'>
						  <h4 class='panel-title'>
							<a data-toggle='collapse' data-parent='#accordion4' href='#ac4-1'>
								<i class='fa fa-angle-right'></i> Agregar Nuevo Tipo de Asignacion
							</a>
						  </h4>
						</div>
						<div id='ac4-1' class='panel-collapse collapse in'>
						  <div class='panel-body'>
							
						          <div class='content'>

							          <form role='form' id='formmaestrostipointervencion' method='post' style='width:50%;'> 
							            <div class='form-group'>
							              <input type='hidden' id='idtipointervencion' name='idusuario'>
							              <label>Nombre de Tipo Asignacion</label> <input type='text' id='nomtipointervencion' name='nomtipointervencion' placeholder='Nombre de Tipo de Asignacion' class='form-control'>
							            </div>
							            <div class='form-group'>";

											 $lista.= " <label>Selecciona el Tipo de Intervencion Superior : </label> 
											 <select class='form-control' id='idtipointervencionsup' name='idtipointervencionsup'>
											 <option value='0'>Ninguno</option>
											 ";
														 while ($retorno = pg_fetch_object($querycomb1)){
														 	$varcomb=$retorno->detalletipointervencion;
															$lista.= "<option value='$retorno->idtipointervencion'>$varcomb</option>";
														}
											 $lista.= "</select>
											 </div>
				



							              <button class='btn btn-danger' type='button' onclick='crudmaestrostipointervencion(1)'>Grabar</button>
							              <button class='btn btn-default' type='button'>Cancelar</button>
							          </form>
						          
						          </div>

						  </div>
						</div>
					  </div>
		</div>

						      <div class='row'>
						        <div class='col-md-12'>
						          <div class='block-flat'>

						            

						            <div class='content'>
						            <div class='tabla'>

						          
						            <div class=''>

						              <div class='table-responsive'>
						              <form role='form' id='formmaestrostipointervencioncrud' method='post'> 
						                <table class='no-border' id='datatablecrudmaestrostipointervencion'>

						                  <thead class='primary-emphasis'>
						                    <tr>
						                      <th></th>
						                      <th>Codigo</th>
						                      <th>Nombre de Asignacion</th>
						                      <th>Pertenece a</th>
						                    </tr>

						                  </thead>
						                  <tbody class='no-border'>";


						                    while ($retorno = pg_fetch_object($query)){

						                       $var=utf8_decode($retorno->idtipointervencion);
						                       $var1=$retorno->detalletipointervencion;
						                       $var2=utf8_decode($retorno->idtipointervencionsup);
						                       $var3=utf8_decode($retorno->detalletipointervencionsup);



						                       $lista.= "<tr class='odd gradeX'>
						                                  <input type ='hidden' id='idtipointervencion[]' name='idtipointervencion[]' value='$var'>
						                                  <td style='width:5%;'><input type ='checkbox' id='idtipointervencionchk[]' name='idtipointervencionchk[]' value='$var'></td>
						                                 <td style='width:5%;'><input type ='text' id='idtipointervencioncrud[]' name='idtipointervencioncrud[]' value='$var'></td>


						                                  <td style='width:30%;'>
						                                  <input style='width:100%;' type ='text' id='detalletipointervencioncrud[]' name='detalletipointervencioncrud[]' value='$var1'>

						                                  </td>
						                                 

						                                  <td style='width:30%;'><select class='form-control' id='idtipointervencionsupcrud[]' name='idtipointervencionsupcrud[]' value='$var2' >
						                                      <option value='$var2'>$var3</option>
						                                      ";
						                                  $querydetasi1 = pg_query("select * from ComMovTab_TipoIntervencion where IdTipoIntervencion !=$var2 ");
						                                   while ($retornodet = pg_fetch_object($querydetasi1)){
						                                    $idt=$retornodet->idtipointervencion;
						                                    $nomt=$retornodet->detalletipointervencion;

						                                    $lista.="<option value='$idt'>$nomt</option>"; 
						                                   } 
						                                  $lista.="</select></td>

						                                 
						                                </tr>";
						                       }
						                  $lista.="</tbody>

						                </table>
						                <button class='btn btn-success' type='button' onclick='crudmaestrostipointervencion(2)'>Grabar</button>
						                <button class='btn btn-danger' type='button' onclick='crudmaestrostipointervencion(3)'>Eliminar</button>
						              </form>              
						              </div>

						              </div>
						              </div>
						              
						            </div>
						          </div>        
						        </div>

						      </div>

        </div>				
      </div>
     
	</div>";

echo $lista;

?>