<?php

date_default_timezone_set('America/Lima');
//require "../conexion.php";
require_once(APP."View/Main/conexion.ctp");


$querycomb1 = pg_query("select * from GlobalMaster_UbicacionRegla");

$querycomb2 = pg_query("select 
GMU.IdUbicacion as IdUbicacion,
GMU.IdUbicacionConc as IdUbicacionConc,
GMU.NomUbicacion as NomUbicacion,
GMU.AbrUbicacion as AbrUbicacion,
GMU.IdConceptosUbicacion as IdConceptosUbicacion,
GMCU.NomConceptosUbicacion as NomConceptosUbicacion
from GlobalMaster_Ubicacion GMU inner join
GlobalMaster_ConceptosUbicacion GMCU on GMU.IdConceptosUbicacion = GMCU.IdConceptosUbicacion");

$querycomb3 = pg_query("select * from ComMovTab_Usuario");



$table1 = pg_query("select 
GMU.IdUbicacion as IdUbicacion,
GMU.IdUbicacionConc as IdUbicacionConc,
GMU.NomUbicacion as NomUbicacion,
GMU.AbrUbicacion as AbrUbicacion,
GMU.IdConceptosUbicacion as IdConceptosUbicacion,
GMCU.NomConceptosUbicacion as NomConceptosUbicacion
from GlobalMaster_Ubicacion GMU inner join
GlobalMaster_ConceptosUbicacion GMCU on GMU.IdConceptosUbicacion = GMCU.IdConceptosUbicacion");


$query = pg_query("select
GDUR.IdUbicacionReglaDetalle as IdUbicacionReglaDetalle, 
GDUR.IdUbicacionRegla as IdUbicacionRegla,
GUR.NomUbicacionRegla as NomUbicacionRegla,
GDUR.IdUbicacion as IdUbicacion,
GU.NomUbicacion as NomUbicacion,
GCU.NomConceptosUbicacion as NomConceptosUbicacion,
GDUR.IdUbicacionSup as IdUbicacionSup,
GUS.NomUbicacion as NomUbicacionSup,
GCUS.NomConceptosUbicacion as NomConceptosUbicacionSup,
GDUR.IdUsuario as IdUsuario,
CU.NomUsuario as NomUsuario,
GMP.IdPersona as IdPersona,
GMP.NomPersona as NomPersona,
GMP.ApePersona as ApePersona
from 
GlobalMaster_DetalleUbicacionRegla GDUR left join 
GlobalMaster_UbicacionRegla GUR on GDUR.IdUbicacionRegla = GUR.IdUbicacionRegla left join
GlobalMaster_Ubicacion GU on GDUR.IdUbicacion = GU.IdUbicacion left join
GlobalMaster_ConceptosUbicacion GCU on GU.IdConceptosUbicacion = GCU.IdConceptosUbicacion left join
GlobalMaster_Ubicacion GUS on GDUR.IdUbicacionSup = GUS.IdUbicacion left join
GlobalMaster_ConceptosUbicacion GCUS on GUS.IdConceptosUbicacion = GCUS.IdConceptosUbicacion left join
ComMovTab_Usuario CU on GDUR.IdUsuario = CU.IdUsuario
join GlobalMaster_Persona GMP on CU.IdPersona = GMP.IdPersona
");



$lista="
	
	

    <div class='row'>
      <div class='col-sm-12 col-md-12'>
        <div class='block-flat'>


        <div class='panel-group accordion accordion-semi' id='accordion4'>
					  <div class='panel panel-default'>
						<div class='panel-heading success'>
						  <h4 class='panel-title'>
							<a data-toggle='collapse' data-parent='#accordion4' href='#ac4-1'>
								<i class='fa fa-angle-right'></i> Agregar Nueva Composicion Regla Ubicacion
							</a>
						  </h4>
						</div>
						<div id='ac4-1' class='panel-collapse collapse in'>
						  <div class='panel-body'>
							
						          <div class='content'>

							          <form role='form' id='formdetalleubicacionregla' method='post' style='width:100%;'>

							          	<div class='form-group' style='width:60%;'>
							              " ;

											 $lista.= " <label>Selecciona la Regla : </label> <select class='form-control' id='idregladetalleubicacionregla' name='idregladetalleubicacionregla'>";
														 while ($retorno = pg_fetch_object($querycomb1)){
															$lista.= "<option value='$retorno->idubicacionregla'>$retorno->nomubicacionregla</option>";
														}
											 $lista.= "</select>
										</div>

							            <div class='form-group'>
							            	<table class='no-border' id='datatableformnormalasigna'>

							                  <thead class='primary-emphasis'>
							                    <tr>
							                      <th></th>
							                      <th>Codigo</th>
							                      <th>Tipo</th>
							                      <th>Ubicacion</th>
							                    </tr>

							                  </thead>
							                  <tbody class='no-border'>";


							                    while ($retorno = pg_fetch_object($table1)){

							                       $var=utf8_decode($retorno->idubicacion);
							                       $var2=utf8_decode($retorno->nomconceptosubicacion);
							                       $var1=utf8_decode($retorno->nomubicacion);
							                       


							                       $lista.= "<tr class='odd gradeX'>
							                                  <input type ='hidden' id='idubi[]' name='idubi[]' value='$var'>
							                                  <td style='width:1%;'><input type ='checkbox' id='idubicaciondetalleubicacionreglachk[]' name='idubicaciondetalleubicacionreglachk[]' value='$var' ></td>
							                                  <td style='width:1%;'>$var</td>
							                                  <td style=''><input type='text' id='nameusuario[]' name='nameusuario[]'  value='.$var2' class='form-control' disabled></td>
							                                  <td style=''><input type='text' id='nameusuario[]' name='nameusuario[]'  value='$var1' class='form-control' style='width:100%' disabled></td>
							                                  
							                                </tr>";
							                       }
							                  $lista.="</tbody>

							                </table>
							            </div>


							            <div class='form-group' style='width:60%;'>
							              " ;

											 $lista.= " <label>A quien Pertenece : </label> <select class='form-control' id='idubicacionsupdetalleubicacionregla' name='idubicacionsupdetalleubicacionregla'>";
														 while ($retorno = pg_fetch_object($querycomb2)){
															$lista.= "<option value='$retorno->idubicacion'>$retorno->nomubicacion - $retorno->nomconceptosubicacion</option>";
														}
											 $lista.= "</select>
											 </div>


										<div class='form-group'>
							              " ;

											 $lista.= " <label>Usuario Asignado : </label> <select class='form-control' id='idusuariodetalleubicacionregla' name='idusuariodetalleubicacionregla'>";
														 while ($retorno = pg_fetch_object($querycomb3)){
															$lista.= "<option value='$retorno->idusuario'>$retorno->nomusuario</option>";
														}
											 $lista.= "</select>
											 </div>




							              <button class='btn btn-danger' type='button' onclick='crudmaestrosdetalleubicacionregla(1)'>Grabar</button>
							              <button class='btn btn-default' type='button'>Cancelar</button>
							          </form>
						          
						          </div>

						  </div>
						</div>
					  </div>
		</div>


		
						      <div class='row'>
						        <div class='col-md-12'>
						          <div class='block-flat'>

						            

						            <div class='content'>
						            <div class='tabla'>

						          
						            <div class=''>

						              <div class='table-responsive'>
						              <form role='form' id='formdetalleubicacionreglacrud' method='post' > 
						                <table class='no-border' id='datatableformnormalasignacrud'>

						                  <thead class='primary-emphasis'>
						                    <tr>
						                      <th></th>
						                      <th>Codigo</th>
						                      <th>Nombre Regla Ubicacion</th>
						                      <th>Ubicacion</th>
						                      <th>Pertenece A</th>
						                      <th>Asignado A</th>
						                    </tr>

						                  </thead>
						                  <tbody class='no-border'>";


						                    while ($retorno = pg_fetch_object($query)){

						                       $var=utf8_decode($retorno->idubicacionregladetalle);
						                       $var11=utf8_decode($retorno->idubicacionregla);
						                       $var1=utf8_decode($retorno->nomubicacionregla);
						                       $var22=utf8_decode($retorno->idubicacion);
						                       $var2=utf8_decode($retorno->nomubicacion);
						                       $var21=utf8_decode($retorno->nomconceptosubicacion);
						                       $var32=utf8_decode($retorno->idubicacionsup);
						                       $var3=utf8_decode($retorno->nomubicacionsup);
						                       $var31=utf8_decode($retorno->nomconceptosubicacionsup);
						                       $var4=utf8_decode($retorno->nomusuario);
						                       $var41=utf8_decode($retorno->idusuario);
						                       $var5=utf8_decode($retorno->nompersona);
						                       $var51=utf8_decode($retorno->apepersona);

						                       $lista.= "<tr class='odd gradeX'>
						                                  <input type ='hidden' id='iddetalleubicacionreglacrud[]' name='iddetalleubicacionreglacrud[]' value='$var'>
						                                  <td style='width:1%;'><input type ='checkbox' id='iddetalleubicacionreglacrudchk[]' name='iddetalleubicacionreglacrudchk[]' value='$var' ></td>
						                                  <td style='width:1%;'>$var</td>
						                                  <td style='width:8%;'>
						                                  <input style='width:100%;' type='hidden' id='reglaubicacionreglacrudid[]' name='reglaubicacionreglacrudid[]'  value='$var11' class='form-control'>
						                                  <input style='width:100%;' type='text' id='reglaubicacionreglacrud[]' name='reglaubicacionreglacrud[]'  value='$var1' class='form-control'>
						                                  </td>
						                                  <td style='width:12%;'>
						                                  <input style='width:100%;' type='hidden' id='ubicacionubicacionreglacrudid[]' name='ubicacionubicacionreglacrudid[]'  value='$var22' class='form-control'>
						                                  <input style='width:100%;' type='text' id='ubicacionubicacionreglacrud[]' name='ubicacionubicacionreglacrud[]'  value='$var2 - $var21' class='form-control' style='width:100%'></td>
						                                  
						                                  <td style='width:15%;'><select style='width:100%;' class='form-control' id='perteneceubicacionreglacrud[]' name='perteneceubicacionreglacrud[]' value='$var32' >    
						                                      <option value='$var32'>$var3 - $var31</option>
						                                      ";
							                                  $queryubic = pg_query("select 
																GMU.IdUbicacion as IdUbicacion,
																GMU.IdUbicacionConc as IdUbicacionConc,
																GMU.NomUbicacion as NomUbicacion,
																GMU.AbrUbicacion as AbrUbicacion,
																GMU.IdConceptosUbicacion as IdConceptosUbicacion,
																GMCU.NomConceptosUbicacion as NomConceptosUbicacion
																from GlobalMaster_Ubicacion GMU inner join
																GlobalMaster_ConceptosUbicacion GMCU on GMU.IdConceptosUbicacion = GMCU.IdConceptosUbicacion 
																where GMU.IdUbicacion!=$var32");
							                                   while ($retornoubi = pg_fetch_object($queryubic)){
							                   
						                                 
						                                    $lista.="<option value='$retornoubi->idubicacion'>$retornoubi->nomubicacion - $retornoubi->nomconceptosubicacion</option>"; 
						                                   } 
						                                  $lista.="</select></td>


						                                  <td style='width:15%;'><select style='width:100%;' class='form-control' id='usuarioasignacionreglacrud[]' name='usuarioasignacionreglacrud[]' value='$var41' >    
						                                      <option value='$var41'>$var4 - $var5 $var51</option>
						                                      ";
							                                  $queryuser = pg_query("select CU.IdUsuario as IdUsuario,CU.NomUsuario as NomUsuario, GMP.NomPersona as NomPersona,GMP.ApePersona as ApePersona from ComMovTab_Usuario CU  
							                                  join GlobalMaster_Persona GMP on CU.IdPersona=GMP.IdPersona where CU.IdUsuario!=$var41");
							                                   while ($retornouser = pg_fetch_object($queryuser)){
							                                    $idusu=$retornouser->idusuario;
							                                    $nomusu=$retornouser->nomusuario;
							                                    $nomper=$retornouser->nompersona;
							                                    $apeper=$retornouser->apepersona;
						                                    

						                                    $lista.="<option value='$idusu'>$nomusu - $nomper $apeper</option>"; 
						                                   } 
						                                  $lista.="</select></td>

						                                </tr>";
						                       }
						                  $lista.="</tbody>

						                </table>
						                <button class='btn btn-success' type='button' onclick='crudmaestrosdetalleubicacionregla(2)'>Grabar</button>
						                <button class='btn btn-danger' type='button' onclick='crudmaestrosdetalleubicacionregla(3)'>Eliminar</button>
						              </form>              
						              </div>

						              </div>
						              </div>
						              
						            </div>
						          </div>        
						        </div>

						      </div>

      

        </div>				
      </div>
      

	</div>";

echo $lista;

?>