<?php

date_default_timezone_set('America/Lima');
//require "../conexion.php";
require_once(APP."View/Main/conexion.ctp");

$query = pg_query("select 
* from GlobalMaster_Persona gp join
ComMovTab_MaestrosEstado cme on gp.IdEstado = cme.IdEstado join
GlobalMaster_PersonaTipoPersona gptp on gp.IdPersona = gptp.IdPersona join
GlobalMaster_TipoPersona gtp on gtp.IdTipoPersona = gptp.IdTipoPersona");

$querycom1 = pg_query("select * from GlobalMaster_TipoPersona");

$lista="
    <div class='row'>
      <div class='col-sm-12 col-md-12'>
        <div class='block-flat'>


		<div class='panel-group accordion accordion-semi' id='accordion4'>
							  <div class='panel panel-default'>
								<div class='panel-heading success'>
								  <h4 class='panel-title'>
									<a data-toggle='collapse' data-parent='#accordion4' href='#ac4-1'>
										<i class='fa fa-angle-right'></i> Agregar Nueva Persona
									</a>
								  </h4>
								</div>
								<div id='ac4-1' class='panel-collapse collapse in'>
								  <div class='panel-body'>
									
								          <div class='content'>

									          <form enctype='multipart/form-data' action='".FULL_URL."main/insertar_firma' method='POST' ajax='false' autocomplete='off' style='width:60%;'>
									            
									          	<div class='form-group'>";

								                $lista.= "<label>Busca Persona:</label> </br>
								                <select class='select2' id='tipopersona' name='tipopersona'>";
								                while ($retorno = pg_fetch_object($querycom1)){
															$lista.= "<option value='$retorno->idtipopersona'>$retorno->nomtipopersona</option>";
														}
								                $lista.= "</select>
								                </div>

									            <div class='form-group'>
									              <input type='hidden' id='idpersona' name='idpersona'>
									              <label>Nombre Persona</label> <input type='text' id='nompersona' name='nompersona' placeholder='Nombre de Persona' class='form-control'>
									            </div>
									            <div class='form-group'> 
									              <label>Apellido Persona</label> <input type='text' id='apepersona' name='apepersona'  placeholder='Apellido de Persona' class='form-control'>
									            </div>
									            <div class='form-group'> 
									              <label>DNI Persona</label> <input type='text' id='dnipersona' name='dnipersona'  placeholder='DNI Persona' class='form-control'>
									            </div>

								                <div class='form-group'>
									            <label>Foto de Persona</label> 
									            <div class='fileupload fileupload-new' data-provides='fileupload'>
				                                    <div class='fileupload-preview thumbnail' style='width:20%;height:20%;'></div>
				           
				                                    <div class='uneditable-input span4'><i class='icon-file fileupload-exists'></i>
				                                      <input type='file' id='foto' name='foto' required/>
				                                    </div>
								                	<br>
								                	<a href='#' class='btn fileupload-exists' data-dismiss='fileupload'>Remover Archivo</a>
				                                </div>
								                </div>
								                		
									            <div class='form-group'>
									            <label>Firma de Persona</label> 
									            <div class='fileupload fileupload-new' data-provides='fileupload'>
				                                    <div class='fileupload-preview thumbnail' style='width:20%;height:20%;'></div>
				           
				                                    <div class='uneditable-input span4'><i class='icon-file fileupload-exists'></i>
				                                      <input type='file' id='archivos' name='archivos' required/>
				                                    </div>
								                	<br>
								                	<a href='#' class='btn fileupload-exists' data-dismiss='fileupload'>Remover Archivo</a>
				                                </div>
				                                </div>
								                		
								                <div class='form-group'>
				                                
				                               		<button type='submit' class='btn btn-small btn-danger'>Grabar Datos</button>
								                </div>
									          </form>
								          
								          </div>

								  </div>
								</div>
							  </div>
				</div>

          
         
			      <div class='row'>
			        <div class='col-md-12'>
			          <div class='block-flat'>

			            

			            <div class='content'>
			            <div class='tabla5'>

			          
			            <div class=''>

			              <div class='table-responsive'>
			              <form role='form' id='formpersonacrud' method='post'> 
			                <table class='no-border' id='datatablecrudpersonas'>

			                  <thead class='primary-emphasis'>
			                    <tr>
			                      <th></th>
			                      <th>Codigo</th>
			                      <th>Nombre</th>
			                      <th>Apellido</th>
			                      <th>DNI</th>
								  <th>Foto</th>
			                      <th>Firma</th>
			                      <th>Tipo de Persona</th>
			                      <th>Estado</th>
			                    </tr>

			                  </thead>
			                  <tbody class='no-border'>";


			                    while ($retorno = pg_fetch_object($query)){

			                       $var=utf8_decode($retorno->idpersona);
			                       $var1=utf8_decode($retorno->nompersona);
			                       $var2=utf8_decode($retorno->apepersona);
			                       $var4=utf8_decode($retorno->dnipersona);
			                       $var3=utf8_decode($retorno->firpersona); 
			                       $var31=utf8_decode($retorno->idtipopersona); 
			                       $var5=utf8_decode($retorno->idestado);
			                       $var7=utf8_decode($retorno->nomestado);
			                       $var6=utf8_decode($retorno->nomtipopersona);
			                       $var_foto=utf8_decode($retorno->foto);

			                       $lista.= "<tr class='odd gradeX'>

			                                  

			                                  <input type ='hidden' id='idper[]' name='idper[]' value='$var'>
			                                  <td style='width:1%;'><input type ='checkbox' id='idperchk[]' name='idperchk[]' value='$var' ></td>
			                                  <td style='width:1%;'>$var</td>
			                                  <td style=''><input type='text' id='nameusuario[]' name='nameusuario[]'  value='$var1' class='form-control'></td>
			                                  <td style=''><input disabled type='text' id='password[]' name='password[]'  value='$var2' class='form-control'></td>
			                                  <td style=''><input type='text' id='apikey[]' name='apikey[]'  value='$var4' class='form-control'></td>
			                                  <td style=''>
			                                  <a class='image-zoom' href='".FULL_URL."$var_foto'><img src='".FULL_URL."$var_foto' class='img-thumbnail' onclick='crash();' style='height:64px;'/></a>
											  </td>
			                                  <td style=''>
			                                  <a class='image-zoom' href='".FULL_URL."$var3'><img src='".FULL_URL."$var3' class='img-thumbnail' onclick='crash();' style='height:64px;'/></a>
											  </td>
			                                  <td style=''><input type='text' id='apikey[]' name='apikey[]'  value='$var6' class='form-control'></td>
			                                  <td style=''><select class='form-control' id='estado[]' name='estado[]' value='$var3' >
			                                      <option value='$var5'>$var7</option>
			                                      ";
			                                  $querydet = pg_query("select ME.IdEstado as IdEstado,NomEstado as NomEstado from ComMovTab_MaestrosEstado ME  
			                                  where ME.EstadoSup = 2 and ME.IdEstado!=$var5");
			                                   while ($retornodet = pg_fetch_object($querydet)){
			                                    $idest=$retornodet->idestado;
			                                    $nomest=$retornodet->nomestado;

			                                    $lista.="<option value='$idest'>$nomest</option>"; 
			                                   } 
			                                  $lista.="</select></td>
			                                </tr>";
			                       }
			                  $lista.="</tbody>

			                </table>
			                <button class='btn btn-success' type='button' onclick='crudpersona(2)'>Grabar</button>
			                <button class='btn btn-danger' type='button' onclick='crudpersona(3)'>Eliminar</button>
			              </form>              
			              </div>

			              </div>
			              </div>
			              
			            </div>
			          </div>        
			        </div>

			      </div>


        </div>				
      </div>
      

	</div>";

echo $lista;

?>
