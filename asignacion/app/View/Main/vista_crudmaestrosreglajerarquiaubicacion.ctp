<?php
ini_set('max_execution_time', 300);
date_default_timezone_set('America/Lima');
//require "../conexion.php";
require_once(APP."View/Main/conexion.ctp");

$query = pg_query("select 
GMRJU.IdReglaJerarquiaUbicacion as IdReglaJerarquiaUbicacion,
GMRJU.NombreReglaJerarquiaUbicacion as NombreReglaJerarquiaUbicacion,
GMRJU.IdEstado as IdEstado,
ME.NomEstado as NomEstado
 from GlobalMaster_ReglaJerarquiaUbicacion GMRJU
inner join ComMovTab_MaestrosEstado ME on GMRJU.IdEstado = ME.IdEstado ");


$lista="
	
    <div class='row'>
      <div class='col-sm-12 col-md-12'>
        <div class='block-flat'>

        <div class='panel-group accordion accordion-semi' id='accordion4'>
					  <div class='panel panel-default'>
						<div class='panel-heading success'>
						  <h4 class='panel-title'>
							<a data-toggle='collapse' data-parent='#accordion4' href='#ac4-1'>
								<i class='fa fa-angle-right'></i> Agregar Nueva Regla de Jerarquias
							</a>
						  </h4>
						</div>
						<div id='ac4-1' class='panel-collapse collapse in'>
						  <div class='panel-body'>
							
						          <div class='content'>

							          <form role='form' id='formreglajerarquiaubicacion' method='post'> 
							            <div class='form-group'>
							              <input type='hidden' id='idreglajerarquiaubicacion' name='idreglajerarquiaubicacion'>
							              <label>Nombre de Regla Jerarquia</label> <input type='text' id='nombrereglajerarquiaubicacion' name='nombrereglajerarquiaubicacion' placeholder='Nombre de Regla' class='form-control'>
							            </div>
							              <button class='btn btn-danger' type='button' onclick='crudmaestrosreglajerarquiaubicacion(1)'>Grabar</button>
							              <button class='btn btn-default' type='button'>Cancelar</button>
							          </form>
						          
						          </div>

						  </div>
						</div>
					  </div>
					</div>

						      <div class='row'>
						        <div class='col-md-12'>
						          <div class='block-flat'>

						            

						            <div class='content'>
						            <div class='tabla'>

						          
						            <div class=''>

						              <div class='table-responsive'>
						              <form role='form' id='formreglajerarquiaubicacioncrud' method='post'> 
						                <table class='no-border' id='datatablecrudreglajerarquiaubicacion'>

						                  <thead class='primary-emphasis'>
						                    <tr>
						                      <th></th>
						                      <th>Codigo</th>
						                      <th>Nombre Regla</th>
						                      <th>Estado</th>
						                    </tr>

						                  </thead>
						                  <tbody class='no-border'>";


						                    while ($retorno = pg_fetch_object($query)){

						                       $var=utf8_decode($retorno->idreglajerarquiaubicacion);
						                       $var1=utf8_decode($retorno->nombrereglajerarquiaubicacion);
						                       $var4=utf8_decode($retorno->idestado);
						                       $var41=utf8_decode($retorno->nomestado);

						                       $lista.= "<tr class='odd gradeX'>
						                                  <input type ='hidden' id='idreglajerarquiaubicacioncrud[]' name='idreglajerarquiaubicacioncrud[]' value='$var'>
						                                  <td style='width:1%;'><input type ='checkbox' id='idchkreglajerarquiaubicacioncrud[]' name='idchkreglajerarquiaubicacioncrud[]' value='$var' ></td>
						                                  <td style='width:1%;'>$var</td>
						                                  <td style='width:20%;'><input type='text' id='nombrereglajerarquiaubicacioncrud[]' name='nombrereglajerarquiaubicacioncrud[]'  value='$var1' class='form-control'></td>
						                                  <td style='width:10%;'><select class='form-control' id='estadoreglajerarquiacrud[]' name='estadoreglajerarquiacrud[]' value='$var41' >    
						                                      <option value='$var4'>$var41</option>
						                                      ";
						                                  $querydet = pg_query("select ME.IdEstado as IdEstado,NomEstado as NomEstado from ComMovTab_MaestrosEstado ME  
						                                  where ME.EstadoSup = 2 and ME.IdEstado!=$var4");
						                                   while ($retornodet = pg_fetch_object($querydet)){
						                                    $idest=$retornodet->idestado;
						                                    $nomest=$retornodet->nomestado;

						                                    $lista.="<option value='$idest'>$nomest</option>"; 
						                                   } 
						                                  $lista.="</select></td>
						                                </tr>";
						                       }
						                  $lista.="</tbody>

						                </table>
						                <button class='btn btn-success' type='button' onclick='crudmaestrosreglajerarquiaubicacion(2)'>Grabar</button>
						                <button class='btn btn-danger' type='button' onclick='crudmaestrosreglajerarquiaubicacion(3)'>Eliminar</button>
						              </form>              
						              </div>

						              </div>
						              </div>
						              
						            </div>
						          </div>        
						        </div>

						      </div>

        </div>				
      </div>
      

	</div>";

echo $lista;

?>