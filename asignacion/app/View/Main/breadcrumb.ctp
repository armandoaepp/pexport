<?php
/*
 * Cambiamos la cabecera de la navegacion segun la variable enviada
* @autor Alan Hugo
*/
$lista2 ='';
switch ($navegador) {
	case 'monitoreo' :
		$lista2 .= "<div class='page-head' style='padding:0px 0px;'>
				<ol class='breadcrumb'>
				<li><button type='button' class='btn btn-default btn-xs' onclick='arbol()'><i class='fa fa-arrows-h'></i></button>
				<button type='button' class='btn btn-default btn-xs' onclick='sintesis()'><i class='fa fa-arrows'></i></button>
				</li>

				<li><a href='#''>Monitorear</a></li>
				<li><a href='#''>Reclamo</a></li>";
		
		switch ($case) {
			case '1' :
				$lista2 .= "<li class='active'>Ver Todo Reclamos</li>
									</ol>
									</div>";
				echo $lista2;
				break;
			case '2' :
				$lista2 .= "<li class='active'>Inspección por corte de servicio</li>
									</ol>
									</div>";
				echo $lista2;
				break;
			case '2' :
				$lista2 .= "<li class='active'>Inspección por corte de servicio</li>
									</ol>
									</div>";
				echo $lista2;
				break;
			case '3' :
				$lista2 .= "<li class='active'>Inspección por cambio de datos de suministro </li>
									</ol>
									</div>";
				echo $lista2;
				break;
			case '10' :
				$lista2 .= "<li class='active'>Inspección por artefactos/equipos quemados </li>
									</ol>
									</div>";
				echo $lista2;
				break;
			case '13' :
				$lista2 .= "<li class='active'>Inspección por variación de tensión y toma de carga </li>
									</ol>
									</div>";
				echo $lista2;
				break;
			case '14' :
				$lista2 .= "<li class='active'>Queja por recibos no entregados</li>
									</ol>
									</div>";
				echo $lista2;
				break;
			case '15' :
				$lista2 .= "<li class='active'>Inspección por exceso de consumo completa</li>
									</ol>
									</div>";
				echo $lista2;
				break;
			case '16' :
				$lista2 .= "<li class='active'>Inspección por toma de lectura (únicamente)</li>
									</ol>
									</div>";
				echo $lista2;
				break;
			case '17' :
				$lista2 .= "<li class='active'>Toma de vistas fotográficas </li>
									</ol>
									</div>";
				echo $lista2;
				break;
		}
		break;
	case 'monitorearav':
		$lista="<div class='page-head' style='padding:0px 0px;'>
				<ol class='breadcrumb'>
				<li><button type='button' class='btn btn-default btn-xs' onclick='arbol()'><i class='fa fa-arrows-h'></i></button>
				<button type='button' class='btn btn-default btn-xs' onclick='sintesis()'><i class='fa fa-arrows'></i></button>
				</li>

				<li><a href='#''>Monitorear</a></li>
				<li><a href='#''>General</a></li>
				<li class='active'>Avance</li>
				</ol>
				</div>";
		echo $lista;
		break;
	case 'emergencias':
		$lista="<div class='page-head' style='padding:0px 0px;'>
				<ol class='breadcrumb'>
				<li><button type='button' class='btn btn-default btn-xs' onclick='arbol()'><i class='fa fa-arrows-h'></i></button></li>
				<li><a href='#''>Emergencias</a></li>
				</ol>
				</div>";
		echo $lista;
		break;
	case 'importar':
		$lista2="<div class='page-head' style='padding:0px 0px;'>
				<ol class='breadcrumb'>
				<li><button type='button' class='btn btn-default btn-xs' onclick='arbol()'><i class='fa fa-arrows-h'></i></button></li>
				<li><a href='#''>Importar</a></li>
				<li><a href='#''>Archivo</a></li>
				<li class='active'>Subir Archivos</li>
				</ol>
				</div>";
		echo $lista2;
		break;
	case 'vistapreviaarchivo':
		$lista2="<div class='page-head' style='padding:0px 0px;'>
				<ol class='breadcrumb'>
				<li><button type='button' class='btn btn-default btn-xs' onclick='arbol()'><i class='fa fa-arrows-h'></i></button></li>
				<li><a href='#''>Importar</a></li>
				<li><a href='#''>Pre Procesamiento de Archivo</a></li>
				<li class='active'>Ver Vista Previa</li>
				</ol>
				</div>";
		echo $lista2;
		break;
	case 'vistapreviaarchivodetalle':
		$lista2="<div class='page-head' style='padding:0px 0px;'>
				<ol class='breadcrumb'>
				<li><button type='button' class='btn btn-default btn-xs' onclick='arbol()'><i class='fa fa-arrows-h'></i></button></li>
				<li><a href='#''>Importar</a></li>
				<li><a href='#''>Pre Procesamiento de Archivo</a></li>
				<li class='active'>Ver Vista Previa Detalle</li>
				</ol>
				</div>";
		echo $lista2;
		break;
	case 'consultarws':
		$lista2="<div class='page-head' style='padding:0px 0px;'>
				<ol class='breadcrumb'>
				<li><button type='button' class='btn btn-default btn-xs' onclick='arbol()'><i class='fa fa-arrows-h'></i></button></li>
				<li><a href='#''>Importar</a></li>
				<li><a href='#''>Consultar en Linea</a></li>
				<li class='active'>Solicitar Descarga OT</li>
				</ol>
				</div>";
		echo $lista2;
		break;
	case 'crudmaestrosconceptoubicacion':
		$lista2="<div class='page-head' style='padding:0px 0px;'>
				<ol class='breadcrumb'>
				<li><button type='button' class='btn btn-default btn-xs' onclick='arbol()'><i class='fa fa-arrows-h'></i></button></li>
				<li><a href='#''>Maestros</a></li>
				<li><a href='#''>Conceptos Asignacion</a></li>
				<li class='active'>Conceptos Ubicacion</li>
				</ol>
				</div>";
		echo $lista2;
		break;
	case 'crudmaestrosreglajerarquiaubicacion':
		$lista2="<div class='page-head'>

				<ol class='breadcrumb'>
				<li><button type='button' class='btn btn-default btn-xs' onclick='arbol()'><i class='fa fa-arrows-h'></i></button></li>
				<li><a href='#''>Maestros</a></li>
				<li><a href='#''>Conceptos Asignacion</a></li>
				<li class='active'>Regla Jerarquia Ubicacion</li>
				</ol>
				</div>";
		echo $lista2;
		break;
	case 'crudmaestrosdetallereglajerarquiaubi':
		$lista2="<div class='page-head' style='padding:0px 0px;'>
				<ol class='breadcrumb'>
				<li><button type='button' class='btn btn-default btn-xs' onclick='arbol()'><i class='fa fa-arrows-h'></i></button></li>
				<li><a href='#''>Maestros</a></li>
				<li><a href='#''>Conceptos Asignacion</a></li>
				<li class='active'>Composicion Regla Jerarquia</li>
				</ol>
				</div>";
		echo $lista2;
		break;
	case 'crudmaestrosubicacion':
		$lista2="<div class='page-head'>
				<ol class='breadcrumb'>
				<li><button type='button' class='btn btn-default btn-xs' onclick='arbol()'><i class='fa fa-arrows-h'></i></button></li>
				<li><a href='#''>Maestros</a></li>
				<li><a href='#''>Regla Asignacion</a></li>
				<li class='active'>Poblacion Ubicacion</li>
				</ol>
				</div>";
		echo $lista2;
		break;
	case 'crudmaestrosubicacionregla':
		$lista2="<div class='page-head'>
				<ol class='breadcrumb'>
				<li><button type='button' class='btn btn-default btn-xs' onclick='arbol()'><i class='fa fa-arrows-h'></i></button></li>
				<li><a href='#''>Maestros</a></li>
				<li><a href='#''>Regla Asignacion</a></li>
				<li class='active'>Regla Ubicacion</li>
				</ol>
				</div>";
		echo $lista2;
		break;
	case 'crudmaestrosdetalleubicacionregla':
		$lista2="<div class='page-head' style='padding:0px 0px;'>
				<ol class='breadcrumb'>
				<li><button type='button' class='btn btn-default btn-xs' onclick='arbol()'><i class='fa fa-arrows-h'></i></button></li>
				<li><a href='#''>Maestros</a></li>
				<li><a href='#''>Regla Asignacion</a></li>
				<li class='active'>Composicion Regla Ubicacion</li>
				</ol>
				</div>";
		echo $lista2;
		break;
	case 'crudmaestrostipoasignacion':
		$lista2="<div class='page-head'>
				<ol class='breadcrumb'>
				<li><button type='button' class='btn btn-default btn-xs' onclick='arbol()'><i class='fa fa-arrows-h'></i></button></li>
				<li><a href='#''>Maestros</a></li>
				<li><a href='#''>Define Asignacion</a></li>
				<li class='active'>Tipo Asignacion</li>
				</ol>
				</div>";
		echo $lista2;
		break;
	case 'crudmaestrosintervencion':
		$lista2="<div class='page-head' style='padding:0px 0px;'>
				<ol class='breadcrumb'>
				<li><button type='button' class='btn btn-default btn-xs' onclick='arbol()'><i class='fa fa-arrows-h'></i></button></li>
				<li><a href='#''>Maestros</a></li>
				<li><a href='#''>Define Asignacion</a></li>
				<li class='active'>Elementos Formulario</li>
				</ol>
				</div>";
		echo $lista2;
		break;
	case 'crudmaestrostipomaestrosasignacion':
		$lista2="<div class='page-head'>
				<ol class='breadcrumb'>
				<li><button type='button' class='btn btn-default btn-xs' onclick='arbol()'><i class='fa fa-arrows-h'></i></button></li>
				<li><a href='#''>Maestros</a></li>
				<li><a href='#''>Define Asignacion</a></li>
				<li class='active'>Diseña Formulario</li>
				</ol>
				</div>";
		echo $lista2;
		break;
	case 'enlaces':
		$lista2="<div class='page-head' style='padding:0px 0px;'>
				<h2>Sistema</h2>
				<ol class='breadcrumb'>
				<li><button type='button' class='btn btn-default btn-xs' onclick='arbol()'><i class='fa fa-arrows-h'></i></button></li>
				<li><a href='#''>Enlaces</a></li>
				</ol>
				</div>";
		echo $lista2;
		break;
	case 'crudusuarios':
		$lista2="<div class='page-head' style='padding:0px 0px;'>
				<ol class='breadcrumb'>
				<li><button type='button' class='btn btn-default btn-xs' onclick='arbol()'><i class='fa fa-arrows-h'></i></button></li>
				<li><a href='#''>Usuarios</a></li>
				<li><a href='#''>Gestion Usuario</a></li>
				<li class='active'>Nuevo Usuario</li>
				</ol>
				</div>";
		echo $lista2;
		break;
	case 'crudasignaequipo':
		$lista2="<div class='page-head' style='padding:0px 0px;'>
				<ol class='breadcrumb'>
				<li><button type='button' class='btn btn-default btn-xs' onclick='arbol()'><i class='fa fa-arrows-h'></i></button></li>
				<li><a href='#''>Usuarios</a></li>
				<li><a href='#''>Gestion Usuario</a></li>
				<li class='active'>Asignacion Equipo</li>
				</ol>
				</div>";
		echo $lista2;
		break;
	case 'crudpersonas':
		$lista2="<div class='page-head' style='padding:0px 0px;'>
				<ol class='breadcrumb'>
				<li><button type='button' class='btn btn-default btn-xs' onclick='arbol()'><i class='fa fa-arrows-h'></i></button></li>
				<li><a href='#''>Usuarios</a></li>
				<li><a href='#''>Gestion Usuario</a></li>
				<li class='active'>Nuevo Persona</li>
				</ol>
				</div>";
		echo $lista2;
		break;
	case 'asignar':
		$lista2="<div class='page-head' style='padding:0px 0px;'>
				<ol class='breadcrumb'>
				<li><button type='button' class='btn btn-default btn-xs' onclick='arbol()'><i class='fa fa-arrows-h'></i></button></li>
				<li><a href='#''>Asignar</a></li>
				<li><a href='#''>Reglas</a></li>
				<li class='active'>Ver Definiciones</li>
				</ol>
				</div>";
		echo $lista2;
		break;
	case 'usuarios':
		$lista="
				<div class='title'>
				<h2>Usuarios</h2>
				</div>
				<ul class='nav nav-list treeview'>
				<li class='open'><label class='tree-toggler nav-header'><i class='fa fa-folder-open-o'></i>Gestion Usuario</label>
				<ul class='nav nav-list tree'>
				<li ><a href='#' onclick='showUsu1()''>Nuevo Usuario</a></li>
				<li ><a href='#' onclick='showUsu2()''>Asignacion Equipo</a></li>
				<li ><a href='#' onclick='showPer1()''>Personas</a></li>
				</ul>
				</li>
				</ul>
				";

		echo $lista;
		break;
}
?>
