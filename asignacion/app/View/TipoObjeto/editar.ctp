<div class="page-aside app tree" id="pageContent">
	<div class="fixed nano nscroller has-scrollbar">
		<div class="content" tabindex="0" style="right: -15px;">
			<div class="header">
				<button class="navbar-toggle" data-target=".treeview"
					data-toggle="collapse" type="button">
					<span class="fa fa-chevron-down"></span>
				</button>
				<h2 class="page-title">Tipo Objeto</h2>
				<p class="description">Mantenimiento</p>
			</div>
			<?php echo $this->element('menu_mantenimiento'); ?>
		</div>

		<div class="pane" style="display: block;">
			<div class="slider" style="height: 428px; top: 0px;"></div>
		</div>
	</div>
</div>
<div class="container-fluid" id="pcont">
	<div id="cabeza">
		<div class="page-head" style="padding: 0px 0px;">
			<ol class="breadcrumb">
				<li><button type="button" class="btn btn-default btn-xs"
						onclick="arbol()">
						<i class="fa fa-arrows-h"></i>
					</button></li>
				<li><a href="#">Mantenimiento</a></li>
				<li><a href="#">Tipo Objeto</a></li>
				<li class="active">Editar</li>
			</ol>
		</div>
	</div>
	<div class="cl-mcont">

		<div class="row">
			<div class="col-md-12">
				<div class="block-flat">
					<div class="header">
						<h3>Editar: Tipo de Objeto</h3>
					</div>
					<div class="content">

						<form class="form-horizontal group-border-dashed" action="<?php echo FULL_URL;?>tipo_objeto/editar/<?php echo $obj_tipo_objeto->getID();?>" method="post" style="border-radius: 0px;">
						
							<div class="form-group">
								<label class="col-sm-3 control-label">Nombre</label>
								<div class="col-sm-6">
									<input type="text" class="form-control" name="data[TipoObjeto][nombre]" id="TipoObjetoNombre" value="<?php echo $obj_tipo_objeto->getAttr('nombre');?>">
								</div>
							</div>

							<div class="form-group">
								<label class="col-sm-3 control-label">Descripci&oacute;n</label>
								<div class="col-sm-6">
									<input type="text" class="form-control" name="data[TipoObjeto][descripcion]" id="TipoObjetoDescripcion" value="<?php echo $obj_tipo_objeto->getAttr('descripcion');?>">
								</div>
							</div>

							<div class="form-group">
								<label class="col-sm-3 control-label">Salida</label>
								<div class="col-sm-2">
									<select class="form-control" name="data[TipoObjeto][salida]" id="TipoObjetoSalida">
										<option value="U" <?php if($obj_tipo_objeto->getAttr('salida')=='U'){ echo 'selected';}?>>&Uacute;nica</option>
										<option value="M" <?php if($obj_tipo_objeto->getAttr('salida')=='M'){ echo 'selected';}?>>Multiple</option>
										<option value="N" <?php if($obj_tipo_objeto->getAttr('salida')=='N'){ echo 'selected';}?>>Ninguna</option>
									</select>
								</div>
							</div>
							
							<div class="form-group">
								<label class="col-sm-3 control-label">Tiene Datos</label>
								<div class="col-sm-2">
									<select class="form-control" name="data[TipoObjeto][datos]" id="TipoObjetoDatos">
										<option value="N" <?php if($obj_tipo_objeto->getAttr('datos')=='N'){ echo 'selected';}?>>No</option>
										<option value="I" <?php if($obj_tipo_objeto->getAttr('datos')=='I'){ echo 'selected';}?>>Internos</option>
										<option value="E" <?php if($obj_tipo_objeto->getAttr('datos')=='E'){ echo 'selected';}?>>Externos</option>
									</select>
								</div>
							</div>
							
							<div class="form-group">
								<label class="col-sm-3 control-label">Tiene Objetos</label>
								<div class="col-sm-2">
									<select class="form-control" name="data[TipoObjeto][objetos]" id="TipoObjetoObjetos">
										<option value="0" <?php if($obj_tipo_objeto->getAttr('objetos')=='0'){ echo 'selected';}?>>No</option>
										<option value="1" <?php if($obj_tipo_objeto->getAttr('objetos')=='1'){ echo 'selected';}?>>Si</option>
									</select>
								</div>
							</div>

							<div class="form-group">
								<div class="col-sm-offset-3 col-sm-9">
									<button type="submit" class="btn btn-primary">Grabar</button>
									<a href="<?php echo FULL_URL;?>tipo_objeto" class="btn btn-default">Cancelar</a>
								</div>
							</div>

						</form>

					</div>
				</div>
			</div>
		</div>
	</div>
</div>