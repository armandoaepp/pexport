<div class="page-aside app tree" id="pageContent">
	<div class="fixed nano nscroller has-scrollbar">
		<div class="content" tabindex="0" style="right: -15px;">
			<div class="header">
				<button class="navbar-toggle" data-target=".treeview"
					data-toggle="collapse" type="button">
					<span class="fa fa-chevron-down"></span>
				</button>
				<h2 class="page-title">Objeto</h2>
				<p class="description">Mantenimiento</p>
			</div>
			<?php echo $this->element('menu_mantenimiento'); ?>
		</div>

		<div class="pane" style="display: block;">
			<div class="slider" style="height: 428px; top: 0px;"></div>
		</div>
	</div>
</div>
<div class="container-fluid" id="pcont">
	<div id="cabeza">
		<div class="page-head" style="padding: 0px 0px;">
			<ol class="breadcrumb">
				<li><button type="button" class="btn btn-default btn-xs"
						onclick="arbol()">
						<i class="fa fa-arrows-h"></i>
					</button></li>
				<li><a href="#">Mantenimiento</a></li>
				<li><a href="#">Objeto</a></li>
				<li class="active">Nuevo</li>
			</ol>
		</div>
	</div>
	<div class="cl-mcont">

		<div class="row">
			<div class="col-md-12">
				<div class="block-flat">
					<div class="header">
						<h3>Nuevo: Objeto</h3>
					</div>
					<div class="content">

						<form class="form-horizontal group-border-dashed" action="<?php echo FULL_URL;?>objeto/nuevo" method="post" style="border-radius: 0px;">
						
							<div class="form-group">
								<label class="col-sm-3 control-label">Nombre</label>
								<div class="col-sm-6">
									<input type="text" class="form-control" name="data[Objeto][nombre]" id="ObjetoNombre">
								</div>
							</div>

							<div class="form-group">
								<label class="col-sm-3 control-label">Etiqueta</label>
								<div class="col-sm-6">
									<input type="text" class="form-control" name="data[Objeto][etiqueta]" id="ObjetoEtiqueta">
								</div>
							</div>
							
							<div class="form-group">
								<label class="col-sm-3 control-label">Descripci&oacute;n</label>
								<div class="col-sm-6">
									<input type="text" class="form-control" name="data[Objeto][descripcion]" id="ObjetoDescripcion">
								</div>
							</div>
							
							<div class="form-group">
								<label class="col-sm-3 control-label">Tipo Objeto</label>
								<div class="col-sm-5">
									<select class="form-control" name="data[Objeto][tipo_objeto_id]" id="ObjetoTipoObjetoId">
										<?php foreach ($arr_obj_tipo_objeto as $k => $obj_tipo_objeto){?>
										<option value="<?php echo $obj_tipo_objeto->getID();?>" data-datos="<?php echo $obj_tipo_objeto->datos;?>"><?php echo $obj_tipo_objeto->getAttr('nombre');?></option>
										<?php }?>
									</select>
								</div>
								<div class="col-sm-1">
									<a href="<?php echo FULL_URL;?>tipo_objeto/nuevo" target="_blank"><i class="fa fa-plus-square"></i></a>
								</div>
							</div>
							
							<div class="form-group">
								<label class="col-sm-3 control-label">Tipo Dato</label>
								<div class="col-sm-5">
									<select class="form-control" name="data[Objeto][tipo_dato_id]" id="ObjetoTipoDatoId">
										<?php foreach ($arr_obj_tipo_dato as $k => $obj_tipo_dato){?>
										<option value="<?php echo $obj_tipo_dato->getID();?>"><?php echo $obj_tipo_dato->getAttr('nombre');?></option>
										<?php }?>
									</select>
								</div>
								<div class="col-sm-1">
									<a href="<?php echo FULL_URL;?>tipo_dato/nuevo" target="_blank"><i class="fa fa-plus-square"></i></a>
								</div>
							</div>
							
							<div class="form-group">
								<label class="col-sm-3 control-label">Longitud</label>
								<div class="col-sm-6">
									<input type="text" class="form-control" name="data[Objeto][longitud]" id="ObjetoLongitud">
								</div>
							</div>
							
							<div class="form-group">
								<label class="col-sm-3 control-label">Valor por Defecto</label>
								<div class="col-sm-6">
									<input type="text" class="form-control" name="data[Objeto][defecto]" id="ObjetoDefecto">
								</div>
							</div>
							
							<div class="form-group">
								<label class="col-sm-3 control-label">Valor por Defecto desde Campo</label>
								<div class="col-sm-6">
									<select class="form-control" name="data[Objeto][defecto_desde_campo]" id="ObjetoDefectoDesdeCampo">
										<option value="">Ninguno</option>
										<?php foreach ($arr_campos_defecto as $k => $dato_campo){?>
										<option value="<?php echo $dato_campo[0]['table_schema'].'.'.$dato_campo[0]['table_name'].'.'.$dato_campo[0]['column_name'];?>"><?php echo $dato_campo[0]['table_schema'].'.'.$dato_campo[0]['table_name'].'.'.$dato_campo[0]['column_name'];?></option>
										<?php }?>
									</select>
								</div>
							</div>
							
							<div class="form-group">
								<label class="col-sm-3 control-label">Placeholder</label>
								<div class="col-sm-6">
									<input type="text" class="form-control" name="data[Objeto][placeholder]" id="Objetoplaceholder">
								</div>
							</div>
							
							<div class="form-group div_objeto_origen_dato_id" style="display: none;">
								<label class="col-sm-3 control-label">Origen de Datos</label>
								<div class="col-sm-5">
									<select class="form-control" name="data[Objeto][objeto_origen_dato_id]" id="ObjetoObjetoOrigenDatoId">
										<option value="">Ninguno</option>
										<?php foreach ($arr_obj_objeto_origen_dato as $k => $obj_objeto_origen_dato){?>
										<option value="<?php echo $obj_objeto_origen_dato->getID();?>"><?php echo $obj_objeto_origen_dato->getAttr('nombre');?></option>
										<?php }?>
									</select>
								</div>
								<div class="col-sm-1">
									<a href="<?php echo FULL_URL;?>objeto_origen_dato/nuevo" target="_blank"><i class="fa fa-plus-square"></i></a>
								</div>
							</div>

							<div class="form-group">
								<div class="col-sm-offset-3 col-sm-9">
									<button type="submit" class="btn btn-primary">Grabar</button>
									<a href="<?php echo FULL_URL;?>objeto" class="btn btn-default">Cancelar</a>
								</div>
							</div>

						</form>

					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<script>
$('#ObjetoTipoObjetoId').change(function (){
	if($(this).find(':selected').data('datos')=='E'){
		$('.div_objeto_origen_dato_id').show();
	}else{
		$('.div_objeto_origen_dato_id').hide();
	}
});
</script>