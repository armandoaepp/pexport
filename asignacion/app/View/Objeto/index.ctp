<div class="page-aside app tree" id="pageContent">
	<div class="fixed nano nscroller has-scrollbar">
		<div class="content" tabindex="0" style="right: -15px;">
			<div class="header">
				<button class="navbar-toggle" data-target=".treeview"
					data-toggle="collapse" type="button">
					<span class="fa fa-chevron-down"></span>
				</button>
				<h2 class="page-title">Objeto</h2>
				<p class="description">Mantenimiento</p>
			</div>
			<?php echo $this->element('menu_mantenimiento'); ?>
		</div>

		<div class="pane" style="display: block;">
			<div class="slider" style="height: 428px; top: 0px;"></div>
		</div>
	</div>
</div>
<div class="container-fluid" id="pcont">
	<div id="cabeza">
		<div class="page-head" style="padding: 0px 0px;">
			<ol class="breadcrumb">
				<li><button type="button" class="btn btn-default btn-xs"
						onclick="arbol()">
						<i class="fa fa-arrows-h"></i>
					</button></li>
				<li><a href="#">Mantenimiento</a></li>
				<li><a href="#">Objeto</a></li>
				<li class="active">Listado</li>
			</ol>
		</div>
	</div>
	<div class="cl-mcont">

		<div class="row">
			<div class="col-md-12">
				<div class="block-flat">
					<div class="header">
						<h3>Objetos</h3>
						<a href="<?php echo FULL_URL;?>objeto/nuevo" class="btn btn-primary pull-right" style="margin-top: -40px;">Nuevo</a>
					</div>
					<div class="content">
					
						<?php echo $this->Session->flash(); ?>
					
						<div class="table-responsive">
							<table class="table table-bordered" id="datatable">
								<thead>
									<tr>
										<th>Id</th>
										<th>Nombre</th>
										<th>Etiqueta</th>
										<th>Descripci&oacute;n</th>
										<th>Tipo Objeto</th>
										<th>Tipo Dato</th>
										<th>Longitud</th>
										<th>Defecto desde Campo</th>
										<th>Placeholder</th>
										<th style="width: 115px;">Acci&oacute;n</th>
									</tr>
								</thead>
								<tbody>
										<?php 
										foreach ($arr_obj_objeto as $k => $obj_objeto){
										?>
										<tr>
											<td><?php echo $obj_objeto->getID();?></td>
											<td><?php echo $obj_objeto->getAttr('nombre');?></td>
											<td><?php echo $obj_objeto->getAttr('etiqueta');?></td>
											<td><?php echo $obj_objeto->getAttr('descripcion');?></td>
											<td><?php echo $obj_objeto->TipoObjeto->getAttr('nombre');?></td>
											<td><?php echo $obj_objeto->TipoDato->getAttr('nombre');?></td>
											<td><?php echo $obj_objeto->getAttr('longitud');?></td>
											<td><?php echo $obj_objeto->getAttr('defecto_desde_campo');?></td>
											<td><?php echo $obj_objeto->getAttr('placeholder');?></td>
											<td>
												<div class="btn-group">
													<button type="button" class="btn btn-default">Acci&oacute;n</button>
													<button type="button"
														class="btn btn-primary dropdown-toggle"
														data-toggle="dropdown">
														<span class="caret"></span> <span class="sr-only">Seleccionar</span>
													</button>
													<ul class="dropdown-menu" role="menu">
														<li><a href="<?php echo FULL_URL;?>objeto/editar/<?php echo $obj_objeto->getID();?>">Editar</a></li>
														<li><a href="<?php echo FULL_URL;?>objeto/eliminar/<?php echo $obj_objeto->getID();?>">Eliminar</a></li>
														<li class="divider"></li>
														<li><a href="<?php echo FULL_URL;?>objeto_atributo/index/<?php echo $obj_objeto->getID();?>">Atributos</a></li>
														<?php if($obj_objeto->TipoObjeto->getAttr('datos')=='I'){?>
														<li><a href="<?php echo FULL_URL;?>objeto_dato/index/<?php echo $obj_objeto->getID();?>">Datos Internos</a></li>
														<?php }elseif($obj_objeto->TipoObjeto->getAttr('datos')=='E'){?>
														<li><a href="<?php echo FULL_URL;?>objeto_origen_dato/editar/<?php echo $obj_objeto->getAttr('objeto_origen_dato_id');?>">Datos Externos: <?php echo $obj_objeto->ObjetoOrigenDato->getAttr('nombre');?></a></li>
														<?php }?>
														<?php if($obj_objeto->TipoObjeto->getAttr('objetos')==true){?>
														<li><a href="<?php echo FULL_URL;?>objeto_grupo/index/<?php echo $obj_objeto->getID();?>">Items (Grupo)</a></li>
														<?php }?>
													</ul>
												</div>
											</td>
										</tr>
										<?php }?>
									</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>