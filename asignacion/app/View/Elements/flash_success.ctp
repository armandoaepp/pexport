<div class="alert alert-success alert-white rounded">
	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
	<div class="icon"><i class="fa fa-check"></i></div>
	<strong>Correcto!</strong> <?php echo $message;?>
</div>