<ul class="nav nav-list treeview collapse">
	<li class="open"><label class="tree-toggler nav-header"><i
			class="fa fa-folder-open-o"></i> Administrar Objetos</label>
		<ul class="nav nav-list tree" style="display: block;">
			<li><a href="<?php echo FULL_URL;?>tipo_objeto">Tipo de Objeto</a></li>
			<li><a href="<?php echo FULL_URL;?>tipo_dato">Tipo de Dato</a></li>
			<li><a href="<?php echo FULL_URL;?>tipo_atributo">Tipo de Atributo</a></li>
			<li><a href="<?php echo FULL_URL;?>objeto_origen_dato">Origen de Datos</a></li>
			<li><a href="<?php echo FULL_URL;?>objeto">Objeto</a></li>
			<li><a href="<?php echo FULL_URL;?>objeto_grupo/manager">Grupos</a></li>
		</ul></li>		
	<li class="open"><label class="tree-toggler nav-header"><i
			class="fa fa-folder-open-o"></i> Administrar Intervenci&oacute;n</label>
		<ul class="nav nav-list tree" style="display: block;">
			<li><a href="<?php echo FULL_URL;?>intervencion_tipo">Tipo Intervenci&oacute;n</a></li>
		</ul></li>
</ul>