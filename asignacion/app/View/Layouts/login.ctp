<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="">
<meta name="author" content="">
<link rel="shortcut icon"
	href="<?php echo FULL_URL ?>images/favicon.png">

<title>Pexport</title>

<link
	href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,400italic,700,800'
	rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Raleway:300,200,100'
	rel='stylesheet' type='text/css'>

<!-- Bootstrap core CSS -->
<link href="<?php echo FULL_URL ?>js/bootstrap/dist/css/bootstrap.css"
	rel="stylesheet">

<link rel="stylesheet"
	href="<?php echo FULL_URL ?>fonts/font-awesome-4/css/font-awesome.min.css">
<link rel="stylesheet" type="text/css"
	href="<?php echo FULL_URL ?>js/jquery.select2/select2.css" />

<!-- Custom styles for this template -->
<link href="<?php echo FULL_URL ?>css/style.css" rel="stylesheet" />


</head>

<body class="texture">

	<div id="cl-wrapper" class="login-container">

		<div class="middle-login">
			<div class="block-flat">
				<div class="header">
					<h3 class="text-center">
						<img class="logo-img" src="<?php echo FULL_URL ?>images/logo.png"
							alt="logo" />Ensa Asignaciones
					</h3>
				</div>
				<div>
					<?php 
					echo $this->fetch('content');
					?>
				</div>
			</div>
			<div class="text-center out-links">
				<a href="#">&copy; 2014 <?php echo Configure::read('acsion.company');?>
				</a>
			</div>
		</div>

	</div>

	<script src="<?php echo FULL_URL ?>js/jquery.js"></script>
	<script type="text/javascript"
		src="<?php echo FULL_URL ?>js/behaviour/general.js"></script>

	<!-- Bootstrap core JavaScript
================================================== -->
	<!-- Placed at the end of the document so the pages load faster -->
	<script src="<?php echo FULL_URL ?>js/behaviour/voice-commands.js"></script>
	<script
		src="<?php echo FULL_URL ?>js/bootstrap/dist/js/bootstrap.min.js"></script>
	<script type="text/javascript"
		src="<?php echo FULL_URL ?>js/jquery.flot/jquery.flot.js"></script>
	<script type="text/javascript"
		src="<?php echo FULL_URL ?>js/jquery.flot/jquery.flot.pie.js"></script>
	<script type="text/javascript"
		src="<?php echo FULL_URL ?>js/jquery.flot/jquery.flot.resize.js"></script>
	<script type="text/javascript"
		src="<?php echo FULL_URL ?>js/jquery.flot/jquery.flot.labels.js"></script>

	<script src="<?php echo FULL_URL ?>js/jquery.select2/select2.min.js"
		type="text/javascript"></script>
	<script type="text/javascript">
		$(".select2").select2({
        width: '100%'
       });
	</script>
<?php echo $this->element('ga');?>
</body>
</html>
