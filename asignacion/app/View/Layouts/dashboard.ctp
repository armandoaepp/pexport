<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="description" content="D&D ANALYTICS">
	<meta name="author" content="D&D ANALYTICS">

	<link rel="shortcut icon"
		href="<?php echo FULL_URL ?>images/favicon.png">

	<title>Pexport</title>

	<link
		href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,400italic,700,800'
		rel='stylesheet' type='text/css'>
	<link href='http://fonts.googleapis.com/css?family=Raleway:300,200,100'
		rel='stylesheet' type='text/css'>
	<link
		href='http://fonts.googleapis.com/css?family=Open+Sans+Condensed:300,700'
		rel='stylesheet' type='text/css'>

	<!-- Bootstrap core CSS -->
	<link href="<?php echo FULL_URL ?>js/bootstrap/dist/css/bootstrap.css"
		rel="stylesheet" />
	<link rel="stylesheet"
		href="<?php echo FULL_URL ?>fonts/font-awesome-4/css/font-awesome.min.css">

	<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!--[if lt IE 9]>
	      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
	    <![endif]-->
	<link rel="stylesheet" type="text/css"
		href="<?php echo FULL_URL ?>js/jquery.gritter/css/jquery.gritter.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo FULL_URL ?>js/jquery.niftymodals/css/component.css" />
	<link rel="stylesheet" type="text/css"
		href="<?php echo FULL_URL ?>js/jquery.nanoscroller/nanoscroller.css" />
	<link rel="stylesheet" type="text/css"
		href="<?php echo FULL_URL ?>js/jquery.easypiechart/jquery.easy-pie-chart.css" />
	<link rel="stylesheet" type="text/css"
		href="<?php echo FULL_URL ?>js/bootstrap.switch/bootstrap-switch.css" />
	<link rel="stylesheet" type="text/css"
		href="<?php echo FULL_URL ?>js/bootstrap.datetimepicker/css/bootstrap-datetimepicker.min.css" />
	<link rel="stylesheet" type="text/css"
		href="<?php echo FULL_URL ?>js/jquery.select2/select2.css" />
	<link rel="stylesheet" type="text/css"
		href="<?php echo FULL_URL ?>js/bootstrap.slider/css/slider.css" />
	<link rel="stylesheet" type="text/css"
		href="<?php echo FULL_URL ?>js/intro.js/introjs.css" />
	<link rel="stylesheet" type="text/css"
		href="<?php echo FULL_URL ?>js/jquery.magnific-popup/dist/magnific-popup.css" />
	<link rel="stylesheet" type="text/css"
		href="<?php echo FULL_URL ?>js/jquery.datatables/extras/TableTools/media/css/TableTools.css" />
	<!-- Custom styles for this template -->
	<link href="<?php echo FULL_URL ?>css/style.css" rel="stylesheet" />

	<link rel="stylesheet" href="//code.jquery.com/ui/1.11.0/themes/smoothness/jquery-ui.css">

	<script type="text/javascript" src="<?php echo FULL_URL ?>js/jquery.js"></script>
	<script type="text/javascript"
		src="<?php echo FULL_URL ?>js/jquery.gritter/js/jquery.gritter.js"></script>
	<script type="text/javascript" src="<?php echo FULL_URL ?>js/jquery.niftymodals/js/jquery.modalEffects.js"></script>
	<script type="text/javascript"
		src="<?php echo FULL_URL ?>js/jquery.nanoscroller/jquery.nanoscroller.js"></script>
	<script type="text/javascript" src="<?php echo FULL_URL ?>js/behaviour/general.js"></script>
	<script type="text/javascript"
		src="<?php echo FULL_URL ?>js/jquery.sparkline/jquery.sparkline.min.js"></script>
	<script type="text/javascript"
		src="<?php echo FULL_URL ?>js/jquery.easypiechart/jquery.easy-pie-chart.js"></script>
	<script type="text/javascript"
		src="<?php echo FULL_URL ?>js/jquery.nestable/jquery.nestable.js"></script>
		<script type="text/javascript" src="<?php echo FULL_URL ?>js/jquery.niftymodals/js/jquery.modalEffects.js"></script>
	<script type="text/javascript"
		src="<?php echo FULL_URL ?>js/bootstrap.switch/bootstrap-switch.min.js"></script>
	<script type="text/javascript" src="<?php echo FULL_URL ?>js/bootstrap-fileupload.js"></script>
	<script type="text/javascript"
		src="<?php echo FULL_URL ?>js/bootstrap.datetimepicker/js/bootstrap-datetimepicker.min.js"></script>
	<script src="<?php echo FULL_URL ?>js/jquery.select2/select2.min.js"
		type="text/javascript"></script>
	<script src="<?php echo FULL_URL ?>js/skycons/skycons.js"
		type="text/javascript"></script>
	<script
		src="<?php echo FULL_URL ?>js/bootstrap.slider/js/bootstrap-slider.js"
		type="text/javascript"></script>
	<script type="text/javascript" src="<?php echo FULL_URL ?>js/masonry.js"></script>
	<script type="text/javascript"
		src="<?php echo FULL_URL ?>js/jquery.magnific-popup/dist/jquery.magnific-popup.min.js"></script>
	<script src="<?php echo FULL_URL ?>js/intro.js/intro.js"
		type="text/javascript"></script>
	<script type="text/javascript"
		src="<?php echo FULL_URL ?>js/jquery.datatables/media/js/jquery.dataTables.js"></script>

	<script
		src="<?php echo FULL_URL ?>js/jquery.datatables/extras/TableTools/media/js/TableTools.js"
		type="text/javascript"></script>
	<script
		src="<?php echo FULL_URL ?>js/jquery.datatables/extras/TableTools/media/js/ZeroClipboard.js"
		type="text/javascript"></script>
	<script type="text/javascript"
		src="<?php echo FULL_URL ?>js/jquery.datatables/bootstrap-adapter/js/datatables.js"></script>
	<script
		src="https://maps.googleapis.com/maps/api/js?v=3.exp&amp;sensor=false"></script>

	<!-- Bootstrap core JavaScript
	    ================================================== -->

	<script src="http://code.highcharts.com/highcharts.js"></script>
	<script src="http://code.highcharts.com/modules/data.js"></script>
	<script src="http://code.highcharts.com/modules/drilldown.js"></script>
	<script src="http://code.highcharts.com/modules/exporting.js"></script>

	<script src="<?php echo FULL_URL ?>js/behaviour/voice-commands.js"></script>
	<script
		src="<?php echo FULL_URL ?>js/bootstrap/dist/js/bootstrap.min.js"></script>
	<script src="<?php echo FULL_URL ?>js/jquery.ui/jquery-ui.js"
		type="text/javascript"></script>
	<script type="text/javascript"
		src="<?php echo FULL_URL ?>js/jquery.flot/jquery.flot.js"></script>
	<script type="text/javascript"
		src="<?php echo FULL_URL ?>js/jquery.flot/jquery.flot.pie.js"></script>
	<script type="text/javascript"
		src="<?php echo FULL_URL ?>js/jquery.flot/jquery.flot.resize.js"></script>
	<script type="text/javascript"
		src="<?php echo FULL_URL ?>js/jquery.flot/jquery.flot.labels.js"></script>

	<?php echo $this->Html->script('require');?>
	<?php echo $this->element('environment_variables');?>
	<script type="text/javascript">
		require.config({
			baseUrl: FULL_URL+"js",
			paths: {
			},
			waitSeconds: 350,
			packages: []
		});
	</script>

</head>

<body>

	<!-- Fixed navbar -->
	<div id="head-nav" class="navbar navbar-default navbar-fixed-top"
		style="zoom: 1">
		<div class="container-fluid">
			
			<div class="navbar-collapse collapse">
				<ul class="nav navbar-nav">
					<li class="dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown" >
							<i class="fa fa-desktop"></i><span> Opciones del sistema</span><b class="caret"></b>
						</a>

						<ul class="dropdown-menu">
							<li class="" onclick="list(this)">
							<?php echo $this->Html->link('Sincronizar',array('controller' => 'main','action' => 'sincronizar'));?>
							</li>
							<li class="" onclick="list(this)"><?php echo $this->Html->link('Asignar',array('controller' => 'main','action' => 'asignar'));?>
							</li>
							<li class="" onclick="list(this)"><?php echo $this->Html->link('Monitorear',array('controller' => 'main','action' => 'monitorear'));?>
							</li>
							<li class="" onclick="list(this)"><a
								href="<?php echo FULL_URL.'dashboard/mapa_monitoreo';?>"
								target="_blank" onclick="">Ubicacion GPS</a></li>
						</ul>
					</li>					
				</ul>

				<ul class="nav navbar-nav navbar-right user-nav" data-position="bottom" data-step="3" data-intro="<strong>Salir del Sistema</strong> <br/> Cierra tu sesion.">
           
		           <div class="navbar-header" style="">
		            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
		              <span class="fa fa-gear"></span>
		            </button>
		            <a class="navbar-brand" href="#"><span>Pexport</span></a>
		          </div>

		          <li class="" id="" onclick="list(this)" style="background-color:#444;">
		             <a href="<?php echo FULL_URL ?>usuarios/logout"> <i class="fa fa-close"></i>  Cerrar</a>
		          </li>

		        </ul>

			</div>
			<!--/.nav-collapse animate-collapse -->
		</div>
	</div>
	<!-- class="sb-collapsed" -->
	<div id="cl-wrapper" class="fixed-menu sb-collapsed">
		<?php echo $this->element('menu');?>
		<!-- CONTENT-->
		<?php 
		echo $this->fetch('content');
		?>
		<!-- /CONTENT -->
	</div>
	<script type="text/javascript"
		src="<?php echo FULL_URL ?>js/jquery.flot/jquery.flot.categories.min.js"></script>
	<script type="text/javascript"
		src="<?php echo FULL_URL ?>js/behaviour/general_dashboard.js"></script>
	<!-- Placed at the end of the document so the pages load faster -->
	<script type="text/javascript">
      $(document).ready(function(){
        //initialize the javascript
        App.init();
        App.dashBoard();        
        
          //introJs().setOption('showBullets', false).start();

      });
    </script>
    <!-- Nifty Modal -->
	<div class='md-modal md-dark custom-width md-effect-9' id='form-green'
	style='margin-top: -15px; width: 900px; height: 75%;'>
	<div class='md-content'>
		<div class='modal-header'>
			<h3>Visualizador de Formularios</h3>
			<button type='button' class='close md-close' data-dismiss='modal'
				aria-hidden='true'>&times;</button>
		</div>
		<div class='modal-body form'>
			<div id='design'></div>
		</div>
	</div>
</div>
<?php echo $this->element('ga');?>
</body>
</html>


