<?php
App::uses('AppHelper', 'View/Helper');

class FormularioHelper extends AppHelper {
	
	public $helpers = array('Html');

	/**
	 * get Form HTML
	 * @param json $json_form
	 * @author Geynen
	 * @version 13 Febrero 2015
	 */
	public function getForm($json_form) {

		$arr_form = json_decode($json_form);
		
		foreach ($arr_form as $k => $objeto){
			echo $this->getObjeto($objeto);
		}
	}
	
	/**
	 * get Objeto HTML
	 * @param json $objeto
	 * @author Geynen
	 * @version 13 Febrero 2015
	 */
	public function getObjeto($objeto){
			
		/* Etiqueta */
		if(isset($objeto->etiqueta) && $objeto->etiqueta!=''){
			$str_etiqueta = $objeto->etiqueta;
		}else{
			$str_etiqueta = '['.$objeto->nombre.']';
		}
		/* Etiqueta - Fin */
		
		/* Valor por defecto */
		if(isset($objeto->defecto_desde_campo) && $objeto->defecto_desde_campo!=''){
			$str_defecto = $objeto->defecto_desde_campo;
		}else{
			$str_defecto = $objeto->defecto;
		}
		/* Valor por defecto - Fin */
		
		/* Atributos */
		$str_css_hide = '';
		$str_class_expand = 'collapsed';
		$str_class_expand2 = '';
		if(isset($objeto->atributos)){
			foreach ($objeto->atributos as $k => $atributo){
				$str_css_hide = $this->getAtributoVisible($atributo);
				if($atributo->tipo->id==5){//expand
					if($atributo->valor=='true'){
						$str_class_expand = '';
						$str_class_expand2 = 'in';
					}
				}
			}
		}
		/* Atributos - Fin */
		
		$str_indica_campo_servicio = '';
		if(isset($objeto->campo_servicio)){
			$str_indica_campo_servicio = '<br><span style="color:green;">Campo Web Service: <b>'.$objeto->campo_servicio.'<b></span>';
		}
		
		switch ($objeto->tipo_objeto->id){
			case 1:// Grupo
				echo '
				<div class="item-objeto panel-group accordion" data-intervencion_grupo_id="'.$objeto->intervencion_grupo_id.'" data-objeto_id="'.$objeto->id.'" style="'.$str_css_hide.'">
					<div class="panel panel-default">
						<div class="panel-heading">
						  <h4 class="panel-title">
							<a class="'.$str_class_expand.'" data-toggle="collapse" data-parent="#accordion" href="#collapse-'.$objeto->intervencion_grupo_id.'-'.(isset($objeto->intervencion_item_id)?$objeto->intervencion_item_id:0).'">
							  <i class="fa fa-angle-right"></i> '.$str_etiqueta.'
							</a>
						  </h4>
						</div>
						<div id="collapse-'.$objeto->intervencion_grupo_id.'-'.(isset($objeto->intervencion_item_id)?$objeto->intervencion_item_id:0).'" class="panel-collapse collapse '.$str_class_expand2.'">
						  <div class="panel-body">';
							if(isset($objeto->items)){
								foreach ($objeto->items as $k => $objeto){
									echo $this->getObjeto($objeto);
								}
							}
				echo '		</div>
						</div>
					</div>
				</div>		
				';
				break;
			case 2:// Grupo Array
				echo '
				<div class="item-objeto panel-group accordion" data-intervencion_grupo_id="'.$objeto->intervencion_grupo_id.'" data-objeto_id="'.$objeto->id.'" style="'.$str_css_hide.'">
					<div class="panel panel-default">
						<div class="panel-heading">
						  <h4 class="panel-title">
							<a class="'.$str_class_expand.'" data-toggle="collapse" data-parent="#accordion" href="#collapse-'.$objeto->intervencion_grupo_id.'-'.(isset($objeto->intervencion_item_id)?$objeto->intervencion_item_id:0).'">
							  <i class="fa fa-angle-right"></i> '.$str_etiqueta.'
							</a>
						  </h4>
						</div>
						<div id="collapse-'.$objeto->intervencion_grupo_id.'-'.(isset($objeto->intervencion_item_id)?$objeto->intervencion_item_id:0).'" class="panel-collapse collapse '.$str_class_expand2.'">
						  <div class="panel-body">';
							if(isset($objeto->items)){
								foreach ($objeto->items as $k => $objeto){
									echo $this->getObjeto($objeto);
								}
							}
				echo '		</div>
						</div>
					</div>
				</div>		
				';
				break;
			case 3:// Input
				echo '<div class="item-objeto form-group" data-objeto_id="'.$objeto->id.'" style="'.$str_css_hide.'">
              			<label for="inputEmail3" class="col-sm-2 control-label">'.$str_etiqueta.'</label>
              			<div class="col-sm-10">
                			<input type="email" class="form-control" id="'.$objeto->intervencion_item_id.'" placeholder="'.$objeto->placeholder.'" value="'.$str_defecto.'">
                			'.$str_indica_campo_servicio.'
              			</div>';
				if($objeto->defecto_desde_intervencion_item_id){
					echo '<script>
						$("#'.$objeto->defecto_desde_intervencion_item_id.'").change(function(){
                			$("#'.$objeto->intervencion_item_id.'").val($(this).val());
						});
                		</script>'; 
				}
                echo '</div>';
				break;
			case 4:// Label
				echo '<div class="item-objeto form-group" data-objeto_id="'.$objeto->id.'" style="'.$str_css_hide.'">
              			<label for="inputEmail3" class="col-sm-2 control-label">'.$str_etiqueta.'</label>
              			<div class="col-sm-10">
                			<input type="text" class="form-control" id="'.$objeto->intervencion_item_id.'" placeholder="'.$objeto->placeholder.'" value="'.$str_defecto.'" disabled>
                			'.$str_indica_campo_servicio.'
              			</div>
              		</div>';
				break;
			case 5:// Select
				echo '<div class="item-objeto form-group" data-objeto_id="'.$objeto->id.'" style="'.$str_css_hide.'">
              			<label for="inputEmail3" class="col-sm-2 control-label">'.$str_etiqueta.'</label>
              			<div class="col-sm-10">
                			<select class="form-control" id="'.$objeto->intervencion_item_id.'">';
							if(isset($objeto->datos)){
								foreach ($objeto->datos as $k => $dato_opcion){
									$str_selected = '';
									if($str_defecto==$dato_opcion->valor && $dato_opcion->valor!=''){
										$str_selected = 'selected';
									}
									echo '<option value="'.$dato_opcion->valor.'" data-children_objeto_id="'.$dato_opcion->children_objeto_id.'" '.$str_selected.'>'.$dato_opcion->nombre.'</option>';
								}
							}
                	echo '</select>
                		<script>
                		$("#'.$objeto->intervencion_item_id.'").change(function (){
                			$parent_group = $(".item-objeto[data-intervencion_grupo_id='.$objeto->intervencion_grupo_id.']");
							$(".item-objeto[data-objeto_id="+$(this).find(":selected").data("children_objeto_id")+"]",$parent_group).show();
                			$("option:not(:selected)",$(this)).each(function( index ) {
                					$(".item-objeto[data-objeto_id="+$(this).data("children_objeto_id")+"]",$parent_group).hide();
                			});
						});	
                		</script>
                		'.$str_indica_campo_servicio.'
              			</div>
              		</div>';
				break;
			case 6:// Select Externo
				echo '<div class="item-objeto form-group" data-objeto_id="'.$objeto->id.'" style="'.$str_css_hide.'">
              			<label for="inputEmail3" class="col-sm-2 control-label">'.$str_etiqueta.'</label>
              			<div class="col-sm-10">
                			<select class="form-control"></select>
              			</div>
              			'.$str_indica_campo_servicio.'
              		</div>';
				break;
			case 11:// Textarea
				echo '<div class="item-objeto form-group" data-objeto_id="'.$objeto->id.'" style="'.$str_css_hide.'">
              		<label for="inputEmail3" class="col-sm-2 control-label">'.$str_etiqueta.'</label>
              		<div class="col-sm-10">
                		<textarea class="form-control" id="'.$objeto->intervencion_item_id.'" placeholder="'.$objeto->placeholder.'">'.$str_defecto.'</textarea>
            			'.$str_indica_campo_servicio.'
              		</div>';
				if($objeto->defecto_desde_intervencion_item_id){
					echo '<script>
					$("#'.$objeto->defecto_desde_intervencion_item_id.'").change(function(){
						$("#'.$objeto->intervencion_item_id.'").val($(this).val());
					});
                	</script>';
				}
				echo '</div>';
				break;
			default:// Objeto no dise�ado
				echo '<div class="item-objeto form-group" data-objeto_id="'.$objeto->id.'" style="'.$str_css_hide.'">
              		<label for="inputEmail3" class="col-sm-2 control-label">'.$str_etiqueta.'</label>
              		<div class="col-sm-10">
                		<input type="text" class="form-control" id="'.$objeto->intervencion_item_id.'" placeholder="'.$objeto->placeholder.'" value="OBJETO NO DISE&Ntilde;ADO ('.$objeto->tipo_objeto->nombre.')" disabled>
						'.$str_indica_campo_servicio.'
              		</div>
              	</div>';
				break;
		}

	}
	
	/**
	 * get atributo visible
	 * @param objeto $atributo
	 * @author Geynen
	 * @version 23 Febrero 2015
	 */
	public function getAtributoVisible($atributo){
		if($atributo->tipo->id==3){
			if($atributo->valor=='false'){
				return 'display:none;';
			}else{
				return '';
			}
		}
	}
	
	/**
	 * get ComboBox de Objetos de Form
	 * @param ArrayObjeto $arr_obj_objeto
	 * @param integer $intervencion_tipo_id
	 * @author Geynen
	 * @version 18 Febrero 2015
	 */
	public function getComboBoxObjeto($name,$selected_value,$arr_obj_objeto,$intervencion_tipo_id) {
	
		echo '<select id="'.$name.'" name="'.$name.'" class="select2">';
		echo '<option value="">Ninguno</option>';
		foreach ($arr_obj_objeto as $k => $obj_objeto){
			echo $this->getComboBoxItem($obj_objeto,$intervencion_tipo_id,$selected_value);
		}
		echo '</select>';
	}
	
	/**
	 * get Option ComboBox n niveles para Objetos de Form
	 * @param Objeto $obj_objeto
	 * @param integer $intervencion_tipo_id
	 * @param integer $index
	 * @author Geynen
	 * @version 18 Febrero 2015
	 */
	private function getComboBoxItem($obj_objeto,$intervencion_tipo_id,$selected_value,$index=0){

		$espacios = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
		$str_espacios = '';
		for ($i=0;$i<$index;$i++){
			$str_espacios .= $espacios;
		}
		
		if($obj_objeto->TipoObjeto->getAttr('objetos')==true){
			echo '<option disabled>';
			echo $str_espacios.$obj_objeto->getAttr('nombre').' - '.$obj_objeto->TipoObjeto->getAttr('nombre').' - '.$obj_objeto->TipoDato->getAttr('nombre');
			echo '</option>';
			
			$index++;
			foreach ($obj_objeto->getItemsGrupo($intervencion_tipo_id) as $kk => $obj_objeto_item){
				$this->getComboBoxItem($obj_objeto_item,$intervencion_tipo_id,$selected_value,$index);
			}
		}else{
			$str_selected = '';
			if($selected_value==$obj_objeto->getAttr('intervencion_item_id')){
				$str_selected = 'selected';
			}
			echo '<option value="'.$obj_objeto->getAttr('intervencion_item_id').'" '.$str_selected.'>';
			echo $str_espacios.$obj_objeto->getAttr('nombre').' - '.$obj_objeto->TipoObjeto->getAttr('nombre').' - '.$obj_objeto->TipoDato->getAttr('nombre');
			echo '</option>';
		}
	}
}