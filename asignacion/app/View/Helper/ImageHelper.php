<?php
App::uses('AppHelper', 'View/Helper');

class ImageHelper extends AppHelper {
	public $helpers = array('Html');

	public function applyWatermark($image_base64, $fecha) {

	$data = base64_decode($image_base64);
		$str_marca = $fecha;
		$im = imagecreatefromstring($data);
		if ($im !== false) {
			 
			$color_texto = imagecolorallocate($im, 246, 246, 85);
				
			$alto_imagen = imagesy($im)-40;
			$ancho_imagen = imagesx($im)-80;
				
			// Escribir la cadena en la parte superior izquierda
			//imagestring($im, 5, 20, $alto_imagen, $str_marca, $color_texto);
			//imagestring($im, 5, $ancho_imagen, $alto_imagen, $str_marca[1], $color_texto);
			
			$fuente = 'fonts/arial.ttf';
			imagettftext($im, 50, 0, 20, $alto_imagen, $color_texto, $fuente, $str_marca);
			ob_start ();
				
			imagejpeg ($im);
			//imagepng ($im);
			$image_data = ob_get_contents ();
				
			ob_end_clean ();
				
			$image_data_base64 = base64_encode ($image_data);
			echo $image_data_base64;
		}
		else {
			echo 'Ocurrió un error.';
		}
	}
}