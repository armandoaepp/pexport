				<?php echo $this->Form->create('Usuario'); ?>
					<div class="content">
							<h4 class="title">Zona de Acceso</h4>
						
							<div class="form-group">
								<div class="col-sm-12">
									<div class="input-group">
										<span class="input-group-addon"><i class="fa fa-user"></i></span>
										<?php echo $this->Form->input('usuario', array('class' => 'form-control','label' => '','placeholder' => 'Usuario' ));?>
									</div>
								</div>
							</div>
							<div class="form-group">
								<div class="col-sm-12">
									<div class="input-group">
										<span class="input-group-addon"><i class="fa fa-lock"></i></span>
										<?php echo $this->Form->input('pass', array('class' => 'form-control','label' => '','placeholder' => 'Contraseña', 'type' => 'password'));?>
									</div>
								</div>
							</div>

							
					</div>
					<div class="foot">
						<?php echo $this->Form->button('Ingresar', array('class' => 'btn btn-primary')); ?>
					</div>
				


