<div class="page-aside app tree" id="pageContent">
	<div class="fixed nano nscroller has-scrollbar">
		<div class="content" tabindex="0" style="right: -15px;">
			<div class="header">
				<button class="navbar-toggle" data-target=".treeview"
					data-toggle="collapse" type="button">
					<span class="fa fa-chevron-down"></span>
				</button>
				<h2 class="page-title">Web Service por Tipo Intervenci&oacute;n</h2>
				<p class="description">Mantenimiento</p>
			</div>
			<?php echo $this->element('menu_mantenimiento'); ?>
		</div>

		<div class="pane" style="display: block;">
			<div class="slider" style="height: 428px; top: 0px;"></div>
		</div>
	</div>
</div>
<div class="container-fluid" id="pcont">
	<div id="cabeza">
		<div class="page-head" style="padding: 0px 0px;">
			<ol class="breadcrumb">
				<li><button type="button" class="btn btn-default btn-xs"
						onclick="arbol()">
						<i class="fa fa-arrows-h"></i>
					</button></li>
				<li><a href="#">Mantenimiento</a></li>
				<li><a href="#">Web Service por Tipo Intervenci&oacute;n</a></li>
				<li class="active">Nuevo</li>
			</ol>
		</div>
	</div>
	<div class="cl-mcont">

		<div class="row">
			<div class="col-md-12">
				<div class="block-flat">
					<div class="header">
						<h3>Nuevo: Web Service por Tipo Intervenci&oacute;n: <?php echo $obj_intervencion_tipo->getAttr('nombre');?></h3>
					</div>
					<div class="content">

						<form class="form-horizontal group-border-dashed" action="<?php echo FULL_URL;?>intervencion_tipo_web_service/nuevo/<?php echo $obj_intervencion_tipo->getID();?>" method="post" style="border-radius: 0px;">
							<input type="hidden" class="form-control" name="data[IntervencionTipoWebService][intervencion_tipo_id]" id="IntervencionTipoWebServiceObjetoId" value="<?php echo $obj_intervencion_tipo->getID();?>">
						
							<div class="form-group">
								<label class="col-sm-3 control-label">Nombre</label>
								<div class="col-sm-6">
									<input type="text" class="form-control" name="data[IntervencionTipoWebService][nombre]" id="IntervencionTipoWebServiceNombre">
								</div>
							</div>

							<div class="form-group">
								<label class="col-sm-3 control-label">Descripci&oacute;n</label>
								<div class="col-sm-6">
									<input type="text" class="form-control" name="data[IntervencionTipoWebService][descripcion]" id="IntervencionTipoWebServiceDescripcion">
								</div>
							</div>
							
							<div class="form-group">
								<label class="col-sm-3 control-label">Url</label>
								<div class="col-sm-6">
									<input type="text" class="form-control" name="data[IntervencionTipoWebService][url]" id="IntervencionTipoWebServiceUrl">
								</div>
							</div>
							
							<div class="form-group">
								<label class="col-sm-3 control-label">Headers</label>
								<div class="col-sm-6">
									<textarea class="form-control" name="data[IntervencionTipoWebService][headers]" id="IntervencionTipoWebServiceHeader"></textarea>
								</div>
							</div>
							
							<div class="form-group">
								<label class="col-sm-3 control-label">Content</label>
								<div class="col-sm-6">
									<textarea class="form-control" name="data[IntervencionTipoWebService][content]" id="IntervencionTipoWebServiceContent"></textarea>
								</div>
							</div>
							
							<div class="form-group">
								<label class="col-sm-3 control-label">Metodo</label>
								<div class="col-sm-5">
									<select class="form-control" name="data[IntervencionTipoWebService][metodo]" id="IntervencionTipoWebServiceMetodo">
										<option value="GET">GET</option>
										<option value="POST">POST</option>
										<option value="PUT">PUT</option>
									</select>
								</div>
							</div>
							
							<div class="form-group">
								<label class="col-sm-3 control-label">Acci&oacute;n</label>
								<div class="col-sm-5">
									<select class="form-control" name="data[IntervencionTipoWebService][accion]" id="IntervencionTipoWebServiceAccion">
										<option value="SAVE">SAVE</option>
										<option value="LIST">LIST</option>
									</select>
								</div>
							</div>

							<div class="form-group">
								<div class="col-sm-offset-3 col-sm-9">
									<button type="submit" class="btn btn-primary">Grabar</button>
									<a href="<?php echo FULL_URL;?>intervencion_tipo_web_service/index/<?php echo $obj_intervencion_tipo->getID();?>" class="btn btn-default">Cancelar</a>
								</div>
							</div>

						</form>

					</div>
				</div>
			</div>
		</div>
	</div>
</div>