<?php 
global $sortable_one_interior;
global $sortable_two_interior;
?>
<div class="page-aside app tree" id="pageContent">
	<div class="fixed nano nscroller has-scrollbar">
		<div class="content" tabindex="0" style="right: -15px;">
			<div class="header">
				<button class="navbar-toggle" data-target=".treeview"
					data-toggle="collapse" type="button">
					<span class="fa fa-chevron-down"></span>
				</button>
				<h2 class="page-title">Tipo Intervenci&oacute;n</h2>
				<p class="description">Mantenimiento</p>
			</div>
			<?php echo $this->element('menu_mantenimiento'); ?>
		</div>

		<div class="pane" style="display: block;">
			<div class="slider" style="height: 428px; top: 0px;"></div>
		</div>
	</div>
</div>
<div class="container-fluid" id="pcont">
	<div id="cabeza">
		<div class="page-head" style="padding: 0px 0px;">
			<ol class="breadcrumb">
				<li><button type="button" class="btn btn-default btn-xs"
						onclick="arbol()">
						<i class="fa fa-arrows-h"></i>
					</button></li>
				<li><a href="#">Mantenimiento</a></li>
				<li><a href="#">Tipo Intervenci&oacute;n</a></li>
				<li class="active">Listado</li>
			</ol>
		</div>
	</div>
	<div class="cl-mcont">

		<div class="row">
			<div class="col-md-12">

				<div class="tab-container tab-right">
					<ul class="nav nav-tabs flat-tabs">
						<li class="active"><a href="#tab4-1" data-toggle="tab"><i
								class="fa fa-building-o"></i></a></li>
						<li><a href="#tab4-2" data-toggle="tab" class="tab_json_form"><i
								class="fa fa-code"></i></a></li>
						<li><a href="#tab4-3" data-toggle="tab" class="tab_view_form"><i
								class="fa fa-eye"></i></a></li>
					</ul>
					<div class="tab-content">
						<div class="tab-pane active cont fade in" id="tab4-1">
							<h2>Creaci&oacute;n de Formularios</h2>
							<div class="content">
					
							<?php echo $this->Session->flash();?>
						
								<div class="row">
								<div class="col-sm-6 col-md-6">
									<div class="block-flat">
										<div class="header">
											<h3>Lista de Grupos</h3>
										</div>
										<ol id="sortable-one" class="connectedSortable dd-list" sort="1" data-intervencion_tipo="<?php echo $intervencion_tipo_id?>" style="min-height: 120px;">
											<?php
											
											$sortable_one = "";
											foreach ($arr_obj_objeto as $k => $obj_objeto){
												if($obj_objeto->TipoObjeto->getAttr('objetos')){
													$sortable_one .="#sortable".$obj_objeto->getID().", "; 
													cadenaObjetoGruposItems($obj_objeto,$intervencion_tipo_id);
												}
											}?>
										</ol>
									</div>
								</div>
	
								<div class="col-sm-6 col-md-6">
									<div class="block-flat">
										<div class="header">
											<h3>Formulario</h3>
										</div>
										<ol id="sortable-two" class="connectedSortable dd-list" sort="2" data-intervencion_tipo="<?php echo $intervencion_tipo_id?>" style="min-height: 120px;">
											<?php
											$sortable_two = "";
											$arr_og = array();
											foreach ($arr_obj_objeto_save as $k => $obj_objeto_save){
													$sortable_two .="#sortableTwo".$obj_objeto_save->getID().", ";
													cadenaObjetoGruposItems($obj_objeto_save,$intervencion_tipo_id);
												}?>
										</ol>
									</div>
								</div>
							</div>
						</div>
						</div>
						
						<div class="tab-pane fade cont" id="tab4-2">
							<h2>Json Form</h2>
							<div class="content">
							<iframe id="iframe_json_form" src="" width="100%" height="1000px" frameborder="0" scrolling="si"></iframe>
							</div>
						</div>
						
						<div class="tab-pane fade cont" id="tab4-3">
							<h2>Vista Previa del Formulario</h2>
							<div class="content">
							<iframe id="iframe_view_form" src="" width="100%" height="1000px" frameborder="0" scrolling="si"></iframe>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<?php
function cadenaObjetoGruposItems($obj_objeto,$intervencion_tipo_id,$parent = null,$agregar_item = null,$obj_objeto_grupo = null){
global $sortable_one_interior;
global $sortable_two_interior;
?>
<li class="dd-item items_grupos_accordion <?php if(isset($agregar_item)){ echo "lirojo";}?>" data-intervencion_item_id="<?php echo $obj_objeto->getAttr('intervencion_item_id');?>">
	<button class="btn-openclose btn-collapse" data-action="collapse" type="button">Collapse</button>
	<button class="btn-openclose btn-expand" data-action="expand" type="button" style="display: none;">Expand</button>
	<div class="dd-handle <?php if(!isset($parent)){echo "items_grupos_parent";}?>" data-grupo_objeto="<?php echo $obj_objeto->getID();?>" 
	data-intervencion_grupo_id="<?php if(!isset($parent)){echo $obj_objeto->getAttr('intervencion_grupo_id');}?>">
		<?php echo $obj_objeto->getAttr('nombre');?> - 
		<?php echo $obj_objeto->TipoObjeto->getAttr('nombre');?> - 
		<?php echo $obj_objeto->TipoDato->getAttr('nombre');?>
		<?php if(!isset($agregar_item) && $obj_objeto->getAttr('intervencion_item_id')){?>
		<a class="btn_quitar_item_grupo pull-right" objeto="grupo" data-intervencion_item_id="<?php echo $obj_objeto->getAttr('intervencion_item_id');?>" href="javascript: void(0);">Quitar</a>
		<?php }?>
		<?php if(isset($agregar_item)){ ?><a class="btn_agregar_item_grupo pull-right" objeto="grupo" data-objeto_grupo="<?php echo $obj_objeto_grupo->getID();?>" data-objeto="<?php echo $obj_objeto->getID();?>" href="javascript: void(0);">Agregar</a><?php } ?>
	</div>
	<ol class="dd-list" id="sortable<?php if(isset($parent) || $obj_objeto->getAttr('intervencion_grupo_id')){echo "Two".$obj_objeto->getID();}else{echo $obj_objeto->getID();}?>">
		<?php
		if(isset($parent)){
			$sortable_two_interior .="#sortableTwo".$obj_objeto->getID().", ";
		}else{
			$sortable_one_interior .="#sortable".$obj_objeto->getID().", ";
		}
		$arr_obj_item_id = array();
		$arr_obj_grupo_id = array();
		if($obj_objeto->getAttr('intervencion_grupo_id')){
			$arr_obj_items = $obj_objeto->getItemsGrupo($intervencion_tipo_id);
			foreach ($arr_obj_items as $kk => $obj_objeto_item){
				//debug($obj_objeto_item->data);
				$arr_obj_item_id[] = $obj_objeto_item->getID();
				if($obj_objeto_item->TipoObjeto->getAttr('objetos')){
					cadenaObjetoGruposItems($obj_objeto_item, $intervencion_tipo_id,1);
			  	}else{?>
				<li class="dd-handle items_grupo" data-objeto="<?php echo $obj_objeto_item->getID();?>" data-intervencion_item_id="<?php echo $obj_objeto_item->getAttr('intervencion_item_id');?>">
					<?php echo $obj_objeto_item->getAttr('nombre');?> - 
					<?php echo $obj_objeto_item->TipoObjeto->getAttr('nombre');?> - 
					<?php echo $obj_objeto_item->TipoDato->getAttr('nombre');?>
					<a class="btn_quitar_item_grupo pull-right" objeto="item" data-intervencion_item_id="<?php echo $obj_objeto_item->getAttr('intervencion_item_id');?>" href="javascript: void(0);">Quitar</a>
					<a class="btn_config_item pull-right" data-intervencion_item_id="<?php echo $obj_objeto_item->getAttr('intervencion_item_id');?>" href="javascript: void(0);">Config. |</a>
				</li>
			<?php }
	 		}
	 		//debug($arr_obj_item_id);
	 		//agregar items faltantes
	 		foreach ($obj_objeto->ObjetoGrupo as $kk => $obj_objeto_grupo){
				//$arr_obj_grupo_id[] = $obj_objeto_grupo->Objeto->getID();
				if(!in_array($obj_objeto_grupo->Objeto->getID(),$arr_obj_item_id)){ //y le preguntamos: esta el valor en el que estamos posicionados actualmente, en el array 2?
					//echo "no esta .." . $obj_objeto_grupo->Objeto->getID();
					if($obj_objeto_grupo->Objeto->TipoObjeto->getAttr('objetos')){
						cadenaObjetoGruposItems($obj_objeto_grupo->Objeto, $intervencion_tipo_id,1,1,$obj_objeto_grupo);
					}else{?>
						<li class="dd-handle items_grupo lirojo" data-objeto="<?php echo $obj_objeto_grupo->Objeto->getID();?>">
							<?php echo $obj_objeto_grupo->Objeto->getAttr('nombre');?> -
							<?php echo $obj_objeto_grupo->Objeto->TipoObjeto->getAttr('nombre');?> - 
							<?php echo $obj_objeto_grupo->Objeto->TipoDato->getAttr('nombre');?>
							<a class="btn_agregar_item_grupo pull-right" data-intervencion_item_id="" objeto="item" data-objeto="<?php echo $obj_objeto_grupo->getID();?>" href="javascript: void(0);">Agregar</a>
						</li>
					<?php
					}
				}
			}
		}else{
			$arr_obj_items = $obj_objeto->ObjetoGrupo;
			foreach ($arr_obj_items as $kk => $obj_objeto_grupo){
				$obj_objeto = $obj_objeto_grupo->Objeto;
				if($obj_objeto->TipoObjeto->getAttr('objetos')){
					cadenaObjetoGruposItems($obj_objeto, $intervencion_tipo_id);
				}else{?>
					<li class="dd-handle items_grupo" data-objeto="<?php echo $obj_objeto->getID();?>">
						<?php echo $obj_objeto->getAttr('nombre');?> - 
						<?php echo $obj_objeto->TipoObjeto->getAttr('nombre');?> - 
						<?php echo $obj_objeto->TipoDato->getAttr('nombre');?>
					</li>
			<?php }
		 	}
	 	}
		?>
	</ol>
<li>
<?php	
}
?>
<script>
  $(function() {
	$("<?php echo substr($sortable_one, 0, -2); ?><?php if($sortable_one_interior !=''){echo ", ".substr($sortable_one_interior, 0, -2); }?>").sortable();
	$("<?php echo substr($sortable_one, 0, -2); ?><?php if($sortable_one_interior !=''){echo ", ".substr($sortable_one_interior, 0, -2); }?>").disableSelection();

	$("<?php echo substr($sortable_two, 0, -2); ?><?php if($sortable_two_interior !=''){echo ", ".substr($sortable_two_interior, 0, -2); }?>").sortable({
		connectWith: ".connectedSortable",
		tolerance: "pointer",
		cursor:"move",
		opacity: .8,
		update: function(event, ui) {
			var arr_intervencion_item_id = [];
			$('>ol>li', $(this).parent('.items_grupos_accordion')).each(function( index ) {
				if($(this).data('intervencion_item_id') != null && $(this).data('intervencion_item_id')!=''){
					arr_intervencion_item_id.push($(this).data('intervencion_item_id'));
				}
			});
			console.log(arr_intervencion_item_id);
			$.ajax({
				url : '<?php echo FULL_URL;?>intervencion_grupo/ajax_cambiar_orden_item/'+JSON.stringify(arr_intervencion_item_id),
				dataType: 'json',
				type: "POST",
				success:function(data, textStatus, jqXHR){
					console.log(data);
					mensajeNotificacion('Se actualizo el orden correctamente','primary');
					if(data.success==false){
						alert(data.msg);
					}
				}
			});
		},
	});
	$("<?php echo substr($sortable_two, 0, -2); ?><?php if($sortable_two_interior !=''){echo ", ".substr($sortable_two_interior, 0, -2); }?>").disableSelection();

	$( "#sortable-one, #sortable-two" ).sortable({
		connectWith: ".connectedSortable",
		tolerance: "pointer",
		cursor:"move",
		opacity: .8,
		start: function(event, ui) {
			$item = ui.item;
			start = $item.parents('.connectedSortable').attr('sort');
		},
		stop: function(event, ui) {
			$item = ui.item;
			end = $item.parents('.connectedSortable').attr('sort')
			
			var intervencion_tipo_id = $item.parents('.connectedSortable').data('intervencion_tipo');
			//alert("start:"+start+",end:"+end);
			if(start != end){
				if(start==1 && end==2){
					//alert('dropable agregar');
					grupo_objeto_id = $item.find('.items_grupos_parent').data('grupo_objeto');// encuentra el primero
					console.log(grupo_objeto_id);
					$.ajax({
						url : '<?php echo FULL_URL;?>intervencion_grupo/ajax_guardar/'+intervencion_tipo_id+'/'+grupo_objeto_id,
						dataType: 'json',
						type: "POST",
						success:function(data, textStatus, jqXHR){
							console.log(data);
							$item.find('.items_grupos_parent').data("intervencion_grupo_id",parseInt(data.intervencion_grupo_id));
							actualizarOrdenGrupos();
							mensajeNotificacion('Se guardo en el formulario correctamente','primary');
							if(data.success==false){
								alert(data.msg);
							}
						}
					});
					
				}else{
					//alert('dropable eliminar');
					intervencion_grupo_id = $item.find('.items_grupos_parent').data('intervencion_grupo_id');// encuentra el primero
					console.log(intervencion_grupo_id);
					$.ajax({
						url : '<?php echo FULL_URL;?>intervencion_grupo/ajax_eliminar/'+intervencion_tipo_id+'/'+intervencion_grupo_id,
						dataType: 'json',
						type: "POST",
						success:function(data, textStatus, jqXHR){
							console.log(data);
							mensajeNotificacion('Se eliminio del formulario correctamente','danger');
							if(data.success==false){
								alert(data.msg);
							}
						}
					});
				}
			}else{
				if(start==2 && end==2){
					//alert('sortable');
					actualizarOrdenGrupos();
				}
			}
      	}
    }).disableSelection();

	function actualizarOrdenGrupos(){
		var arr_intervencion_grupo_id = [];
		var intervencion_tipo_id = $('#sortable-two').data('intervencion_tipo');
		$( ".items_grupos_parent" , $('#sortable-two')).each(function( index ) {
			if($(this).data('intervencion_grupo_id') != null && $(this).data('intervencion_grupo_id')!=''){
				arr_intervencion_grupo_id.push($(this).data('intervencion_grupo_id'));
			}
		});
		console.log(arr_intervencion_grupo_id);
		$.ajax({
			url : '<?php echo FULL_URL;?>intervencion_grupo/ajax_cambiar_orden/'+intervencion_tipo_id+'/'+JSON.stringify(arr_intervencion_grupo_id),
			dataType: 'json',
			type: "POST",
			success:function(data, textStatus, jqXHR){
				console.log(data);
				mensajeNotificacion('Se actualizo el orden correctamente','primary');
				if(data.success==false){
					alert(data.msg);
				}
			}
		});
	}

	function enabledBtnAgregarItemGrupo(){
    	//$('.btn_agregar_item_grupo').off('click');
	    $('.btn_agregar_item_grupo').on('click',function(){
		    var intervencion_tipo_id = $('#sortable-two').data('intervencion_tipo');
		   	var intervencion_grupo_id = $(this).parents('.items_grupos_accordion').find('.items_grupos_parent').data('intervencion_grupo_id');// encuentra el primero
	    	var objeto_id = $(this).data('objeto');
	    	var objeto_grupo_id = $(this).data('objeto_grupo');
	    	var attr = $(this).attr('objeto');
	    	var $item = $(this);
	    	console.log(intervencion_grupo_id);

	    	$.ajax({
				url : '<?php echo FULL_URL;?>intervencion_grupo/agregar_item/'+intervencion_tipo_id+'/'+intervencion_grupo_id+'/'+objeto_id+'/'+objeto_grupo_id+'/'+attr,
				dataType: 'json',
				type: "POST",
				success:function(data, textStatus, jqXHR){
					console.log(data);
					if(attr=="grupo"){
						$item.parents('li.items_grupos_accordion').removeClass('lirojo');
					}else{
						$item.parent('li').removeClass('lirojo');
						$item.parent('li').removeClass('lirojo');
						/*$item.parent('li').append('<a class="btn_config_item pull-right">Config.</a>');
						$item.parent('li').children('.btn_config_item').attr('href','javascript: void(0);');
						$item.parent('li').children('.btn_config_item').attr('data-intervencion_item_id',data.intervencion_item_id);*/
					}
					$item.remove();
					mensajeNotificacion('Se agrego al formulario correctamente','primary');
					if(data.success==false){
						alert(data.msg);
					}
				}
	  	    });
	    });
    }
	enabledBtnAgregarItemGrupo();

	function enabledBtnQuitarItemGrupo(){
    	$('.btn_quitar_item_grupo').off('click');
	    $('.btn_quitar_item_grupo').on('click',function(){
	    	var intervencion_tipo_id = $('#sortable-two').data('intervencion_tipo');
	    	var objeto = $(this).attr('objeto');
	    	var intervencion_item_id = $(this).data('intervencion_item_id');
	    	var $item = $(this);
	    	console.log(intervencion_item_id);
	    	console.log(objeto);

	    	$.ajax({
				url : '<?php echo FULL_URL;?>intervencion_grupo/ajax_quitar/'+intervencion_tipo_id+'/'+intervencion_item_id+'/'+objeto,
				dataType: 'json',
				type: "POST",
				success:function(data, textStatus, jqXHR){
					console.log(data);
					if(objeto == 'grupo'){
						$item.parent('div').parent('li.items_grupos_accordion').remove();
			    	}else{
			    		$item.parent('li.items_grupo').remove();
			    	}
					$item.remove();
					mensajeNotificacion('Se retiro del formulario correctamente','danger');
					if(data.success==false){
						alert(data.msg);
					}
				}
	  	    });
	    });
    }
	enabledBtnQuitarItemGrupo();

    $('.btn-openclose').click(function (){
    	if($(this).data('action')=='collapse'){
	    	$('.dd-list', $(this).parent('.items_grupos_accordion')).hide();
	    	$('.btn-collapse', $(this).parent('.items_grupos_accordion')).hide();
	    	$('.btn-expand', $(this).parent('.items_grupos_accordion')).show();
    	}else{
	    	$('.dd-list', $(this).parent('.items_grupos_accordion')).show();
	    	$('.btn-collapse', $(this).parent('.items_grupos_accordion')).show();
	    	$('.btn-expand', $(this).parent('.items_grupos_accordion')).hide();
    	}
    });

    $('.tab_json_form').click(function (){
    	$('#iframe_json_form').attr('src','<?php echo FULL_URL;?>intervencion_grupo/formulario_json/<?php echo $intervencion_tipo_id;?>');
    });

	$('.tab_view_form').click(function (){
    	$('#iframe_view_form').attr('src','<?php echo FULL_URL;?>intervencion_grupo/formulario_preview/<?php echo $intervencion_tipo_id;?>');
	});
  });
</script>

<div id="dialog-config-item" title="Configuraci&oacute;n" style="display: none;">
	<span class="dialog_loading" style="display: none;"><img
		src="<?php echo FULL_URL ?>/js/jquery.select2/select2-spinner.gif">
		Cargando...</span>
	<div id='dialog-contenido'>
	</div>
</div>

<script>
//$('.btn_config_item').off('click');
$('.btn_config_item').on('click',function(){
	console.log($(this).data('intervencion_item_id'));
	$('.dialog_loading').show();
	$("#dialog-config-item").dialog({ minWidth: 500});
	$.ajax({
        url : "<?php echo FULL_URL;?>intervencion_grupo/ajax_config_item/<?php echo $intervencion_tipo_id;?>/"+$(this).data('intervencion_item_id'),
		type : "POST",
		dataType : 'html',
		success : function(data) {
			//console.log(data);
			$('#dialog-contenido').html(data);
			$("#cbo_defecto_desde_intervencion_item_id").select2({
            	width: '100%'
 	       	});
			$("#cbo_campo_servicio").select2({
            	width: '100%'
 	       	});
			//$('.form_cronograma_ciclo_loading').hide();
		}
	});
	$('.dialog_loading').hide();
})
</script>
<style>
.lirojo .btn_agregar_item_grupo {
	color: red !important;
}
</style>