<form class="form-horizontal group-border-dashed" style="border-radius: 0px;">

	<div class="form-group">
		<label class="col-sm-3 control-label">Valor por defecto</label>
		<div class="col-sm-9">
			<input type="text" class="form-control"
				id="intervencion_item_defecto"
				value="<?php echo $obj_intervencion_item->getAttr('defecto');?>">
		</div>
	</div>

	<div class="form-group">
		<label class="col-sm-3 control-label">Copiar valor desde</label>
		<div class="col-sm-9">
			<?php 
			$this->Formulario->getComboBoxObjeto('cbo_defecto_desde_intervencion_item_id',$obj_intervencion_item->getAttr('defecto_desde_intervencion_item_id'),$arr_obj_objeto_save,$intervencion_tipo_id);
			?>
		</div>
	</div>
	
	<div class="form-group">
		<label class="col-sm-3 control-label">Campo Web Servicio</label>
		<div class="col-sm-9">
			<?php 
			$arr_content_json = json_decode($obj_intervencion_tipo_web_service->getAttr('content'));
			?>
			<select id="cbo_campo_servicio" name="cbo_campo_servicio" class="select2">
			<option value="0">Ninguno</option>
			<?php 
			foreach ($arr_content_json as $k => $data){
				?>
				<option value="<?php echo $k;?>" <?php if($obj_intervencion_item->getAttr('campo_servicio')==$k){echo 'selected';}?>><?php echo $k;?></option>
				<?php 
			}
			?>
			</select>
		</div>
	</div>

	<div class="form-group">
		<div class="col-sm-offset-3 col-sm-9">
			<button type="button" class='btn btn-primary' id='guardar-config-item'>Guardar</button>
         	<button type="button" class='btn' id='cancelar-config-item' onclick="javascript: $('#dialog-config-item').dialog('close');">Cancelar</button>
		</div>
	</div>

</form>
<script>
$('#guardar-config-item').click(function(){
	$.ajax({
        url : "<?php echo FULL_URL;?>intervencion_grupo/ajax_config_item_save/<?php echo $intervencion_tipo_id;?>/<?php echo $obj_intervencion_item->getID();?>",
		type : "POST",
		data: {
			'defecto':$('#intervencion_item_defecto').val(),
			'defecto_desde_intervencion_item_id':$('#cbo_defecto_desde_intervencion_item_id').val(),
			'campo_servicio':$('#cbo_campo_servicio').val()
			},
		dataType : 'json',
		success : function(data) {
			console.log(data);
			if(data.success==true){
				$('#dialog-config-item').dialog('close');
			}else{
				alert(data.msg);
			}
		}
	});
});
</script>