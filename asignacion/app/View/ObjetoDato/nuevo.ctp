<div class="page-aside app tree" id="pageContent">
	<div class="fixed nano nscroller has-scrollbar">
		<div class="content" tabindex="0" style="right: -15px;">
			<div class="header">
				<button class="navbar-toggle" data-target=".treeview"
					data-toggle="collapse" type="button">
					<span class="fa fa-chevron-down"></span>
				</button>
				<h2 class="page-title">Objeto Dato</h2>
				<p class="description">Mantenimiento</p>
			</div>
			<?php echo $this->element('menu_mantenimiento'); ?>
		</div>

		<div class="pane" style="display: block;">
			<div class="slider" style="height: 428px; top: 0px;"></div>
		</div>
	</div>
</div>
<div class="container-fluid" id="pcont">
	<div id="cabeza">
		<div class="page-head" style="padding: 0px 0px;">
			<ol class="breadcrumb">
				<li><button type="button" class="btn btn-default btn-xs"
						onclick="arbol()">
						<i class="fa fa-arrows-h"></i>
					</button></li>
				<li><a href="#">Mantenimiento</a></li>
				<li><a href="#">Objeto Dato</a></li>
				<li class="active">Nuevo</li>
			</ol>
		</div>
	</div>
	<div class="cl-mcont">

		<div class="row">
			<div class="col-md-12">
				<div class="block-flat">
					<div class="header">
						<h3>Nuevo: Dato de Objeto: <?php echo $obj_objeto->getAttr('nombre');?></h3>
					</div>
					<div class="content">

						<form class="form-horizontal group-border-dashed" action="<?php echo FULL_URL;?>objeto_dato/nuevo/<?php echo $obj_objeto->getID();?>" method="post" style="border-radius: 0px;">
							<input type="hidden" class="form-control" name="data[ObjetoDato][objeto_id]" id="ObjetoDatoObjetoId" value="<?php echo $obj_objeto->getID();?>">
						
							<div class="form-group">
								<label class="col-sm-3 control-label">Nombre</label>
								<div class="col-sm-6">
									<input type="text" class="form-control" name="data[ObjetoDato][nombre]" id="ObjetoDatoNombre">
								</div>
							</div>

							<div class="form-group">
								<label class="col-sm-3 control-label">Descripci&oacute;n</label>
								<div class="col-sm-6">
									<input type="text" class="form-control" name="data[ObjetoDato][descripcion]" id="ObjetoDatoDescripcion">
								</div>
							</div>
							
							<div class="form-group">
								<label class="col-sm-3 control-label">Valor</label>
								<div class="col-sm-6">
									<input type="text" class="form-control" name="data[ObjetoDato][valor]" id="ObjetoDatoValor">
								</div>
							</div>
							
							<div class="form-group">
								<label class="col-sm-3 control-label">Orden</label>
								<div class="col-sm-6">
									<input type="text" class="form-control" name="data[ObjetoDato][orden]" id="ObjetoDatoOrden">
								</div>
							</div>							
							
							<div class="form-group">
								<label class="col-sm-3 control-label">Abre Objeto</label>
								<div class="col-sm-5">
									<select class="form-control" name="data[ObjetoDato][children_objeto_id]" id="ObjetoDatoChildrenObjetoId">
										<option value="">Ninguno</option>
										<?php foreach ($arr_obj_objeto as $k => $obj_objeto1){?>
										<option value="<?php echo $obj_objeto1->getID();?>"><?php echo $obj_objeto1->getAttr('nombre');?> - <?php echo $obj_objeto1->TipoObjeto->getAttr('nombre');?> - <?php echo $obj_objeto1->TipoDato->getAttr('nombre');?></option>
										<?php }?>
									</select>
								</div>
							</div>

							<div class="form-group">
								<div class="col-sm-offset-3 col-sm-9">
									<button type="submit" class="btn btn-primary">Grabar</button>
									<a href="<?php echo FULL_URL;?>objeto_dato/index/<?php echo $obj_objeto->getID();?>" class="btn btn-default">Cancelar</a>
								</div>
							</div>

						</form>

					</div>
				</div>
			</div>
		</div>
	</div>
</div>