<div class="page-aside app tree" id="pageContent">
	<div class="fixed nano nscroller has-scrollbar">
		<div class="content" tabindex="0" style="right: -15px;">
			<div class="header">
				<button class="navbar-toggle" data-target=".treeview"
					data-toggle="collapse" type="button">
					<span class="fa fa-chevron-down"></span>
				</button>
				<h2 class="page-title">Grupos</h2>
				<p class="description">Administrador</p>
			</div>
			<?php echo $this->element('menu_mantenimiento'); ?>
		</div>

		<div class="pane" style="display: block;">
			<div class="slider" style="height: 428px; top: 0px;"></div>
		</div>
	</div>
</div>
<div class="container-fluid" id="pcont">
	<div id="cabeza">
		<div class="page-head" style="padding: 0px 0px;">
			<ol class="breadcrumb">
				<li><button type="button" class="btn btn-default btn-xs"
						onclick="arbol()">
						<i class="fa fa-arrows-h"></i>
					</button></li>
				<li><a href="#">Administrador</a></li>
				<li><a href="#">Grupos</a></li>
				<li class="active">Vista Gr&aacute;fica</li>
			</ol>
		</div>
	</div>
	<div class="cl-mcont">

		<div class="row">
			<div class="col-md-12">
				<div class="block-flat">
					<div class="header">
						<h3>Administrador de Grupos</h3>
					</div>
					<div class="content">
					
						<?php echo $this->Session->flash(); ?>
					
						<div class="row">
							<div class="col-sm-6 col-md-6">
								<div class="block-flat" style="padding-top: 0px;">
									<div class="header">
										<h3>Objetos</h3>
									</div>
									<div class="content">
										
										<input type="text" class="form-control" id="search_objetos" placeholder="Escriba aqu&iacute; para buscar...">
										
										<ul class="lista_objetos dd-list">
										<?php 
										foreach ($arr_obj_objeto as $k => $obj_objeto){
											?>
											<li class="dd-item">
											<div class="item_grupo dd-handle" data-id="<?php echo $obj_objeto->getID();?>">
											<span style="display: none;"><?php echo strtolower($obj_objeto->getAttr('nombre'));?> - <?php echo strtolower($obj_objeto->TipoObjeto->getAttr('nombre'));?> - <?php echo strtolower($obj_objeto->TipoDato->getAttr('nombre'));?></span>
											<?php echo $obj_objeto->getAttr('nombre');?> - <?php echo $obj_objeto->TipoObjeto->getAttr('nombre');?> - <?php echo $obj_objeto->TipoDato->getAttr('nombre');?>
											<a class="btn_remove_item_grupo pull-right" style="display: none;" href="javascript: void(0);">Quitar</a>
											</div>
											</li>
											<?php 										
										}?>
										</ul>
										
									</div>
								</div>
							</div>

							<div class="col-sm-6 col-md-6">
								<div class="block-flat" style="padding-top: 0px;">
									<div class="header">
										<h3>Grupos</h3>
									</div>
									<div class="content">
									
										<input type="text" class="form-control" id="search_grupos" placeholder="Escriba aqu&iacute; para buscar...">
										
										<ul class="lista_grupos dd-list">
										<?php 
										foreach ($arr_obj_objeto as $k => $obj_objeto){
											if($obj_objeto->TipoObjeto->getAttr('objetos')){
											?>
											<li class="items_grupos dd-item">
												<button class="btn-openclose btn-collapse" data-action="collapse" type="button">Collapse</button>
												<button class="btn-openclose btn-expand" data-action="expand" type="button" style="display: none;">Expand</button>
												<div class="dd-handle">
												<span style="display: none;"><?php echo strtolower($obj_objeto->getAttr('nombre'));?> - <?php echo strtolower($obj_objeto->TipoObjeto->getAttr('nombre'));?> - <?php echo strtolower($obj_objeto->TipoDato->getAttr('nombre'));?></span>
												<?php echo $obj_objeto->getAttr('nombre');?> - <?php echo $obj_objeto->TipoObjeto->getAttr('nombre');?> - <?php echo $obj_objeto->TipoDato->getAttr('nombre');?>
												</div>
												<ol class="dd-list" data-grupo_id="<?php echo $obj_objeto->getID();?>">
												<?php 
												foreach ($obj_objeto->ObjetoGrupo as $kk => $obj_objeto_grupo){
													?>
													<li class="dd-item li_item_grupo">
													<div class="item_grupo dd-handle" data-id="<?php echo $obj_objeto_grupo->Objeto->getID();?>">
													<?php echo $obj_objeto_grupo->Objeto->getAttr('nombre');?> - <?php echo $obj_objeto_grupo->Objeto->TipoObjeto->getAttr('nombre');?> - <?php echo $obj_objeto_grupo->Objeto->TipoDato->getAttr('nombre');?>
													<a class="btn_remove_item_grupo pull-right" href="javascript: void(0);">Quitar</a>
													</div>
													</li>
													<?php 										
												}?>
												<li class="dd-item dd-handle placeholder" style="border-style: dashed;border-width: 2px;">Arrastra un Objeto aqu&iacute;</li>
												</ol>
											</li>
											<?php 										
											}
										}?>
										</ul>
										
									</div>
								</div>
							</div>
						</div>

					</div>

				</div>
			</div>
		</div>
	</div>
</div>
<script>
  $(function() {
    $( ".lista_objetos li" ).draggable({
      appendTo: "body",
      helper: "clone"
    });
    $( ".items_grupos ol" ).droppable({
      activeClass: "ui-state-default",
      hoverClass: "ui-state-hover",
      accept: ":not(.ui-sortable-helper)",
      drop: function( event, ui ) {
          var objeto_grupo_id = $(this).data('grupo_id');
          var objeto_id = $(ui.draggable.html()).data('id');

        var existe_objeto = false;
        $( "li" , this).each(function( index ) {
        	 if($( '.item_grupo', this ).data('id') == objeto_id){
        		 existe_objeto = true;
        	 }
        });

        if(existe_objeto == false){
        	$( this ).find( ".placeholder" ).remove();
        	$( "<li class='dd-item li_item_grupo'></li>" ).html( ui.draggable.html() ).appendTo( this );
        	$('.btn_remove_item_grupo', $( this )).show();
        	enabledBtnRemoveItemGrupo();

        	$.ajax({
				url : '<?php echo FULL_URL;?>objeto_grupo/ajax_guardar/'+objeto_grupo_id+'/'+objeto_id,
				dataType: 'json',
				type: "POST",
				success:function(data, textStatus, jqXHR){
					console.log(data);
					if(data.success==false){
						alert(data.msg);
					}
				}
        	});
        }
      }
    }).sortable({
      items: "li:not(.placeholder)",
      sort: function() {
        // gets added unintentionally by droppable interacting with sortable
        // using connectWithSortable fixes this, but doesn't allow you to customize active/hoverClass options
        $( this ).removeClass( "ui-state-default" );
      },
      update: function( event, ui ) {
    	  var objeto_grupo_id = $(this).data('grupo_id');
    	  var arr_objeto_id = [];
          $( "li" , this).each(function( index ) {
              if($( '.item_grupo', this ).data('id') != null){
            	  arr_objeto_id.push($( '.item_grupo', this ).data('id'));
              }
         	});
          console.log(arr_objeto_id);

          $.ajax({
				url : '<?php echo FULL_URL;?>objeto_grupo/ajax_cambiar_orden/'+objeto_grupo_id+'/'+JSON.stringify(arr_objeto_id),
				dataType: 'json',
				type: "POST",
				success:function(data, textStatus, jqXHR){
					console.log(data);
					if(data.success==false){
						alert(data.msg);
					}
				}
      	  });
      }
    });

    $('.btn-openclose').click(function (){
		if($(this).data('action')=='collapse'){
			$('.dd-list', $(this).parents('.items_grupos')).hide();
			$('.btn-collapse', $(this).parents('.items_grupos')).hide();
			$('.btn-expand', $(this).parents('.items_grupos')).show();
		}else{
			$('.dd-list', $(this).parents('.items_grupos')).show();
			$('.btn-collapse', $(this).parents('.items_grupos')).show();
			$('.btn-expand', $(this).parents('.items_grupos')).hide();
		}
    });

    function enabledBtnRemoveItemGrupo(){
    	$('.btn_remove_item_grupo').off('click');
	    $('.btn_remove_item_grupo').on('click',function(){
	        var target_parent = $(this).parents('.li_item_grupo');
	    	var objeto_grupo_id = $(this).parents('.dd-list').data('grupo_id');
	    	var objeto_id = $(this).parents('.item_grupo').data('id');
	
	    	$.ajax({
				url : '<?php echo FULL_URL;?>objeto_grupo/ajax_eliminar/'+objeto_grupo_id+'/'+objeto_id,
				dataType: 'json',
				type: "POST",
				success:function(data, textStatus, jqXHR){
					console.log(data);
					if(data.success==false){
						alert(data.msg);
					}else{
						target_parent.remove();
					}
				}
	  	    });
	    });
    }
    enabledBtnRemoveItemGrupo();

    $("#search_objetos").keyup(function(e) {
		var filter = $(this).val().toLowerCase();
		console.log(filter);
		if(filter){
			$(".lista_objetos .dd-item").find("span:not(:contains(" + filter + "))").parents('.dd-item').hide();
			$(".lista_objetos .dd-item").find("span:contains(" + filter + ")").parents('.dd-item').show();
		}else{
			$(".lista_objetos .dd-item").show();
		}
	});

    $("#search_grupos").keyup(function(e) {
		var filter = $(this).val().toLowerCase();
		console.log(filter);
		if(filter){
			$(".lista_grupos .dd-item").find("span:not(:contains(" + filter + "))").parents('.dd-item').hide();
			$(".lista_grupos .dd-item").find("span:contains(" + filter + ")").parents('.dd-item').show();
		}else{
			$(".lista_grupos .dd-item").show();
		}
	});
  });
  </script>