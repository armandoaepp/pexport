<div class="page-aside app tree" id="pageContent">
	<div class="fixed nano nscroller has-scrollbar">
		<div class="content" tabindex="0" style="right: -15px;">
			<div class="header">
				<button class="navbar-toggle" data-target=".treeview"
					data-toggle="collapse" type="button">
					<span class="fa fa-chevron-down"></span>
				</button>
				<h2 class="page-title">Objeto Grupo</h2>
				<p class="description">Mantenimiento</p>
			</div>
			<?php echo $this->element('menu_mantenimiento'); ?>
		</div>

		<div class="pane" style="display: block;">
			<div class="slider" style="height: 428px; top: 0px;"></div>
		</div>
	</div>
</div>
<div class="container-fluid" id="pcont">
	<div id="cabeza">
		<div class="page-head" style="padding: 0px 0px;">
			<ol class="breadcrumb">
				<li><button type="button" class="btn btn-default btn-xs"
						onclick="arbol()">
						<i class="fa fa-arrows-h"></i>
					</button></li>
				<li><a href="#">Mantenimiento</a></li>
				<li><a href="#">Objeto Grupo</a></li>
				<li class="active">Listado</li>
			</ol>
		</div>
	</div>
	<div class="cl-mcont">

		<div class="row">
			<div class="col-md-12">
				<div class="block-flat">
					<div class="header">
						<h3>Items de Grupo de Objeto: <?php echo $obj_objeto->getAttr('nombre');?></h3>
						<a href="<?php echo FULL_URL;?>objeto" class="btn pull-right" style="margin-top: -40px;margin-right: 70px;"><i class="fa fa-arrow-left"></i> Objetos</a>
						<a href="<?php echo FULL_URL;?>objeto_grupo/nuevo/<?php echo $obj_objeto->getID();?>" class="btn btn-primary pull-right" style="margin-top: -40px;">Nuevo</a>
					</div>
					<div class="content">
					
						<?php echo $this->Session->flash(); ?>
					
						<div class="table-responsive">
							<table class="table table-bordered" id="datatable">
								<thead>
									<tr>
										<th>Id</th>
										<th>Objeto Item</th>
										<th>Orden</th>
										<th>Acci&oacute;n</th>
									</tr>
								</thead>
								<tbody>
										<?php 
										foreach ($obj_objeto->ObjetoGrupo as $k => $obj_objeto_grupo){
										?>
										<tr>
											<td><?php echo $obj_objeto_grupo->getID();?></td>
											<td><?php echo $obj_objeto_grupo->Objeto->getAttr('nombre');?></td>
											<td><?php echo $obj_objeto_grupo->getAttr('orden');?></td>
											<td>
												<div class="btn-group">
													<button type="button" class="btn btn-default">Acci&oacute;n</button>
													<button type="button"
														class="btn btn-primary dropdown-toggle"
														data-toggle="dropdown">
														<span class="caret"></span> <span class="sr-only">Seleccionar</span>
													</button>
													<ul class="dropdown-menu" role="menu">
														<li><a href="<?php echo FULL_URL;?>objeto_grupo/editar/<?php echo $obj_objeto_grupo->getID();?>">Editar</a></li>
														<li><a href="<?php echo FULL_URL;?>objeto_grupo/eliminar/<?php echo $obj_objeto->getID();?>/<?php echo $obj_objeto_grupo->getID();?>">Eliminar</a></li>
													</ul>
												</div>
											</td>
										</tr>
										<?php }?>
									</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>