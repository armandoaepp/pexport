<div class="page-aside app tree" id="pageContent">
	<div class="fixed nano nscroller has-scrollbar">
		<div class="content" tabindex="0" style="right: -15px;">
			<div class="header">
				<button class="navbar-toggle" data-target=".treeview"
					data-toggle="collapse" type="button">
					<span class="fa fa-chevron-down"></span>
				</button>
				<h2 class="page-title">Tipo Intervenci&oacute;n</h2>
				<p class="description">Mantenimiento</p>
			</div>
			<?php echo $this->element('menu_mantenimiento'); ?>
		</div>

		<div class="pane" style="display: block;">
			<div class="slider" style="height: 428px; top: 0px;"></div>
		</div>
	</div>
</div>
<div class="container-fluid" id="pcont">
	<div id="cabeza">
		<div class="page-head" style="padding: 0px 0px;">
			<ol class="breadcrumb">
				<li><button type="button" class="btn btn-default btn-xs"
						onclick="arbol()">
						<i class="fa fa-arrows-h"></i>
					</button></li>
				<li><a href="#">Mantenimiento</a></li>
				<li><a href="#">Tipo Intervenci&oacute;n</a></li>
				<li class="active">Editar</li>
			</ol>
		</div>
	</div>
	<div class="cl-mcont">

		<div class="row">
			<div class="col-md-12">
				<div class="block-flat">
					<div class="header">
						<h3>Editar: Tipo de Intervenci&oacute;n</h3>
					</div>
					<div class="content">

						<form class="form-horizontal group-border-dashed" action="<?php echo FULL_URL;?>intervencion_tipo/editar/<?php echo $obj_intervencion_tipo->getID();?>" method="post" style="border-radius: 0px;">
						
							<div class="form-group">
								<label class="col-sm-3 control-label">Nombre</label>
								<div class="col-sm-6">
									<input type="text" class="form-control" name="data[IntervencionTipo][nombre]" id="IntervencionTipoNombre" value="<?php echo $obj_intervencion_tipo->getAttr('nombre');?>">
								</div>
							</div>

							<div class="form-group">
								<label class="col-sm-3 control-label">Descripci&oacute;n</label>
								<div class="col-sm-6">
									<input type="text" class="form-control" name="data[IntervencionTipo][descripcion]" id="IntervencionTipoDescripcion" value="<?php echo $obj_intervencion_tipo->getAttr('descripcion');?>">
								</div>
							</div>

							<div class="form-group">
								<label class="col-sm-3 control-label">Parent</label>
								<div class="col-sm-2">
									<select class="form-control" name="data[IntervencionTipo][parent_id]" id="IntervencionTipoParent">
										<option value="" <?php if($obj_intervencion_tipo->getAttr('parent_id')==''){ echo 'selected';}?>>Ninguno</option>
										<?php foreach ($arr_obj_intervencion_tipo as $k => $obj_intervencion_tipo2){?>
										<option value="<?php echo $obj_intervencion_tipo2->getID();?>" <?php if($obj_intervencion_tipo2->getAttr('id')==$obj_intervencion_tipo->getAttr('parent_id')){ echo 'selected';}?>><?php echo $obj_intervencion_tipo2->getAttr('nombre');?></option>
										<?php }?>
									</select>
								</div>
							</div>

							<div class="form-group">
								<div class="col-sm-offset-3 col-sm-9">
									<button type="submit" class="btn btn-primary">Grabar</button>
									<a href="<?php echo FULL_URL;?>intervencion_tipo" class="btn btn-default">Cancelar</a>
								</div>
							</div>

						</form>

					</div>
				</div>
			</div>
		</div>
	</div>
</div>