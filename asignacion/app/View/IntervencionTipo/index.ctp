<div class="page-aside app tree" id="pageContent">
	<div class="fixed nano nscroller has-scrollbar">
		<div class="content" tabindex="0" style="right: -15px;">
			<div class="header">
				<button class="navbar-toggle" data-target=".treeview"
					data-toggle="collapse" type="button">
					<span class="fa fa-chevron-down"></span>
				</button>
				<h2 class="page-title">Tipo Intervenci&oacute;n</h2>
				<p class="description">Mantenimiento</p>
			</div>
			<?php echo $this->element('menu_mantenimiento'); ?>
		</div>

		<div class="pane" style="display: block;">
			<div class="slider" style="height: 428px; top: 0px;"></div>
		</div>
	</div>
</div>
<div class="container-fluid" id="pcont">
	<div id="cabeza">
		<div class="page-head" style="padding: 0px 0px;">
			<ol class="breadcrumb">
				<li><button type="button" class="btn btn-default btn-xs"
						onclick="arbol()">
						<i class="fa fa-arrows-h"></i>
					</button></li>
				<li><a href="#">Mantenimiento</a></li>
				<li><a href="#">Tipo Intervenci&oacute;n</a></li>
				<li class="active">Listado</li>
			</ol>
		</div>
	</div>
	<div class="cl-mcont">

		<div class="row">
			<div class="col-md-12">
				<div class="block-flat">
					<div class="header">
						<h3>Tipos de Intervenci&oacute;n</h3>
						<a href="<?php echo FULL_URL;?>intervencion_tipo/nuevo" class="btn btn-primary pull-right" style="margin-top: -40px;">Nuevo</a>
					</div>
					<div class="content">
					
						<?php echo $this->Session->flash(); ?>
					
						<div class="table-responsive">
							<table class="table table-bordered" id="datatable">
								<thead>
									<tr>
										<th>Id</th>
										<th>Nombre</th>
										<th>Descripci&oacute;n</th>
										<th>Parent</th>
										<th>Acci&oacute;n</th>
									</tr>
								</thead>
								<tbody>
										<?php 
										//debug($arr_tipo_objeto);exit;
										foreach ($arr_obj_intervencion_tipo as $k => $obj_intervencion_tipo){
										?>
										<tr>
											<td><?php echo $obj_intervencion_tipo->getID();?></td>
											<td><?php echo $obj_intervencion_tipo->getAttr('nombre');?></td>
											<td><?php echo $obj_intervencion_tipo->getAttr('descripcion');?></td>
											<td><?php echo $obj_intervencion_tipo->IntervencionTipoParent->getAttr('nombre');?></td>
											<td>
												<div class="btn-group">
													<button type="button" class="btn btn-default">Acci&oacute;n</button>
													<button type="button"
														class="btn btn-primary dropdown-toggle"
														data-toggle="dropdown">
														<span class="caret"></span> <span class="sr-only">Seleccionar</span>
													</button>
													<ul class="dropdown-menu" role="menu">
														<li><a href="<?php echo FULL_URL;?>intervencion_tipo/editar/<?php echo $obj_intervencion_tipo->getID();?>">Editar</a></li>
														<li><a href="<?php echo FULL_URL;?>intervencion_tipo/eliminar/<?php echo $obj_intervencion_tipo->getID();?>">Eliminar</a></li>
														<li><a href="<?php echo FULL_URL;?>intervencion_grupo/formulario/<?php echo $obj_intervencion_tipo->getID();?>">Formulario</a></li>
														<li><a href="<?php echo FULL_URL;?>intervencion_tipo_web_service/index/<?php echo $obj_intervencion_tipo->getID();?>">Web Services</a></li>
													</ul>
												</div>
											</td>
										</tr>
										<?php }?>
									</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>