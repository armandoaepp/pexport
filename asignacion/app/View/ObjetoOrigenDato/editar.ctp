<div class="page-aside app tree" id="pageContent">
	<div class="fixed nano nscroller has-scrollbar">
		<div class="content" tabindex="0" style="right: -15px;">
			<div class="header">
				<button class="navbar-toggle" data-target=".treeview"
					data-toggle="collapse" type="button">
					<span class="fa fa-chevron-down"></span>
				</button>
				<h2 class="page-title">Objeto Origen Dato</h2>
				<p class="description">Mantenimiento</p>
			</div>
			<?php echo $this->element('menu_mantenimiento'); ?>
		</div>

		<div class="pane" style="display: block;">
			<div class="slider" style="height: 428px; top: 0px;"></div>
		</div>
	</div>
</div>
<div class="container-fluid" id="pcont">
	<div id="cabeza">
		<div class="page-head" style="padding: 0px 0px;">
			<ol class="breadcrumb">
				<li><button type="button" class="btn btn-default btn-xs"
						onclick="arbol()">
						<i class="fa fa-arrows-h"></i>
					</button></li>
				<li><a href="#">Mantenimiento</a></li>
				<li><a href="#">Objeto Origen Dato</a></li>
				<li class="active">Editar</li>
			</ol>
		</div>
	</div>
	<div class="cl-mcont">

		<div class="row">
			<div class="col-md-12">
				<div class="block-flat">
					<div class="header">
						<h3>Editar: Origen de Datos</h3>
					</div>
					<div class="content">

						<form class="form-horizontal group-border-dashed" action="<?php echo FULL_URL;?>objeto_origen_dato/editar/<?php echo $obj_objeto_origen_dato->getID();?>" method="post" style="border-radius: 0px;">
						
							<div class="form-group">
								<label class="col-sm-3 control-label">Nombre</label>
								<div class="col-sm-6">
									<input type="text" class="form-control" name="data[ObjetoOrigenDato][nombre]" id="ObjetoOrigenDatoNombre" value="<?php echo $obj_objeto_origen_dato->getAttr('nombre');?>">
								</div>
							</div>

							<div class="form-group">
								<label class="col-sm-3 control-label">Descripci&oacute;n</label>
								<div class="col-sm-6">
									<input type="text" class="form-control" name="data[ObjetoOrigenDato][descripcion]" id="ObjetoOrigenDatoDescripcion" value="<?php echo $obj_objeto_origen_dato->getAttr('descripcion');?>">
								</div>
							</div>
							
							<div class="form-group">
								<label class="col-sm-3 control-label">Base de Datos: Host</label>
								<div class="col-sm-6">
									<input type="text" class="form-control" name="data[ObjetoOrigenDato][bd_host]" id="ObjetoOrigenDatoBdHost" placeholder="http://..." value="<?php echo $obj_objeto_origen_dato->getAttr('bd_host');?>">
								</div>
							</div>
							
							<div class="form-group">
								<label class="col-sm-3 control-label">Base de Datos: Nombre</label>
								<div class="col-sm-6">
									<input type="text" class="form-control" name="data[ObjetoOrigenDato][bd_nombre]" id="ObjetoOrigenDatoBdNombre" value="<?php echo $obj_objeto_origen_dato->getAttr('bd_nombre');?>">
								</div>
							</div>
							
							<div class="form-group">
								<label class="col-sm-3 control-label">Base de Datos: Campos</label>
								<div class="col-sm-6">
									<input type="text" class="form-control" name="data[ObjetoOrigenDato][bd_campos]" id="ObjetoOrigenDatoBdCampos" placeholder="campo1,campo2,campo3..." value="<?php echo $obj_objeto_origen_dato->getAttr('bd_campos');?>">
								</div>
							</div>
							
							<div class="form-group">
								<label class="col-sm-3 control-label">Base de Datos: Campos Ocultos</label>
								<div class="col-sm-6">
									<input type="text" class="form-control" name="data[ObjetoOrigenDato][bd_campos_ocultos]" id="ObjetoOrigenDatoBdCamposOcultos" placeholder="campo1,campo2,campo3..." value="<?php echo $obj_objeto_origen_dato->getAttr('bd_campos_ocultos');?>">
								</div>
							</div>
							
							<div class="form-group">
								<label class="col-sm-3 control-label">Base de Datos: Tablas</label>
								<div class="col-sm-6">
									<input type="text" class="form-control" name="data[ObjetoOrigenDato][bd_tablas]" id="ObjetoOrigenDatoBdTablas" placeholder="tabla1 inner join tabla2 on ..." value="<?php echo $obj_objeto_origen_dato->getAttr('bd_tablas');?>">
								</div>
							</div>
							
							<div class="form-group">
								<label class="col-sm-3 control-label">Base de Datos: Filtros</label>
								<div class="col-sm-6">
									<input type="text" class="form-control" name="data[ObjetoOrigenDato][bd_filtros]" id="ObjetoOrigenDatoBdFiltros" placeholder="filtro1=[$objeto_id] and filtro1=condicion1" value="<?php echo $obj_objeto_origen_dato->getAttr('bd_filtros');?>">
								</div>
							</div>
							
							<div class="form-group">
								<label class="col-sm-3 control-label">Web Service: Host</label>
								<div class="col-sm-6">
									<input type="text" class="form-control" name="data[ObjetoOrigenDato][ws_host]" id="ObjetoOrigenDatoWsHost" placeholder="http://..." value="<?php echo $obj_objeto_origen_dato->getAttr('ws_host');?>">
								</div>
							</div>
							
							<div class="form-group">
								<label class="col-sm-3 control-label">Web Service: Metodo</label>
								<div class="col-sm-2">
									<select class="form-control" name="data[ObjetoOrigenDato][ws_metodo]" id="ObjetoOrigenDatoWsMetodo">
										<option value="POST" <?php if($obj_objeto_origen_dato->getAttr('ws_metodo')=='POST'){ echo 'selected';}?>>POST</option>
										<option value="GET" <?php if($obj_objeto_origen_dato->getAttr('ws_metodo')=='GET'){ echo 'selected';}?>>GET</option>
										<option value="PUT" <?php if($obj_objeto_origen_dato->getAttr('ws_metodo')=='PUT'){ echo 'selected';}?>>PUT</option>
									</select>
								</div>
							</div>
							
							<div class="form-group">
								<label class="col-sm-3 control-label">Web Service: Parametros</label>
								<div class="col-sm-6">
									<input type="text" class="form-control" name="data[ObjetoOrigenDato][ws_parametros]" id="ObjetoOrigenDatoWsParametros" placeholder="parametro1=valor1,parametro2=valor2" value="<?php echo $obj_objeto_origen_dato->getAttr('ws_parametros');?>">
								</div>
							</div>
							
							<div class="form-group">
								<label class="col-sm-3 control-label">Web Service: Modo</label>
								<div class="col-sm-2">
									<select class="form-control" name="data[ObjetoOrigenDato][ws_modo]" id="ObjetoOrigenDatoWsModo">
										<option value="ONLINE" <?php if($obj_objeto_origen_dato->getAttr('ws_modo')=='ONLINE'){ echo 'selected';}?>>On Line</option>
										<option value="OFFLINE" <?php if($obj_objeto_origen_dato->getAttr('ws_modo')=='OFFLINE'){ echo 'selected';}?>>Off Line</option>
									</select>
								</div>
							</div>
							
							<div class="form-group">
								<label class="col-sm-3 control-label">Local: Filtros</label>
								<div class="col-sm-6">
									<input type="text" class="form-control" name="data[ObjetoOrigenDato][local_filtros]" id="ObjetoOrigenDatoLocalFiltros" placeholder="filtro1=[$objeto_id] and filtro1=condicion1" value="<?php echo $obj_objeto_origen_dato->getAttr('local_filtros');?>">
								</div>
							</div>

							<div class="form-group">
								<div class="col-sm-offset-3 col-sm-9">
									<button type="submit" class="btn btn-primary">Grabar</button>
									<a href="<?php echo FULL_URL;?>objeto_origen_dato" class="btn btn-default">Cancelar</a>
								</div>
							</div>

						</form>

					</div>
				</div>
			</div>
		</div>
	</div>
</div>