<div class="page-aside app tree" id="pageContent">
	<div class="fixed nano nscroller has-scrollbar">
		<div class="content" tabindex="0" style="right: -15px;">
			<div class="header">
				<button class="navbar-toggle" data-target=".treeview"
					data-toggle="collapse" type="button">
					<span class="fa fa-chevron-down"></span>
				</button>
				<h2 class="page-title">Tipo Intervenci&oacute;n</h2>
				<p class="description">Mantenimiento</p>
			</div>
			<?php echo $this->element('menu_mantenimiento'); ?>
		</div>

		<div class="pane" style="display: block;">
			<div class="slider" style="height: 428px; top: 0px;"></div>
		</div>
	</div>
</div>
<div class="container-fluid" id="pcont">
	<div id="cabeza">
		<div class="page-head" style="padding: 0px 0px;">
			<ol class="breadcrumb">
				<li><button type="button" class="btn btn-default btn-xs"
						onclick="arbol()">
						<i class="fa fa-arrows-h"></i>
					</button></li>
				<li><a href="#">Mantenimiento</a></li>
				<li><a href="#">Tipo Intervenci&oacute;n</a></li>
				<li class="active">Listado</li>
			</ol>
		</div>
	</div>
	<div class="cl-mcont">

		<div class="row">
			<div class="col-md-12">

				<div class="tab-container tab-right">
					<ul class="nav nav-tabs flat-tabs">
						<li class="active"><a href="#tab4-1" data-toggle="tab"><i
								class="fa fa-building-o"></i></a></li>
						<li><a href="#tab4-2" data-toggle="tab" class="tab_json_form"><i
								class="fa fa-code"></i></a></li>
						<li><a href="#tab4-3" data-toggle="tab" class="tab_view_form"><i
								class="fa fa-eye"></i></a></li>
					</ul>
					<div class="tab-content">
						<div class="tab-pane active cont fade in" id="tab4-1">
							<h2>Creaci&oacute;n de Formularios</h2>
							<div class="content">
					
							<?php echo $this->Session->flash();?>
						
								<div class="row">
								<div class="col-sm-6 col-md-6">
									<div class="block-flat">
										<div class="header">
											<h3>Lista de Grupos</h3>
										</div>
										<ol id="sortable-one" class="connectedSortable dd-list" sort="1" data-intervencion_tipo="<?php echo $intervencion_tipo_id?>" style="min-height: 120px;">
											<?php
											$sortable_one = ""; 
											foreach ($arr_obj_objeto as $k => $obj_objeto){
												if($obj_objeto->TipoObjeto->getAttr('objetos')){
													$sortable_one .="#sortable".$obj_objeto->getID().", "; 
													cadenaObjetoGruposItems($obj_objeto);
												}
											}?>
										</ol>
									</div>
								</div>
	
								<div class="col-sm-6 col-md-6">
									<div class="block-flat">
										<div class="header">
											<h3>Formulario</h3>
										</div>
										<ol id="sortable-two" class="connectedSortable dd-list" sort="2" data-intervencion_tipo="<?php echo $intervencion_tipo_id?>" style="min-height: 120px;">
											<?php
											$sortable_two = ""; 
											$arr_og = array();
											foreach ($arr_obj_objeto_grupo as $k => $obj_objeto_grupo){
												if (!in_array($obj_objeto_grupo->data['ObjetoGrupo']['grupo_objeto_id'], $arr_og)) {
													$arr_og[]= $obj_objeto_grupo->data['ObjetoGrupo']['grupo_objeto_id'];
													$sortable_two .="#sortable".$obj_objeto_grupo->ObjetoParent->getID().", ";
													cadenaObjetoGruposItems($obj_objeto_grupo->ObjetoParent);
												}}?>
										</ol>
									</div>
								</div>
							</div>
						</div>
						</div>
						
						<div class="tab-pane fade cont" id="tab4-2">
							<h2>Json Form</h2>
							<div class="content">
							<iframe id="iframe_json_form" src="" width="100%" height="1000px" frameborder="0" scrolling="si"></iframe>
							</div>
						</div>
						
						<div class="tab-pane fade cont" id="tab4-3">
							<h2>Vista Previa del Formulario</h2>
							<div class="content">
							<iframe id="iframe_view_form" src="" width="100%" height="1000px" frameborder="0" scrolling="si"></iframe>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<?php 
function cadenaObjetoGruposItems($obj_objeto){
?>
<li class="items_grupos dd-item" data-objeto_grupo="<?php echo $obj_objeto->getID();?>">
	<button class="btn-openclose btn-collapse" data-action="collapse" type="button">Collapse</button>
	<button class="btn-openclose btn-expand" data-action="expand" type="button" style="display: none;">Expand</button>
	<div class="dd-handle">
		<?php echo $obj_objeto->getAttr('nombre');?> - 
		<?php echo $obj_objeto->TipoObjeto->getAttr('nombre');?> - 
		<?php echo $obj_objeto->TipoDato->getAttr('nombre');?>
	</div>
	<ol class="dd-list" id="sortable<?php echo $obj_objeto->getID();?>">
		<?php foreach ($obj_objeto->ObjetoGrupo as $kk => $obj_objeto_grupo){?>
			<?php if($obj_objeto_grupo->Objeto->TipoObjeto->getAttr('objetos')){
						cadenaObjetoGruposItems($obj_objeto_grupo->Objeto);
				  }else{?>
					<li class="dd-handle items_grupo" data-objeto="<?php echo $obj_objeto_grupo->getID();?>">
						<?php echo $obj_objeto_grupo->Objeto->getAttr('nombre');?> - 
						<?php echo $obj_objeto_grupo->Objeto->TipoObjeto->getAttr('nombre');?> - 
						<?php echo $obj_objeto_grupo->Objeto->TipoDato->getAttr('nombre');?>
					</li>
			<?php }
		 	}?>
	</ol>
<li>
<?php	
}
?>
<script>
  $(function() {
	$("<?php echo substr($sortable_one, 0, -2); ?>").sortable();
	$("<?php echo substr($sortable_one, 0, -2); ?>").disableSelection();

	$("<?php echo substr($sortable_two, 0, -2); ?>").sortable();
	$("<?php echo substr($sortable_two, 0, -2); ?>").disableSelection();

	$( "#sortable-one, #sortable-two" ).sortable({
		connectWith: ".connectedSortable",
		tolerance: "pointer",
		cursor:"move",
		opacity: .8,
		start: function(event, ui) {
			$item = ui.item;
			start = $item.parents('.connectedSortable').attr('sort');
		},
		stop: function(event, ui) {
			$item = ui.item;
			end = $item.parents('.connectedSortable').attr('sort')
			
			var intervencion_tipo_id = $item.parents('.connectedSortable').data('intervencion_tipo');
			var arr_objeto_id = [];
			$( "li.items_grupo" , $item).each(function( index ) {
				if($(this).data('objeto') != null){
					arr_objeto_id.push($(this).data('objeto'));
				}
			});
			//alert("start:"+start+",end:"+end);
			if(start != end){
				if(start==1 && end==2){
					alert('dropable agregar');
					console.log(arr_objeto_id);
					$.ajax({
						url : '<?php echo FULL_URL;?>intervencion_grupo/ajax_guardar/'+intervencion_tipo_id+'/'+JSON.stringify(arr_objeto_id),
						dataType: 'json',
						type: "POST",
						success:function(data, textStatus, jqXHR){
							console.log(data);
							if(data.success==false){
								alert(data.msg);
							}
						}
					});
					actualizarOrdenGrupos();
				}else{
					alert('dropable eliminar');
					$.ajax({
						url : '<?php echo FULL_URL;?>intervencion_grupo/ajax_eliminar/'+intervencion_tipo_id+'/'+JSON.stringify(arr_objeto_id),
						dataType: 'json',
						type: "POST",
						success:function(data, textStatus, jqXHR){
							console.log(data);
							if(data.success==false){
								alert(data.msg);
							}
						}
					});
					actualizarOrdenGrupos();
				}
			}else{
				if(start==2 && end==2){
					alert('sortable');
					actualizarOrdenGrupos();
				}
			}
      	}
    }).disableSelection();

	function actualizarOrdenGrupos(){
		var arr_orden_objeto_id = [];
		var intervencion_tipo_id = $('#sortable-two').data('intervencion_tipo');
		$( "li.items_grupo" , $('#sortable-two')).each(function( index ) {
			if($(this).data('objeto') != null){
				arr_orden_objeto_id.push($(this).data('objeto'));
			}
		});
		console.log(arr_orden_objeto_id);

		$.ajax({
			url : '<?php echo FULL_URL;?>intervencion_grupo/ajax_cambiar_orden/'+intervencion_tipo_id+'/'+JSON.stringify(arr_orden_objeto_id),
			dataType: 'json',
			type: "POST",
			success:function(data, textStatus, jqXHR){
				console.log(data);
				if(data.success==false){
					alert(data.msg);
				}
			}
		});
	}
    
    $('.btn-openclose').click(function (){
    	if($(this).data('action')=='collapse'){
	    	$('.dd-list', $(this).parent('.items_grupos')).hide();
	    	$('.btn-collapse', $(this).parent('.items_grupos')).hide();
	    	$('.btn-expand', $(this).parent('.items_grupos')).show();
    	}else{
	    	$('.dd-list', $(this).parent('.items_grupos')).show();
	    	$('.btn-collapse', $(this).parent('.items_grupos')).show();
	    	$('.btn-expand', $(this).parent('.items_grupos')).hide();
    	}
    });

    $('.tab_json_form').click(function (){
    	$('#iframe_json_form').attr('src','<?php echo FULL_URL;?>intervencion_grupo/formulario_json/<?php echo $intervencion_tipo_id;?>');
    });

	$('.tab_view_form').click(function (){
    	$('#iframe_view_form').attr('src','<?php echo FULL_URL;?>intervencion_grupo/formulario_preview/<?php echo $intervencion_tipo_id;?>');
	});
  });
</script>