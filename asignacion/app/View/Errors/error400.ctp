<?php
/**
 *
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.View.Errors
 * @since         CakePHP(tm) v 0.10.0.1076
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */
?>
<!-- 
<h2><?php echo $name; ?></h2>
<p class="error">
	<strong><?php echo __d('cake', 'Error'); ?>: </strong>
	<?php printf(
		__d('cake', 'The requested address %s was not found on this server.'),
		"<strong>'{$url}'</strong>"
	); ?>
</p>
<?php
if (Configure::read('debug') > 0):
	echo $this->element('exception_stack_trace');
endif;
?>
 -->

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="description" content="">
	<meta name="author" content="">
	<link rel="shortcut icon" href="images/favicon.png">

	<title>SIGOF - Sistema Integrado para la Gesti&oacute;n Operativa de Facturaci&oacute;n</title>
	<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,400italic,700,800' rel='stylesheet' type='text/css'>
	<link href='http://fonts.googleapis.com/css?family=Raleway:300,200,100' rel='stylesheet' type='text/css'>

	<!-- Bootstrap core CSS -->
	<link href="<?php echo FULL_URL ?>js/bootstrap/dist/css/bootstrap.css" rel="stylesheet">

	<link rel="stylesheet" href="<?php echo FULL_URL ?>fonts/font-awesome-4/css/font-awesome.min.css">

	<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!--[if lt IE 9]>
	  <script src="../../assets/js/html5shiv.js"></script>
	  <script src="../../assets/js/respond.min.js"></script>
	<![endif]-->

	<!-- Custom styles for this template -->
	<link href="<?php echo FULL_URL ?>css/style.css" rel="stylesheet" />	

</head>

<body class="texture">

<div id="cl-wrapper" class="error-container">
	<div class="page-error">
		<h1 class="number text-center">404</h1>
		<h2 class="description text-center">Lo sentimos, pero esta página no existe!</h2>
		<h3 class="text-center">¿Le gustaría ir a <a href="<?php echo FULL_URL ?>index.html">home</a>?</h3>
		<h4>
		<?php
		if (Configure::read('debug') > 0):
			echo $this->element('exception_stack_trace');
		endif;
		?>
		</h4>
	</div>
	<div class="text-center copy">&copy; 2014 <a href="#"><?php echo Configure::read('lecturas.company');?></a></div>

	
</div>

<script src="<?php echo FULL_URL ?>js/jquery.js"></script>
	<script type="text/javascript" src="<?php echo FULL_URL ?>js/behaviour/general.js"></script>

<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
  <script src="<?php echo FULL_URL ?>js/behaviour/voice-commands.js"></script>
  <script src="<?php echo FULL_URL ?>js/bootstrap/dist/js/bootstrap.min.js"></script>
<script type="text/javascript" src="<?php echo FULL_URL ?>js/jquery.flot/jquery.flot.js"></script>
<script type="text/javascript" src="<?php echo FULL_URL ?>js/jquery.flot/jquery.flot.pie.js"></script>
<script type="text/javascript" src="<?php echo FULL_URL ?>js/jquery.flot/jquery.flot.resize.js"></script>
<script type="text/javascript" src="<?php echo FULL_URL ?>js/jquery.flot/jquery.flot.labels.js"></script>
</body>
</html>