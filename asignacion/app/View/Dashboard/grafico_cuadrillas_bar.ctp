<?php
date_default_timezone_set('America/Lima');

//require_once "../conexion.php";
require_once(APP."View/Main/conexion.ctp");

$fechaactual=date("Y-m-d");

if(isset($_GET["idini1"]))
{
  $idini1=$_GET["idini1"];
} else {
  $idini1=$fechaactual;
}


if(isset($_GET["idfin1"]))
{
  $idfin1=$_GET["idfin1"];
} else {
  $idfin1=$fechaactual;
}



$query_grafico = pg_query("
select 
Cuadrilla,
SubCuadrilla,
IdConcesionaria,
setfinal.Sector,
TotalAsignadas,
TotalDescargadas,
TotalExportadas,
TotalFinalizadas from (
select IdSector,Sector,
SUM(TotalAsignadas) as TotalAsignadas,
SUM(TotalDescargadas) as TotalDescargadas,
SUM(TotalExportadas) as TotalExportadas,
SUM(TotalFinalizadas) as TotalFinalizadas from (
select  
co.IdSector as IdSector,
co.Sector as Sector,
(case when ci.IdEstado = 13 and co.Condicion='0' and co.SubActividad 
in (select DetalleTipoIntervencion from ComMovTab_TipoIntervencion)
then 1 else 0 end) as TotalDescargadas,
(case when ci.IdEstado = 5 and co.Condicion='0' and co.SubActividad 
in (select DetalleTipoIntervencion from ComMovTab_TipoIntervencion)
then 1 else 0 end) as TotalAsignadas,
(case when ci.IdEstado = 14 and co.Condicion='0' and co.SubActividad 
in (select DetalleTipoIntervencion from ComMovTab_TipoIntervencion)
then 1 else 0 end) as TotalExportadas,
(case when ci.IdEstado = 15 and co.Condicion='0' and co.SubActividad 
in (select DetalleTipoIntervencion from ComMovTab_TipoIntervencion)
then 1 else 0 end) as TotalFinalizadas
from ComMovTab_Orden co 
inner join ComMovTab_Intervencion ci on co.IdOrden = ci.IdOrden
where co.Condicion='0' and co.SubActividad 
in (select DetalleTipoIntervencion from ComMovTab_TipoIntervencion)) as superconsulta
group by IdSector,Sector) as setfinal
inner join (
select
gu.IdUbicacionConc as IdConcesionaria,
case when (select IdConceptosUbicacion from GlobalMaster_Ubicacion where IdUbicacion =dr2.IdUbicacionSup)=8
or (select IdConceptosUbicacion from GlobalMaster_Ubicacion where IdUbicacion =dr2.IdUbicacionSup)=7  
then gu3.NomUbicacion else gu2.NomUbicacion end as Cuadrilla,
gu2.NomUbicacion as SubCuadrilla,
gu.NomUbicacion as Sector
from GlobalMaster_DetalleUbicacionRegla dr
inner join GlobalMaster_DetalleUbicacionRegla dr2 on dr.IdUbicacionSup = dr2.IdUbicacion
inner join GlobalMaster_Ubicacion gu on dr.IdUbicacion = gu.IdUbicacion
inner join GlobalMaster_Ubicacion gu2 on dr.IdUbicacionSup = gu2.IdUbicacion
inner join GlobalMaster_Ubicacion gu3 on dr2.IdUbicacionSup = gu3.IdUbicacion
where gu.IdConceptosUbicacion = 5 ) as setfinal2 on
setfinal.IdSector=cast(setfinal2.IdConcesionaria as integer)
order by Cuadrilla asc, SubCuadrilla asc
");


$rs=pg_num_rows($query_grafico);

$cuadrilla = '';
$sub_cuadrilla = '';
$sector = '';
$series = '';
$series_drilldown = '';
$sum_cuadrillas = 0;
$sum_sub_cuadrillas = 0;
$sum_sectores = 0;
if ($rs > 0) {

	while ($obj = pg_fetch_object($query_grafico)){
	/*$arr = array();
	$arr[] = (Object) array('Cuadrilla'=>'Cuadrilla1','SubCuadrilla'=>'SubCuadrilla1','Sector'=>'Sector1','TotalAsignadas'=>4,'TotalDescargadas'=>2,'TotalExportadas'=>1,'TotalFinalizadas'=>3);
	$arr[] = (Object) array('Cuadrilla'=>'Cuadrilla1','SubCuadrilla'=>'SubCuadrilla2','Sector'=>'Sector2','TotalAsignadas'=>4,'TotalDescargadas'=>2,'TotalExportadas'=>1,'TotalFinalizadas'=>3);
	$arr[] = (Object) array('Cuadrilla'=>'Cuadrilla1','SubCuadrilla'=>'SubCuadrilla2','Sector'=>'Sector3','TotalAsignadas'=>4,'TotalDescargadas'=>2,'TotalExportadas'=>1,'TotalFinalizadas'=>3);
	$arr[] = (Object) array('Cuadrilla'=>'Cuadrilla2','SubCuadrilla'=>'SubCuadrilla3','Sector'=>'Sector4','TotalAsignadas'=>4,'TotalDescargadas'=>2,'TotalExportadas'=>1,'TotalFinalizadas'=>3);
	$arr[] = (Object) array('Cuadrilla'=>'Cuadrilla2','SubCuadrilla'=>'SubCuadrilla3','Sector'=>'Sector5','TotalAsignadas'=>4,'TotalDescargadas'=>2,'TotalExportadas'=>1,'TotalFinalizadas'=>3);
	$arr = (Object) $arr;
	
	foreach ($arr as $k=>$obj){*/
		
		if($cuadrilla!=$obj->cuadrilla){
			$series = str_replace('%y-cuadrillas%',$sum_cuadrillas,$series);
			$sum_cuadrillas = 0;
			$series .= "{
	            name: '".$obj->cuadrilla."',
	            y: %y-cuadrillas%,
	            drilldown: '".$obj->cuadrilla."'
	        },";
			
			$series_drilldown = str_replace(',%sub-cuadrilla%','',$series_drilldown);
			$series_drilldown .= "{
				id: '".$obj->cuadrilla."',
	            name: '".$obj->cuadrilla."',
	            data: [%sub-cuadrilla%]
	        },";
			$cuadrilla = $obj->cuadrilla;
		}
		if($sub_cuadrilla!=$obj->subcuadrilla){
			$series_drilldown = str_replace('%y-sub-cuadrillas%',$sum_sub_cuadrillas,$series_drilldown);
			$sum_sub_cuadrillas = 0;
			
			$series_drilldown = str_replace(',%sector%','',$series_drilldown);
			$series_sub_cuadrilla = "{
	            name: '".$obj->subcuadrilla."',
	            y: %y-sub-cuadrillas%,
	            drilldown: '".$obj->subcuadrilla."'
	        },%sub-cuadrilla%";
			$series_drilldown = str_replace('%sub-cuadrilla%',$series_sub_cuadrilla,$series_drilldown);
			$series_drilldown .= "{
	            id: '".$obj->subcuadrilla."',
	            name: '".$obj->subcuadrilla."',
	            data: [%sector%]
	        },";
			$sub_cuadrilla = $obj->subcuadrilla;
		}
		if($sector!=$obj->sector){
			$series_drilldown = str_replace('%y-sectores%',$sum_sectores,$series_drilldown);
			$sum_sectores = 0;
			
			$series_sector = "{
	            name: '".$obj->sector."',
	            y: %y-sectores%,
	            drilldown: '".$obj->sector."'
	        },%sector%";
			$series_drilldown = str_replace('%sector%',$series_sector,$series_drilldown);
			$series_drilldown .= "{
				id: '".$obj->sector."',
	            name: '".$obj->sector."',
	            data: [%datos%]
	        },";
			$sector = $obj->sector;
		}
		
		$series_datos = "['Asignadas', ".$obj->totalasignadas."],
				['Descargadas', ".$obj->totaldescargadas."],
				['Exportadas', ".$obj->totalexportadas."],
				['Finalizadas', ".$obj->totalfinalizadas."]";
		$series_drilldown = str_replace('%datos%',$series_datos,$series_drilldown);
		
		$sum_cuadrillas += ($obj->totalasignadas + $obj->totaldescargadas + $obj->totalexportadas + $obj->totalfinalizadas);
		$sum_sub_cuadrillas += ($obj->totalasignadas + $obj->totaldescargadas + $obj->totalexportadas + $obj->totalfinalizadas);
		$sum_sectores += ($obj->totalasignadas + $obj->totaldescargadas + $obj->totalexportadas + $obj->totalfinalizadas);
		
	}
	$series = substr($series,0,-1);
	$series = str_replace('%y-cuadrillas%',$sum_cuadrillas,$series);
	$series_drilldown = str_replace('%y-sub-cuadrillas%',$sum_sub_cuadrillas,$series_drilldown);
	$series_drilldown = str_replace('%y-sectores%',$sum_sectores,$series_drilldown);
	$series_drilldown = str_replace(',%sub-cuadrilla%','',$series_drilldown);
	$series_drilldown = str_replace(',%sector%','',$series_drilldown);
	$series_drilldown = substr($series_drilldown,0,-1);
	//echo $series;
	//echo '<br>';
	//echo $series_drilldown;
	//exit();
}else{
	echo 'No hay datos';
	exit();
}

?>
<!DOCTYPE html>
<html lang="es" >
<head>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta http-equiv="content-type" content="text/html; charset=UTF-8">
<title>Grafico Pexport</title>

<script type="text/javascript" src="//code.jquery.com/jquery-1.9.1.js"></script>



<script type="text/javascript">//<![CDATA[

$(function () {

	// Internationalization
	Highcharts.setOptions({
	    lang: {
	        drillUpText: '◁ Regresar a {series.name}'
	    }
	});

	var options = {

	    chart: {
	        height: 300
	    },
	    
	    title: {
	        text: 'Ordenes por cuadrillas, sub cuadrillas y sectores'
	    },

	    subtitle: {
            text: 'Click en la columna para ver el detalle'
        },

	    xAxis: {
	        categories: true
	    },

	    yAxis: {
            title: {
                text: 'Cantidad de Ordenes'
            }
        },
	    
	    drilldown: {
	        series: [<?php echo $series_drilldown;?>
	     	        /*{
	            id: 'fruits',
	            name: 'Fruits',
	            data: [
	                ['Apples', 4],
	                ['Pears', 6],
	                ['Oranges', 2],
	                ['Grapes', 8]
	            ]
	        }, {
	            id: 'cars',
	            name: 'Cars',
	            data: [{
	                name: 'Toyota', 
	                y: 4,
	                drilldown: 'toyota'
	            },
	            ['Volkswagen', 3],
	            ['Opel', 5]
	            ]
	        }, {
	            id: 'toyota',
	            name: 'Toyota',
	            data: [
	                ['RAV4', 3],
	                ['Corolla', 1],
	                ['Carina', 4],
	                ['Land Cruiser', 5],
	                {
		                name: 'Toyota2', 
		                y: 4,
		                drilldown: 'toyota2'
		            }
	            ]
	        }, {
	            id: 'toyota2',
	            name: 'Toyota2',
	            data: [
	                ['RAV4xx', 3],
	                ['Corollaxx', 1]
	            ]
	        }*/]
	    },
	    
	    legend: {
	        enabled: false
	    },
	    
	    plotOptions: {
	        series: {
	            dataLabels: {
	                enabled: true
	            },
	            shadow: false
	        },
	        pie: {
	            size: '80%'
	        }
	    },

	    tooltip: {
            headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
            pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:f}</b> Ordenes<br/>'
        }, 

        credits: {
            enabled: false
        },
	    
	    series: [{
	        name: 'Cuadrillas',
	        colorByPoint: true,
	        data: [<?php echo $series;?>/*{
	            name: 'Fruits',
	            y: 10,
	            drilldown: 'fruits'
	        }, {
	            name: 'Cars',
	            y: 12,
	            drilldown: 'cars'
	        }, {
	            name: 'Countries',
	            y: 8
	        }*/]
	    }]
	};

	// Column chart
	options.chart.renderTo = 'container';
	options.chart.type = 'column';
	var chart1 = new Highcharts.Chart(options);	
    
});
//]]>

</script>


</head>
<body style="text-align: center;">
	<!--
	<script src="http://code.highcharts.com/highcharts.js"></script>
	<script src="http://code.highcharts.com/modules/exporting.js"></script>
	 -->

	<script src="../js/jquery.high/highcharts.js"></script>
	<script src="../js/jquery.high/modules/exporting.js"></script>
	<script src="../js/jquery.high/modules/drilldown.js"></script>

	<div id="container" style="min-width: 300px; max-width: 800px; height: 330px; margin: 0 auto;">
	</div>

</body>
</html>
