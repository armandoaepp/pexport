<?php

date_default_timezone_set('America/Lima');

//require_once "../conexion.php";
require_once(APP."View/Main/conexion.ctp");


$query_grafico = pg_query("select consulta1.anio as Anio,consulta1.mes as Mes,consulta1.NomMes as NomMes,
consulta2.Total as Generadas,
consulta1.TotalFin as Finalizadas,
((consulta2.Total)-consulta1.TotalFin) as Pendientes,
((consulta1.TotalFin*100)/consulta2.Total) as Porcentaje,
consulta3.TotalFin as Infructuosa,
consulta4.Total as GeneradasEnsa from 
(select extract('year' from FechaAtencion) as anio, extract('month' from FechaAtencion) as mes, to_char(FechaAtencion,'Mon') as NomMes,count(ci.IdEstado) as TotalFin  from ComMovTab_Intervencion ci
inner join ComMovTab_Orden co on ci.IdOrden=co.IdOrden
where co.SubActividad in (select DetalleTipoIntervencion from ComMovTab_TipoIntervencion)
and ci.IdEstado=15 and co.Condicion='0'
group by extract('year' from FechaAtencion), extract('month' from FechaAtencion), to_char(FechaAtencion,'Mon')) as consulta1 
left outer join 
(
select extract('year' from FechaAsignado) as anio, extract('month' from FechaAsignado) as mes,count(ci.IdEstado) as Total  from ComMovTab_Intervencion ci
inner join ComMovTab_Orden co on ci.IdOrden=co.IdOrden
where co.SubActividad in (select DetalleTipoIntervencion from ComMovTab_TipoIntervencion) and co.Condicion='0' 
group by extract('year' from FechaAsignado), extract('month' from FechaAsignado)) as consulta2 on consulta1.anio = consulta2.anio and consulta1.mes = consulta2.mes 
left outer join 
(
select extract('year' from FechaAtencion) as anio, extract('month' from FechaAtencion) as mes,count(ci.IdEstado) as TotalFin  from ComMovTab_Intervencion ci
inner join ComMovTab_Orden co on ci.IdOrden=co.IdOrden
inner join ComMovTab_Detalle_Intervencion DI on DI.IdIntervencion=ci.IdIntervencion
where co.SubActividad in (select DetalleTipoIntervencion from ComMovTab_TipoIntervencion)
and co.Condicion='0' and DI.IdMaestrosIntervencion in (
select IdMaestrosIntervencion from ComMovTab_MaestrosIntervencion 
where IdentificadorMaestroIntervencion like 'LabelInfructuosa%') 
group by extract('year' from FechaAtencion), extract('month' from FechaAtencion)) as consulta3 on consulta1.anio = consulta3.anio and consulta1.mes = consulta3.mes 
left outer join 
(
select extract('year' from cast(FechaGeneracion as timestamp)) as anio, extract('month' from cast(FechaGeneracion as timestamp)) as mes,count(ci.IdEstado) as Total  from ComMovTab_Intervencion ci
inner join ComMovTab_Orden co on ci.IdOrden=co.IdOrden
where co.SubActividad in (select DetalleTipoIntervencion from ComMovTab_TipoIntervencion) and co.Condicion='0' 
group by extract('year' from cast(FechaGeneracion as timestamp)), extract('month' from cast(FechaGeneracion as timestamp))) as consulta4 on consulta1.anio = consulta4.anio and consulta1.mes = consulta4.mes 
 order by 1 desc,2 desc limit 6");


$rs=pg_num_rows($query_grafico);

$series = '';
$mons = array(1 => "Ene", 2 => "Feb", 3 => "Mar", 4 => "Abr", 5 => "May", 6 => "Jun", 7 => "Jul", 8 => "Ago", 9 => "Sep", 10 => "Oct", 11 => "Nov", 12 => "Dic");
if ($rs > 0) {
	
	while ($obj = pg_fetch_object($query_grafico)){
		$series .= "{name: '".$mons[$obj->mes]." ".$obj->anio."',data: [".$obj->generadasensa.",".$obj->generadas.",".$obj->finalizadas.",".$obj->infructuosa."]},";
	}
	$series = substr($series,0,-1);
}else{
	echo 'No hay datos';
	exit();
}

?>
<!DOCTYPE html>
<html lang="es" >
<head>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta http-equiv="content-type" content="text/html; charset=UTF-8">
<title>Grafico Pexport</title>

<script type="text/javascript" src="//code.jquery.com/jquery-1.9.1.js"></script>



<script type="text/javascript">//<![CDATA[ 

$(function () {
    $('#container').highcharts({
        chart: {
            type: 'bar'
        },
        title: {
            text: 'Evolución de rendimiento por meses'
        },
        subtitle: {
            text: 'Fuente: PEXPORT S.A.C'
        },
        xAxis: {
            categories: ['Generadas Ensa','Recibidas Pexport','Finalizadas','Infructuosas'],
            title: {
                text: null
            }
        },
        yAxis: {
            min: 0,
            title: {
                text: 'Cantidad de Ordenes',
                align: 'high'
            },
            labels: {
                overflow: 'justify'
            }
        },
        tooltip: {
            valueSuffix: ' ordenes'
        },
        plotOptions: {
            bar: {
                dataLabels: {
                    enabled: true
                }
            }
        },
        /*legend: {
            layout: 'vertical',
            align: 'right',
            verticalAlign: 'top',
            x: -40,
            y: 100,
            floating: true,
            borderWidth: 1,
            backgroundColor: ((Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'),
            shadow: true
        },*/
        credits: {
            enabled: false
        },
        series: [<?php echo $series;?>]
    });
});
//]]>  

</script>


</head>
<body>
	<!-- 
	<script src="http://code.highcharts.com/highcharts.js"></script>
	<script src="http://code.highcharts.com/modules/exporting.js"></script>
	 -->
	
	<script src="../js/jquery.high/highcharts.js"></script>
	<script src="../js/jquery.high/modules/exporting.js"></script>

	<div id="container" style="min-width: 310px; max-width: 800px; height: 300px; margin: 0 auto">
	</div>
	
</body>
</html>