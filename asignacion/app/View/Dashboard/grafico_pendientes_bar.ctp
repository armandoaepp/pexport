<?php
date_default_timezone_set('America/Lima');

//require_once "../conexion.php";
require_once(APP."View/Main/conexion.ctp");

$fechaactual=date("Y-m-d");

if(isset($_GET["idini1"]))
{
  $idini1=$_GET["idini1"];
} else {
  $idini1=$fechaactual;
}


if(isset($_GET["idfin1"]))
{
  $idfin1=$_GET["idfin1"];
} else {
  $idfin1=$fechaactual;
}



$query_grafico = pg_query("
select case when me.IdEstado in (select IdEstado from ComMovTab_MaestrosEstado where EstadoSup=13) then 13 else me.IdEstado end as IdEstado, 
		case when me.IdEstado in (select IdEstado from ComMovTab_MaestrosEstado where EstadoSup=13) then 'Descargado' else me.NomEstado end as Estado,ci.IdUsuario,gp.NomPersona,gp.ApePersona,count(ci.IdEstado) as Total  from ComMovTab_Intervencion ci
inner join ComMovTab_Orden co on ci.IdOrden=co.IdOrden
inner join ComMovTab_MaestrosEstado me on ci.IdEstado=me.IdEstado
inner join ComMovTab_MaestrosEstado mesup on me.EstadoSup=mesup.IdEstado
inner join ComMovTab_Usuario cu on ci.IdUsuario = cu.IdUsuario
inner join GlobalMaster_Persona gp on cu.IdPersona = gp.IdPersona
where co.SubActividad in (select DetalleTipoIntervencion from ComMovTab_TipoIntervencion)
and ci.IdEstado not in (15) and co.Condicion='0' 
group by me.IdEstado,me.NomEstado,ci.IdUsuario,gp.NomPersona,gp.ApePersona
order by 1,3 asc
");


$rs=pg_num_rows($query_grafico);

$cuadrilla = '';
$sub_cuadrilla = '';
$series = '';
$series_drilldown = '';
$sum_cuadrillas = 0;
$sum_sub_cuadrillas = 0;
if ($rs > 0) {

	while ($obj = pg_fetch_object($query_grafico)){
	/*$arr = array();
	$arr[] = (Object) array('Cuadrilla'=>'Cuadrilla1','SubCuadrilla'=>'SubCuadrilla1','Sector'=>'Sector1','TotalAsignadas'=>4,'TotalDescargadas'=>2,'TotalExportadas'=>1,'TotalFinalizadas'=>3);
	$arr[] = (Object) array('Cuadrilla'=>'Cuadrilla1','SubCuadrilla'=>'SubCuadrilla2','Sector'=>'Sector2','TotalAsignadas'=>4,'TotalDescargadas'=>2,'TotalExportadas'=>1,'TotalFinalizadas'=>3);
	$arr[] = (Object) array('Cuadrilla'=>'Cuadrilla1','SubCuadrilla'=>'SubCuadrilla2','Sector'=>'Sector3','TotalAsignadas'=>4,'TotalDescargadas'=>2,'TotalExportadas'=>1,'TotalFinalizadas'=>3);
	$arr[] = (Object) array('Cuadrilla'=>'Cuadrilla2','SubCuadrilla'=>'SubCuadrilla3','Sector'=>'Sector4','TotalAsignadas'=>4,'TotalDescargadas'=>2,'TotalExportadas'=>1,'TotalFinalizadas'=>3);
	$arr[] = (Object) array('Cuadrilla'=>'Cuadrilla2','SubCuadrilla'=>'SubCuadrilla3','Sector'=>'Sector5','TotalAsignadas'=>4,'TotalDescargadas'=>2,'TotalExportadas'=>1,'TotalFinalizadas'=>3);
	$arr = (Object) $arr;
	
	foreach ($arr as $k=>$obj){*/
		
		if($cuadrilla!=$obj->idestado){
			$series = str_replace('%y-cuadrillas%',$sum_cuadrillas,$series);
			$sum_cuadrillas = 0;
			$series .= "{
	            name: '".$obj->estado."',
	            y: %y-cuadrillas%,
	            drilldown: 'estado-".$obj->idestado."'
	        },";
			
			$series_drilldown = str_replace(',%sub-cuadrilla%','',$series_drilldown);
			$series_drilldown .= "{
				id: 'estado-".$obj->idestado."',
	            name: '".$obj->estado."',
	            data: [%sub-cuadrilla%]
	        },";
			$cuadrilla = $obj->idestado;
		}
		if($sub_cuadrilla!=$obj->idestado."-".$obj->idusuario){
			$series_drilldown = str_replace('%y-sub-cuadrillas%',$sum_sub_cuadrillas,$series_drilldown);
			$sum_sub_cuadrillas = 0;
			
			$series_sub_cuadrilla = "{
	            name: '".$obj->nompersona."',
	            y: %y-sub-cuadrillas%,
	            drilldown: '".$obj->idestado."-".$obj->idusuario."'
	        },%sub-cuadrilla%";
			$series_drilldown = str_replace('%sub-cuadrilla%',$series_sub_cuadrilla,$series_drilldown);
			$series_drilldown .= "{
	            id: '".$obj->idestado."-".$obj->idusuario."',
	            name: '".$obj->nompersona."',
	            data: [".$obj->total."]
	        },";
			$sub_cuadrilla = $obj->idestado."-".$obj->idusuario;
		}
		
		$sum_cuadrillas += $obj->total;
		$sum_sub_cuadrillas += $obj->total;
		
	}
	$series = substr($series,0,-1);
	$series = str_replace('%y-cuadrillas%',$sum_cuadrillas,$series);
	$series_drilldown = str_replace('%y-sub-cuadrillas%',$sum_sub_cuadrillas,$series_drilldown);
	$series_drilldown = str_replace(',%sub-cuadrilla%','',$series_drilldown);
	$series_drilldown = substr($series_drilldown,0,-1);
	//echo $series;
	//echo '<br>';
	//echo $series_drilldown;
	//exit();
}else{
	echo 'No hay datos';
	exit();
}

?>
<!DOCTYPE html>
<html lang="es" >
<head>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta http-equiv="content-type" content="text/html; charset=UTF-8">
<title>Grafico Pexport</title>

<script type="text/javascript" src="//code.jquery.com/jquery-1.9.1.js"></script>



<script type="text/javascript">//<![CDATA[

$(function () {

	// Internationalization
	Highcharts.setOptions({
	    lang: {
	        drillUpText: '◁ Regresar a {series.name}'
	    }
	});

	var options = {

	    chart: {
	        height: 300
	    },
	    
	    title: {
	        text: 'Ordenes Pendientes'
	    },

	    subtitle: {
            text: 'Click en la columna para ver el detalle'
        },

	    xAxis: {
	        categories: true
	    },

	    yAxis: {
            title: {
                text: 'Cantidad de Ordenes'
            }
        },
	    
	    drilldown: {
	        series: [<?php echo $series_drilldown;?>
	     	        /*{
	            id: 'fruits',
	            name: 'Fruits',
	            data: [
	                ['Apples', 4],
	                ['Pears', 6],
	                ['Oranges', 2],
	                ['Grapes', 8]
	            ]
	        }, {
	            id: 'cars',
	            name: 'Cars',
	            data: [{
	                name: 'Toyota', 
	                y: 4,
	                drilldown: 'toyota'
	            },
	            ['Volkswagen', 3],
	            ['Opel', 5]
	            ]
	        }, {
	            id: 'toyota',
	            name: 'Toyota',
	            data: [
	                ['RAV4', 3],
	                ['Corolla', 1],
	                ['Carina', 4],
	                ['Land Cruiser', 5],
	                {
		                name: 'Toyota2', 
		                y: 4,
		                drilldown: 'toyota2'
		            }
	            ]
	        }, {
	            id: 'toyota2',
	            name: 'Toyota2',
	            data: [
	                ['RAV4xx', 3],
	                ['Corollaxx', 1]
	            ]
	        }*/]
	    },
	    
	    legend: {
	        enabled: false
	    },
	    
	    plotOptions: {
	        series: {
	            dataLabels: {
	                enabled: true
	            },
	            shadow: false
	        },
	        pie: {
	            size: '80%'
	        }
	    },

	    tooltip: {
            headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
            pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:f}</b> Ordenes<br/>'
        }, 

        credits: {
            enabled: false
        },
	    
	    series: [{
	        name: 'Ordenes Pendientes',
	        colorByPoint: true,
	        data: [<?php echo $series;?>/*{
	            name: 'Fruits',
	            y: 10,
	            drilldown: 'fruits'
	        }, {
	            name: 'Cars',
	            y: 12,
	            drilldown: 'cars'
	        }, {
	            name: 'Countries',
	            y: 8
	        }*/]
	    }]
	};

	// Column chart
	options.chart.renderTo = 'container';
	options.chart.type = 'column';
	var chart1 = new Highcharts.Chart(options);	
    
});
//]]>

</script>


</head>
<body style="text-align: center;">
	<!--
	<script src="http://code.highcharts.com/highcharts.js"></script>
	<script src="http://code.highcharts.com/modules/exporting.js"></script>
	 -->

	<script src="../js/jquery.high/highcharts.js"></script>
	<script src="../js/jquery.high/modules/exporting.js"></script>
	<script src="../js/jquery.high/modules/drilldown.js"></script>

	<div id="container" style="min-width: 300px; max-width: 800px; height: 330px; margin: 0 auto;">
	</div>

</body>
</html>
