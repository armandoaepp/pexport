<?php
date_default_timezone_set('America/Lima');

//require_once "../conexion.php";
require_once(APP."View/Main/conexion.ctp");

$mes_actual		=	(int) date('m');
$mes_anterior	=	(int) date('m',strtotime('-1 month', strtotime(date("Y-m-d"))));

$dia_actual		=	(int) date('d');
$fecha_actual	=	date('Y-m-d');

/*
sacar promedio entre cada atencion
select avg(tiempo) as tiempo_promedio from (
    select * from (select *, datediff ( ss, F2 , F1 ) as tiempo from
    (select FechaAtencion AS F1, ROW_NUMBER() OVER(order by FechaAtencion asc) as I1 from ComMovTab_Intervencion
    WHERE FechaAtencion is not null) as t11
    LEFT OUTER JOIN
    (select FechaAtencion AS F2, ROW_NUMBER() OVER(order by FechaAtencion asc) as I2 from ComMovTab_Intervencion
    WHERE FechaAtencion is not null) as t22
    on t11.I1=t22.I2+1) as t3
    ) as t4

 */

function getTiempoString($tiempo_segundos){
	$str_dias = 0;
	$str_horas = 0;
	$str_minutos = 0;
	if($tiempo_segundos>36400){
		$str_dias = floor ($tiempo_segundos/36400);
		$tiempo_segundos = $tiempo_segundos%36400;
	}
	if($tiempo_segundos>3600){
		$str_horas = floor ($tiempo_segundos/3600);
		$tiempo_segundos = $tiempo_segundos%3600;
	}
	if($tiempo_segundos>60){
		$str_minutos = floor ($tiempo_segundos/60);
		$tiempo_segundos = floor ($tiempo_segundos%60);
	}
	
	$str_horas      = str_pad( $str_horas, 2, "0", STR_PAD_LEFT );
	$str_minutos    = str_pad( $str_minutos, 2, "0", STR_PAD_LEFT );
	$tiempo_segundos= str_pad( $tiempo_segundos, 2, "0", STR_PAD_LEFT );
	
	return ($str_dias>0?$str_dias.' días ':' ').$str_horas.':'.$str_minutos.':'.$tiempo_segundos;
}

$query_dia_actual = pg_query("select 
(select count(ci.IdEstado) as TotalFin  from ComMovTab_Intervencion ci
inner join ComMovTab_Orden co on ci.IdOrden=co.IdOrden
where co.SubActividad in (select DetalleTipoIntervencion from ComMovTab_TipoIntervencion)
and ci.IdEstado in (14,15) and co.Condicion='0' and FechaAtencion>='".$fecha_actual."') as Finalizadas,
(select count(ci.IdEstado) as TotalFin  from ComMovTab_Intervencion ci
inner join ComMovTab_Orden co on ci.IdOrden=co.IdOrden
where co.SubActividad in (select DetalleTipoIntervencion from ComMovTab_TipoIntervencion)
and ci.IdEstado in (15) and co.Condicion='0' and FechaAtencion>='".$fecha_actual."') as FinalizadasEnsa,
(
select count(ci.IdEstado) as Total  from ComMovTab_Intervencion ci
inner join ComMovTab_Orden co on ci.IdOrden=co.IdOrden
where co.SubActividad in (select DetalleTipoIntervencion from ComMovTab_TipoIntervencion) and co.Condicion='0'
and FechaAsignado>='".$fecha_actual."') as Generadas,
(
select count(ci.IdEstado) as Total  from ComMovTab_Intervencion ci
inner join ComMovTab_Orden co on ci.IdOrden=co.IdOrden
where co.SubActividad in (select DetalleTipoIntervencion from ComMovTab_TipoIntervencion) and co.Condicion='0'
and created>='".$fecha_actual."') as GeneradasEnsa,
(
select count(ci.IdEstado) as TotalFin  from ComMovTab_Intervencion ci
inner join ComMovTab_Orden co on ci.IdOrden=co.IdOrden
where co.SubActividad in (select DetalleTipoIntervencion from ComMovTab_TipoIntervencion)
and ci.IdEstado not in (15) and co.Condicion='0') as Pendientes,
(
select count(ci.IdEstado) as TotalFin  from ComMovTab_Intervencion ci
inner join ComMovTab_Orden co on ci.IdOrden=co.IdOrden
inner join ComMovTab_Detalle_Intervencion DI on DI.IdIntervencion=ci.IdIntervencion
where co.SubActividad in (select DetalleTipoIntervencion from ComMovTab_TipoIntervencion)
and co.Condicion='0' and FechaAtencion>='".$fecha_actual."' and DI.IdMaestrosIntervencion in (
select IdMaestrosIntervencion from ComMovTab_MaestrosIntervencion
where IdentificadorMaestroIntervencion like 'LabelInfructuosa%')
) as Infructuosas,
(
select avg(tiempo) as tiempo_promedio from (
select FechaAsignado, FechaAtencion, ((DATE_PART('day', FechaAtencion - FechaAsignado) * 24 + 
                DATE_PART('hour', FechaAtencion - FechaAsignado)) * 60 +
                DATE_PART('minute', FechaAtencion - FechaAsignado)) * 60 +
                DATE_PART('second', FechaAtencion - FechaAsignado) as tiempo
from ComMovTab_Intervencion where FechaAtencion is not null and FechaAtencion>='".$fecha_actual."') as T
) as TiempoPromedio,
(
select count(ci.IdEstado) as TotalFin  from ComMovTab_Intervencion ci
inner join ComMovTab_Orden co on ci.IdOrden=co.IdOrden
where co.SubActividad in (select DetalleTipoIntervencion from ComMovTab_TipoIntervencion)
and ci.IdEstado not in (15) and co.Condicion='0' and DATE_PART('day',NOW()-cast(FechaGeneracion as timestamp))>2) AS pendientes_mayor_2_dias,
(select count(ci.IdEstado) as TotalFin  from ComMovTab_Intervencion ci
inner join ComMovTab_Orden co on ci.IdOrden=co.IdOrden
where co.SubActividad in (select DetalleTipoIntervencion from ComMovTab_TipoIntervencion)
and ci.IdEstado in (14) and co.Condicion='0' and DATE_PART('day',NOW()-cast(FechaGeneracion as timestamp))>2) AS pendientes_exportado_2_dias ");

$count_rows=pg_num_rows($query_dia_actual);
$arr_datos = array();
while ($obj_dia_actual = pg_fetch_object($query_dia_actual)){
	$dia_actual_generadas_ensa = $obj_dia_actual->generadasensa;
	$dia_actual_generadas = $obj_dia_actual->generadas;
	$dia_actual_finalizadas = $obj_dia_actual->finalizadas;
	$dia_actual_finalizadas_ensa = $obj_dia_actual->finalizadasensa;
	$dia_actual_pendientes = $obj_dia_actual->pendientes;
	$dia_actual_infructuosas = $obj_dia_actual->infructuosas;
	$dia_actual_tiempo_promedio = getTiempoString($obj_dia_actual->tiempopromedio);
	$dia_actual_pendientes_mayor_2_dias = $obj_dia_actual->pendientes_mayor_2_dias;
	$dia_actual_pendientes_exportado_2_dias = $obj_dia_actual->pendientes_exportado_2_dias;
}

$query_resumen_2_ultimos_meses = pg_query("select consulta1.anio as Anio,consulta1.mes as Mes,
consulta1.Total as Generadas,
consulta2.TotalFin as Finalizadas,
consulta22.TotalFin as FinalizadasEnsa,
((consulta1.Total)-consulta2.TotalFin) as Pendientes,
((consulta2.TotalFin*100)/consulta1.Total) as Porcentaje,
consulta3.TotalFin as Infructuosa,
consulta4.tiempo_promedio as TiempoPromedio,
consulta5.Total as GeneradasEnsa from
(
select extract('year' from FechaAsignado) as anio, extract('month' from FechaAsignado) as mes,count(ci.IdEstado) as Total  from ComMovTab_Intervencion ci
inner join ComMovTab_Orden co on ci.IdOrden=co.IdOrden
where co.SubActividad in (select DetalleTipoIntervencion from ComMovTab_TipoIntervencion) and co.Condicion='0'
and extract('day' from FechaAsignado)<=".$dia_actual."
group by extract('year' from FechaAsignado), extract('month' from FechaAsignado)) as consulta1
left outer join
(
select extract('year' from FechaAtencion) as anio, extract('month' from FechaAtencion) as mes,count(ci.IdEstado) as TotalFin  from ComMovTab_Intervencion ci
inner join ComMovTab_Orden co on ci.IdOrden=co.IdOrden
where co.SubActividad in (select DetalleTipoIntervencion from ComMovTab_TipoIntervencion)
and ci.IdEstado in (14,15) and co.Condicion='0' and extract('day' from FechaAtencion)<=".$dia_actual."
group by extract('year' from FechaAtencion), extract('month' from FechaAtencion)) as consulta2 on consulta1.anio = consulta2.anio and consulta1.mes = consulta2.mes
left outer join
(
select extract('year' from FechaAtencion) as anio, extract('month' from FechaAtencion) as mes,count(ci.IdEstado) as TotalFin  from ComMovTab_Intervencion ci
inner join ComMovTab_Orden co on ci.IdOrden=co.IdOrden
where co.SubActividad in (select DetalleTipoIntervencion from ComMovTab_TipoIntervencion)
and ci.IdEstado=15 and co.Condicion='0' and extract('day' from FechaAtencion)<=".$dia_actual."
group by extract('year' from FechaAtencion), extract('month' from FechaAtencion)) as consulta22 on consulta1.anio = consulta22.anio and consulta1.mes = consulta22.mes		
left outer join
(
select extract('year' from FechaAtencion) as anio, extract('month' from FechaAtencion) as mes,count(ci.IdEstado) as TotalFin  from ComMovTab_Intervencion ci
inner join ComMovTab_Orden co on ci.IdOrden=co.IdOrden
inner join ComMovTab_Detalle_Intervencion DI on DI.IdIntervencion=ci.IdIntervencion
where co.SubActividad in (select DetalleTipoIntervencion from ComMovTab_TipoIntervencion)
and co.Condicion='0' and extract('day' from FechaAtencion)<=".$dia_actual." and DI.IdMaestrosIntervencion in (
select IdMaestrosIntervencion from ComMovTab_MaestrosIntervencion
where IdentificadorMaestroIntervencion like 'LabelInfructuosa%')
group by extract('year' from FechaAtencion), extract('month' from FechaAtencion)) as consulta3 on consulta1.anio = consulta3.anio and consulta1.mes = consulta3.mes
left outer join
(
select extract('year' from FechaAtencion) as anio, extract('month' from FechaAtencion) as mes,avg(tiempo) as tiempo_promedio from (
select FechaAsignado, FechaAtencion, ((DATE_PART('day', FechaAtencion - FechaAsignado) * 24 + 
                DATE_PART('hour', FechaAtencion - FechaAsignado)) * 60 +
                DATE_PART('minute', FechaAtencion - FechaAsignado)) * 60 +
                DATE_PART('second', FechaAtencion - FechaAsignado) as tiempo
from ComMovTab_Intervencion where FechaAtencion is not null and extract('day' from FechaAtencion)<=".$dia_actual.") as T
group by extract('year' from FechaAtencion), extract('month' from FechaAtencion)) as consulta4 on consulta1.anio = consulta4.anio and consulta1.mes = consulta4.mes
left outer join
(
select extract('year' from created) as anio, extract('month' from created) as mes,count(ci.IdEstado) as Total  from ComMovTab_Intervencion ci
inner join ComMovTab_Orden co on ci.IdOrden=co.IdOrden
where co.SubActividad in (select DetalleTipoIntervencion from ComMovTab_TipoIntervencion) and co.Condicion='0'
and extract('day' from created)<=".$dia_actual."
group by extract('year' from created), extract('month' from created)) as consulta5 on consulta1.anio = consulta5.anio and consulta1.mes = consulta5.mes 
 order by 1 desc,2 desc limit 2");

$count_rows=pg_num_rows($query_resumen_2_ultimos_meses);
$arr_datos = array();
while ($obj_resumen_2_ultimos_meses = pg_fetch_object($query_resumen_2_ultimos_meses)){
	$arr_datos[$obj_resumen_2_ultimos_meses->mes]['Mes'] = $obj_resumen_2_ultimos_meses->mes;
	$arr_datos[$obj_resumen_2_ultimos_meses->mes]['GeneradasEnsa'] = $obj_resumen_2_ultimos_meses->generadasensa;
	$arr_datos[$obj_resumen_2_ultimos_meses->mes]['Generadas'] = $obj_resumen_2_ultimos_meses->generadas;
	$arr_datos[$obj_resumen_2_ultimos_meses->mes]['Finalizadas'] = $obj_resumen_2_ultimos_meses->finalizadas;
	$arr_datos[$obj_resumen_2_ultimos_meses->mes]['FinalizadasEnsa'] = $obj_resumen_2_ultimos_meses->finalizadasensa;
	$arr_datos[$obj_resumen_2_ultimos_meses->mes]['Pendientes'] = $obj_resumen_2_ultimos_meses->pendientes;
	$arr_datos[$obj_resumen_2_ultimos_meses->mes]['Infructuosa'] = $obj_resumen_2_ultimos_meses->infructuosa;
	$arr_datos[$obj_resumen_2_ultimos_meses->mes]['TiempoProm'] = $obj_resumen_2_ultimos_meses->tiempopromedio;

	$arr_datos[$obj_resumen_2_ultimos_meses->mes]['TiempoPromedio'] = getTiempoString($obj_resumen_2_ultimos_meses->tiempopromedio);
}

if(@$arr_datos[$mes_actual]['GeneradasEnsa']>@$arr_datos[$mes_anterior]['GeneradasEnsa']){
	$icon_generadas_ensa = "<img src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAB8ElEQVR42mNgoDb4/OfD/9UPpv6vv5z4v+C83/+sM17/c0+F/595o/f/u+/v/uPVfOHrgf8114L+L33f+H/j997/s98V/O95Hvu/5UHI/5wzLv+dtuv+3/Vy23+cmuvvRvw/9H/h/6U/qv53fgj93/DM93/VA/f/Jbcd/xdctfmffcH8v+kaqf/bnqMZAnJ22RW///v+zP0/52f+/76vUXADah97gQ3IvWz1P/OcKRgrLZNG9c6K+1P+z3lZDrcZhFtfB4E1V9xz/V980wFsQNoZ4/9xJ/T/W62T+d9+oh1hQOm5iP8bPvX9b30ZCMfozgfZnAjUHHlM+7/HdpX/NkusEQYknHACuwCkAWQrzGYQBmkGYZjtQYc0/1tvlv0vN1kSYUDkYdv/Ex6lgG1DxjDNyLb77FcDB6T0BAmEATGHvP933ouCa4BhUKiDMLJmj70q/7Xmi/63mm2JMKD7Yis4nmGhjIzjoJoDD2qANYNsl53C+79lbwvCAFCUGG5QBfsTZBsMgzSC/AyzGeR3ldlC/yXbRDFT5bon68DxC9OErNFpjyLYZpBmgWbO/+uurcOeGtc8WPNfbr44OJ6dtyuCMUij1kJRsLPFW0Vwa4aBF19f/K/eW/3faLbBf9FuATA2mqz/v2ZrzX+QHNVzLwAjy7LsIPIjLQAAAABJRU5ErkJggg=='>";
}elseif(@$arr_datos[$mes_actual]['GeneradasEnsa']<@$arr_datos[$mes_anterior]['GeneradasEnsa']){
	$icon_generadas_ensa = "<img src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAABrElEQVR42q2TyytEURzH/QFW1JRHIxsTUiNWHgtCFOWRPEpZyAJZkKSZhccgsVBIFqRkZdKUZCFFNLEQJUnJQk0eGcQCk+bj/O6dyZ3rsXLr272dcz7f3/d3zrlRUf/98PIIK1Mw0gzOKnCU6+/FUYJPD/wNH23DcB14F+DcA7sz4B5ScK8+XmuDvXV+h6da4GkfzpZhywWePljqhLlWmGiEwWoCFdbvJlpsNcm1MjmZVwnGfzaQdpT8RdbIdrSe16fhcFEHRWsOtRfdsNCuJ5MWxKCrgMeadPzTowYDp5o8dutQWAKb4tNTqhm8NmdxWZVnMFCDbM3ogFECTzZFVKctl7eGDE6zEwwGalDbbQHCkthSOQyHqstaaeHYHmcw6CjSj0oAoyS2CaY1i6vCJI7Kcr4MgrMD+mK5MEYJaIKlusS/GHMZDNSRvFWmfFUzgyE4UJ/GZW48ezYLAf+96S5srvJckhgJhUCRFBDYmxyNf83z820Mbrjx5cVpMeWoZLfl26cujsTeSbH8Doefj7sbfCMOToozOUyN5cAWw0G+nbN+J+93t/z73/sJB/zttLkU5gUAAAAASUVORK5CYII='>";
}else{
	$icon_generadas_ensa = "<img src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAABpUlEQVR42q2TyytEURzH/QFWRCFTVhJF2WFla0XJwkKxlJ2VRxI1lA2RlRRWnqVGiRqPmJJXYqKGPEOGYdIwg/Nxzpm55ozByq1v3c6538fve85NSPjvhzcfnPTBYQ3slyP2ymC3Ajx21N7fZN8S4qgKAkO8M4N4HQBvB9w26nXhzIabOX4lc1qHn3WCjMFHJ7w0S4EGvc5JtRYJztviRXRsGTeAU5KH+KALQh0xAsJTiXCXadyN22S6h6iImlk89krX4bCz5f7YKN3qowK6j1IeHLncu+yGgCopNBEmWVBkI75KqMgKgeVCPCMlpkBppLCGWFzXaugC3WF3tovxL+aw05NmCMhF3XaE8AVVXGR2y53NQj1CrIDakEelo5pQsY3o2kgKnI3a2OovigqIg/boxyYsokH2ObJw96fgcXQaAvJI1FxWy3HECPllKY/j4XRW2lIJPXu/3YWLaZ5mM+IcLShnRV5rTcS7O/3zbRSnk5yPpOk0odV87ajeL8czdWxnU+rvZOt5f77haqGZvcECNuzJuNqTcHUX4J5qIei/5d//3k/ScyAkq+CgnAAAAABJRU5ErkJggg=='>";
}

if(@$arr_datos[$mes_actual]['Generadas']>@$arr_datos[$mes_anterior]['Generadas']){
	$icon_generadas = "<img src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAB8ElEQVR42mNgoDb4/OfD/9UPpv6vv5z4v+C83/+sM17/c0+F/595o/f/u+/v/uPVfOHrgf8114L+L33f+H/j997/s98V/O95Hvu/5UHI/5wzLv+dtuv+3/Vy23+cmuvvRvw/9H/h/6U/qv53fgj93/DM93/VA/f/Jbcd/xdctfmffcH8v+kaqf/bnqMZAnJ22RW///v+zP0/52f+/76vUXADah97gQ3IvWz1P/OcKRgrLZNG9c6K+1P+z3lZDrcZhFtfB4E1V9xz/V980wFsQNoZ4/9xJ/T/W62T+d9+oh1hQOm5iP8bPvX9b30ZCMfozgfZnAjUHHlM+7/HdpX/NkusEQYknHACuwCkAWQrzGYQBmkGYZjtQYc0/1tvlv0vN1kSYUDkYdv/Ex6lgG1DxjDNyLb77FcDB6T0BAmEATGHvP933ouCa4BhUKiDMLJmj70q/7Xmi/63mm2JMKD7Yis4nmGhjIzjoJoDD2qANYNsl53C+79lbwvCAFCUGG5QBfsTZBsMgzSC/AyzGeR3ldlC/yXbRDFT5bon68DxC9OErNFpjyLYZpBmgWbO/+uurcOeGtc8WPNfbr44OJ6dtyuCMUij1kJRsLPFW0Vwa4aBF19f/K/eW/3faLbBf9FuATA2mqz/v2ZrzX+QHNVzLwAjy7LsIPIjLQAAAABJRU5ErkJggg=='>";
}elseif(@$arr_datos[$mes_actual]['Generadas']<@$arr_datos[$mes_anterior]['Generadas']){
	$icon_generadas = "<img src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAABrElEQVR42q2TyytEURzH/QFW1JRHIxsTUiNWHgtCFOWRPEpZyAJZkKSZhccgsVBIFqRkZdKUZCFFNLEQJUnJQk0eGcQCk+bj/O6dyZ3rsXLr272dcz7f3/d3zrlRUf/98PIIK1Mw0gzOKnCU6+/FUYJPD/wNH23DcB14F+DcA7sz4B5ScK8+XmuDvXV+h6da4GkfzpZhywWePljqhLlWmGiEwWoCFdbvJlpsNcm1MjmZVwnGfzaQdpT8RdbIdrSe16fhcFEHRWsOtRfdsNCuJ5MWxKCrgMeadPzTowYDp5o8dutQWAKb4tNTqhm8NmdxWZVnMFCDbM3ogFECTzZFVKctl7eGDE6zEwwGalDbbQHCkthSOQyHqstaaeHYHmcw6CjSj0oAoyS2CaY1i6vCJI7Kcr4MgrMD+mK5MEYJaIKlusS/GHMZDNSRvFWmfFUzgyE4UJ/GZW48ezYLAf+96S5srvJckhgJhUCRFBDYmxyNf83z820Mbrjx5cVpMeWoZLfl26cujsTeSbH8Doefj7sbfCMOToozOUyN5cAWw0G+nbN+J+93t/z73/sJB/zttLkU5gUAAAAASUVORK5CYII='>";
}else{
	$icon_generadas = "<img src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAABpUlEQVR42q2TyytEURzH/QFWRCFTVhJF2WFla0XJwkKxlJ2VRxI1lA2RlRRWnqVGiRqPmJJXYqKGPEOGYdIwg/Nxzpm55ozByq1v3c6538fve85NSPjvhzcfnPTBYQ3slyP2ymC3Ajx21N7fZN8S4qgKAkO8M4N4HQBvB9w26nXhzIabOX4lc1qHn3WCjMFHJ7w0S4EGvc5JtRYJztviRXRsGTeAU5KH+KALQh0xAsJTiXCXadyN22S6h6iImlk89krX4bCz5f7YKN3qowK6j1IeHLncu+yGgCopNBEmWVBkI75KqMgKgeVCPCMlpkBppLCGWFzXaugC3WF3tovxL+aw05NmCMhF3XaE8AVVXGR2y53NQj1CrIDakEelo5pQsY3o2kgKnI3a2OovigqIg/boxyYsokH2ObJw96fgcXQaAvJI1FxWy3HECPllKY/j4XRW2lIJPXu/3YWLaZ5mM+IcLShnRV5rTcS7O/3zbRSnk5yPpOk0odV87ajeL8czdWxnU+rvZOt5f77haqGZvcECNuzJuNqTcHUX4J5qIei/5d//3k/ScyAkq+CgnAAAAABJRU5ErkJggg=='>";
}

if(@$arr_datos[$mes_actual]['Finalizadas']>@$arr_datos[$mes_anterior]['Finalizadas']){
	$icon_finalizadas = "<img src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAB8ElEQVR42mNgoDb4/OfD/9UPpv6vv5z4v+C83/+sM17/c0+F/595o/f/u+/v/uPVfOHrgf8114L+L33f+H/j997/s98V/O95Hvu/5UHI/5wzLv+dtuv+3/Vy23+cmuvvRvw/9H/h/6U/qv53fgj93/DM93/VA/f/Jbcd/xdctfmffcH8v+kaqf/bnqMZAnJ22RW///v+zP0/52f+/76vUXADah97gQ3IvWz1P/OcKRgrLZNG9c6K+1P+z3lZDrcZhFtfB4E1V9xz/V980wFsQNoZ4/9xJ/T/W62T+d9+oh1hQOm5iP8bPvX9b30ZCMfozgfZnAjUHHlM+7/HdpX/NkusEQYknHACuwCkAWQrzGYQBmkGYZjtQYc0/1tvlv0vN1kSYUDkYdv/Ex6lgG1DxjDNyLb77FcDB6T0BAmEATGHvP933ouCa4BhUKiDMLJmj70q/7Xmi/63mm2JMKD7Yis4nmGhjIzjoJoDD2qANYNsl53C+79lbwvCAFCUGG5QBfsTZBsMgzSC/AyzGeR3ldlC/yXbRDFT5bon68DxC9OErNFpjyLYZpBmgWbO/+uurcOeGtc8WPNfbr44OJ6dtyuCMUij1kJRsLPFW0Vwa4aBF19f/K/eW/3faLbBf9FuATA2mqz/v2ZrzX+QHNVzLwAjy7LsIPIjLQAAAABJRU5ErkJggg=='>";
}elseif(@$arr_datos[$mes_actual]['Finalizadas']<@$arr_datos[$mes_anterior]['Finalizadas']){
	$icon_finalizadas = "<img src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAABrElEQVR42q2TyytEURzH/QFW1JRHIxsTUiNWHgtCFOWRPEpZyAJZkKSZhccgsVBIFqRkZdKUZCFFNLEQJUnJQk0eGcQCk+bj/O6dyZ3rsXLr272dcz7f3/d3zrlRUf/98PIIK1Mw0gzOKnCU6+/FUYJPD/wNH23DcB14F+DcA7sz4B5ScK8+XmuDvXV+h6da4GkfzpZhywWePljqhLlWmGiEwWoCFdbvJlpsNcm1MjmZVwnGfzaQdpT8RdbIdrSe16fhcFEHRWsOtRfdsNCuJ5MWxKCrgMeadPzTowYDp5o8dutQWAKb4tNTqhm8NmdxWZVnMFCDbM3ogFECTzZFVKctl7eGDE6zEwwGalDbbQHCkthSOQyHqstaaeHYHmcw6CjSj0oAoyS2CaY1i6vCJI7Kcr4MgrMD+mK5MEYJaIKlusS/GHMZDNSRvFWmfFUzgyE4UJ/GZW48ezYLAf+96S5srvJckhgJhUCRFBDYmxyNf83z820Mbrjx5cVpMeWoZLfl26cujsTeSbH8Doefj7sbfCMOToozOUyN5cAWw0G+nbN+J+93t/z73/sJB/zttLkU5gUAAAAASUVORK5CYII='>";
}else{
	$icon_finalizadas = "<img src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAABpUlEQVR42q2TyytEURzH/QFWRCFTVhJF2WFla0XJwkKxlJ2VRxI1lA2RlRRWnqVGiRqPmJJXYqKGPEOGYdIwg/Nxzpm55ozByq1v3c6538fve85NSPjvhzcfnPTBYQ3slyP2ymC3Ajx21N7fZN8S4qgKAkO8M4N4HQBvB9w26nXhzIabOX4lc1qHn3WCjMFHJ7w0S4EGvc5JtRYJztviRXRsGTeAU5KH+KALQh0xAsJTiXCXadyN22S6h6iImlk89krX4bCz5f7YKN3qowK6j1IeHLncu+yGgCopNBEmWVBkI75KqMgKgeVCPCMlpkBppLCGWFzXaugC3WF3tovxL+aw05NmCMhF3XaE8AVVXGR2y53NQj1CrIDakEelo5pQsY3o2kgKnI3a2OovigqIg/boxyYsokH2ObJw96fgcXQaAvJI1FxWy3HECPllKY/j4XRW2lIJPXu/3YWLaZ5mM+IcLShnRV5rTcS7O/3zbRSnk5yPpOk0odV87ajeL8czdWxnU+rvZOt5f77haqGZvcECNuzJuNqTcHUX4J5qIei/5d//3k/ScyAkq+CgnAAAAABJRU5ErkJggg=='>";
}

if(@$arr_datos[$mes_actual]['FinalizadasEnsa']>@$arr_datos[$mes_anterior]['FinalizadasEnsa']){
	$icon_finalizadas = "<img src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAB8ElEQVR42mNgoDb4/OfD/9UPpv6vv5z4v+C83/+sM17/c0+F/595o/f/u+/v/uPVfOHrgf8114L+L33f+H/j997/s98V/O95Hvu/5UHI/5wzLv+dtuv+3/Vy23+cmuvvRvw/9H/h/6U/qv53fgj93/DM93/VA/f/Jbcd/xdctfmffcH8v+kaqf/bnqMZAnJ22RW///v+zP0/52f+/76vUXADah97gQ3IvWz1P/OcKRgrLZNG9c6K+1P+z3lZDrcZhFtfB4E1V9xz/V980wFsQNoZ4/9xJ/T/W62T+d9+oh1hQOm5iP8bPvX9b30ZCMfozgfZnAjUHHlM+7/HdpX/NkusEQYknHACuwCkAWQrzGYQBmkGYZjtQYc0/1tvlv0vN1kSYUDkYdv/Ex6lgG1DxjDNyLb77FcDB6T0BAmEATGHvP933ouCa4BhUKiDMLJmj70q/7Xmi/63mm2JMKD7Yis4nmGhjIzjoJoDD2qANYNsl53C+79lbwvCAFCUGG5QBfsTZBsMgzSC/AyzGeR3ldlC/yXbRDFT5bon68DxC9OErNFpjyLYZpBmgWbO/+uurcOeGtc8WPNfbr44OJ6dtyuCMUij1kJRsLPFW0Vwa4aBF19f/K/eW/3faLbBf9FuATA2mqz/v2ZrzX+QHNVzLwAjy7LsIPIjLQAAAABJRU5ErkJggg=='>";
}elseif(@$arr_datos[$mes_actual]['FinalizadasEnsa']<@$arr_datos[$mes_anterior]['FinalizadasEnsa']){
	$icon_finalizadas = "<img src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAABrElEQVR42q2TyytEURzH/QFW1JRHIxsTUiNWHgtCFOWRPEpZyAJZkKSZhccgsVBIFqRkZdKUZCFFNLEQJUnJQk0eGcQCk+bj/O6dyZ3rsXLr272dcz7f3/d3zrlRUf/98PIIK1Mw0gzOKnCU6+/FUYJPD/wNH23DcB14F+DcA7sz4B5ScK8+XmuDvXV+h6da4GkfzpZhywWePljqhLlWmGiEwWoCFdbvJlpsNcm1MjmZVwnGfzaQdpT8RdbIdrSe16fhcFEHRWsOtRfdsNCuJ5MWxKCrgMeadPzTowYDp5o8dutQWAKb4tNTqhm8NmdxWZVnMFCDbM3ogFECTzZFVKctl7eGDE6zEwwGalDbbQHCkthSOQyHqstaaeHYHmcw6CjSj0oAoyS2CaY1i6vCJI7Kcr4MgrMD+mK5MEYJaIKlusS/GHMZDNSRvFWmfFUzgyE4UJ/GZW48ezYLAf+96S5srvJckhgJhUCRFBDYmxyNf83z820Mbrjx5cVpMeWoZLfl26cujsTeSbH8Doefj7sbfCMOToozOUyN5cAWw0G+nbN+J+93t/z73/sJB/zttLkU5gUAAAAASUVORK5CYII='>";
}else{
	$icon_finalizadas = "<img src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAABpUlEQVR42q2TyytEURzH/QFWRCFTVhJF2WFla0XJwkKxlJ2VRxI1lA2RlRRWnqVGiRqPmJJXYqKGPEOGYdIwg/Nxzpm55ozByq1v3c6538fve85NSPjvhzcfnPTBYQ3slyP2ymC3Ajx21N7fZN8S4qgKAkO8M4N4HQBvB9w26nXhzIabOX4lc1qHn3WCjMFHJ7w0S4EGvc5JtRYJztviRXRsGTeAU5KH+KALQh0xAsJTiXCXadyN22S6h6iImlk89krX4bCz5f7YKN3qowK6j1IeHLncu+yGgCopNBEmWVBkI75KqMgKgeVCPCMlpkBppLCGWFzXaugC3WF3tovxL+aw05NmCMhF3XaE8AVVXGR2y53NQj1CrIDakEelo5pQsY3o2kgKnI3a2OovigqIg/boxyYsokH2ObJw96fgcXQaAvJI1FxWy3HECPllKY/j4XRW2lIJPXu/3YWLaZ5mM+IcLShnRV5rTcS7O/3zbRSnk5yPpOk0odV87ajeL8czdWxnU+rvZOt5f77haqGZvcECNuzJuNqTcHUX4J5qIei/5d//3k/ScyAkq+CgnAAAAABJRU5ErkJggg=='>";
}

if(@$arr_datos[$mes_actual]['Infructuosa']>@$arr_datos[$mes_anterior]['Infructuosa']){
	$icon_infructuosas = "<img src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAB8ElEQVR42mNgoDb4/OfD/9UPpv6vv5z4v+C83/+sM17/c0+F/595o/f/u+/v/uPVfOHrgf8114L+L33f+H/j997/s98V/O95Hvu/5UHI/5wzLv+dtuv+3/Vy23+cmuvvRvw/9H/h/6U/qv53fgj93/DM93/VA/f/Jbcd/xdctfmffcH8v+kaqf/bnqMZAnJ22RW///v+zP0/52f+/76vUXADah97gQ3IvWz1P/OcKRgrLZNG9c6K+1P+z3lZDrcZhFtfB4E1V9xz/V980wFsQNoZ4/9xJ/T/W62T+d9+oh1hQOm5iP8bPvX9b30ZCMfozgfZnAjUHHlM+7/HdpX/NkusEQYknHACuwCkAWQrzGYQBmkGYZjtQYc0/1tvlv0vN1kSYUDkYdv/Ex6lgG1DxjDNyLb77FcDB6T0BAmEATGHvP933ouCa4BhUKiDMLJmj70q/7Xmi/63mm2JMKD7Yis4nmGhjIzjoJoDD2qANYNsl53C+79lbwvCAFCUGG5QBfsTZBsMgzSC/AyzGeR3ldlC/yXbRDFT5bon68DxC9OErNFpjyLYZpBmgWbO/+uurcOeGtc8WPNfbr44OJ6dtyuCMUij1kJRsLPFW0Vwa4aBF19f/K/eW/3faLbBf9FuATA2mqz/v2ZrzX+QHNVzLwAjy7LsIPIjLQAAAABJRU5ErkJggg=='>";
}elseif(@$arr_datos[$mes_actual]['Infructuosa']<@$arr_datos[$mes_anterior]['Infructuosa']){
	$icon_infructuosas = "<img src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAABrElEQVR42q2TyytEURzH/QFW1JRHIxsTUiNWHgtCFOWRPEpZyAJZkKSZhccgsVBIFqRkZdKUZCFFNLEQJUnJQk0eGcQCk+bj/O6dyZ3rsXLr272dcz7f3/d3zrlRUf/98PIIK1Mw0gzOKnCU6+/FUYJPD/wNH23DcB14F+DcA7sz4B5ScK8+XmuDvXV+h6da4GkfzpZhywWePljqhLlWmGiEwWoCFdbvJlpsNcm1MjmZVwnGfzaQdpT8RdbIdrSe16fhcFEHRWsOtRfdsNCuJ5MWxKCrgMeadPzTowYDp5o8dutQWAKb4tNTqhm8NmdxWZVnMFCDbM3ogFECTzZFVKctl7eGDE6zEwwGalDbbQHCkthSOQyHqstaaeHYHmcw6CjSj0oAoyS2CaY1i6vCJI7Kcr4MgrMD+mK5MEYJaIKlusS/GHMZDNSRvFWmfFUzgyE4UJ/GZW48ezYLAf+96S5srvJckhgJhUCRFBDYmxyNf83z820Mbrjx5cVpMeWoZLfl26cujsTeSbH8Doefj7sbfCMOToozOUyN5cAWw0G+nbN+J+93t/z73/sJB/zttLkU5gUAAAAASUVORK5CYII='>";
}else{
	$icon_infructuosas = "<img src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAABpUlEQVR42q2TyytEURzH/QFWRCFTVhJF2WFla0XJwkKxlJ2VRxI1lA2RlRRWnqVGiRqPmJJXYqKGPEOGYdIwg/Nxzpm55ozByq1v3c6538fve85NSPjvhzcfnPTBYQ3slyP2ymC3Ajx21N7fZN8S4qgKAkO8M4N4HQBvB9w26nXhzIabOX4lc1qHn3WCjMFHJ7w0S4EGvc5JtRYJztviRXRsGTeAU5KH+KALQh0xAsJTiXCXadyN22S6h6iImlk89krX4bCz5f7YKN3qowK6j1IeHLncu+yGgCopNBEmWVBkI75KqMgKgeVCPCMlpkBppLCGWFzXaugC3WF3tovxL+aw05NmCMhF3XaE8AVVXGR2y53NQj1CrIDakEelo5pQsY3o2kgKnI3a2OovigqIg/boxyYsokH2ObJw96fgcXQaAvJI1FxWy3HECPllKY/j4XRW2lIJPXu/3YWLaZ5mM+IcLShnRV5rTcS7O/3zbRSnk5yPpOk0odV87ajeL8czdWxnU+rvZOt5f77haqGZvcECNuzJuNqTcHUX4J5qIei/5d//3k/ScyAkq+CgnAAAAABJRU5ErkJggg=='>";
}

if(@$arr_datos[$mes_actual]['TiempoProm']>@$arr_datos[$mes_anterior]['TiempoProm']){
	$icon_tiempo_promedio = "<img src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAABrElEQVR42q2TyytEURzH/QFW1JRHIxsTUiNWHgtCFOWRPEpZyAJZkKSZhccgsVBIFqRkZdKUZCFFNLEQJUnJQk0eGcQCk+bj/O6dyZ3rsXLr272dcz7f3/d3zrlRUf/98PIIK1Mw0gzOKnCU6+/FUYJPD/wNH23DcB14F+DcA7sz4B5ScK8+XmuDvXV+h6da4GkfzpZhywWePljqhLlWmGiEwWoCFdbvJlpsNcm1MjmZVwnGfzaQdpT8RdbIdrSe16fhcFEHRWsOtRfdsNCuJ5MWxKCrgMeadPzTowYDp5o8dutQWAKb4tNTqhm8NmdxWZVnMFCDbM3ogFECTzZFVKctl7eGDE6zEwwGalDbbQHCkthSOQyHqstaaeHYHmcw6CjSj0oAoyS2CaY1i6vCJI7Kcr4MgrMD+mK5MEYJaIKlusS/GHMZDNSRvFWmfFUzgyE4UJ/GZW48ezYLAf+96S5srvJckhgJhUCRFBDYmxyNf83z820Mbrjx5cVpMeWoZLfl26cujsTeSbH8Doefj7sbfCMOToozOUyN5cAWw0G+nbN+J+93t/z73/sJB/zttLkU5gUAAAAASUVORK5CYII='>";
}elseif(@$arr_datos[$mes_actual]['TiempoProm']<@$arr_datos[$mes_anterior]['TiempoProm']){
	$icon_tiempo_promedio = "<img src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAB8ElEQVR42mNgoDb4/OfD/9UPpv6vv5z4v+C83/+sM17/c0+F/595o/f/u+/v/uPVfOHrgf8114L+L33f+H/j997/s98V/O95Hvu/5UHI/5wzLv+dtuv+3/Vy23+cmuvvRvw/9H/h/6U/qv53fgj93/DM93/VA/f/Jbcd/xdctfmffcH8v+kaqf/bnqMZAnJ22RW///v+zP0/52f+/76vUXADah97gQ3IvWz1P/OcKRgrLZNG9c6K+1P+z3lZDrcZhFtfB4E1V9xz/V980wFsQNoZ4/9xJ/T/W62T+d9+oh1hQOm5iP8bPvX9b30ZCMfozgfZnAjUHHlM+7/HdpX/NkusEQYknHACuwCkAWQrzGYQBmkGYZjtQYc0/1tvlv0vN1kSYUDkYdv/Ex6lgG1DxjDNyLb77FcDB6T0BAmEATGHvP933ouCa4BhUKiDMLJmj70q/7Xmi/63mm2JMKD7Yis4nmGhjIzjoJoDD2qANYNsl53C+79lbwvCAFCUGG5QBfsTZBsMgzSC/AyzGeR3ldlC/yXbRDFT5bon68DxC9OErNFpjyLYZpBmgWbO/+uurcOeGtc8WPNfbr44OJ6dtyuCMUij1kJRsLPFW0Vwa4aBF19f/K/eW/3faLbBf9FuATA2mqz/v2ZrzX+QHNVzLwAjy7LsIPIjLQAAAABJRU5ErkJggg=='>";
}else{
	$icon_tiempo_promedio = "<img src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAABpUlEQVR42q2TyytEURzH/QFWRCFTVhJF2WFla0XJwkKxlJ2VRxI1lA2RlRRWnqVGiRqPmJJXYqKGPEOGYdIwg/Nxzpm55ozByq1v3c6538fve85NSPjvhzcfnPTBYQ3slyP2ymC3Ajx21N7fZN8S4qgKAkO8M4N4HQBvB9w26nXhzIabOX4lc1qHn3WCjMFHJ7w0S4EGvc5JtRYJztviRXRsGTeAU5KH+KALQh0xAsJTiXCXadyN22S6h6iImlk89krX4bCz5f7YKN3qowK6j1IeHLncu+yGgCopNBEmWVBkI75KqMgKgeVCPCMlpkBppLCGWFzXaugC3WF3tovxL+aw05NmCMhF3XaE8AVVXGR2y53NQj1CrIDakEelo5pQsY3o2kgKnI3a2OovigqIg/boxyYsokH2ObJw96fgcXQaAvJI1FxWy3HECPllKY/j4XRW2lIJPXu/3YWLaZ5mM+IcLShnRV5rTcS7O/3zbRSnk5yPpOk0odV87ajeL8czdWxnU+rvZOt5f77haqGZvcECNuzJuNqTcHUX4J5qIei/5d//3k/ScyAkq+CgnAAAAABJRU5ErkJggg=='>";
}

$lista="

    <h3 class='text-center'>Dashboard - Tablero de Control Actividades Comerciales</h3>
    
      <div class='row'>

        <div class='col-sm-6 col-md-6'>


          <div class='block-flat'>
            <div class='header'>
              <h3>KPI 1 - Indicadores Clave de Proceso</h3>
            </div>

            <div class='content overflow-hidden' >
				<iframe class='preview-pane' src='".FULL_URL."dashboard/grafico_medidor_ordenes' width='103%' style='zoom:0.8;'height='250' frameborder='0' scrolling='no'></iframe>
            </div>

            <div class='content no-padding'>
              <table class='red'>
                <thead>
                  <tr>
                    <th>Name</th>
					<th class='right'><span></span>D&iacute;a Actual</th>
                    <th class='right'><span></span>Mes Actual</th>
                    <th class='right'><span></span>Mes Anterior</th>
                    <th class='right'><span></span>KPI</th>
                  </tr>
                </thead>
                <tbody class='no-border-x'>
				  <tr>
                    <td style='width:40%;'><i class='fa fa-sitemap'></i> Ordenes Generadas Ensa</td>
					<td class='text-right'>".$dia_actual_generadas_ensa."</td>
                    <td class='text-right'>".@$arr_datos[$mes_actual]['GeneradasEnsa']."</td>
                    <td class='text-right'>".@$arr_datos[$mes_anterior]['GeneradasEnsa']."</td>
                    <td class='text-right'>
                    ".$icon_generadas_ensa."
                    </td>
                  </tr>
                  <tr>
                    <td style='width:40%;'><i class='fa fa-sitemap'></i> Ordenes Recibidas Pexport</td>
					<td class='text-right'>".$dia_actual_generadas."</td>
                    <td class='text-right'>".@$arr_datos[$mes_actual]['Generadas']."</td>
                    <td class='text-right'>".@$arr_datos[$mes_anterior]['Generadas']."</td>
                    <td class='text-right'>
                    ".$icon_generadas."
                    </td>
                  </tr>
                  <tr>
                    <td><i class='fa fa-tasks'></i> Ordenes Finalizadas Pexport</td>
                    <td class='text-right'><a href='#' data-toggle='modal' data-target='#mod-success4' onclick='showModalReportFinalizadas()'>".$dia_actual_finalizadas."</a></td>
                    <td class='text-right'>".@$arr_datos[$mes_actual]['Finalizadas']."</td>
                    <td class='text-right'>".@$arr_datos[$mes_anterior]['Finalizadas']."</td>
                    <td class='text-right'>
                    ".$icon_finalizadas."
                    </td>
                  </tr>
                  <tr>
                    <td><i class='fa fa-tasks'></i> Ordenes Finalizadas Ensa</td>
	                 <td class='text-right'><a href='#' data-toggle='modal' data-target='#mod-success4' onclick='showModalReportFinalizadas()'>".$dia_actual_finalizadas_ensa."</a></td>
                     <td class='text-right'>".@$arr_datos[$mes_actual]['FinalizadasEnsa']."</td>
                     <td class='text-right'>".@$arr_datos[$mes_anterior]['FinalizadasEnsa']."</td>
                     <td class='text-right'>
                     ".$icon_finalizadas."
                     </td>
                   </tr>
                   <tr>
                    <td><i class='fa fa-signal'></i> Visitas Infructuosas</td>
                    <td class='text-right'><a href='#' data-toggle='modal' data-target='#mod-success4' onclick='showModalReportInfructuosas()'>".$dia_actual_infructuosas."</a></td>
                    <td class='text-right'>".@$arr_datos[$mes_actual]['Infructuosa']."</td>
                    <td class='text-right'>".@$arr_datos[$mes_anterior]['Infructuosa']."</td>
                    <td class='text-right'>
                    ".$icon_infructuosas."
                    </td>
                  </tr>
                  <tr>
                    <td><i class='fa fa-bolt'></i> Tiempo de Ejecucion</td>
                    <td class='text-right'>".$dia_actual_tiempo_promedio."</td>
                    <td class='text-right'>".@$arr_datos[$mes_actual]['TiempoPromedio']."</td>
                    <td class='text-right'>".@$arr_datos[$mes_anterior]['TiempoPromedio']."</td>
                    <td class='text-right'>
                    ".$icon_tiempo_promedio."
                    </td>
                  </tr>
				 </tbody>
               </table>
               <table class='red'>
                 <thead>                 
                  <tr>
                    <th></th>
 					<th></th>
                    <th></th>
                    <th></th>
                  </tr>
                  <tr>
                    <td><i class='fa fa-tasks'></i> Ordenes Pendientes</td>
                    <td class='text-right'><a href='".FULL_URL."main/monitorear/pendienteactual'>".$dia_actual_pendientes."</a></td>
                    <td><i class='fa fa-tasks'></i> Ordenes Pendientes mayor a 2 d&iacute;as</td>
                    <td class='text-right'><a href='".FULL_URL."main/monitorear/pendientemayor2dias'>".$dia_actual_pendientes_mayor_2_dias."</a></td>
                  </tr>
				  <tr>
                     <td><i class='fa fa-tasks'></i> Ordenes Pendientes de Envio a ENSA mayor a 2 d&iacute;as</td>
                     <td class='text-right'><a href='".FULL_URL."main/monitorear/pendienteexportado2dias'>".$dia_actual_pendientes_exportado_2_dias."</a></td>
                     <td class='text-right'></td>
                     <td class='text-right'></td>
                     <td class='text-right'></td>
                  </tr>
                  </thead>
                 <tbody class='no-border-x'>
                </tbody>
              </table>
              <div class='text-right'>
              * Datos comparados entre los ".$dia_actual." primeros d&iacute;as de cada mes.
              </div>
            </div>
          </div>


          <div class='block-flat' >
            <div class='header'>
              <h3>Monitor 1 - Control de Ordenes Pendientes</h3>
            </div>
            <div class='content'>
              <iframe class='preview-pane' src='".FULL_URL."dashboard/grafico_pendientes_bar' width='100%' height='310' frameborder='0' scrolling='no'></iframe>

            </div>
          </div>
              		
          <div class='block-flat' >
            <div class='header'>
              <h3>Monitor 2 - Control de Ordenes por Tecnico</h3>
            </div>
            <div class='content'>
              <iframe class='preview-pane' src='".FULL_URL."dashboard/grafico_estado_ordenes_por_tecnico' width='100%' height='310' frameborder='0' scrolling='no'></iframe>

            </div>
          </div>

          <div class='block-flat'>
            <div class='header'>
              <h3>Monitor 3 - Control Productividad Semanal Tecnicos</h3>
            </div>
            <div class='content overflow-hidden'>
              <iframe class='preview-pane' src='".FULL_URL."dashboard/grafico_tecnico_semana' width='100%' height='395' frameborder='0' scrolling='no'></iframe>
            </div>
          </div>
              		
          <div class='block-flat' >
            <div class='header'>
              <h3>Monitor 4 - Control de Cuadrillas por Estado Actividad</h3>
            </div>
            <div class='content overflow-hidden'>
	       		<iframe class='preview-pane' src='".FULL_URL."dashboard/grafico_cuadrillas_pie' width='100%' height='315' frameborder='0' scrolling='no'></iframe>
            </div>
          </div>

        </div>


        <div class='col-sm-6 col-md-6'>


          <div class='block-flat' >
            <div class='header'>
              <h3>Monitor GPS - Ubicacion en Tiempo Real</h3>
            </div>
            <div class='content' style='height:490px;'>
              	<iframe class='preview-pane' src='".FULL_URL."dashboard/mapa_monitoreo2' width='100%' height='465' frameborder='0' scrolling='si'></iframe>
              	<a href='".FULL_URL."dashboard/mapa_monitoreo' target='_blank'>ver mapa completo</a>
            </div>
          </div>

          <div class='block-flat'>
            <div class='header'>
              <h3>Monitor 5 - Resumen y Comparativo Mensual</h3>
            </div>
            <div class='content'>
              <iframe class='preview-pane' src='".FULL_URL."dashboard/grafico_rendimiento_por_mes' width='100%' height='320' frameborder='0' scrolling='no'></iframe>
            </div>
          </div>
              		
          <div class='block-flat'>
            <div class='header'>
              <h3>Monitor 6 - Control Semanal de Actividad ENSA-PEXPORT</h3>
            </div>
            <div class='content'>
              <iframe class='preview-pane' src='".FULL_URL."dashboard/grafico_rendimiento_por_semana' width='100%' height='380' frameborder='0' scrolling='no'></iframe>
            </div>
          </div>

          <div class='block-flat'>
            <div class='header'>
              <h3>Monitor 7 - Control por Actividad</h3>
            </div>
            <div class='content overflow-hidden'>
			         <iframe class='preview-pane' src='".FULL_URL."dashboard/grafico_actividad' width='100%' height='395' frameborder='0' scrolling='no'></iframe>
            </div>
          </div>
              		
          <div class='block-flat'>
            <div class='header'>
              <h3>Monitor 8 - Resumen Visitas Infructuosas</h3>
            </div>
            <div class='content overflow-hidden'>
			         <iframe class='preview-pane' src='".FULL_URL."dashboard/grafico_ordenes_infructuosas_por_tecnico' width='100%' height='345' frameborder='0' scrolling='no'></iframe>
            </div>
          </div>

        </div>
              		
        <div class='col-sm-12 col-md-12'>
          <div class='block-flat'>
            <div class='header'>
              <h3>Monitor 9 - Distribucion Cuadrillas y Sectores</h3>
            </div>
            <div class='content overflow-hidden'>
	       		<iframe class='preview-pane' src='".FULL_URL."dashboard/grafico_cuadrillas_bar' width='100%' height='335' frameborder='0' scrolling='no'></iframe>
            </div>
          </div>

          <div class='block-flat'>
            <div class='header'>
              <h3>Monitor 10 - Distribucion Cuadrillas y Sectores Global</h3>
            </div>
            <div class='content overflow-hidden'>
	       		<iframe class='preview-pane' src='".FULL_URL."dashboard/grafico_cuadrillas_donut' width='100%' height='505' frameborder='0' scrolling='no'></iframe>
            </div>
          </div>
              		
          <div class='block-flat'>
            <div class='header'>
              <h3>Monitor 11 - Control por Tipo de Inspeccion Realizado</h3>
            </div>
            <div class='content overflow-hidden'>
	       		<iframe class='preview-pane' src='".FULL_URL."dashboard/grafico_actividad_finalizado' width='100%' height='340' frameborder='0' scrolling='no'></iframe>
            </div>
          </div>
              		
        </div>

      </div>

      </div>

    ";

      echo $lista;


?>
