<?php
date_default_timezone_set('America/Lima');
//require "../conexion.php";
require_once(APP."View/Main/conexion_emergencia.ctp");


function getColorEstado($var2){
	if($var2==1){
		//Universo
		$color_flag = 'E6E6E6';
	}elseif($var2==2){
		//Eliminacion Logica
		$color_flag = 'FF4000';
	}elseif($var2==3){
		//Activo
		$color_flag = '81F7F3';
	}elseif($var2==4){
		//Inactivo
		$color_flag = 'F3E2A9';
	}elseif($var2==5){
		//Asignado
		$color_flag = '5FB404';
	}elseif($var2==6){
		//Pendiente
		$color_flag = 'FFFF00';
	}elseif($var2==7){
		//En Camino
		$color_flag = 'DF7401';
	}elseif($var2==8){
		//Ejecutado
		$color_flag = '04B45F';
	}elseif($var2==9){
		//Finalizado
		$color_flag = '088A08';
	}elseif($var2==10){
		//Observado
		$color_flag = '81DAF5';
	}elseif($var2==11){
		//Asignaciones
		$color_flag = 'D8D8D8';
	}elseif($var2==12){
		//Generado
		$color_flag = 'E1F5A9';
	}elseif($var2==13){
		//Descargado
		$color_flag = 'A9D0F5';
	}elseif($var2==14){
		//Exportado
		$color_flag = '2E9AFE';
	}elseif($var2==15){
		//Finalizado
		$color_flag = '088A29';
	}
	return $color_flag;
}
?>
<!DOCTYPE html>

<html lang="es">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="">
<meta name="author" content="">
<link rel="shortcut icon" href="<?php echo FULL_URL;?>images/favicon.png">

<title>ACSION</title>
<link
	href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,400italic,700,800'
	rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Raleway:100'
	rel='stylesheet' type='text/css'>
<link
	href='http://fonts.googleapis.com/css?family=Open+Sans+Condensed:300,700'
	rel='stylesheet' type='text/css'>


<!-- Bootstrap core CSS -->
<link href="<?php echo FULL_URL;?>js/bootstrap/dist/css/bootstrap.css" rel="stylesheet" />
<link rel="stylesheet"
	href="<?php echo FULL_URL;?>fonts/font-awesome-4/css/font-awesome.min.css">
<link rel="stylesheet" type="text/css"
	href="<?php echo FULL_URL;?>js/jquery.magnific-popup/dist/magnific-popup.css" />

<style type="text/css" title="currentStyle">
thead input {
	width: 100%
}

input.search_init {
	color: #999
}

p {
	text-align: left;
}
</style>


<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
      <script src="<?php echo FULL_URL;?>../assets/js/html5shiv.js"></script>
      <script src="<?php echo FULL_URL;?>../assets/js/respond.min.js"></script>
    <![endif]-->
<link rel="stylesheet" type="text/css"
	href="<?php echo FULL_URL;?>js/jquery.gritter/css/jquery.gritter.css" />
<script type="text/javascript"
	src="<?php echo FULL_URL;?>js/jquery.nestable/jquery.nestable.js"></script>
<link rel="stylesheet" type="text/css"
	href="<?php echo FULL_URL;?>js/jquery.nanoscroller/nanoscroller.css" />
<link rel="stylesheet" type="text/css"
	href="<?php echo FULL_URL;?>js/jquery.easypiechart/jquery.easy-pie-chart.css" />
<link rel="stylesheet" type="text/css"
	href="<?php echo FULL_URL;?>js/bootstrap.switch/bootstrap-switch.css" />
<link rel="stylesheet" type="text/css"
	href="<?php echo FULL_URL;?>js/bootstrap.datetimepicker/css/bootstrap-datetimepicker.min.css" />
<link rel="stylesheet" type="text/css"
	href="<?php echo FULL_URL;?>js/jquery.select2/select2.css" />
<link rel="stylesheet" type="text/css"
	href="<?php echo FULL_URL;?>js/bootstrap.slider/css/slider.css" />
<link rel="stylesheet" type="text/css"
	href="<?php echo FULL_URL;?>js/dropzone/css/dropzone.css" />
<link rel="stylesheet" type="text/css"
	href="<?php echo FULL_URL;?>js/jquery.niftymodals/css/component.css" />
<!-- Custom styles for this template -->
<link href="<?php echo FULL_URL;?>css/style.css" rel="stylesheet" />

<style>
html, body, #map-canvas {
	height: 100%;
	margin: 0px;
	padding: 0px;
	background-color: white;
}
.form-group {
	margin-bottom: 0px !important;
}
</style>
</head>
<body>
<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&amp;sensor=false"></script>

<?php 
$str_where = "";
if(isset($movil) && $movil!='0'){
	$str_where .= " and CI.idusuario=".$movil;
}
?>
<div class="row" style="margin: 0px;">
<div class="form-group">
	<label class="col-sm-2 control-label">Selecciona una movil :</label>
	<div class="col-sm-2">
		<select class='select2' id='cbo_movil'>
		<option value='0'>Todos</option>
			<option value='115' <?php if($movil=='115'){ echo 'selected';}?>>Movil 01 Pexport</option>
			<option value='116' <?php if($movil=='116'){ echo 'selected';}?>>Movil 02 Ensa</option>
		</select>
	</div>
	<div class="col-sm-3">
	<button id="generar-mapa" class="btn btn-primary" type="button">Generar Mapa</button>
	</div>
 </div>
</div>
<div class="row" style="height: 92%; margin: 0px; padding: 0px;">
<div id='map-canvas'></div>
</div>
<?php
/* Finalizados por Movil */
$query_finalizadas = pg_query ( "select 
CI.IdIntervencion as IdIntervencion,
CO.NombreSector as NombreSector,
CI.IdEstado as IdEstado,
ME.NomEstado as NomEstado,
ME.IdEstado as IdEstado,
CO.FechaGeneracion as NuevaFecha,
CO.Suministro as Suministro,
CU.idusuario,
GMP.nompersona as nompersona,
GMP.idpersona as idpersona,
GMG.longitud1 as longitud,
GMG.latitud1 as latitud,
CO.NombreCliente as NombreCliente,
CO.DireccionCliente as DireccionCliente,
CO.Cartera as Cartera,
CO.FechaGeneracion as FechaGeneracion,
CO.Alimentador as Alimentador,
CO.Sed as Sed,
CO.Medidor as Medidor,
CI.fechaatencion
from ComMovTab_Intervencion CI 
join ComMovTab_Orden CO on CI.IdOrden = CO.IdOrden
join ComMovTab_Usuario CU on CU.IdUsuario = CI.IdUsuario
join GlobalMaster_Persona GMP on GMP.IdPersona = CU.IdPersona
join ComMovTab_Documento CD on CO.IdDocumento = CD.IdDocumento
join ComMovTab_TipoIntervencion TI on CD.IdTipoIntervencion = TI.IdTipoIntervencion
join ComMovTab_MaestrosEstado ME on CI.IdEstado = ME.IdEstado
join GlobalMaster_Gps GMG on GMG.suministro = CO.suministro
where CD.IdTipoIntervencion=4 and CI.idestado in (9,14,15) ".$str_where." order by CU.idusuario, CO.FechaGeneracion asc;");

$marker = '';
$str_rutas = '';
$str_path = '';
$tmp_idusuario = '';
$count_inter_usuario = 1;
$arr_ultima_intervencion = array();
$arr_colors = array('2E2EFE','FF0040','B60466','8A2908','04B431');
while ($obj_inter = pg_fetch_object($query_finalizadas)){
	
	if($tmp_idusuario!=$obj_inter->idusuario){
		$tmp_idusuario = $obj_inter->idusuario;
		
		$key_color = array_rand($arr_colors);
		$color = $arr_colors[$key_color];
		unset($arr_colors[$key_color]);
		
		$str_rutas .= "
				var polyOptions".$tmp_idusuario." = {
		    	strokeColor: '#".$color."',
		    	strokeOpacity: 1.0,
		    	strokeWeight: 3,
		    	icons: [{
            		repeat: '70px',
            		icon: iconsetngs,
            		offset: '100%'}]
		    	};
				poly".$tmp_idusuario." = new google.maps.Polyline(polyOptions".$tmp_idusuario.");
  				poly".$tmp_idusuario.".setMap(map5);
  				var path".$tmp_idusuario." = poly".$tmp_idusuario.".getPath();";
		$count_inter_usuario = 1;
	}
	
	$str_estado = '';
	$var2 = $obj_inter->idestado;
	$var22 = $obj_inter->nomestado;
	
	$fechaejecucion = strtotime($obj_inter->fechaatencion);
	if($count_inter_usuario>1){
		$tiempo = $fechaejecucion - $fechaejecucion_tmp;
	}else{
		$tiempo = '0';
	}
	$fechaejecucion_tmp = strtotime($obj_inter->fechaatencion);
	
	$horas              = floor ( $tiempo / 3600 );
	$minutes            = ( ( $tiempo / 60 ) % 60 );
	$seconds            = ( $tiempo % 60 );
		
	$time['horas']      = str_pad( $horas, 2, "0", STR_PAD_LEFT );
	$time['minutes']    = str_pad( $minutes, 2, "0", STR_PAD_LEFT );
	$time['seconds']    = str_pad( $seconds, 2, "0", STR_PAD_LEFT );
		
	$str_tiempo               = implode( ':', $time );
	 
	$color_flag = getColorEstado($var2);	
	
	$str_rutas .="path".$tmp_idusuario.".push(new google.maps.LatLng(".$obj_inter->latitud.", ".$obj_inter->longitud."));";
	
	$str_rutas .= "
		var contentString = '<div id=\"content\"><div id=\"siteNotice\"></div><h1 id=\"firstHeading\" class=\"firstHeading\">Suministro: ".$obj_inter->suministro."</h1><div id=\"bodyContent\"><p>' +
      			'Cliente: <strong>".$obj_inter->nombrecliente."</strong><br>'+
				'Direcci&oacute;n: <strong>".$obj_inter->direccioncliente."</strong><br>'+
		  		'Fecha de Atenci&oacute;n: <strong>".$obj_inter->fechaatencion."</strong><br>'+
				'Atendido por: <strong>".$obj_inter->nompersona."</strong><br>'+
		  		'Tiempo transcurrido desde el punto anterior: <strong>".$str_tiempo."</strong><br>'+
    			'Estado: <strong>".$obj_inter->nomestado."</strong>'+
		  		'</p></div></div>';
		  var infowindow".$obj_inter->idintervencion." = new google.maps.InfoWindow({
		      content: contentString
		  });
			var marker".$obj_inter->idintervencion." = new google.maps.Marker({
		 	     position: new google.maps.LatLng(".(($obj_inter->latitud=='')?'-6.779033333333335':$obj_inter->latitud).", ".(($obj_inter->longitud=='')?'-79.88541166666666':$obj_inter->longitud)."),
		 	     map: map5,
 	     		 icon: 'https://chart.googleapis.com/chart?chst=d_map_pin_letter&chld=".$count_inter_usuario."|".$color_flag."|000000',
		 	     title: '".$count_inter_usuario."'
		 	  });
			google.maps.event.addListener(marker".$obj_inter->idintervencion.", 'click', function() {
			  infowindow".$obj_inter->idintervencion.".open(map5,marker".$obj_inter->idintervencion.");
			}); ";
	$arr_ultima_intervencion[$tmp_idusuario] = array('nombremovil'=>$obj_inter->nompersona,'latitud'=>$obj_inter->latitud,'longitud'=>$obj_inter->longitud,'color'=>$color,'count'=>$count_inter_usuario);
	$count_inter_usuario++;
}

/* Estado en Camino */
$query_finalizadas = pg_query("select
CI.IdIntervencion as IdIntervencion,
CO.NombreSector as NombreSector,
CI.IdEstado as IdEstado,
ME.NomEstado as NomEstado,
ME.IdEstado as IdEstado,
CO.FechaGeneracion as NuevaFecha,
CO.Suministro as Suministro,
CU.idusuario,
GMP.nompersona as nompersona,
GMP.idpersona as idpersona,
GMG.longitud1 as longitud,
GMG.latitud1 as latitud,
CO.NombreCliente as NombreCliente,
CO.DireccionCliente as DireccionCliente,
CO.Cartera as Cartera,
CO.FechaGeneracion as FechaGeneracion,
CO.Alimentador as Alimentador,
CO.Sed as Sed,
CO.Medidor as Medidor
from ComMovTab_Intervencion CI
join ComMovTab_Orden CO on CI.IdOrden = CO.IdOrden
join ComMovTab_Usuario CU on CU.IdUsuario = CI.IdUsuario
join GlobalMaster_Persona GMP on GMP.IdPersona = CU.IdPersona
join ComMovTab_Documento CD on CO.IdDocumento = CD.IdDocumento
join ComMovTab_TipoIntervencion TI on CD.IdTipoIntervencion = TI.IdTipoIntervencion
join ComMovTab_MaestrosEstado ME on CI.IdEstado = ME.IdEstado
join GlobalMaster_Gps GMG on GMG.suministro = CO.suministro
where CD.IdTipoIntervencion=4 and CI.idestado in (7) ".$str_where." order by CU.idusuario, CO.FechaGeneracion asc;");

$tmp_idusuario = '';
while ($obj_inter = pg_fetch_object($query_finalizadas)){

	if($tmp_idusuario!=$obj_inter->idusuario){
		$tmp_idusuario = $obj_inter->idusuario;

		$obj_ultima_inter = $arr_ultima_intervencion[$tmp_idusuario];
		$color = $obj_ultima_inter['color'];
		$count_inter_usuario = ($obj_ultima_inter['count']) + 1;
		$str_rutas .= "
				var polyOptionsEC".$tmp_idusuario." = {
		    	strokeColor: '#".$color."',
		    	strokeOpacity: 0,
		    	strokeWeight: 3,
			    icons: [{
			      icon: lineSymbol,
			      offset: '0',
			      repeat: '20px'
			    }]
		    	};
				polyEC".$tmp_idusuario." = new google.maps.Polyline(polyOptionsEC".$tmp_idusuario.");
  				polyEC".$tmp_idusuario.".setMap(map5);
  				var pathEC".$tmp_idusuario." = polyEC".$tmp_idusuario.".getPath();";
		$str_rutas .="pathEC".$tmp_idusuario.".push(new google.maps.LatLng(".$obj_ultima_inter['latitud'].", ".$obj_ultima_inter['longitud']."));";
	}

	$var2 = $obj_inter->idestado;
	$var22 = $obj_inter->nomestado;

	$color_flag = getColorEstado($var2);

	$str_rutas .="pathEC".$tmp_idusuario.".push(new google.maps.LatLng(".$obj_inter->latitud.", ".$obj_inter->longitud."));";

	$str_rutas .= "
		var contentString = '<div id=\"content\"><div id=\"siteNotice\"></div><h1 id=\"firstHeading\" class=\"firstHeading\">Suministro: ".$obj_inter->suministro."</h1><div id=\"bodyContent\"><p>' +
      			'Cliente: <strong>".$obj_inter->nombrecliente."</strong><br>'+
				'Direcci&oacute;n: <strong>".$obj_inter->direccioncliente."</strong><br>'+
		  		'Fecha de Generaci&oacute;n: <strong>".$obj_inter->fechageneracion."</strong><br>'+
		  		'Tiempo Estimado de Llegada: <strong>5 minutos</strong><br>'+
				'Ser&aacute; Atendido por: <strong>".$obj_inter->nompersona."</strong><br>'+
    			'Estado: <strong>".$obj_inter->nomestado."</strong>'+
		  		'</p></div></div>';
		  var infowindow".$obj_inter->idintervencion." = new google.maps.InfoWindow({
		      content: contentString
		  });
			var marker".$obj_inter->idintervencion." = new google.maps.Marker({
		 	     position: new google.maps.LatLng(".(($obj_inter->latitud=='')?'-6.779033333333335':$obj_inter->latitud).", ".(($obj_inter->longitud=='')?'-79.88541166666666':$obj_inter->longitud)."),
		 	     map: map5,
 	     		 icon: 'https://chart.googleapis.com/chart?chst=d_map_pin_letter&chld=".$count_inter_usuario."|".$color_flag."|000000',
		 	     title: '".$count_inter_usuario."'
		 	  });
			google.maps.event.addListener(marker".$obj_inter->idintervencion.", 'click', function() {
			  infowindow".$obj_inter->idintervencion.".open(map5,marker".$obj_inter->idintervencion.");
			}); ";
	$count_inter_usuario++;
}

/* Pendientes */
$query_pendientes = pg_query("select
CI.IdIntervencion as IdIntervencion,
CO.NombreSector as NombreSector,
CI.IdEstado as IdEstado,
ME.NomEstado as NomEstado,
ME.IdEstado as IdEstado,
CO.FechaGeneracion as NuevaFecha,
CO.Suministro as Suministro,
CU.idusuario,
GMP.nompersona as nompersona,
GMP.idpersona as idpersona,
GMG.longitud1 as longitud,
GMG.latitud1 as latitud,
CO.NombreCliente as NombreCliente,
CO.DireccionCliente as DireccionCliente,
CO.Cartera as Cartera,
CO.FechaGeneracion as FechaGeneracion,
CO.Alimentador as Alimentador,
CO.Sed as Sed,
CO.Medidor as Medidor
from ComMovTab_Intervencion CI
join ComMovTab_Orden CO on CI.IdOrden = CO.IdOrden
join ComMovTab_Usuario CU on CU.IdUsuario = CI.IdUsuario
join GlobalMaster_Persona GMP on GMP.IdPersona = CU.IdPersona
join ComMovTab_Documento CD on CO.IdDocumento = CD.IdDocumento
join ComMovTab_TipoIntervencion TI on CD.IdTipoIntervencion = TI.IdTipoIntervencion
join ComMovTab_MaestrosEstado ME on CI.IdEstado = ME.IdEstado
join GlobalMaster_Gps GMG on GMG.suministro = CO.suministro
where CD.IdTipoIntervencion=4 and CI.idestado in (6) order by CO.FechaGeneracion asc;");

$count_inter_pendientes = 1;
while ($obj_inter = pg_fetch_object($query_pendientes)){

	$var2 = $obj_inter->idestado;
	$var22 = $obj_inter->nomestado;

	$color_flag = getColorEstado($var2);

	$str_rutas .= "
		var contentString = '<div id=\"content\"><div id=\"siteNotice\"></div><h1 id=\"firstHeading\" class=\"firstHeading\">Suministro: ".$obj_inter->suministro."</h1><div id=\"bodyContent\"><p>' +
      			'Cliente: <strong>".$obj_inter->nombrecliente."</strong><br>'+
				'Direcci&oacute;n: <strong>".$obj_inter->direccioncliente."</strong><br>'+
		  		'Fecha de Generaci&oacute;n: <strong>".$obj_inter->fechageneracion."</strong><br>'+
    			'Estado: <strong>".$obj_inter->nomestado."</strong>'+
		  		'</p></div></div>';
		  var infowindow".$obj_inter->idintervencion." = new google.maps.InfoWindow({
		      content: contentString
		  });
			var marker".$obj_inter->idintervencion." = new google.maps.Marker({
		 	     position: new google.maps.LatLng(".(($obj_inter->latitud=='')?'-6.779033333333335':$obj_inter->latitud).", ".(($obj_inter->longitud=='')?'-79.88541166666666':$obj_inter->longitud)."),
		 	     map: map5,
 	     		 icon: 'https://chart.googleapis.com/chart?chst=d_map_pin_letter&chld=".$count_inter_pendientes."|".$color_flag."|000000',
		 	     title: '".$count_inter_pendientes."'
		 	  });
			google.maps.event.addListener(marker".$obj_inter->idintervencion.", 'click', function() {
			  infowindow".$obj_inter->idintervencion.".open(map5,marker".$obj_inter->idintervencion.");
			}); ";
	$count_inter_pendientes++;
}
?>
<div style="display: none;">
	<div id="legend" style="text-align: right;">
	
		<?php
		foreach ($arr_ultima_intervencion as $k => $v){
			?>
			<div class="">
				<label class="control-label"
					style="background: white; padding-left: 3px; padding-right: 3px;"><?php echo $v['nombremovil'];?></label>
				<span class="label" style="background-color: #<?php echo $v['color'];?>;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
			</div>
			<?php 
		} 
		?>

		<div class="">
			<label class="control-label"
				style="background: white; padding-left: 3px; padding-right: 3px;">Pendiente</label>
			<img
				src="https://chart.googleapis.com/chart?chst=d_map_pin_letter&chld=|FFFF00|000000">
		</div>

		<div class="">
			<label class="control-label"
				style="background: white; padding-left: 3px; padding-right: 3px;">En
				Camino</label> <img
				src="https://chart.googleapis.com/chart?chst=d_map_pin_letter&chld=|DF7401|000000">
		</div>

		<div class="">
			<label class="control-label"
				style="background: white; padding-left: 3px; padding-right: 3px;">Finalizado</label>
			<img
				src="https://chart.googleapis.com/chart?chst=d_map_pin_letter&chld=|088A08|000000">
		</div>

	</div>
</div>
<script>
    /*Maps*/
      var map5;
      
      var myLatlng = new google.maps.LatLng(-6.77361,-79.8417);
      
      var mapOptions = {
        zoom: 13,
        center: myLatlng,
        mapTypeId: google.maps.MapTypeId.ROADMAP
      };
 	 
      map5 = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);
      map5.controls[google.maps.ControlPosition.RIGHT_BOTTOM].push(document.getElementById('legend'));

      var iconsetngs = {
   		  path: google.maps.SymbolPath.FORWARD_CLOSED_ARROW
      };

      // Define a symbol using SVG path notation, with an opacity of 1.
      var lineSymbol = {
        path: 'M 0,-1 0,1',
        strokeOpacity: 1,
        scale: 4
      };
    		  
	/*
      var marker = new google.maps.Marker({
 	     position: myLatlng,
 	     map: map5,
 	     title: 'Hello World!'
 	  });
 	  */
 	  <?php echo $str_rutas;?>
/*
 * 
 var myLatlng = new google.maps.LatLng(-25.363882,131.044922);
 var mapOptions = {
   zoom: 4,
   center: myLatlng
 }
 var map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);

 var marker = new google.maps.Marker({
     position: myLatlng,
     map: map,
     title: 'Hello World!'
 });
 */
</script>

	<script src="<?php echo FULL_URL;?>js/jquery.js"></script>
	<script type="text/javascript"
		src="<?php echo FULL_URL;?>js/jquery.nanoscroller/jquery.nanoscroller.js"></script>
	<script type="text/javascript"
		src="<?php echo FULL_URL;?>js/jquery.sparkline/jquery.sparkline.min.js"></script>
	<script type="text/javascript"
		src="<?php echo FULL_URL;?>js/jquery.easypiechart/jquery.easy-pie-chart.js"></script>
	<script type="text/javascript" src="<?php echo FULL_URL;?>js/behaviour/general.js"></script>
	<script src="<?php echo FULL_URL;?>js/jquery.ui/jquery-ui.js" type="text/javascript"></script>
	<script type="text/javascript"
		src="<?php echo FULL_URL;?>js/jquery.nestable/jquery.nestable.js"></script>
	<script type="text/javascript"
		src="<?php echo FULL_URL;?>js/bootstrap.switch/bootstrap-switch.min.js"></script>
	<script type="text/javascript"
		src="<?php echo FULL_URL;?>js/bootstrap.datetimepicker/js/bootstrap-datetimepicker.min.js"></script>
	<script src="<?php echo FULL_URL;?>js/jquery.select2/select2.min.js"
		type="text/javascript"></script>
	<script src="<?php echo FULL_URL;?>js/bootstrap.slider/js/bootstrap-slider.js"
		type="text/javascript"></script>
	<script type="text/javascript"
		src="<?php echo FULL_URL;?>js/jquery.gritter/js/jquery.gritter.js"></script>

	<script type="text/javascript">
    $(document).ready(function(){
      //initialize the javascript
      App.init();
      	$('body').on('click','#generar-mapa',function(e){
    		cbo_movil = $('#cbo_movil').val();
    	 	window.open("<?php echo FULL_URL;?>dashboard/mapa_emergencia/"+cbo_movil,"_self");
    	});
    });
  </script>
	<script type='text/javascript'
		src='../js/jquery.fullcalendar/fullcalendar/fullcalendar.js'></script>

	<!-- Bootstrap core JavaScript
================================================== -->
	<!-- Placed at the end of the document so the pages load faster -->
	<script src="<?php echo FULL_URL;?>js/behaviour/voice-commands.js"></script>
	<script src="<?php echo FULL_URL;?>js/bootstrap/dist/js/bootstrap.min.js"></script>
	<script type="text/javascript" src="<?php echo FULL_URL;?>js/jquery.flot/jquery.flot.js"></script>
	<script type="text/javascript"
		src="<?php echo FULL_URL;?>js/jquery.flot/jquery.flot.pie.js"></script>
	<script type="text/javascript"
		src="<?php echo FULL_URL;?>js/jquery.flot/jquery.flot.resize.js"></script>
	<script type="text/javascript"
		src="<?php echo FULL_URL;?>js/jquery.flot/jquery.flot.labels.js"></script>
</body>
</html>