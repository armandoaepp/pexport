<?php
date_default_timezone_set('America/Lima');

//require_once "../conexion.php";
require_once(APP."View/Main/conexion.ctp");

$fechaactual=date("Y-m-d");

$idini1 = date('Y-m-01');
$idfin1 = date("Y-m-d H:i:s");



$query_grafico = pg_query("select TipoIntervencion,SubActividad,
sum(TotalGeneradas) as TotalGeneradas,
sum(TotalFinalizadas) as TotalFinalizadas,
((sum(TotalGeneradas))-(sum(TotalFinalizadas))) as TotalPendientes,
case when sum(TotalGeneradas) > 0 then ((sum(TotalFinalizadas)*100)/sum(TotalGeneradas)) else 0 end as PorcentajeTotal,
sum(OrdenesGeneradas) as OrdenesGeneradasDia,
sum(OrdenesFinalizadasOrdenesGeneradasDia) as OrdenesFinalizadasDia,
((sum(OrdenesGeneradas))-(sum(OrdenesFinalizadasOrdenesGeneradasDia))) as OrdenesPendientesDia,
case when sum(OrdenesGeneradas) > 0 then ((sum(OrdenesFinalizadasOrdenesGeneradasDia)*100)/sum(OrdenesGeneradas)) else 0 end as PorcentajeDia,
sum(OrdenesFinalizadas) as OrdenesFinalizadasAcarreo
from (
select ctis.DetalleTipoIntervencion as TipoIntervencion,
cti.DetalleTipoIntervencion as SubActividad,
(case when (cast(ci.FechaAtencion as date) BETWEEN '2014-06-23' AND '$idfin1') and ci.IdEstado=15 and co.Condicion='0'
then COUNT(*) else 0 end) as TotalFinalizadas,
(case when (cast(ci.FechaAtencion as date) BETWEEN '$idini1' AND '$idfin1') and ci.IdEstado=15 and co.Condicion='0'
then COUNT(*) else 0 end) as OrdenesFinalizadas,
(case when (cast(ci.FechaAtencion as date) BETWEEN '$idini1' AND '$idfin1') and (ci.IdEstado=15) and co.Condicion='0' and ci.IdIntervencion in (select ci.IdIntervencion from ComMovTab_Orden co
inner join ComMovTab_Intervencion ci
on co.IdOrden = ci.IdOrden where cast(ci.FechaAsignado as date) = '$idfin1' and ci.IdEstado = 15) and co.Condicion='0'
then COUNT(*) else 0 end) as OrdenesFinalizadasOrdenesGeneradasDia,
(case when cast(ci.FechaAsignado as date) BETWEEN '2014-06-23' AND '$idfin1' and co.Condicion='0'
then COUNT(*) else 0 end) as TotalGeneradas,
(case when cast(ci.FechaAsignado as date) BETWEEN '$idini1' AND '$idfin1' and co.Condicion='0'
then COUNT(*) else 0 end) as OrdenesGeneradas
from ComMovTab_TipoIntervencion cti
inner join ComMovTab_TipoIntervencion ctis on ctis.IdTipoIntervencion = cti.IdTipoIntervencionSup
left outer join ComMovTab_Orden co on cti.DetalleTipoIntervencion = co.SubActividad
left outer join ComMovTab_Intervencion ci on co.IdOrden = ci.IdOrden
group by ctis.DetalleTipoIntervencion,cti.DetalleTipoIntervencion,co.Condicion,ci.FechaAtencion,co.FechaGeneracion,ci.FechaAsignado,ci.IdEstado,ci.IdIntervencion) as consulta2
group by TipoIntervencion,SubActividad");


$rs=pg_num_rows($query_grafico);

$serie='';
if ($rs > 0) {

	while ($obj = pg_fetch_object($query_grafico)){
		$serie .= "['".$obj->subactividad."', ".$obj->ordenesfinalizadasacarreo."],";
	}
	$serie = substr($serie,0,-1);
}else{
	echo 'No hay datos';
	exit();
}

?>
<!DOCTYPE html>
<html lang="es" >
<head>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta http-equiv="content-type" content="text/html; charset=UTF-8">
<title>Grafico Pexport</title>

<script type="text/javascript" src="//code.jquery.com/jquery-1.9.1.js"></script>



<script type="text/javascript">//<![CDATA[

$(function () {
    $('#container').highcharts({
        chart: {
            plotBackgroundColor: null,
            plotBorderWidth: null,
            plotShadow: false
        },
        title: {
            text: 'Resumen por sub actividad desde <?php echo $idini1;?>'
        },
        tooltip: {
            pointFormat: '{series.name}: <b> {point.y} ({point.percentage:.1f}%)</b>'
        },
        plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                dataLabels: {
                    enabled: true,
                    format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                    style: {
                        color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                    },
                    connectorColor: 'silver'
                }
            }
        },
        credits: {
            enabled: false
        },
        series: [{
            type: 'pie',
            name: 'Cantidad de Ordenes',
            data: [<?php echo $serie;?>]
        }]
    });
});
//]]>

</script>


</head>
<body>
	<!--
	<script src="http://code.highcharts.com/highcharts.js"></script>
	<script src="http://code.highcharts.com/modules/exporting.js"></script>
	 -->

	<script src="../js/jquery.high/highcharts.js"></script>
	<script src="../js/jquery.high/modules/exporting.js"></script>

	<div id="container" style="min-width: 310px; max-width: 800px; height: 380px; margin: 0 auto">
	</div>

</body>
</html>
