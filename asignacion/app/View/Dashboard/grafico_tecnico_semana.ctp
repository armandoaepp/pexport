<?php

date_default_timezone_set('America/Lima');

//require_once "../conexion.php";
require_once(APP."View/Main/conexion.ctp");

$dia_semmana = date('w');
$fecha_ultima_semana=date('Y-m-d',strtotime('-1 weeks', strtotime(date("Y-m-d"))));
$query_grafico = pg_query("select ci.IdUsuario,gp.NomPersona,gp.ApePersona, date_part('dow',FechaAtencion) as dia_semana,count(ci.IdEstado) as TotalFin  from ComMovTab_Intervencion ci
inner join ComMovTab_Orden co on ci.IdOrden=co.IdOrden
inner join ComMovTab_Usuario cu on ci.IdUsuario = cu.IdUsuario
inner join GlobalMaster_Persona gp on cu.IdPersona = gp.IdPersona
where co.SubActividad in (select DetalleTipoIntervencion from ComMovTab_TipoIntervencion)
and ci.IdEstado in (14,15) and co.Condicion='0' 
and FechaAtencion > '$fecha_ultima_semana 23:59:59'
group by ci.IdUsuario,gp.NomPersona,gp.ApePersona, date_part('dow',FechaAtencion) 
order by ci.IdUsuario asc, date_part('dow',FechaAtencion) asc");


$rs=pg_num_rows($query_grafico);

$categorias_x='';
$serie='';
if ($rs > 0) {
	$i=0;
	$num_dia=0;
	$num_dias_semana=0;
	while ($obj = pg_fetch_object($query_grafico)){
		if($num_dias_semana==0){
			$num_dia=$obj->dia_semana;
			$categorias_x .= "'".$obj->nompersona."',";
		}
		if($obj->dia_semana>0 && $num_dias_semana==0){
			for($j=0;$j<$obj->dia_semana;$j++){
				$serie .= "[".$i.", ".($j).", ''],";
				$num_dias_semana++;
			}
		}
		if($obj->dia_semana<$num_dia){
			$i++;
			$categorias_x .= "'".$obj->nompersona."',";
		}
		
		if($num_dia==$obj->dia_semana){
			$serie .= "[".$i.", ".($obj->dia_semana).", ".$obj->totalfin."],";
		}else{
			for($j=0;$j<=($obj->dia_semana-$num_dia);$j++){
				$serie .= "[".$i.", ".($num_dia+$j-1).", ''],";
				$num_dias_semana++;
			}
			$num_dia=$obj->dia_semana;
			$serie .= "[".$i.", ".($obj->dia_semana).", ".$obj->totalfin."],";
		}
		
		if($num_dia==6){
			$num_dia=0;
			$i++;
			$num_dias_semana=0;
		}else{
			$num_dia++;
			$num_dias_semana++;
		}
	}
	$categorias_x = substr($categorias_x,0,-1);
	$serie = substr($serie,0,-1);
}else{
	echo 'No hay datos';
	exit();
}

$categorias_y = '';
switch ($dia_semmana){
	case 0:
		$categorias_y = "'<strong>Domingo ".date('m-d')."</strong>', 
			'Lunes ".date('m-d', mktime(0, 0, 0, date("m"), date("d")-1, date("Y")))."', 
			'Martes ".date('m-d', mktime(0, 0, 0, date("m"), date("d")-2, date("Y")))."', 
			'Miercoles ".date('m-d', mktime(0, 0, 0, date("m"), date("d")-3, date("Y")))."', 
			'Jueves ".date('m-d', mktime(0, 0, 0, date("m"), date("d")-4, date("Y")))."', 
			'Viernes ".date('m-d', mktime(0, 0, 0, date("m"), date("d")-5, date("Y")))."', 
			'Sabado ".date('m-d', mktime(0, 0, 0, date("m"), date("d")-6, date("Y")))."'";
		break;
	case 1:
		$categorias_y = "'Domingo ".date('m-d', mktime(0, 0, 0, date("m"), date("d")-1, date("Y")))."',
			'<strong>Lunes ".date('m-d')."</strong>',
			'Martes ".date('m-d', mktime(0, 0, 0, date("m"), date("d")-6, date("Y")))."',
			'Miercoles ".date('m-d', mktime(0, 0, 0, date("m"), date("d")-5, date("Y")))."',
			'Jueves ".date('m-d', mktime(0, 0, 0, date("m"), date("d")-4, date("Y")))."',
			'Viernes ".date('m-d', mktime(0, 0, 0, date("m"), date("d")-3, date("Y")))."',
			'Sabado ".date('m-d', mktime(0, 0, 0, date("m"), date("d")-2, date("Y")))."'";
		break;
	case 2:
		$categorias_y = "'Domingo ".date('m-d', mktime(0, 0, 0, date("m"), date("d")-2, date("Y")))."',
			'Lunes ".date('m-d', mktime(0, 0, 0, date("m"), date("d")-1, date("Y")))."',
			'<strong>Martes ".date('m-d')."</strong>',
			'Miercoles ".date('m-d', mktime(0, 0, 0, date("m"), date("d")-6, date("Y")))."',
			'Jueves ".date('m-d', mktime(0, 0, 0, date("m"), date("d")-5, date("Y")))."',
			'Viernes ".date('m-d', mktime(0, 0, 0, date("m"), date("d")-4, date("Y")))."',
			'Sabado ".date('m-d', mktime(0, 0, 0, date("m"), date("d")-3, date("Y")))."'";
		break;
	case 3:
		$categorias_y = "'Domingo ".date('m-d', mktime(0, 0, 0, date("m"), date("d")-3, date("Y")))."',
			'Lunes ".date('m-d', mktime(0, 0, 0, date("m"), date("d")-2, date("Y")))."',
			'Martes ".date('m-d', mktime(0, 0, 0, date("m"), date("d")-1, date("Y")))."',
			'<strong>Miercoles ".date('m-d')."</strong>',
			'Jueves ".date('m-d', mktime(0, 0, 0, date("m"), date("d")-6, date("Y")))."',
			'Viernes ".date('m-d', mktime(0, 0, 0, date("m"), date("d")-5, date("Y")))."',
			'Sabado ".date('m-d', mktime(0, 0, 0, date("m"), date("d")-4, date("Y")))."'";
		break;
	case 4:
		$categorias_y = "'Domingo ".date('m-d', mktime(0, 0, 0, date("m"), date("d")-4, date("Y")))."',
			'Lunes ".date('m-d', mktime(0, 0, 0, date("m"), date("d")-3, date("Y")))."',
			'Martes ".date('m-d', mktime(0, 0, 0, date("m"), date("d")-2, date("Y")))."',
			'Miercoles ".date('m-d', mktime(0, 0, 0, date("m"), date("d")-1, date("Y")))."',
			'<strong>Jueves ".date('m-d')."</strong>',
			'Viernes ".date('m-d', mktime(0, 0, 0, date("m"), date("d")-6, date("Y")))."',
			'Sabado ".date('m-d', mktime(0, 0, 0, date("m"), date("d")-5, date("Y")))."'";
		break;
	case 5:
		$categorias_y = "'Domingo ".date('m-d', mktime(0, 0, 0, date("m"), date("d")-5, date("Y")))."',
			'Lunes ".date('m-d', mktime(0, 0, 0, date("m"), date("d")-4, date("Y")))."',
			'Martes ".date('m-d', mktime(0, 0, 0, date("m"), date("d")-3, date("Y")))."',
			'Miercoles ".date('m-d', mktime(0, 0, 0, date("m"), date("d")-2, date("Y")))."',
			'Jueves ".date('m-d', mktime(0, 0, 0, date("m"), date("d")-1, date("Y")))."',
			'<strong>Viernes ".date('m-d')."</strong>',
			'Sabado ".date('m-d', mktime(0, 0, 0, date("m"), date("d")-6, date("Y")))."'";
		break;
	case 6:
		$categorias_y = "'Domingo ".date('m-d', mktime(0, 0, 0, date("m"), date("d")-6, date("Y")))."',
			'Lunes ".date('m-d', mktime(0, 0, 0, date("m"), date("d")-5, date("Y")))."',
			'Martes ".date('m-d', mktime(0, 0, 0, date("m"), date("d")-4, date("Y")))."',
			'Miercoles ".date('m-d', mktime(0, 0, 0, date("m"), date("d")-3, date("Y")))."',
			'Jueves ".date('m-d', mktime(0, 0, 0, date("m"), date("d")-2, date("Y")))."',
			'Viernes ".date('m-d', mktime(0, 0, 0, date("m"), date("d")-1, date("Y")))."',
			'<strong>Sabado ".date('m-d')."</strong>'";
		break;
}
?>
<!DOCTYPE html>
<html lang="es" >
<head>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta http-equiv="content-type" content="text/html; charset=UTF-8">
<title>Grafico Pexport</title>

<script type="text/javascript" src="//code.jquery.com/jquery-1.9.1.js"></script>



<script type="text/javascript">//<![CDATA[ 

$(function () {
    $('#container').highcharts({

        chart: {
            type: 'heatmap',
            marginTop: 40,
            marginBottom: 40
        },


        title: {
            text: 'Rendimiento técnicos última semana'
        },

        xAxis: {
            categories: [<?php echo $categorias_x;?>]
        },

        yAxis: {
            categories: [<?php echo $categorias_y;?>],
            title: null
        },

        colorAxis: {
            min: 0,
            minColor: '#FFFFFF',
            maxColor: Highcharts.getOptions().colors[0]
        },

        legend: {
            align: 'right',
            layout: 'vertical',
            margin: 0,
            verticalAlign: 'top',
            y: 25,
            symbolHeight: 320
        },

        tooltip: {
            formatter: function () {
                return '<b>' + this.series.xAxis.categories[this.point.x] + '</b> realizo <br><b>' +
                    this.point.value + '</b> ordenes el <br><b>' + this.series.yAxis.categories[this.point.y] + '</b>';
            }
        },

        series: [{
            name: 'Sales per employee',
            borderWidth: 1,
            data: [<?php echo $serie;?>],
            dataLabels: {
                enabled: true,
                color: 'black',
                style: {
                    textShadow: 'none',
                    HcTextStroke: null
                }
            }
        }],

        credits: {
            enabled: false
        }

    });
});
//]]>  

</script>


</head>
<body>
	<!-- 
	<script src="http://code.highcharts.com/highcharts.js"></script>
	<script src="http://code.highcharts.com/modules/exporting.js"></script>
	 -->
	
	<script src="../js/jquery.high/highcharts.js"></script>
	<script src="../js/jquery.high/modules/heatmap.js"></script>
	<script src="../js/jquery.high/modules/exporting.js"></script>
	<script src="../js/jquery.high/highcharts-more.js"></script>

	<div id="container" style="min-width: 310px; max-width: 800px; height: 390px; margin: 0 auto">
	</div>
	
</body>
</html>