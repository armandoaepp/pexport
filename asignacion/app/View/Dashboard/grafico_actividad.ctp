<?php
date_default_timezone_set('America/Lima');

//require_once "../conexion.php";
require_once(APP."View/Main/conexion.ctp");

$fechaactual=date("Y-m-d");

if(isset($_GET["idini1"]))
{
  $idini1=$_GET["idini1"];
} else {
  $idini1=$fechaactual;
}


if(isset($_GET["idfin1"]))
{
  $idfin1=$_GET["idfin1"];
} else {
  $idfin1=$fechaactual;
}

$query_grafico = pg_query("select TipoIntervencion,SubActividad,
sum(TotalGeneradas) as TotalGeneradas,
sum(TotalFinalizadas) as TotalFinalizadas,
((sum(TotalGeneradas))-(sum(TotalFinalizadas))) as TotalPendientes,
case when sum(TotalGeneradas) > 0 then ((sum(TotalFinalizadas)*100)/sum(TotalGeneradas)) else 0 end as PorcentajeTotal,
sum(OrdenesGeneradas) as OrdenesGeneradasDia,
sum(OrdenesFinalizadasOrdenesGeneradasDia) as OrdenesFinalizadasDia,
((sum(OrdenesGeneradas))-(sum(OrdenesFinalizadasOrdenesGeneradasDia))) as OrdenesPendientesDia,
case when sum(OrdenesGeneradas) > 0 then ((sum(OrdenesFinalizadasOrdenesGeneradasDia)*100)/sum(OrdenesGeneradas)) else 0 end as PorcentajeDia,
sum(OrdenesFinalizadas) as OrdenesFinalizadasAcarreo
from (
select ctis.DetalleTipoIntervencion as TipoIntervencion,
cti.DetalleTipoIntervencion as SubActividad,
(case when (cast(ci.FechaAtencion as date) BETWEEN '2014-06-23' AND '$idfin1') and ci.IdEstado=15 and co.Condicion='0'
then COUNT(*) else 0 end) as TotalFinalizadas,
(case when (cast(ci.FechaAtencion as date) BETWEEN '$idini1' AND '$idfin1') and ci.IdEstado=15 and co.Condicion='0'
then COUNT(*) else 0 end) as OrdenesFinalizadas,
(case when (cast(ci.FechaAtencion as date) BETWEEN '$idini1' AND '$idfin1') and (ci.IdEstado=15) and co.Condicion='0' and ci.IdIntervencion in (select ci.IdIntervencion from ComMovTab_Orden co
inner join ComMovTab_Intervencion ci
on co.IdOrden = ci.IdOrden where cast(ci.FechaAsignado as date) = '$idfin1' and ci.IdEstado = 15) and co.Condicion='0'
then COUNT(*) else 0 end) as OrdenesFinalizadasOrdenesGeneradasDia,
(case when cast(ci.FechaAsignado as date) BETWEEN '2014-06-23' AND '$idfin1' and co.Condicion='0'
then COUNT(*) else 0 end) as TotalGeneradas,
(case when cast(ci.FechaAsignado as date) BETWEEN '$idini1' AND '$idfin1' and co.Condicion='0'
then COUNT(*) else 0 end) as OrdenesGeneradas
from ComMovTab_TipoIntervencion cti
inner join ComMovTab_TipoIntervencion ctis on ctis.IdTipoIntervencion = cti.IdTipoIntervencionSup
left outer join ComMovTab_Orden co on cti.DetalleTipoIntervencion = co.SubActividad
left outer join ComMovTab_Intervencion ci on co.IdOrden = ci.IdOrden
group by ctis.DetalleTipoIntervencion,cti.DetalleTipoIntervencion,co.Condicion,ci.FechaAtencion,co.FechaGeneracion,ci.FechaAsignado,ci.IdEstado,ci.IdIntervencion) as consulta2
group by TipoIntervencion,SubActividad");


$rs=pg_num_rows($query_grafico);

$categorias='';
$serie_finalizadas='';
$serie_pendientes='';
if ($rs > 0) {

	while ($obj = pg_fetch_object($query_grafico)){
		$categorias .= "'".$obj->subactividad."',";
		$serie_finalizadas .= $obj->ordenesfinalizadasacarreo.',';
		$serie_pendientes .= $obj->totalpendientes.',';
	}
	$categorias = substr($categorias,0,-1);
	$serie_finalizadas = substr($serie_finalizadas,0,-1);
	$serie_pendientes = substr($serie_pendientes,0,-1);
}else{
	echo 'No hay datos';
	exit();
}

?>
<!DOCTYPE html>
<html lang="es" >
<head>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta http-equiv="content-type" content="text/html; charset=UTF-8">
<title>Grafico Pexport</title>

<script type="text/javascript" src="//code.jquery.com/jquery-1.9.1.js"></script>



<script type="text/javascript">//<![CDATA[

$(function () {
    $('#container').highcharts({
        chart: {
            type: 'bar'
        },
        title: {
            text: 'Resumen por sub actividad del <?php echo $fechaactual;?>'
        },
        xAxis: {
            categories: [<?php echo $categorias;?>]
        },
        yAxis: {
            min: 0,
            title: {
                text: 'Total actividades por Estado'
            }
        },
        legend: {
            reversed: true
        },
        plotOptions: {
            series: {
                stacking: 'normal'
            }
        },
        series: [{
            name: 'Pendientes',
            data: [<?php echo $serie_pendientes;?>]
        }, {
            name: 'Finalizadas',
            data: [<?php echo $serie_finalizadas;?>]
        }],
        credits: {
            enabled: false
        }
    });
});
//]]>

</script>


</head>
<body>
	<!--
	<script src="http://code.highcharts.com/highcharts.js"></script>
	<script src="http://code.highcharts.com/modules/exporting.js"></script>
	 -->

	<script src="../js/jquery.high/highcharts.js"></script>
	<script src="../js/jquery.high/modules/exporting.js"></script>

	<div id="container" style="min-width: 310px; max-width: 800px; height: 380px; margin: 0 auto">
	</div>

</body>
</html>
