<?php

date_default_timezone_set('America/Lima');

//require_once "../conexion.php";
require_once(APP."View/Main/conexion.ctp");

$fechaactual=date("Y-m-d");

$fecha_inicio = date('Y-m-01');
$fecha_fin = date("Y-m-d H:i:s");

$query_grafico = pg_query("select consulta1.IdUsuario as IdUsuario,consulta1.NomPersona as NomPersona,consulta1.ApePersona as ApePersona,consulta1.Total as Generadas,
consulta2.TotalFin as Finalizadas,
((consulta1.Total)-consulta2.TotalFin) as Pendientes,
((consulta2.TotalFin*100)/consulta1.Total) as Porcentaje  from 
(
select ci.IdUsuario,gp.NomPersona,gp.ApePersona,count(ci.IdEstado) as Total  from ComMovTab_Intervencion ci
inner join ComMovTab_Orden co on ci.IdOrden=co.IdOrden
inner join ComMovTab_Usuario cu on ci.IdUsuario = cu.IdUsuario
inner join GlobalMaster_Persona gp on cu.IdPersona = gp.IdPersona
where co.SubActividad in (select DetalleTipoIntervencion from ComMovTab_TipoIntervencion) 
and co.Condicion='0' and (FechaAsignado >= '$fecha_inicio' and FechaAsignado <= '$fecha_fin')
group by ci.IdUsuario,gp.NomPersona,gp.ApePersona) as consulta1 
left outer join 
(
select ci.IdUsuario,gp.NomPersona,gp.ApePersona,count(ci.IdEstado) as TotalFin  from ComMovTab_Intervencion ci
inner join ComMovTab_Orden co on ci.IdOrden=co.IdOrden
inner join ComMovTab_Usuario cu on ci.IdUsuario = cu.IdUsuario
inner join GlobalMaster_Persona gp on cu.IdPersona = gp.IdPersona
inner join ComMovTab_MovimientosIntervencion mi on mi.IdIntervencion=ci.IdIntervencion
where co.SubActividad in (select DetalleTipoIntervencion from ComMovTab_TipoIntervencion)
and ci.IdEstado=15 and co.Condicion='0' 
and mi.FechaHora >= '$fecha_inicio' and mi.FechaHora <= '$fecha_fin' and ValorMovimiento='15' 
group by ci.IdUsuario,gp.NomPersona,gp.ApePersona) as consulta2 on consulta1.IdUsuario = consulta2.IdUsuario");


$rs=pg_num_rows($query_grafico);

$categorias='';
$serie='';
if ($rs > 0) {
	
	while ($obj = pg_fetch_object($query_grafico)){
		
		$query3mon2 = pg_query("select me.IdEstado,me.NomEstado,COALESCE(consulta1.cantidad, '0') as cantidad from ComMovTab_MaestrosEstado me
		left outer join ( select ci.IdEstado as Id,COUNT(*) as cantidad from ComMovTab_Intervencion ci join
		ComMovTab_Orden co on ci.IdOrden = co.IdOrden where ci.IdUsuario=".$obj->idusuario." and co.Condicion='0'
		and co.SubActividad in (select DetalleTipoIntervencion from ComMovTab_TipoIntervencion)
		group by ci.IdEstado) as consulta1
		on consulta1.Id = me.IdEstado where me.EstadoSup=11 and me.IdEstado !=12
		");

		$str_estados_head = '';
		$str_estados_value = '';
		while ($obj2 = pg_fetch_object($query3mon2)){
			if($obj2->idestado!=15){
				$str_estados_head .= "'".$obj2->nomestado."',";
				$str_estados_value .= $obj2->cantidad.',';
			}else{
				$str_estados_head .= "'".$obj2->nomestado."',";
				$str_estados_value .= $obj->finalizadas.',';
			}
		}
		$categorias = "'Generado', ".$str_estados_head;
		$serie .= "{name: '".$obj->nompersona."',data: [".$obj->generadas.",".$str_estados_value."],stack: '".$obj->nompersona."'},"; 
	}
	$categorias = substr($categorias,0,-1);
	$serie = substr($serie,0,-1);
}else{
	echo 'No hay datos';
	exit();
}

?>
<!DOCTYPE html>
<html lang="es" >
<head>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta http-equiv="content-type" content="text/html; charset=UTF-8">
<title>Grafico Pexport</title>

<script type="text/javascript" src="//code.jquery.com/jquery-1.9.1.js"></script>



<script type="text/javascript">//<![CDATA[ 

$(function () {
    $('#container').highcharts({

        chart: {
            type: 'column',
            options3d: {
                enabled: true,
                alpha: 15,
                beta: 15,
                viewDistance: 25,
                depth: 40
            },
            marginTop: 80,
            marginRight: 40
        },

        title: {
            text: 'Resumen de ordenes por técnico desde: <?php echo $fecha_inicio;?>'
        },

        xAxis: {
            categories: [<?php echo $categorias;?>]
        },

        yAxis: {
            allowDecimals: false,
            min: 0,
            title: {
                text: 'Cantidad de Ordenes'
            }
        },

        tooltip: {
            headerFormat: '<b>{point.key}</b><br>',
            pointFormat: '<span style="color:{series.color}">\u25CF</span> {series.name}: {point.y} / {point.stackTotal}'
        },

        plotOptions: {
            column: {
                stacking: 'normal',
                depth: 40
            }
        },

        credits: {
            enabled: false
        },

        series: [<?php echo $serie;?>]
    });
});
//]]>  

</script>


</head>
<body>
	<!-- 
	<script src="http://code.highcharts.com/highcharts.js"></script>
	<script src="http://code.highcharts.com/modules/exporting.js"></script>
	 -->
	
	<script src="../js/jquery.high/highcharts.js"></script>
	<script src="../js/jquery.high/modules/exporting.js"></script>
	<script src="../js/jquery.high/highcharts-3d.js"></script>

	<div id="container" style="min-width: 310px; max-width: 800px; height: 300px; margin: 0 auto">
	</div>
	
</body>
</html>