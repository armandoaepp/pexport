<?php
date_default_timezone_set('America/Lima');

//require_once "../conexion.php";
require_once(APP."View/Main/conexion.ctp");

if(isset($_GET['f'])){
	$fecha_inicio = $_GET['f'];
	$fecha_fin = $_GET['f'];
}else{
	$fecha_inicio = date('Y-m-d');
	$fecha_fin = date("Y-m-d");
}

$fechaactual=date("Y-m-d");

$query_grafico = pg_query("select consulta1.IdUsuario as IdUsuario,consulta1.NomPersona as NomPersona,consulta1.ApePersona as ApePersona,
consulta1.Total as Infructuosas from 
(
select ci.IdUsuario,gp.NomPersona,gp.ApePersona,count(ci.IdEstado) as Total  from ComMovTab_Intervencion ci
inner join ComMovTab_Orden co on ci.IdOrden=co.IdOrden 
inner join ComMovTab_Usuario cu on ci.IdUsuario = cu.IdUsuario
inner join GlobalMaster_Persona gp on cu.IdPersona = gp.IdPersona 
inner join ComMovTab_Detalle_Intervencion DI on DI.IdIntervencion=ci.IdIntervencion
where co.SubActividad in (select DetalleTipoIntervencion from ComMovTab_TipoIntervencion)
and co.Condicion='0' and FechaAtencion >= '$fecha_inicio' and FechaAtencion <= '$fecha_fin 23:59:59' and DI.IdMaestrosIntervencion in (
select IdMaestrosIntervencion from ComMovTab_MaestrosIntervencion
where IdentificadorMaestroIntervencion like 'LabelInfructuosa%')
group by ci.IdUsuario,gp.NomPersona,gp.ApePersona) as consulta1 
");


$rs=pg_num_rows($query_grafico);

$serie='';
if ($rs > 0) {
	
	while ($obj = pg_fetch_object($query_grafico)){
		$serie .= "['".$obj->nompersona."',".$obj->infructuosas."],"; 
	}
	$serie = substr($serie,0,-1);
}else{
	echo 'No hay datos';
	exit();
}

?>
<!DOCTYPE html>
<html lang="es">
<head>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta http-equiv="content-type" content="text/html; charset=UTF-8">
<title>Grafico Pexport</title>

<script type="text/javascript" src="//code.jquery.com/jquery-1.9.1.js"></script>

	<!-- Bootstrap core CSS -->
    <link href="../js/bootstrap/dist/css/bootstrap.css" rel="stylesheet" />
    <link rel="stylesheet" href="../fonts/font-awesome-4/css/font-awesome.min.css">
    
    <link rel="stylesheet" type="text/css" href="../js/bootstrap.datetimepicker/css/bootstrap-datetimepicker.min.css" />
    
    <!-- Custom styles for this template -->
    <link href="../css/style.css" rel="stylesheet" />
    
    <script type="text/javascript" src="../js/behaviour/general.js"></script>
    <script type="text/javascript" src="../js/bootstrap.datetimepicker/js/bootstrap-datetimepicker.min.js"></script>


<script type="text/javascript">//<![CDATA[ 

$(function () {
    $('#container').highcharts({

        chart: {
            type: 'column',
            options3d: {
                enabled: true,
                alpha: 15,
                beta: 15,
                viewDistance: 25,
                depth: 40
            },
            marginTop: 80,
            marginRight: 40
        },

        title: {
            text: 'Resumen de ordenes infructuosas por técnico (<?php echo $fecha_inicio;?>)'
        },

        xAxis: {
            type: 'category',
            labels: {
                rotation: -45,
                style: {
                    fontSize: '13px',
                    fontFamily: 'Verdana, sans-serif'
                }
            }
        },
        yAxis: {
            min: 0,
            title: {
                text: 'Ordenes Infructuosas'
            }
        },

        tooltip: {
            pointFormat: 'Ordenes Infructuosas: <b>{point.y} </b>'
        },

        plotOptions: {
            series: {
                borderWidth: 0,
                dataLabels: {
                    enabled: true,
                    format: '{point.y}'
                }
            }
        },
        
        legend: {
            enabled: false
        },

        credits: {
            enabled: false
        },

        series: [{
            name: 'Infructuosas',
            data: [<?php echo $serie;?>]
        }]

    });
});
//]]>  

</script>

</head>
<body style="background: white;">
	<!-- 
	<script src="http://code.highcharts.com/highcharts.js"></script>
	<script src="http://code.highcharts.com/modules/exporting.js"></script>
	 -->

	<script src="../js/jquery.high/highcharts.js"></script>
	<script src="../js/jquery.high/modules/exporting.js"></script>
	<script src="../js/jquery.high/highcharts-3d.js"></script>

	<div class='form-group' style="margin: 0px;">
		<label class='col-sm-1 control-label' style="float: left; padding-top: 5px;">Fecha:</label>
		<div class='col-sm-3' style="float: left; width: 170px">
			<div class='input-group date datetime col-md-6 col-xs-4'
				data-min-view='2' data-date-format='yyyy-mm-dd' style="width: 150px;">
				<input class='form-control' size='16' type='text' value='<?php echo $fecha_inicio;?>'
					id='fechaini1' name='fechaini1' readonly> <span
					class='input-group-addon btn btn-primary'><span
					class='glyphicon glyphicon-th'></span></span>
			</div>
		</div>
		<button id="btn_consultar" class="btn btn-primary">Consultar</button>
	</div>

	<div id="container"
		style="min-width: 310px; max-width: 800px; height: 300px; margin: 0 auto">
	</div>
	
	<script type="text/javascript">
	$(".datetime").datetimepicker({autoclose: true});

	$('#btn_consultar').on('click',function() {
		window.open('<?php echo FULL_URL;?>dashboard/grafico_ordenes_infructuosas_por_tecnico?f='+$('#fechaini1').val(),'_self');
	});
	</script>

</body>
</html>