<?php

date_default_timezone_set('America/Lima');

if(isset($_GET['ajax'])){
	$ajax = $_GET['ajax'];
}else{
	$ajax = false;
}

//require_once "../conexion.php";
require_once(APP."View/Main/conexion.ctp");

$minutos = -60;

$query_grafico = pg_query("select
(select count(ci.IdEstado) as Total from ComMovTab_Intervencion ci
inner join ComMovTab_Orden co on ci.IdOrden=co.IdOrden
where co.SubActividad in (select DetalleTipoIntervencion from ComMovTab_TipoIntervencion) and co.Condicion='0'
and created >= NOW() + INTERVAL '".$minutos." minute'
) as Generadas
,(
select count(ci.IdEstado) as Total from ComMovTab_Intervencion ci
inner join ComMovTab_Orden co on ci.IdOrden=co.IdOrden
inner join ComMovTab_MovimientosIntervencion mi on mi.IdIntervencion=ci.IdIntervencion
where co.SubActividad in (select DetalleTipoIntervencion from ComMovTab_TipoIntervencion)
and ci.IdEstado=15 and co.Condicion='0'
and mi.FechaHora >= NOW() + INTERVAL '".$minutos." minute' and ValorMovimiento='15'
) as Finalizadas");


$rs=pg_num_rows($query_grafico);

$generadas = 0;
$finalizadas = 0;

while ($obj = pg_fetch_object($query_grafico)){
	$generadas = $obj->generadas;
	$finalizadas = $obj->finalizadas;
}

if($ajax){
	echo json_encode(array('Generadas'=>$generadas,'Finalizadas'=>$finalizadas));
	exit();
}
?>
<!DOCTYPE html>
<html lang="es" >
<head>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta http-equiv="content-type" content="text/html; charset=UTF-8">
<title>Grafico Pexport</title>

<script type="text/javascript" src="//code.jquery.com/jquery-1.9.1.js"></script>



<script type="text/javascript">//<![CDATA[

$(function () {
	$(function () {

	    var gaugeOptions = {

	        chart: {
	            type: 'solidgauge'
	        },

	        title: null,

	        pane: {
	            center: ['50%', '85%'],
	            size: '140%',
	            startAngle: -90,
	            endAngle: 90,
	            background: {
	                backgroundColor: (Highcharts.theme && Highcharts.theme.background2) || '#EEE',
	                innerRadius: '60%',
	                outerRadius: '100%',
	                shape: 'arc'
	            }
	        },

	        tooltip: {
	            enabled: false
	        },

	        // the value axis
	        yAxis: {
	            stops: [
	                [0.1, '#55BF3B'], // green
	                [0.5, '#DDDF0D'], // yellow
	                [0.9, '#DF5353'] // red
	            ],
	            lineWidth: 0,
	            minorTickInterval: null,
	            tickPixelInterval: 400,
	            tickWidth: 0,
	            title: {
	                y: -70
	            },
	            labels: {
	                y: 16
	            }
	        },

	        plotOptions: {
	            solidgauge: {
	                dataLabels: {
	                    y: 5,
	                    borderWidth: 0,
	                    useHTML: true
	                }
	            }
	        }
	    };

	    // The speed gauge
	    $('#container-1').highcharts(Highcharts.merge(gaugeOptions, {
	        yAxis: {
	            min: 0,
	            max: 200,
	            title: {
	                text: 'Ingresadas'
	            }
	        },

	        credits: {
	            enabled: false
	        },

	        series: [{
	            name: 'Ingresadas',
	            data: [<?php echo $generadas;?>],
	            dataLabels: {
	                format: '<div style="text-align:center"><span style="font-size:25px;color:' +
	                    ((Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black') + '">{y}</span><br/>' +
	                       '<span style="font-size:12px;color:silver">Ordenes/Hora</span></div>'
	            },
	            tooltip: {
	                valueSuffix: ' Ordenes/Hora'
	            }
	        }]

	    }));

	    // The RPM gauge
	    $('#container-2').highcharts(Highcharts.merge(gaugeOptions, {
	        yAxis: {
	            min: 0,
	            max: 200,
	            title: {
	                text: 'Finalizadas'
	            }
	        },

	        credits: {
	            enabled: false
	        },

	        series: [{
	            name: 'Finalizadas',
	            data: [<?php echo $finalizadas;?>],
	            dataLabels: {
	                format: '<div style="text-align:center"><span style="font-size:25px;color:' +
	                    ((Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black') + '">{y}</span><br/>' +
	                       '<span style="font-size:12px;color:silver">Ordenes/Hora</span></div>'
	            },
	            tooltip: {
	                valueSuffix: ' Ordenes/Hora'
	            }
	        }]

	    }));

	    // Bring life to the dials
	    setInterval(function () {



	    	$.ajax({
          	  url: "<?php echo FULL_URL;?>dashboard/grafico_medidor_ordenes?ajax=true",
          	  dataType: 'json',
          	  success: function(data) {

          		// Speed
      	        var chart = $('#container-1').highcharts(),
      	            point,
      	            newVal,
      	            inc;

    	        if (chart) {
    	        	point = chart.series[0].points[0];
    		        /*
    	            inc = Math.round((Math.random() - 0.5) * 100);
    	            newVal = point.y + inc;

    	            if (newVal < 0 || newVal > 200) {
    	                newVal = point.y - inc;
    	            }*/
    	            point.update(data.Generadas);
    	        }

    	        // RPM
    	        chart = $('#container-2').highcharts();
    	        if (chart) {
    	            point = chart.series[0].points[0];
					/*
    	            inc = Math.random() - 0.5;
    	            newVal = point.y + inc;

    	            if (newVal < 0 || newVal > 5) {
    	                newVal = point.y - inc;
    	            }*/

    	            point.update(data.Finalizadas);
    	        }
          	  }
	    	});
	    }, 60000);


	});
});
//]]>

</script>


</head>
<body>
	<!--
	<script src="http://code.highcharts.com/highcharts.js"></script>
	<script src="http://code.highcharts.com/modules/exporting.js"></script>
	 -->

	<script src="../js/jquery.high/highcharts.js"></script>
	<script src="../js/jquery.high/modules/exporting.js"></script>
    <script src="../js/jquery.high/highcharts-more.js"></script>
	<script src="../js/jquery.high/modules/solid-gauge.src.js"></script>

	<div id="container-1" style="width: 250px; height: 180px; float: left;">
	</div>
	<div id="container-2" style="width: 250px; height: 180px; float: left;">
	</div>

</body>
</html>
