<?php
date_default_timezone_set('America/Lima');

//require_once "../conexion.php";
require_once(APP."View/Main/conexion.ctp");

$fechaactual=date("Y-m-d");

if(isset($_GET["idini1"]))
{
  $idini1=$_GET["idini1"];
} else {
  $idini1=$fechaactual;
}


if(isset($_GET["idfin1"]))
{
  $idfin1=$_GET["idfin1"];
} else {
  $idfin1=$fechaactual;
}



$query_grafico = pg_query("
select 
Cuadrilla,
SubCuadrilla,
IdConcesionaria,
setfinal.Sector,
TotalAsignadas,
TotalDescargadas,
TotalExportadas,
TotalFinalizadas from (
select IdSector,Sector,
SUM(TotalAsignadas) as TotalAsignadas,
SUM(TotalDescargadas) as TotalDescargadas,
SUM(TotalExportadas) as TotalExportadas,
SUM(TotalFinalizadas) as TotalFinalizadas from (
select  
co.IdSector as IdSector,
co.Sector as Sector,
(case when ci.IdEstado = 13 and co.Condicion='0' and co.SubActividad 
in (select DetalleTipoIntervencion from ComMovTab_TipoIntervencion)
then 1 else 0 end) as TotalDescargadas,
(case when ci.IdEstado = 5 and co.Condicion='0' and co.SubActividad 
in (select DetalleTipoIntervencion from ComMovTab_TipoIntervencion)
then 1 else 0 end) as TotalAsignadas,
(case when ci.IdEstado = 14 and co.Condicion='0' and co.SubActividad 
in (select DetalleTipoIntervencion from ComMovTab_TipoIntervencion)
then 1 else 0 end) as TotalExportadas,
(case when ci.IdEstado = 15 and co.Condicion='0' and co.SubActividad 
in (select DetalleTipoIntervencion from ComMovTab_TipoIntervencion)
then 1 else 0 end) as TotalFinalizadas
from ComMovTab_Orden co 
inner join ComMovTab_Intervencion ci on co.IdOrden = ci.IdOrden
where co.Condicion='0' and co.SubActividad 
in (select DetalleTipoIntervencion from ComMovTab_TipoIntervencion)) as superconsulta
group by IdSector,Sector) as setfinal
inner join (
select
gu.IdUbicacionConc as IdConcesionaria,
case when (select IdConceptosUbicacion from GlobalMaster_Ubicacion where IdUbicacion =dr2.IdUbicacionSup)=8
or (select IdConceptosUbicacion from GlobalMaster_Ubicacion where IdUbicacion =dr2.IdUbicacionSup)=7  
then gu3.NomUbicacion else gu2.NomUbicacion end as Cuadrilla,
gu2.NomUbicacion as SubCuadrilla,
gu.NomUbicacion as Sector
from GlobalMaster_DetalleUbicacionRegla dr
inner join GlobalMaster_DetalleUbicacionRegla dr2 on dr.IdUbicacionSup = dr2.IdUbicacion
inner join GlobalMaster_Ubicacion gu on dr.IdUbicacion = gu.IdUbicacion
inner join GlobalMaster_Ubicacion gu2 on dr.IdUbicacionSup = gu2.IdUbicacion
inner join GlobalMaster_Ubicacion gu3 on dr2.IdUbicacionSup = gu3.IdUbicacion
where gu.IdConceptosUbicacion = 5 ) as setfinal2 on
setfinal.IdSector=cast(setfinal2.IdConcesionaria as integer)
order by Cuadrilla asc, SubCuadrilla asc
");


$rs=pg_num_rows($query_grafico);

$cuadrilla = '';
$sub_cuadrilla = '';
$sector = '';
$series = '';
$series_drilldown = '';

$series_cuadrillas = '';
$series_sub_cuadrillas = '';
$series_sectores = '';

$sum_cuadrillas = 0;
$sum_sub_cuadrillas = 0;
$sum_sectores = 0;
if ($rs > 0) {

	while ($obj = pg_fetch_object($query_grafico)){
	/*$arr = array();
	$arr[] = (Object) array('Cuadrilla'=>'Cuadrilla1','SubCuadrilla'=>'SubCuadrilla1','Sector'=>'Sector1','TotalAsignadas'=>4,'TotalDescargadas'=>2,'TotalExportadas'=>1,'TotalFinalizadas'=>3);
	$arr[] = (Object) array('Cuadrilla'=>'Cuadrilla1','SubCuadrilla'=>'SubCuadrilla2','Sector'=>'Sector2','TotalAsignadas'=>4,'TotalDescargadas'=>2,'TotalExportadas'=>1,'TotalFinalizadas'=>3);
	$arr[] = (Object) array('Cuadrilla'=>'Cuadrilla1','SubCuadrilla'=>'SubCuadrilla2','Sector'=>'Sector3','TotalAsignadas'=>4,'TotalDescargadas'=>2,'TotalExportadas'=>1,'TotalFinalizadas'=>3);
	$arr[] = (Object) array('Cuadrilla'=>'Cuadrilla2','SubCuadrilla'=>'SubCuadrilla3','Sector'=>'Sector4','TotalAsignadas'=>4,'TotalDescargadas'=>2,'TotalExportadas'=>1,'TotalFinalizadas'=>3);
	$arr[] = (Object) array('Cuadrilla'=>'Cuadrilla2','SubCuadrilla'=>'SubCuadrilla3','Sector'=>'Sector5','TotalAsignadas'=>4,'TotalDescargadas'=>2,'TotalExportadas'=>1,'TotalFinalizadas'=>3);
	$arr = (Object) $arr;
	
	foreach ($arr as $k=>$obj){*/
		
		if($cuadrilla!=$obj->cuadrilla){
			$series_cuadrillas = str_replace('%y-cuadrillas%',$sum_cuadrillas,$series_cuadrillas);
			$sum_cuadrillas = 0;
			$series_cuadrillas .= "['".$obj->cuadrilla."',%y-cuadrillas%],";
			$cuadrilla = $obj->cuadrilla;
		}
		if($sub_cuadrilla!=$obj->subcuadrilla){
			$series_sub_cuadrillas = str_replace('%y-sub-cuadrillas%',$sum_sub_cuadrillas,$series_sub_cuadrillas);
			$sum_sub_cuadrillas = 0;
			$series_sub_cuadrillas .= "['".$obj->subcuadrilla."',%y-sub-cuadrillas%],";
			$sub_cuadrilla = $obj->subcuadrilla;
		}
		if($sector!=$obj->sector){
			$series_sectores = str_replace('%y-sector%',$sum_sectores,$series_sectores);
			$sum_sectores = 0;
			$series_sectores .= "['".$obj->sector."',%y-sector%],";
			$sector = $obj->sector;
		}
		
		$sum_cuadrillas += ($obj->totalasignadas + $obj->totaldescargadas + $obj->totalexportadas + $obj->totalfinalizadas);
		$sum_sub_cuadrillas += ($obj->totalasignadas + $obj->totaldescargadas + $obj->totalexportadas + $obj->totalfinalizadas);
		$sum_sectores += ($obj->totalasignadas + $obj->totaldescargadas + $obj->totalexportadas + $obj->totalfinalizadas);
		
	}
	
	$series_cuadrillas = str_replace('%y-cuadrillas%',$sum_cuadrillas,$series_cuadrillas);
	$series_sub_cuadrillas = str_replace('%y-sub-cuadrillas%',$sum_sub_cuadrillas,$series_sub_cuadrillas);
	$series_sectores = str_replace('%y-sector%',$sum_sectores,$series_sectores);
	
	$series_cuadrillas = substr($series_cuadrillas,0,-1);
	$series_sub_cuadrillas = substr($series_sub_cuadrillas,0,-1);
	$series_sectores = substr($series_sectores,0,-1);

	//echo $series;
	//echo '<br>';
	//echo $series_drilldown;
	//exit();
}else{
	echo 'No hay datos';
	exit();
}

?>
<!DOCTYPE html>
<html lang="es" >
<head>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta http-equiv="content-type" content="text/html; charset=UTF-8">
<title>Grafico Pexport</title>

<script type="text/javascript" src="//code.jquery.com/jquery-1.9.1.js"></script>



<script type="text/javascript">//<![CDATA[

$(function () {

	// Create the chart
    $('#container').highcharts({
        chart: {
            type: 'pie'
        },
        title: {
            text: 'Ordenes por cuadrillas, sub cuadrillas y sectores'
        },
        yAxis: {
            title: {
                text: 'Cantidad de Ordenes'
            }
        },
        credits: {
            enabled: false
        },
        plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                dataLabels: {
                    enabled: true,
                    format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                    style: {
                        color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                    },
                    connectorColor: 'silver'
                }
            }
        },
        tooltip: {
            pointFormat: '{series.name}: <b> {point.y} ({point.percentage:.1f}%)</b>'
        },
        series: [{
            name: 'Cuadrillas',
            data: [<?php echo $series_cuadrillas;?>],
            size: '40%',
            dataLabels: {
                formatter: function() {
                    return this.y > 5 ? this.point.name : null;
                },
                color: 'white',
                distance: -30
            }
        }, {
            name: 'Sub Cuadrillas',
            data: [<?php echo $series_sub_cuadrillas;?>],
            size: '70%',
            innerSize: '40%',
            dataLabels: {
                formatter: function() {
                    // display only if larger than 1
                    return this.y > 1 ? '<b>'+ this.point.name +':</b> '+ this.y +'%'  : null;
                }
            }
        }, {
            name: 'Sectores',
            data: [<?php echo $series_sectores;?>],
            size: '80%',
            innerSize: '70%',
            dataLabels: {
                formatter: function() {
                    // display only if larger than 1
                    return this.y > 1 ? '<b>'+ this.point.name +':</b> '+ this.y +'%'  : null;
                }
            }
        }]
    });
    
});
//]]>

</script>


</head>
<body>
	<!--
	<script src="http://code.highcharts.com/highcharts.js"></script>
	<script src="http://code.highcharts.com/modules/exporting.js"></script>
	 -->

	<script src="../js/jquery.high/highcharts.js"></script>
	<script src="../js/jquery.high/modules/exporting.js"></script>
	<script src="../js/jquery.high/modules/drilldown.js"></script>

	<div id="container" style="min-width: 310px; max-width: 800px; height: 500px; margin: 0 auto">
	</div>

</body>
</html>
