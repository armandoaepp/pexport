<?php
date_default_timezone_set('America/Lima');
//require_once "../conexion.php";
require_once(APP."View/Main/conexion.ctp");

$fecha_actual	=	date('Y-m-d');
/*
echo 'Listado de Personal<br>';
$query_personal_activos = pg_query("select distinct CU.IdUsuario, GMP.IdPersona, NomPersona, ApePersona from [ComMovTab.Intervencion] CI inner join [ComMovTab.Usuario] CU on CI.IdUsuario = CU.IdUsuario
            inner join [GlobalMaster.Persona] GMP on CU.IdPersona = GMP.IdPersona");

$rs=pg_num_rows($query_personal_activos);
if ($rs > 0) {
	while ($obj_personal = pg_fetch_object($query_personal_activos)){
		print_r($obj_personal);
		echo '<br>';
		//$varmondia1TipoIntervencion=$retornomondia1->TipoIntervencion;
		
		echo 'Ultima Intervencion<br>';
		$query_ultima_intervencion = pg_query("select * from [ComMovTab.Intervencion] CI
inner join [ComMovTab.Orden] O on CI.IdOrden=O.IdOrden
left join [GlobalMaster.Gps] GPS on O.Suministro=GPS.suministro 
where IdUsuario=".$obj_personal->IdUsuario." and FechaAtencion=(
select max(FechaAtencion) from [ComMovTab.Intervencion] CI where IdUsuario=".$obj_personal->IdUsuario."
group by IdUsuario)");
		$rs2=pg_num_rows($query_ultima_intervencion);
		if ($rs2 > 0) {
			while ($obj_intervencion = pg_fetch_object($query_ultima_intervencion)){
				print_r($obj_intervencion);
				echo '<br>';
			}
		}
	}
}*/
?>


<!DOCTYPE html>


<html lang="es">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="images/favicon.png">

    <title>Pexport</title>
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,400italic,700,800' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Raleway:100' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Open+Sans+Condensed:300,700' rel='stylesheet' type='text/css'>
  

    <!-- Bootstrap core CSS -->
    <link href="<?php echo FULL_URL;?>js/bootstrap/dist/css/bootstrap.css" rel="stylesheet" />
    <link rel="stylesheet" href="<?php echo FULL_URL;?>fonts/font-awesome-4/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo FULL_URL;?>js/jquery.magnific-popup/dist/magnific-popup.css" />
    
    <style type="text/css" title="currentStyle">
      thead input { width: 100% }
      input.search_init { color: #999 }
      p { text-align: left; }


      
    </style>


    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="../../assets/js/html5shiv.js"></script>
      <script src="../../assets/js/respond.min.js"></script>
    <![endif]-->
    <link rel="stylesheet" type="text/css" href="<?php echo FULL_URL;?>js/jquery.gritter/css/jquery.gritter.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo FULL_URL;?>js/jquery.nanoscroller/nanoscroller.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo FULL_URL;?>js/jquery.easypiechart/jquery.easy-pie-chart.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo FULL_URL;?>js/bootstrap.switch/bootstrap-switch.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo FULL_URL;?>js/bootstrap.datetimepicker/css/bootstrap-datetimepicker.min.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo FULL_URL;?>js/jquery.select2/select2.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo FULL_URL;?>js/bootstrap.slider/css/slider.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo FULL_URL;?>js/dropzone/css/dropzone.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo FULL_URL;?>js/jquery.niftymodals/css/component.css" />
    <!-- Custom styles for this template -->
    <link href="<?php echo FULL_URL;?>css/style.css" rel="stylesheet" />

</head>
<body>





  
  <!-- INICIO CODIGO MAPA -->
  
  <?php 
$str_rutas = '';
$i = 1;
$page = 0; 
$arr_tecnicos_activos = array();
//$query_personal_activos = pg_query("select distinct CU.IdUsuario, GMP.IdPersona, NomPersona, ApePersona from [ComMovTab.Intervencion] CI inner join [ComMovTab.Usuario] CU on CI.IdUsuario = CU.IdUsuario
//            inner join [GlobalMaster.Persona] GMP on CU.IdPersona = GMP.IdPersona");

$query_personal_activos = pg_query("
select consulta1.IdUsuario as IdUsuario,consulta1.NomPersona as NomPersona,consulta1.ApePersona as ApePersona,consulta1.Total as Generadas,
consulta2.TotalFin as Finalizadas,
((consulta1.Total)-consulta2.TotalFin) as Pendientes,
((consulta2.TotalFin*100)/consulta1.Total) as Porcentaje  from 
(
select ci.IdUsuario,gp.NomPersona,gp.ApePersona,count(ci.IdEstado) as Total  from ComMovTab_Intervencion ci
inner join ComMovTab_Orden co on ci.IdOrden=co.IdOrden
inner join ComMovTab_Usuario cu on ci.IdUsuario = cu.IdUsuario
inner join GlobalMaster_Persona gp on cu.IdPersona = gp.IdPersona
where co.SubActividad in (select DetalleTipoIntervencion from ComMovTab_TipoIntervencion) and co.Condicion='0' 
and FechaAtencion>='".$fecha_actual."'
group by ci.IdUsuario,gp.NomPersona,gp.ApePersona) as consulta1 
inner join 
(
select ci.IdUsuario,gp.NomPersona,gp.ApePersona,count(ci.IdEstado) as TotalFin  from ComMovTab_Intervencion ci
inner join ComMovTab_Orden co on ci.IdOrden=co.IdOrden
inner join ComMovTab_Usuario cu on ci.IdUsuario = cu.IdUsuario
inner join GlobalMaster_Persona gp on cu.IdPersona = gp.IdPersona
where co.SubActividad in (select DetalleTipoIntervencion from ComMovTab_TipoIntervencion)
and ci.IdEstado in (14,15) and co.Condicion='0' and FechaAtencion>='".$fecha_actual."'
group by ci.IdUsuario,gp.NomPersona,gp.ApePersona) as consulta2 on consulta1.IdUsuario = consulta2.IdUsuario");

$rs=pg_num_rows($query_personal_activos);
if ($rs > 0) {
while ($obj_personal = pg_fetch_object($query_personal_activos)){
	$arr_tecnicos_activos[$i] = $obj_personal;
	
  	//if($obj_personal->DownMovil>0){
	//left join [GlobalMaster.Gps] GPS on O.Suministro=GPS.suministro
	
  		$query_ultima_intervencion = pg_query("select * from ComMovTab_Intervencion CI
inner join ComMovTab_Orden O on CI.IdOrden=O.IdOrden 
left join GlobalMaster_Gps GPS on O.Suministro=GPS.suministro
where CI.IdEstado in (14,15) and IdUsuario=".$obj_personal->idusuario." order by FechaAtencion desc limit 1");
  		
  		$rs2=pg_num_rows($query_ultima_intervencion);
  		if ($rs2 > 0) {
  			$obj_intervencion = pg_fetch_object($query_ultima_intervencion);
  		}
  		
	if($obj_intervencion->latitud1!='' && $obj_intervencion->longitud1!=''){
		
		$arr_tecnicos_activos[$i]->mapa = 'OK';
		
	$color_flag = '72BAF7';
	

	$latitud_center = $obj_intervencion->latitud1;
	$longitud_center = $obj_intervencion->longitud1;
	
	$fechaejecucion = strtotime(date('Y-m-d H:i:s'));
	$fechaejecucion_tmp = strtotime($obj_intervencion->fechaatencion);
	$tiempo = $fechaejecucion - $fechaejecucion_tmp;

	$horas              = floor ( $tiempo / 3600 );
	$minutes            = ( ( $tiempo / 60 ) % 60 );
	$seconds            = ( $tiempo % 60 );
	 
	$time['horas']      = str_pad( $horas, 2, "0", STR_PAD_LEFT );
	$time['minutes']    = str_pad( $minutes, 2, "0", STR_PAD_LEFT );
	$time['seconds']    = str_pad( $seconds, 2, "0", STR_PAD_LEFT );
	 
	$str_tiempo               = implode( ':', $time );
	
	/*
	$fechainicio = strtotime($obj_lecturista['0']["fecha_hora_inicio"]);
	$fechafin = strtotime($obj_lecturista['0']["fecha_hora_fin"]);
	$tiempo_total = $fechafin - $fechainicio;
	
	$horas              = floor ( $tiempo_total / 3600 );
	$minutes            = ( ( $tiempo_total / 60 ) % 60 );
	$seconds            = ( $tiempo_total % 60 );
	
	$time['horas']      = str_pad( $horas, 2, "0", STR_PAD_LEFT );
	$time['minutes']    = str_pad( $minutes, 2, "0", STR_PAD_LEFT );
	$time['seconds']    = str_pad( $seconds, 2, "0", STR_PAD_LEFT );
	
	$TT               = implode( ':', $time );*/
	
	/*
	$inconsistencias_originales = $obj_lecturista['0']['lecturas_inconsistentes']+$obj_lecturista['0']['lecturas_evaluadas'];
	$inconsistencias_actuales = $inconsistencias_originales-$obj_lecturista['0']['lecturas_evaluadas'];
	if($inconsistencias_actuales==0){
		$style_inconcistencias_actuales = 'badge badge-success';
	}else{
		$style_inconcistencias_actuales = 'badge badge-danger';
	}
	
	$obs_total = $obj_lecturista['0']['obs_total'];
	$obs_sin_lectura = $obj_lecturista['0']['obs_sin_lectura'];
	$obs_con_lectura = $obj_lecturista['0']['obs_con_lectura'];
	*/
	$link_ver_mapa = '<a class=\"btn btn-info btn-sm btn\" target=\"_blank\" href=\"'.FULL_URL.'main/mapa_rutas?id='.$obj_personal->idusuario.'\"><i class=\"fa fa-globe\"></i> Ver Ruta de Intervenciones</a>';
	
	$str_estado = '';
	
	if($obj_personal->generadas>0 && $obj_personal->finalizadas==0){
		$str_estado = 'Estado del Proceso: <strong>SIN COMENZAR</strong>';
		$str_icon = FULL_URL.'images/markers/male-2-gris.png';
	}elseif($obj_personal->generadas>0 && $obj_personal->finalizadas>0 && 
			($obj_personal->pendientes)>0 && 
			($tiempo / 60) > 60){
		$str_estado = 'Estado del Proceso: <strong>EN PROCESO</strong>';
		$str_icon = FULL_URL.'images/markers/male-2-rojo.png';
	}elseif($obj_personal->generadas>0 && $obj_personal->finalizadas>0 &&
			($obj_personal->pendientes)>0 &&
			($tiempo / 60) > 30){
		$str_estado = 'Estado del Proceso: <strong>EN PROCESO</strong>';
		$str_icon = FULL_URL.'images/markers/male-2-naranja.png';
	}elseif($obj_personal->generadas>0 && $obj_personal->finalizadas>0 &&
			($obj_personal->pendientes)>0 &&
			($tiempo / 60) > 10){
		$str_estado = 'Estado del Proceso: <strong>EN PROCESO</strong>';
		$str_icon = FULL_URL.'images/markers/male-2-amarillo.png';
	}elseif($obj_personal->generadas>0 && $obj_personal->finalizadas>0 &&
			($obj_personal->pendientes)>0){
		$str_estado = 'Estado del Proceso: <strong>EN PROCESO</strong>';
		$str_icon = FULL_URL.'images/markers/male-2-azul.png';
	}elseif(($obj_personal->pendientes)==0){
		$str_estado = 'Estado del Proceso: <strong>FINALIZADO</strong>';
		$str_icon = FULL_URL.'images/markers/male-2-verde.png';
	}
	
	$str_rutas .="
		  var contentString = '<div id=\"content\"><div id=\"siteNotice\"></div><h1 id=\"firstHeading\" class=\"firstHeading\">".$obj_personal->nompersona.' '.$obj_personal->apepersona."</h1><div id=\"bodyContent\"><p>' +
		  		'Suministro: <strong>".$obj_intervencion->suministro."</strong><br>'+
				'Cliente: <strong>".utf8_decode($obj_intervencion->nombresuministro)."</strong><br>'+
				'Direcci&oacute;n: <strong>".$obj_intervencion->direccionsuministro."</strong><br>'+
		  		'Fecha de Ultima Intervenci&oacute;n: <strong>".$obj_intervencion->fechaatencion."</strong><br>'+
		  		'Tiempo Transcurrido desde la Ultima Intervenci&oacute;n: <strong>".$str_tiempo."</strong>'+
		  		'<br><br>'+
		  		'Generadas: <strong>".$obj_personal->generadas."</strong> '+
                'Terminadas: <strong>".$obj_personal->finalizadas."</strong> '+
                'Pendientes: <strong>".$obj_personal->pendientes."</strong><br>'+
                '".$str_estado."<br>'+
                '".$link_ver_mapa."'+
		  		'</p></div></div>';
		  var infowindow".($i+$page)." = new google.maps.InfoWindow({
		      content: contentString
		  });
		  var marker".($i+$page)." = new google.maps.Marker({
		    position: new google.maps.LatLng(".$obj_intervencion->latitud1.", ".$obj_intervencion->longitud1."),
		    title: '# ".($i+$page)."',
		    map: map,
		    icon: '".$str_icon."',
		    size: 'tiny'
		  });
		     var label".($i+$page)." = new Label({
		       map: map
		     });
		     label".($i+$page).".bindTo('position', marker".($i+$page).", 'position');
		     label".($i+$page).".bindTo('text', marker".($i+$page).", 'title');
		     
		  google.maps.event.addListener(marker".($i+$page).", 'click', function() {
			  infowindow".($i+$page).".open(map,marker".($i+$page).");
			});
		  ";
	}
  	//}
  	$i++;
}
}
?>
<!DOCTYPE html>
<html>
  <head>
    <meta name="viewport" content="initial-scale=1.0, user-scalable=no">
    <meta charset="utf-8">
    <title>Monitoreo de T&eacute;cnicos</title>
    <style>
      html, body, #map-canvas {
        height: 100%;
        margin: 0px;
        padding: 0px
      }
    </style>
    <script src="https://maps.googleapis.com/maps/api/js?v=3.exp"></script>
    <script src="<?php echo FULL_URL;?>js/google-maps-label.js"></script>
  </head>
  <body>

    <div id="arbol" class="page-aside app filters" style="display: none;">
        <div>
						<div style="display: none;">
						  <div id="legend" style="text-align: right;">
						  
						  <div class="">
			              <label class="control-label" style="background: white;padding-left: 3px;padding-right: 3px;">Sin comenzar</label>
			              <img src="<?php echo FULL_URL;?>images/markers/male-2-gris.png">
			              </div>
						  
						  <div class="">
			              <label class="control-label" style="background: white;padding-left: 3px;padding-right: 3px;">En Progreso</label>
			              <img src="<?php echo FULL_URL;?>images/markers/male-2-azul.png">
			              </div>
			              
			              <div class="">
			              <label class="control-label" style="background: white;padding-left: 3px;padding-right: 3px;">Finalizado</label>
			              <img src="<?php echo FULL_URL;?>images/markers/male-2-verde.png">
			              </div>
			              
			              <div class="">
			              <label class="control-label" style="background: white;padding-left: 3px;padding-right: 3px;">Inactivo hace mas 10 min</label>
			              <img src="<?php echo FULL_URL;?>images/markers/male-2-amarillo.png">
			              </div>
						  
						  <div class="">
			              <label class="control-label" style="background: white;padding-left: 3px;padding-right: 3px;">Inactivo hace mas 30 min</label>
			              <img src="<?php echo FULL_URL;?>images/markers/male-2-naranja.png">
			              </div>
			              
			              <div class="">
			              <label class="control-label" style="background: white;padding-left: 3px;padding-right: 3px;">Inactivo hace mas 1 hora</label>
			              <img src="<?php echo FULL_URL;?>images/markers/male-2-rojo.png">
			              </div>
			              
						  </div>
					    </div>
		  
      </div>
    </div>

    <div id="map-canvas"></div>
  </body>
</html>

<script type="text/javascript">
//function initialize() {
  var myLatlng = new google.maps.LatLng(<?php echo isset($latitud_center)?$latitud_center:'-6.77361';?>, <?php echo isset($longitud_center)?$longitud_center:'-79.8417';?>);
  var mapOptions = {
    zoom: 11,
    center: myLatlng
  }
  var map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);

	<?php echo $str_rutas;?>

//}

//google.maps.event.addDomListener(window, 'load', initialize);

function openInfo(i){
	$('.item_lecturista').each(function(){
		$k = $(this).data('item');
		if(i==$k){     						
	    	eval('infowindow'+$k+'.open(map,marker'+$k+');');
		}else{
			eval('infowindow'+$k+'.close();');
		}
	});
}

map.controls[google.maps.ControlPosition.RIGHT_BOTTOM].push(document.getElementById('legend'));

</script>
  
  <!-- FIN CODIGO MAPA -->
  
  


	<script src="<?php echo FULL_URL;?>js/jquery.js"></script>
	<script type="text/javascript" src="<?php echo FULL_URL;?>js/jquery.nestable/jquery.nestable.js"></script>
	<script type="text/javascript" src="<?php echo FULL_URL;?>js/jquery.nanoscroller/jquery.nanoscroller.js"></script>
	<script type="text/javascript" src="<?php echo FULL_URL;?>js/jquery.sparkline/jquery.sparkline.min.js"></script>
	<script type="text/javascript" src="<?php echo FULL_URL;?>js/jquery.easypiechart/jquery.easy-pie-chart.js"></script>
	<script type="text/javascript" src="<?php echo FULL_URL;?>js/behaviour/general.js"></script>
  	<script src="<?php echo FULL_URL;?>js/jquery.ui/jquery-ui.js" type="text/javascript"></script>
	<script type="text/javascript" src="<?php echo FULL_URL;?>js/jquery.nestable/jquery.nestable.js"></script>
	<script type="text/javascript" src="<?php echo FULL_URL;?>js/bootstrap.switch/bootstrap-switch.min.js"></script>
	<script type="text/javascript" src="<?php echo FULL_URL;?>js/bootstrap.datetimepicker/js/bootstrap-datetimepicker.min.js"></script>
	<script src="<?php echo FULL_URL;?>js/jquery.select2/select2.min.js" type="text/javascript"></script>
	<script src="<?php echo FULL_URL;?>js/bootstrap.slider/js/bootstrap-slider.js" type="text/javascript"></script>
	<script type="text/javascript" src="<?php echo FULL_URL;?>js/jquery.gritter/js/jquery.gritter.js"></script>
   
  <script type="text/javascript">
    $(document).ready(function(){
      //initialize the javascript
      App.init();
    });
  </script>
  <script type='text/javascript' src='<?php echo FULL_URL;?>js/jquery.fullcalendar/fullcalendar/fullcalendar.js'></script>

<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
  <script src="<?php echo FULL_URL;?>js/behaviour/voice-commands.js"></script>
  <script src="<?php echo FULL_URL;?>js/bootstrap/dist/js/bootstrap.min.js"></script>
<script type="text/javascript" src="<?php echo FULL_URL;?>js/jquery.flot/jquery.flot.js"></script>
<script type="text/javascript" src="<?php echo FULL_URL;?>js/jquery.flot/jquery.flot.pie.js"></script>
<script type="text/javascript" src="<?php echo FULL_URL;?>js/jquery.flot/jquery.flot.resize.js"></script>
<script type="text/javascript" src="<?php echo FULL_URL;?>js/jquery.flot/jquery.flot.labels.js"></script>
</body>
</html>