<?php

date_default_timezone_set('America/Lima');

//require_once "../conexion.php";
require_once(APP."View/Main/conexion.ctp");

$dia_semmana = date('w');
$fecha_ultima_semana=date('Y-m-d 23:59:59',strtotime('-1 weeks', strtotime(date("Y-m-d"))));

$query_grafico = pg_query("select consulta1.mes as Mes,consulta1.dia as Dia,consulta1.NomSemana as NomSemana,
consulta2.Total as Generadas,
consulta1.TotalFin as Finalizadas,
((consulta2.Total)-consulta1.TotalFin) as Pendientes,
((consulta1.TotalFin*100)/consulta2.Total) as Porcentaje,
consulta3.TotalFin as Infructuosa,
consulta4.Total as GeneradasEnsa from 
(select date_part('dow',FechaAtencion) as dia, extract('month' from FechaAtencion) as mes, to_char(FechaAtencion,'DAY') as NomSemana,count(ci.IdEstado) as TotalFin  from ComMovTab_Intervencion ci
inner join ComMovTab_Orden co on ci.IdOrden=co.IdOrden
where co.SubActividad in (select DetalleTipoIntervencion from ComMovTab_TipoIntervencion)
and ci.IdEstado in (14,15) and co.Condicion='0' and FechaAtencion>'".$fecha_ultima_semana."' 
group by extract('month' from FechaAtencion), date_part('dow',FechaAtencion), to_char(FechaAtencion,'DAY')) as consulta1 
left outer join 
(
select date_part('dow',FechaAsignado) as dia, extract('month' from FechaAsignado) as mes,count(ci.IdEstado) as Total  from ComMovTab_Intervencion ci
inner join ComMovTab_Orden co on ci.IdOrden=co.IdOrden
where co.SubActividad in (select DetalleTipoIntervencion from ComMovTab_TipoIntervencion) and co.Condicion='0' 
and FechaAsignado>'".$fecha_ultima_semana."'  
group by extract('month' from FechaAsignado), date_part('dow',FechaAsignado)) as consulta2 on consulta1.dia = consulta2.dia and consulta1.mes = consulta2.mes 
left outer join 
(
select date_part('dow',FechaAtencion) as dia, extract('month' from FechaAtencion) as mes,count(ci.IdEstado) as TotalFin  from ComMovTab_Intervencion ci
inner join ComMovTab_Orden co on ci.IdOrden=co.IdOrden
inner join ComMovTab_Detalle_Intervencion DI on DI.IdIntervencion=ci.IdIntervencion
where co.SubActividad in (select DetalleTipoIntervencion from ComMovTab_TipoIntervencion)
and co.Condicion='0' and DI.IdMaestrosIntervencion in (
select IdMaestrosIntervencion from ComMovTab_MaestrosIntervencion 
where IdentificadorMaestroIntervencion like 'LabelInfructuosa%') and FechaAtencion>'".$fecha_ultima_semana."' 
group by extract('month' from FechaAtencion), date_part('dow',FechaAtencion)) as consulta3 on consulta1.dia = consulta3.dia and consulta1.mes = consulta3.mes 
left outer join 
(
select date_part('dow',cast(FechaGeneracion as timestamp)) as dia, extract('month' from cast(FechaGeneracion as timestamp)) as mes,count(ci.IdEstado) as Total  from ComMovTab_Intervencion ci
inner join ComMovTab_Orden co on ci.IdOrden=co.IdOrden
where co.SubActividad in (select DetalleTipoIntervencion from ComMovTab_TipoIntervencion) and co.Condicion='0' 
and cast(FechaGeneracion as timestamp)>'".$fecha_ultima_semana."'  
group by extract('month' from cast(FechaGeneracion as timestamp)), date_part('dow',cast(FechaGeneracion as timestamp))) as consulta4 on consulta1.dia = consulta4.dia and consulta1.mes = consulta4.mes 
order by 1 desc,2 asc limit 7");


$rs=pg_num_rows($query_grafico);

$categorias = '';
$series = '';
$serie_generadas_ensa = '';
$serie_generadas = '';
$serie_finalizadas = '';
$serie_infructuosas = '';
if ($rs > 0) {
	
	$num_dias_semana = 0;//domingo
	while ($obj = pg_fetch_object($query_grafico)){
		/*if($dia_semmana==$obj->Dia){
			$categorias .= "'<b>".utf8_encode($obj->NomSemana)." ".date('m-d')."</b>',";
		}else{
			$categorias .= "'".utf8_encode($obj->NomSemana)."',";
		}*/
		if($num_dias_semana < $obj->dia){			
			for($j=$num_dias_semana;$j<$obj->dia;$j++){
				$serie_generadas_ensa .= "0,";
				$serie_generadas .= "0,";
				$serie_finalizadas .= "0,";
				$serie_infructuosas .= "0,";
				$num_dias_semana++;
			}
		}
		$serie_generadas_ensa .= $obj->generadasensa.",";
		$serie_generadas .= $obj->generadas.",";
		$serie_finalizadas .= $obj->finalizadas.",";
		$serie_infructuosas .= $obj->infructuosa.",";
		$num_dias_semana++;
	}
	
	if($num_dias_semana < 7){
		for($j=$num_dias_semana;$j<7;$j++){
			$serie_generadas_ensa .= "0,";
			$serie_generadas .= "0,";
			$serie_finalizadas .= "0,";
			$serie_infructuosas .= "0,";
			$num_dias_semana++;
		}
	}
	
	$serie_generadas_ensa = substr($serie_generadas_ensa,0,-1);
	$serie_generadas = substr($serie_generadas,0,-1);
	$serie_finalizadas = substr($serie_finalizadas,0,-1);
	$serie_infructuosas = substr($serie_infructuosas,0,-1);
	
	$series = "{name: 'Generadas Ensa',data: [".$serie_generadas_ensa."]}";
	$series .= ",{name: 'Recibidas Pexport',data: [".$serie_generadas."]}";
	$series .= ",{name: 'Finalizadas',data: [".$serie_finalizadas."]}";
	$series .= ",{name: 'Infructuosas',data: [".$serie_infructuosas."]}";
	
	switch ($dia_semmana){
		case 0:
			$categorias = "<strong>Domingo ".date('m-d')."</strong>',
			'Lunes ".date('m-d', mktime(0, 0, 0, date("m"), date("d")-6, date("Y")))."',
			'Martes ".date('m-d', mktime(0, 0, 0, date("m"), date("d")-5, date("Y")))."',
			'Miercoles ".date('m-d', mktime(0, 0, 0, date("m"), date("d")-4, date("Y")))."',
			'Jueves ".date('m-d', mktime(0, 0, 0, date("m"), date("d")-3, date("Y")))."',
			'Viernes ".date('m-d', mktime(0, 0, 0, date("m"), date("d")-2, date("Y")))."',
			'Sabado ".date('m-d', mktime(0, 0, 0, date("m"), date("d")-1, date("Y")))."'";
			break;
		case 1:
			$categorias = "'Domingo ".date('m-d', mktime(0, 0, 0, date("m"), date("d")-1, date("Y")))."',
			'<strong>Lunes ".date('m-d')."</strong>',
			'Martes ".date('m-d', mktime(0, 0, 0, date("m"), date("d")-6, date("Y")))."',
			'Miercoles ".date('m-d', mktime(0, 0, 0, date("m"), date("d")-5, date("Y")))."',
			'Jueves ".date('m-d', mktime(0, 0, 0, date("m"), date("d")-4, date("Y")))."',
			'Viernes ".date('m-d', mktime(0, 0, 0, date("m"), date("d")-3, date("Y")))."',
			'Sabado ".date('m-d', mktime(0, 0, 0, date("m"), date("d")-2, date("Y")))."'";
			break;
		case 2:
			$categorias = "'Domingo ".date('m-d', mktime(0, 0, 0, date("m"), date("d")-2, date("Y")))."',
			'Lunes ".date('m-d', mktime(0, 0, 0, date("m"), date("d")-1, date("Y")))."',
			'<strong>Martes ".date('m-d')."</strong>',
			'Miercoles ".date('m-d', mktime(0, 0, 0, date("m"), date("d")-6, date("Y")))."',
			'Jueves ".date('m-d', mktime(0, 0, 0, date("m"), date("d")-5, date("Y")))."',
			'Viernes ".date('m-d', mktime(0, 0, 0, date("m"), date("d")-4, date("Y")))."',
			'Sabado ".date('m-d', mktime(0, 0, 0, date("m"), date("d")-3, date("Y")))."'";
			break;
		case 3:
			$categorias = "'Domingo ".date('m-d', mktime(0, 0, 0, date("m"), date("d")-3, date("Y")))."',
			'Lunes ".date('m-d', mktime(0, 0, 0, date("m"), date("d")-2, date("Y")))."',
			'Martes ".date('m-d', mktime(0, 0, 0, date("m"), date("d")-1, date("Y")))."',
			'<strong>Miercoles ".date('m-d')."</strong>',
			'Jueves ".date('m-d', mktime(0, 0, 0, date("m"), date("d")-6, date("Y")))."',
			'Viernes ".date('m-d', mktime(0, 0, 0, date("m"), date("d")-5, date("Y")))."',
			'Sabado ".date('m-d', mktime(0, 0, 0, date("m"), date("d")-4, date("Y")))."'";
			break;
		case 4:
			$categorias = "'Domingo ".date('m-d', mktime(0, 0, 0, date("m"), date("d")-4, date("Y")))."',
			'Lunes ".date('m-d', mktime(0, 0, 0, date("m"), date("d")-3, date("Y")))."',
			'Martes ".date('m-d', mktime(0, 0, 0, date("m"), date("d")-2, date("Y")))."',
			'Miercoles ".date('m-d', mktime(0, 0, 0, date("m"), date("d")-1, date("Y")))."',
			'<strong>Jueves ".date('m-d')."</strong>',
			'Viernes ".date('m-d', mktime(0, 0, 0, date("m"), date("d")-6, date("Y")))."',
			'Sabado ".date('m-d', mktime(0, 0, 0, date("m"), date("d")-5, date("Y")))."'";
			break;
		case 5:
			$categorias = "'Domingo ".date('m-d', mktime(0, 0, 0, date("m"), date("d")-5, date("Y")))."',
			'Lunes ".date('m-d', mktime(0, 0, 0, date("m"), date("d")-4, date("Y")))."',
			'Martes ".date('m-d', mktime(0, 0, 0, date("m"), date("d")-3, date("Y")))."',
			'Miercoles ".date('m-d', mktime(0, 0, 0, date("m"), date("d")-2, date("Y")))."',
			'Jueves ".date('m-d', mktime(0, 0, 0, date("m"), date("d")-1, date("Y")))."',
			'<strong>Viernes ".date('m-d')."</strong>',
			'Sabado ".date('m-d', mktime(0, 0, 0, date("m"), date("d")-6, date("Y")))."'";
			break;
		case 6:
			$categorias = "'Domingo ".date('m-d', mktime(0, 0, 0, date("m"), date("d")-6, date("Y")))."',
			'Lunes ".date('m-d', mktime(0, 0, 0, date("m"), date("d")-5, date("Y")))."',
			'Martes ".date('m-d', mktime(0, 0, 0, date("m"), date("d")-4, date("Y")))."',
			'Miercoles ".date('m-d', mktime(0, 0, 0, date("m"), date("d")-3, date("Y")))."',
			'Jueves ".date('m-d', mktime(0, 0, 0, date("m"), date("d")-2, date("Y")))."',
			'Viernes ".date('m-d', mktime(0, 0, 0, date("m"), date("d")-1, date("Y")))."',
			'<strong>Sabado ".date('m-d')."</strong>'";
			break;
	}
}else{
	echo 'No hay datos';
	exit();
}

?>
<!DOCTYPE html>
<html lang="es" >
<head>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta http-equiv="content-type" content="text/html; charset=UTF-8">
<title>Grafico Pexport</title>

<script type="text/javascript" src="//code.jquery.com/jquery-1.9.1.js"></script>



<script type="text/javascript">//<![CDATA[ 

$(function () {
    $('#container').highcharts({
        chart: {
            type: 'bar'
        },
        title: {
            text: 'Evolución de rendimiento por semana'
        },
        subtitle: {
            text: 'Fuente: PEXPORT S.A.C'
        },
        xAxis: {
            categories: [<?php echo $categorias;?>],
            title: {
                text: null
            }
        },
        yAxis: {
            min: 0,
            title: {
                text: 'Cantidad de Ordenes',
                align: 'high'
            },
            labels: {
                overflow: 'justify'
            }
        },
        tooltip: {
            valueSuffix: ' ordenes'
        },
        plotOptions: {
            bar: {
                dataLabels: {
                    enabled: true
                }
            }
        },
        /*legend: {
            layout: 'vertical',
            align: 'right',
            verticalAlign: 'top',
            x: -40,
            y: 100,
            floating: true,
            borderWidth: 1,
            backgroundColor: ((Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'),
            shadow: true
        },*/
        credits: {
            enabled: false
        },
        series: [<?php echo $series;?>]
    });
});
//]]>  

</script>


</head>
<body>
	<!-- 
	<script src="http://code.highcharts.com/highcharts.js"></script>
	<script src="http://code.highcharts.com/modules/exporting.js"></script>
	 -->
	
	<script src="../js/jquery.high/highcharts.js"></script>
	<script src="../js/jquery.high/modules/exporting.js"></script>

	<div id="container" style="min-width: 310px; max-width: 800px; height: 375px; margin: 0 auto">
	</div>
	
</body>
</html>