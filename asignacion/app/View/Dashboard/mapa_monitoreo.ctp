<?php
date_default_timezone_set('America/Lima');
//require_once "../conexion.php";
require_once(APP."View/Main/conexion.ctp");
if(!isset($_SESSION['idusuario'])) header("location: ../index.php");

$fecha_actual	=	date('Y-m-d');

if(isset($_GET['c'])) {
	$criterio = $_GET['c'];
}else{
	$criterio = 'F';
}

if(isset($_GET['date_start'])) {
	$date_start = $_GET['date_start'];
}else{
	$date_start = date('Y-m-d');
}

if(isset($_GET['date_end'])) {
	$date_end = $_GET['date_end'];
}else{
	$date_end = date('Y-m-d');
}
?>


<!DOCTYPE html>
<html lang="es">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="<?php echo FULL_URL;?>images/favicon.png">

    <title>Pexport</title>
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,400italic,700,800' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Raleway:100' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Open+Sans+Condensed:300,700' rel='stylesheet' type='text/css'>
  

    <!-- Bootstrap core CSS -->
    <link href="<?php echo FULL_URL;?>js/bootstrap/dist/css/bootstrap.css" rel="stylesheet" />
    <link rel="stylesheet" href="<?php echo FULL_URL;?>fonts/font-awesome-4/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo FULL_URL;?>js/jquery.magnific-popup/dist/magnific-popup.css" />
    
    <style type="text/css" title="currentStyle">
      thead input { width: 100% }
      input.search_init { color: #999 }
      p { text-align: left; }


      
    </style>


    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="../../assets/js/html5shiv.js"></script>
      <script src="../../assets/js/respond.min.js"></script>
    <![endif]-->
    <link rel="stylesheet" type="text/css" href="<?php echo FULL_URL;?>js/jquery.gritter/css/jquery.gritter.css" />
    <script type="text/javascript" src="<?php echo FULL_URL;?>js/jquery.nestable/jquery.nestable.js"></script>
    <link rel="stylesheet" type="text/css" href="<?php echo FULL_URL;?>js/jquery.nanoscroller/nanoscroller.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo FULL_URL;?>js/jquery.easypiechart/jquery.easy-pie-chart.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo FULL_URL;?>js/bootstrap.switch/bootstrap-switch.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo FULL_URL;?>js/bootstrap.datetimepicker/css/bootstrap-datetimepicker.min.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo FULL_URL;?>js/jquery.select2/select2.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo FULL_URL;?>js/bootstrap.slider/css/slider.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo FULL_URL;?>js/dropzone/css/dropzone.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo FULL_URL;?>js/jquery.niftymodals/css/component.css" />
    <!-- Custom styles for this template -->
    <link href="../css/style.css" rel="stylesheet" />

</head>
<body>

  <!-- Fixed navbar -->
  <div id="head-nav" class="navbar navbar-default navbar-fixed-top" >
    <div class="container-fluid">
      
      <div class="navbar-collapse collapse">
        <ul class="nav navbar-nav">
			<li class="dropdown">
				<a href="#" class="dropdown-toggle" data-toggle="dropdown" >
					<i class="fa fa-desktop"></i><span> Opciones del sistema</span><b class="caret"></b>
				</a>

				<ul class="dropdown-menu">
					<li class="" onclick="list(this)">
					<?php echo $this->Html->link('Sincronizar',array('controller' => 'main','action' => 'sincronizar'));?>
					</li>
					<li class="" onclick="list(this)"><?php echo $this->Html->link('Asignar',array('controller' => 'main','action' => 'asignar'));?>
					</li>
					<li class="" onclick="list(this)"><?php echo $this->Html->link('Monitorear',array('controller' => 'main','action' => 'monitorear'));?>
					</li>
					<li class="" onclick="list(this)"><a
						href="<?php echo FULL_URL.'dashboard/mapa_monitoreo';?>"
						target="_blank" onclick="">Ubicacion GPS</a></li>
				
				</ul>
			</li>					
							
		</ul>

		<ul class="nav navbar-nav navbar-right user-nav" data-position="bottom" data-step="3" data-intro="<strong>Salir del Sistema</strong> <br/> Cierra tu sesion.">
   
           <div class="navbar-header" style="">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
              <span class="fa fa-gear"></span>
            </button>
            <a class="navbar-brand" href="#"><span>Pexport</span></a>
          </div>

          <li class="" id="" onclick="list(this)" style="background-color:#444;">
             <a href="<?php echo FULL_URL ?>usuarios/logout"> <i class="fa fa-close"></i>  Cerrar</a>
          </li>

        </ul>



      </div><!--/.nav-collapse animate-collapse -->

    </div>
  </div>


  <div id="cl-wrapper" class="fixed-menu">
  
  <!-- INICIO CODIGO MAPA -->
  
  <?php 
$str_rutas = '';
$i = 1;
$page = 0; 
$arr_tecnicos_activos = array();

if($criterio=='F'){
	$str_where_fecha = " and (cast(ci.FechaAtencion as date) BETWEEN '".$date_start."' AND '".$date_end."')";
}else{
	$str_where_fecha = "";
}
//$query_personal_activos = pg_query("select distinct CU.IdUsuario, GMP.IdPersona, NomPersona, ApePersona from ComMovTab_Intervencion CI inner join ComMovTab_Usuario CU on CI.IdUsuario = CU.IdUsuario
//            inner join GlobalMaster_Persona GMP on CU.IdPersona = GMP.IdPersona");

$query_personal_activos = pg_query("
select consulta1.IdUsuario as IdUsuario,consulta1.NomPersona as NomPersona,consulta1.ApePersona as ApePersona,consulta1.Total as Generadas,
consulta2.TotalFin as Finalizadas,
((consulta1.Total)-consulta2.TotalFin) as Pendientes,
((consulta2.TotalFin*100)/consulta1.Total) as Porcentaje  from 
(
select ci.IdUsuario,gp.NomPersona,gp.ApePersona,count(ci.IdEstado) as Total  from ComMovTab_Intervencion ci
inner join ComMovTab_Orden co on ci.IdOrden=co.IdOrden
inner join ComMovTab_Usuario cu on ci.IdUsuario = cu.IdUsuario
inner join GlobalMaster_Persona gp on cu.IdPersona = gp.IdPersona
where co.SubActividad in (select DetalleTipoIntervencion from ComMovTab_TipoIntervencion) and co.Condicion='0' 
".$str_where_fecha."
group by ci.IdUsuario,gp.NomPersona,gp.ApePersona) as consulta1 
inner join 
(
select ci.IdUsuario,gp.NomPersona,gp.ApePersona,count(ci.IdEstado) as TotalFin  from ComMovTab_Intervencion ci
inner join ComMovTab_Orden co on ci.IdOrden=co.IdOrden
inner join ComMovTab_Usuario cu on ci.IdUsuario = cu.IdUsuario
inner join GlobalMaster_Persona gp on cu.IdPersona = gp.IdPersona
where co.SubActividad in (select DetalleTipoIntervencion from ComMovTab_TipoIntervencion)
and ci.IdEstado in (14,15) and co.Condicion='0' ".$str_where_fecha."
group by ci.IdUsuario,gp.NomPersona,gp.ApePersona) as consulta2 on consulta1.IdUsuario = consulta2.IdUsuario");

$rs=pg_num_rows($query_personal_activos);
if ($rs > 0) {
while ($obj_personal = pg_fetch_object($query_personal_activos)){
	$arr_tecnicos_activos[$i] = $obj_personal;
	
  	//if($obj_personal->DownMovil>0){
	//left join GlobalMaster_Gps GPS on O.Suministro=GPS.suministro
	
  		$query_ultima_intervencion = pg_query("select * from ComMovTab_Intervencion CI
inner join ComMovTab_Orden O on CI.IdOrden=O.IdOrden 
left join GlobalMaster_Gps GPS on O.Suministro=GPS.suministro
where CI.IdEstado in (14,15) and IdUsuario=".$obj_personal->idusuario." order by FechaAtencion desc limit 1");
  		
  		$rs2=pg_num_rows($query_ultima_intervencion);
  		if ($rs2 > 0) {
  			$obj_intervencion = pg_fetch_object($query_ultima_intervencion);
  		}
  		
	if($obj_intervencion->latitud1!='' && $obj_intervencion->longitud1!='' && $obj_intervencion->latitud1!='0' && $obj_intervencion->longitud1!='0'){
		
		$arr_tecnicos_activos[$i]->mapa = 'OK';
		
	$color_flag = '72BAF7';
	

	$latitud_center = $obj_intervencion->latitud1;
	$longitud_center = $obj_intervencion->longitud1;
	
	$fechaejecucion = strtotime(date('Y-m-d H:i:s'));
	$fechaejecucion_tmp = strtotime($obj_intervencion->fechaatencion);
	$tiempo = $fechaejecucion - $fechaejecucion_tmp;

	$horas              = floor ( $tiempo / 3600 );
	$minutes            = ( ( $tiempo / 60 ) % 60 );
	$seconds            = ( $tiempo % 60 );
	 
	$time['horas']      = str_pad( $horas, 2, "0", STR_PAD_LEFT );
	$time['minutes']    = str_pad( $minutes, 2, "0", STR_PAD_LEFT );
	$time['seconds']    = str_pad( $seconds, 2, "0", STR_PAD_LEFT );
	 
	$str_tiempo               = implode( ':', $time );
	
	/*
	$fechainicio = strtotime($obj_lecturista['0']["fecha_hora_inicio"]);
	$fechafin = strtotime($obj_lecturista['0']["fecha_hora_fin"]);
	$tiempo_total = $fechafin - $fechainicio;
	
	$horas              = floor ( $tiempo_total / 3600 );
	$minutes            = ( ( $tiempo_total / 60 ) % 60 );
	$seconds            = ( $tiempo_total % 60 );
	
	$time['horas']      = str_pad( $horas, 2, "0", STR_PAD_LEFT );
	$time['minutes']    = str_pad( $minutes, 2, "0", STR_PAD_LEFT );
	$time['seconds']    = str_pad( $seconds, 2, "0", STR_PAD_LEFT );
	
	$TT               = implode( ':', $time );*/
	
	/*
	$inconsistencias_originales = $obj_lecturista['0']['lecturas_inconsistentes']+$obj_lecturista['0']['lecturas_evaluadas'];
	$inconsistencias_actuales = $inconsistencias_originales-$obj_lecturista['0']['lecturas_evaluadas'];
	if($inconsistencias_actuales==0){
		$style_inconcistencias_actuales = 'badge badge-success';
	}else{
		$style_inconcistencias_actuales = 'badge badge-danger';
	}
	
	$obs_total = $obj_lecturista['0']['obs_total'];
	$obs_sin_lectura = $obj_lecturista['0']['obs_sin_lectura'];
	$obs_con_lectura = $obj_lecturista['0']['obs_con_lectura'];
	*/
	$link_ver_mapa = '<a class=\"btn btn-info btn-sm btn\" target=\"_blank\" href=\"'.FULL_URL.'main/mapa_rutas?id='.$obj_personal->idusuario.'&c='.$criterio.'&date_start='.$date_start.'&date_end='.$date_end.'\"><i class=\"fa fa-globe\"></i> Ver Ruta de Intervenciones</a>';
	
	$str_estado = '';
	
	if($obj_personal->generadas>0 && $obj_personal->finalizadas==0){
		$str_estado = 'Estado del Proceso: <strong>SIN COMENZAR</strong>';
		$str_icon = '../images/markers/male-2-gris.png';
	}elseif($obj_personal->generadas>0 && $obj_personal->finalizadas>0 && 
			($obj_personal->pendientes)>0 && 
			($tiempo / 60) > 60){
		$str_estado = 'Estado del Proceso: <strong>EN PROCESO</strong>';
		$str_icon = '../images/markers/male-2-rojo.png';
	}elseif($obj_personal->generadas>0 && $obj_personal->finalizadas>0 &&
			($obj_personal->pendientes)>0 &&
			($tiempo / 60) > 30){
		$str_estado = 'Estado del Proceso: <strong>EN PROCESO</strong>';
		$str_icon = '../images/markers/male-2-naranja.png';
	}elseif($obj_personal->generadas>0 && $obj_personal->finalizadas>0 &&
			($obj_personal->pendientes)>0 &&
			($tiempo / 60) > 10){
		$str_estado = 'Estado del Proceso: <strong>EN PROCESO</strong>';
		$str_icon = '../images/markers/male-2-amarillo.png';
	}elseif($obj_personal->generadas>0 && $obj_personal->finalizadas>0 &&
			($obj_personal->pendientes)>0){
		$str_estado = 'Estado del Proceso: <strong>EN PROCESO</strong>';
		$str_icon = '../images/markers/male-2-azul.png';
	}elseif(($obj_personal->pendientes)==0){
		$str_estado = 'Estado del Proceso: <strong>FINALIZADO</strong>';
		$str_icon = '../images/markers/male-2-verde.png';
	}
	
	$str_rutas .="
		  var contentString = '<div id=\"content\"><div id=\"siteNotice\"></div><h1 id=\"firstHeading\" class=\"firstHeading\">".$obj_personal->nompersona.' '.$obj_personal->apepersona."</h1><div id=\"bodyContent\"><p>' +
		  		'Suministro: <strong>".$obj_intervencion->suministro."</strong><br>'+
				'Cliente: <strong>".utf8_decode($obj_intervencion->nombresuministro)."</strong><br>'+
				'Direcci&oacute;n: <strong>".utf8_decode($obj_intervencion->direccionsuministro)."</strong><br>'+
		  		'Fecha de Ultima Intervenci&oacute;n: <strong>".$obj_intervencion->fechaatencion."</strong><br>'+
		  		'Tiempo Transcurrido desde la Ultima Intervenci&oacute;n: <strong>".$str_tiempo."</strong>'+
		  		'<br><br>'+
		  		'Generadas: <strong>".$obj_personal->generadas."</strong> '+
                'Terminadas: <strong>".$obj_personal->finalizadas."</strong> '+
                'Pendientes: <strong>".$obj_personal->pendientes."</strong><br>'+
                '".$str_estado."<br>'+
                '".$link_ver_mapa."'+
		  		'</p></div></div>';
		  var infowindow".($i+$page)." = new google.maps.InfoWindow({
		      content: contentString
		  });
		  var marker".($i+$page)." = new google.maps.Marker({
		    position: new google.maps.LatLng(".$obj_intervencion->latitud1.", ".$obj_intervencion->longitud1."),
		    title: '# ".($i+$page)."',
		    map: map,
		    icon: '".$str_icon."',
		    size: 'tiny'
		  });
		     var label".($i+$page)." = new Label({
		       map: map
		     });
		     label".($i+$page).".bindTo('position', marker".($i+$page).", 'position');
		     label".($i+$page).".bindTo('text', marker".($i+$page).", 'title');
		     
		  google.maps.event.addListener(marker".($i+$page).", 'click', function() {
			  infowindow".($i+$page).".open(map,marker".($i+$page).");
			});
		  ";
	}
  	//}
  	$i++;
}
}
?>
<!DOCTYPE html>
<html>
  <head>
    <meta name="viewport" content="initial-scale=1.0, user-scalable=no">
    <meta charset="utf-8">
    <title>Monitoreo de T&eacute;cnicos</title>
    <style>
      html, body, #map-canvas {
        height: 100%;
        margin: 0px;
        padding: 0px
      }
    </style>
    <script src="https://maps.googleapis.com/maps/api/js?v=3.exp"></script>
    <script src="../js/google-maps-label.js"></script>
  </head>
  <body>
    
    <div class="container-fluid" id="pcont" style="height: 100%;margin: 0px;padding: 0px;">
    	<div id="map-canvas"></div>
    </div>
  	
  	<div id="arbol" class="page-aside app filters">
        <div>
        
        <div class="panel-group accordion" id="accordion">
        <div class="panel panel-default">
        
        	<div class="panel-heading">
				    <h4 class="panel-title">
						<a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseThree">
						    <i class="fa fa-angle-right"></i> Filtros
						</a>
				    </h4>
				</div>

				<div id="collapseThree" class="panel-collapse collapse" style="height: 0px;">
				    <div class="panel-body">
						
						<?php 
			            if(strpos($criterio, 'F') !== false){
							$show_tab2 = 'display: block;';
						}else{
							$show_tab2 = 'display:none;';
						}
						if(strpos($criterio, 'U') !== false){
							$show_tab1 = 'display: block;';
						}else{
							$show_tab1 = 'display:none;';
						}
			            ?>
	            
			            <div class="form-group">
			              <label class="control-label">Por :</label>
			              <input id="cbo_criterio_mapa" name="cbo_criterio_mapa" style="width:240px;" placeholder="Seleccione una opcion">
			            </div>
	            
			            <div class="por_fecha_asignacion" style="<?php echo $show_tab2;?>">
				            <div class="form-group">
				              <label class="control-label">Por Fecha de Asignaci&oacute;n (Inicio) :</label>
				              <div class="input-group date datetime" data-min-view="2" data-date-format="yyyy-mm-dd"  style='width:45%;' >
				                <input id="dpt_date_start" class="form-control" size="16" type="text" value="<?php echo $date_start;?>" readonly>
				                <span class="input-group-addon btn btn-primary"><span class="glyphicon glyphicon-th"></span></span>
				              </div>	
				            </div>

				            <div class="form-group">
				              <label class="control-label">Por Fecha de Asignaci&oacute;n (Fin) :</label>
				              <div class="input-group date datetime" data-min-view="2" data-date-format="yyyy-mm-dd"  style='width:45%;' >
				                <input id="dpt_date_end" class="form-control" size="16" type="text" value="<?php echo $date_end;?>" readonly>
				                <span class="input-group-addon btn btn-primary"><span class="glyphicon glyphicon-th"></span></span>
				              </div>	
				            </div>
				        </div>

	   
	           			<button class='btn btn-primary' id='generar-mapa'>Generar Mapa</button>
				  	</div>
				</div>
		  
		  	<div class="panel-heading">
			  <h4 class="panel-title">
				<a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
				  <i class="fa fa-angle-right"></i> T&eacute;cnicos
				</a>
			  </h4>
			</div>
			<div id="collapseOne" class="panel-collapse collapse in" style="height: auto;">
			  <div class="panel-body">
			  
			  <ol class="dd-list" style="max-height: 470px; overflow: auto;">
	            <?php 
	            foreach ($arr_tecnicos_activos as $k =>$obj_personal){
				?>
				<li class="dd-item" data-id="1">
                  <div class="dd-handle">
                  	<?php 
                  	//if($obj_personal->DownMovil>0 && $obj_lecturista['0']["lecturas_terminadas"]>0 && isset($obj_lecturista['0']['mapa']) && $obj_lecturista['0']['mapa']=='OK'){
                  	if($obj_personal->finalizadas>0 && isset($obj_personal->mapa) && $obj_personal->mapa=='OK'){
						$str_class_item = 'item_lecturista';
						$str_action = 'openInfo('.($k).');';
					}else{
						$str_class_item = '';
						$str_action = "alert('No hay datos para mostrar en el mapa');";
					}
					?>
                  <a class="<?php echo $str_class_item;?>" data-item="<?php echo ($k);?>" href="javascript: <?php echo $str_action;?>">
                  <div>
                  <?php echo ($k).' - '.$obj_personal->nompersona.' '.$obj_personal->apepersona;?>
                    <div class="progress progress-striped" style="margin-bottom: 0px" title ='<?php echo $obj_personal->porcentaje.'% '?> - <?php echo $obj_personal->finalizadas;?> <?php echo "Lecturas Terminadas" ?>'>
					  <div class="<?php if($obj_personal->porcentaje < '50'){ echo 'progress-bar progress-bar-danger';} else {echo 'progress-bar progress-bar-success';}?>" style="width: <?php echo $obj_personal->porcentaje.'% '?>; color: black; font-weight: bold;"><?php echo $obj_personal->porcentaje.'% '?>(<?php echo $obj_personal->finalizadas;?>)
					  </div>
					</div>
				  </div>
				  </a>
                  </div>
                </li>
				<?php 
				}
	            ?>    
	       		</ol>
			  
			  </div>
			</div>
			
			<div style="display: none;">
			  <div id="legend" style="text-align: right;">
			  
			  <div class="">
              <label class="control-label" style="background: white;padding-left: 3px;padding-right: 3px;">Sin comenzar</label>
              <img src="<?php echo FULL_URL;?>images/markers/male-2-gris.png">
              </div>
			  
			  <div class="">
              <label class="control-label" style="background: white;padding-left: 3px;padding-right: 3px;">En Progreso</label>
              <img src="<?php echo FULL_URL;?>images/markers/male-2-azul.png">
              </div>
              
              <div class="">
              <label class="control-label" style="background: white;padding-left: 3px;padding-right: 3px;">Finalizado</label>
              <img src="<?php echo FULL_URL;?>images/markers/male-2-verde.png">
              </div>
              
              <div class="">
              <label class="control-label" style="background: white;padding-left: 3px;padding-right: 3px;">Inactivo hace mas 10 min</label>
              <img src="<?php echo FULL_URL;?>images/markers/male-2-amarillo.png">
              </div>
			  
			  <div class="">
              <label class="control-label" style="background: white;padding-left: 3px;padding-right: 3px;">Inactivo hace mas 30 min</label>
              <img src="<?php echo FULL_URL;?>images/markers/male-2-naranja.png">
              </div>
              
              <div class="">
              <label class="control-label" style="background: white;padding-left: 3px;padding-right: 3px;">Inactivo hace mas 1 hora</label>
              <img src="<?php echo FULL_URL;?>images/markers/male-2-rojo.png">
              </div>
              
			  </div>
		  </div>
		  </div>
		  </div>
      </div>
    </div>
  <!-- FIN CODIGO MAPA -->
  
  
  </div>

	<script src="../js/jquery.js"></script>
	<script type="text/javascript" src="<?php echo FULL_URL;?>js/jquery.nanoscroller/jquery.nanoscroller.js"></script>
	<script type="text/javascript" src="<?php echo FULL_URL;?>js/jquery.sparkline/jquery.sparkline.min.js"></script>
	<script type="text/javascript" src="<?php echo FULL_URL;?>js/jquery.easypiechart/jquery.easy-pie-chart.js"></script>
	<script type="text/javascript" src="<?php echo FULL_URL;?>js/behaviour/general.js"></script>
  <script src="<?php echo FULL_URL;?>js/jquery.ui/jquery-ui.js" type="text/javascript"></script>
	<script type="text/javascript" src="<?php echo FULL_URL;?>js/jquery.nestable/jquery.nestable.js"></script>
	<script type="text/javascript" src="<?php echo FULL_URL;?>js/bootstrap.switch/bootstrap-switch.min.js"></script>
	<script type="text/javascript" src="<?php echo FULL_URL;?>js/bootstrap.datetimepicker/js/bootstrap-datetimepicker.min.js"></script>
	<script src="<?php echo FULL_URL;?>js/jquery.select2/select2.min.js" type="text/javascript"></script>
	<script src="<?php echo FULL_URL;?>js/bootstrap.slider/js/bootstrap-slider.js" type="text/javascript"></script>
	<script type="text/javascript" src="<?php echo FULL_URL;?>js/jquery.gritter/js/jquery.gritter.js"></script>
   
  <script type="text/javascript">
    $(document).ready(function(){
      //initialize the javascript
      App.init();
    });
  </script>
  <script type='text/javascript' src='<?php echo FULL_URL;?>js/jquery.fullcalendar/fullcalendar/fullcalendar.js'></script>

<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
  <script src="<?php echo FULL_URL;?>js/behaviour/voice-commands.js"></script>
  <script src="<?php echo FULL_URL;?>js/bootstrap/dist/js/bootstrap.min.js"></script>
<script type="text/javascript" src="<?php echo FULL_URL;?>js/jquery.flot/jquery.flot.js"></script>
<script type="text/javascript" src="<?php echo FULL_URL;?>js/jquery.flot/jquery.flot.pie.js"></script>
<script type="text/javascript" src="<?php echo FULL_URL;?>js/jquery.flot/jquery.flot.resize.js"></script>
<script type="text/javascript" src="<?php echo FULL_URL;?>js/jquery.flot/jquery.flot.labels.js"></script>

<script type="text/javascript">

var preload_data = [
	{ id: 'F', text: 'Fecha de Atencion'}
	//, { id: 'U', text: 'Ubicaci�n Geogr�fica'}
   	];


$('#cbo_criterio_mapa').select2({
    multiple: true
    ,query: function (query){
        var data = {results: []};

        $.each(preload_data, function(){
            if(query.term.length == 0 || this.text.toUpperCase().indexOf(query.term.toUpperCase()) >= 0 ){
                data.results.push({id: this.id, text: this.text });
            }
        });

        query.callback(data);
    }
});

$('#cbo_criterio_mapa').select2('data', [<?php if(strpos($criterio, 'F') !== false){ echo "{id: 'F', text: 'Fecha de Atencion'},";}if(strpos($criterio, 'U') !== false){echo "{id: 'U', text: 'Ubicaci�n Geogr�fica'}";}?>] )

$('#cbo_criterio_mapa').on("change",function (e) { 
	console.log("change "+e.val);
	if(e.val.indexOf("F")>-1){
		$('.por_fecha_asignacion').show();					
	}else{
		$('.por_fecha_asignacion').hide();
	}
	if(e.val.indexOf("U")>-1){
		$('.por_ubicacion').show();
	}else{
		$('.por_ubicacion').hide();
	}
});
                
                
    			
//function initialize() {
  var myLatlng = new google.maps.LatLng(<?php echo isset($latitud_center)?$latitud_center:'-6.77361';?>, <?php echo isset($longitud_center)?$longitud_center:'-79.8417';?>);
  var mapOptions = {
    zoom: 14,
    center: myLatlng
  }
  var map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);

	<?php echo $str_rutas;?>

//}

//google.maps.event.addDomListener(window, 'load', initialize);

function openInfo(i){
	$('.item_lecturista').each(function(){
		$k = $(this).data('item');
		if(i==$k){     						
	    	eval('infowindow'+$k+'.open(map,marker'+$k+');');
		}else{
			eval('infowindow'+$k+'.close();');
		}
	});
}

map.controls[google.maps.ControlPosition.RIGHT_BOTTOM].push(document.getElementById('legend'));

$('body').on('click','#generar-mapa',function(e){
  criterio = $('#cbo_criterio_mapa').val();
  date_start = $('#dpt_date_start').val();
  date_end = $('#dpt_date_end').val();
  window.open("<?php echo FULL_URL;?>dashboard/mapa_monitoreo?c="+criterio+"&date_start="+date_start+"&date_end="+date_end,"_top");
});
</script>
</body>
</html>