<div class="page-aside app tree" id="pageContent">
	<div class="fixed nano nscroller has-scrollbar">
		<div class="content" tabindex="0" style="right: -15px;">
			<div class="header">
				<button class="navbar-toggle" data-target=".treeview"
					data-toggle="collapse" type="button">
					<span class="fa fa-chevron-down"></span>
				</button>
				<h2 class="page-title">Objeto Atributo</h2>
				<p class="description">Mantenimiento</p>
			</div>
			<?php echo $this->element('menu_mantenimiento'); ?>
		</div>

		<div class="pane" style="display: block;">
			<div class="slider" style="height: 428px; top: 0px;"></div>
		</div>
	</div>
</div>
<div class="container-fluid" id="pcont">
	<div id="cabeza">
		<div class="page-head" style="padding: 0px 0px;">
			<ol class="breadcrumb">
				<li><button type="button" class="btn btn-default btn-xs"
						onclick="arbol()">
						<i class="fa fa-arrows-h"></i>
					</button></li>
				<li><a href="#">Mantenimiento</a></li>
				<li><a href="#">Objeto Atributo</a></li>
				<li class="active">Editar</li>
			</ol>
		</div>
	</div>
	<div class="cl-mcont">

		<div class="row">
			<div class="col-md-12">
				<div class="block-flat">
					<div class="header">
						<h3>Editar: Atributo de Objeto: <?php echo $obj_objeto_atributo->Objeto->getAttr('nombre');?></h3>
					</div>
					<div class="content">

						<form class="form-horizontal group-border-dashed" action="<?php echo FULL_URL;?>objeto_atributo/editar/<?php echo $obj_objeto_atributo->getID();?>" method="post" style="border-radius: 0px;">
							<input type="hidden" class="form-control" name="data[ObjetoAtributo][objeto_id]" id="ObjetoAtributoObjetoId" value="<?php echo $obj_objeto_atributo->getAttr('objeto_id');?>">
						
							<div class="form-group">
								<label class="col-sm-3 control-label">Tipo Atributo</label>
								<div class="col-sm-5">
									<select class="form-control" name="data[ObjetoAtributo][tipo_atributo_id]" id="ObjetoAtributoTipoAtributoId">
										<?php 
										foreach ($arr_obj_tipo_atributo as $k => $obj_tipo_atributo){
											$str_disabled = '';
											if($obj_objeto_atributo->getAttr('tipo_atributo_id')==$obj_tipo_atributo->getID()){
												$str_disabled = 'selected';	
											}
											?>
											<option value="<?php echo $obj_tipo_atributo->getID();?>" <?php echo $str_disabled;?>><?php echo $obj_tipo_atributo->getAttr('nombre');?></option>
										<?php }?>
									</select>
								</div>
								<div class="col-sm-1">
									<a href="<?php echo FULL_URL;?>tipo_atributo/nuevo" target="_blank"><i class="fa fa-plus-square"></i></a>
								</div>
							</div>
							
							<div class="form-group">
								<label class="col-sm-3 control-label">Valor</label>
								<div class="col-sm-6">
									<input type="text" class="form-control" name="data[ObjetoAtributo][valor]" id="ObjetoAtributoValor" value="<?php echo $obj_objeto_atributo->getAttr('valor');?>">
								</div>
							</div>

							<div class="form-group">
								<div class="col-sm-offset-3 col-sm-9">
									<button type="submit" class="btn btn-primary">Grabar</button>
									<a href="<?php echo FULL_URL;?>objeto_atributo/index/<?php echo $obj_objeto_atributo->getAttr('objeto_id');?>" class="btn btn-default">Cancelar</a>
								</div>
							</div>

						</form>

					</div>
				</div>
			</div>
		</div>
	</div>
</div>