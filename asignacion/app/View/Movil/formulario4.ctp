<?php
date_default_timezone_set('America/Lima');
/*timer = setTimeout('auto_reload()',5000);*/

require_once(APP."View/Main/conexion.ctp");
//require ("../conexion.php");

?>


<!DOCTYPE html> 
<html> 
<head> 
    <title>Pexport Movil</title>
    <meta charset="utf-8"> 
    <meta name="viewport" content="width=device-width, initial-scale=1"> 
    <link rel="stylesheet" href="<?php echo FULL_URL?>movil-s/jquery.mobile-1.3.2.min.css" />
    <script src="http://code.jquery.com/jquery-1.8.3.min.js"></script>
    <script src="<?php echo FULL_URL?>movil-s/jquery.mobile-1.3.2.min.js"></script>
</head> 
<body> 

<div data-role="page" data-theme="a">

    <div data-role="header">
        <h1><img src="images/logo_ensa4.gif" width="100px" height="35px"/></h1>
        <a href="main.php" data-icon="back" class="ui-btn-left" >Regresar</a>
        <a href="#modal" data-icon="refresh" class="ui-btn-right" >Opciones</a>
    </div><!-- /header -->

    <div data-role="content"  style="background-size:100%;">


            <div class="ui-bar ui-bar-a" >
                <div data-role="fieldcontain" class="ui-field-contain ui-body ui-br">
                <label for="name-a" class="ui-input-text">Solicitud:</label>
                <input type="text" name="name" id="name-a" value="100215214" >
                </div>
                <div data-role="fieldcontain" class="ui-field-contain ui-body ui-br">
                <label for="name-a" class="ui-input-text">Operacion:</label>
                <div class="ui-select">

                <select  data-native-menu="false" tabindex="-1" data-theme="e">
                    <option value="rush">Reclamo</option>
                    <option data-placeholder="true">Tipo</option>
                    <option value="standard">Instalacion Nueva</option>
                    <option value="standard">Remodelacion</option>
                    <option value="rush">Ampliacion de Potencia</option>
                    <option value="rush">Servicio Extraodinario</option>
                    <option value="rush">Reapertura</option>
                    <option value="rush">Inspeccion</option>
                    <option value="rush">Notificacion</option>
                    <option value="rush">Clandestinaje</option>
                    <option value="rush">Varios</option>
                    <option value="rush">Emergencia</option>
                    
                </select></div>

                </div>
            </div>



            <div class="ui-bar ui-bar-a">
            <div data-role="collapsible" data-collapsed="false" data-theme="e" data-content-theme="a" data-inset="false">
                <h3 style="width:200px;">Orden de Trabajo</h3>
                
                <div data-role="fieldcontain" class="ui-field-contain ui-body ui-br">
                <label for="name-a" class="ui-input-text">Suministro:</label>
                <input type="text" name="name" id="name-a" value="25230243" readonly="readonly">
                </div>
                <div data-role="fieldcontain" class="ui-field-contain ui-body ui-br">
                <label for="name-a" class="ui-input-text">Orden:</label>
                <input type="text" name="name" id="name-a" value="451454" readonly="readonly">
                </div>
                <div data-role="fieldcontain" class="ui-field-contain ui-body ui-br">
                <label for="name-a" class="ui-input-text">Fecha:</label>
                <input type="text" name="name" id="name-a" value="09-11-2013" readonly="readonly">
                </div>

                <div data-role="fieldcontain" class="ui-field-contain ui-body ui-br">
                <label for="name-a" class="ui-input-text">S Anterior:</label>
                <input type="text" name="name" id="name-a" value="" data-theme="c">
                </div>
                <div data-role="fieldcontain" class="ui-field-contain ui-body ui-br">
                <label for="name-a" class="ui-input-text">S Posterior:</label>
                <input type="text" name="name" id="name-a" value="" data-theme="c">
                </div>
            </div><!-- /collapsible -->
            </div>


           
        
            <div class="ui-bar ui-bar-a">
            <div data-role="collapsible" data-collapsed="false" data-theme="e" data-content-theme="a" data-inset="false">
                <h3 style="width:200px;">Responsables</h3>
                <div data-role="fieldcontain" class="ui-field-contain ui-body ui-br">
                <label for="name-a" class="ui-input-text">Responsable:</label>
                <input type="text" name="name" id="name-a" value="Pexport" >
                </div>
                <div data-role="fieldcontain" class="ui-field-contain ui-body ui-br">
                <label for="name-a" class="ui-input-text">Supervisor:</label>
                <input type="text" name="name" id="name-a" value="Pexport" >
                </div>
                <div data-role="fieldcontain" class="ui-field-contain ui-body ui-br">
                <label for="name-a" class="ui-input-text">Ejecutor:</label>
                <input type="text" name="name" id="name-a" value="Abel" >
                </div>
            </div><!-- /collapsible -->
            </div>


            <div class="ui-bar ui-bar-a">
            <div data-role="collapsible" data-collapsed="false" data-theme="e" data-content-theme="a" data-inset="false">
                <h3 style="width:200px;">Cliente</h3>
                <div data-role="fieldcontain" class="ui-field-contain ui-body ui-br">
                <label for="name-a" class="ui-input-text">Cliente:</label>
                <input type="text" name="name" id="name-a" value="Samuel Ventura Alva" >
                </div>
                <div data-role="fieldcontain" class="ui-field-contain ui-body ui-br">
                <label for="name-a" class="ui-input-text">Direccion:</label>
                <input type="text" name="name" id="name-a" value="Av Grau #229" >
                </div>
                <div data-role="fieldcontain" class="ui-field-contain ui-body ui-br">
                <label for="name-a" class="ui-input-text">Vivienda:</label>
                <div class="ui-select">
                <select data-native-menu="false" tabindex="-1" data-theme="b">
                    <option data-placeholder="true">Tipo</option>
                    <option value="standard">Propio</option>
                    <option value="standard">Familiar</option>
                    <option value="rush">Alquilado</option>
                </select></div>
                </div>


                <div data-role="fieldcontain" class="ui-field-contain ui-body ui-br">
                <label for="name-a" class="ui-input-text"></label>
                <div class="ui-select">
                <select data-native-menu="false" tabindex="-1" data-theme="b">
                    <option value="standard">Rustico</option>
                    <option data-placeholder="true">Construccion</option>
                    <option value="standard">Rustico</option>
                    <option value="standard">Noble</option>
                    <option value="rush">Terreno</option>
                    <option value="rush">Adobe</option>
                    <option value="rush">Adobe Noble</option>
                    <option value="rush">Construccion</option>
                </select></div>
                </div>

                <div data-role="fieldcontain" class="ui-field-contain ui-body ui-br">
                <label for="name-a" class="ui-input-text"></label>
                <div class="ui-select">
                <select  data-native-menu="false" tabindex="-1" data-theme="b">
                    <option data-placeholder="true" >Uso</option>
                    <option value="standard">Vivienda</option>
                    <option value="standard">Taller</option>
                    <option value="rush">Comercio</option>
                    <option value="rush">Industria</option>
                    <option value="rush">Edificio</option>
                    <option value="rush">Comunitario</option>
                </select></div>
                </div>

                <div data-role="fieldcontain" class="ui-field-contain ui-body ui-br">
                <label for="name-a" class="ui-input-text"></label>
                <div class="ui-select">
                <select  data-native-menu="false" tabindex="-1" data-theme="b">
                    <option data-placeholder="true">Situacion</option>
                    <option value="standard">Habilitado</option>
                    <option value="standard">Deshabilitado</option>
                    <option value="rush">Abandonado</option>
                    <option value="rush">Siniestrado</option>
                    <option value="rush">Ruina</option>
                </select></div>
                </div>



                <div data-role="fieldcontain" class="ui-field-contain ui-body ui-br">
                <label for="name-a" class="ui-input-text">Zona:</label>
                <input type="text" name="name" id="name-a" value="" >
                </div>
                <div data-role="fieldcontain" class="ui-field-contain ui-body ui-br">
                <label for="name-a" class="ui-input-text">Localidad:</label>
                <input type="text" name="name" id="name-a" value="" >
                </div>

                <div data-role="fieldcontain" class="ui-field-contain ui-body ui-br">
                <label for="name-a" class="ui-input-text">Estado 1</label>
                <a href="#" data-role="button" data-theme="c" data-inline="true"><img src="images/icono_camara.png" width="20px" height="16px"/> Camara</a>
                <a href="#" data-role="button" data-theme="c" data-inline="true">Galería</a>
                <a href="#" data-role="button" data-theme="c" data-inline="true">Borrar</a>
                </div>

                <div data-role="fieldcontain" class="ui-field-contain ui-body ui-br">
                <label for="name-a" class="ui-input-text">Vista Previa</label>
                <img src="images/medidor.jpg" width="150px" height="100px">
                </div>



            </div><!-- /collapsible -->
            </div>



        

            <div class="ui-bar ui-bar-a" >
            <div data-role="collapsible" data-collapsed="false" data-theme="e" data-content-theme="a" data-inset="false">
                <h3 style="width:200px;">Medidor</h3>
                <div data-role="fieldcontain" class="ui-field-contain ui-body ui-br">
                <label for="name-a" class="ui-input-text">Medidor</label>
                <div class="ui-select">

                <select  data-native-menu="false" tabindex="-1" data-theme="b">
                    <option data-placeholder="true">Tipo</option>
                    <option value="standard">Instalado</option>
                    <option value="standard">Retirado</option>
                    <option value="rush">Existente</option>

                </select></div>
                </div>

                <div data-role="fieldcontain" class="ui-field-contain ui-body ui-br">
                <label for="name-a" class="ui-input-text">Numero:</label>
                <input type="text" name="name" id="name-a" value="" >
                </div>
                <div data-role="fieldcontain" class="ui-field-contain ui-body ui-br">
                <label for="name-a" class="ui-input-text">Marca:</label>
                <input type="text" name="name" id="name-a" value="" >
                </div>
                <div data-role="fieldcontain" class="ui-field-contain ui-body ui-br">
                <label for="name-a" class="ui-input-text">Modelo:</label>
                <input type="text" name="name" id="name-a" value="" >
                </div>
                <div data-role="fieldcontain" class="ui-field-contain ui-body ui-br">
                <label for="name-a" class="ui-input-text">Lectura:</label>
                <input type="text" name="name" id="name-a" value="" data-theme="c">
                </div>
                <div data-role="fieldcontain" class="ui-field-contain ui-body ui-br">
                <label for="name-a" class="ui-input-text">Anio Fab:</label>
                <input type="text" name="name" id="name-a" value="" >
                </div>



            </div><!-- /collapsible -->
            </div>


            <div class="ui-bar ui-bar-a" >
            <div data-role="collapsible" data-collapsed="false" data-theme="e" data-content-theme="a" data-inset="false">
                <h3 style="width:200px;">Suministro</h3>

                <div data-role="fieldcontain" class="ui-field-contain ui-body ui-br">
                <label for="name-a" class="ui-input-text">Potencia:</label>
                <input type="text" name="name" id="name-a" value="" >
                </div>

                <div data-role="fieldcontain" class="ui-field-contain ui-body ui-br">
                <label for="name-a" class="ui-input-text">Tarifa:</label>
                <input type="text" name="name" id="name-a" value="" >
                </div>

                <div data-role="fieldcontain" class="ui-field-contain ui-body ui-br">
                <label for="name-a" class="ui-input-text">Suministro</label>
                <div class="ui-select">

                <select  data-native-menu="false" tabindex="-1" data-theme="b">
                    <option data-placeholder="true">Tipo</option>
                    <option value="standard">1 </option>
                    <option value="standard">3 </option>
                </select></div>
                </div>

                <div data-role="fieldcontain" class="ui-field-contain ui-body ui-br">
                <label for="name-a" class="ui-input-text">Sistema</label>
                <div class="ui-select">

                <select  data-native-menu="false" tabindex="-1" data-theme="b">
                    <option data-placeholder="true">Tipo</option>
                    <option value="standard">220 </option>
                    <option value="standard">380/220 </option>
                </select></div>
                </div>

                <div data-role="fieldcontain" class="ui-field-contain ui-body ui-br">
                <label for="name-a" class="ui-input-text">Fase</label>
                <div class="ui-select">

                <select  data-native-menu="false" tabindex="-1" data-theme="b">
                    <option data-placeholder="true">Tipo</option>
                    <option value="standard">R-S </option>
                    <option value="standard">R-T </option>
                    <option value="standard">T-S </option>
                </select></div>
                </div>

                <div data-role="fieldcontain" class="ui-field-contain ui-body ui-br">
                <label for="name-a" class="ui-input-text">Conexion</label>
                <div class="ui-select">

                <select  data-native-menu="false" tabindex="-1" data-theme="b">
                    <option data-placeholder="true">Tipo</option>
                    <option value="standard">Aérea </option>
                    <option value="standard">Subterranea </option>
                </select></div>
                </div>

                <div data-role="fieldcontain" class="ui-field-contain ui-body ui-br">
                <label for="name-a" class="ui-input-text">Subestacion:</label>
                <input type="text" name="name" id="name-a" value="" >
                </div>

                <div data-role="fieldcontain" class="ui-field-contain ui-body ui-br">
                <label for="name-a" class="ui-input-text">Sector:</label>
                <input type="text" name="name" id="name-a" value="" >
                </div>


            </div><!-- /collapsible -->
            </div>



            <div class="ui-bar ui-bar-a" >
            <div data-role="collapsible" data-collapsed="false"  data-theme="e" data-content-theme="b" data-inset="false">
                <h3 style="width:200px;">Artefactos</h3>

                        <table data-role="table" id="table-custom-2" data-mode="columntoggle" class="ui-body-d ui-shadow table-stripe ui-responsive" data-column-btn-theme="b" data-column-btn-text="Columnas a mostrar" data-column-popup-theme="a">
                         <thead>
                           <tr class="ui-bar-d">
                             <th data-priority="2">#</th>
                             <th data-priority="4">Nombre</th>
                             <th data-priority="3">Uso</th>
                             <th data-priority="1">Cantidad 1</th>
                             <th data-priority="5">Cantidad 2</th>
                           </tr>
                         </thead>
                         <tbody>
                           <tr>
                             <th>1</th>
                             <td>Computadora</td>
                             <td><select name="flip-1" id="flip-1" data-role="slider" data-mini="true">
                                    <option value="off">Off</option>
                                    <option value="on">On</option>
                                </select></td>
                             <td>
                                 <input type="range" name="range-10a" id="range-10a" min="0" max="10" step=".1" value="2.6">
                             </td>
                             <td>
                                 <input type="range" name="range-10a" id="range-10a" min="0" max="10" step=".1" value="2.6">
                             </td>
                           </tr>

                           <tr>
                             <th>2</th>
                             <td>Hervidor de Agua</td>
                             <td><select name="flip-1" id="flip-1" data-role="slider" data-mini="true">
                                    <option value="off">Off</option>
                                    <option value="on">On</option>
                                </select></td>
                             <td>
                                 <input type="range" name="range-10a" id="range-10a" min="0" max="10" step=".1" value="2.6">
                             </td>
                             <td>
                                 <input type="range" name="range-10a" id="range-10a" min="0" max="10" step=".1" value="2.6">
                             </td>
                           </tr>

                           <tr>
                             <th>3</th>
                             <td>Lavadora Automatica</td>
                             <td><select name="flip-1" id="flip-1" data-role="slider" data-mini="true">
                                    <option value="off">Off</option>
                                    <option value="on">On</option>
                                </select></td>
                             <td>
                                 <input type="range" name="range-10a" id="range-10a" min="0" max="10" step=".1" value="2.6">
                             </td>
                             <td>
                                 <input type="range" name="range-10a" id="range-10a" min="0" max="10" step=".1" value="2.6">
                             </td>
                           </tr>

                           <tr>
                             <th>4</th>
                             <td>Licuadora</td>
                             <td><select name="flip-1" id="flip-1" data-role="slider" data-mini="true">
                                    <option value="off">Off</option>
                                    <option value="on">On</option>
                                </select></td>
                             <td>
                                 <input type="range" name="range-10a" id="range-10a" min="0" max="10" step=".1" value="2.6">
                             </td>
                             <td>
                                 <input type="range" name="range-10a" id="range-10a" min="0" max="10" step=".1" value="2.6">
                             </td>
                           </tr>

                           <tr>
                             <th>5</th>
                             <td>Microondas</td>
                             <td><select name="flip-1" id="flip-1" data-role="slider" data-mini="true">
                                    <option value="off">Off</option>
                                    <option value="on">On</option>
                                </select></td>
                             <td>
                                 <input type="range" name="range-10a" id="range-10a" min="0" max="10" step=".1" value="2.6">
                             </td>
                             <td>
                                 <input type="range" name="range-10a" id="range-10a" min="0" max="10" step=".1" value="2.6">
                             </td>
                           </tr>

                           <tr>
                             <th>6</th>
                             <td>Olla Arrocera</td>
                             <td><select name="flip-1" id="flip-1" data-role="slider" data-mini="true">
                                    <option value="off">Off</option>
                                    <option value="on">On</option>
                                </select></td>
                             <td>
                                 <input type="range" name="range-10a" id="range-10a" min="0" max="10" step=".1" value="2.6">
                             </td>
                             <td>
                                 <input type="range" name="range-10a" id="range-10a" min="0" max="10" step=".1" value="2.6">
                             </td>
                           </tr>

                           <tr>
                             <th>7</th>
                             <td>Plancha</td>
                             <td><select name="flip-1" id="flip-1" data-role="slider" data-mini="true">
                                    <option value="off">Off</option>
                                    <option value="on">On</option>
                                </select></td>
                             <td>
                                 <input type="range" name="range-10a" id="range-10a" min="0" max="10" step=".1" value="2.6">
                             </td>
                             <td>
                                 <input type="range" name="range-10a" id="range-10a" min="0" max="10" step=".1" value="2.6">
                             </td>
                           </tr>

                           <tr>
                             <th>8</th>
                             <td>Refrigeradora</td>
                             <td><select name="flip-1" id="flip-1" data-role="slider" data-mini="true">
                                    <option value="off">Off</option>
                                    <option value="on">On</option>
                                </select></td>
                             <td>
                                 <input type="range" name="range-10a" id="range-10a" min="0" max="10" step=".1" value="2.6">
                             </td>
                             <td>
                                 <input type="range" name="range-10a" id="range-10a" min="0" max="10" step=".1" value="2.6">
                             </td>
                           </tr>

                           <tr>
                             <th>9</th>
                             <td>Televisor</td>
                             <td><select name="flip-1" id="flip-1" data-role="slider" data-mini="true">
                                    <option value="off">Off</option>
                                    <option value="on">On</option>
                                </select></td>
                             <td>
                                 <input type="range" name="range-10a" id="range-10a" min="0" max="10" step=".1" value="2.6">
                             </td>
                             <td>
                                 <input type="range" name="range-10a" id="range-10a" min="0" max="10" step=".1" value="2.6">
                             </td>
                           </tr>

                           <tr>
                             <th>10</th>
                             <td>Terma</td>
                             <td><select name="flip-1" id="flip-1" data-role="slider" data-mini="true">
                                    <option value="off">Off</option>
                                    <option value="on">On</option>
                                </select></td>
                             <td>
                                 <input type="range" name="range-10a" id="range-10a" min="0" max="10" step=".1" value="2.6">
                             </td>
                             <td>
                                 <input type="range" name="range-10a" id="range-10a" min="0" max="10" step=".1" value="2.6">
                             </td>
                           </tr>

                           <tr>
                             <th>11</th>
                             <td>Cafetera</td>
                             <td><select name="flip-1" id="flip-1" data-role="slider" data-mini="true">
                                    <option value="off">Off</option>
                                    <option value="on">On</option>
                                </select></td>
                             <td>
                                 <input type="range" name="range-10a" id="range-10a" min="0" max="10" step=".1" value="2.6">
                             </td>
                             <td>
                                 <input type="range" name="range-10a" id="range-10a" min="0" max="10" step=".1" value="2.6">
                             </td>
                           </tr>

                           <tr>
                             <th>12</th>
                             <td>Ventilador</td>
                             <td><select name="flip-1" id="flip-1" data-role="slider" data-mini="true">
                                    <option value="off">Off</option>
                                    <option value="on">On</option>
                                </select></td>
                             <td>
                                 <input type="range" name="range-10a" id="range-10a" min="0" max="10" step=".1" value="2.6">
                             </td>
                             <td>
                                 <input type="range" name="range-10a" id="range-10a" min="0" max="10" step=".1" value="2.6">
                             </td>
                           </tr>

                         </tbody>
                       </table>
            </div>

            </div>


            <div class="ui-bar ui-bar-a" >
                    <div data-role="collapsible" data-collapsed="false" data-theme="e" data-content-theme="a" data-inset="false">
                        <h3 style="width:200px;">Observaciones</h3>
                        <div data-role="fieldcontain" class="ui-field-contain ui-body ui-br">
                        <label for="name-a" class="ui-input-text">Codigo</label>
                        <div class="ui-select">

                        <select  data-native-menu="false" tabindex="-1" data-theme="b">
                            <option data-placeholder="true">Tipo</option>
                            <option value="standard">Infructuosa</option>
                            <option value="standard">Exceso de Consumo 1</option>
                            <option value="rush">Exceso de Consumo 2</option>
                            <option value="rush">Quema Artefacto</option>
                            <option value="rush">Ruteo</option>
                            <option value="rush">Cambio de Nombre</option>
                            <option value="rush">Cambio de Nombre y Direccion</option>
                        </select></div>
                        </div>

                        <div data-role="fieldcontain" class="ui-field-contain ui-body ui-br">
                        <label for="name-a" class="ui-input-text">Dinamico:</label>

                        </div>




                    </div><!-- /collapsible -->
         </div>


         <div class="ui-bar ui-bar-a" >
                    <div data-role="collapsible" data-collapsed="false" data-theme="e" data-content-theme="a" data-inset="false">
                        <h3 style="width:200px;">Validacion</h3>

                        <div data-role="fieldcontain" class="ui-field-contain ui-body ui-br">
                        <label for="name-a" class="ui-input-text">Estado 2</label>
                        <a href="#" data-role="button" data-theme="c" data-inline="true"><img src="../images/icono_camara.png" width="20px" height="16px"/> Camara</a>
                        <a href="#" data-role="button" data-theme="c" data-inline="true">Galería</a>
                        <a href="#" data-role="button" data-theme="c" data-inline="true">Borrar</a>
                        </div>

                        <div data-role="fieldcontain" class="ui-field-contain ui-body ui-br">
                        <label for="name-a" class="ui-input-text">Vista Previa</label>
                        <img src="images/medidor.jpg" width="150px" height="100px">
                        </div>

                        

                        <div data-role="fieldcontain" class="ui-field-contain ui-body ui-br">
                        <label for="name-a" class="ui-input-text">Usuario</label>
                        <div class="ui-select">

                        <select  data-native-menu="false" tabindex="-1" data-theme="b">
                            <option data-placeholder="true">Tipo</option>
                            <option value="standard">Titular</option>
                            <option value="standard">Representante</option>
                        </select></div>
                        </div>

                        <div data-role="fieldcontain" class="ui-field-contain ui-body ui-br">
                        <label for="name-a" class="ui-input-text">Nombre</label>
                        <input type="text" name="name" id="name-a" value="" data-theme="c">
                        </div>

                        <div data-role="fieldcontain" class="ui-field-contain ui-body ui-br">
                        <label for="name-a" class="ui-input-text">DNI:</label>
                        <input type="text" name="name" id="name-a" value="" data-theme="c">
                        </div>

                        <div data-role="fieldcontain" class="ui-field-contain ui-body ui-br">
                        <label for="textarea-6">Firma</label>
                        <textarea style="height:230px;" data-theme="c" cols="40" rows="8" name="textarea-6" id="textarea-6" placeholder="Firma Digital del Cliente:"></textarea>
                        </div>

                        <div data-role="fieldcontain" class="ui-field-contain ui-body ui-br">
                        <label for="name-a" class="ui-input-text">Tecnico</label>
                        <input type="text" name="name" id="name-a" value="" >
                        </div>

                        <div data-role="fieldcontain" class="ui-field-contain ui-body ui-br">
                        <label for="textarea-6">Firma</label>
                        <textarea style="height:230px;" data-theme="c" cols="40" rows="8" name="textarea-6" id="textarea-6" placeholder="Firma Digital del Tecnico:"></textarea>
                        </div>

                        

                    </div><!-- /collapsible -->
         </div>


         <div class="ui-bar ui-bar-a" >
                <div data-role="fieldcontain" class="ui-field-contain ui-body ui-br">
                <label for="name-a" class="ui-input-text">Accion:</label>
                <a href="#" data-role="button" data-theme="a" data-inline="true">Grabar Formulario</a>
                <a href="main.php" data-role="button" data-theme="a" data-inline="true">Cancelar</a>
                </div>


        </div>

        
    </div><!-- /content -->

</div><!-- /page -->


<div data-role="dialog" id="modal" data-theme="a">

    <div data-role="content" data-theme="a" class="ui-content ui-body-a" role="main">
        <h3>Opciones</h3>
        <a href="dialog-success.html" data-theme="b" data-role="button">Personalizacion</a>     
        <a href="index.php" data-theme="b" data-role="button">Cerrar Aplicacion</a>     
        <a href="main.php" data-theme="b" data-role="button">Cancelar</a>     
    </div>
</div>

</body>
</html>