<!DOCTYPE html> 
<html> 
<head> 
    <title>Pexport Movil</title>
    <meta charset="utf-8"> 
    <meta name="viewport" content="width=device-width, initial-scale=1"> 
    <link rel="stylesheet" href="http://code.jquery.com/mobile/1.2.1/jquery.mobile-1.2.1.min.css" />
    <script src="http://code.jquery.com/jquery-1.8.3.min.js"></script>
    <script src="http://code.jquery.com/mobile/1.2.1/jquery.mobile-1.2.1.min.js"></script>



</head> 
<body> 

<div data-role="page" data-theme="a">

    <div data-role="header">
    <h1><img src="<?php echo FULL_URL?>movil-s/images/logo_ensa4.gif" width="100px" height="35px"/></h1>
    <a href="#tec" data-icon="refresh" class="ui-btn-left"  >Sincronizar</a>
    <a href="#artefactos" data-icon="refresh" class="ui-btn-right" >Opciones</a>


    </div>

    <div  data-role="content" >


       <div data-role="collapsible-set" >   
        <div data-role="collapsible" data-collapsed="false" data-theme="b" data-content-theme="c">
            <h3>Asignaciones Pendientes</h3>
            
            <ul data-role="listview" data-inset="true" data-filter="true" data-ajax="false" data-filter-placeholder="Filtra datos" >


                        <li data-corners="true" data-shadow="false" data-iconshadow="true" data-wrapperels="div" data-icon="arrow-r" data-iconpos="right" data-theme="e" class="ui-btn ui-btn-icon-right ui-li-has-arrow ui-li ui-btn-up-c">
          
                                <a href="<?php echo FULL_URL?>movil/formulario5" class="ui-link-inherit">
                                    <p class="ui-li-aside ui-li-desc"><strong>Fecha Asignacion</strong></p>
                                    <h3 class="ui-li-heading">20020001 Chiclayo SectorA Ruta 2</h3>
                                    <p class="ui-li-desc"><strong>Av Grau #229 - Samuel Ventura Alva</strong></p>
                                    <p class="ui-li-desc">Reclamo - Exceso de Consumo</p>
                                </a>
                        </li>

                        <li data-corners="true" data-shadow="false" data-iconshadow="true" data-wrapperels="div" data-icon="arrow-r" data-iconpos="right" data-theme="e" class="ui-btn ui-btn-icon-right ui-li-has-arrow ui-li ui-btn-up-c">
          
                                <a href="<?php echo FULL_URL?>movil/formulario5" class="ui-link-inherit">
                                    <p class="ui-li-aside ui-li-desc"><strong>Fecha Asignacion</strong></p>
                                    <h3 class="ui-li-heading">20020341 Zona1 Sector2 Ruta 1</h3>
                                    <p class="ui-li-desc"><strong>Direccion Completa 2 Usuario 2 - Nombre Completo 2</strong></p>
                                    <p class="ui-li-desc">Tipo de Operacion - Otro Detalle</p>
                                </a>
                        </li>

                        <li data-corners="true" data-shadow="false" data-iconshadow="true" data-wrapperels="div" data-icon="arrow-r" data-iconpos="right" data-theme="e" class="ui-btn ui-btn-icon-right ui-li-has-arrow ui-li ui-btn-up-c">
          
                                <a href="<?php echo FULL_URL?>movil/formulario4" class="ui-link-inherit">
                                    <p class="ui-li-aside ui-li-desc"><strong>Fecha Asignacion</strong></p>
                                    <h3 class="ui-li-heading">20020321 Zona1 Sector2 Ruta 1</h3>
                                    <p class="ui-li-desc"><strong>Direccion Completa 3 Usuario 3 - Nombre Completo 3</strong></p>
                                    <p class="ui-li-desc">Tipo de Operacion - Otro Detalle</p>
                                </a>
                        </li>

                        <li data-corners="true" data-shadow="false" data-iconshadow="true" data-wrapperels="div" data-icon="arrow-r" data-iconpos="right" data-theme="e" class="ui-btn ui-btn-icon-right ui-li-has-arrow ui-li ui-btn-up-c">
          
                                <a href="<?php echo FULL_URL?>movil/formulario4" class="ui-link-inherit">
                                    <p class="ui-li-aside ui-li-desc"><strong>Fecha Asignacion</strong></p>
                                    <h3 class="ui-li-heading">21020341 Zona2 Sector1 Ruta 4</h3>
                                    <p class="ui-li-desc"><strong>Direccion Completa 4 Usuario 4 - Nombre Completo 4</strong></p>
                                    <p class="ui-li-desc">Tipo de Operacion - Otro Detalle</p>
                                </a>
                        </li>

                        <li data-corners="true" data-shadow="false" data-iconshadow="true" data-wrapperels="div" data-icon="arrow-r" data-iconpos="right" data-theme="e" class="ui-btn ui-btn-icon-right ui-li-has-arrow ui-li ui-btn-up-c">
          
                                <a href="<?php echo FULL_URL?>movil/formulario4" class="ui-link-inherit">
                                    <p class="ui-li-aside ui-li-desc"><strong>Fecha Asignacion</strong></p>
                                    <h3 class="ui-li-heading">20028781 Zona2 Sector2 Ruta 3</h3>
                                    <p class="ui-li-desc"><strong>Direccion Completa 5 Usuario 5 - Nombre Completo 5</strong></p>
                                    <p class="ui-li-desc">Tipo de Operacion - Otro Detalle</p>
                                </a>
                        </li>

                        <li data-corners="true" data-shadow="false" data-iconshadow="true" data-wrapperels="div" data-icon="arrow-r" data-iconpos="right" data-theme="e" class="ui-btn ui-btn-icon-right ui-li-has-arrow ui-li ui-btn-up-c">
          
                                <a href="<?php echo FULL_URL?>movil/formulario4" class="ui-link-inherit">
                                    <p class="ui-li-aside ui-li-desc"><strong>Fecha Asignacion</strong></p>
                                    <h3 class="ui-li-heading">20320501 Zona3 Sector1 Ruta 1</h3>
                                    <p class="ui-li-desc"><strong>Direccion Completa 6 Usuario 6 - Nombre Completo 6</strong></p>
                                    <p class="ui-li-desc">Tipo de Operacion - Otro Detalle</p>
                                </a>
                        </li>

                        <li data-corners="true" data-shadow="false" data-iconshadow="true" data-wrapperels="div" data-icon="arrow-r" data-iconpos="right" data-theme="e" class="ui-btn ui-btn-icon-right ui-li-has-arrow ui-li ui-btn-up-c">
          
                                <a href="<?php echo FULL_URL?>movil/formulario4" class="ui-link-inherit">
                                    <p class="ui-li-aside ui-li-desc"><strong>Fecha Asignacion</strong></p>
                                    <h3 class="ui-li-heading">21020341 Zona1 Sector2 Ruta 1</h3>
                                    <p class="ui-li-desc"><strong>Direccion Completa 7 Usuario 7 - Nombre Completo 7</strong></p>
                                    <p class="ui-li-desc">Tipo de Operacion - Otro Detalle</p>
                                </a>
                        </li>


               


                    </ul>
                    
        </div>
        <div data-role="collapsible"  data-theme="b"  data-content-theme="c">
            <h3>Asignaciones Finalizadas</h3>


                <ul data-role="listview" data-inset="true" data-filter="true" data-ajax="false" data-filter-placeholder="Filtra datos" >
                
                        <li data-corners="true" data-shadow="false" data-iconshadow="true" data-wrapperels="div" data-icon="arrow-r" data-iconpos="right"  class="ui-btn ui-btn-icon-right ui-li-has-arrow ui-li ui-btn-up-c">
          
                                <a href="<?php echo FULL_URL?>movil/formulario4" class="ui-link-inherit">
                                    <img src="<?php echo FULL_URL?>movil/images/medidor.jpg" class="ui-li-thumb">
                                    <p class="ui-li-aside ui-li-desc"><strong>Fecha</strong>Hora</p>

                                    <h3 class="ui-li-heading">22220001 Zona1 Sector2 Ruta 1</h3>
                                    <p class="ui-li-desc"><strong>Direccion Completa 8 Usuario 8 - Nombre Completo 8</strong></p>
                                    <p class="ui-li-desc">Tipo de Operacion - Otro Detalle</p>
                                </a>
                        </li>

                        <li data-corners="true" data-shadow="false" data-iconshadow="true" data-wrapperels="div" data-icon="arrow-r" data-iconpos="right"  class="ui-btn ui-btn-icon-right ui-li-has-arrow ui-li ui-btn-up-c">
          
                                <a href="<?php echo FULL_URL?>movil/formulario4" class="ui-link-inherit">
                                    <img src="<?php echo FULL_URL?>movil/images/medidor.jpg" class="ui-li-thumb">
                                    <p class="ui-li-aside ui-li-desc"><strong>Fecha</strong>Hora</p>

                                    <h3 class="ui-li-heading">23423001 Zona1 Sector2 Ruta 1</h3>
                                    <p class="ui-li-desc"><strong>Direccion Completa 9 Usuario 9 - Nombre Completo 9</strong></p>
                                    <p class="ui-li-desc">Tipo de Operacion - Otro Detalle</p>
                                </a>
                        </li>

                        <li data-corners="true" data-shadow="false" data-iconshadow="true" data-wrapperels="div" data-icon="arrow-r" data-iconpos="right"  class="ui-btn ui-btn-icon-right ui-li-has-arrow ui-li ui-btn-up-c">
          
                                <a href="<?php echo FULL_URL?>movil/formulario4" class="ui-link-inherit">
                                    <img src="<?php echo FULL_URL?>movil/images/medidor.jpg" class="ui-li-thumb">
                                    <p class="ui-li-aside ui-li-desc"><strong>Fecha</strong>Hora</p>

                                    <h3 class="ui-li-heading">20026581 Zona2 Sector3 Ruta 4</h3>
                                    <p class="ui-li-desc"><strong>Direccion Completa 10 Usuario 10 - Nombre Completo 10</strong></p>
                                    <p class="ui-li-desc">Tipo de Operacion - Otro Detalle</p>
                                </a>
                        </li>

                        <li data-corners="true" data-shadow="false" data-iconshadow="true" data-wrapperels="div" data-icon="arrow-r" data-iconpos="right"  class="ui-btn ui-btn-icon-right ui-li-has-arrow ui-li ui-btn-up-c">
          
                                <a href="<?php echo FULL_URL?>movil/formulario4" class="ui-link-inherit">
                                    <img src="<?php echo FULL_URL?>movil/images/medidor.jpg" class="ui-li-thumb">
                                    <p class="ui-li-aside ui-li-desc"><strong>Fecha</strong>Hora</p>

                                    <h3 class="ui-li-heading">20585001 Zona1 Sector2 Ruta 1</h3>
                                    <p class="ui-li-desc"><strong>Direccion Completa 11 Usuario 11 - Nombre Completo 11</strong></p>
                                    <p class="ui-li-desc">Tipo de Operacion - Otro Detalle</p>
                                </a>
                        </li>

                        <li data-corners="true" data-shadow="false" data-iconshadow="true" data-wrapperels="div" data-icon="arrow-r" data-iconpos="right"  class="ui-btn ui-btn-icon-right ui-li-has-arrow ui-li ui-btn-up-c">
          
                                <a href="<?php echo FULL_URL?>movil/formulario4" class="ui-link-inherit">
                                    <img src="<?php echo FULL_URL?>movil/images/medidor.jpg" class="ui-li-thumb">
                                    <p class="ui-li-aside ui-li-desc"><strong>Fecha</strong>Hora</p>

                                    <h3 class="ui-li-heading">20659001 Zona1 Sector2 Ruta 1</h3>
                                    <p class="ui-li-desc"><strong>Direccion Completa 12 Usuario 12 - Nombre Completo 12</strong></p>
                                    <p class="ui-li-desc">Tipo de Operacion - Otro Detalle</p>
                                </a>
                        </li>

                        <li data-corners="true" data-shadow="false" data-iconshadow="true" data-wrapperels="div" data-icon="arrow-r" data-iconpos="right"  class="ui-btn ui-btn-icon-right ui-li-has-arrow ui-li ui-btn-up-c">
          
                                <a href="formulario4.php" class="ui-link-inherit">
                                    <img src="<?php echo FULL_URL?>movil/images/medidor.jpg" class="ui-li-thumb">
                                    <p class="ui-li-aside ui-li-desc"><strong>Fecha</strong>Hora</p>

                                    <h3 class="ui-li-heading">20088801 Zona1 Sector2 Ruta 1</h3>
                                    <p class="ui-li-desc"><strong>Direccion Completa 13 Usuario 13 - Nombre Completo 13</strong></p>
                                    <p class="ui-li-desc">Tipo de Operacion - Otro Detalle</p>
                                </a>
                        </li>

                        <li data-corners="true" data-shadow="false" data-iconshadow="true" data-wrapperels="div" data-icon="arrow-r" data-iconpos="right"  class="ui-btn ui-btn-icon-right ui-li-has-arrow ui-li ui-btn-up-c">
          
                                <a href="<?php echo FULL_URL?>movil/formulario4" class="ui-link-inherit">
                                    <img src="<?php echo FULL_URL?>movil/images/medidor.jpg" class="ui-li-thumb">
                                    <p class="ui-li-aside ui-li-desc"><strong>Fecha</strong>Hora</p>

                                    <h3 class="ui-li-heading">20087901 Zona1 Sector2</h3>
                                    <p class="ui-li-desc"><strong>Direccion Completa 14 Usuario 14 - Nombre Completo 14</strong></p>
                                    <p class="ui-li-desc">Tipo de Operacion - Otro Detalle</p>
                                </a>
                        </li>



            </ul>
        </div>
        <div data-role="collapsible" data-theme="b"  data-content-theme="c">
            <h3>Otros</h3>

                <ul data-role="listview" data-inset="true" data-filter="true" data-ajax="false" data-filter-placeholder="Filtra datos" >
                
                        <li data-corners="true" data-shadow="false" data-iconshadow="true" data-wrapperels="div" data-icon="arrow-r" data-iconpos="right"  class="ui-btn ui-btn-icon-right ui-li-has-arrow ui-li ui-btn-up-c">
          
                                <a href="<?php echo FULL_URL?>movil/formulario4" class="ui-link-inherit">
                                    <img src="<?php echo FULL_URL?>movil/images/warning.png" class="ui-li-thumb">
                                    <p class="ui-li-aside ui-li-desc"><strong>Fecha</strong></p>

                                    <h3 class="ui-li-heading">20920001 Zona1 Sector2 Ruta 1</h3>
                                    <p class="ui-li-desc"><strong>Direccion Completa 15 Usuario 15 - Nombre Completo 15</strong></p>
                                    <p class="ui-li-desc">Tipo de Operacion - Otro Detalle</p>
                                    <p class="ui-li-desc">Motivo Impedimento</p>
                                </a>
                        </li>
                        <li data-corners="true" data-shadow="false" data-iconshadow="true" data-wrapperels="div" data-icon="arrow-r" data-iconpos="right"  class="ui-btn ui-btn-icon-right ui-li-has-arrow ui-li ui-btn-up-c">
          
                                <a href="<?php echo FULL_URL?>movil/formulario4" class="ui-link-inherit">
                                    <img src="<?php echo FULL_URL?>movil/images/warning.png" class="ui-li-thumb">
                                    <p class="ui-li-aside ui-li-desc"><strong>Fecha</strong></p>

                                    <h3 class="ui-li-heading">20023501 Zona1 Sector2 Ruta 1</h3>
                                    <p class="ui-li-desc"><strong>Direccion Completa 16 Usuario 16 - Nombre Completo 16</strong></p>
                                    <p class="ui-li-desc">Tipo de Operacion - Otro Detalle</p>
                                    <p class="ui-li-desc">Motivo Impedimento</p>
                                </a>
                        </li>
                
            </ul>
        </div>
    </div><!--/content-primary -->


        
    </div><!-- /content -->




</div><!-- /page -->

<div data-role="dialog" id="modal" data-theme="a">

    <div data-role="content" data-theme="a" class="ui-content ui-body-a" role="main">
        <h3>Opciones</h3>
        <a href="<?php echo FULL_URL?>movil/dialog_success" data-theme="b" data-role="button">Personalizacion</a>     
        <a href="<?php echo FULL_URL?>movil/index" data-theme="b" data-role="button">Cerrar Aplicacion</a>     
        <a href="<?php echo FULL_URL?>movil/main" data-theme="b" data-role="button">Cancelar</a>
        <a href="#artefactos" data-role="button" data-theme="a" data-inline="true">Artefactos</a>     
    </div>
</div>




<div data-role="dialog" id="artefactos" data-theme="a">

    <div data-role="content" data-theme="a" class="ui-content ui-body-a" role="main">
        <h3>Opciones</h3>

        <ul data-role="listview" data-inset="true" data-filter="true" data-ajax="false" data-filter-placeholder="Filtra datos" >




        <li data-corners="true" data-shadow="false" data-iconshadow="true" data-wrapperels="div" data-icon="arrow-r" data-iconpos="right" data-theme="e" class="ui-btn ui-btn-icon-right ui-li-has-arrow ui-li ui-btn-up-c">
        
        </li>





        <table data-role="table" id="financial-table-reflow" data-mode="reflow" class="">
                                        <thead>
                                            <tr class="th-groups">
                                                <th style='width:100px;' >ARTEF</th>
                                                <th>C</th>
                                                <th>T</th>
                                                <th>F</th>
                                            </tr>
                                        </thead>
                                        <tbody>

                       
                                    
                                            <tr>
                                                <td>TELEVISOR</td>
                                                <td><select name="select-more-1a" id="select-more-1a">
                                                    <option value="#">1</option>
                                                    <option value="#">2</option>
                                                    <option value="#">3</option>
                                                    <option value="#">4</option>
                                                    <option value="#">5</option>
                                                    <option value="#">6</option>
                                                    <option value="#">7</option>
                                                    <option value="#">8</option>
                                                    <option value="#">9</option>
                                                    <option value="#">>10</option>
                                                </select></td>
                                                <td><select name="select-more-1a" id="select-more-1e">
   
                                                    <option value="#">1</option>
                                                    <option value="#">2</option>
                                                    <option value="#">3</option>
                                                    <option value="#">4</option>
                                                    <option value="#">5</option>
                                                    <option value="#">6</option>
                                                    <option value="#">7</option>
                                                    <option value="#">8</option>
                                                    <option value="#">9</option>
                                                    <option value="#">>10</option>
                                                </select></td>
                                                <td><select name="select-more-1a" id="select-more-1e">
                                                    <option value="#">Diaria</option>
                                                    <option value="#">Semana</option>
                                                    <option value="#">Mes</option>
                                                </select></td>
                                            </tr>

                                            <tr>
                                                <td>RADIO</td>
                                                <td><select name="select-more-1a" id="select-more-1a">
                                                    <option value="#">1</option>
                                                    <option value="#">2</option>
                                                    <option value="#">3</option>
                                                    <option value="#">4</option>
                                                    <option value="#">5</option>
                                                    <option value="#">6</option>
                                                    <option value="#">7</option>
                                                    <option value="#">8</option>
                                                    <option value="#">9</option>
                                                    <option value="#">>10</option>
                                                </select></td>
                                                <td><select name="select-more-1a" id="select-more-1e">
   
                                                    <option value="#">1</option>
                                                    <option value="#">2</option>
                                                    <option value="#">3</option>
                                                    <option value="#">4</option>
                                                    <option value="#">5</option>
                                                    <option value="#">6</option>
                                                    <option value="#">7</option>
                                                    <option value="#">8</option>
                                                    <option value="#">9</option>
                                                    <option value="#">>10</option>
                                                </select></td>
                                                <td><select name="select-more-1a" id="select-more-1e">
                                                    <option value="#">Diaria</option>
                                                    <option value="#">Semana</option>
                                                    <option value="#">Mes</option>
                                                </select></td>
                                            </tr>



                                        </tbody>
                                    </table>             
                        
        </ul>

</div>
</div>

</body>

</html>