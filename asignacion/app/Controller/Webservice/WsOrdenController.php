<?php
App::uses('WsFilterController', 'Controller');

class WsOrdenController extends WsFilterController {

	public $components = array('RequestHandler');
	
	public function beforeFilter(){
		$this->Auth->allow('*');
		parent::beforeFilter();
	}
	
	/**
	 * get OTs por usuario.
	 * @throws Exception
	 * @return json
	 * @author Geynen
	 * @version 26 Febrero 2015
	 */
	public function get_data() {
		//TODO
		try{

			$this->loadModel('IntervencionTipo');
			$arr_obj_intervencion_tipo = $this->IntervencionTipo->findObjects('all');
			
			$this->loadModel('IntervencionGrupo');
			$arr_intervencion_tipos = array();
			foreach ($arr_obj_intervencion_tipo as $k => $obj_intervencion_tipo){
				$arr_intervencion_tipos[$k] = $obj_intervencion_tipo->data['IntervencionTipo'];
				$json_form = $this->IntervencionGrupo->getJsonForm($obj_intervencion_tipo->getID());
				$arr_intervencion_tipos[$k]['form'] = json_decode($json_form);
			}
				
			$this->set(array(
					'success'=>true,
					'message'=>'Listado de formularios generado correctamente.',
					'data'=>$arr_intervencion_tipos
			));
				
		}catch(Exception $e){
			$this->handleException($e);
		}
	}
	
}