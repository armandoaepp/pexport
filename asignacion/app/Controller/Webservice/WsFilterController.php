<?php
App::uses('AppController', 'Controller');

class WsFilterController extends AppController {

	public $validate 	= false;

	protected $oauth;

	/*
	 * header['OAUTH-ACCESS-CODE']
	 * header['OAUTH-KEY']
	 * header['OAUTH-SESSION']
	 */
	public $header			= null;

	public $oauth_access_code	= null;

	public $auth_param		= false;

	/**
	 * Validate the KEY and ACCESS-CODE.
	 * @see AppController::beforeFilter()
	 * @param string OAUTH-KEY
	 * @param string OAUTH-ACCESS-CODE
	 * @param string OAUTH-SESSION optional
	 * @return true | json
	 * @author Geynen
	 * @version 26 Febrero 2015
	 */
	public function beforeFilter(){
		return true;
		/*
		$this->header = getallheaders();
		if(!isset($this->header['OAUTH-KEY'])){
			$this->header['OAUTH-KEY'] = null;
		}
		if(!isset($this->header['OAUTH-URI'])){
			$this->header['OAUTH-URI'] = null;
		}
		if(!isset($this->header['OAUTH-SESSION'])){
			$this->header['OAUTH-SESSION'] = null;
		}

		$param = array(
			'client_id' 	=> $this->header['OAUTH-KEY'],
			'response_type' => 'code',
			'redirect_uri'	=> isset($this->header['OAUTH-URI'])?$this->header['OAUTH-URI']:'',
				'scope' => array()
			);

		$this->auth_param = $this->OAuth->getAuthorizeParams($param);

		if(!$this->isAccessCodeExpired() && $this->isKeyValid()){
			$this->loadLanguage();
			return true;
		}else{
			$this->set('success',false);
			$this->set('message','Access denied');
		}
		*/
	}
	
	private function isAccessCodeExpired(){
			
		/*if (time() >= strtotime($this->oauth_access_code['expires'])) {
		
			//@TODO: Delete the access code expired.
			$this->loadModel("AuthCode");
		
			$this->AuthCode->delete($this->oauth_access_code);
		
			return true;
		}*/
		
		return false;
	}
	
	/**
	 * load object logged user by user_id
	 * @param int $user_id
	 * @return Object
	 * @author Geynen
	 * @version 26 Febrero 2015
	 */
	private function loadObjLoggedUser($user_id){
		//TODO
		$tmp_cache_obj_user = Cache::read('obj_logged_user::'.$user_id,'redis');
	
		if(!is_object($tmp_cache_obj_user) || !$tmp_cache_obj_user->getID()){
			$this->loadModel('User');
			$tmp_cache_obj_user = $this->User->findById($user_id);
			$tmp_cache_obj_user->AllFriendsGroup; //Load AllFriendsGroup
			Cache::write('obj_logged_user::'.$user_id, $tmp_cache_obj_user, 'redis');
		}
	
		$this->obj_logged_user = $tmp_cache_obj_user;
		unset($tmp_cache_obj_user);
		
		if(isset($this->obj_logged_user) && is_object($this->obj_logged_user) && $this->obj_logged_user->getID()){
			if(!$this->obj_logged_user->checkIsActive()){
				$this->obj_logged_user->sendActivationEmail();
				$this->Cookie->delete('autologin');
				$this->Cookie->delete('auto_redirect');
				$this->Auth->logout();
				$this->Session->destroy();
					
				throw new Exception('Your account is not active. Check your email for instructions on activating your account.');
			}
				
			if(!$this->obj_logged_user->checkIsStatus()){
				$this->Cookie->delete('autologin');
				$this->Cookie->delete('auto_redirect');
				$this->Auth->logout();
				$this->Session->destroy();
					
				throw new Exception('Your account is disabled.');
			}
			return true;
		}else{
			throw new Exception('User not valid');
		}
	}
	
	/**
	 * Load language session
	 * @return void
	 * @author Geynen
	 * @version 26 Febrero 2015
	 */
	public function loadLanguage(){
		//TODO
		//Internationalization
		if(isset($this->obj_logged_user) && is_object($this->obj_logged_user) && $this->obj_logged_user->getID()){
			$this->changeLanguage();
		}else{
				
			$this->Session->write('Config.language','en');
		
			if($this->Session->check('Config.language')){
				Configure::write('Config.language', $this->Session->read('Config.language'));
			}
			$all_languages = Configure::read('all_languages');
				
			$lc_time_format = $all_languages[Configure::read('Config.language')]['lc_time_format'];
			putenv("LANG=$lc_time_format");
			setlocale(LC_ALL, $lc_time_format);
		}
	}

	/**
	 * validate if session is valid
	 * @param string OAUTH-SESSION
	 * @return boolean
	 * @author Geynen
	 * @version 26 Febrero 2015
	 */
	public function isSessionValid(){
		//TODO
		if(isset($this->header['OAUTH-SESSION'])){
			$hashValue = $this->getDecodeHash($this->header['OAUTH-SESSION']);
			
			$user_id = $this->getDecodeHash($hashValue['user_id']);
			
			if($user_id!=''){
				if($this->loadObjLoggedUser($user_id)){
					$this->loadLanguage();
					return true;
				}
			}
		}
		return false;
	}

	/**
	 * validate key
	 * @param string OAUTH-KEY
	 * @param string OAUTH-ACCESS-CODE
	 * @return boolean
	 * @author Geynen
	 * @version 26 Febrero 2015
	 */
	private function isKeyValid(){
		//TODO
		$client_id = $this->header['OAUTH-KEY'];
		$client_secret = $this->header['OAUTH-ACCESS-CODE'];

		$conditions = array('client_id' => $client_id);
		if ($client_secret) {
			$conditions['client_secret'] = $client_secret;
		}
		$this->loadModel("Client");
		$client = $this->Client->find('first', array(
			'conditions' => $conditions,
			'recursive' => -1
		));
		if ($client){
			return true;
		};
		return false;
	}
	
	/**
	 * validate key
	 * @return boolean | ArrayObject
	 * @author Geynen
	 * @version 26 Febrero 2015
	 */
	public function getApp(){
		//TODO
		$client_id = $this->header['OAUTH-KEY'];
		$client_secret = $this->header['OAUTH-ACCESS-CODE'];
		
		$conditions = array('client_id' => $client_id);
		if ($client_secret) {
			$conditions['client_secret'] = $client_secret;
		}
		$this->loadModel("Client");
		$obj_client = $this->Client->findObjects('first', array(
				'conditions' => $conditions,
				'recursive' => -1
		));
	
		if(!is_object($obj_client) || !$obj_client->getID()){
			throw new InternalException(__('Your appID is not valid.'));
		}
	
		if ($obj_client){
			return $obj_client;
		};
		return false;
	}

	/**
	 * before render to json
	 * @see Controller::beforeRender()
	 * @return json
	 * @author Geynen
	 * @version 26 Febrero 2015
	 */
	public function beforeRender(){
		if(!isset($this->viewVars['error_code'])){
			$this->set('error_code', "200");
		}
		$this->toJson();
	}

	/**
	 * handle exceptions to json
	 * @param unknown_type $exception
	 * @return json
	 * @author Geynen
	 * @version 26 Febrero 2015
	 */
	public function handleException($exception){
		$this->set('success', false);
		$this->set('error_code', "500");
		$this->set('message', $exception->getMessage());
	}
}