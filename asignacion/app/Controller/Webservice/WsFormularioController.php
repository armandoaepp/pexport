<?php
App::uses('WsFilterController', 'Controller');

class WsFormularioController extends WsFilterController {

	public $components = array('RequestHandler');
	
	public function beforeFilter(){
		$this->Auth->allow();
		parent::beforeFilter();
	}
	
	/**
	 * get Formulario por tipo de intervencion
	 * @param string $intervencion_tipo_id
	 * @throws Exception
	 * @return json
	 * @author Geynen
	 * @version 26 Febrero 2015
	 */
	public function get_form($intervencion_tipo_id = null) {
		
		try{
		
			if (!$intervencion_tipo_id) {
				throw new Exception('intervencion_tipo_id not provided');
			}
			
			$this->loadModel('IntervencionGrupo');
			$json_form = $this->IntervencionGrupo->getJsonForm($intervencion_tipo_id);
			
			$this->set(array(
					'success'=>true,
					'message'=>'Formulario generado correctamente.',
					'data'=>json_decode($json_form)
			));
			
		}catch(Exception $e){
			$this->handleException($e);
		}
	}
	
	/**
	 * get Todos los Formulario de acuerdo a los Tipos de Intervencion.
	 * @throws Exception
	 * @return json
	 * @author Geynen
	 * @version 26 Febrero 2015
	 */
	public function get_all_form() {
	
		try{

			$this->loadModel('IntervencionTipo');
			$arr_obj_intervencion_tipo = $this->IntervencionTipo->findObjects('all');
			
			$this->loadModel('IntervencionGrupo');
			$arr_intervencion_tipos = array();
			foreach ($arr_obj_intervencion_tipo as $k => $obj_intervencion_tipo){
				$arr_intervencion_tipos[$k] = $obj_intervencion_tipo->data['IntervencionTipo'];
				$json_form = $this->IntervencionGrupo->getJsonForm($obj_intervencion_tipo->getID());
				$arr_intervencion_tipos[$k]['form'] = json_decode($json_form);
			}
				
			$this->set(array(
					'success'=>true,
					'message'=>'Listado de formularios generado correctamente.',
					'data'=>$arr_intervencion_tipos
			));
				
		}catch(Exception $e){
			$this->handleException($e);
		}
	}
	
}