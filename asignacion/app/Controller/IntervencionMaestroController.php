<?php 
App::uses('AppController', 'Controller');

class IntervencionMaestroController extends AppController {

	public $helpers = array('Html', 'Form');
	public $name = 'IntervencionMaestro';
	
	public function beforeFilter(){
		$this->layout = 'ajax';
		parent::beforeFilter();
	}
	
	/**
	 * Crear Json de Formulario por tipo de intervencion
	 * @author Geynen
	 * @version 13 Febrero 2015
	 */
	public function formulario_json($intervencion_tipo_id = null) {
		$this->layout = 'ajax';
		
		if (!$intervencion_tipo_id) {
			throw new NotFoundException(__('Invalid post'));
		}
		
		$this->loadModel('IntervencionGrupo');
		$json_form = $this->IntervencionGrupo->getJsonForm($intervencion_tipo_id);
		
		//echo $json_form;	
		$this->set(compact('json_form'));
	}
	
	/**
	 * Crear formulario por tipo de intervencion
	 * @author Alan Hugo
	 * @version 09 Febrero 2015
	 */
	public function formulario($intervencion_tipo_id = null) {
		$this->layout = 'dashboard';
		
		if (!$intervencion_tipo_id) {
			throw new NotFoundException(__('Invalid post'));
		}
		
		$this->loadModel('ObjetoGrupo');
		$this->loadModel('Objeto');
		
		$arr_obj_intervencion_maestro = $this->IntervencionMaestro->find('all',array(
				'fields'=>array('ObjetoGrupo.grupo_objeto_id'),
				'joins'=>array(
						array('table' => 'formulario.objeto_grupos',
								'alias' => 'ObjetoGrupo',
								'type' => 'INNER',
								'conditions' => array(
										'IntervencionMaestro.objeto_grupo_id = ObjetoGrupo.id'
								))
				),
				'conditions'=>array('intervencion_tipo_id'=>$intervencion_tipo_id),
				//'order'=>array('IntervencionMaestro.orden'),
				'group'=>array('ObjetoGrupo.grupo_objeto_id')
		
		));
		debug($arr_obj_intervencion_maestro);exit();
		$arr_obj_objeto_grupo = $this->ObjetoGrupo->findObjects('all',array(
				'joins'=>array(
						array('table' => 'actividades.intervencion_maestros',
						'alias' => 'IntervencionMaestro',
						'type' => 'INNER',
						'conditions' => array(
						'IntervencionMaestro.objeto_grupo_id = ObjetoGrupo.id'
						))
				),				
				'conditions'=>array('intervencion_tipo_id'=>$intervencion_tipo_id),
				'order'=>array('IntervencionMaestro.orden'),
				
		));

		$arr_og = array();
		foreach ($arr_obj_objeto_grupo as $k=> $objeto_grupo){
			$arr_og[]= $objeto_grupo->data['ObjetoGrupo']['grupo_objeto_id'];
		}
		
		$arr_obj_objeto = $this->Objeto->findObjects('all',array(
				'conditions'=>array('id !='=> $arr_og)
		));
	
		$this->set(compact('arr_obj_objeto','arr_obj_objeto_grupo','intervencion_tipo_id'));
	}
	
	/**
	 * Vista Previa de Formulario por tipo de intervencion
	 * @author Geynen
	 * @version 13 Febrero 2015
	 */
	public function formulario_preview($intervencion_tipo_id = null) {
		$this->layout = 'default_no_header';
		$this->helpers[] = 'Formulario';
	
		if (!$intervencion_tipo_id) {
			throw new NotFoundException(__('Invalid post'));
		}
	
		$this->loadModel('IntervencionGrupo');
		$json_form = $this->IntervencionGrupo->getJsonForm($intervencion_tipo_id);
	
		//echo $json_form;
		$this->set(compact('json_form'));
	}
}