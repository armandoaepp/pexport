<?php 
App::uses('AppController', 'Controller');

class TipoDatoController extends AppController {

	public $helpers = array('Html', 'Form');
	public $name = 'TipoDato';
	
	public function beforeFilter(){
		$this->layout = 'ajax';
		parent::beforeFilter();
	}

	/**
	 * Listado de Tipos de Dato
	 * @author Geynen
	 * @version 06 Febrero 2015
	 */
	public function index() {
		$this->layout = 'dashboard';
		$arr_obj_tipo_dato = $this->TipoDato->findObjects('all');
		
		$this->set(compact('arr_obj_tipo_dato'));
	}
	
	/**
	 * Nuevo Tipo de Dato
	 * @author Geynen
	 * @version 06 Febrero 2015
	 */
	public function nuevo() {
		$this->layout = 'dashboard';
		
		if ($this->request->is('post')) {
			if ($this->TipoDato->save($this->request->data)) {
				$this->Session->setFlash('Datos Guardados Correctamente.','flash_success');
				$this->redirect(array('action' => 'index'));
			}
		}	
	}
	
	/**
	 * Editar Tipo de Dato
	 * @param integer $id
	 * @author Geynen
	 * @version 06 Febrero 2015
	 */
	public function editar($id = null) {
		$this->layout = 'dashboard';
	
		if (!$id) {
			throw new NotFoundException(__('Invalid post'));
		}
		
		$obj_tipo_dato = $this->TipoDato->findById($id);
		if (!$obj_tipo_dato) {
			throw new NotFoundException(__('Invalid post'));
		}
		
		if ($this->request->is(array('post', 'put'))) {
			$this->TipoDato->id = $id;
			if ($this->TipoDato->save($this->request->data)) {
				$this->Session->setFlash('Datos Actualizados Correctamente.','flash_success');
				return $this->redirect(array('action' => 'index'));
			}
			$this->Session->setFlash('No se puede actualizar.','flash_error');
		}
		
		$this->set(compact('obj_tipo_dato'));
	}
	
	/**
	 * Eliminar Tipo de Dato
	 * @author Geynen
	 * @version 06 Febrero 2015
	 */
	public function eliminar($id = null) {
		$this->layout = 'dashboard';
	
	    if ($this->TipoDato->delete($id)) {
	        $this->Session->setFlash('El Tipo de Dato con Id: ' . $id . ' ha sido eliminado.','flash_success');
	        $this->redirect(array('action' => 'index'));
	    }
	}
	
}