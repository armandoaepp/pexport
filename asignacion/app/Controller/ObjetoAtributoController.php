<?php 
App::uses('AppController', 'Controller');

class ObjetoAtributoController extends AppController {

	public $helpers = array('Html', 'Form');
	public $name = 'ObjetoAtributo';
	
	public function beforeFilter(){
		$this->layout = 'ajax';
		parent::beforeFilter();
	}

	/**
	 * Listado de Tipos de Objeto
	 * @param integer $objeto_id
	 * @author Geynen
	 * @version 07 Febrero 2015
	 */
	public function index($objeto_id = null) {
		$this->layout = 'dashboard';
		$this->loadModel('Objeto');
		$obj_objeto = $this->Objeto->findById($objeto_id);
		
		$this->set(compact('obj_objeto'));
	}
	
	/**
	 * Nuevo Atributo de Objeto
	 * @param integer $objeto_id
	 * @author Geynen
	 * @version 07 Febrero 2015
	 */
	public function nuevo($objeto_id = null) {
		$this->layout = 'dashboard';
		
		if ($this->request->is('post')) {
			if ($this->ObjetoAtributo->save($this->request->data)) {
				$this->Session->setFlash('Datos Guardados Correctamente.','flash_success');
				$this->redirect(array('action' => 'index', $objeto_id));
			}
		}
		
		$this->loadModel('Objeto');
		$obj_objeto = $this->Objeto->findById($objeto_id);
		$this->loadModel('TipoAtributo');
		$arr_obj_tipo_atributo = $this->TipoAtributo->findObjects('all');
		
		$this->set(compact('obj_objeto','arr_obj_tipo_atributo'));
	}
	
	/**
	 * Editar Atributo de Objeto
	 * @param integer $id
	 * @author Geynen
	 * @version 07 Febrero 2015
	 */
	public function editar($id = null) {
		$this->layout = 'dashboard';
	
		if (!$id) {
			throw new NotFoundException(__('Invalid post'));
		}
		
		$obj_objeto_atributo = $this->ObjetoAtributo->findById($id);
		if (!$obj_objeto_atributo) {
			throw new NotFoundException(__('Invalid post'));
		}
		
		if ($this->request->is(array('post', 'put'))) {
			$this->ObjetoAtributo->id = $id;
			if ($this->ObjetoAtributo->save($this->request->data)) {
				$this->Session->setFlash('Datos Actualizados Correctamente.','flash_success');
				return $this->redirect(array('action' => 'index', $obj_objeto_atributo->getAttr('objeto_id')));
			}
			$this->Session->setFlash('No se puede actualizar.','flash_error');
		}
		
		$this->loadModel('TipoAtributo');
		$arr_obj_tipo_atributo = $this->TipoAtributo->findObjects('all');
		
		$this->set(compact('obj_objeto_atributo','arr_obj_tipo_atributo'));
	}
	
	/**
	 * Eliminar Atributo de Objeto
	 * @param integer $objeto_id
	 * @param integer $id
	 * @author Geynen
	 * @version 07 Febrero 2015
	 */
	public function eliminar($objeto_id = null, $id = null) {
		$this->layout = 'dashboard';
	
	    if ($this->ObjetoAtributo->delete($id)) {
	        $this->Session->setFlash('El Atributo de Objeto con Id: ' . $id . ' ha sido eliminado.','flash_success');
	        $this->redirect(array('action' => 'index', $objeto_id));
	    }
	}
	
}