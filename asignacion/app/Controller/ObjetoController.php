<?php 
App::uses('AppController', 'Controller');

class ObjetoController extends AppController {

	public $helpers = array('Html', 'Form');
	public $name = 'Objeto';
	
	public function beforeFilter(){
		$this->layout = 'ajax';
		parent::beforeFilter();
	}

	/**
	 * Listado de Tipos de Objeto
	 * @author Geynen
	 * @version 07 Febrero 2015
	 */
	public function index() {
		$this->layout = 'dashboard';
		$arr_obj_objeto = $this->Objeto->findObjects('all');
		
		$this->set(compact('arr_obj_objeto'));
	}
	
	/**
	 * Nuevo Tipo de Objeto
	 * @author Geynen
	 * @version 07 Febrero 2015
	 */
	public function nuevo() {
		$this->layout = 'dashboard';
		
		if ($this->request->is('post')) {
			if ($this->Objeto->save($this->request->data)) {
				$this->Session->setFlash('Datos Guardados Correctamente.','flash_success');
				
				$obj_objeto = $this->Objeto->findById($this->Objeto->id);				
				if($obj_objeto->TipoObjeto->getAttr('datos')=='I'){
					return $this->redirect(array('controller' => 'objeto_dato', 'action' => 'index', $this->Objeto->id));
				}elseif($obj_objeto->TipoObjeto->getAttr('objetos')==true){
					return $this->redirect(array('controller' => 'objeto_grupo', 'action' => 'index', $this->Objeto->id));
				}else{
					return $this->redirect(array('action' => 'index'));
				}
			}
		}
		
		$this->loadModel('Orden');
		$arr_campos_defecto = $this->Orden->getCamposPopulate();
		$this->loadModel('TipoObjeto');
		$arr_obj_tipo_objeto = $this->TipoObjeto->findObjects('all');
		$this->loadModel('TipoDato');
		$arr_obj_tipo_dato = $this->TipoDato->findObjects('all');
		$this->loadModel('ObjetoOrigenDato');
		$arr_obj_objeto_origen_dato = $this->ObjetoOrigenDato->findObjects('all');		
		
		$this->set(compact('arr_obj_tipo_objeto','arr_obj_tipo_dato','arr_obj_objeto_origen_dato','arr_campos_defecto'));
	}
	
	/**
	 * Editar Tipo de Objeto
	 * @param integer $id
	 * @author Geynen
	 * @version 07 Febrero 2015
	 */
	public function editar($id = null) {
		$this->layout = 'dashboard';
	
		if (!$id) {
			throw new NotFoundException(__('Invalid post'));
		}
		
		$obj_objeto = $this->Objeto->findById($id);
		if (!$obj_objeto) {
			throw new NotFoundException(__('Invalid post'));
		}
		
		if ($this->request->is(array('post', 'put'))) {
			$this->Objeto->id = $id;
			if ($this->Objeto->save($this->request->data)) {
				$this->Session->setFlash('Datos Actualizados Correctamente.','flash_success');
				
				$obj_objeto = $this->Objeto->findById($id);				
				if($obj_objeto->TipoObjeto->getAttr('datos')=='I'){
					return $this->redirect(array('controller' => 'objeto_dato', 'action' => 'index', $id));
				}elseif($obj_objeto->TipoObjeto->getAttr('objetos')==true){
					return $this->redirect(array('controller' => 'objeto_grupo', 'action' => 'index', $id));
				}else{
					return $this->redirect(array('action' => 'index'));
				}
			}
			$this->Session->setFlash('No se puede actualizar.','flash_error');
		}
		
		$this->loadModel('Orden');
		$this->Orden->setSchema($this->Orden->schema);
		$arr_campos_defecto = $this->Orden->getCamposPopulate();
		$this->loadModel('TipoObjeto');
		$arr_obj_tipo_objeto = $this->TipoObjeto->findObjects('all');
		$this->loadModel('TipoDato');
		$arr_obj_tipo_dato = $this->TipoDato->findObjects('all');
		$this->loadModel('ObjetoOrigenDato');
		$arr_obj_objeto_origen_dato = $this->ObjetoOrigenDato->findObjects('all');
		
		$this->set(compact('obj_objeto','arr_obj_tipo_objeto','arr_obj_tipo_dato','arr_obj_objeto_origen_dato','arr_campos_defecto'));
	}
	
	/**
	 * Eliminar Tipo de Objeto
	 * @author Geynen
	 * @version 07 Febrero 2015
	 */
	public function eliminar($id = null) {
		$this->layout = 'dashboard';
	
	    if ($this->Objeto->delete($id)) {
	        $this->Session->setFlash('El Tipo de Objeto con Id: ' . $id . ' ha sido eliminado.','flash_success');
	        $this->redirect(array('action' => 'index'));
	    }
	}
	
}