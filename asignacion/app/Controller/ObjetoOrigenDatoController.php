<?php 
App::uses('AppController', 'Controller');

class ObjetoOrigenDatoController extends AppController {

	public $helpers = array('Html', 'Form');
	public $name = 'ObjetoOrigenDato';
	
	public function beforeFilter(){
		$this->layout = 'ajax';
		parent::beforeFilter();
	}

	/**
	 * Listado de Tipos de Objeto
	 * @author Geynen
	 * @version 12 Febrero 2015
	 */
	public function index() {
		$this->layout = 'dashboard';
		$arr_obj_objeto_origen_dato= $this->ObjetoOrigenDato->findObjects('all');
		
		$this->set(compact('arr_obj_objeto_origen_dato'));
	}
	
	/**
	 * Nuevo Tipo de Objeto
	 * @author Geynen
	 * @version 12 Febrero 2015
	 */
	public function nuevo() {
		$this->layout = 'dashboard';
		
		if ($this->request->is('post')) {
			if ($this->ObjetoOrigenDato->save($this->request->data)) {
				$this->Session->setFlash('Datos Guardados Correctamente.','flash_success');
				$this->redirect(array('action' => 'index'));
			}
		}	
	}
	
	/**
	 * Editar Tipo de Objeto
	 * @param integer $id
	 * @author Geynen
	 * @version 12 Febrero 2015
	 */
	public function editar($id = null) {
		$this->layout = 'dashboard';
	
		if (!$id) {
			throw new NotFoundException(__('Invalid post'));
		}
		
		$obj_objeto_origen_dato= $this->ObjetoOrigenDato->findById($id);
		if (!$obj_objeto_origen_dato) {
			throw new NotFoundException(__('Invalid post'));
		}
		
		if ($this->request->is(array('post', 'put'))) {
			$this->ObjetoOrigenDato->id = $id;
			if ($this->ObjetoOrigenDato->save($this->request->data)) {
				$this->Session->setFlash('Datos Actualizados Correctamente.','flash_success');
				return $this->redirect(array('action' => 'index'));
			}
			$this->Session->setFlash('No se puede actualizar.','flash_error');
		}
		
		$this->set(compact('obj_objeto_origen_dato'));
	}
	
	/**
	 * Eliminar Tipo de Objeto
	 * @author Geynen
	 * @version 12 Febrero 2015
	 */
	public function eliminar($id = null) {
		$this->layout = 'dashboard';
	
	    if ($this->ObjetoOrigenDato->delete($id)) {
	        $this->Session->setFlash('El Origen de Dato con Id: ' . $id . ' ha sido eliminado.','flash_success');
	        $this->redirect(array('action' => 'index'));
	    }
	}
	
}