<?php 
App::uses('AppController', 'Controller');

class ObjetoGrupoController extends AppController {

	public $helpers = array('Html', 'Form');
	public $name = 'ObjetoGrupo';
	
	public function beforeFilter(){
		$this->layout = 'ajax';
		parent::beforeFilter();
	}

	/**
	 * Listado de Tipos de Objeto
	 * @param integer $objeto_id
	 * @author Geynen
	 * @version 07 Febrero 2015
	 */
	public function index($objeto_id = null) {
		$this->layout = 'dashboard';
		$this->loadModel('Objeto');
		$obj_objeto = $this->Objeto->findById($objeto_id);
		
		$this->set(compact('obj_objeto'));
	}
	
	/**
	 * Nuevo Grupo de Objeto
	 * @param integer $objeto_id
	 * @author Geynen
	 * @version 07 Febrero 2015
	 */
	public function nuevo($grupo_objeto_id = null) {
		$this->layout = 'dashboard';
		
		if ($this->request->is('post')) {
			if ($this->ObjetoGrupo->save($this->request->data)) {
				$this->Session->setFlash('Datos Guardados Correctamente.','flash_success');
				$this->redirect(array('action' => 'index', $grupo_objeto_id));
			}
		}
		
		$this->loadModel('Objeto');
		$obj_objeto = $this->Objeto->findById($grupo_objeto_id);
		
		$arr_obj_objeto = $this->Objeto->findObjects('all',
				array('conditions'=>array('id !='=>$grupo_objeto_id)));
		
		$this->set(compact('obj_objeto','arr_obj_objeto'));
	}
	
	/**
	 * Editar Grupo de Objeto
	 * @param integer $id
	 * @author Geynen
	 * @version 07 Febrero 2015
	 */
	public function editar($id = null) {
		$this->layout = 'dashboard';
	
		if (!$id) {
			throw new NotFoundException(__('Invalid post'));
		}
		
		$obj_objeto_grupo = $this->ObjetoGrupo->findById($id);
		if (!$obj_objeto_grupo) {
			throw new NotFoundException(__('Invalid post'));
		}
		
		if ($this->request->is(array('post', 'put'))) {
			$this->ObjetoGrupo->id = $id;
			if ($this->ObjetoGrupo->save($this->request->data)) {
				$this->Session->setFlash('Datos Actualizados Correctamente.','flash_success');
				return $this->redirect(array('action' => 'index', $obj_objeto_grupo->getAttr('grupo_objeto_id')));
			}
			$this->Session->setFlash('No se puede actualizar.','flash_error');
		}
		
		$this->loadModel('Objeto');
		$arr_obj_objeto = $this->Objeto->findObjects('all');
		
		$this->set(compact('obj_objeto_grupo','arr_obj_objeto'));
	}
	
	/**
	 * Eliminar Grupo de Objeto
	 * @param integer $grupo_objeto_id
	 * @param integer $id
	 * @author Geynen
	 * @version 07 Febrero 2015
	 */
	public function eliminar($grupo_objeto_id = null, $id = null) {
		$this->layout = 'dashboard';
	
	    if ($this->ObjetoGrupo->delete($id)) {
	        $this->Session->setFlash('El Grupo de Objeto con Id: ' . $id . ' ha sido eliminado.','flash_success');
	        $this->redirect(array('action' => 'index', $grupo_objeto_id));
	    }
	}
	
	/**
	 * Administrar Grupo de Objeto (Vista Grafica)
	 * @author Geynen
	 * @version 09 Febrero 2015
	 */
	public function manager() {
		$this->layout = 'dashboard';
	
		$this->loadModel('Objeto');
		$arr_obj_objeto = $this->Objeto->findObjects('all');
		
		$this->set(compact('arr_obj_objeto'));
	}
	
	/**
	 * Nuevo Grupo de Objeto (ajax)
	 * @param integer $objeto_grupo_id
	 * @param integer $objeto_id
	 * @author Geynen
	 * @version 09 Febrero 2015
	 */
	public function ajax_guardar($grupo_objeto_id = null, $item_objeto_id = null) {
		$this->layout = 'dashboard';
	
		$this->request->data['ObjetoGrupo']['grupo_objeto_id'] = $grupo_objeto_id;
		$this->request->data['ObjetoGrupo']['item_objeto_id'] = $item_objeto_id;
		if ($this->ObjetoGrupo->save($this->request->data)) {
			echo json_encode(array('success'=>true, 'msg'=>'Guardado Correctamente.', 'objeto_grupo_id'=>$this->ObjetoGrupo->id));
			exit;
		}
		
		echo json_encode(array('success'=>false, 'msg'=>'Ocurrio un Error.'));
		exit;
	}
	
	/**
	 * Nuevo Grupo de Objeto (ajax)
	 * @param integer $grupo_objeto_id
	 * @param integer $arr_item_objeto_id
	 * @author Geynen
	 * @version 09 Febrero 2015
	 */
	public function ajax_cambiar_orden($grupo_objeto_id = null, $arr_item_objeto_id = null) {
		$this->layout = 'dashboard';
		
		$arr_item_objeto_id = json_decode($arr_item_objeto_id);
		
		$this->ObjetoGrupo->setSchema($this->ObjetoGrupo->schema);
		foreach ($arr_item_objeto_id as $i => $id){
			$this->ObjetoGrupo->updateAll(
					array('orden' => $i+1),
					array(
						'grupo_objeto_id' => $grupo_objeto_id,
						'item_objeto_id' => $id
					)
			);
		}

		echo json_encode(array('success'=>true, 'msg'=>'Guardado Correctamente.'));
		exit;
	}
	
	/**
	 * Eliminar Item de Grupo de Objeto (ajax)
	 * @param integer $grupo_objeto_id
	 * @param integer $item_objeto_id
	 * @author Geynen
	 * @version 11 Febrero 2015
	 */
	public function ajax_eliminar($grupo_objeto_id = null, $item_objeto_id = null) {
		$this->layout = 'dashboard';
	
	
		$this->ObjetoGrupo->setSchema($this->ObjetoGrupo->schema);
		if($this->ObjetoGrupo->deleteAll(
				array(
						'grupo_objeto_id' => $grupo_objeto_id,
						'item_objeto_id' => $item_objeto_id
				)
			,false)){
	
			echo json_encode(array('success'=>true, 'msg'=>'Eliminado Correctamente.'));
		}else{
			echo json_encode(array('success'=>false, 'msg'=>'Ocurrio un error.'));
		}
		exit;
	}
	
}