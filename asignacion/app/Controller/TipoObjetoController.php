<?php 
App::uses('AppController', 'Controller');

class TipoObjetoController extends AppController {

	public $helpers = array('Html', 'Form');
	public $name = 'TipoObjeto';
	
	public function beforeFilter(){
		$this->layout = 'ajax';
		parent::beforeFilter();
	}

	/**
	 * Listado de Tipos de Objeto
	 * @author Geynen
	 * @version 06 Febrero 2015
	 */
	public function index() {
		$this->layout = 'dashboard';
		$arr_obj_tipo_objeto = $this->TipoObjeto->findObjects('all');
		
		$this->set(compact('arr_obj_tipo_objeto'));
	}
	
	/**
	 * Nuevo Tipo de Objeto
	 * @author Geynen
	 * @version 06 Febrero 2015
	 */
	public function nuevo() {
		$this->layout = 'dashboard';
		
		if ($this->request->is('post')) {
			if ($this->TipoObjeto->save($this->request->data)) {
				$this->Session->setFlash('Datos Guardados Correctamente.','flash_success');
				$this->redirect(array('action' => 'index'));
			}
		}	
	}
	
	/**
	 * Editar Tipo de Objeto
	 * @param integer $id
	 * @author Geynen
	 * @version 06 Febrero 2015
	 */
	public function editar($id = null) {
		$this->layout = 'dashboard';
	
		if (!$id) {
			throw new NotFoundException(__('Invalid post'));
		}
		
		$obj_tipo_objeto = $this->TipoObjeto->findById($id);
		if (!$obj_tipo_objeto) {
			throw new NotFoundException(__('Invalid post'));
		}
		
		if ($this->request->is(array('post', 'put'))) {
			$this->TipoObjeto->id = $id;
			if ($this->TipoObjeto->save($this->request->data)) {
				$this->Session->setFlash('Datos Actualizados Correctamente.','flash_success');
				return $this->redirect(array('action' => 'index'));
			}
			$this->Session->setFlash('No se puede actualizar.','flash_error');
		}
		
		$this->set(compact('obj_tipo_objeto'));
	}
	
	/**
	 * Eliminar Tipo de Objeto
	 * @author Geynen
	 * @version 06 Febrero 2015
	 */
	public function eliminar($id = null) {
		$this->layout = 'dashboard';
	
	    if ($this->TipoObjeto->delete($id)) {
	        $this->Session->setFlash('El Tipo de Objeto con Id: ' . $id . ' ha sido eliminado.','flash_success');
	        $this->redirect(array('action' => 'index'));
	    }
	}
	
}