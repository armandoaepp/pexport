<?php 
App::uses('AppController', 'Controller');

class IntervencionTipoWebServiceController extends AppController {

	public $helpers = array('Html', 'Form');
	public $name = 'IntervencionTipoWebService';
	
	public function beforeFilter(){
		$this->layout = 'ajax';
		parent::beforeFilter();
	}

	/**
	 * Listado de Web Services de IntervencionTipo
	 * @param integer $intervencion_tipo_id
	 * @author Geynen
	 * @version 27 Febrero 2015
	 */
	public function index($intervencion_tipo_id = null) {
		$this->layout = 'dashboard';
		$this->loadModel('IntervencionTipo');
		$obj_intervencion_tipo = $this->IntervencionTipo->findById($intervencion_tipo_id);
		
		$this->set(compact('obj_intervencion_tipo'));
	}
	
	/**
	 * Nuevo Web Service de IntervencionTipo
	 * @param integer $intervencion_tipo_id
	 * @author Geynen
	 * @version 27 Febrero 2015
	 */
	public function nuevo($intervencion_tipo_id = null) {
		$this->layout = 'dashboard';
		
		if ($this->request->is('post')) {
			if ($this->IntervencionTipoWebService->save($this->request->data)) {
				$this->Session->setFlash('Datos Guardados Correctamente.','flash_success');
				$this->redirect(array('action' => 'index', $intervencion_tipo_id));
			}
		}
		
		$this->loadModel('IntervencionTipo');
		$obj_intervencion_tipo = $this->IntervencionTipo->findById($intervencion_tipo_id);
		
		$arr_obj_intervencion_tipo = $this->IntervencionTipo->findObjects('all');
		
		$this->set(compact('obj_intervencion_tipo','arr_obj_intervencion_tipo'));
	}
	
	/**
	 * Editar Web Service de IntervencionTipo
	 * @param integer $id
	 * @author Geynen
	 * @version 27 Febrero 2015
	 */
	public function editar($id = null) {
		$this->layout = 'dashboard';
	
		if (!$id) {
			throw new NotFoundException(__('Invalid post'));
		}
		
		$obj_intervencion_tipo_web_service = $this->IntervencionTipoWebService->findById($id);
		if (!$obj_intervencion_tipo_web_service) {
			throw new NotFoundException(__('Invalid post'));
		}
		
		if ($this->request->is(array('post', 'put'))) {
			$this->IntervencionTipoWebService->id = $id;
			if ($this->IntervencionTipoWebService->save($this->request->data)) {
				$this->Session->setFlash('Datos Actualizados Correctamente.','flash_success');
				return $this->redirect(array('action' => 'index', $obj_intervencion_tipo_web_service->getAttr('intervencion_tipo_id')));
			}
			$this->Session->setFlash('No se puede actualizar.','flash_error');
		}
		
		$this->loadModel('IntervencionTipo');
		$arr_obj_intervencion_tipo = $this->IntervencionTipo->findObjects('all');
		
		$this->set(compact('obj_intervencion_tipo_web_service','arr_obj_intervencion_tipo'));
	}
	
	/**
	 * Eliminar Web Service de IntervencionTipo
	 * @param integer $intervencion_tipo_id
	 * @param integer $id
	 * @author Geynen
	 * @version 27 Febrero 2015
	 */
	public function eliminar($intervencion_tipo_id = null, $id = null) {
		$this->layout = 'dashboard';
	
	    if ($this->IntervencionTipoWebService->delete($id)) {
	        $this->Session->setFlash('El Web Service con Id: ' . $id . ' ha sido eliminado.','flash_success');
	        $this->redirect(array('action' => 'index', $intervencion_tipo_id));
	    }
	}
	
}