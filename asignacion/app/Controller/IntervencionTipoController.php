<?php 
App::uses('AppController', 'Controller');

class IntervencionTipoController extends AppController {

	public $helpers = array('Html', 'Form');
	public $name = 'IntervencionTipo';
	
	public function beforeFilter(){
		$this->layout = 'ajax';
		parent::beforeFilter();
	}

	/**
	 * Listado de Tipos de Intervencion
	 * @author Alan Hugo
	 * @version 07 Febrero 2015
	 */
	public function index() {
		$this->layout = 'dashboard';
		$arr_obj_intervencion_tipo = $this->IntervencionTipo->findObjects('all');
		
		$this->set(compact('arr_obj_intervencion_tipo'));
	}
	
	/**
	 * Nuevo Tipo de Intervencion
	 * @author Alan Hugo
	 * @version 07 Febrero 2015
	 */
	public function nuevo() {
		$this->layout = 'dashboard';
		
		if ($this->request->is('post')) {
			if ($this->IntervencionTipo->save($this->request->data)) {
				$this->Session->setFlash('Datos Guardados Correctamente.','flash_success');
				$this->redirect(array('action' => 'index'));
			}
		}
		
		$arr_obj_intervencion_tipo = $this->IntervencionTipo->findObjects('all');
		$this->set(compact('arr_obj_intervencion_tipo'));
	}
	
	/**
	 * Editar Tipo de Intervencion
	 * @param integer $id
	 * @author Alan Hugo
	 * @version 07 Febrero 2015
	 */
	public function editar($id = null) {
		$this->layout = 'dashboard';
	
		if (!$id) {
			throw new NotFoundException(__('Invalid post'));
		}
		
		$obj_intervencion_tipo = $this->IntervencionTipo->findById($id);
		if (!$obj_intervencion_tipo) {
			throw new NotFoundException(__('Invalid post'));
		}
		
		if ($this->request->is(array('post', 'put'))) {
			$this->IntervencionTipo->id = $id;
			if ($this->IntervencionTipo->save($this->request->data)) {
				$this->Session->setFlash('Datos Actualizados Correctamente.','flash_success');
				return $this->redirect(array('action' => 'index'));
			}
			$this->Session->setFlash('No se puede actualizar.','flash_error');
		}
		
		$arr_obj_intervencion_tipo = $this->IntervencionTipo->findObjects('all');
		
		$this->set(compact('obj_intervencion_tipo','arr_obj_intervencion_tipo'));
	}
	
	/**
	 * Eliminar Tipo de Intervencion
	 * @author Alan Hugo
	 * @version 07 Febrero 2015
	 */
	public function eliminar($id = null) {
		$this->layout = 'dashboard';
	
	    if ($this->IntervencionTipo->delete($id)) {
	        $this->Session->setFlash('El Tipo de Intervencion con Id: ' . $id . ' ha sido eliminado.','flash_success');
	        $this->redirect(array('action' => 'index'));
	    }
	}
	
	/**
	 * Crear formulario por tipo de intervencion
	 * @author Alan Hugo
	 * @version 09 Febrero 2015
	 */
	public function formulario($id = null) {
		$this->layout = 'dashboard';
		
		if (!$id) {
			throw new NotFoundException(__('Invalid post'));
		}
		
		$this->loadModel('ObjetoGrupo');
		$this->loadModel('Objeto');
		
		$arr_obj_objeto_grupo = $this->ObjetoGrupo->findObjects('all',array(
				//'fields'=>array('ObjetoGrupo.grupo_objeto_id','IntervencionMaestro.orden'),
				'joins'=>array(
						array('table' => 'actividades.intervencion_maestros',
						'alias' => 'IntervencionMaestro',
						'type' => 'INNER',
						'conditions' => array(
						'IntervencionMaestro.objeto_grupo_id = ObjetoGrupo.id'
						))
				),				
				'conditions'=>array('intervencion_tipo_id'=>$id),
				//'group'=>array('ObjetoGrupo.grupo_objeto_id'),
				'order'=>array('IntervencionMaestro.orden'),
				
		));

		$arr_og = array();
		foreach ($arr_obj_objeto_grupo as $k=> $objeto_grupo){
			$arr_og[]= $objeto_grupo->data['ObjetoGrupo']['grupo_objeto_id'];
		}
		
		$arr_obj_objeto = $this->Objeto->findObjects('all',array(
				'conditions'=>array('id !='=> $arr_og)
		));

		$this->set(compact('arr_obj_objeto','arr_obj_objeto_grupo','id'));
	}
}
