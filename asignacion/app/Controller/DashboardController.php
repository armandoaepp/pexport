<?php 
App::uses('AppController', 'Controller');

class DashboardController extends AppController {

	public $helpers = array('Html', 'Form');
	public $name = 'Dashboard';

	public function beforeFilter(){
		$this->layout = 'ajax';
		parent::beforeFilter();
	}

	public function home() {
		$this->layout = 'dashboard';
	}
	
	public function dashboard() {
		$this->layout = 'dashboard';
	}
	
	public function listar_dashboard(){
		return true;
	}

	public function grafico_estado_ordenes_por_tecnico() {
		return true;
	}

	public function grafico_medidor_ordenes(){
		return true;
	}
	
	public function mapa_emergencia($movil = null){
		$this->set(compact('movil'));
	}

	public function grafico_pendientes_bar(){
		return true;
	}

	public function grafico_tecnico_semana(){
		return true;
	}

	public function grafico_cuadrillas_pie(){
		return true;
	}

	public function grafico_rendimiento_por_mes(){
		return true;
	}

	public function grafico_rendimiento_por_semana(){
		return true;
	}

	public function grafico_actividad(){
		return true;
	}

	public function mapa_monitoreo(){
		return true;
	}
	
	public function mapa_monitoreo2(){
		return true;
	}

	public function grafico_ordenes_infructuosas_por_tecnico(){
		return true;
	}

	public function grafico_cuadrillas_bar(){
		return true;
	}

	public function grafico_cuadrillas_donut(){
		return true;
	}

	public function grafico_actividad_finalizado(){
		return true;
	}
}