<?php 
App::uses('AppController', 'Controller');

class TipoAtributoController extends AppController {

	public $helpers = array('Html', 'Form');
	public $name = 'TipoAtributo';
	
	public function beforeFilter(){
		$this->layout = 'ajax';
		parent::beforeFilter();
	}

	/**
	 * Listado de Tipos de Atributo
	 * @author Geynen
	 * @version 06 Febrero 2015
	 */
	public function index() {
		$this->layout = 'dashboard';
		$arr_obj_tipo_atributo = $this->TipoAtributo->findObjects('all');
		
		$this->set(compact('arr_obj_tipo_atributo'));
	}
	
	/**
	 * Nuevo Tipo de Atributo
	 * @author Geynen
	 * @version 06 Febrero 2015
	 */
	public function nuevo() {
		$this->layout = 'dashboard';
		
		if ($this->request->is('post')) {
			if ($this->TipoAtributo->save($this->request->data)) {
				$this->Session->setFlash('Atributos Guardados Correctamente.','flash_success');
				$this->redirect(array('action' => 'index'));
			}
		}	
	}
	
	/**
	 * Editar Tipo de Atributo
	 * @param integer $id
	 * @author Geynen
	 * @version 06 Febrero 2015
	 */
	public function editar($id = null) {
		$this->layout = 'dashboard';
	
		if (!$id) {
			throw new NotFoundException(__('Invalid post'));
		}
		
		$obj_tipo_atributo = $this->TipoAtributo->findById($id);
		if (!$obj_tipo_atributo) {
			throw new NotFoundException(__('Invalid post'));
		}
		
		if ($this->request->is(array('post', 'put'))) {
			$this->TipoAtributo->id = $id;
			if ($this->TipoAtributo->save($this->request->data)) {
				$this->Session->setFlash('Datos Actualizados Correctamente.','flash_success');
				return $this->redirect(array('action' => 'index'));
			}
			$this->Session->setFlash('No se puede actualizar.','flash_error');
		}
		
		$this->set(compact('obj_tipo_atributo'));
	}
	
	/**
	 * Eliminar Tipo de Atributo
	 * @author Geynen
	 * @version 06 Febrero 2015
	 */
	public function eliminar($id = null) {
		$this->layout = 'dashboard';
	
	    if ($this->TipoAtributo->delete($id)) {
	        $this->Session->setFlash('El Tipo de Atributo con Id: ' . $id . ' ha sido eliminado.','flash_success');
	        $this->redirect(array('action' => 'index'));
	    }
	}
	
}