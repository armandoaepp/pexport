<?php 
App::uses('AppController', 'Controller');
App::uses('CakeEmail', 'Network/Email');

class MainController extends AppController {

	public $helpers = array('Html', 'Form');
	public $components = array('RequestHandler');
	public $name = 'Main';
	
	public function beforeFilter(){
		$this->Auth->allow(array('consultas','consulta_orden','envio_email_monitor_pdf_pg','generaacta_monitorear'));
		$this->layout = 'ajax';
		parent::beforeFilter();
	}
	
	public function breadcrumb($navegador=null,$case=null){
		$this->set(compact('navegador','case'));
	}
	
	public function monitorear($pendientes=null) {
		$this->layout = 'dashboard';
		$this->set(compact('pendientes'));
	}
	
	public function emergencia() {
		$this->layout = 'dashboard';
	}
	
	public function reporte_intervenciones(){
		$this->layout = 'dashboard';
	}
	
	public function perfilusuario(){
		$this->layout = 'dashboard';
	}
	
	public function consulta(){
		$this->layout = 'dashboard';
	}
	
	public function usuarios(){
		$this->layout = 'dashboard';
	}
	
	public function maestros(){
		$this->layout = 'dashboard';
	}
	
	public function asignar(){
		$this->layout = 'dashboard';
	}
	
	public function sincronizar(){
		$this->layout = 'dashboard';
	}
	
	public function consultas(){
	}
	
	public function personalizacion_formulario(){
	}
	
	public function actualizar_formulario(){
	}
	
	public function actualizar_emergencia_lec(){
	}
	
	public function consulta_orden(){
	}
	
	public function listar_monitorear(){
	}
	
	public function listar_monitor_avance(){
	}
	
	public function arbol_monitorear(){
	}
	
	public function ramaarbol(){
	}
	
	public function monitor_avance_persadic1b(){
	}
	
	public function monitor_avance_persadic1c(){
	}
	
	public function monitor_avance_persadic2b(){
	}
	
	public function monitor_avance_persadic2c(){
	}
	
	public function monitor_avance_persadic3b(){
	}
	
	public function monitor_avance_persadic3c(){
	}
	
	public function monitor_avance_persadic3d(){
	}
	
	public function exportar_monitorear(){
	}
	
	public function reporte_sumary_intervenciones_suministros(){
	}
	
	public function grafico_intervenciones_mes(){
	}
	
	public function grafico_tiempo_promedio(){
	}
	
	public function listar_perfilusuario(){
	}
	
	public function listar_consultaindividual(){
	}
	
	public function buscar_ordenes(){
	}
	
	public function orden_cronologia(){
		$this->helpers[] = "Image";
	}
	
	public function mapa_suministro(){
	}
	
	public function generaacta_monitorear(){
	}
	
	public function extension_listar_monitorear(){
	}
	
	public function monitor_avance_filtro(){
	}
	
	public function generaacta_multiple(){
	}
	
	public function monitor_avance_filtro_asignar_sector(){
	}
	
	public function asignar_orden(){
	}
	
	public function enviar_email_monitorear(){
	}
	
	public function envio_email_monitor_pdf_pg(){
	}
	
	public function arbol_usuarios(){
	}
	
	public function vista_crudusuarios(){
	}
	
	public function crud_usuarios(){
	}

	public function crud_personas(){
	}
	
	public function vista_crudasignaequipo(){
	}
	
	public function vista_crudpersonas(){
	}
	
	public function insertar_firma(){
	}
	
	public function crud_asignaequipo(){
	}
	
	public function mapa_rutas(){
	}
	
	public function arbol_maestros(){
	}
	
	public function vista_crudmaestrosconceptoubicacion(){
	}
	
	public function vista_crudmaestrosdetallereglajerarquiaubi(){
	}
	
	public function vista_crudmaestrosdetalleubicacionregla(){
	}
	
	public function vista_crudmaestrosintervencion(){
	}
	
	public function vista_crudmaestrosreglajerarquiaubicacion(){
	}
	
	public function vista_crudmaestrostipoasignacion(){
	}
	
	public function vista_crudmaestrostipomaestrosasignacion(){
	}
	
	public function vista_crudmaestrosubicacion(){
	}
	
	public function vista_crudmaestrosubicacionregla(){
	}
	
	public function vista_verformulario(){
	}
	
	public function vista_enlaces(){
	}
	
	public function crud_conceptoubicacion(){
	}
	
	public function crud_reglajerarquiaubicacion(){
	}
	
	public function crud_detallereglajerarquiaubi(){
	}
	
	public function crud_ubicacion(){
	}
	
	public function crud_ubicacionregla(){
	}
	
	public function crud_detalleubicacionregla(){
	}
	
	public function crud_tipoasignacion(){
	}
	
	public function crud_maestrosintervencion(){
	}
	
	public function crud_tipomaestrointervencion(){
	}
	
	public function formulario5(){
	}
	
	public function movil_multimaestro(){
	}
	
	public function arbol_asignar(){
	}
	
	public function listar_asignar(){
	}
	
	public function vista_asignadefinicion(){
	}
	
	public function arbol_subir(){
	}
	
	public function vista_subir(){
	}
	
	public function insertar_archivo(){
	}
	
	public function listar_vistapreviaarchivo(){
	}
	
	public function vista_consultarws(){
	}
	
	public function vistapreviaarchivodetalle(){
	}
	
	public function insertar_convertirarchivo(){
	}
	
	public function actualizar_convertirarchivo(){
	}
	
	public function eliminar_convertirarchivo(){
	}
	
	public function actualizar_emergencia(){
	}
	
	public function listar_emergencias(){
	}
	
	public function generaacta_multiple2(){
	}
		
	public function validarorden(){
	}
	
	public function listar_home(){
	}
}