<?php
App::uses('Component', 'Controller');

class HashPassComponent extends Component {
    public $components = array(
        'Session'
    );
    
    /**
     * Servicio que logea usuario
     * @param $url, $params
     * @return Array Pass Hash
     * @author Joel Vasquez Villalobos
     */
	// blowfish
	private static $algo = '$2a';
	
	// cost parameter
	private static $cost = '$10';
	
	// mainly for internal use
	public static function unique_salt() {
		return substr(sha1(mt_rand()),0,22);
	}

	// this will be used to generate a hash
	public function hash($password) {
		return crypt($password,	self::$algo . self::$cost .	'$' . self::unique_salt());
	}
	
	// this will be used to compare a password against a hash
	public function check_password($hash, $password) {
		$full_salt = substr($hash, 0, 29);
		$new_hash = crypt($password, $full_salt);
		if($hash == $new_hash){
			return true;
		}else{
			return false;
		}	
	}
}

?>