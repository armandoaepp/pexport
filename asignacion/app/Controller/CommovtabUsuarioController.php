<?php
App::uses('AppController', 'Controller');
App::uses('Sanitize', 'Utility');
App::uses('CakeTime', 'Utility');
class CommovtabUsuarioController extends AppController {

	public $name = 'CommovtabUsuario';
	public $helpers = array('Html', 'Form');
		
	public function beforeFilter(){	
		$this->Auth->allow(array('login'));
		parent::beforeFilter();
	}
	
	function login(){
		set_time_limit(0);
		$this->layout = 'login';
		$this->components[] = 'HashPass';
		
		if ($this->request->is('post')){
			if (!empty($this->data) && !($this->Auth->login())){
				$usuario = $this->request->data['Usuario']['usuario'];
				$pass = $this->request->data['Usuario']['pass'];
				
				$data= array();
					
				$obj_user_login = $this->CommovtabUsuario->validarUsuarioWeb($usuario);
					
				if(count($obj_user_login)==1){
					$user_id = $obj_user_login[0][0]['idusuario'];
					$db_password = $obj_user_login[0][0]['passusuario'];
					// check the password the user tried to login with
					$is_valid_pass =  $this->Components->load('HashPass')->check_password($db_password, $pass);

					if($is_valid_pass){
						$data['IdUsuario'] = $user_id;
						$data['success'] = true;
						$data['message'] = 'Bienvenido: '.$obj_user_login[0][0]['nompersona'];
					
						//Inicio de sesion exitosa
						$data_login = json_encode($data);
					}else{
						// deny access
						$data['success'] = false;
						$data['message'] = 'contrasenia incorrecta';
						$data_login = json_encode($data);
					}
						
					
				}else{
					$data['success'] = false;
					$data['message'] = 'Usuario no existe o duplicidad Usuarios';
					$data_login = json_encode($data);
				}

				if($data_login){
					$usuario= json_decode($data_login);
					if($usuario->success != false){
						$this->set('auth', true);
						//debug($obj_user_login);exit();						
						$this->Session->write('idusuario', $obj_user_login[0][0]['idusuario']);
						$this->Session->write('nomusuario', $obj_user_login[0][0]['nomusuario']);
						$this->Session->write('nompersona', $obj_user_login[0][0]['nompersona']);
						$this->Session->write('fotopersona', $obj_user_login[0][0]['foto']);
						//$this->Session->check('username');
						
						$obj_user = $this->CommovtabUsuario->findByIdusuario($usuario->IdUsuario);
						$this->Auth->login($obj_user);
						$this->set('obj_logged_user', $obj_user);
						
						return $this->redirect(array('controller' => 'dashboard', 'action' => 'index'));
						//return $this->redirect($this->Auth->redirect());
					}else{
						return $this->redirect($this->Auth->logout());
					}
				}else{
					return $this->redirect($this->Auth->logout());
				}
			}else{
				return $this->redirect($this->Auth->logout());
			}
		}

		/* redirect if user exist */
		if ($this->Auth->loggedIn()){
			return $this->redirect($this->Auth->redirect());
		}
	}
	
	public function logout() {
		/* logout and redirect to url set in app controller */
		$this->Session->destroy();
		return $this->redirect($this->Auth->logout());
	}

	public function index(){
		$this->layout = 'home_page';
		//$this->loadModel('GlomasEmpleado');
		//$arr_obj_user = $this->GlomasEmpleado->find('all');
		//pr($arr_obj_user);
		//debug($data);
		//pr($data);
		//print_r($data);
		//exit();
		$hola="hello word";
		$this->set(compact('hola'));		
	}
}
