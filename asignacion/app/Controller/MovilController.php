<?php 
App::uses('AppController', 'Controller');

class MovilController extends AppController {

	public $helpers = array('Html', 'Form');
	public $components = array('RequestHandler');
	public $name = 'Movil';
	
	public function beforeFilter(){
		$this->layout = 'ajax';
		parent::beforeFilter();
	}
	
	public function formulario5(){	
	}
	
	public function formulario4(){
	}
	
	public function main(){
	}
	
	public function index(){
	}
	
	public function movil_multimaestro(){
	}
	
	public function actualizar_formulario(){
	}
	
	public function personalizacion_formulario(){
	}
}