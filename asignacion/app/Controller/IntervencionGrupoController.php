<?php 
App::uses('AppController', 'Controller');

class IntervencionGrupoController extends AppController {

	public $helpers = array('Html', 'Form');
	public $name = 'IntervencionGrupo';
	
	public function beforeFilter(){
		$this->layout = 'ajax';
		parent::beforeFilter();
	}
	
	/**
	 * Crear Json de Formulario por tipo de intervencion
	 * @author Geynen
	 * @version 13 Febrero 2015
	 */
	public function formulario_json($intervencion_tipo_id = null) {
		$this->layout = 'ajax';
		
		if (!$intervencion_tipo_id) {
			throw new NotFoundException(__('Invalid post'));
		}
		
		$this->loadModel('IntervencionGrupo');
		$json_form = $this->IntervencionGrupo->getJsonForm($intervencion_tipo_id);
		
		//echo $json_form;	
		$this->set(compact('json_form'));
	}
	
	/**
	 * Crear formulario por tipo de intervencion
	 * @author Alan Hugo
	 * @version 09 Febrero 2015
	 */
	public function formulario($intervencion_tipo_id = null) {
		$this->layout = 'dashboard';
		
		if (!$intervencion_tipo_id) {
			throw new NotFoundException(__('Invalid post'));
		}
		
		$this->loadModel('Objeto');
		
		$this->Objeto->virtualFields = array(
				'intervencion_grupo_id' => 'IntervencionGrupo.id'
		);
		
		$arr_obj_objeto_save = $this->Objeto->findObjects('all',array(
				'fields'=>'Objeto.*, intervencion_grupo_id',
				'joins'=>array(
						array('table' => 'actividades.intervencion_grupos',
								'alias' => 'IntervencionGrupo',
								'type' => 'INNER',
								'conditions' => array(
										'IntervencionGrupo.grupo_objeto_id = Objeto.id'
								))
				),
				'conditions'=>array('intervencion_tipo_id'=>$intervencion_tipo_id,'parent_id is null'),
				'order'=>array('IntervencionGrupo.orden asc')
		));
		
		//debug($arr_obj_objeto_save);exit();
		$arr_og = array();
		foreach ($arr_obj_objeto_save as $k=> $obj_objeto_save){
			$arr_og[]= $obj_objeto_save->getID();
		}
		//debug($arr_og);
		$this->Objeto->virtualFields = array();
		$arr_obj_objeto = $this->Objeto->findObjects('all',array(
				'conditions'=>array('Objeto.id !='=> $arr_og)
		));
		//debug($arr_obj_objeto);exit();
		$this->set(compact('arr_obj_objeto','arr_obj_objeto_save','intervencion_tipo_id'));
	}
	
	/**
	 * Nuevo Grupo de Objetos a formulario (ajax)
	 * @param integer $intervencion_tipo_id
	 * @param Array $arr_item_objeto_id
	 * @author Alan Hugo
	 * @version 15 Febrero 2015
	 */
	public function ajax_guardar($intervencion_tipo_id = null, $grupo_objeto_id = null) {
		$this->layout = 'dashboard';
		$this->loadModel('IntervencionItem');
		$this->loadModel('Objeto');
	
		$this->request->data['IntervencionGrupo']['intervencion_tipo_id'] = $intervencion_tipo_id;
		$this->request->data['IntervencionGrupo']['grupo_objeto_id'] = $grupo_objeto_id;
		$this->request->data['IntervencionGrupo']['orden'] = 1;
		$this->IntervencionGrupo->id = null;
		$this->IntervencionGrupo->save($this->request->data);
		$intervencion_grupo_id = $this->IntervencionGrupo->id;
		
		$obj_objeto = $this->Objeto->findById($grupo_objeto_id);
		$this->guardar_items($intervencion_tipo_id, $obj_objeto, $intervencion_grupo_id);
		$success = true;
	
		if (isset($success) && $success) {
			echo json_encode(array('success'=>true, 'msg'=>'Guardado Correctamente.', 'intervencion_grupo_id'=>$intervencion_grupo_id));
			exit;
		}
	
		echo json_encode(array('success'=>false, 'msg'=>'Ocurrio un Error.'));
		exit;
	}
	
	public function guardar_items($intervencion_tipo_id = null, $obj_objeto, $intervencion_grupo_id = null) {
		$this->loadModel('IntervencionItem');
		
		foreach ($obj_objeto->ObjetoGrupo as $kk => $obj_objeto_grupo){
			$obj_objeto = $obj_objeto_grupo->Objeto;
			if($obj_objeto->TipoObjeto->getAttr('objetos')){
				$this->request->data['IntervencionGrupo']['intervencion_tipo_id'] = $intervencion_tipo_id;
				$this->request->data['IntervencionGrupo']['grupo_objeto_id'] = $obj_objeto->getID();
				$this->request->data['IntervencionGrupo']['parent_id'] = $intervencion_grupo_id;
				$this->request->data['IntervencionGrupo']['orden'] = $kk+1;
				$this->IntervencionGrupo->id = null;
				$this->IntervencionGrupo->save($this->request->data);
				//Guardar Intervencion tambien
				$this->request->data['IntervencionItem']['intervencion_grupo_id'] = $intervencion_grupo_id;
				$this->request->data['IntervencionItem']['objeto_grupo_id'] = $obj_objeto_grupo->getID();
				$this->IntervencionItem->id = null;
				$this->IntervencionItem->save($this->request->data);
				
				$obj_objeto = $this->Objeto->findById($obj_objeto->getID());
				$this->guardar_items($intervencion_tipo_id, $obj_objeto, $this->IntervencionGrupo->id);
			}else{
				$this->request->data['IntervencionItem']['intervencion_grupo_id'] = $intervencion_grupo_id;
				$this->request->data['IntervencionItem']['objeto_grupo_id'] = $obj_objeto_grupo->getID();
				$this->request->data['IntervencionItem']['orden'] = $kk+1;
				$this->IntervencionItem->id = null;
				$this->IntervencionItem->save($this->request->data);
			}
		}
	}
	
	/**
	 * Eliminar Grupo de Objeto del formulario (ajax)
	 * @param integer $intervencion_tipo_id
	 * @param Array $arr_item_objeto_id
	 * @author ALan Hugo
	 * @version 13 Febrero 2015
	 */
	public function ajax_eliminar($intervencion_tipo_id = null, $intervencion_grupo_id = null) {
		$this->layout = 'dashboard';
		$this->loadModel('IntervencionItem');
		$this->loadModel('Objeto');
	
		$obj_intervencion_group = $this->IntervencionGrupo->findById($intervencion_grupo_id);
		$obj_objeto = $this->Objeto->findById($obj_intervencion_group->getAttr('grupo_objeto_id'));		
		$this->eliminar_items($intervencion_tipo_id, $obj_objeto);

		$success = true;

		if (isset($success) && $success) {
			echo json_encode(array('success'=>true, 'msg'=>'Se elimino correctamente'));
			exit;
		}
	
		echo json_encode(array('success'=>false, 'msg'=>'Ocurrio un Error.'));
		exit;
	}
	
	/**
	 * Quitar Item o Grupo de Objeto del formulario (ajax)
	 * @param integer $intervencion_tipo_id
	 * @param Array $arr_item_objeto_id
	 * @author ALan Hugo
	 * @version 27 Febrero 2015
	 */
	public function ajax_quitar($intervencion_tipo_id = null, $intervencion_item_id = null, $tipoobjeto = 'item') {
		$this->layout = 'dashboard';
		$this->loadModel('IntervencionItem');
		$this->loadModel('Objeto');
	
		if($tipoobjeto == 'item'){
			$this->IntervencionItem->setSchema($this->IntervencionItem->schema);
			$this->IntervencionItem->deleteAll(array('id' => $intervencion_item_id),false);
	
			$success = true;
		}else{
			$obj_intervencion_item = $this->IntervencionItem->findById($intervencion_item_id);
			$obj_intervencion_grupo = $this->IntervencionGrupo->findObjects('first',
					array('conditions'=>
							array('parent_id'=>$obj_intervencion_item->getAttr('intervencion_grupo_id'),
									'grupo_objeto_id'=>$obj_intervencion_item->ObjetoGrupo->getAttr('item_objeto_id')
							)
					)
			);
			
			$this->Objeto->virtualFields = array(
					'intervencion_grupo_id' => 'IntervencionGrupo.id'
			);
			$obj_objeto = $this->Objeto->findObjects('first',array(
					'fields'=>'Objeto.*, intervencion_grupo_id',
					'joins'=>array(
							array('table' => 'actividades.intervencion_grupos',
									'alias' => 'IntervencionGrupo',
									'type' => 'INNER',
									'conditions' => array(
											'IntervencionGrupo.grupo_objeto_id = Objeto.id'
									))
					),
					'conditions'=> array('Objeto.id'=>$obj_intervencion_grupo->getAttr('grupo_objeto_id'))));
			$this->eliminar_items($intervencion_tipo_id, $obj_objeto);

			$this->IntervencionItem->setSchema($this->IntervencionItem->schema);
			$this->IntervencionItem->deleteAll(array('id' => $intervencion_item_id),false);
			
			$success = true;
		}
	
		if (isset($success) && $success) {
			echo json_encode(array('success'=>true, 'msg'=>'Se elimino correctamente'));
			exit;
		}
	
		echo json_encode(array('success'=>false, 'msg'=>'Ocurrio un Error.'));
		exit;
	}
	
	public function eliminar_items($intervencion_tipo_id = null, $obj_objeto){
		$this->loadModel('IntervencionItem');
		$arr_obj_objetos = $obj_objeto->getItemsGrupo($intervencion_tipo_id);
		foreach ($arr_obj_objetos as $kk => $obj_objeto){
			if($obj_objeto->TipoObjeto->getAttr('objetos')){
				//Delete items				
				$this->IntervencionItem->setSchema($this->IntervencionItem->schema);
				$this->IntervencionItem->deleteAll(
						array('id' => $obj_objeto->getAttr('intervencion_item_id')),false);
				$this->eliminar_items($intervencion_tipo_id, $obj_objeto);
			}else{
				//Delete items
				$this->IntervencionGrupo->setSchema($this->IntervencionGrupo->schema);
				$this->IntervencionGrupo->deleteAll(
						array('id' => $obj_objeto->getAttr('intervencion_grupo_id')),false);
				
				//Delete grupos
				$this->IntervencionItem->setSchema($this->IntervencionItem->schema);
				$this->IntervencionItem->deleteAll(
						array('id' => $obj_objeto->getAttr('intervencion_item_id')),false);
			}
		}
	}
	
	/**
	 * Cambiar orden de objetos en el formulario (ajax)
	 * @param integer $intervencion_tipo_id
	 * @param integer $arr_item_objeto_id
	 * @author Alan Hugo
	 * @version 13 Febrero 2015
	 */
	public function ajax_cambiar_orden($intervencion_tipo_id = null, $arr_intervencion_grupo_id = null) {
		$this->layout = 'dashboard';
	
		$arr_intervencion_grupo_id = json_decode($arr_intervencion_grupo_id);
		$this->IntervencionGrupo->setSchema($this->IntervencionGrupo->schema);
		foreach ($arr_intervencion_grupo_id as $i => $id){
			$this->IntervencionGrupo->updateAll(array('orden' => $i+1),array('id' => $id));
		}
	
		echo json_encode(array('success'=>true, 'msg'=>'Actualizo el orden Correctamente.'));
		exit;
	}
	
	/**
	 * Cambiar orden de objetos en el formulario (ajax)
	 * @param integer $intervencion_tipo_id
	 * @param integer arr_intervencion_item_id
	 * @author Alan Hugo
	 * @version 24 Febrero 2015
	 */
	public function ajax_cambiar_orden_item($arr_intervencion_item_id = null) {
		$this->layout = 'dashboard';
		$this->loadModel('IntervencionItem');
		
		$arr_intervencion_item_id = json_decode($arr_intervencion_item_id);
		$this->IntervencionItem->setSchema($this->IntervencionItem->schema);
		foreach ($arr_intervencion_item_id as $i => $id){
			$this->IntervencionItem->updateAll(array('orden' => $i+1),array('id' => $id));
		}
	
		echo json_encode(array('success'=>true, 'msg'=>'Actualizo el orden Correctamente.'));
		exit;
	}
	
	/**
	 * Agregar orden de objetos en el formulario (ajax)
	 * @param integer $intervencion_tipo_id
	 * @param integer $arr_item_objeto_id
	 * @author Alan Hugo
	 * @version 17 Febrero 2015
	 */
	public function agregar_item($intervencion_tipo_id = null, $intervencion_grupo_id = null, $objeto_id = null, $objeto_grupo_id = null, $attr = null) {
		$this->layout = 'dashboard';
		$this->loadModel('Objeto');
		$this->loadModel('ObjetoGrupo');
		$this->loadModel('IntervencionItem');
		
		if($attr=='grupo'){
			$grupo_objeto_id = $objeto_id;
			$this->request->data['IntervencionGrupo']['intervencion_tipo_id'] = $intervencion_tipo_id;
			$this->request->data['IntervencionGrupo']['grupo_objeto_id'] = $grupo_objeto_id;
			$this->request->data['IntervencionGrupo']['parent_id'] = $intervencion_grupo_id;
			$this->request->data['IntervencionGrupo']['orden'] = 0;
			$this->IntervencionGrupo->id = null;
			$this->IntervencionGrupo->save($this->request->data);
			
			
			$this->request->data['IntervencionItem']['intervencion_grupo_id'] = $intervencion_grupo_id;
			$this->request->data['IntervencionItem']['objeto_grupo_id'] = $objeto_grupo_id;
			$this->request->data['IntervencionItem']['orden'] = 0;
			$this->IntervencionItem->id = null;
			$this->IntervencionItem->save($this->request->data);
			
			$obj_objeto = $this->Objeto->findById($grupo_objeto_id);
			$this->guardar_items($intervencion_tipo_id, $obj_objeto, $this->IntervencionGrupo->id);
			
		}else if($attr=='item'){
			$this->request->data['IntervencionItem']['intervencion_grupo_id'] = $intervencion_grupo_id;
			$this->request->data['IntervencionItem']['objeto_grupo_id'] = $objeto_id;
			$this->request->data['IntervencionItem']['orden'] = 0;
			$this->IntervencionItem->id = null;
			$this->IntervencionItem->save($this->request->data);
		}
		
		$success = true;
	
		if (isset($success) && $success) {
			echo json_encode(array('success'=>true, 'msg'=>'Guardado Correctamente.', 'intervencion_item_id'=>$this->IntervencionItem->id));
			exit;
		}
	
		echo json_encode(array('success'=>false, 'msg'=>'Ocurrio un Error.'));
		exit;
	}
	
	/**
	 * Vista Previa de Formulario por tipo de intervencion
	 * @author Geynen
	 * @version 13 Febrero 2015
	 */
	public function formulario_preview($intervencion_tipo_id = null) {
		$this->layout = 'default_no_header';
		$this->helpers[] = 'Formulario';
	
		if (!$intervencion_tipo_id) {
			throw new NotFoundException(__('Invalid post'));
		}
	
		$this->loadModel('IntervencionGrupo');
		$json_form = $this->IntervencionGrupo->getJsonForm($intervencion_tipo_id);
	
		//echo $json_form;
		$this->set(compact('json_form'));
	}
	
	/**
	 * Muestra la vista del modal de configuracion por item
	 * @param integer $intervencion_tipo_id
	 * @param integer $intervencion_item_id
	 * @return View
	 * @author Geynen
	 * @version 18 Febrero 2015
	 */
	public function ajax_config_item($intervencion_tipo_id,$intervencion_item_id){
		$this->helpers[] = 'Formulario';
		
		$this->loadModel('Objeto');
		$this->Objeto->virtualFields = array(
				'intervencion_grupo_id' => 'IntervencionGrupo.id'
		);
		$arr_obj_objeto_save = $this->Objeto->findObjects('all',array(
				'fields'=>'Objeto.*, intervencion_grupo_id',
				'joins'=>array(
						array('table' => 'actividades.intervencion_grupos',
								'alias' => 'IntervencionGrupo',
								'type' => 'INNER',
								'conditions' => array(
										'IntervencionGrupo.grupo_objeto_id = Objeto.id'
								))
				),
				'conditions'=>array('intervencion_tipo_id'=>$intervencion_tipo_id,'parent_id is null'),
				'order'=>array('IntervencionGrupo.orden asc')
		));
		
		$this->loadModel('IntervencionItem');
		$obj_intervencion_item = $this->IntervencionItem->findById($intervencion_item_id);
		
		$this->loadModel('IntervencionTipoWebService');
		$obj_intervencion_tipo_web_service = $this->IntervencionTipoWebService->findObjects('first',array(
				'conditions'=>array('intervencion_tipo_id'=>$intervencion_tipo_id,'accion'=>'SAVE'),
			));
		
		$this->set(compact('obj_intervencion_item','arr_obj_objeto_save','intervencion_tipo_id','obj_intervencion_tipo_web_service'));
	}
	
	/**
	 * Guarda la Configuracion por Item
	 * @param integer $intervencion_tipo_id
	 * @param integer $intervencion_item_id
	 * @throws NotFoundException
	 * @return json
	 * @author Geynen
	 * @version 18 Febrero 2015
	 */
	public function ajax_config_item_save($intervencion_tipo_id,$intervencion_item_id){
		$this->helpers[] = 'Formulario';
	
		$this->loadModel('IntervencionItem');
		$obj_intervencion_item = $this->IntervencionItem->findById($intervencion_item_id);
		if (!$obj_intervencion_item) {
			throw new NotFoundException(__('Invalid post'));
		}
		
		if ($this->request->is(array('post', 'put'))) {
			$this->IntervencionItem->id = $intervencion_item_id;
			$arr_data = array();
			//$arr_data['IntervencionItem']['id'] = $obj_intervencion_item;
			$arr_data['IntervencionItem']['defecto'] = $this->request->data['defecto'];
			$arr_data['IntervencionItem']['defecto_desde_intervencion_item_id'] = $this->request->data['defecto_desde_intervencion_item_id'];
			$arr_data['IntervencionItem']['campo_servicio'] = $this->request->data['campo_servicio'];
			if ($this->IntervencionItem->save($arr_data)) {
				echo json_encode(array('success'=>true,'msg'=>'Guardado Correctamente'));
			}else{
				echo json_encode(array('success'=>false,'msg'=>'Ocurrio un error.'));
			}
		}
		exit();
	}
}
