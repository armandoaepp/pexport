<?php
date_default_timezone_set('America/Lima');
/*timer = setTimeout('auto_reload()',5000);*/
?>

<!DOCTYPE html> 
<html> 
<head> 
    <title>Pexport Movil</title> 
    <meta name="viewport" content="width=device-width, initial-scale=1"> 
    <link rel="stylesheet" href="jquery.mobile-1.3.2.min.css" />
    <script src="http://code.jquery.com/jquery-1.8.3.min.js"></script>
    <script src="jquery.mobile-1.3.2.min.js"></script>
</head> 
<body> 

<div data-role="page" data-theme="a">

    <div data-role="header" data-position="fixed">
    <h1>Pexport Movil</h1>
    <a href="main.php" data-icon="back" class="ui-btn-left" >Regresar</a>
    <a href="#" data-icon="gear" class="ui-btn-right"  >Grabar</a>
    </div><!-- /header -->

    <div data-role="content"  style="background-size:100%;">


    <div class="ui-grid-a" >
        <div class="ui-block-a">
            <div class="ui-bar ui-bar-a" >
                <div data-role="fieldcontain" class="ui-field-contain ui-body ui-br">
                <label for="name-a" class="ui-input-text">Solicitud:</label>
                <input type="text" name="name" id="name-a" value="" >
                </div>
            </div>
        </div>
   


        <div class="ui-block-a">
            <div class="ui-bar ui-bar-a">
                <div data-role="fieldcontain" class="ui-field-contain ui-body ui-br">
                <label for="name-a" class="ui-input-text">Suministro:</label>
                <input type="text" name="name" id="name-a" value="" >
                </div>
                <div data-role="fieldcontain" class="ui-field-contain ui-body ui-br">
                <label for="name-a" class="ui-input-text">Orden:</label>
                <input type="text" name="name" id="name-a" value="" >
                </div>
                <div data-role="fieldcontain" class="ui-field-contain ui-body ui-br">
                <label for="name-a" class="ui-input-text">Fecha:</label>
                <input type="text" name="name" id="name-a" value="" >
                </div>

                <div data-role="fieldcontain" class="ui-field-contain ui-body ui-br">
                <label for="name-a" class="ui-input-text">S Anterior:</label>
                <input type="text" name="name" id="name-a" value="" >
                </div>
                <div data-role="fieldcontain" class="ui-field-contain ui-body ui-br">
                <label for="name-a" class="ui-input-text">S Posterior:</label>
                <input type="text" name="name" id="name-a" value="" >
                </div>
            </div>
        </div>
        <div class="ui-block-b">
            <div class="ui-bar ui-bar-a">
                <div data-role="fieldcontain" class="ui-field-contain ui-body ui-br">
                <label for="name-a" class="ui-input-text">Responsable:</label>
                <input type="text" name="name" id="name-a" value="" >
                </div>
                <div data-role="fieldcontain" class="ui-field-contain ui-body ui-br">
                <label for="name-a" class="ui-input-text">Supervisor:</label>
                <input type="text" name="name" id="name-a" value="" >
                </div>
                <div data-role="fieldcontain" class="ui-field-contain ui-body ui-br">
                <label for="name-a" class="ui-input-text">Ejecutor:</label>
                <input type="text" name="name" id="name-a" value="" >
                </div>
            </div>
        </div>

        <div class="ui-block-a">
            <div class="ui-bar ui-bar-a" >
                <div data-role="fieldcontain" class="ui-field-contain ui-body ui-br">
                <label for="name-a" class="ui-input-text">Tipo:</label>
                <div class="ui-select">

                <select name="select-choice-a" id="select-choice-a" data-native-menu="false" tabindex="-1" data-theme="e">
                    <option data-placeholder="true">Tipo</option>
                    <option value="standard">Instalacion Nueva</option>
                    <option value="standard">Remodelacion</option>
                    <option value="rush">Ampliacion de Potencia</option>
                    <option value="rush">Servicio Extraodinario</option>
                    <option value="rush">Reapertura</option>
                    <option value="rush">Inspeccion</option>
                    <option value="rush">Notificacion</option>
                    <option value="rush">Clandestinaje</option>
                    <option value="rush">Varios</option>
                    <option value="rush">Emergencia</option>
                    <option value="rush">Reclamo</option>
                </select></div>

                </div>
            </div>
        </div>


        <div class="ui-block-a">
            <div class="ui-bar ui-bar-a">
                <div data-role="fieldcontain" class="ui-field-contain ui-body ui-br">
                <label for="name-a" class="ui-input-text">Cliente:</label>
                <input type="text" name="name" id="name-a" value="" >
                </div>
                <div data-role="fieldcontain" class="ui-field-contain ui-body ui-br">
                <label for="name-a" class="ui-input-text">Direccion:</label>
                <input type="text" name="name" id="name-a" value="" >
                </div>
                <div data-role="fieldcontain" class="ui-field-contain ui-body ui-br">
                <label for="name-a" class="ui-input-text">Vivienda:</label>
                <div class="ui-select">
                <select name="select-choice-a" id="select-choice-a" data-native-menu="false" tabindex="-1" data-theme="b">
                    <option data-placeholder="true">Tipo</option>
                    <option value="standard">Propio</option>
                    <option value="standard">Familiar</option>
                    <option value="rush">Alquilado</option>
                </select></div>
                </div>


                <div data-role="fieldcontain" class="ui-field-contain ui-body ui-br">
                <label for="name-a" class="ui-input-text"></label>
                <div class="ui-select">
                <select name="select-choice-a" id="select-choice-a" data-native-menu="false" tabindex="-1" data-theme="b">
                    <option data-placeholder="true">Construccion</option>
                    <option value="standard">Rustico</option>
                    <option value="standard">Noble</option>
                    <option value="rush">Terreno</option>
                    <option value="rush">Adobe</option>
                    <option value="rush">Adobe Noble</option>
                    <option value="rush">Construccion</option>
                </select></div>
                </div>

                <div data-role="fieldcontain" class="ui-field-contain ui-body ui-br">
                <label for="name-a" class="ui-input-text"></label>
                <div class="ui-select">
                <select name="select-choice-a" id="select-choice-a" data-native-menu="false" tabindex="-1" data-theme="b">
                    <option data-placeholder="true" >Uso</option>
                    <option value="standard">Vivienda</option>
                    <option value="standard">Taller</option>
                    <option value="rush">Comercio</option>
                    <option value="rush">Industria</option>
                    <option value="rush">Edificio</option>
                    <option value="rush">Comunitario</option>
                </select></div>
                </div>

                <div data-role="fieldcontain" class="ui-field-contain ui-body ui-br">
                <label for="name-a" class="ui-input-text"></label>
                <div class="ui-select">
                <select name="select-choice-a" id="select-choice-a" data-native-menu="false" tabindex="-1" data-theme="b">
                    <option data-placeholder="true">Situacion</option>
                    <option value="standard">Habilitado</option>
                    <option value="standard">Deshabilitado</option>
                    <option value="rush">Abandonado</option>
                    <option value="rush">Siniestrado</option>
                    <option value="rush">Ruina</option>
                </select></div>
                </div>
            </div>
        </div>

        <div class="ui-block-b">
            <div class="ui-bar ui-bar-a">
                <div data-role="fieldcontain" class="ui-field-contain ui-body ui-br">
                <label for="name-a" class="ui-input-text">Zona:</label>
                <input type="text" name="name" id="name-a" value="" >
                </div>
                <div data-role="fieldcontain" class="ui-field-contain ui-body ui-br">
                <label for="name-a" class="ui-input-text">Localidad:</label>
                <input type="text" name="name" id="name-a" value="" >
                </div>
            </div>
        </div>


        <div class="ui-block-a">
            <div class="ui-bar ui-bar-a" >
                <div data-role="fieldcontain" class="ui-field-contain ui-body ui-br">
                <label for="name-a" class="ui-input-text">Medidor</label>
                <div class="ui-select">

                <select name="select-choice-a" id="select-choice-a" data-native-menu="false" tabindex="-1" data-theme="e">
                    <option data-placeholder="true">Tipo</option>
                    <option value="standard">Instalado</option>
                    <option value="standard">Retirado</option>
                    <option value="rush">Existente</option>

                </select></div>
                </div>
            </div>
        </div>

        <div class="ui-block-a">
            <div class="ui-bar ui-bar-a" >
                <div data-role="fieldcontain" class="ui-field-contain ui-body ui-br">
                <label for="name-a" class="ui-input-text">Numero:</label>
                <input type="text" name="name" id="name-a" value="" >
                </div>
                <div data-role="fieldcontain" class="ui-field-contain ui-body ui-br">
                <label for="name-a" class="ui-input-text">Marca:</label>
                <input type="text" name="name" id="name-a" value="" >
                </div>
                <div data-role="fieldcontain" class="ui-field-contain ui-body ui-br">
                <label for="name-a" class="ui-input-text">Modelo:</label>
                <input type="text" name="name" id="name-a" value="" >
                </div>
                <div data-role="fieldcontain" class="ui-field-contain ui-body ui-br">
                <label for="name-a" class="ui-input-text">Lectura:</label>
                <input type="text" name="name" id="name-a" value="" >
                </div>
                <div data-role="fieldcontain" class="ui-field-contain ui-body ui-br">
                <label for="name-a" class="ui-input-text">Anio Fab:</label>
                <input type="text" name="name" id="name-a" value="" >
                </div>
            </div>
        </div>

        <div class="ui-block-b">
            <div class="ui-bar ui-bar-a" >
                <div data-role="fieldcontain" class="ui-field-contain ui-body ui-br">
                <label for="name-a" class="ui-input-text">Numero:</label>
                <input type="text" name="name" id="name-a" value="" >
                </div>
                <div data-role="fieldcontain" class="ui-field-contain ui-body ui-br">
                <label for="name-a" class="ui-input-text">Marca:</label>
                <input type="text" name="name" id="name-a" value="" >
                </div>
                <div data-role="fieldcontain" class="ui-field-contain ui-body ui-br">
                <label for="name-a" class="ui-input-text">Modelo:</label>
                <input type="text" name="name" id="name-a" value="" >
                </div>
                <div data-role="fieldcontain" class="ui-field-contain ui-body ui-br">
                <label for="name-a" class="ui-input-text">Lectura:</label>
                <input type="text" name="name" id="name-a" value="" >
                </div>
                <div data-role="fieldcontain" class="ui-field-contain ui-body ui-br">
                <label for="name-a" class="ui-input-text">Anio Fab:</label>
                <input type="text" name="name" id="name-a" value="" >
                </div>
            </div>
        </div>


    </div><!-- /grid-c -->

        
    </div><!-- /content -->



</div><!-- /page -->

</body>
</html>