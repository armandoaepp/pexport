var db;
	
	var webServices = "http://190.41.196.139:9090/Ws.Asignacion/api/v1/index.php";
	var url = webServices + "/login/";
	var urldata = webServices + "/data/";
	var urlconfirm = webServices + "/syncresp/";
	var userid = localStorage.getItem('userid') || '<empty>';
	var user = localStorage.getItem('user') || '<empty>';
	var pass = localStorage.getItem('pass') || '<empty>';
	var apikey = localStorage.getItem('apikey') || '<empty>';
	var tipoorden = localStorage.getItem('tipoorden') || '<empty>';
	var idorden = localStorage.getItem('idorden') || '<empty>';
	var grupos = [];
	var orden;
	var d = new Date();
	var intervenciones;
	var ordenes;
	var tipointervenciones;
	var maestrosintervencion;
	var tiposmaestrosintervencion;
	var dataJSON;
	var latitud;
	var longitud;
	var ido;
	
  function populateDB(tx) {
    tx.executeSql('CREATE TABLE IF NOT EXISTS usuario (userid integer PRIMARY KEY, user, pass, apikey);');
  	tx.executeSql('CREATE TABLE IF NOT EXISTS orden (IdOrden integer PRIMARY KEY, IdDocumento, DownMovil,IdEstado,Created,TipoServicio,NombreSector,NombreCliente,DireccionCliente,Cartera,Alimentador,Sed,Medidor,Condicion,Exportado,Suministro,IdOrdenTrabajo,Empresa,IdUnidadNegocio,UnidadNegocio,IdCentroServicio,CentroServicio,Proveedor,Actividad,SubActividad,FechaGeneracion,Periodo,UsuarioGenero,Observacion,Solicitud,SuministroAnterior,SuministroPosterior,NombreSuministro,DireccionSuministro,UltimaFacturacion,IdCiclo,Ciclo,IdSector,Sector,IdRuta,Ruta,LCorrelati,Localidad,Zona,TipoZona,Potencia,Tarifa,TipoSuministro,Sistema,TipoConexion,SerieMedidor,MarcaMedidor,ModeloMedidor,DigitosMedidor,LecturaAnterior,Promedio,ConsumoAnterior,Responsable,Supervisor,Ejecutor, NroReclamo, NroAtencion, TipoNegocio);');
  	tx.executeSql('CREATE TABLE IF NOT EXISTS intervencion (IdIntervencion integer primary key, IdOrden, IdUsuario, FechaAsignado, FechaAtencion,Coordenadas, estado);');
  	tx.executeSql('CREATE TABLE IF NOT EXISTS tipointervencion (IdTipoIntervencion integer primary key, DetalleTipoIntervencion,  IdTipoIntervencionSup );');
  	tx.executeSql('CREATE TABLE IF NOT EXISTS MaestrosIntervencion (IdMaestrosIntervencion integer primary key, IdentificadorMaestroIntervencion,  EtiquetaMaestroIntervencion, TipoMaestroIntervencion,LongitudTipoDatoMaestrosInterv, ControlObjetoMaestroIntervencion, EstiloObjetoMaestroIntervencion, PosicioMaestroIntervencion, RaizMaestroIntervencion);');
  	tx.executeSql('CREATE TABLE IF NOT EXISTS tipomaestrosintervencion (idtipointervencion integer, idmaestrosintervencion integer);');
  	tx.executeSql('CREATE TABLE IF NOT EXISTS detalleintervencion (IdDetalleIntervencion INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,idorden ,idmaestrosintervencion,detalleintervencion);');
  }

  function errorCB(err) {
       console.log("Error processing SQL: Codigo: " + err.code + " Mensaje: "+err.message);
  }

  function successCB() {
    alert("bien!");
  }
  
  function AgregaUsuario(tx) {
  	tx.executeSql('INSERT INTO usuario (userid, user, pass, apikey) VALUES (?, ?, ?, ?)', [userid, user, pass, apikey]);
  }
  
  function AgregaIntervencion(tx) {
	  $.each(intervenciones, function( key, val ) {
		  sql = 'INSERT INTO intervencion (IdIntervencion, IdOrden, IdUsuario, FechaAsignado, FechaAtencion,Coordenadas, estado) VALUES ('+val.IdIntervencion+',"'+val.IdOrden+'",'+val.IdUsuario+',"'+val.FechaAsignado+'","'+val.FechaAtencion+'","'+val.Coordenadas+'","'+val.IdEstado+'")';
		  tx.executeSql(sql);
	  });
  }
  
  function AgregaOrden(tx) {
	  $.each(ordenes, function( key, val ) {																																																																																																																																																																																																																																											
		  sql = 'INSERT INTO orden (IdOrden,IdDocumento,DownMovil,IdEstado,Created,TipoServicio,NombreSector,NombreCliente,DireccionCliente,Cartera,Alimentador,Sed,Medidor,Condicion,Exportado,Suministro,IdOrdenTrabajo,Empresa,IdUnidadNegocio,UnidadNegocio,IdCentroServicio,CentroServicio,Proveedor,Actividad,SubActividad,FechaGeneracion,Periodo,UsuarioGenero,Observacion,Solicitud,SuministroAnterior,SuministroPosterior,NombreSuministro,DireccionSuministro,UltimaFacturacion,IdCiclo,Ciclo,IdSector,Sector,IdRuta,Ruta,LCorrelati,Localidad,Zona,TipoZona,Potencia,Tarifa,TipoSuministro,Sistema,TipoConexion,SerieMedidor,MarcaMedidor,ModeloMedidor,DigitosMedidor,LecturaAnterior,Promedio,ConsumoAnterior,Responsable,Supervisor,Ejecutor, NroReclamo, NroAtencion, TipoNegocio) VALUES ('+val.IdOrden+', "'+val.IdDocumento+'", "'+val.DownMovil+'", "'+val.IdEstado+'", "'+val.Created+'", "'+val.TipoServicio+'", "'+val.NombreSector+'", "'+val.NombreCliente+'", "'+val.DireccionCliente+'", "'+val.Cartera+'", "'+val.Alimentador+'", "'+val.Sed+'", "'+val.Medidor+'", "'+val.Condicion+'", "'+val.Exportado+'", "'+val.Suministro+'", "'+val.IdOrdenTrabajo+'", "'+val.Empresa+'", "'+val.IdUnidadNegocio+'", "'+val.UnidadNegocio+'", "'+val.IdCentroServicio+'", "'+val.CentroServicio+'", "'+val.Proveedor+'", "'+val.Actividad+'", "'+val.SubActividad+'", "'+val.FechaGeneracion+'", "'+val.Periodo+'", "'+val.UsuarioGenero+'", "'+val.Observacion+'", "'+val.Solicitud+'", "'+val.SuministroAnterior+'", "'+val.SuministroPosterior+'", "'+val.NombreSuministro+'", "'+val.DireccionSuministro+'", "'+val.UltimaFacturacion+'", "'+val.IdCiclo+'", "'+val.Ciclo+'", "'+val.IdSector+'", "'+val.Sector+'", "'+val.IdRuta+'", "'+val.Ruta+'", "'+val.LCorrelati+'", "'+val.Localidad+'", "'+val.Zona+'", "'+val.TipoZona+'", "'+val.Potencia+'", "'+val.Tarifa+'", "'+val.TipoSuministro+'", "'+val.Sistema+'", "'+val.TipoConexion+'", "'+val.SerieMedidor+'", "'+val.MarcaMedidor+'", "'+val.ModeloMedidor+'", "'+val.DigitosMedidor+'", "'+val.LecturaAnterior+'", "'+val.Promedio+'", "'+val.ConsumoAnterior+'", "'+val.Responsable+'", "'+val.Supervisor+'", "'+val.Ejecutor+'",  "'+val.NroReclamo+'", "'+val.NroAtencion+'", "'+val.TipoNegocio+'")';
		  tx.executeSql(sql);
	  });
  }

  function AgregaTipointervencion(tx) {
	  tx.executeSql('DELETE FROM tipointervencion;');
	  $.each(tipointervenciones, function( key, val ) {
		  sql = 'INSERT INTO tipointervencion (IdTipoIntervencion, DetalleTipoIntervencion,  IdTipoIntervencionSup ) VALUES ('+val.IdTipoIntervencion+', "'+val.DetalleTipoIntervencion+'", "'+val.IdTipoIntervencionSup+'" )';
		  tx.executeSql(sql);
	  });
  }
  
  function AgregaMaestrointervencion(tx) {
	  $.each(maestrosintervencion, function( key, val ) {
		  sql = 'INSERT INTO MaestrosIntervencion (IdMaestrosIntervencion, IdentificadorMaestroIntervencion,  EtiquetaMaestroIntervencion, TipoMaestroIntervencion, LongitudTipoDatoMaestrosInterv, ControlObjetoMaestroIntervencion, EstiloObjetoMaestroIntervencion,PosicioMaestroIntervencion , RaizMaestroIntervencion) VALUES ('+val.IdMaestrosIntervencion+',"'+val.IdentificadorMaestroIntervenci+'", "'+val.EtiquetaMaestroIntervencion+'", "'+val.TipoDatoMaestroIntervencion+'",'+val.LongitudTipoDatoMaestrosInterv+',"'+val.ControlObjetoMaestroIntervencion+'","'+val.EstiloObjetoMaestroIntervencion+'",'+val.PosicionMaestroIntervencion+','+val.RaizMaestroIntervencion+')';
		  tx.executeSql(sql);
	  });
  }
  
  function AgregaTipomaestrointervencion(tx) {
	  tx.executeSql('DELETE FROM tipomaestrosintervencion;');
	  $.each(tiposmaestrosintervencion, function( key, val ) {
		  sql='INSERT INTO tipomaestrosintervencion (idtipointervencion, idmaestrosintervencion) VALUES ('+val.IdTipoIntervencion+','+val.IdMaestrosIntervencion+' )';
		  tx.executeSql(sql);
	  });
  }
  
  function AgregarDetalleMaestro(tx){
	for (var i = 0; i < dataJSON.length; i++) {
		var temp = dataJSON[i];
		if(temp['id']==undefined){
			sql='INSERT INTO detalleintervencion (idorden ,idmaestrosintervencion,detalleintervencion) VALUES ('+idorden+','+temp['id']+','+temp['value']+' )';
			tx.executeSql(sql);
		}
	}
  }
  
  function ObtenerUsuarios(tx) {
      tx.executeSql('SELECT * FROM usuario WHERE user = ? ', [$("#username").val()], validarUsuario, errorCB);
  }
  
  function CambiarEstadoOrden(tx) {
      tx.executeSql('UPDATE intervencion SET estado = ? , FechaAtencion = ?, Coordenadas = ? WHERE IdOrden = ? ', ['finalizado',d.getDate(),latitud+longitud,idorden]);
  }  
  
  function EliminarOrden(tx) {
      tx.executeSql('DELETE FROM orden WHERE IdOrden = ? ', [idorden]);
      tx.executeSql('DELETE FROM intervencion WHERE IdOrden = ? ', [idorden]);
  } 

  function validarUsuario(tx, results) {
    var len = results.rows.length;
    console.log("DEMO table: " + len + " rows found.");
    for (var i=0; i<len; i++){
      console.log("Row = " + i + " ID = " + results.rows.item(i).pass + " Data =  " + results.rows.item(i).apikey );
      if($("#username").val()==results.rows.item(i).user && $("#pass").val()==results.rows.item(i).pass){
    	userid = results.rows.item(i).userid;
    	user = results.rows.item(i).user;
    	pass = results.rows.item(i).pass;
    	apikey = results.rows.item(i).apikey;
    	localStorage.setItem("userid", userid);
    	localStorage.setItem("user", user);
    	localStorage.setItem("pass", pass);
    	localStorage.setItem("apikey", apikey);
    	alert('Logeado');
      	location.href='asignaciones.html';
      }else{
          	alert("Contrase\u00d1a incorrecta");
      }
    }
    
    if(len==0){
    	$.mobile.loading('show');
    	$.ajax({
    		 beforeSend: function(xhrObj){
    			 xhrObj.setRequestHeader("IMEI","'"+device.uuid+"'");
    		 },
    		 crossDomain: true,
    		 url: url+$("#username").val()+"/"+$("#pass").val(),
    		 type: 'GET',
    		 dataType: 'json',
    		 headers: { 'IMEI': device.uuid,'usuario':$("#username").val() }
    	})
    	.done(function( data ) {
    		var login = data.login
    		if(login.success){
    			userid = login.IdUsuario;
    			user = login.NomUsuario;
    			pass = $("#pass").val();
    			apikey = login.ApiKey;
    			localStorage.setItem("userid", login.IdUsuario);
    	    	localStorage.setItem("user", login.NomUsuario);
    	    	localStorage.setItem("pass", $("#pass").val());
    	    	localStorage.setItem("apikey", login.ApiKey);
    	    	db.transaction(AgregaUsuario, errorCB);
    	    	alert(login.message)
    	    
    			location.href='asignaciones.html';
    		}else{
    			$.mobile.loading('hide');
    			alert(login.message);
    		}
        })
        .fail(function(jqXHR, textStatus) {
        	$.mobile.loading('hide');
        	alert( "Web Services Caida" );
        	alert( "Request failed: " + textStatus )
        })
    }
  }
  
  function sincronizar() {
	   $.mobile.loading('show');
  	   $.ajax({
  		 beforeSend: function(xhrObj){
  			 xhrObj.setRequestHeader("apikey",apikey);
  		 },
  		 crossDomain: true,
  		 url: urldata,
  		 type: 'GET',
  		 dataType: 'json',
  		 headers: { 'apikey': apikey }
  	})
  	.done(function( data ) {
  		if(data.success){
  			alert(data.message);
  			intervenciones = data.datos.intervencion;
  			db.transaction(AgregaIntervencion, errorCB);
  			
  			ordenes = data.datos.orden;
  			db.transaction(AgregaOrden, errorCB);
  			
  			tipointervenciones = data.datos.tipointervencion;
  			db.transaction(AgregaTipointervencion, errorCB);
  			
  			maestrosintervencion = data.datos.maestrosintervencion;
  			db.transaction(AgregaMaestrointervencion, errorCB);
  			
  			tiposmaestrosintervencion = data.datos.tipomaestrosintervencion;
  			db.transaction(AgregaTipomaestrointervencion, errorCB);
  			
  			db.transaction(ObtenerSectores, errorCB);
  			$.mobile.loading('hide');
  			confirmar();
  			
  		}else{
  			$.mobile.loading('hide');
  			alert(data.message);
  		}
      })
      .fail(function() {
	       	$.mobile.loading('hide');
	       	alert( "Web Services Caida" );
      })
 }
  
  function confirmar(){
	  $.mobile.loading('show');
	  
	  var idintervenciones="";
	  var idordenes="";
	  var idtipointervenciones="";
	  var idmaestrosintervencion="";
	  var idtiposmaestrosintervencion="";
	  
	  $.each(ordenes, function( key, val ) {
		  idordenes=idordenes+val.IdOrden+'-';
	  });
	  $.each(intervenciones, function( key, val ) {
		  idintervenciones=idintervenciones+val.IdIntervencion+'-';
	  });
	  $.each(tipointervenciones, function( key, val ) {
		  idtipointervenciones=idtipointervenciones+val.IdTipoIntervencion+'-';
	  });
	  $.each(maestrosintervencion, function( key, val ) {
		  idmaestrosintervencion=idmaestrosintervencion+val.IdMaestrosIntervencion+'-';
	  });
	  $.each(tiposmaestrosintervencion, function( key, val ) {
		  idtiposmaestrosintervencion=idtiposmaestrosintervencion+val.IdTipoIntervencion+val.IdMaestrosIntervencion+'-';
	  });	  
	  
	  
	  $.ajax({
		 type: "GET",
		 url: urlconfirm+idordenes+'/'+idintervenciones+'/'+idtipointervenciones+'/'+idmaestrosintervencion+'/'+idtiposmaestrosintervencion,
		 headers: { 'apikey': apikey }
	  })
	  .done(function( data ) {
		  $.mobile.loading('hide');
		  alert(data.message);
	  })
      .fail(function() {
		 $.mobile.loading('hide');
		 alert( "Web Services Caida" );
	 })
  }

  
  
  function ObtenerSectores(tx){
      tx.executeSql('SELECT Sector FROM orden o, intervencion i WHERE o.IdOrden = i.IdOrden AND i.estado != "finalizado" GROUP BY Sector',[],AdjuntarSectores,errorCB);
  }
  
  function AdjuntarSectores(tx,results){
	  $('#grupos').empty();
      var len = results.rows.length;
      console.log("Verificar intervencion: " + len + " rows found.");
      for (var i=0; i<len; i++){
          var row = results.rows.item(i);
          tx.executeSql('SELECT * FROM orden o, intervencion i WHERE o.IdOrden = i.IdOrden AND o.Sector = ? ',[row['Sector']],querySuccess,errorCB);
      }
  }
  
  function ObtenerOrdenes(tx){
      tx.executeSql('SELECT * FROM orden o, intervencion i WHERE o.IdOrden = i.IdOrden AND i.id_usuario = ? AND o.Sector = ? ',[userid,],querySuccess,errorCB);
  }
  
  function querySuccess(tx,results){
		var len = results.rows.length;
	 	console.log("DEMO ordenes por sector: " + len + " rows found.");
	 	var sector;
		for (var i=0; i<len; i++){
	        var row = results.rows.item(i);
	        sector=row['Sector'];
	    }
		var n  = "<div data-role='collapsible'>";
				n += "<h3>"+sector+"</h3>";
		  		n += '<ul data-role="listview" data-inset="true" data-filter="true"  data-filter-placeholder="Filtra datos" >';
		  		for (var i=0; i<len; i++){
		  			 var row = results.rows.item(i);
		  			 if(row['estado']!='finalizado'){
			  			 n += "<li>";
			  			 	n += '<a href="" onclick="abrirformulario('+row['IdOrden']+',\''+row['Observacion']+'\'); return false;">';
			  			 		n += '<h3>'+row['Solicitud']+' | '+row['Sector']+'</h3>';
			  			 		n += '<p><strong>'+row['DireccionSuministro']+' - '+row['NombreSuministro']+'</strong></p>';
			  			 		n += '<p>'+row['Observacion']+'</p>';
			  			 		n += '<p class="ui-li-aside">'+row['FechaGeneracion']+'</p>';;
			  			 	n += '</a>'
			  			 n += "</li>";
		  			 }
		  		}
		  		n += "</ul>";
		  	  n += "</div>";

		$(n).appendTo('#grupos').trigger("create");
		$( "#grupos" ).collapsibleset( "refresh" );
	  }
  
  function ObtenerFinalizado(tx){
      tx.executeSql('SELECT * FROM orden o, intervencion i WHERE o.IdOrden = i.IdOrden AND i.estado = "finalizado"',[],adjuntarFinalizados,errorCB);
  }
  
  function adjuntarFinalizados(tx,results){
	var len = results.rows.length;
 	console.log("DEMO ordenes finalizadas: " + len + " rows found.");
 	var sector;
	var n  = "<div data-role='collapsible'>";
			n += '<h3> Asignaciones Finalizadas </h3>';
	  		n += '<ul data-role="listview" data-inset="true" data-filter="true"  data-filter-placeholder="Filtra datos" >';
	  		for (var i=0; i<len; i++){
	  			 var row = results.rows.item(i);
		  			 n += "<li>";
		  			 	n += '<a href="" onclick="abrirformulario('+row['IdOrden']+',\''+row['TipoServicio']+'\'); return false;">';
		  			 		n += '<h3>'+row['NroSolicitud']+' | '+row['Sector']+'</h3>';
		  			 		n += '<p><strong>'+row['DireccionCliente']+' - '+row['NombreCliente']+'</strong></p>';
		  			 		n += '<p>'+row['TipoServicio']+'</p>';
		  			 		n += '<p class="ui-li-aside">'+row['FechaGeneracion']+'</p>';;
		  			 	n += '</a>'
		  			 n += "</li>";
	  		}
	  		n += "</ul>";
	  	  n += "</div>";

	$(n).appendTo('#OrdenFin').trigger("create");
	$( "#OrdenFin" ).collapsibleset( "refresh" );
  }
  
  function ObtenerOrden(tx){
      tx.executeSql('SELECT * FROM orden WHERE IdOrden = ?',[idorden],cargarOrden,errorCB);
  }
  
  function verificarMulti(tx){
      tx.executeSql('SELECT * FROM MaestrosIntervencion WHERE tipomaestrointervencion =  ?',['multi'],asignarMulti,errorCB);
  }
  
  
  function ObtenerGrupos(tx){
     tx.executeSql('SELECT * FROM tipointervencion ti,tipomaestrosintervencion tmi,maestrosintervencion mi  WHERE ti.IdTipoIntervencion = tmi.idtipointervencion AND mi.idmaestrosintervencion = tmi.idmaestrosintervencion  AND  ti.DetalleTipointervencion = ? ORDER BY mi.PosicioMaestroIntervencion',[tipoorden],adjuntarGrupos,errorCB);
      //tx.executeSql('SELECT * FROM tipointervencion ti,tipomaestrosintervencion tmi WHERE ti.IdTipoIntervencion = tmi.idtipointervencion  AND  ti.DetalleTipointervencion = ? ',[tipoorden],adjuntarGrupos,errorCB);
  }
  
  function ObtenerContenido(tx){
      tx.executeSql('SELECT * FROM MaestrosIntervencion WHERE RaizMaestroIntervencion = ? ORDER BY PosicioMaestroIntervencion',[grupo],adjuntarContenido,errorCB);
  }
  
  function adjuntarGrupos(tx,results){
	  var len = results.rows.length;
	  console.log("Grupos : " + len + " rows found.");
      for (var i=0; i<len; i++){
          var row = results.rows.item(i);
          var n = '<div class="ui-bar ui-bar-a">';
	          	n = n + '<div id="'+row['IdMaestrosIntervencion']+'" data-role="collapsible" data-collapsed="true"  data-inset="false">';
	          		n = n + '<h3>';
	          					n = n + row['EtiquetaMaestroIntervencion'];
	          		n = n + '</h3>';
	          	n = n + '</div>';
          	n = n +	 '</div>';     
          $(n).appendTo("#contenedor").trigger("create");
          grupos.push(row['IdMaestrosIntervencion']);
      }
      db.transaction(generarContenido, errorCB);
  }
  
  function generarContenido(tx){
	  var len = grupos.length;
      for (var i=0; i<len; i++){
          grupo = grupos[i];
          tx.executeSql('SELECT * FROM MaestrosIntervencion WHERE RaizMaestroIntervencion = ? ORDER BY PosicioMaestroIntervencion',[grupo],adjuntarContenido,errorCB);
      }
  }
  
  function adjuntarContenido(tx,results){
	  var len = results.rows.length;
      for (var i=0; i<len; i++){
          var row = results.rows.item(i);
          var id = row['IdMaestrosIntervencion'];
          var value = orden[row['IdentificadorMaestroIntervencion']];
          var modoread = ( value == undefined ) ? '' : 'readonly';
          
          var value = ( value == undefined ) ? '' : value;
          if(id==45){
          	value=$('#9').val();
          	modoread='';
          }
          var firma = localStorage.getItem('firma') || '';
          var fotografia = localStorage.getItem(row['EtiquetaMaestroIntervencion']) || '';
          var n = '<div data-role="fieldcontain" data-inline="true" id="div'+id+'" class="ui-field-contain ui-body ui-br">';
          	n = n + '<label for="'+row['IdMaestrosIntervencion']+'">'+row['EtiquetaMaestroIntervencion']+':</label>';
          		
          		if(row['TipoMaestroIntervencion']=='step'){
      				n = n + '<div data-role="rangeslider" ><input name="range-10b" id="'+row['IdMaestrosIntervencion']+'" min="0" max="5" step="1" value="0" type="range"  /></div>';
      				n = n + '<input id="'+row['IdMaestrosIntervencion']+'_h" type="number" name="name" step="1,1" id="name-a" maxlength="'+row['LongitudTipoDatoMaestrosInterv']+'"  data-inline="true" style="width: 20px !important;" value="0" '+modoread+'>';
      				n = n + '<input id="'+row['IdMaestrosIntervencion']+'_d" type="number" name="name" id="name-a" maxlength="'+row['LongitudTipoDatoMaestrosInterv']+'"  data-inline="true" style="width: 20px !important;" value="0" '+modoread+'>';
      			}
      			
          		if(row['TipoMaestroIntervencion']=='numero'){
          			n = n + '<input id="'+row['IdMaestrosIntervencion']+'" type="number" name="name" id="name-a" maxlength="'+row['LongitudTipoDatoMaestrosInterv']+'" value="'+value+'" '+modoread+'>';
          		}
          		
          		if(row['TipoMaestroIntervencion']=='cadena' || value !=""){
          			n = n + '<input id="'+row['IdMaestrosIntervencion']+'" type="text" name="name"  id="name-a" maxlength="'+row['LongitudTipoDatoMaestrosInterv']+'" value="'+value+'" '+modoread+'>';
          		}
          		
          		if(row['TipoMaestroIntervencion']=='fotografia'){
          			n = n + '<a data-inline="true"  data-role="button" href="#" onclick="capturePhoto(\''+row['EtiquetaMaestroIntervencion']+'\');" id="tomarfoto" data-corners="true" data-shadow="true" data-iconshadow="true" data-wrapperels="span" class="ui-btn ui-shadow ui-btn-corner-all ui-btn-inline ui-btn-up-c"><span class="ui-btn-inner"><span class="ui-btn-text">';
                    n = n + '<img id="camaraicon" width="20px" height="16px" src="images/icono_camara.png"> Camara </span></span></a>';
                    n = n + '</div><div data-role="fieldcontain" class="ui-field-contain ui-body ui-br">'
                    n = n + '<label for="name-a">Vista Previa</label>';
                    n = n + '<img id="'+row['EtiquetaMaestroIntervencion']+'" title="'+row['IdMaestrosIntervencion']+'" src="'+fotografia+'" width="150px" height="100px">';
          		}
          		
          		if(row['TipoMaestroIntervencion']=='firma'){
          			n = n + '<a data-inline="true"  data-role="button" href="#" onclick="captureFirmac();" id="tomarfoto" data-corners="true" data-shadow="true" data-iconshadow="true" data-wrapperels="span" class="ui-btn ui-shadow ui-btn-corner-all ui-btn-inline ui-btn-up-c"><span class="ui-btn-inner"><span class="ui-btn-text">';
          			//n = n + '<a data-inline="true"  data-role="button" href="#firmac" id="tomarfoto" data-corners="true" data-shadow="true" data-iconshadow="true" data-wrapperels="span" class="ui-btn ui-shadow ui-btn-corner-all ui-btn-inline ui-btn-up-c"><span class="ui-btn-inner"><span class="ui-btn-text">';
          			n = n + 'Firma </span></span></a>';
          			n = n + '</div><div data-role="fieldcontain" class="ui-field-contain ui-body ui-br">'
                    n = n + '<label for="name-a">Vista Previa</label>';
                    n = n + '<img id="'+row['IdMaestrosIntervencion']+'" title="'+row['IdMaestrosIntervencion']+'" src="'+firma+'" width="150px" height="100px">';
          		}
          		
          		if(row['TipoMaestroIntervencion']=='multi' && value=="" ){          			          			
          			tx.executeSql('SELECT * FROM MaestrosIntervencion om, MaestrosIntervencion mm   WHERE mm.IdMaestrosIntervencion = om.RaizMaestroIntervencion  AND om.RaizMaestroIntervencion = ? ORDER BY PosicioMaestroIntervencion',[id],adjuntarMulti,errorCB);
          		}
          		
				if(row['TipoMaestroIntervencion']=='multimaestro' && value=="" ){          			          			
          			tx.executeSql('SELECT * FROM MaestrosIntervencion om, MaestrosIntervencion mm   WHERE mm.IdMaestrosIntervencion = om.RaizMaestroIntervencion  AND om.RaizMaestroIntervencion = ? ORDER BY PosicioMaestroIntervencion',[id],adjuntarMultiMaestro,errorCB);
          		}
          		
          n = n + '</div>';
          $(n).appendTo('#'+row['RaizMaestroIntervencion']+' .ui-collapsible-content').trigger("create");
          
      }
      
      $("#fomulario").find(':input').each(function() {
          var elemento= this;
          if(elemento.value!=''){
        	  localStorage.setItem($(this).attr("id"), elemento.value);  
          }
		});
      
      $("#fomulario").find(':input').each(function() {
          var elemento= this;
          var temp = new Object();
          valor = localStorage.getItem($(this).attr("id")) || '';
          elemento.value = valor;
	   });
      
  }
  
  function adjuntarMulti(tx,results){
	  
	  var len = results.rows.length;
	  
	  console.log("Maestro table: " + len + " rows found.");
	  
	  var id;
	  
	  for (var i=0; i<len; i++){
          var row = results.rows.item(i);
          id=row['RaizMaestroIntervencion'];
      }
	  var n = '<div class="ui-select"><select onchange="validacion(this.value,id)" id="'+id+'" data-native-menu="false" tabindex="-1" >';
      
	  for (var i=0; i<len; i++){
          var row = results.rows.item(i);
          n = n + '<option value="'+row['IdMaestrosIntervencion']+'">'+row['EtiquetaMaestroIntervencion']+'</option>';
      }
      
      n=n+'</select></div>';
      
      $(n).appendTo('#div'+row['RaizMaestroIntervencion']).trigger("create");
      
  }
  
function validacion(value,id){	
	console.log(value +" , "+id);
	if(id==42){
		if(value==43){
			$('#45').val($('#9').val());
		}else{
			$('#45').val('');
		}
	}		
}
  
 function adjuntarMultiMaestro(tx,results){
	  
	  var len = results.rows.length;
	  
	  console.log("Maestro table: " + len + " rows found.");
	  
	  var id;	  
	  
	  for (var i=0; i<len; i++){
          var row = results.rows.item(0);
          id=row['RaizMaestroIntervencion'];
          ido=row['IdMaestrosIntervencion'];
      }
	  
	  var n = '<div class="ui-select"><select onchange="rohan(this.value)" id="'+id+'" data-native-menu="false" tabindex="-1" >';
      
	  for (var i=0; i<len; i++){
          var row = results.rows.item(i);
          n = n + '<option value="'+row['IdMaestrosIntervencion']+'">'+row['EtiquetaMaestroIntervencion']+'</option>';
      }
      
      n=n+'</select></div>';
      
      n=n+"<div id='obsmulti'></div>";
      
      $(n).appendTo('#div'+row['RaizMaestroIntervencion']).trigger("create");
      
      console.log("ido: " + ido);
      tx.executeSql('SELECT * FROM MaestrosIntervencion WHERE RaizMaestroIntervencion = ? ORDER BY PosicioMaestroIntervencion',[ido],adjuntarElementosObservacion,errorCB);
  }
  
  function rohan(id){
	ido=id;
  	db.transaction(GenerarObservacion, errorCB);
  }

function GenerarObservacion(tx){
	console.log("ido: " + ido);
	tx.executeSql('SELECT * FROM MaestrosIntervencion WHERE RaizMaestroIntervencion ='+ido+' ORDER BY PosicioMaestroIntervencion',[],adjuntarElementosObservacion,errorCB);
}

  
  
function adjuntarElementosObservacion(tx,results){
	$('#obsmulti').empty();
	var len = results.rows.length;	  
	console.log("Elementos observacion table: " + len + " rows found.");
	
	for (var i=0; i<len; i++){
        var row = results.rows.item(i);
        var id = row['IdMaestrosIntervencion'];
        var value = orden[row['IdentificadorMaestroIntervencion']];
        var modoread = ( value == undefined ) ? '' : 'readonly';
        var value = ( value == undefined ) ? '' : value;
		var firma = localStorage.getItem('firma') || '';
        var fotografia = localStorage.getItem(row['EtiquetaMaestroIntervencion']) || '';
		var n = '<div data-role="fieldcontain" id="div'+id+'" class="ui-field-contain ui-body ui-br">';
			console.log(row['TipoMaestroIntervencion']);
			if(row['TipoMaestroIntervencion']=='areatexto'){
      				n = n + '<textarea id="'+row['IdMaestrosIntervencion']+'" class="ui-input-text ui-body-c ui-corner-all ui-shadow-inset" name="textarea-1" rows="8" cols="40">'+row['EtiquetaMaestroIntervencion']+'</textarea>';
      			}
          		
          		if(row['TipoMaestroIntervencion']=='step'){
          			n = n + '<label for="'+row['IdMaestrosIntervencion']+'">'+row['EtiquetaMaestroIntervencion']+':</label>';
      				n = n + '<div data-role="rangeslider" ><input name="range-10b" id="'+row['IdMaestrosIntervencion']+'" min="0" max="5" step="1" value="0" type="range" /></div>';          		
      			}
          		if(row['TipoMaestroIntervencion']=='numero'){
          			n = n + '<label  for="'+row['IdMaestrosIntervencion']+'">'+row['EtiquetaMaestroIntervencion']+':</label>';
          			n = n + '<input id="'+row['IdMaestrosIntervencion']+'" type="number" name="name"  id="name-a" maxlength="'+row['LongitudTipoDatoMaestrosInterv']+'" value="'+value+'" '+modoread+'>';;          		
          		}
          		if(row['TipoMaestroIntervencion']=='cadena'){
          			n = n + '<label for="'+row['IdMaestrosIntervencion']+'">'+row['EtiquetaMaestroIntervencion']+':</label>';
          			n = n + '<input id="'+row['IdMaestrosIntervencion']+'" type="text" name="name"  id="name-a" maxlength="'+row['LongitudTipoDatoMaestrosInterv']+'" value="'+value+'" '+modoread+'>';
          		}
          		if(row['TipoMaestroIntervencion']=='fotografia'){
          			n = n + '<label for="'+row['IdMaestrosIntervencion']+'">'+row['EtiquetaMaestroIntervencion']+':</label>';
          			n = n + '<a data-inline="true"  data-role="button" href="#" onclick="capturePhoto(\''+row['EtiquetaMaestroIntervencion']+'\');" id="tomarfoto" data-corners="true" data-shadow="true" data-iconshadow="true" data-wrapperels="span" class="ui-btn ui-shadow ui-btn-corner-all ui-btn-inline ui-btn-up-c"><span class="ui-btn-inner"><span class="ui-btn-text">';
                    n = n + '<img id="camaraicon" width="20px" height="16px" src="images/icono_camara.png"> Camara </span></span></a>';
                    n = n + '</div><div data-role="fieldcontain" class="ui-field-contain ui-body ui-br">'
                    n = n + '<label for="name-a">Vista Previa</label>';
                    n = n + '<img id="'+row['EtiquetaMaestroIntervencion']+'" title="'+row['IdMaestrosIntervencion']+'" src="'+fotografia+'" width="150px" height="100px">';
          		}
          		if(row['TipoMaestroIntervencion']=='firma'){
          			n = n + '<label for="'+row['IdMaestrosIntervencion']+'">'+row['EtiquetaMaestroIntervencion']+':</label>';
          			n = n + '<a data-inline="true"  data-role="button" href="#" onclick="captureFirmac();" id="tomarfoto" data-corners="true" data-shadow="true" data-iconshadow="true" data-wrapperels="span" class="ui-btn ui-shadow ui-btn-corner-all ui-btn-inline ui-btn-up-c"><span class="ui-btn-inner"><span class="ui-btn-text">';
          			//n = n + '<a data-inline="true"  data-role="button" href="#firmac" id="tomarfoto" data-corners="true" data-shadow="true" data-iconshadow="true" data-wrapperels="span" class="ui-btn ui-shadow ui-btn-corner-all ui-btn-inline ui-btn-up-c"><span class="ui-btn-inner"><span class="ui-btn-text">';
          			n = n + 'Firma </span></span></a>';
          			n = n + '</div><div data-role="fieldcontain" class="ui-field-contain ui-body ui-br">'
                    n = n + '<label for="name-a">Vista Previa</label>';
                    n = n + '<img id="'+row['IdMaestrosIntervencion']+'" title="'+row['IdMaestrosIntervencion']+'" src="'+firma+'" width="150px" height="100px">';
          		}
          		if(row['TipoMaestroIntervencion']=='multi' ){
          			n = n + '<label for="'+row['IdMaestrosIntervencion']+'">'+row['EtiquetaMaestroIntervencion']+':</label>';          			          			
          			tx.executeSql('SELECT * FROM MaestrosIntervencion WHERE RaizMaestroIntervencion = ? ORDER BY PosicioMaestroIntervencion',[id],adjuntarMulti,errorCB);
          		}
				if(row['TipoMaestroIntervencion']=='multimaestro' ){
					n = n + '<label  for="'+row['IdMaestrosIntervencion']+'">'+row['EtiquetaMaestroIntervencion']+':</label>';          			          			
          			tx.executeSql('SELECT * FROM MaestrosIntervencion  WHERE RaizMaestroIntervencion = ? ORDER BY PosicioMaestroIntervencion',[id],adjuntarMultiMaestro,errorCB);
          		}
          n = n + '</div>';
          
          $(n).appendTo('#obsmulti').trigger("create");
		}  
	}
  
  function cargarOrden(tx,results){
      
	  var len = results.rows.length;
      console.log("Orden Seleccionada: " + len + " rows found.");
      
      for (var i=0; i<len; i++){
    	  orden = results.rows.item(i);
    	  $('#nsolicitud').val(orden['Solicitud']);
      }
  
  }
    
  function captureFirmac() {
	  
	$("#fomulario").find(':input').each(function() {
    	var elemento= this;
        localStorage.setItem($(this).attr("id"), elemento.value);
	});
	  
	location.href='dibujo.html';
  }
  