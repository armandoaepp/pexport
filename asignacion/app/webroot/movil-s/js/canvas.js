$(function(){
  var drawing = new DrawingApp();
});

var DrawingApp = function(options) {
	
	var canvas = document.getElementById("firma"),
	ctxt = canvas.getContext("2d"),
	guardar = document.getElementById("guardar");
	guardar.addEventListener("click",function(){	
			img = canvas.toDataURL("image/jpg")
			localStorage.setItem("firma", img);
			navigator.app.backHistory();
	},false);
	borrar = document.getElementById("borrar");
	borrar.addEventListener("click",function(){	
		lines = [];
		ctxt.clearRect(drawingAreaX, drawingAreaY, drawingAreaWidth, drawingAreaHeight);
		self.drawRect();
	},false);
	borrar = document.getElementById("cancelar");
	borrar.addEventListener("click",function(){	
		navigator.app.backHistory();
	},false);
	curColor = "black",
	curSize = 2,
	lines = [,,],
	offset = $(canvas).offset(),
    colorsShowing = false,
    selColor = curColor;
    adWidth = 50;

    canvas.width = window.innerWidth;
	canvas.height = window.innerHeight-80;

    //if the ads aren't there then don't fix the layout
	

    //setup the canvas context
	ctxt.lineWidth = 5;
	ctxt.lineCap = "round";
	ctxt.pX = undefined;
	ctxt.pY = undefined;

 //setup the drawing area
     var drawingAreaX = 1,
     drawingAreaY = 1,
  	 drawingAreaWidth = canvas.width - 1,
  	 drawingAreaHeight = canvas.height;

    var self = {
        //bind click events
        init: function() {
            canvas.addEventListener('touchstart', self.preDraw, false);
            canvas.addEventListener('touchmove', self.draw, false);
        },
        //draws the rectangle
        drawRect: function() {
        	ctxt.clearRect(0, 0, canvas.width, canvas.height)
        	ctxt.beginPath();
        	ctxt.rect(drawingAreaX, drawingAreaY, drawingAreaWidth, drawingAreaHeight);
        	ctxt.closePath();
        	ctxt.lineWidth = "1";
        	ctxt.strokeStyle = "black";
        	ctxt.fillStyle = "white";
        	ctxt.fill();
        	ctxt.strokeRect(drawingAreaX, drawingAreaY, drawingAreaWidth, drawingAreaHeight);
		  },
       
       
  //this is the single press event
        preDraw: function(event) {
            $.each(event.touches, function(i, touch) {
            var x = this.pageX - offset.left,
            y = this.pageY - offset.top,
            id = touch.identifier;

                //if it's in the drawing area then record the draw
            if (y < drawingAreaHeight && x < drawingAreaWidth) {
                 lines[id] = { x     : x,
                               y     : y,
                               color : curColor,
                               size  : curSize
                            };
                 //force a dot to be drawn
                 self.move(id, 1, 0);
            } else {
              //this checks if the cusor size needs to be changed
            	var redraw = false;
            	for (i = 0; i < cursor.length; i++) {
            		if (x > cursor[i].location-25 && x < cursor[i].location+15) {
            			curSize = cursor[i].radius;
            			redraw = true;
            		}
            	}
            	if (redraw) self.drawCursors();
            	if (x > trashX && x < trashX+20) {
            		var ck = confirm("Are you sure you want to clear your drawing?");
            		if (ck == true) {
            			lines = [];
            			ctxt.clearRect(drawingAreaX, drawingAreaY, drawingAreaWidth+10, drawingAreaHeight+10);
            			self.drawRect();
            		}
            	}
             }
        });
   //prevent scrolling
            event.preventDefault();
        },
        //when the screen is touched this where the drawing happens
        draw: function(event) {
            $.each(event.touches, function(i, touch) {
                var x = this.pageX - offset.left,
                y = this.pageY - offset.top;
                //only store the keystrokes if they are in the drawing area
                if (y < drawingAreaHeight && x < drawingAreaWidth) {
                	var id = touch.identifier,
                    moveX = x - lines[id].x,
                    moveY = y - lines[id].y,
                    ret = self.move(id, moveX, moveY);
                	lines[id].x = ret.x;
                	lines[id].y = ret.y;
                }
            });
            //prevents scrolling
            event.preventDefault();
        },
        //this is where the actual drawing on the screen happens
        move: function(i, changeX, changeY) {
            ctxt.strokeStyle = lines[i].color;
            ctxt.lineWidth = lines[i].size;
            ctxt.beginPath();
            ctxt.moveTo(lines[i].x, lines[i].y);
            ctxt.lineTo(lines[i].x + changeX, lines[i].y + changeY);
            ctxt.stroke();
            ctxt.closePath();
            return { x: lines[i].x + changeX, y: lines[i].y + changeY };
        }
    };

	 self.drawRect();

	 

	 return self.init();
};