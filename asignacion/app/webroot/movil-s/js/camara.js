 var div;
 
 function capturePhoto(pdiv) {
	  div = pdiv;
      navigator.camera.getPicture(onPhotoDataSuccess, onFail, { quality: 14, correctOrientation: true,
          destinationType: Camera.DestinationType.DATA_URL,
          allowEdit: true });
 }

 function onPhotoDataSuccess(imageData) {
    console.log(imageData);
    var smallImage = document.getElementById(div);
    smallImage.src = "data:image/jpeg;base64," + imageData;
    localStorage.setItem(div, smallImage.src);
 }

 function onPhotoURISuccess(imageURI) {

    var largeImage = document.getElementById('fotocliente');
    largeImage.style.display = 'block';
    largeImage.src = imageURI;
  }

   function onPhotoFileSuccess(imageData) {
      alert("alertaaaaaaa ---" + JSON.stringify(imageData));
      var smallImage = document.getElementById('fotocliente');
      smallImage.style.display = 'block';
      smallImage.src = imageData;
  }


  function getPhoto(source) {
    // Retrieve image file location from specified source
    navigator.camera.getPicture(onPhotoURISuccess, onFail, { quality: 50,
    destinationType: destinationType.FILE_URI,
    sourceType: source });
  }


  function capturePhotoWithFile() {
      navigator.camera.getPicture(onPhotoFileSuccess, onFail, { quality: 50, destinationType: Camera.DestinationType.FILE_URI });
  }
  
  function onFail(message) {
    alert('Failed because: ' + message);
  }