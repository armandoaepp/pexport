
define(['jquery.datatables','fancyapps','loadmask'],function(){
	if(window.Ordenlecturas){ //Prevents defining the module twice
		return false;
	}
	console.log('INIT: Ordenlecturas.js');
	var Ordenlecturas = {

		init: function(){			
			this.initViewController();
			this.bindEvents();
			this.listarXml();
			this.listarAsignacion();
		},
		
		bindEvents: function(){
			$body = $('body');
		},
		
		initViewController: function(){			
			/*Ordenlecturas.listarOrdenLecturas();*/
			Ordenlecturas.buscarPorCicloSector();			
			Ordenlecturas.ajaxModalLibro();
			Ordenlecturas.asignarLecturistaLibro();
            Ordenlecturas.asignarLecturistaLibroRelecturas();
			Ordenlecturas.digitarLecturas();
			Ordenlecturas.tableColorDigitar();
			Ordenlecturas.tabIndex();
			Ordenlecturas.arbolTopMenu();
			Ordenlecturas.modalAsignar();
			Ordenlecturas.buscarPorCicloSectorReLecturas();
            Ordenlecturas.mostrarLecturistaPorLibro();
            Ordenlecturas.liberarDescargaLibro();
            Ordenlecturas.listarDigitarLecturas();
            Ordenlecturas.imprimirLibroLecturas();
            Ordenlecturas.listarPersonalParaLecturas();
		},
		
		imprimirLibroLecturas: function(){
			
			$body = $('body');
			$body.off('click','#imprimir-libros-lectura');
			$body.on('click','#imprimir-libros-lectura',function(){	
				var libro = $(this).attr('libro');
				var ruta = $(this).attr('ruta');
				 	event.stopPropagation();          		
				window.open(ENV_WEBROOT_FULL_URL+'ComlecOrdenlecturas/pdf_libros/'+libro+'/'+ruta,'_blank');
			});	
		},
		
		arbolTopMenu: function(){
			$('body').on('click','.arbol-top-lecturas',function(e){
					$('#arbol').toggle();
			});
		},
		ajaxModalLibro: function(){
			$('body').on('click','#listar_create_ruta',function(e){
				var ciclo = $(this).attr('ciclo');
				var sector = $(this).attr('sector');
				var nombruta = $(this).attr('nombruta');
				$.ajax({
					url : ENV_WEBROOT_FULL_URL+'ComlecOrdenlecturas/listacreatelibro',
					type: "POST",
					data : {ciclo: ciclo, sector: sector ,ruta: nombruta},
					success:function(data, textStatus, jqXHR){
						//$("#loadingBuscarPorCicloSector").hide();
						$('#modal_create_libro').html(data);
						Ordenlecturas.agregarDataTableLibros();
					}
				});	
			});
		},
		
		tabIndex: function(){
			$(document).on("keypress", ".TabOnEnter" , function(e){
                            
                        if( e.keyCode ==  13 ){

                        	tipolecturaid = $("#id_tipolectura").val();

			        		if(tipolecturaid == ''){
			        			tipolecturaid = 0;
			        		}
                    
                            cb = parseInt($(this).attr('tabindex'));

                            if ($(':input[tabindex=\'' + (cb + 1) + '\']') != null) {
                                $(':input[tabindex=\'' + (cb + 1) + '\']').focus();
                                $(':input[tabindex=\'' + (cb + 1) + '\']').select();
                            }

							if((this.tabIndex)%2==0){

		                        var num = this.tabIndex;

		                        var item = $('[tabindex="' +num+ '"]').data('index');

		                        lectura = $('#'+item+'lectura').val();
		                        codigoobservacion = $('#'+item+'codigoobservacion').val();
		                        codigo = $('#'+item+'codigo').html();

		                        if(lectura == '' && codigoobservacion == ''){
		                        	 alert('Ingrese una Lectura o Codigo de Observacion');
		                        	 $('#'+item+'lectura').focus();
		                        	 return false;
		                        }

								$.ajax({
									url : ENV_WEBROOT_FULL_URL+'ComlecOrdenlecturas/saveregistarlecturista',
									dataType: 'json',
									type: "POST",
									data : { tipolectura:tipolecturaid,lectura: lectura, codigo: codigo, codigoobservacion: codigoobservacion },
									success:function(data, textStatus, jqXHR){
 
                                      	var montoconsumo=data.montoconsumo;
                                      	var resultadoevaluacion=data.resultadoevaluacion;
                                      	var validacionlectura=data.validacionlectura;
                                      	var validacioncodigo=data.validacioncodigo;
                                      	var etiquetacodigo=data.etiquetacodigo;
                                  
                                      	var mensaje=data.mensaje;
                                      	var save=data.save;
                                             
                                      	$("#"+item+"etiquetacodigo").html('');
                                      	if (validacionlectura==true){
	                                        alert(mensaje);  
	                                        $("#"+item+"lectura").css('background','red');
	                                        $("#"+item+"lectura").css('color','#fff');
	                                        $("#"+item+"etiquetalectura").html(mensaje);
                                      	}
                                        if (validacioncodigo==true){
                                            alert(mensaje);  
                                            $("#"+item+"codigoobservacion").css('background','red');
                                            $("#"+item+"codigoobservacion").css('color','#fff');
                                            $("#"+item+"etiquetacodigo").html(etiquetacodigo);
                                            $('#'+item+'codigoobservacion').val('');
                                            $('#'+item+'codigoobservacion').focus();
                                            
                                       	}
                                      	if (validacionlectura==false && validacioncodigo==false){
	                                        $("#"+item+"lectura").css('background','#666');  
	                                        $("#"+item+"lectura").css('color','#fff');
	                                        $("#"+item+"codigoobservacion").css('background','#666');
	                                        $("#"+item+"codigoobservacion").css('color','#fff');
	                                        $("#"+item+"etiquetalectura").html('');
	                                        $("#"+item+"etiquetacodigo").html(etiquetacodigo);
	                                        $("#"+item+"montoconsumo").html(montoconsumo);

	                                        if(resultadoevaluacion == 'CONSISTENTE'){
		                                        var imagen = "<img title= 'CONSISTENTE' src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAB8ElEQVR42mNgoDb4/OfD/9UPpv6vv5z4v+C83/+sM17/c0+F/595o/f/u+/v/uPVfOHrgf8114L+L33f+H/j997/s98V/O95Hvu/5UHI/5wzLv+dtuv+3/Vy23+cmuvvRvw/9H/h/6U/qv53fgj93/DM93/VA/f/Jbcd/xdctfmffcH8v+kaqf/bnqMZAnJ22RW///v+zP0/52f+/76vUXADah97gQ3IvWz1P/OcKRgrLZNG9c6K+1P+z3lZDrcZhFtfB4E1V9xz/V980wFsQNoZ4/9xJ/T/W62T+d9+oh1hQOm5iP8bPvX9b30ZCMfozgfZnAjUHHlM+7/HdpX/NkusEQYknHACuwCkAWQrzGYQBmkGYZjtQYc0/1tvlv0vN1kSYUDkYdv/Ex6lgG1DxjDNyLb77FcDB6T0BAmEATGHvP933ouCa4BhUKiDMLJmj70q/7Xmi/63mm2JMKD7Yis4nmGhjIzjoJoDD2qANYNsl53C+79lbwvCAFCUGG5QBfsTZBsMgzSC/AyzGeR3ldlC/yXbRDFT5bon68DxC9OErNFpjyLYZpBmgWbO/+uurcOeGtc8WPNfbr44OJ6dtyuCMUij1kJRsLPFW0Vwa4aBF19f/K/eW/3faLbBf9FuATA2mqz/v2ZrzX+QHNVzLwAjy7LsIPIjLQAAAABJRU5ErkJggg=='>"	                                        
	                                        }
	                                        
	                                        if(resultadoevaluacion == 'INCONSISTENTE'){
	                                        	var imagen = "<img title='INCONSISTENTE' src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAABrElEQVR42q2TyytEURzH/QFW1JRHIxsTUiNWHgtCFOWRPEpZyAJZkKSZhccgsVBIFqRkZdKUZCFFNLEQJUnJQk0eGcQCk+bj/O6dyZ3rsXLr272dcz7f3/d3zrlRUf/98PIIK1Mw0gzOKnCU6+/FUYJPD/wNH23DcB14F+DcA7sz4B5ScK8+XmuDvXV+h6da4GkfzpZhywWePljqhLlWmGiEwWoCFdbvJlpsNcm1MjmZVwnGfzaQdpT8RdbIdrSe16fhcFEHRWsOtRfdsNCuJ5MWxKCrgMeadPzTowYDp5o8dutQWAKb4tNTqhm8NmdxWZVnMFCDbM3ogFECTzZFVKctl7eGDE6zEwwGalDbbQHCkthSOQyHqstaaeHYHmcw6CjSj0oAoyS2CaY1i6vCJI7Kcr4MgrMD+mK5MEYJaIKlusS/GHMZDNSRvFWmfFUzgyE4UJ/GZW48ezYLAf+96S5srvJckhgJhUCRFBDYmxyNf83z820Mbrjx5cVpMeWoZLfl26cujsTeSbH8Doefj7sbfCMOToozOUyN5cAWw0G+nbN+J+93t/z73/sJB/zttLkU5gUAAAAASUVORK5CYII='>"	                                        
	                                        }
	                                        
	                                        $("#"+item+"resultadoevaluacion").html(imagen);

	                                        if(resultadoevaluacion == 'INCONSISTENTE'){	                                        	

	                                             $("#"+item+"codigoobservacion").css('background','#F52D2D');
	                                             $("#"+item+"codigoobservacion").css('color','#fff');
	                                             $("#"+item+"lectura").css('background','#F52D2D'); 

	                                        }
                                      	}
							}
						});	
					 }	
				}   
			});
		},
		
		asignarLecturistaLibro: function(){
			$('body').on('click','#asignar_lecturista_libro',function(e){
				
				$('#asignar_lecturista_libro').removeClass("btn-primary");
				$('#asignar_lecturista_libro').addClass("btn-success");
				$('#asignar_lecturista_libro').html('Asignado Lectura...');	
				
				
				var desde = $('#desde').val();
				var hasta = $('#hasta').val();
				var lecturista = $('#lecturista').val();
				var ciclo = $('#ciclo').val();
				var sector = $('#sector').val();
				var ruta = $('#ruta').val();
				var negocio = $('#negocio').val();
				var fecha = $('#dpt_date_end').val();
				
				$.ajax({
					url : ENV_WEBROOT_FULL_URL+'ComlecOrdenlecturas/guardarlecturistalibro',
					type: "POST",
					data : {desde: desde, hasta: hasta ,lecturista: lecturista, ciclo: ciclo, sector:sector, ruta: ruta, negocio: negocio, fecha: fecha },
					success:function(data, textStatus, jqXHR){
						var newdesde = parseInt($('#hasta').val()) + parseInt(1);
						$('#desde').val(newdesde);
						$('#hasta').val("");
						$('#hasta').focus();
						$('#total-lecturistas').val("");
						
						$('#asignar_lecturista_libro').removeClass("btn-success");
						$('#asignar_lecturista_libro').addClass("btn-primary");
						$('#asignar_lecturista_libro').html('Grabar');	
						

						//$("#loadingBuscarPorCicloSector").hide();
						//$('#modal_create_libro').html(data);
						//Ordenlecturas.agregarDataTableLibros();
					}
				});					
			});	
		},
        asignarLecturistaLibroRelecturas: function(){
			$('body').on('click','#asignar_lecturista_libro_relecturas',function(e){
				var desde = $('#desde').val();
				var hasta = $('#hasta').val();
				var lecturista = $('#lecturista').val();
				var ciclo = $('#ciclo').val();
				var sector = $('#sector').val();
				var ruta = $('#ruta').val();
				var negocio = $('#negocio').val();
				
				$.ajax({
					url : ENV_WEBROOT_FULL_URL+'ComlecOrdenlecturas/guardarlecturistalibrorelecturas',
					type: "POST",
					data : {desde: desde, hasta: hasta ,lecturista: lecturista, ciclo: ciclo, sector:sector, ruta: ruta, negocio: negocio },
					success:function(data, textStatus, jqXHR){
						//$("#loadingBuscarPorCicloSector").hide();
						//$('#modal_create_libro').html(data);
						//Ordenlecturas.agregarDataTableLibros();
					}
				});					
			});	
		},
		listarOrdenLecturas: function(){
	      /*   $.ajax({
	             url : ENV_WEBROOT_FULL_URL+'ComlecOrdenlecturas/listarordenlecturasintro',
	             type: "POST",
	            // data : $(form).serialize(),
	             success:function(data, textStatus, jqXHR){
	                 $("#loadingBuscar").hide();
	                 $('#grid').html(data);
	                 Ordenlecturas.agregarDataTable();
	             }
             });	*/         
			//$('#grid').load(ENV_WEBROOT_FULL_URL+'ComlecOrdenlecturas/listarordenlecturasintro',function(){				
			//	$("#loadingBuscar").hide();
			//	Ordenlecturas.agregarDataTable();
			//});
			$(document).ready(function(){
				Ordenlecturas.agregarDataTable();
				$("#loadingBuscar").hide();
			});			
		},
		
		buscarPorCicloSector: function(){
			//$body.on('click','#orden_lectura',function(e){
			Ordenlecturas.tableColor();
			$("#grid #listar_asignacion tbody tr").click(function(){
				var id_tipolectura = $('#id_tipolectura').val();
				var idciclo = $(this).attr('idciclo');
				var idsector = $(this).attr('idsector');
				$('#accordion-tab1').click();
				$('.tabla_buscar_ciclo_sector_loading').show();
				//$("html, body").animate({ scrollTop: $('#tabla_buscar_ciclo_sector').offset().top }, 600);
				
				$.ajax({
					url : ENV_WEBROOT_FULL_URL+'ComlecOrdenlecturas/buscarciclosector',
					type: "POST",
					data : { tipo: id_tipolectura, ciclo: idciclo, sector: idsector },
					success:function(data, textStatus, jqXHR){                                          
						$('.tabla_buscar_ciclo_sector_loading').hide();
						$('#tabla_buscar_ciclo_sector').html(data);
						Ordenlecturas.agregarDataTableCicloSector();
						//Open detalle
						$('#ciclosector tbody td img').click();
					}
				});	
			});
			
		},
		
		buscarPorCicloSectorReLecturas: function(){
			//$body.on('click','#orden_lectura',function(e){
			Ordenlecturas.tableColorReLectura();
			$("#re-lectura tbody tr").click(function(){
			var idciclo = $(this).attr('idciclo');
			var idsector = $(this).attr('idsector');
				$.ajax({
					url : ENV_WEBROOT_FULL_URL+'ComlecOrdenlecturas/buscarciclosectorrelectura',
					type: "POST",
					data : { ciclo: idciclo, sector: idsector },
					success:function(data, textStatus, jqXHR){                                          
						$("#loadingBuscarPorCicloSector").hide();
						$('#tabla_buscar_ciclo_sector_re_lectura').html(data);
						Ordenlecturas.agregarDataTableCicloSectorRelecturas();
					}
				});	
			});
			
		},
                
                
                listarXml: function(){
                	
                	$('.fancybox').fancybox({width: 880,autoCenter: true});
                	
                      $('#contenido_xml').html('');
  
                      //$("#contenido_xml").append('<table id="tabla_xml" class="table table-bordered  " style="background-color:#fff;color:#000;font-weight: bold; "    ><thead><tr><th width="20" >Id</th><th width="30">Pfactura</th><th width="50">Sectores</th><th width="50">Descripcion</th><th>Fecha Importacion</th><th>Fecha Exportacion</th><th width="10">#Entrada</th><th width="30">#Salida</th><th width="10">#Procesadas</th><th width="10">#X_Procesar</th><th width="10">#Inconsistentes No Procesadas</th><th width="10">Exportar</th>  </tr>                              </thead>                                         </table>');
                      $("#contenido_xml").append('<table id="tabla_xml" class="table table-bordered tableWithFloatingHeader" style=""><thead><tr><th>Id</th><th>Pfactura</th><th>Sectores</th><th >Descripcion</th><th>Fecha Importacion</th><th>Fecha Exportacion</th><th >#Entrada</th><th >#Salida</th><th>Exportar</th></tr></thead></table>');
                      var oTable = $('#tabla_xml').dataTable( {

                      "sAjaxSource": ENV_WEBROOT_FULL_URL+"/ComlecOrdenlecturas/listarXml",
                      "fnServerData": function ( sSource, aoData, fnCallback ) {
                           //     aoData.push( { "name": "ciclo", "value": ciclo } );
                            //    aoData.push( { "name": "sector", "value": sector } );
                             //   aoData.push( { "name": "ruta", "value": ruta } );
                                $.ajax( {
                                    "dataType": 'json',
                                    "type": "POST",
                                    "url": sSource,
                                    "data": aoData,
                                    "success": fnCallback
                                } );
                            }
                         });
                      
                      var preload_data = [
                      	{ id: 'F', text: 'Fecha de Importación'}
                      	, { id: 'S', text: 'Sector'}
                          ];
                                             
                      $('#cbo_criterio').select2({
                          multiple: true
                          ,query: function (query){
                              var data = {results: []};
                     
                              $.each(preload_data, function(){
                                  if(query.term.length == 0 || this.text.toUpperCase().indexOf(query.term.toUpperCase()) >= 0 ){
                                      data.results.push({id: this.id, text: this.text });
                                  }
                              });
                     
                              query.callback(data);
                          }
                      });
                      $('#cbo_criterio').select2('data', [{ id: 'F', text: 'Fecha de Importación'}] )
                      
                      $('#cbo_criterio').on("change",function (e) { 
          				console.log("change "+e.val);
          				if(e.val.indexOf("F")>-1){
          					$('.por_fecha_asignacion').show();					
          				}else{
          					$('.por_fecha_asignacion').hide();
          				}
          				if(e.val.indexOf("S")>-1){
          					$('.por_ubicacion').show();
          				}else{
          					$('.por_ubicacion').hide();
          				}
          			});

                      $('#monitoreo_id_sector').select2({
                      	width: '100%'
           	       	});

                      $('body').on('click','#buscar-listado-xml',function(e){

      					criterio = $('#cbo_criterio').val();
      	    	 		date_start = $('#dpt_date_start').val();
      	    	 		date_end = $('#dpt_date_end').val();
      	    	 		id_sector = $('#monitoreo_id_sector').val();
      	        		
      	        		if(oTable != null || typeof oTable != 'undefined') {
                    			$('#tabla_xml').dataTable().fnDestroy();
      	        		}


      	        		$('#tabla_xml').dataTable({
      						"bProcessing": true,
      						"bDeferRender": true,
      						"bServerSide": false,
      						"paging": false,
      						"bSort": false,
      						"iDisplayLength" : 30,
      						"sAjaxSource": ENV_WEBROOT_FULL_URL+"ComlecOrdenlecturas/listarXml/"+criterio+"/"+date_start+"/"+date_end+"/"+id_sector,
      						"aLengthMenu": [
      										[30,50,100],
      										[30,50,100]
      										]
      					});

      	        	});
			
		},

        listarDigitarLecturas: function(){

        	$('#id_tipolectura').select2({
            	width: '100%'
 	       	});
        	$('#id_tipolectura').on("change",function (e) { 
				console.log("change "+e.val);
				$("#id_ruta").change();
			});

        	$('#id_sector').select2({
            	width: '100%'
 	       	});
        	$('#id_sector').on("change",function (e) { 
				console.log("change "+e.val);
				$('.ruta_loading').show();
				$('#id_ruta').select2("enable", false);
				id_sector = e.val || 0;
				$.ajax({
					url : ENV_WEBROOT_FULL_URL+"/ComlecOrdenlecturas/listarRutasPorSector/"+id_sector,
					type : "POST",
					dataType : 'html',
					success : function(data) {
						console.log(data);
						$('#id_ruta').html(data);
						$("#id_ruta").select2("val", "0");
						$("#id_ruta").change();
						$('#id_ruta').select2("enable", true);
						$('.ruta_loading').hide();
					}
				});
			});

			$('#id_ruta').select2({
            	width: '100%'
 	       	});
        	$('#id_ruta').on("change",function (e) { 
				console.log("change "+e.val);
				$('.lecturista_loading').show();
				$('#id_lecturista').select2("enable", false);
				tipolecturaid = $("#id_tipolectura").val();
				id_ruta = e.val || 0;
				$.ajax({
					url : ENV_WEBROOT_FULL_URL+"/ComlecOrdenlecturas/listarLecturistaPorRuta/"+tipolecturaid+"/"+id_ruta,
					type : "POST",
					dataType : 'html',
					success : function(data) {
						console.log(data);
						$('#id_lecturista').html(data);
						$("#id_lecturista").select2("val", "0");
						$('#id_lecturista').select2("enable", true);
						$('.lecturista_loading').hide();
					}
				});
			});

        	var oTable = $('#tabla_digitar').dataTable();
        	
	    	 $('body').on('click','#buscar-lectura',function(e){


	    		 	$("#loading-digitar").mask("Estamos buscando Suministros para Digitación, cargando...");
	    		 
	    	 		tipolecturaid = $("#id_tipolectura").val();
	        		
	        		if(tipolecturaid == ''){
	        			tipolecturaid = 0;
	        		}

	        		rutaid = $("#id_ruta").val();
	        		
	        		if(rutaid == ''){
	        			rutaid = 0;
	        		}

	        		sectorid = $("#id_sector").val();
	        		
	        		if(sectorid == ''){
	        			sectorid = 0;
	        		}

	        		lecturistaid = $("#id_lecturista").val();
	        
	        		if(lecturistaid == ''){
	        			lecturistaid = 0;
	        		}

	        		id_lectespecial = $("#id_lectespecial").val();
	        		
	        		if(id_lectespecial == ''){
	        			id_lectespecial = 0;
	        		}
	        		
	        		search_suministro = $( "#txt_search_by_suministro" ).val();
	        		if(search_suministro == ''){
	    				search_suministro = '0';
    				}else{
    					search_suministro = $( "#txt_search_by_suministro" ).val()
    				}

	        		
	        		if(oTable != null || typeof oTable != 'undefined') {
	        			$('#tabla_digitar tbody').html('');
              			$('#tabla_digitar').dataTable().fnDestroy();
	        		}

	        		$('#tabla_digitar').dataTable({
						"bProcessing": true,
						"bDeferRender": true,
						"bServerSide": true,
						"paging": false,
						"bSort": false,
						"iDisplayLength" : 100,
						"sAjaxSource": ENV_WEBROOT_FULL_URL+"ComlecOrdenlecturas/listar_digitar_lecturas/"+tipolecturaid+"/"+sectorid+"/"+rutaid+"/"+lecturistaid+"/"+id_lectespecial+"/"+search_suministro,
						"aLengthMenu": [
										[100,300,500,800],
										[100,300,500,800]
										],
						"fnDrawCallback" : fnEachTd
					});

	        		function fnEachTd(oSettings){
		        		$('#tabla_digitar tbody').find('tr').each(function(){     						
	              		    var td1 = $(this).children('td').eq(0);
	              		    td1.addClass('hidden');

	              		});	
		        		$('.btn-tooltip').tooltip();
		        		$('[data-popover="popover"]').popover({
		        	        html : true});
		        		
		        		$("#loading-digitar").unmask();
		        		$("#tabla_digitar").unmask();
	        		}
	        		 $('#tabla_digitar').css( "width","100%" );   

					  
					  /*
					  $('#tabla_digitar').css( "font-size","16px" );
					  $('.dataTable thead th').eq(0).css( "width","2%" );
					  $('.dataTable thead th').eq(1).css( "width","3%" );
					  $('.dataTable thead th').eq(5).css( "width","5%" );
					  $('.dataTable thead th').eq(6).css( "width","17%" );
					  $('.dataTable thead th').eq(11).css( "width","2%" );*/
	        		 //Para jonatan
	        		/* var styles = {
	   					  backgroundColor : "#ffffff"
	   					};
	        		 $('#tabla_digitar').css(styles);*/
	        	}); 
	    	 
	    	 Ordenlecturas.loadingBeforeSearchPaginationTable();
		},
		
		loadingBeforeSearchPaginationTable: function(){
			$body = $('body');
			$body.off('click','.pagination a');
			$body.on('click','.pagination a',function(){	
				$("#loading-digitar").mask("Estamos buscando Suministros para Digitación, cargando...");
			});
			
			$body.off('change','#tabla_digitar_length select');
			$body.on('change','#tabla_digitar_length select',function(){	
				$("#loading-digitar").mask("Estamos buscando Suministros para Digitación, cargando...");
			});
			
			$body.off('keyup','#tabla-digitar_filter input');
			$body.on('keyup','#tabla_digitar_filter input',function(){	
				$("#tabla_digitar").mask("Estamos buscando Suministros para Digitación, cargando...");
			});
		},
	
		modalAsignar : function(){			
			$('body').on('click','.md-trigger',function(e){
				/*$('#sidebar-collapse').click();*/
                          
				$('.md-trigger').modalEffects();
				setTimeout(function(){
					$("#tabla_modal tbody tr").on("click",function(event){
					event.preventDefault();
					var itemid = $(this).find("td").eq(0).html();
					$("#hasta").val("");
					$("#hasta").val(itemid);
					$("#desde").val();
					
					total= $("#hasta").val() - $("#desde").val() ;
					    
					$("#total-lecturistas").val("");
					$("#total-lecturistas").val(total + 1);
					
					});
					
					$('select[name="tabla_modal_length"]').change(function(e) {
					  $("#tabla_modal tbody tr").on("click",function(event){
						event.preventDefault();
						var itemid = $(this).find("td").eq(0).html();
						$("#hasta").val("");
						$("#hasta").val(itemid);
						$("#desde").val();
                                               
						total= $("#hasta").val() - $("#desde").val() ;						
						$("#total-lecturistas").val("");
						$("#total-lecturistas").val(total + 1);
						});
					});
					
					$('.pagination').on("click",function(e){
					  $("#tabla_modal tbody tr").on("click",function(event){
						event.preventDefault();
						var itemid = $(this).find("td").eq(0).html();
						$("#hasta").val("");
						$("#hasta").val(itemid);
						$("#desde").val();					
						total= $("#hasta").val() - $("#desde").val() ;						
						$("#total-lecturistas").val("");
						$("#total-lecturistas").val(total + 1);

						});
					});

				}, 2000);
				
	
			});
		},
		
		tableColor: function(){
			$( "#listar_asignacion tbody tr" ).on( "mouseover", function() {
				  $( this ).css( "color", "blue" );
				});
				
				$("#listar_asignacion tbody tr").on("mouseenter", function(){
					$(this).css({
					  "background-color": "yellow"
					});
				}).on("mouseleave", function(){
					var styles = {
					  backgroundColor : "#ffffff"
					};
					$(this).css(styles);
				}).on("click", function(){
					var styles = {
					  backgroundColor : "#ddd"
					};
					$(this).css(styles);
				});
		},
				
		tableColorReLectura: function(){
			$( "#re-lectura tbody tr" ).on( "mouseover", function() {
				  $( this ).css( "color", "blue" );
				});
				
				$("#re-lectura tbody tr").on("mouseenter", function(){
					$(this).css({
					  "background-color": "yellow"
					});
				}).on("mouseleave", function(){
					var styles = {
					  backgroundColor : "#ffffff"
					};
					$(this).css(styles);
				}).on("click", function(){
					var styles = {
					  backgroundColor : "#ddd"
					};
					$(this).css(styles);
				});
		},
		
		tableColorDigitar: function(){
							
				$("#digitar_lecturas tbody tr").on("mouseenter", function(){
					$(this).css({
					  "background-color": "yellow"
					});
				}).on("mouseleave", function(){
					var styles = {
					  backgroundColor : "#ffffff"
					};
					$(this).css(styles);
				}).on("click", function(){
					var styles = {
					  backgroundColor : "#ddd"
					};
					$(this).css(styles);
				});
		}, 
		digitarLecturas: function(){
			var oTable = $('#digitar_lecturas').dataTable( {
				"bScrollCollapse": true,
				"sDom": 'C<"clear">lfrtip'
				} );	
		},
		
		agregarDataTable: function(){
			var oTable = $('#listar_asignacion').dataTable( {
			"paging": true,
    				"bSort": true,
    				"aaSorting": [[ 7, "desc" ]],
    				"iDisplayLength" : 5,
    				"aLengthMenu": [
    								[5,10,30,50,100],
    								[5,10,30,50,100]
    								],
			"bScrollCollapse": true,
			"sDom": 'C<"clear">lfrtip'
			} );





		},
		
		agregarDataTableLibros: function(){
			var oTable = $('#createlibro').dataTable( {
			"bScrollCollapse": true,
			"sDom": 'C<"clear">lfrtip'
			} );
		},
                    

		agregarDataTableCicloSector : function() {
			
			function fnFormatDetails(oTable, nTr) {
				var aData = oTable.fnGetData(nTr);
				var formURL = ENV_WEBROOT_FULL_URL+'/ComlecOrdenlecturas/listarlibro';
				var postData = 'tipo=' + $('#id_tipolectura').val() + '&ciclo=' + aData[1] + '&sector='+ aData[2] + '&ruta=' + aData[9] + '&pfactura=' + aData[10];
				$.ajax({
					url : ENV_WEBROOT_FULL_URL+"/ComlecOrdenlecturas/listarLibros",
					type : "POST",
					data : postData,
					dataType : 'json',
					success : function(data, textStatus, jqXHR) {
						var sRetorno = '<table id= "libro_lecturista" style="font-weight:bold;" id="'+ aData[1] + aData[2]	+ aData[9] + 'tabla" cellpadding="5" cellspacing="0" border="0" style="padding-left:50px;">';
						sRetorno += '<tr><th>Libro</th><th>Desde - Hasta</th><th>Numero Suministros</th><th>Lecturista</th><th>Estado</th><th></th></tr>';
						$.each(data['aaData'], function(i, item) {
									sRetorno += '<tr ciclo="'+ aData[1] + '" sector="'
                                            + aData[2]  
                                            + '"ruta="' 
                                            + aData[9]
                                            + '"id_lecturista = "'
											+ item[0]
											+ '"><td><a id="liberar_descargas" libro= "'
											+ item[0]
											+ '" >'
											+ item[0]
											+ '</a></td><td>'
											+ item[3] + '-'
											+ item[4]
											+ '</td><td>'
											+ item[2]
											+ '</td><td>'
											+ item[1]
											+ '</td><td>'
											+ item[5]
											+ '</td><td><button class=\"btn btn-info\" id=\"imprimir-libros-lectura\" libro= "'+ item[0]+ '" ruta="' + aData[9]+ '" >Imprimir</button></td></tr>';
								});
						sRetorno += '</table>';

						$('#' + aData[1] + aData[2] + aData[9]).html(sRetorno);
					}
				});
		
				var sOut = '<div id="' + aData[1] + aData[2] + aData[9]	+ '" >';		
				sOut += '<img src="'+ENV_WEBROOT_FULL_URL+'/images/ajax-loader.gif" /></div>';		
				return sOut;
			}

			var nCloneTh = document.createElement('th');
			var nCloneTd = document.createElement('td');
			nCloneTd.innerHTML = '<img class="toggle-details" src="'+ENV_WEBROOT_FULL_URL+'/images/plus.png" />';
			nCloneTd.className = "center";

			$('#ciclosector thead tr').each(function() {
				this.insertBefore(nCloneTh, this.childNodes[0]);
			});

			$('#ciclosector tbody tr').each(function(){
				this.insertBefore(nCloneTd.cloneNode(true),	this.childNodes[0]);
			});

			var oTable = $('#ciclosector').dataTable({
				"bScrollCollapse" : true,
				"sDom" : 'C<"clear">lfrtip',
				"aoColumnDefs" : [ {
					"bSortable" : false,
					"aTargets" : [ 0 ]
				} ],
				"aaSorting" : [ [ 1, 'asc' ] ]
			});
			
			$('.md-trigger').modalEffects();
			
			$('#ciclosector').delegate(
					'tbody td img',
					'click',
					function() {
						var nTr = $(this).parents('tr')[0];
						if (oTable.fnIsOpen(nTr)) {

							this.src = ENV_WEBROOT_FULL_URL+"/images/plus.png";
							oTable.fnClose(nTr);
						} else {
							this.src = ENV_WEBROOT_FULL_URL+"/images/minus.png";
							oTable.fnOpen(nTr, fnFormatDetails(oTable,
									nTr), 'details');
						}
					});

			$('.dataTables_filter input').addClass('form-control').attr('placeholder', 'Search');
			$('.dataTables_length select').addClass('form-control');

		},
                agregarDataTableCicloSectorRelecturas : function() {
			
			function fnFormatDetails(oTable, nTr) {
				var aData = oTable.fnGetData(nTr);
				//var formURL = '/ComlecOrdenlecturas/listarlibrorelecturas';
				var postData = 'ciclo=' + aData[1] + '&sector='+ aData[2] + '&ruta=' + aData[7] + '&pfactura=' + aData[8];
				$.ajax({
					url : ENV_WEBROOT_FULL_URL+"/ComlecOrdenlecturas/listarLibrosRelecturas",
					type : "POST",
					data : postData,
					dataType : 'json',
					success : function(data, textStatus, jqXHR) {
						var sRetorno = '<table style="font-weight:bold;" id="'+ aData[1] + aData[2]	+ aData[7] + 'tabla" cellpadding="5" cellspacing="0" border="0" style="padding-left:50px;">';
						sRetorno += '<tr><th>Libro</th><th>Desde - Hasta</th><th>Numero Suministros</th><th>Lecturista</th><th>Estado</th></tr>';
						$.each(data['aaData'], function(i, item) {
									sRetorno += '<tr><td>'
											+ item[0]
											+ '</td><td>'
											+ item[3] + '-'
											+ item[4]
											+ '</td><td>'
											+ item[2]
											+ '</td><td>'
											+ item[1]
											+ '</td><td>'
											+ item[5]
											+ '</td></tr>';
								});
						sRetorno += '</table>';

						$('#' + aData[1] + aData[2] + aData[7]).html(sRetorno);
					}
				});
		
				var sOut = '<div id="' + aData[1] + aData[2] + aData[7]	+ '" >';		
				sOut += '<img src="'+ENV_WEBROOT_FULL_URL+'/images/ajax-loader.gif" /></div>';		
				return sOut;
			}

			var nCloneTh = document.createElement('th');
			var nCloneTd = document.createElement('td');
			nCloneTd.innerHTML = '<img class="toggle-details" src="'+ENV_WEBROOT_FULL_URL+'/images/plus.png" />';
			nCloneTd.className = "center";

			$('#ciclosector thead tr').each(function() {
				this.insertBefore(nCloneTh, this.childNodes[0]);
			});

			$('#ciclosector tbody tr').each(function(){
				this.insertBefore(nCloneTd.cloneNode(true),	this.childNodes[0]);
			});

			var oTable = $('#ciclosector').dataTable({
				"bScrollCollapse" : true,
				"sDom" : 'C<"clear">lfrtip',
				"aoColumnDefs" : [ {
					"bSortable" : false,
					"aTargets" : [ 0 ]
				} ],
				"aaSorting" : [ [ 1, 'asc' ] ]
			});
			
			$('.md-trigger').modalEffects();

			$('#ciclosector').delegate(
					'tbody td img',
					'click',
					function() {
						var nTr = $(this).parents('tr')[0];
						if (oTable.fnIsOpen(nTr)) {

							this.src = ENV_WEBROOT_FULL_URL+"/images/plus.png";
							oTable.fnClose(nTr);
						} else {
							this.src = ENV_WEBROOT_FULL_URL+"/images/minus.png";
							oTable.fnOpen(nTr, fnFormatDetails(oTable,
									nTr), 'details');
						}
					});

			$('.dataTables_filter input').addClass('form-control').attr('placeholder', 'Search');
			$('.dataTables_length select').addClass('form-control');

		},
                mostrarLecturistaPorLibro : function(){
                    
                    $body = $('body');
                    $body.off('click','#libro_lecturista tr');
                    $body.on('click','#libro_lecturista tr',function(){
                        var lecturita_id = $(this).attr('id_lecturista');
                        var ciclo = $(this).attr('ciclo');
                        var secgtor = $(this).attr('sector');
                        var ruta = $(this).attr('ruta');
                        


                    })
                },
                
                liberarDescargaLibro : function(){
				
        				$body = $('body');
        				$body.off('click','#liberar_descargas');
        				$body.on('click','#liberar_descargas',function(event){
        					var libro = $(this).attr('libro');
        					 event.stopPropagation();
        					   if(confirm("Desea Liberar la Descarga")) {
        						   var actn = $(this);
               					//$(this).addClass("active");
                                   $.ajax({
                       				url: ENV_WEBROOT_FULL_URL+"ComlecOrdenlecturas/liberarLibro/",
                       				type: 'post',
                       				data: {'libro':libro},
                       				dataType: 'html'
	                       			}).done(function(data){
	                       				if(data == 0){
	                       					alert ('Ya puede descargar desde el movil');
	                       					actn.parents('tr:first').css({background:'#3380FF'});
	                       				}else {
	                       					alert('Ya puede descargar desde el movil');
	                       					actn.parents('tr:first').css({background:'#3380FF'});
	                       				}			
	                       			});
        					   } else {
        					       alert("Cancel");
        					   }       
        					   event.preventDefault(); 
        				});

        		},
        		
    		listarAsignacion: function (){
    			$('#unidadneg').select2({
                	width: '100%'
     	       	});
            	$('#unidadneg').on("change",function (e) { 
    				console.log("change "+e.val);
    				$('.ciclo_loading').show();
    				$('#id_ciclo').select2("enable", false);
    				unidad_neg = e.val || 0;
    				$.ajax({
                        url : ENV_WEBROOT_FULL_URL+"/ComlecOrdenlecturas/listarCicloPorUnidadneg/"+unidad_neg,
    					type : "POST",
    					dataType : 'html',
    					success : function(data) {
    						console.log(data);
    						$('#id_ciclo').html(data);
    						$("#id_ciclo").select2("val", "0");
    						$('#id_ciclo').select2("enable", true);
    						$('.ciclo_loading').hide();
    					}
    				});
    			});  
                $('#id_ciclo').select2({
                	width: '100%'
     	       	});
            	$('#id_ciclo').on("change",function (e) { 
    				console.log("change "+e.val);
    				$('.sector_loading').show();
    				$('#monitoreo_id_sector').select2("enable", false);
    				id_ciclo = e.val || 0;
    				$.ajax({
    					url : ENV_WEBROOT_FULL_URL+"/ComlecOrdenlecturas/listarSectorPorCiclo/"+id_ciclo,
    					type : "POST",
    					dataType : 'html',
    					success : function(data) {
    						console.log(data);
    						$('#monitoreo_id_sector').html(data);
    						$("#monitoreo_id_sector").select2("val", "0");
    						$('#monitoreo_id_sector').select2("enable", true);
    						$('.sector_loading').hide();
    					}
    				});
    			});
                            
    			$('#monitoreo_id_sector').select2({
                	width: '100%'
     	       	});
    			
    			$('body').on('click','#buscar-listar-asignacion',function(e){

					tipo_lectura = $('#id_tipolectura').val();
					unidadneg = $('#unidadneg').val();
	    	 		id_ciclo = $('#id_ciclo').val();
	    	 		id_sector = $('#monitoreo_id_sector').val();
	    	 		
	    	 		window.open(ENV_WEBROOT_FULL_URL+'ComlecOrdenlecturas/listar_asignacion/'+tipo_lectura+'/'+unidadneg+'/'+id_ciclo+'/'+id_sector,'_self');
    			});
    			



    			var oTable = $('#listar_asignacion').dataTable({
    				"paging": true,
    				"bSort": true,
    				"aaSorting": [[ 7, "desc" ]],
    				"iDisplayLength" : 5,
    				"aLengthMenu": [
    								[5,10,30,50,100],
    								[5,10,30,50,100]
    								],
					"fnFooterCallback": function( nFoot, aData, iStart, iEnd, aiDisplay ){

							$(nFoot).html('');
							$(nFoot).append('<td colspan="5" style="text-align:right"><strong>Total:</strong></td>');
							
							var col = 5;
							var tot = 0;
							var tot_filter = 0;
							for ( var i = 0; i < aData.length; i++) {
								tot += aData[i][col] * 1;
							}
							for ( var i = iStart; i < iEnd; i++) {
								tot_filter += aData[aiDisplay[i]][col] * 1;
							}
							$(nFoot).append('<td><strong>' + tot_filter + ' (' + tot + '  total)</strong></td>');
							
							var col = 6;
							var tot = 0;
							var tot_filter = 0;
							for ( var i = 0; i < aData.length; i++) {
								tot += aData[i][col] * 1;
							}
							for ( var i = iStart; i < iEnd; i++) {
								tot_filter += aData[aiDisplay[i]][col] * 1;
							}
							$(nFoot).append('<td><strong>' + tot_filter + ' (' + tot + '  total)</strong></td>');
							
							var col = 7;
							var tot = 0;
							var tot_filter = 0;
							for ( var i = 0; i < aData.length; i++) {
								tot += aData[i][col] * 1;
							}
							for ( var i = iStart; i < iEnd; i++) {
								tot_filter += aData[aiDisplay[i]][col] * 1;
							}
							$(nFoot).append('<td><strong>' + tot_filter + ' (' + tot + '  total)</strong></td>');

			        }
    			});


    			$('.dataTables_filter input').addClass('form-control').attr('placeholder','Escribe Aquí');
    		},
    		
    		listarPersonalParaLecturas: function() {
    			var oTable = $('#tabla_listar_personal_para_lecturas').dataTable({
   						"iDisplayLength" : 50,
   						"aLengthMenu": [
   										[50,100,300,500,800],
   										[50,100,300,500,800]
   										],
						"sDom": '<"top"p>T<"clear"><"datatables_filter"lrf><"bottom">ti',
			         	"oTableTools": {
			         		"sSwfPath":  ENV_WEBROOT_FULL_URL+"js/jquery.datatables/extras/TableTools/media/swf/copy_csv_xls_pdf.swf"
						},
   					});
            	
   	    	 	$('body').on('click','#buscar-personal-para-lectura',function(e){


   	    	 		tipolecturaid = $("#id_tipolectura").val();
   	        		
   	        		if(tipolecturaid == ''){
   	        			tipolecturaid = 0;
   	        		}
   	        		
   	        		unidadneg = $("#unidadneg").val();
   	        		
   	        		if(unidadneg == ''){
   	        			unidadneg = 0;
   	        		}
   	        		
   	        		idciclo = $("#id_ciclo").val();
   	        		
   	        		if(idciclo == ''){
   	        			idciclo = 0;
   	        		}

   	        		sectorid = $("#monitoreo_id_sector").val();
   	        		
   	        		if(sectorid == ''){
   	        			sectorid = 0;
   	        		}

   	        		if(oTable != null || typeof oTable != 'undefined') {
                 			$('#tabla_listar_personal_para_lecturas').dataTable().fnDestroy();
   	        		}

   	        		$('#tabla_listar_personal_para_lecturas').dataTable({
   						"bProcessing": true,
   						"bDeferRender": true,
   						"bServerSide": false,
   						"paging": false,
   						"bSort": false,
   						"iDisplayLength" : 50,
   						"sAjaxSource": ENV_WEBROOT_FULL_URL+"ComlecOrdenlecturas/ajax_listar_personal_para_lecturas/"+tipolecturaid+"/"+unidadneg+"/"+idciclo+"/"+sectorid,
   						"aLengthMenu": [
   										[50,100,300,500,800],
   										[50,100,300,500,800]
   										],
   						"fnDrawCallback" : fnEachTd,
   						"sDom": '<"top"p>T<"clear"><"datatables_filter"lrf><"bottom">ti',
			         	"oTableTools": {
			         		"sSwfPath":  ENV_WEBROOT_FULL_URL+"js/jquery.datatables/extras/TableTools/media/swf/copy_csv_xls_pdf.swf"
						},
   					});

   	        		function fnEachTd(oSettings){
   		        		$('.btn-tooltip').tooltip();
   	        		}
   	        		$('#tabla_listar_personal_para_lecturas').css( "width","100%" );
   	        	}); 
    		}

	};
	
Ordenlecturas.init();

return Ordenlecturas;

});