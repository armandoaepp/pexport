define(['ajax/inconsistencia','jquery.datatables','fancyapps','loadmask'],function(Inconsistencia){
	if(window.monitorear){ //Prevents defining the module twice
		return false;
	}
	console.log('INIT: Monitorear.js');
	var Monitorear = {

		init: function(){			
			this.initViewController();
			this.bindEvents();
			},
		
		bindEvents: function(){
			$body = $('body');
		},
		
		initViewController: function(){        
			Inconsistencia.ModalImagenInconsistencia();
			Monitorear.listMonitorearLecturistas();
			
		},
		
		listMonitorearLecturistas: function(){
			
			$("#loading-monitoreo").mask("Estamos buscando datos del Monitoreo de Lecturistas, cargando...");

			var oTable = $('#list-monitorear-lecturistas').dataTable({
				"paging": true,
				"bSort": false,
				"iDisplayLength" : 30,
				"aLengthMenu": [
								[30,50,100],
								[30,50,100]
								],
				"fnFooterCallback": function( nFoot, aData, iStart, iEnd, aiDisplay ){
					Monitorear.mi_fnFooterCallback( nFoot, aData, iStart, iEnd, aiDisplay );
				}
			});
			
			var preload_data = [
	            { id: 'F', text: 'Fecha de Asignación'}
	            , { id: 'U', text: 'Ubicación Geográfica'}
	          ];
           
            $('#cbo_criterio').select2({
                multiple: true
                ,query: function (query){
                    var data = {results: []};
           
                    $.each(preload_data, function(){
                        if(query.term.length == 0 || this.text.toUpperCase().indexOf(query.term.toUpperCase()) >= 0 ){
                            data.results.push({id: this.id, text: this.text });
                        }
                    });
           
                    query.callback(data);
                }
            });
            $('#cbo_criterio').select2('data', [{ id: 'F', text: 'Fecha de Asignación'}] )
            
            $('#cbo_criterio').on("change",function (e) { 
				console.log("change "+e.val);
				if(e.val.indexOf("F")>-1){
					$('.por_fecha_asignacion').show();					
				}else{
					$('.por_fecha_asignacion').hide();
				}
				if(e.val.indexOf("U")>-1){
					$('.por_ubicacion').show();
				}else{
					$('.por_ubicacion').hide();
				}
			});
           
            $('#unidadneg').select2({
            	width: '100%'
 	       	});
        	$('#unidadneg').on("change",function (e) { 
				console.log("change "+e.val);
				$('.ciclo_loading').show();
				$('#id_ciclo').select2("enable", false);
				unidad_neg = e.val || 0;
				$.ajax({
                                        url : ENV_WEBROOT_FULL_URL+"/ComlecOrdenlecturas/listarCicloPorUnidadneg/"+unidad_neg,
					type : "POST",
					dataType : 'html',
					success : function(data) {
						console.log(data);
						$('#id_ciclo').html(data);
						$("#id_ciclo").select2("val", "0");
						$('#id_ciclo').select2("enable", true);
						$('.ciclo_loading').hide();
					}
				});
			});  
            $('#id_ciclo').select2({
            	width: '100%'
 	       	});
        	$('#id_ciclo').on("change",function (e) { 
				console.log("change "+e.val);
				$('.sector_loading').show();
				$('#monitoreo_id_sector').select2("enable", false);
				id_ciclo = e.val || 0;
				$.ajax({
					url : ENV_WEBROOT_FULL_URL+"/ComlecOrdenlecturas/listarSectorPorCiclo/"+id_ciclo,
					type : "POST",
					dataType : 'html',
					success : function(data) {
						console.log(data);
						$('#monitoreo_id_sector').html(data);
						$("#monitoreo_id_sector").select2("val", "0");
						$('#monitoreo_id_sector').select2("enable", true);
						$('.sector_loading').hide();
					}
				});
			});
                        
                                   
                        
			$('#monitoreo_id_sector').select2({
            	width: '100%'
 	       	});

			$('body').on('click','#buscar-monitoreo',function(e){

					criterio = $('#cbo_criterio').val();
	    	 		date_start = $('#dpt_date_start').val();
	    	 		date_end = $('#dpt_date_end').val();
	    	 		unidadneg = $('#unidadneg').val();
	    	 		id_ciclo = $('#id_ciclo').val();
	    	 		id_sector = $('#monitoreo_id_sector').val();
	        		
	        		if(oTable != null || typeof oTable != 'undefined') {
              			$('#list-monitorear-lecturistas').dataTable().fnDestroy();
	        		}

	        		$('#list-monitorear-lecturistas').dataTable({
						"bProcessing": true,
						"bDeferRender": true,
						"bServerSide": false,
						"paging": false,
						"bSort": false,
						"iDisplayLength" : 30,
						"sAjaxSource": ENV_WEBROOT_FULL_URL+"MonitorearLecturas/ajax_monitorear_lectura/"+criterio+"/"+date_start+"/"+date_end+"/"+unidadneg+"/"+id_ciclo+"/"+id_sector,
						"aLengthMenu": [
										[30,50,100],
										[30,50,100]
										],
						"fnDrawCallback" : fnEachTd,
						"fnFooterCallback": function( nFoot, aData, iStart, iEnd, aiDisplay ){
							Monitorear.mi_fnFooterCallback( nFoot, aData, iStart, iEnd, aiDisplay );
						}
					});
	        		
	        		$("#loading-monitoreo").mask("Estamos buscando datos del Monitoreo de Lecturistas, cargando...");

	        		function fnEachTd(oSettings){
		        		$('#list-monitorear-lecturistas tbody').find('tr').each(function(){     						
		        			$(this).children('td').eq(0).addClass('hidden');
		        			$(this).children('td').eq(1).addClass('hidden');
		        			$(this).children('td').eq(2).addClass('hidden');
		        			$(this).children('td').eq(3).addClass('hidden');
		        			$(this).children('td').eq(4).addClass('hidden');
		        			$(this).children('td').eq(5).addClass('hidden');
		        			$(this).children('td').eq(6).addClass('hidden');
	              		});
	        		}
	        		$('#list-monitorear-lecturistas').css( "width","100%" );
	        		$('.dataTable thead th').eq(7).css( "width","1%" );
	        		$('.dataTable thead th').eq(8).css( "width","10%" );
	        		$('.dataTable thead th').eq(9).css( "width","4%" );
	        		$('.dataTable thead th').eq(10).css( "width","4%" );
	        		$('.dataTable thead th').eq(11).css( "width","8%" );
	        		$('.dataTable thead th').eq(12).css( "width","3%" );
	        		$('.dataTable thead th').eq(13).css( "width","3%" );    
	        		$('.dataTable thead th').eq(14).css( "width","15%" );
	        		$('.dataTable thead th').eq(15).css( "width","7%" ); 
	        		$('.dataTable thead th').eq(16).css( "width","4%" ); 
	        		$('.dataTable thead th').eq(17).css( "width","4%" );
	        		$('.dataTable thead th').eq(18).css( "width","5%" );
	        		$('.dataTable thead th').eq(19).css( "width","2%" );   
	        	}); 

			$('body').on('click','#btn_mapa_monitoreo',function(e){
				criterio = $('#cbo_criterio').val();
    	 		date_start = $('#dpt_date_start').val();
    	 		date_end = $('#dpt_date_end').val();
    	 		unidadneg = $('#unidadneg').val();
    	 		id_ciclo = $('#id_ciclo').val();
    	 		id_sector = $('#monitoreo_id_sector').val();
    	 		
    	 		window.open(ENV_WEBROOT_FULL_URL+"MonitorearLecturas/mapa_monitoreo/"+criterio+"/"+date_start+"/"+date_end+"/"+unidadneg+"/"+id_ciclo+"/"+id_sector,"_blank");
			});
			
			Monitorear.loadingBeforeSearchPaginationTable();
		},
		
		mi_fnFooterCallback: function( nFoot, aData, iStart, iEnd, aiDisplay ){
			$(nFoot).html('');
			$(nFoot).append('<td colspan="2" style="text-align:right"><strong>Total:</strong></td>');
			
			var col = 9;
			var tot = 0;
			var tot_filter = 0;
			for ( var i = 0; i < aData.length; i++) {
				tot += aData[i][col] * 1;
			}
			for ( var i = iStart; i < iEnd; i++) {
				tot_filter += aData[aiDisplay[i]][col] * 1;
			}
			$(nFoot).append('<td><strong>' + tot_filter + ' (' + tot + '  total)</strong></td>');
			
			var col = 10;
			var tot = 0;
			var tot_filter = 0;
			for ( var i = 0; i < aData.length; i++) {
				tot += aData[i][col] * 1;
			}
			for ( var i = iStart; i < iEnd; i++) {
				tot_filter += aData[aiDisplay[i]][col] * 1;
			}
			$(nFoot).append('<td><strong>' + tot_filter + ' (' + tot + '  total)</strong></td>');
			
			$(nFoot).append('<td></td>');
			
			var col = 12;
			var tot = 0;
			var tot_filter = 0;
			for ( var i = 0; i < aData.length; i++) {
				tot += aData[i][col] * 1;
			}
			for ( var i = iStart; i < iEnd; i++) {
				tot_filter += aData[aiDisplay[i]][col] * 1;
			}
			$(nFoot).append('<td><strong>' + tot_filter + ' (' + tot + '  total)</strong></td>');
			
			var col = 13;
			var tot = 0;
			var tot_filter = 0;
			for ( var i = 0; i < aData.length; i++) {
				tot += aData[i][col] * 1;
			}
			for ( var i = iStart; i < iEnd; i++) {
				tot_filter += aData[aiDisplay[i]][col] * 1;
			}
			$(nFoot).append('<td><strong>' + tot_filter + ' (' + tot + '  total)</strong></td>');
			
			//$(nFoot).append('<td></td>');
			
			var col = 5;
			var tot = 0;
			var tot_filter = 0;
			for ( var i = 0; i < aData.length; i++) {
				tot += aData[i][col] * 1;
			}
			for ( var i = iStart; i < iEnd; i++) {
				tot_filter += aData[aiDisplay[i]][col] * 1;
			}
			str_total_terminadas_page = tot_filter;
			str_total_terminadas = tot;
			
			var col = 6;
			var tot = 0;
			var tot_filter = 0;
			for ( var i = 0; i < aData.length; i++) {
				tot += aData[i][col] * 1;
			}
			for ( var i = iStart; i < iEnd; i++) {
				tot_filter += aData[aiDisplay[i]][col] * 1;
			}
			str_total_asignadas_page = tot_filter;
			str_total_asignadas = tot;
			str_porcentaje_page = ((str_total_terminadas_page * 100) / str_total_asignadas_page).toFixed(2);
			str_porcentaje = ((str_total_terminadas * 100) / str_total_asignadas).toFixed(2);
			if(str_porcentaje_page>=50){
				str_style_porcentaje_page = 'progress-bar progress-bar-success';
			}else{
				str_style_porcentaje_page = 'progress-bar progress-bar-success';
			}
			str_progress_page = '<div class="progress progress-striped" title ="' + str_porcentaje_page + '% - ' + str_total_terminadas_page + ' Lecturas Terminadas.\"><div class="'+ str_style_porcentaje_page +'" style="width: '+ str_porcentaje_page +'% ; color: black; font-weight: bold;\">'+ str_porcentaje_page +'% ('+ str_total_terminadas_page +')</div></div>';
			
			if(str_porcentaje>=50){
				str_style_porcentaje = 'progress-bar progress-bar-success';
			}else{
				str_style_porcentaje = 'progress-bar progress-bar-success';
			}
			str_progress = '<div class="progress progress-striped" title ="' + str_porcentaje + '% - ' + str_total_terminadas + ' Lecturas Terminadas.\"><div class="'+ str_style_porcentaje +'" style="width: '+ str_porcentaje +'% ; color: black; font-weight: bold;\">'+ str_porcentaje +'% ('+ str_total_terminadas +')</div></div>';
			//$(nFoot).append('<td><div class="progress progress-striped" title ="' + str_porcentaje + '% - ' + str_total_terminadas + ' Lecturas Terminadas.\"><div class="'+ str_style_porcentaje +'" style="width: '+ str_porcentaje +'% ; color: black; font-weight: bold;\">'+ str_porcentaje +'% ('+ str_total_terminadas +')</div></div></td>');
			$(nFoot).append('<td>' + str_progress_page + '<br>' + str_progress + '</td>');
					
			var col = 3;
			var tot = 0;
			var tot_filter = 0;
			for ( var i = 0; i < aData.length; i++) {
				tot += aData[i][col] * 1;
			}
			for ( var i = iStart; i < iEnd; i++) {
				tot_filter += aData[aiDisplay[i]][col] * 1;
			}
			tot_str1 = '<strong> Originales: ' + tot_filter + ' (' + tot + '  total)</strong>';
			
			var col = 4;
			var tot = 0;
			var tot_filter = 0;
			for ( var i = 0; i < aData.length; i++) {
				tot += aData[i][col] * 1;
			}
			for ( var i = iStart; i < iEnd; i++) {
				tot_filter += aData[aiDisplay[i]][col] * 1;
			}
			tot_str2 = '<strong> Actuales: ' + tot_filter + ' (' + tot + '  total)</strong>';
			$(nFoot).append('<td>' + tot_str1 + '<br>' + tot_str2 + '</td>');
			
			var col = 16;
			var tot = 0;
			var tot_filter = 0;
			for ( var i = 0; i < aData.length; i++) {
				tot += aData[i][col] * 1;
			}
			for ( var i = iStart; i < iEnd; i++) {
				tot_filter += aData[aiDisplay[i]][col] * 1;
			}
			$(nFoot).append('<td><strong>' + tot_filter + ' (' + tot + '  total)</strong></td>');
			
			var col = 17;
			var tot = 0;
			var tot_filter = 0;
			for ( var i = 0; i < aData.length; i++) {
				tot += aData[i][col] * 1;
			}
			for ( var i = iStart; i < iEnd; i++) {
				tot_filter += aData[aiDisplay[i]][col] * 1;
			}
			$(nFoot).append('<td><strong>' + tot_filter + ' (' + tot + '  total)</strong></td>');
			
			var col = 0;
			var tot = 0;
			var tot_filter = 0;
			for ( var i = 0; i < aData.length; i++) {
				tot += aData[i][col] * 1;
			}
			for ( var i = iStart; i < iEnd; i++) {
				tot_filter += aData[aiDisplay[i]][col] * 1;
			}
			tot_str1 = '<strong> T: ' + tot_filter + ' (' + tot + '  total)</strong>';
			
			var col = 1;
			var tot = 0;
			var tot_filter = 0;
			for ( var i = 0; i < aData.length; i++) {
				tot += aData[i][col] * 1;
			}
			for ( var i = iStart; i < iEnd; i++) {
				tot_filter += aData[aiDisplay[i]][col] * 1;
			}
			tot_str2 = '<strong> SL: ' + tot_filter + ' (' + tot + '  total)</strong>';
			
			var col = 2;
			var tot = 0;
			var tot_filter = 0;
			for ( var i = 0; i < aData.length; i++) {
				tot += aData[i][col] * 1;
			}
			for ( var i = iStart; i < iEnd; i++) {
				tot_filter += aData[aiDisplay[i]][col] * 1;
			}
			tot_str3 = '<strong> CL: ' + tot_filter + ' (' + tot + '  total)</strong>';
			$(nFoot).append('<td colspan="2">' + tot_str1 + '<br>' + tot_str2 + '<br>' + tot_str3 + '</td>');
			
			//$(nFoot).append('<td></td>');
			
			$("#loading-monitoreo").unmask();
		},
		
		loadingBeforeSearchPaginationTable: function(){
			$body = $('body');
			$body.off('click','.pagination a');
			$body.on('click','.pagination a',function(){	
				$("#loading-monitoreo").mask("Estamos buscando datos del Monitoreo de Lecturistas, cargando...");
			});
		}
	};
	
	Monitorear.init();

return Monitorear;

});