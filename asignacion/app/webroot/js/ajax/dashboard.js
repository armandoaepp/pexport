define(function(){
	if(window.Dashboard){ //Prevents defining the module twice
		return false;
	}
	console.log('INIT: Dashboard.js');
	var Dashboard = {

		init: function(){			
			this.initViewController();
			this.bindEvents();
			},
		
		bindEvents: function(){
			$body = $('body');
		},
		
		initViewController: function(){        
			
			$('#unidadneg').select2({
            	width: '100%'
 	       	});
        	$('#unidadneg').on("change",function (e) { 
				console.log("change "+e.val);
				$('.ciclo_loading').show();
				$('#id_ciclo').select2("enable", false);
				unidad_neg = e.val || 0;
				$.ajax({
                                        url : ENV_WEBROOT_FULL_URL+"/ComlecOrdenlecturas/listarCicloPorUnidadneg/"+unidad_neg,
					type : "POST",
					dataType : 'html',
					success : function(data) {
						console.log(data);
						$('#id_ciclo').html(data);
						$("#id_ciclo").select2("val", "0");
						$('#id_ciclo').select2("enable", true);
						$('.ciclo_loading').hide();
					}
				});
			});  
            $('#id_ciclo').select2({
            	width: '100%'
 	       	});
            
            $('#btn_reload_dashboard').on('click',function(){
            	if($('#unidadneg').val()!='0' || $('#id_ciclo').val()!='0'){
            		window.open(ENV_WEBROOT_FULL_URL+'dashboard/index_new/'+$('#unidadneg').val()+'/'+$('#id_ciclo').val(),'_top');
            	}else{
            		window.open(ENV_WEBROOT_FULL_URL+'dashboard','_top');
            	}
            	
            });
        	
		}
	};
	
	Dashboard.init();

	return Dashboard;
});