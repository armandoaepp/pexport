<?php
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
* Easy set variables
*/
 
/* Array of database columns which should be read and sent back to DataTables. Use a space where
* you want to insert a non-database field (for example a counter or static image)
*/
 
// add your columns here!!!
$aColumns = array( 'IdTipoIntervencion', 'DetalleTipoIntervencion', 'IdTipoIntervencionSup', 'DownMovil' );
 
/* MSSQL Database infomation */
$host = '190.41.196.139';
$user = "dmdeveloper";
$pass = "dmdeveloper";
$db = "wsPexport";
 
/* MSSQL Connection */
$link = mssql_connect ( $host, $user, $pass ) or die ( 'Can not connect to server' );
 
/* MSSQL Database Selection */
mssql_select_db ( $db, $link ) or die ( 'Can not select database' );
 
/* Indexed column (used for fast and accurate table cardinality) */
$sIndexColumn = "IdTipoIntervencion";
 
/* DB table to use */
$sTable = "[ComMovTab.TipoIntervencion]";
 
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
* If you just want to use the basic configuration for DataTables with PHP server-side, there is
* no need to edit below this line
*/
 
/*
* Local functions
*/
function fatal_error ( $sErrorMessage = '' )
{
header( $_SERVER['SERVER_PROTOCOL'] .' 500 Internal Server Error' );
die( $sErrorMessage );
}
 
 
/* Ordering */
$sOrder = "";
if ( isset( $_GET['iSortCol_0'] ) )
{
$sOrder = "ORDER BY ";
for ( $i=0 ; $i<intval( $_GET['iSortingCols'] ) ; $i++ )
{
if ( $_GET[ 'bSortable_'.intval($_GET['iSortCol_'.$i]) ] == "true" )
{
$sOrder .= "".$aColumns[ intval( $_GET['iSortCol_'.$i] ) ]." ".
($_GET['sSortDir_'.$i]==='asc' ? 'asc' : 'desc');
}
}
}
 
/* escape function */
function mssql_escape($data) {
if(is_numeric($data))
return $data;
$unpacked = unpack('H*hex', $data);
return '0x' . $unpacked['hex'];
}
 
/* Filtering */
$sWhere = "";
if ( isset($_GET['sSearch']) && $_GET['sSearch'] != "" ) {
$sWhere = "WHERE (";
for ( $i=0 ; $i<count($aColumns) ; $i++ ) {
$sWhere .= $aColumns[$i]." LIKE '%".addslashes( $_GET['sSearch'] )."%' OR ";
}
$sWhere = substr_replace( $sWhere, "", -3 );
$sWhere .= ')';
}
/* Individual column filtering */
for ( $i=0 ; $i<count($aColumns) ; $i++ ) {
if ( isset($_GET['bSearchable_'.$i]) && $_GET['bSearchable_'.$i] == "true" && $_GET['sSearch_'.$i] != '' ) {
if ( $sWhere == "" ) {
$sWhere = "WHERE ";
} else {
$sWhere .= " AND ";
}
$sWhere .= $aColumns[$i]." LIKE '%".addslashes($_GET['sSearch_'.$i])."%' ";
}
}
 
/* Paging */
$top = (isset($_GET['iDisplayStart']))?((int)$_GET['iDisplayStart']):0 ;
$limit = (isset($_GET['iDisplayLength']))?((int)$_GET['iDisplayLength'] ):10;
$sQuery = "SELECT TOP $limit ".implode(",",$aColumns)."
FROM $sTable
$sWhere ".(($sWhere=="")?" WHERE ":" AND ")." $sIndexColumn NOT IN
(
SELECT $sIndexColumn FROM
(
SELECT TOP $top ".implode(",",$aColumns)."
FROM $sTable
$sWhere
$sOrder
)
as [virtTable]
)
$sOrder";
$rResult = mssql_query($sQuery) or die(mssql_get_last_message());
 
 
/* Data set length after filtering */
$sQueryCnt = "SELECT * FROM $sTable $sWhere";
$rResultCnt = mssql_query( $sQueryCnt );
$iFilteredTotal = mssql_num_rows( $rResultCnt );
 
 
/* Total data set length */
$sQuery = "
SELECT COUNT(IdTipoIntervencion)
FROM $sTable
";
$rResultTotal = mssql_query( $sQuery );
$aResultTotal = mssql_fetch_array($rResultTotal);
$iTotal = $aResultTotal[0];
 
 
/*
* Output
*/
$output = array(
"sEcho" => intval($_GET['sEcho']),
"iTotalRecords" => $iTotal,
"iTotalDisplayRecords" => $iFilteredTotal,
"aaData" => array()
);
 
 
while ( $aRow = mssql_fetch_array( $rResult ) )
{
$row = array();
for ( $i=0 ; $i<count($aColumns) ; $i++ )
{
 
/* General output */
$row[] = $aRow[ $aColumns[$i] ];
 
}
$output['aaData'][] = $row;
}
 
echo json_encode( $output );
 
?>