<?php
App::uses('AppModel', 'Model');
/**
 * Activity Model
 *
 * @property TipoDato $TipoDato
*/ 
class IntervencionTipo extends AppModel {
	/**
	 * Display field
	 *
	 * @var string
	 */
	public $name = 'IntervencionTipo';
	
	public $schema = 'actividades';

	/**
	 * belongsTo associations
	 *
	 * @var array
	 */
	public $belongsTo = array(
			'IntervencionTipoParent' => array(
					'className' => 'IntervencionTipo',
					'foreignKey' => 'parent_id',
					'conditions' => '',
					'fields' => '',
					'order' => ''
			)
	);
	
	/**
	 * hasMany associations
	 *
	 * @var array
	 */
	public $hasMany = array(
			'IntervencionMaestro' => array(
					'className' => 'IntervencionMaestro',
					'foreignKey' => 'intervencion_tipo_id',
					'dependent' => false,
					'conditions' => '',
					'fields' => '',
					'order' => '',
					'limit' => '',
					'offset' => '',
					'exclusive' => '',
					'finderQuery' => '',
					'counterQuery' => ''
			),
			'IntervencionTipoWebService' => array(
					'className' => 'IntervencionTipoWebService',
					'foreignKey' => 'intervencion_tipo_id',
					'dependent' => false,
					'conditions' => '',
					'fields' => '',
					'order' => '',
					'limit' => '',
					'offset' => '',
					'exclusive' => '',
					'finderQuery' => '',
					'counterQuery' => ''
			)
	);

}