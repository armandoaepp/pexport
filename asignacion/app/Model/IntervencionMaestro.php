<?php
App::uses('AppModel', 'Model');
/**
 * Activity Model
 *
 * @property TipoDato $TipoDato
*/ 
class IntervencionMaestro extends AppModel {
	/**
	 * Display field
	 *
	 * @var string
	 */
	public $name = 'IntervencionMaestro';
	
	public $schema = 'actividades';

	/**
	 * hasMany associations
	 *
	 * @var array
	 */
	public $hasMany = array(
			'IntervencionRegistro' => array(
					'className' => 'IntervencionRegistro',
					'foreignKey' => 'intervencion_maestro_id',
					'dependent' => false,
					'conditions' => '',
					'fields' => '',
					'order' => '',
					'limit' => '',
					'offset' => '',
					'exclusive' => '',
					'finderQuery' => '',
					'counterQuery' => ''
			)
	);
	
	/**
	 * belongsTo associations
	 *
	 * @var array
	 */
	public $belongsTo = array(
			'ObjetoGrupo' => array(
					'className' => 'ObjetoGrupo',
					'foreignKey' => 'objeto_grupo_id',
					'conditions' => '',
					'fields' => '',
					'order' => ''
			),
			'IntervencionTipo' => array(
					'className' => 'IntervencionTipo',
					'foreignKey' => 'intervencion_tipo_id',
					'conditions' => '',
					'fields' => '',
					'order' => ''
			)
	);
	
	
	/**
	 * Get Json Form
	 * @param intervencion_tipo $intervencion_tipo_id
	 * @return json
	 * @author Geynen
	 * @version 13 Febrero 2015
	 */
	public function getJsonForm($intervencion_tipo_id){
		$this->loadModel('ObjetoGrupo');
		$this->loadModel('Objeto');
		
		/* GRUPOS */
		$arr_objeto_grupo = $this->ObjetoGrupo->find('all',array(
				'fields'=>'ObjetoGrupo.grupo_objeto_id, min(IntervencionMaestro.orden)',
				'joins'=>array(
						array('table' => 'actividades.intervencion_maestros',
								'alias' => 'IntervencionMaestro',
								'type' => 'INNER',
								'conditions' => array(
										'IntervencionMaestro.objeto_grupo_id = ObjetoGrupo.id'
								))
				),
				'conditions'=>array('intervencion_tipo_id'=>$intervencion_tipo_id),
				'group'=>array('ObjetoGrupo.grupo_objeto_id'),
				'order'=>array('min(IntervencionMaestro.orden) asc')
		));
		
		$arr_og = array();
		foreach ($arr_objeto_grupo as $k=> $objeto_grupo){
			$arr_og[]= $objeto_grupo['ObjetoGrupo']['grupo_objeto_id'];
		}
		
		$arr_obj_objeto= $this->Objeto->findObjects('all',array(
				'conditions'=>array('id'=> $arr_og)
		));
		/* GRUPOS - $arr_obj_objeto */
		
		$arr_formulario = array();
		foreach ($arr_obj_objeto as $k => $obj_objeto){
			$arr_formulario[] = $this->getJsonObjeto($obj_objeto,$intervencion_tipo_id);
		}
		
		return json_encode($arr_formulario);		
	}
	
	/**
	 * Get Json Objeto
	 * @param objteo $objeto
	 * @return json
	 * @author Geynen
	 * @version 13 Febrero 2015
	 */
	private function getJsonObjeto($objeto,$intervencion_tipo_id){
		$arr_objeto = $objeto->data['Objeto'];
		$arr_objeto['tipo_objeto'] = $objeto->TipoObjeto->data['TipoObjeto'];
		$arr_objeto['tipo_dato'] = $objeto->TipoDato->data['TipoDato'];
		
		if(count($objeto->ObjetoAtributo)>0){
			foreach ($objeto->ObjetoAtributo as $j => $obj_objeto_atributo){
				$arr_objeto['atributos'][] = $obj_objeto_atributo->data['ObjetoAtributo'];
			}
		}else{
			$arr_objeto['atributos'] = null;
		}
		
		if($objeto->TipoObjeto->getAttr('datos')=='I'){
			if(count($objeto->ObjetoDato)>0){
				foreach ($objeto->ObjetoDato as $jj => $obj_objeto_dato){
					$arr_objeto['datos'][] = $obj_objeto_dato->data['ObjetoDato'];
				}
			}else{
				$arr_objeto['datos'] = null;
			}
		}
		
		if($objeto->TipoObjeto->getAttr('datos')=='E'){
			if(count($objeto->ObjetoOrigenDato)>0){
				foreach ($objeto->ObjetoOrigenDato as $jj => $obj_objeto_origen_dato){
					$arr_objeto['origen_datos'][] = $obj_objeto_origen_dato->data['ObjetoOrigenDato'];
				}
			}else{
				$arr_objeto['origen_datos'] = null;
			}
		}
		
		foreach ($objeto->getItemsGrupo($intervencion_tipo_id) as $kk => $obj_objeto){
			$arr_objeto['items'][] = $this->getJsonObjeto($obj_objeto,$intervencion_tipo_id);
		}
		
		return $arr_objeto;
	}

}