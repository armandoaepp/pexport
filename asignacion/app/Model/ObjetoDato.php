<?php
App::uses('AppModel', 'Model');
/**
 * Activity Model
 * Contiene los datos de un objeto (Ejemplo de Objeto: Select)
 *
 * @property ObjetoDato $ObjetoDato
*/ 
class ObjetoDato extends AppModel {
	/**
	 * Display field
	 *
	 * @var string
	 */
	public $name = 'ObjetoDato';
	
	public $schema = 'formulario';
	
	/**
	 * belongsTo associations
	 *
	 * @var array
	 */
	public $belongsTo = array(
			'Objeto' => array(
					'className' => 'Objeto',
					'foreignKey' => 'objeto_id',
					'conditions' => '',
					'fields' => '',
					'order' => ''
			),
			'ObjetoOpen' => array(
					'className' => 'Objeto',
					'foreignKey' => 'children_objeto_id',
					'conditions' => '',
					'fields' => '',
					'order' => ''
			)
	);

}