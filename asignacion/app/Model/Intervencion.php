<?php
App::uses('AppModel', 'Model');
/**
 * Activity Model
 *
 * @property Usuario $Usuario
*/ 
class Intervencion extends AppModel {
	/**
	 * Display field
	 *
	 * @var string
	 */
	public $name = 'Intervencion';
	public $useTable = 'commovtab_intervencion';
	public $schema = 'public';
	
	/**
	 * hasMany associations
	 *
	 * @var array
	 */
	public $hasMany = array(
			'IntervencionRegistro' => array(
					'className' => 'IntervencionRegistro',
					'foreignKey' => 'idintervencion',
					'dependent' => false,
					'conditions' => '',
					'fields' => '',
					'order' => '',
					'limit' => '',
					'offset' => '',
					'exclusive' => '',
					'finderQuery' => '',
					'counterQuery' => ''
			)
	);
	
	/**
	 * Get campos de la estructura de la tabla para popularizar formulario
	 * @author Geynen
	 * @version 24 Febrero 2015
	 */
	public function getCamposPopulate(){
		$this->setDateSourceCustom();
		$db = $this->getDataSource();
		$sql = "SELECT * FROM information_schema.columns WHERE table_schema = 'public' and table_name = ('commovtab_intervencion')";
		return $db->query($sql);
	}
}