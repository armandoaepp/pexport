<?php
App::uses('AppModel', 'Model');
/**
 * Activity Model
 *
 * @property TipoDato $TipoDato
*/ 
class IntervencionGrupo extends AppModel {
	/**
	 * Display field
	 *
	 * @var string
	 */
	public $name = 'IntervencionGrupo';
	
	public $schema = 'actividades';

	/**
	 * hasMany associations
	 *
	 * @var array
	 */
	public $hasMany = array(
			'IntervencionItem' => array(
					'className' => 'IntervencionItem',
					'foreignKey' => 'intervencion_grupo_id',
					'dependent' => false,
					'conditions' => '',
					'fields' => '',
					'order' => '',
					'limit' => '',
					'offset' => '',
					'exclusive' => '',
					'finderQuery' => '',
					'counterQuery' => ''
			)
	);
	
	/**
	 * belongsTo associations
	 *
	 * @var array
	 */
	public $belongsTo = array(
			'ObjetoGrupo' => array(
					'className' => 'ObjetoGrupo',
					'foreignKey' => 'grupo_objeto_id',
					'conditions' => '',
					'fields' => '',
					'order' => ''
			),
			'IntervencionTipo' => array(
					'className' => 'IntervencionTipo',
					'foreignKey' => 'intervencion_tipo_id',
					'conditions' => '',
					'fields' => '',
					'order' => ''
			)
	);
	
	/**
	 * Get Json Form
	 * @param intervencion_tipo $intervencion_tipo_id
	 * @return json
	 * @author Geynen
	 * @version 13 Febrero 2015
	 */
	public function getJsonForm($intervencion_tipo_id){
		$this->loadModel('ObjetoGrupo');
		$this->loadModel('Objeto');
	
		/* GRUPOS */
		$this->Objeto->virtualFields = array(
				'intervencion_grupo_id' => 'IntervencionGrupo.id'
		);
		$arr_obj_objeto = $this->Objeto->findObjects('all',array(
				'fields'=>'Objeto.*, intervencion_grupo_id',
				'joins'=>array(
						array('table' => 'actividades.intervencion_grupos',
								'alias' => 'IntervencionGrupo',
								'type' => 'INNER',
								'conditions' => array(
										'IntervencionGrupo.grupo_objeto_id = Objeto.id'
								))
				),
				'conditions'=>array('intervencion_tipo_id'=>$intervencion_tipo_id,
						'parent_id is null'
				),
				'order'=>array('IntervencionGrupo.orden asc')
		));
		/* GRUPOS - $arr_obj_objeto */
	
		$arr_formulario = array();
		foreach ($arr_obj_objeto as $k => $obj_objeto){
			$arr_formulario[] = $this->getJsonObjeto($obj_objeto,$intervencion_tipo_id);
		}
	
		return json_encode($arr_formulario);
	}
	
	/**
	 * Get Json Objeto
	 * @param objteo $objeto
	 * @return json
	 * @author Geynen
	 * @version 13 Febrero 2015
	 */
	private function getJsonObjeto($objeto,$intervencion_tipo_id){
		$arr_objeto = $objeto->data['Objeto'];
		$arr_objeto['tipo_objeto'] = $objeto->TipoObjeto->data['TipoObjeto'];
		$arr_objeto['tipo_dato'] = $objeto->TipoDato->data['TipoDato'];
	
		$objeto->buildHasMany('ObjetoAtributo');
		if(count($objeto->ObjetoAtributo)>0){
			foreach ($objeto->ObjetoAtributo as $i => $obj_objeto_atributo){
				$arr_objeto['atributos'][$i] = $obj_objeto_atributo->data['ObjetoAtributo'];
				$arr_objeto['atributos'][$i]['tipo'] = $obj_objeto_atributo->TipoAtributo->data['TipoAtributo'];
			}
		}else{
			$arr_objeto['atributos'] = null;
		}
	
		if($objeto->TipoObjeto->getAttr('datos')=='I'){
			if(count($objeto->ObjetoDato)>0){
				foreach ($objeto->ObjetoDato as $jj => $obj_objeto_dato){
					$arr_objeto['datos'][] = $obj_objeto_dato->data['ObjetoDato'];
				}
			}else{
				$arr_objeto['datos'] = null;
			}
		}
	
		if($objeto->TipoObjeto->getAttr('datos')=='E'){
			if(count($objeto->ObjetoOrigenDato)>0){
				$arr_data_tmp = $objeto->ObjetoOrigenDato->data['ObjetoOrigenDato'];
				unset($arr_data_tmp['bd_host']);
				unset($arr_data_tmp['bd_nombre']);
				unset($arr_data_tmp['bd_campos']);
				unset($arr_data_tmp['bd_campos_ocultos']);
				unset($arr_data_tmp['bd_tablas']);
				unset($arr_data_tmp['bd_filtros']);
				$arr_objeto['origen_datos'] = $arr_data_tmp;
			}else{
				$arr_objeto['origen_datos'] = null;
			}
		}
	
		if($objeto->TipoObjeto->getAttr('objetos')==true){
			foreach ($objeto->getItemsGrupo($intervencion_tipo_id) as $kk => $obj_objeto){
				$arr_objeto['items'][] = $this->getJsonObjeto($obj_objeto,$intervencion_tipo_id);
			}
		}
	
		return $arr_objeto;
	}
	
}
