<?php
App::uses('AppModel', 'Model');
/**
 * Activity Model
 *
 * @property Usuario $Usuario
*/ 
class CommovtabUsuario extends AppModel {
	/**
	 * Display field
	 *
	 * @var string
	 */
	public $name = 'CommovtabUsuario';
	public $useTable = 'commovtab_usuario';

	public $schema = 'public';

	/**
	 * $belongsTo associations
	 *
	 * @var array
	 */

	
	public function validarUsuarioWeb($usuario){
		$this->setDateSourceCustom ();$db = $this->getDataSource();
		$sql = "select * from ComMovTab_Usuario cu
		inner join GlobalMaster_Persona gp
		on cu.IdPersona = gp.IdPersona where cu.NomUsuario = '$usuario' and cu.IdEstado = 3";
		return $db->query($sql);
	}
}