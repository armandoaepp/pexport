<?php
App::uses('AppModel', 'Model');
/**
 * Activity Model
 *
 * @property TipoDato $TipoDato
*/ 
class IntervencionItem extends AppModel {
	/**
	 * Display field
	 *
	 * @var string
	 */
	public $name = 'IntervencionItem';
	
	public $schema = 'actividades';

	/**
	 * hasMany associations
	 *
	 * @var array
	 */
	public $hasMany = array(
			'IntervencionRegistro' => array(
					'className' => 'IntervencionRegistro',
					'foreignKey' => 'intervencion_item_id',
					'dependent' => false,
					'conditions' => '',
					'fields' => '',
					'order' => '',
					'limit' => '',
					'offset' => '',
					'exclusive' => '',
					'finderQuery' => '',
					'counterQuery' => ''
			)
	);
	
	/**
	 * belongsTo associations
	 *
	 * @var array
	 */
	public $belongsTo = array(
			'IntervencionTipo' => array(
					'className' => 'IntervencionGrupo',
					'foreignKey' => 'intervencion_grupo_id',
					'conditions' => '',
					'fields' => '',
					'order' => ''
			),
			'ObjetoGrupo' => array(
					'className' => 'ObjetoGrupo',
					'foreignKey' => 'objeto_grupo_id',
					'conditions' => '',
					'fields' => '',
					'order' => ''
			)
	);

}