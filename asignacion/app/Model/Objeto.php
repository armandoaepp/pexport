<?php
App::uses('AppModel', 'Model');
/**
 * Activity Model
 *
 * @property Objeto $Objeto
*/ 
class Objeto extends AppModel {
	/**
	 * Display field
	 *
	 * @var string
	 */
	public $name = 'Objeto';
	
	public $schema = 'formulario';
	
	/**
	 * belongsTo associations
	 *
	 * @var array
	 */
	public $belongsTo = array(
			'TipoObjeto' => array(
					'className' => 'TipoObjeto',
					'foreignKey' => 'tipo_objeto_id',
					'conditions' => '',
					'fields' => '',
					'order' => ''
			),
			'TipoDato' => array(
					'className' => 'TipoDato',
					'foreignKey' => 'tipo_dato_id',
					'conditions' => '',
					'fields' => '',
					'order' => ''
			),
			'ObjetoGrupoParent' => array(
					'className' => 'ObjetoGrupo',
					'foreignKey' => 'item_objeto_id',
					'conditions' => '',
					'fields' => '',
					'order' => ''
			),
			'ObjetoOrigenDato' => array(
					'className' => 'ObjetoOrigenDato',
					'foreignKey' => 'objeto_origen_dato_id',
					'conditions' => '',
					'fields' => '',
					'order' => ''
			)
	);

	/**
	 * hasMany associations
	 *
	 * @var array
	 */
	public $hasMany = array(
			'ObjetoAtributo' => array(
					'className' => 'ObjetoAtributo',
					'foreignKey' => 'objeto_id',
					'dependent' => false,
					'conditions' => '',
					'fields' => '',
					'order' => 'orden asc',
					'limit' => '',
					'offset' => '',
					'exclusive' => '',
					'finderQuery' => '',
					'counterQuery' => ''
			),
			'ObjetoDato' => array(
					'className' => 'ObjetoDato',
					'foreignKey' => 'objeto_id',
					'dependent' => false,
					'conditions' => '',
					'fields' => '',
					'order' => 'orden asc',
					'limit' => '',
					'offset' => '',
					'exclusive' => '',
					'finderQuery' => '',
					'counterQuery' => ''
			),
			'ObjetoGrupo' => array(
					'className' => 'ObjetoGrupo',
					'foreignKey' => 'grupo_objeto_id',
					'dependent' => false,
					'conditions' => '',
					'fields' => '',
					'order' => 'orden asc',
					'limit' => '',
					'offset' => '',
					'exclusive' => '',
					'finderQuery' => '',
					'counterQuery' => ''
			)
	);	
	
	/**
	 * Trae los Items de cada Grupo
	 * @param integer $intervencion_tipo_id
	 * @return ArrayObject
	 * @author Geynen
	 * @version 16 Febrero 2015
	 */
	public function getItemsGrupo($intervencion_tipo_id){
		$this->virtualFields = array(
				'intervencion_grupo_id' => 'IntervencionItem.intervencion_grupo_id',
				'intervencion_item_id' => 'IntervencionItem.id',
				'defecto_desde_intervencion_item_id' => 'IntervencionItem.defecto_desde_intervencion_item_id',
				'campo_servicio' => 'IntervencionItem.campo_servicio'
		);
		$arr_obj_objeto = $this->findObjects('all',array(
				'fields'=>'Objeto.*, intervencion_grupo_id, intervencion_item_id, defecto_desde_intervencion_item_id, campo_servicio',
				'joins'=>array(
						array('table' => 'formulario.objeto_grupos',
								'alias' => 'ObjetoGrupo',
								'type' => 'INNER',
								'conditions' => array(
										'ObjetoGrupo.item_objeto_id = Objeto.id'
								)
						),
						array('table' => 'actividades.intervencion_items',
								'alias' => 'IntervencionItem',
								'type' => 'INNER',
								'conditions' => array(
										'IntervencionItem.objeto_grupo_id = ObjetoGrupo.id'
								)
						),
						array('table' => 'actividades.intervencion_grupos',
								'alias' => 'IntervencionGrupo',
								'type' => 'INNER',
								'conditions' => array(
										'IntervencionItem.intervencion_grupo_id = IntervencionGrupo.id',
										'OR'=>array('IntervencionGrupo.parent_id'=>$this->intervencion_grupo_id,'IntervencionGrupo.id'=>$this->intervencion_grupo_id),
										'IntervencionGrupo.grupo_objeto_id'=>$this->id,
										'IntervencionGrupo.intervencion_tipo_id'=>$intervencion_tipo_id,
								)
						)
				),
				'order'=>array('IntervencionItem.orden asc')
		));
	
		return $arr_obj_objeto;
	}

}