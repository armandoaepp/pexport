<?php
App::uses('AppModel', 'Model');
/**
 * Activity Model
 *
 * @property Orden $Orden
*/ 
class Orden extends AppModel {
	/**
	 * Display field
	 *
	 * @var string
	 */
	public $name = 'Orden';
	public $useTable = 'commovtab_orden';
	public $schema = 'public';
	
	/**
	 * hasOne associations
	 *
	 * @var array
	 */
	public $hasOne = array(
        'Intervencion' => array(
				'className' => 'Intervencion',
				'foreignKey' => 'idorden',
				'dependent' => false,
				'conditions' => '',
				'fields' => '',
				'order' => '',
				'limit' => '1',
				'offset' => '',
				'exclusive' => '',
				'finderQuery' => '',
				'counterQuery' => ''
		)
    );
	
	/**
	 * Get campos de la estructura de la tabla para popularizar formulario
	 * @author Geynen
	 * @version 24 Febrero 2015
	 */
	public function getCamposPopulate(){
		$this->setDateSourceCustom();
		$db = $this->getDataSource();
		$sql = "SELECT * FROM information_schema.columns WHERE table_schema = 'public' and table_name = ('commovtab_orden')";
		return $db->query($sql);
	}
}