<?php
App::uses('AppModel', 'Model');
/**
 * Activity Model
 *
 * @property ObjetoOrigenDato $ObjetoOrigenDato
*/ 
class ObjetoOrigenDato extends AppModel {
	/**
	 * Display field
	 *
	 * @var string
	 */
	public $name = 'ObjetoOrigenDato';
	
	public $schema = 'formulario';
	
	/**
	 * hasMany associations
	 *
	 * @var array
	 */
	public $hasMany = array(
			'Objeto' => array(
					'className' => 'Objeto',
					'foreignKey' => 'objeto_origen_dato_id',
					'dependent' => false,
					'conditions' => '',
					'fields' => '',
					'order' => '',
					'limit' => '',
					'offset' => '',
					'exclusive' => '',
					'finderQuery' => '',
					'counterQuery' => ''
			)
	);

}