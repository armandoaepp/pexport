<?php
App::uses('AppModel', 'Model');
/**
 * Activity Model
 *
 * @property ObjetoGrupo $ObjetoGrupo
*/ 
class ObjetoGrupo extends AppModel {
	/**
	 * Display field
	 *
	 * @var string
	 */
	public $name = 'ObjetoGrupo';
	
	public $schema = 'formulario';
	
	/**
	 * belongsTo associations
	 *
	 * @var array
	 */
	public $belongsTo = array(
			'ObjetoParent' => array(
					'className' => 'Objeto',
					'foreignKey' => 'grupo_objeto_id',
					'conditions' => '',
					'fields' => '',
					'order' => ''
			),
			'Objeto' => array(
					'className' => 'Objeto',
					'foreignKey' => 'item_objeto_id',
					'conditions' => '',
					'fields' => '',
					'order' => ''
			)
	);

	/**
	 * hasMany associations
	 *
	 * @var array
	 */
	public $hasMany = array(
			'ObjetoGrupoItem' => array(
					'className' => 'Objeto',
					'foreignKey' => 'item_objeto_id',
					'dependent' => false,
					'conditions' => '',
					'fields' => 'orden asc',
					'order' => '',
					'limit' => '',
					'offset' => '',
					'exclusive' => '',
					'finderQuery' => '',
					'counterQuery' => ''
			)
	);

}