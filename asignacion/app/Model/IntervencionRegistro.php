<?php
App::uses('AppModel', 'Model');
/**
 * Activity Model
 *
 * @property TipoDato $TipoDato
*/ 
class IntervencionRegistro extends AppModel {
	/**
	 * Display field
	 *
	 * @var string
	 */
	public $name = 'IntervencionRegistro';
	
	public $schema = 'actividades';

	/**
	 * hasMany associations
	 *
	 * @var array
	 */
	public $hasMany = array(
			'Objeto' => array(
					'className' => 'Objeto',
					'foreignKey' => 'tipo_objeto_id',
					'dependent' => false,
					'conditions' => '',
					'fields' => '',
					'order' => '',
					'limit' => '',
					'offset' => '',
					'exclusive' => '',
					'finderQuery' => '',
					'counterQuery' => ''
			)
	);

}