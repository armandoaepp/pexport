<?php
App::uses('AppModel', 'Model');
/**
 * Activity Model
 *
 * @property TipoAtributo $TipoAtributo
*/ 
class TipoAtributo extends AppModel {
	/**
	 * Display field
	 *
	 * @var string
	 */
	public $name = 'TipoAtributo';
	
	public $schema = 'formulario';

	/**
	 * hasMany associations
	 *
	 * @var array
	 */
	public $hasMany = array(
			'ObjetoAtributo' => array(
					'className' => 'ObjetoAtributo',
					'foreignKey' => 'tipo_atributo_id',
					'dependent' => false,
					'conditions' => '',
					'fields' => '',
					'order' => '',
					'limit' => '',
					'offset' => '',
					'exclusive' => '',
					'finderQuery' => '',
					'counterQuery' => ''
			)
	);

}