<?php
App::uses('AppModel', 'Model');
/**
 * Activity Model
 *
 * @property ObjetoAtributo $ObjetoAtributo
*/ 
class ObjetoAtributo extends AppModel {
	/**
	 * Display field
	 *
	 * @var string
	 */
	public $name = 'ObjetoAtributo';
	
	public $schema = 'formulario';
	
	/**
	 * belongsTo associations
	 *
	 * @var array
	 */
	public $belongsTo = array(
			'Objeto' => array(
					'className' => 'Objeto',
					'foreignKey' => 'objeto_id',
					'conditions' => '',
					'fields' => '',
					'order' => ''
			),
			'TipoAtributo' => array(
					'className' => 'TipoAtributo',
					'foreignKey' => 'tipo_atributo_id',
					'conditions' => '',
					'fields' => '',
					'order' => ''
			)
	);

}