<?php
App::uses('AppModel', 'Model');
/**
 * Activity Model
 *
 * @property IntervencionTipoWebService $IntervencionTipoWebService
*/ 
class IntervencionTipoWebService extends AppModel {
	/**
	 * Display field
	 *
	 * @var string
	 */
	public $name = 'IntervencionTipoWebService';
	
	public $schema = 'actividades';
	
	/**
	 * belongsTo associations
	 *
	 * @var array
	 */
	public $belongsTo = array(
			'IntervencionTipo' => array(
					'className' => 'IntervencionTipo',
					'foreignKey' => 'intervencion_tipo_id',
					'conditions' => '',
					'fields' => '',
					'order' => ''
			)
	);
	
}
